<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//$route['default_controller'] = 'admin/dashboard';


$route['default_controller'] = 'welcome';
$route['home'] = 'th/home';
$route['home/detail/(.*)'] = 'th/home/detail/$1';
$route['about'] = 'th/about';
$route['product'] = 'th/product';
$route['product/detail/(.*)'] = 'th/product/detail/$1';
$route['contact'] = 'th/contact';
$route['contact/(.*)'] = 'th/contact/intereste/$1';
$route['member'] = 'th/member';

$route['privacypolicy'] = 'th/pdpa/privacypolicy';
$route['termsservice'] = 'th/pdpa/termsservice';

//$route['default_controller'] = 'welcome';
$route['admin'] = 'admin/dashboard';
$route['sale'] = 'sale/calculate';
$route['admin/contract/contractPDF/(.*)/(.*)'] = "admin/contract/contractPDF/$1/$2";


/**  Viewer */
$route['viewer'] = "viewer/User";
$route['viewer/contract'] = "viewer/Contract";


/** ADMIN  */
###  Contract Addon ###
$route['admin/contractaddon/(.*)'] = "admin/ContractAddon/list/$1";
$route['contractaddon/getResContract'] = "admin/ContractAddon/GetContract";
$route['contractaddon/getResContractAddon'] = "admin/ContractAddon/GetContractAddon";
$route['contractaddon/getResProduct'] = "admin/ContractAddon/GetProduct";
$route['contractaddon/addon'] = "admin/ContractAddon/CreatAddon";

###  Product Addon ###
$route['contractaddon/addonproduct'] = "admin/ContractAddon/CreatAddonProduct";
$route['contractaddon/contractAddonProduct'] = "admin/ContractAddon/GetContractAddonProduct";
$route['contractaddon/getResPcate'] = "admin/ContractAddon/getResProductCate";
$route['contractaddon/getResPmaster'] = "admin/ContractAddon/getResProductMaster";
$route['contractaddon/delPmaster'] = "admin/ContractAddon/delProductMaster";
$route['contractaddon/delPcontract'] = "admin/ContractAddon/delContract";
$route['contractaddon/getStatus'] = "admin/ContractAddon/getStatus";

###  Serial  ###
$route['admin/contractSerial/(.*)/(.*)'] = "admin/ContractAddon/Serial/$1/$2";
$route['serialEdit'] = "admin/contractSerial/updateSerial";
$route['contractaddon/getResSerials'] = "admin/ContractAddon/getResSerials";
$route['contractaddon/UpdateSerials'] = "admin/ContractAddon/UpdateSerial";

###  Installment  ###
$route['admin/Installments/(.*)'] = "admin/ContractInst/Installment_list/$1";
$route['getResInstallments'] = "admin/ContractInst/getInstallment";
$route['admin/AddonInstallments/(.*)/(.*)'] = "admin/ContractInst/AddonInstallment_list/$1/$2";
$route['getResAddonInstallments'] = "admin/ContractInst/getAddonInstallment";
$route['admin/updateInstallments'] = "admin/ContractInst/updatContractIns";
$route['admin/sendSms'] = "admin/ContractInst/sendSMS";





/** BACKEND  */
// // customer purchase
// $route['backend/CustomerPurchase'] = "backend/CustomerPurchase/Customer_purchase";
# defult controller
$route['backend'] = "backend/product/product";

/** route -> product management */
$route['backend/product'] = "backend/product/product";
$route['backend/product/(.*)'] = "backend/product/product/$1";
$route['backend/product/ajaxuploadtmpimg'] = "backend/product/ajaxuploadtmpimg";
$route['backend/product/ajaxuploadimgOnActionEdit/(.*)']  = "backend/product/ajaxuploadimgOnActionEdit/$1";
$route['backend/product/ajaxretrievefilefromserver'] = "backend/product/ajaxretrievefilefromserver";
$route['backend/product/delete_img'] = "backend/product/delete_img";
/** EOF management */

/** route -> product part management */
$route['backend/productpart'] = "backend/productpart/productpart";
$route['backend/productpart/(.*)'] = "backend/productpart/productpart/$1";
$route['backend/productpart/ajaxretrievefilefromserver'] = "backend/productpart/ajaxretrievefilefromserver";


$route['backend/productpart/ajaxuploadproductinstallimgOnActionEdit/(.*)']  = "backend/productpart/ajaxuploadproductinstallimgOnActionEdit/$1";

/** EOF management */

/** route -> productinstallments product */
$route['backend/productinstallments'] = "backend/productinstallments/productinstallments";

$route['backend/productinstallments/(.*)'] = "backend/productinstallments/productinstallments/$1";
$route['backend/productinstallments/ajaxretrieveproductsetfilefromserver'] = "backend/productinstallments/ajaxretrieveproductsetfilefromserver";
$route['backend/productinstallments/ajaxdrawformulagroup'] = "backend/productinstallments/ajaxdrawformulagroup";
$route['backend/productinstallments/(.*)/(.*)'] = "backend/productinstallments/productinstallments/$1/$2";
/** EOF management */


/** route -> productinstall product */
$route['backend/productinstall'] = "backend/productinstall/productinstall";
$route['backend/productinstall/(.*)'] = "backend/productinstall/productinstall/$1";

$route['backend/productinstall/ajaxuploadtmpimgset'] = "backend/productinstall/ajaxuploadtmpimgset";
$route['backend/productinstall/(.*)/(.*)'] = "backend/productinstall/productinstall/$1/$2";
/** EOF management */




/** route -> product category management */
$route['backend/productcategory'] = "backend/productcategory/productcategory";
$route['backend/productcategory/(.*)'] = "backend/productcategory/productcategory/$1";
$route['backend/productcategory/(.*)/(.*)'] = "backend/productcategory/productcategory/$1/$2";
/** EOF management */

/** route -> product Serialnumber management */
$route['backend/productserialnumber'] = "backend/productserialnumber/productserialnumber";
$route['backend/productserialnumber/(.*)'] = "backend/productserialnumber/productserialnumber/$1";
$route['backend/productserialnumber/processtbl'] = "backend/productserialnumber/processtbl";
$route['backend/productserialnumber/create_newserialnumber'] = "backend/productserialnumber/create_newserialnumber";
$route['backend/productserialnumber/(.*)/(.*)'] = "backend/productserialnumber/productserialnumber/$1/$2";
/** EOF management */

// import excel -> หน้า serial number 
// $route['backend/productserialnumber/importFile'] = "backend/productserialnumber/importExcel";
// $route['backend/productserialnumber/importFile/(.*)'] = "backend/productserialnumber/importExcel/$1";

/** route -> promotion product */
$route['backend/promotion'] = "backend/promotion/promotion";
$route['backend/promotion/(.*)'] = "backend/promotion/promotion/$1";
$route['backend/promotion/(.*)/(.*)'] = "backend/promotion/promotion/$1/$2";
/** EOF management */

/** route -> uom product */
$route['backend/uom'] = "backend/uom/uom";
$route['backend/uom/(.*)'] = "backend/uom/uom/$1";
$route['backend/uom/(.*)/(.*)'] = "backend/uom/uom/$1/$2";
/** EOF management */

/** route -> uom product */
$route['backend/warranty'] = "backend/warranty/warranty";
$route['backend/warranty/(.*)'] = "backend/warranty/warranty/$1";
$route['backend/warranty/(.*)/(.*)'] = "backend/warranty/warranty/$1/$2";
/** EOF management */



/** API ( หลังบ้าน ) */
// test ส่ง sms 

$route['api/TestSendSmsV2'] = "api/SmsModule/TestSendSmsV2";

$route['api/sendnotifymessage'] = "api/SmsModule/sendnotifymessage";







// API
$route['api/login'] = "api/Application/login";
$route['api/get_contract'] = "api/Application/get_contract";
$route['api/get_contract_code'] = "api/Application/get_contract_code";
$route['api/get_contract_installments'] = "api/Application/get_contract_installments";
$route['api/get_contract_installments_all'] = "api/Application/get_contract_installments_all";
$route['api/get_profile'] = "api/Application/get_profile";
$route['api/update_device_token'] = "api/Application/update_device_token";
