<?php
class User_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	function getRows($email = null, $pass = null){ 
        $this->db->select('*');
        $this->db->where('email',$email); 
        $this->db->where('password',$pass); 
        $this->db->where('type','viewer'); 
        $this->db->from('user'); 
        $query = $this->db->get(); 
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; 
        return $result; 
    } 
    function getRowsEmail($email = null){ 
        $this->db->select('*');
        $this->db->where('email',$email);
        $this->db->from('user'); 
        $query = $this->db->get(); 
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; 
        return $result; 
    } 

    public function insert($data = array()) { 
        
        if ($this->db->insert("user", $data)) { 
            return true; 
        }
    } 
    
}

?>