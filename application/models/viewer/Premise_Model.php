<?php
class Premise_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
		 $this->load->helper('file'); 
	  } 
	  
	  public function select(){

		$Query = "select * from customer";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where customer_code ='".$id."'";
		}
		$Query = "select * from customer ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  public function amphurs(){
		$Query = "select * from amphurs order by amphur_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  public function district(){
		$Query = "select * from tumbon as tb order by district_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }


	  public function amphoe($postData=array()){
		$response = array();
		if(isset($postData['province_cod']) ){
			$Query = "select * from amphurs where provinces_id = ".$postData['province_cod']." and amphur_name not like '%*%' order by amphur_name";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
	  }

	  public function districs($DataAmphure=array()){
		$response = array();
	   if(isset($DataAmphure['amphue_cod']) ){
		   $Query = "select * from tumbon where amphurs_id = ".$DataAmphure['amphue_cod']." and district_name not like '%*%' order by district_name";
		   $Res= $this->db->query($Query);
		   $response = $Res->result();
	   }
	   return $response;
	  }

	  public function zipcodes($DataAmphure=array()){
		$response = array();
		if(isset($DataAmphure['district']) ){
			$Query = "select * from zipcode where provinces_id = ".$DataAmphure['province']." and amphurs_id = ".$DataAmphure['amphue']." and districts_id = ".$DataAmphure['district']." ";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
      }

	  public function CustomerByIdcard($Data=array()){
		$response = array();
		if(isset($Data['idcard']) ){
			$Query = "select * from customer where idcard = '".$Data['idcard']."' ";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
      }
	 

	  public function insert($data) { 
		
        if ($this->db->insert("premise", $data)) { 
            return true; 
        }
      } 

      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
   
      public function update($data,$premise_code) { 
      	
         $this->db->set($data); 
         $this->db->where("premise_code", $premise_code); 
         $this->db->update("premise", $data); 
      } 






	  public function getPremise($id, $elementID){
		$where = null;
		if(!empty($id)){
			$where = "WHERE ref_code like N'%".$id."%' AND element_id like N'%".$elementID."%'";
		}
		$Query = "SELECT * ";  
		$Query .= " FROM premise ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function insertPremis($data) { 
        if ($this->db->insert("premise", $data)) { 
            return true; 
        }
      } 

	  public function delPremis($id, $path) { 
		if ($this->db->delete("premise", "premise_code = ".$id)) { 
			if (file_exists($path)) {
				unlink($path);
			}
			return true; 
        }else{
			return false; 
		}
      } 


}

?>