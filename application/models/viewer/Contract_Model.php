<?php
class Contract_Model extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 

	public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
					case 'status':
                        if($item != null){
                            $string = "( status.id = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "contract.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " Where ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	}
	public function getToList($Search = null, $itemStt= null , $itemEnd= null){
		$Where = $this->Conditions($Search);
		
		$Query = "select * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER ( ORDER BY contract.contract_code ) AS RowNum,contract.*, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as CusAddr, " ;
		$Query .= " product.product_name, product.product_cate as productCate,  " ;
        //$Query .= " product.product_version as productVersion,  " ;
        $Query .= " product.product_detail, ";
        $Query .= " status.label, status.color, status.background_color, " ;
		$Query .= " cusType.label as customerType_name, ";
		$Query .= " conType.label as contractType_name ";
        $Query .= " FROM contract ";
        $Query .= " INNER JOIN customer ON contract.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_set product ON contract.product_id = product.product_id";
        $Query .= " LEFT JOIN status ON contract.status = status.id ";
		$Query .= " LEFT JOIN status cusType ON contract.customer_type = cusType.id  ";
		$Query .= " LEFT JOIN status conType ON contract.contract_type = conType.id  ";
        $Query .= "  ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

    public function requestStatus($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where stautus_category ='".$id."'";
		}
		$Query = "select * from status ".$where ."  order by id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}












    public function CustomerToCombo(){
		$Query = "select customer.* ";
		$Query .= "from customer inner join status on  customer.status = status.id  ";
		$Query .= "where status.stautus_category = N'210601' and status.status_code = 0 ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function ProductCateToCombo(){
        $Query = "select pc.*";
		$Query .= ", (select COUNT(product_cate) from product_set where product_cate = pc.cate_id group by product_cate ) as coundOfProductMaster";
		$Query .= " from product_category pc ";
		$Query .= " where cate_id is not null";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function CharngToCombo($column = null, $val = null){
		$Query = "SELECT * FROM charng WHERE ".$column." = N'".$val."'" ;
		$res= $this->db->query($Query)->result();
	    return $res;
	}
    public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

    public function GetProducts($product_cate = null, $customer_type = null, $contract_type = null){
		$where = null;
		if(!empty($contract_type)){
			$where = "WHERE product.product_cate = N'".$product_cate."' ";
			$where .= "and product.product_customer_type = N'".$customer_type."' ";
			$where .= "and product.product_contract_type = N'".$contract_type."' ";
		}
		$Query = "SELECT product.product_id,product.product_name, brand.brand_id, brand.brand_name  ";
		$Query .= "FROM product_set product " ;
		//$Query .= "INNER JOIN product_sub ON product.product_id = product_sub.product_id " ;
		$Query .= "LEFT JOIN brand ON product.product_brand = brand.brand_id" ;
		$Query .= " ".$where ;
		//$Query .= "GROUP BY product.product_id,product.product_name,brand.brand_id, brand.brand_name ,product.product_version " ;
		$res = $this->db->query($Query)->result();
	    return $res;
	}
    
	public function getIhbByCus($Data=array()){
		$where = null;
		if(isset($Data['customer_code']) ){
			$where = "where ihb.customer_code = N'".$Data['customer_code']."'";
		}
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    
    public function GetInstallmentTypeByProductID($product_id = null){
        $where = null;
        if(!empty($product_id)){
            $where = "WHERE installment_type.product_id LIKE '%".$product_id."%' ";
        }
        $Query = "SELECT installment_type.* FROM installment_type ".$where ;
        $res = $this->db->query($Query)->result();
        return $res;
    }

    
	public function getContractToGenCode($prefix){
		$where = null;
		if(!empty($prefix)){
			$where = "WHERE contract.contract_code LIKE '%".$prefix."%' ";
		}
		$Query = "select * from contract ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function insert($data) { 
        if ($this->db->insert("contract", $data)) { 
            return true; 
        }
    } 

    public function getStatus($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where status.id ='".$id."'";
		}
		$Query = "select status.* FROM status ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getInstallment($contract_code = null){
		$where = null;
		if(!empty($contract_code)){
			$where = "where installment.contract_code ='".$contract_code."'";
		}
		$Query = "select installment.* FROM contract_installment installment ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    
	public function insertInstallment($data) { 
        if ($this->db->insert("contract_installment", $data)) { 
            return true; 
        }
    } 
    public function GetSubByProduct($product_id = null){
		$where = null;
		if(!empty($product_id)){
			$where = "WHERE product.product_set_id LIKE '%".$product_id."%' ";
		}
		$Query = "SELECT product.* FROM product_set_sub product ".$where ;
		$res = $this->db->query($Query)->result();
	    return $res;
	}
	public function CreateSerial($data) { 
        if ($this->db->insert("contract_serialnumber", $data)) { 
            return true; 
        }
    } 

    
	public function getService($contract_code = null){
		$where = null;
		if(!empty($contract_code)){
			$where = "where contract_service.contract_code ='".$contract_code."'";
		}
		$Query = "select contract_service.* FROM contract_service ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function insertService($data) { 
		
        if ($this->db->insert("contract_service", $data)) { 
            return true; 
        }
    } 
    public function insertSupporter($data) {
        if ($this->db->insert("contract_supporter", $data)) { 
            return true; 
        }
    } 

    


	public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "select COUNT(*) as allitems from contract INNER JOIN customer ON contract.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN status ON contract.status = status.id ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	}


    public function selectOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = "WHERE contract.contract_code ='".$id."'";
		}

		$Query = "SELECT contract.*,";
		$Query .= "customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as customerAddr,";
		$Query .= "product.product_name, product.product_cate,";
        //$Query .= "product.product_version";
        $Query .= "product.product_detail,product_category.cate_name, brand.brand_name,";
		$Query .= "status.label, status.color, status.background_color, ";
		$Query .= "cusType.label as customerType_name, ";
		$Query .= "conType.label as contractType_name ";
		$Query .= " FROM contract ";
		$Query .= " LEFT JOIN customer ON contract.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_set product  ON contract.product_id = product.product_id";
		$Query .= " LEFT JOIN brand  ON contract.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN product_category ON contract.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON contract.status = status.id ";
		$Query .= " LEFT JOIN status cusType ON contract.customer_type = cusType.id  ";
		$Query .= " LEFT JOIN status conType ON contract.contract_type = conType.id  ";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function selectSupporter($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where contract_code ='".$id."'";
		}
		$Query = "SELECT TOP 1 supporter.* ";
		$Query .= " ,provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " FROM contract_supporter supporter ";
		$Query .= " LEFT JOIN provinces ON supporter.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON supporter.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON supporter.district_id = tumbon.id ".$where;
        $Query .= " ORDER BY id DESC ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getInhabitedEdit($id = null){
		$where = null;
		if(!empty($id)){
			//$where = "where ihb.customer_code = N'".$id."'";
			$where = "where ihb.inhabited_code = N'".$id."'";
		}
		
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    
    public function update($data,$temp_code) { 
        $this->db->set($data); 
        $this->db->where("contract_code", $temp_code); 
        $this->db->update("contract", $data); 
    }
	public function getContractToGenCodes(){
		$Query = "select * from contract ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
     
	public function selectOneDeatil($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where contract.contract_code ='".$id."'";
		}
		$Query = "select contract.* ";
		$Query .= ", cus.office_name, cus.firstname, cus.lastname, cus.addr as address, contract.addr as installationlocation, cus.idcard , cus.birthday";
		$Query .= ", cus.tel, cus.email, cus.sex , cus.phone, cus.career_text, cus.monthly_income, cus.type";
		//$Query .= ", inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv ";
		$Query .= ", product.product_name, product.product_cate , product.product_contract_type ";
        $Query .= ", product.product_version, product.product_pricce as productPricce, product.product_detail, product.product_contract_doc";
        $Query .= ", product_category.cate_name";
		$Query .= ", status.color,status.background_color, status.label, brand.brand_name ";

		$Query .= " FROM contract ";
		$Query .= " LEFT JOIN customer cus  ON contract.customer_code = cus.customer_code";
		$Query .= " LEFT JOIN product_set product  ON contract.product_id = product.product_id";
		$Query .= " LEFT JOIN brand ON contract.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN product_category ON contract.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON contract.status = status.id  ";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

    public function detailAddress($id = null, $cate = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited.customer_code = N'".$id."' and inhabited.category = N'".$cate."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
    }

    public function detailInstallationLocation($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where inhabited_code ='".$id."'";
		}
		$Query = "select inhabited.*, p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	
	public function SubByProductId($code = null, $id = null){
		$where = null;
		if(!empty($id)){
			$where = "WHERE psub.product_set_id = N'".$id."'";
		}
		$Query = "SELECT psub.* ";
		$Query .= " , serial.serial_number";
		$Query .= " , pm.product_master_name, pm.product_master_version  ";
		$Query .= " , subType.label  ";
		$Query .= " FROM product_set_sub psub";
		$Query .= " JOIN contract_serialnumber serial ON psub.product_set_id = serial.product_id and psub.id = serial.product_sub_id  ";
		$Query .= " left JOIN product_master pm ON psub.product_master_id = pm.product_master_id   ";
		$Query .= " left JOIN status subType ON psub.type = subType.id";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function ProductSubByProductId($code = null, $id = null){
		$where = null;
		if(!empty($id)){
			$where = "WHERE psub.product_set_id = N'".$id."'";
		}
		$Query = "SELECT psub.* ";
		$Query .= " , (select top 1 sr.serial_number from contract_serialnumber sr where sr.product_id = psub.product_set_id and sr.product_sub_id = psub.id order by sr.no desc) as serial_number";
		$Query .= " , pm.product_master_name, pm.product_master_version  ";
		$Query .= " , subType.label  ";
		$Query .= " FROM product_set_sub psub";
		$Query .= " JOIN product_master pm ON psub.product_master_id = pm.product_master_id   ";
		$Query .= " left JOIN status subType ON psub.type = subType.id";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function serialNodata($code = null, $id = null){
		$serialNo ="select top 1 max(serial.no) as no FROM contract ";
		$serialNo .="JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_id = serial.product_id  ";
		$serialNo .="WHERE contract.contract_code = N'".$code."'  and serial.product_id = N'".$id."' ";
		$serialNoRes= $this->db->query($serialNo);
		$serialNodata = $serialNoRes->result();
	    return $serialNodata;
	}

	public function serialData($code = null, $id = null, $no = null){
		$serial = "select serial.*, pm.product_master_name as productSubName, psub.count  ";
		$serial .= "FROM contract   ";
		$serial .= "JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_id = serial.product_id " ; 
		$serial .= "JOIN product_set_sub psub ON serial.product_sub_id = psub.id  ";
		$serial .= "JOIN product_master pm ON psub.product_master_id = pm.product_master_id ";
		$serial .= "WHERE contract.contract_code = N'".$code."'  and serial.product_id = N'".$id."' and serial.no = N'".$no."' ";
		$serial .= "order by psub.type ";
		$serialRes= $this->db->query($serial);
		$serialData = $serialRes->result();

		$ser = "";
		$ser .= "select serial.* ";
		$ser .= ", pm.product_master_name as productSubName ";
		//$ser .= ", psub.count   ";
		$ser .= "FROM contract_addon_product contract ";
		$ser .= "LEFT JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_addon_code = serial.product_sub_id ";
		$ser .= "LEFT JOIN product_master pm ON contract.product_master_id = pm.product_master_id ";
		$ser .= "WHERE contract.contract_code = N'".$code."' ";
		$serRes= $this->db->query($ser);
		$serData = $serRes->result();
		foreach($serData as $item){
			array_push($serialData,$item );
		}

	    return $serialData;
	}
	
	public function CharngToPDF($id = null){
		$Query = "SELECT charng.* ";
		$Query .= ", agent.name as agentName, agent.sname as agentSname, p.province_name, a.amphur_name, t.district_name ";
		$Query .= ", op.province_name as officeProvince, oa.amphur_name as officeAmphurs, ot.district_name as officeDistrict ";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN agent ON charng.agent_code = agent.agent_code ";
		
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ";

		$Query .= " LEFT JOIN provinces op ON charng.office_province = op.id ";
		$Query .= " LEFT JOIN amphurs oa ON charng.office_amphurs = oa.id ";
		$Query .= " LEFT JOIN tumbon ot ON charng.office_district = ot.id ";


		$Query .= " WHERE charng_code like '%".$id."%'" ; 
		$res= $this->db->query($Query)->result();
	    return $res;
	}

	public function GetInstallmentProductID($contract_code = null){
        $where = null;
        if(!empty($contract_code)){
            $where = "WHERE ci.contract_code = '".$contract_code."' ";
        }
        $Query = "SELECT ci.* ";
		$Query .= " ,st.label ";
		$Query .= "FROM contract_installment ci ";
		$Query .= "left JOIN status st ON ci.status = st.id ";
		$Query .= " ".$where ;
        $res = $this->db->query($Query)->result();
        return $res;
    }



/*




	public function CustomerToCombo(){
		$Query = "select customer.* ";
		$Query .= "from customer inner join status on  customer.status = status.id  ";
		$Query .= "where status.stautus_category = N'210601' and status.status_code = 0 ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  public function ProductCateToCombo(){
		$Query = "SELECT * FROM product_category WHERE cate_id is not null " ;
		$res= $this->db->query($Query)->result();
	    return $res;
	  }

	  

	  
	  

	 


	  public function addrProductInstall($id = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited.inhabited_code ='".$id."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

































      
	  

	  public function getInhabited($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where ihb.customer_code = N'".$id."'";
			//$where = "where ihb.inhabited_code = N'".$id."'";
		}
		
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  

	  public function selectPremise($id = null, $mode=null){
		
		$where = null;
		if(!empty($id)){
			$where = "where premise.ref_code = N'".$id."' and premise.mode = N'".$mode."' ";
		}
		
		$Query = "select premise.*  from premise  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }




	  public function getInstOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = " and intt.temp_code = N'".$id."'";
		}
		$Query = "select intt.*, status.label as status_label, status.color, status.background_color FROM installment intt";
		$Query .= " LEFT JOIN status ON intt.status = status.status_code ";
		$Query .= " WHERE status.stautus_category = N'Installment'".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  public function customer_select(){
		$Query = "select customer.* from customer inner join status on  customer.status = status.id where status.stautus_category=N'Customer' and status.status_code =0";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function product_set(){
		
		$Query = "select inst.*" ;
		$Query .= " FROM product_install inst " ;
		$res= $this->db->query($Query)->result();

	    return $res;
	  }

	  public function getProductSerial($id = null){
		$resArr = array();
		$getMainProduct = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$id."' and used_by is null";
		$ResMainProduct= $this->db->query($getMainProduct)->result();
		if($ResMainProduct != null){
			$main = (object) array(
				'id' =>  $ResMainProduct[0]->id,
				'MainProduct' => $ResMainProduct[0]->masterproduct_id, 
				'Serial' => $ResMainProduct[0]->mp_serial_number
			);
			array_push($resArr,$main);
		}else{
			array_push($resArr,(object) array('MainProduct' => $id, 'Serial' => null));
		}
		
		
		$getSupProduct = "select * from masterproduct where  product_parent_id = N'".$id."'";
		$ResSupProduct= $this->db->query($getSupProduct)->result();
		if($ResSupProduct != null){
			$sub= [];
			foreach($ResSupProduct as $item){
				$getSerial = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$item->id."' and used_by is null";
				$ResSupProducterial = $this->db->query($getSerial)->result();
				
				if($ResSupProducterial != null){
					$subarray = (object) array(
						'id' =>  $ResSupProducterial[0]->id,
						'SupProduct' => $ResSupProducterial[0]->masterproduct_id, 
						'Serial' => $ResSupProducterial[0]->mp_serial_number
					);
					array_push($resArr , $subarray);
				}else{
					array_push($resArr,(object) array('SupProduct' => $item->id, 'Serial' => null));
				}
			}
		}
	    return $resArr;
	  }


	  

	 

     //public function delete($id) { 
         //if ($this->db->delete("news", "id = ".$id)) { 
            //return true; 
         //} 
      //} 
   
	  public function updateSupporter($data,$temp_code) { 
      	
		$this->db->set($data); 
		$this->db->where("temp_code", $temp_code); 
		$this->db->update("supporter", $data); 
	 }

	  public function updateSerial($data,$id) { 
      	
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->update("masterproduct_serialnumber", $data); 
	 }
	 
	 

	public function requestStatus($category = null, $code = null){
		$where = null;
		if(!empty($category)){
			$where = "where stautus_category ='".$category."' and status_code = N'".$code."'";
		}
		$Query = "select * from status ".$where ."  order by id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	public function getTemp($temp_code = null){
		$where = null;
		if(!empty($temp_code)){
			$where = "where temp.temp_code ='".$temp_code."'";
		}
		$Query = "select temp.* FROM temp ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}


	  public function getSparesByInstallId($id = null){
		$where = null;
		if(isset($id) ){
			$where = " where pis.product_install_id = N'".$id."' ";
		}
		$Query = "select pis.*"; 
		$Query .= " ,mp.mpname";
		$Query .= " from product_install_set pis ";
		$Query .= " inner join masterproduct mp on pis.masterproduct_id =  mp.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  

	  public function CharngToCombo($column = null, $val = null){
		$Query = "SELECT * FROM charng WHERE ".$column." = N'".$val."'" ;
		$res= $this->db->query($Query)->result();
	    return $res;
	  }
      */

}

?>