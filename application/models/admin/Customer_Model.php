<?php
class Customer_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select($Search = null, $itemStt= null , $itemEnd= null){
		
		$Where = $this->Conditions($Search);

		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, customer.*, ";  
		$Query .= "status.status_code as statusid, status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color, ";
		$Query .= "inhabited.address ";
		$Query .= "from customer ";
		$Query .= "INNER JOIN status ON customer.status = status.id ";
		$Query .= "LEFT JOIN inhabited ON customer.addr = inhabited.inhabited_code ";
		$Query .= "WHERE customer.type = N'person' ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "select COUNT(*) as allitems from customer INNER JOIN status ON customer.status = status.id ";
		$Query .= "WHERE customer.type = N'person' ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
					case 'status':
                        if($item != null){
                            $string = "( status.id = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "customer.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " AND ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	  }

	  public function selectOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where customer.customer_code like N'%".$id."%'";
		}
		
		$Query = "select customer.*  from customer  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function getIhbByCode($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where inhabited.inhabited_code like N'%".$id."%'";
		}
		$Query = "select inhabited.*  from inhabited  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectOneDetail($id = null){
		$where = null;
		if(!empty($id)){
			$where = "and customer.customer_code = N'".$id."'";
		}
		
		$Query = "select customer.*, ";
		//$Query .= " inhabited.address, tumbon.district_name,amphurs.amphur_name,provinces.province_name, inhabited.zip_code, ";
		$Query .= " status.color,status.background_color, status.label ";
		$Query .= " from customer  ";
		$Query .= " LEFT JOIN status ON customer.status = status.id  ";
		//$Query .= " LEFT JOIN inhabited ON customer.addr = inhabited.inhabited_code ";
		//$Query .= " LEFT JOIN tumbon ON inhabited.district_id = tumbon.id ";
		//$Query .= " LEFT JOIN amphurs ON inhabited.amphurs_id = amphurs.id  ";
		//$Query .= " LEFT JOIN provinces ON inhabited.province_id = provinces.id  ";
		
		$Query .= " where status.stautus_category = N'210601'  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectTemp($id = null){
		$where = null;
		if(!empty($id)){
			$where = "WHERE contract.customer_code = N'".$id."'";
		}
		
		$Query = "SELECT contract.*,status.color,status.background_color, status.label,product_set.product_name 
		FROM contract 
		LEFT JOIN customer ON contract.customer_code = customer.customer_code 
		LEFT JOIN product_set  ON contract.product_id = product_set.product_id
		LEFT JOIN status ON contract.status = status.id ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  public function amphurs(){
		$Query = "select * from amphurs order by amphur_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  public function district(){
		$Query = "select * from tumbon as tb order by district_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }


	  public function amphoe($postData=array()){
		$response = array();
		if(isset($postData['province_cod']) ){
			$Query = "select * from amphurs where provinces_id = ".$postData['province_cod']." and amphur_name not like '%*%' order by amphur_name";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
	  }

	  public function districs($DataAmphure=array()){
		$response = array();
	   if(isset($DataAmphure['amphue_cod']) ){
		   $Query = "select * from tumbon where amphurs_id = ".$DataAmphure['amphue_cod']." and district_name not like '%*%' order by district_name";
		   $Res= $this->db->query($Query);
		   $response = $Res->result();
	   }
	   return $response;
	  }

	  public function zipcodes($DataAmphure=array()){
		$response = array();
		if(isset($DataAmphure['district']) ){
			$Query = "select * from zipcode where provinces_id = ".$DataAmphure['province']." and amphurs_id = ".$DataAmphure['amphue']." and districts_id = ".$DataAmphure['district']." ";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
      }

	  public function CustomerByIdcard($Data=array()){
		$response = array();
		if(isset($Data['idcard']) ){
			$Query = "select * from customer where idcard = '".$Data['idcard']."' ";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
      }
	 

	  public function insert($data) { 
        if ($this->db->insert("customer", $data)) { 
            return true; 
        }
      } 
	  public function insertInhabitd($data) { 
        if ($this->db->insert("inhabited", $data)) { 
            return true; 
        }
      } 

	  public function getInhabited($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where ihb.customer_code = N'".$id."'";
		}
		
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function getInhabitedByCode($Data=array()){
		$where = null;
		if(isset($Data['inhabited_code']) ){
			$where = "where ihb.inhabited_code = N'".$Data['inhabited_code']."'";
		}
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getInhabitedByCodes($Data=array()){
		$where = null;
		if(isset($Data['customer_code']) ){
			$where = "where (ihb.category like'%ที่อยู่ตามบัตรประชาชน%' or  ihb.category like'%ที่อยู่ปัจจุบัน%') and  ihb.customer_code = N'".$Data['customer_code']."'";
		}
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
   
      public function update($data,$customer_code) { 
      	
         $this->db->set($data); 
         $this->db->where("customer_code", $customer_code); 
         $this->db->update("customer", $data); 
      } 

	  public function detailAddress($id = null, $cate = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited.customer_code = N'".$id."' and inhabited.category = N'".$cate."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }


}

?>