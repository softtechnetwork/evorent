<?php
class ContractDoc_Model extends CI_Model {

	function __construct() { 
		parent::__construct(); 
	} 
	
	public function getResContractDoc(){
		$Query = "select contract_doc.*, ";
		$Query .= "contractType.label, ";
		$Query .= "useStatus.label as useStatustitle, useStatus.color as useStatusColor, useStatus.background_color as useStatusBackground ";
		$Query .= "from contract_doc ";
		$Query .= "left join status contractType on contract_doc.contract_type = contractType.id ";
		$Query .= "left join status useStatus  on contract_doc.use_status = useStatus.id ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function statusTocombo($colums = null, $id = null){
		$Query = "select * from status where ".$colums." = N'".$id."'";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function getContractDocToGenCode($id = null){
		$where = null;
		if(!empty($id)){
			//$where = "where product_id like N'%".$id."%'";
		}
		$Query = "select TOP (1) * from contract_doc ".$where." order by id desc";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function getContractType($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where id = N'".$id."'";
		}
		$Query = "select * from status ".$where." ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	###########  Creat ##########
	public function ContractDocInsert($data) { 
		if ($this->db->insert("contract_doc", $data)) { 
			return true; 
		}
	} 
	public function ContractImgInsert($data) { 
		if ($this->db->insert("contract_img", $data)) { 
			return true; 
		}
	} 

	###########  Edit ##########
	public function GetToEdit($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where contract_id = N'".$id."'";
		}

		$Query = "SELECT *  FROM contract_doc ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function GetContractImgToEdit($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where contract_id = N'".$id."'";
		}

		$Query = "SELECT *  FROM contract_img ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function update($data,$id) { 
		$this->db->set($data); 
		$this->db->where("contract_id", $id); 
		$this->db->update("contract_doc", $data); 
	}

	###########  Delete ##########
	/*public function DeleteProduct($id) { 

		if(!empty($id)){
			$this->db->where('product_id', $id);
			$this->db->delete('product');

			$this->db->where('product_id', $id);
			$this->db->delete('installment_type');
			return true;
		}
	} 
	*/
	/*
	public function delProductSetInstallment($id) { 
		if(!empty($id)){
			$this->db->where('id', $id);
			$this->db->delete('installment_type');
			return true;
		}
	} 

	public function delProductSet_sub($id) { 
		if(!empty($id)){
			$this->db->where('id', $id);
			$this->db->delete('product_set_sub');
			return true;
		}
	} 
	*/

}

?>