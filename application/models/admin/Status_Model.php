<?php
class Status_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select(){

		$Query = "SELECT status.*, sc.label as cateLabel  FROM status ";
		$Query .= " INNER JOIN status_category sc ON status.stautus_category = sc.status_category_code";
		$Query .= " order by status.stautus_category, status.status_code ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selected($Search = null, $itemStt= null , $itemEnd= null){
		
		$Where = $this->Conditions($Search);

		$Query = "SELECT * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER (  ORDER BY status.stautus_category, status.status_code ) AS RowNum, status.*, sc.label as cateLabel  FROM status ";
		$Query .= " INNER JOIN status_category sc ON status.stautus_category = sc.status_category_code";
		$Query .= " ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "select COUNT(*) as allitems from status INNER JOIN status_category sc ON status.stautus_category = sc.status_category_code ";
		$Query .= " ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'label':
                        if(!empty($item)){
                            $string = "( status.label like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
					case 'statusCate':
                        if($item != null){
                            $string = "( sc.status_category_code = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " AND ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	  }

	  public function statusCate(){

		$Query = "select * from status_category order by status_category_code ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where id ='".$id."'";
		}
		
		$Query = "select * from status ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getStatus($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where stautus_category ='".$id."'";
		}
		
		$Query = "select * from status ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function requestStatus($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where stautus_category ='".$id."'";
		}
		$Query = "select * from status ".$where ."  order by id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function temp_select(){

		$Query = "select * from status where stautus_category = 'Temp' order by label desc";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }



	  public function insert($data) { 
		
        if ($this->db->insert("status", $data)) { 
            return true; 
        }
      } 

     /* public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } */
   
      public function update($data,$status_category_code) { 
      	
         $this->db->set($data); 
         $this->db->where("id", $status_category_code); 
         $this->db->update("status", $data); 
      } 


}

?>