<?php
class Dashboard_Model extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 
	  
	public function DashboardWebContractRes(){
		$Query = "select web_contact.*, status.status_code, status.label  from web_contact left join  status on web_contact.status = status.id ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

    public function DashboardInstallmentRes(){

        $Query = "SELECT * FROM   ( ";
        $Query .= " SELECT ";
        $Query .= " installment.temp_code, installment.period, installment.extend_duedate, ";
            
        $Query .= " CASE   WHEN installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) > CONVERT(date,installment.extend_duedate)  ) THEN 10 ";
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,installment.extend_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  5 "; 
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,installment.extend_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  4 ";
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,installment.extend_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  3 ";
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,installment.extend_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  2 ";
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,installment.extend_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  1 ";
        $Query .= " WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  )THEN  0 ";
        $Query .= " ELSE 100 END AS overdue_code ";
                
        $Query .= " FROM installment ";
        $Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code ";
        //$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code  ";
        //$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
        //$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id ";
                
        $Query .= " ) AS RowConstrainedResult ";
        $Query .= " WHERE overdue_code in(N'0', N'1', N'2', N'3', N'4', N'5', N'10')  ORDER BY overdue_code";
	
        $Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
    }

    public function TableInstallmentRes(){

        $Query = " SELECT * FROM   ( 
            SELECT top 7 ROW_NUMBER() OVER ( ORDER BY installment.contract_code ) AS RowNum,  installment.*, 

            CASE  
            WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) 
            THEN 'เกินกำหนดชำระมาแล้ว '+ CAST( DATEDIFF(DAY,  CONVERT(date,dateadd(year,-543,installment.extend_duedate)),   CONVERT(date,GETDATE()) )AS nvarchar)+' วัน'
            WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) 
            THEN  N'ครบกำหนดชำระแล้ว' 
            ELSE '' END AS overdue,   

            CASE  
            WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) THEN 10 
            WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate) ) THEN  0 
            ELSE 111  
            END AS overdue_code,  
            c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name,
            sms.cdate as sms_cdate, CASE WHEN  ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) ) THEN  1 ELSE 0 END AS sendsms 

            
            FROM contract_installment installment  INNER JOIN contract t ON installment.contract_code = t.contract_code 
            INNER JOIN customer c ON installment.customer_code = c.customer_code 
            LEFT JOIN product  ON t.product_id = product.product_id 
            LEFT JOIN sms_history sms ON t.contract_code = sms.temp_code and installment.period = sms.period and ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) )

            WHERE  
            t.status not in (10,11) and  
            installment.status_code = 0 
            and (     
                CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate)) 
                or  
                CONVERT(date,dateadd(year,+543,GETDATE()))  between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)
            ) 
        ) AS RowConstrainedResult 
        ORDER BY overdue_code, overdue desc ";

        $Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
    }

	public function TableInstallmentResPie($id){
		$normal_duedate = null;
		if(!empty($id)){
			$normal_duedate = "where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$id."' and CONVERT(date,contract_installment.installment_date) <= CONVERT(date,contract_installment.extend_duedate) ";
		}

		$over_duedate = null;
		if(!empty($id)){
			$over_duedate = "where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$id."' and CONVERT(date,contract_installment.installment_date) > CONVERT(date,contract_installment.extend_duedate) ";
		}

		$over_due = null;
		if(!empty($id)){
			$over_due = "where contract_installment.status_code = 0 and contract_installment.contract_code = N'".$id."' and ( CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,contract_installment.extend_duedate)) )  ";
		}

		$normal_due = null;
		if(!empty($id)){
			$normal_due = "where contract_installment.status_code = 0 and contract_installment.contract_code = N'".$id."' and (CONVERT(date,GETDATE()) <= CONVERT(date,dateadd(year,-543,contract_installment.extend_duedate)) ) ";
		}

		$Query = "select ";
		$Query .= " ( select COUNT(*) from contract_installment ".$normal_duedate." ) AS normal_duedate, "; 
		$Query .= " ( select COUNT(*) from contract_installment ".$over_duedate." ) AS over_duedate, "; 
		$Query .= " ( select COUNT(*) from contract_installment where contract_installment.contract_code = N'".$id."' ) AS count_period, "; 
		$Query .= " ( select COUNT(*) from contract_installment where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$id."' ) AS count_installment, "; 
		$Query .= " ( select COUNT(*) from contract_installment ".$over_due."  ) AS over_due, ";
		$Query .= " ( select COUNT(*) from contract_installment ".$normal_due."  ) AS normal_due ";
	
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }



}

?>