<?php
class Temp_Model extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 


	public function getToList($Search = null, $itemStt= null , $itemEnd= null){
		$Where = $this->Conditions($Search);
		
		$Query = "select * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum,temp.*, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as CusAddr, " ;
		$Query .= " product.product_name, product.product_cate as productCate, product.product_version as productVersion, product.product_detail, ";
        $Query .= " status.label, status.color, status.background_color " ;
        $Query .= " FROM temp ";
        $Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
        $Query .= " LEFT JOIN status ON temp.status = status.id ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "select COUNT(*) as allitems from temp INNER JOIN customer ON temp.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN status ON temp.status = status.id ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	}

	public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
					case 'status':
                        if($item != null){
                            $string = "( status.id = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "temp.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " Where ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	}

	public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function getTempToCreatGenCode($prefix){
		$where = null;
		if(!empty($prefix)){
			$where = "WHERE temp.temp_code LIKE '%".$prefix."%' ";
		}
		$Query = "select * from temp ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getTempToGenCode(){
		$Query = "select * from temp ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function CustomerToCombo(){
		$Query = "select customer.* ";
		$Query .= "from customer inner join status on  customer.status = status.id  ";
		$Query .= "where status.stautus_category = N'210601' and status.status_code = 0 ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  public function ProductCateToCombo(){
		$Query = "SELECT * FROM product_category WHERE cate_id is not null " ;
		$res= $this->db->query($Query)->result();
	    return $res;
	  }

	  public function GetProducts($productCateId = null){
		$where = null;
		if(!empty($productCateId)){
			$where = "WHERE product.product_cate LIKE '%".$productCateId."%' ";
		}
		$Query = "SELECT product.product_id,product.product_name, brand.brand_id, brand.brand_name ,product.product_version ";
		$Query .= "FROM product " ;
		$Query .= "INNER JOIN product_sub ON product.product_id = product_sub.product_id " ;
		$Query .= "LEFT JOIN brand ON product.product_brand = brand.brand_id" ;
		$Query .= " ".$where ;
		$Query .= "GROUP BY product.product_id,product.product_name,brand.brand_id, brand.brand_name ,product.product_version " ;
		$res = $this->db->query($Query)->result();
	    return $res;
	  }

	  public function GetSubByProduct($product_id = null){
		$where = null;
		if(!empty($product_id)){
			$where = "WHERE product_sub.product_id LIKE '%".$product_id."%' ";
		}
		$Query = "SELECT product_sub.* FROM product_sub ".$where ;
		$res = $this->db->query($Query)->result();
	    return $res;
	  }

	  public function GetInstallmentTypeByProductID($product_id = null){
		$where = null;
		if(!empty($product_id)){
			$where = "WHERE installment_type.product_id LIKE '%".$product_id."%' ";
		}
		$Query = "SELECT installment_type.* FROM installment_type ".$where ;
		$res = $this->db->query($Query)->result();
	    return $res;
	  }

	  public function selectOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = "WHERE temp.temp_code ='".$id."'";
		}

		$Query = "SELECT temp.*,";
		$Query .= "customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as customerAddr,";
		$Query .= "product.product_name, product.product_cate, product.product_version, product.product_detail,product_category.cate_name, brand.brand_name,";
		$Query .= "status.label, status.color, status.background_color ";
		$Query .= " FROM temp ";
		$Query .= " LEFT JOIN customer ON temp.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
		$Query .= " LEFT JOIN brand  ON temp.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN product_category ON temp.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON temp.status = status.id ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  public function selectOneDeatil($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where temp_code ='".$id."'";
		}
		$Query = "select temp.* ";
		$Query .= ", cus.office_name, cus.firstname, cus.lastname, cus.addr as address, temp.addr as installationlocation, cus.idcard , cus.birthday";
		$Query .= ", cus.tel, cus.email, cus.sex , cus.phone, cus.career_text, cus.monthly_income, cus.type";
		//$Query .= ", inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv ";
		$Query .= ", product.product_name, product.product_cate, product.product_version, product.product_detail,product_category.cate_name";
		$Query .= ", status.color,status.background_color, status.label, brand.brand_name ";

		$Query .= " FROM temp ";
		$Query .= " LEFT JOIN customer cus  ON temp.customer_code = cus.customer_code";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
		$Query .= " LEFT JOIN brand ON temp.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN product_category ON temp.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON temp.status = status.id  ";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function SubByProductId($code = null, $id = null){
		$where = null;
		if(!empty($id)){
			$where = "where serial.contract_code like N'%".$code."%' AND product_sub.product_id like N'%".$id."%'";
		}
		$Query = "select product_sub.*, serial.serial_number ";
		$Query .= " FROM product_sub ";
		$Query .= " LEFT JOIN product_serialnumber serial ON product_sub.product_sub_id = serial.product_sub_id  ";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function detailAddress($id = null, $cate = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited.customer_code = N'".$id."' and inhabited.category = N'".$cate."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function addrProductInstall($id = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited.inhabited_code ='".$id."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

































      
	  

	  public function getInhabited($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where ihb.customer_code = N'".$id."'";
			//$where = "where ihb.inhabited_code = N'".$id."'";
		}
		
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function getInhabitedEdit($id = null){
		$where = null;
		if(!empty($id)){
			//$where = "where ihb.customer_code = N'".$id."'";
			$where = "where ihb.inhabited_code = N'".$id."'";
		}
		
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectSupporter($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where temp_code ='".$id."'";
		}
		$Query = "SELECT TOP 1 supporter.* ";
		$Query .= " ,provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " FROM supporter ";
		$Query .= " LEFT JOIN provinces ON supporter.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON supporter.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON supporter.district_id = tumbon.id ".$where;
        $Query .= " ORDER BY id DESC ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectPremise($id = null, $mode=null){
		
		$where = null;
		if(!empty($id)){
			$where = "where premise.ref_code = N'".$id."' and premise.mode = N'".$mode."' ";
		}
		
		$Query = "select premise.*  from premise  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }



	  public function detailInstallationLocation($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where inhabited_code ='".$id."'";
		}
		$Query = "select inhabited.*, p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getInstOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = " and intt.temp_code = N'".$id."'";
		}
		$Query = "select intt.*, status.label as status_label, status.color, status.background_color FROM installment intt";
		$Query .= " LEFT JOIN status ON intt.status = status.status_code ";
		$Query .= " WHERE status.stautus_category = N'Installment'".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  public function customer_select(){
		$Query = "select customer.* from customer inner join status on  customer.status = status.id where status.stautus_category=N'Customer' and status.status_code =0";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function product_set(){
		/*
		$Query = "select sets.id, sets.product_install_id, sets.masterproduct_id, sets.amount,sets.active_date,sets.expire_date" ;
        $Query .= " , inst.price_a,inst.price_b,inst.price_c,inst.price_d, inst.picode, inst.piname, inst.piabbrv ,inst.picode_ref_arm, inst.brand, inst.product_category_id" ;
        $Query .= " , mast.mpcode,mast.mpcode_ref_arm,mast.mpname,mast.mpabbrv,mast.brand,mast.product_category_id,mast.typeproduct_id,mast.product_parent_id,mast.mp_desc,mast.uom_id,mast.active,mast.tax_rate,mast.pic,mast.out_of_stock " ;
        //$Query .= " , mpserial.mp_serial_number " ;
		$Query .= " , phi.product_hirepurchase_code,phi.formula_id,phi.product_install_id,phi.term_of_payment_start,phi.term_of_payment_end,phi.help_pay_installments_status,phi.holdonpayment,phi.installment_amount_per_installment,phi.interest " ;
        $Query .= " FROM product_install_set sets " ;
		
		//$Query .= " LEFT JOIN masterproduct_serialnumber mpserial  ON sets.id = mpserial.masterproduct_id";
        $Query .= " INNER JOIN product_install inst  ON sets.product_install_id = inst.id";
        $Query .= " INNER JOIN masterproduct mast ON sets.masterproduct_id = mast.id";
        $Query .= " LEFT JOIN product_hirepurchase_interest phi  ON inst.id = phi.product_install_id";

		$res = $this->db->query($Query)->result();
		*/
		

		/*$Arr = array();
		$QueryProductSe = "select sets.masterproduct_id,  mast.product_parent_id FROM product_install_set sets " ;
        $QueryProductSe .= " INNER JOIN masterproduct mast ON sets.masterproduct_id = mast.id";
        $QueryProductSe .= " where mast.typeproduct_id = N'1'";

		$ResProductSet= $this->db->query($QueryProductSe)->result();
		$resArr = array();
		###### check serial number #########
		foreach($ResProductSet as $item){
			
			$getMainProduct = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$item->masterproduct_id."' and used_by is null";
			$ResMainProduct= $this->db->query($getMainProduct)->result();
			if($ResMainProduct != null){
				$array = array();
				
				$getSupProduct = "select id from masterproduct where  product_parent_id = N'".$item->masterproduct_id."'";
				$ResSupProduct= $this->db->query($getSupProduct)->result();
				if($ResSupProduct != null){
					$sub = array();
					$count = 0;
					foreach($ResSupProduct as $items){
						$getSerial = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$items->id."' and used_by is null";
						$ResSupProducterial = $this->db->query($getSerial)->result();
						
						if($ResSupProducterial == null){
							$count++;
						}else{
							$subarray = (object) array(
								'id' => $ResSupProducterial[0]->masterproduct_id, 
								'serial' => $ResSupProducterial[0]->mp_serial_number
							);
							array_push($sub, $subarray);
						}
					}

					if($count == 0){
						$r = array(
							'main' =>  array('id' => $ResMainProduct[0]->masterproduct_id, 'serial' => $ResMainProduct[0]->mp_serial_number),
							'sub'=> (object)$sub
						);
						array_push($Arr,(object) $r);
					}
				}
				else{
					$r = array(
						'main' =>  array('id' => $ResMainProduct[0]->masterproduct_id, 'serial' => $ResMainProduct[0]->mp_serial_number),
						'sub'=>null
					);
					array_push($Arr, (object)$r);
				}

			}
		}
		
		############ get result #############
		$res = array();
		$mpid = '';
		$c = 1;
		foreach($Arr as $item){
			
			$Query = "select sets.id, sets.product_install_id, sets.masterproduct_id, sets.amount,sets.active_date,sets.expire_date" ;
			$Query .= " , inst.price_a,inst.price_b,inst.price_c,inst.price_d, inst.picode, inst.piname, inst.piabbrv ,inst.picode_ref_arm, inst.brand, inst.product_category_id" ;
			$Query .= " , mast.mpcode,mast.mpcode_ref_arm,mast.mpname,mast.mpabbrv,mast.brand,mast.product_category_id,mast.typeproduct_id,mast.product_parent_id,mast.mp_desc,mast.uom_id,mast.active,mast.tax_rate,mast.pic,mast.out_of_stock " ;
			//$Query .= " , null as mp_serial_number" ;

			$Query .= " , CASE	WHEN mast.id = N'".$item->main['id']."'    THEN '".json_encode($item)."'  ELSE ''   END  as mp_serial_number";

			$Query .= " , phi.product_hirepurchase_code,phi.formula_id,phi.product_install_id,phi.term_of_payment_start,phi.term_of_payment_end,phi.help_pay_installments_status,phi.holdonpayment,phi.installment_amount_per_installment,phi.interest " ;
			$Query .= " FROM product_install_set sets " ;
			
			//$Query .= " LEFT JOIN masterproduct_serialnumber mpserial  ON sets.id = mpserial.masterproduct_id";
			$Query .= " INNER JOIN product_install inst  ON sets.product_install_id = inst.id";
			$Query .= " INNER JOIN masterproduct mast ON sets.masterproduct_id = mast.id";
			$Query .= " LEFT JOIN product_hirepurchase_interest phi  ON inst.id = phi.product_install_id";
			$Query .= " WHERE mast.id = N'".$item->main['id']."' ";

			array_push($res, $this->db->query($Query)->result()[0]);
		}*/


		$Query = "select inst.*" ;
		$Query .= " FROM product_install inst " ;
		$res= $this->db->query($Query)->result();

	    return $res;
	  }

	  public function getProductSerial($id = null){
		$resArr = array();
		$getMainProduct = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$id."' and used_by is null";
		$ResMainProduct= $this->db->query($getMainProduct)->result();
		if($ResMainProduct != null){
			$main = (object) array(
				'id' =>  $ResMainProduct[0]->id,
				'MainProduct' => $ResMainProduct[0]->masterproduct_id, 
				'Serial' => $ResMainProduct[0]->mp_serial_number
			);
			array_push($resArr,$main);
		}else{
			array_push($resArr,(object) array('MainProduct' => $id, 'Serial' => null));
		}
		
		
		$getSupProduct = "select * from masterproduct where  product_parent_id = N'".$id."'";
		$ResSupProduct= $this->db->query($getSupProduct)->result();
		if($ResSupProduct != null){
			$sub= [];
			foreach($ResSupProduct as $item){
				$getSerial = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$item->id."' and used_by is null";
				$ResSupProducterial = $this->db->query($getSerial)->result();
				
				if($ResSupProducterial != null){
					$subarray = (object) array(
						'id' =>  $ResSupProducterial[0]->id,
						'SupProduct' => $ResSupProducterial[0]->masterproduct_id, 
						'Serial' => $ResSupProducterial[0]->mp_serial_number
					);
					array_push($resArr , $subarray);
				}else{
					array_push($resArr,(object) array('SupProduct' => $item->id, 'Serial' => null));
				}
			}
		}
	    return $resArr;
	  }


	  public function insert($data) { 
		
        if ($this->db->insert("temp", $data)) { 
            return true; 
        }
      } 

	  public function insertSupporter($data) { 
		
        if ($this->db->insert("supporter", $data)) { 
            return true; 
        }
      } 

     /* public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } */
   
      public function update($data,$temp_code) { 
      	
         $this->db->set($data); 
         $this->db->where("temp_code", $temp_code); 
         $this->db->update("temp", $data); 
      }

	  public function updateSupporter($data,$temp_code) { 
      	
		$this->db->set($data); 
		$this->db->where("temp_code", $temp_code); 
		$this->db->update("supporter", $data); 
	 }

	  public function updateSerial($data,$id) { 
      	
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->update("masterproduct_serialnumber", $data); 
	 }
	 
	 public function getStatus($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where status.id ='".$id."'";
		}
		$Query = "select status.* FROM status ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function requestStatus($category = null, $code = null){
		$where = null;
		if(!empty($category)){
			$where = "where stautus_category ='".$category."' and status_code = N'".$code."'";
		}
		$Query = "select * from status ".$where ."  order by id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	public function getTemp($temp_code = null){
		$where = null;
		if(!empty($temp_code)){
			$where = "where temp.temp_code ='".$temp_code."'";
		}
		$Query = "select temp.* FROM temp ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getInstallment($temp_code = null){
		$where = null;
		if(!empty($temp_code)){
			$where = "where installment.temp_code ='".$temp_code."'";
		}
		$Query = "select installment.* FROM installment ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function insertInstallment($data) { 
		
        if ($this->db->insert("installment", $data)) { 
            return true; 
        }
    } 
	public function getService($temp_code = null){
		$where = null;
		if(!empty($temp_code)){
			$where = "where product_service.temp_code ='".$temp_code."'";
		}
		$Query = "select product_service.* FROM product_service ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function getIhbByCus($Data=array()){
		$where = null;
		if(isset($Data['customer_code']) ){
			$where = "where ihb.customer_code = N'".$Data['customer_code']."'";
		}
		$Query = "select ihb.*, ";
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";  
		$Query .= " from inhabited ihb";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getSparesByInstallId($id = null){
		$where = null;
		if(isset($id) ){
			$where = " where pis.product_install_id = N'".$id."' ";
		}
		$Query = "select pis.*"; 
		$Query .= " ,mp.mpname";
		$Query .= " from product_install_set pis ";
		$Query .= " inner join masterproduct mp on pis.masterproduct_id =  mp.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  
	  // Insert Serial //
	  public function CreateSerial($data) { 
        if ($this->db->insert("product_serialnumber", $data)) { 
            return true; 
        }
      } 

	  public function CharngToCombo($column = null, $val = null){
		$Query = "SELECT * FROM charng WHERE ".$column." = N'".$val."'" ;
		$res= $this->db->query($Query)->result();
	    return $res;
	  }
	  public function CharngToPDF($id = null){
		$Query = "SELECT charng.* ";
		$Query .= ", agent.name as agentName, agent.sname as agentSname, p.province_name, a.amphur_name, t.district_name ";
		$Query .= ", op.province_name as officeProvince, oa.amphur_name as officeAmphurs, ot.district_name as officeDistrict ";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN agent ON charng.agent_code = agent.agent_code ";
		
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ";

		$Query .= " LEFT JOIN provinces op ON charng.office_province = op.id ";
		$Query .= " LEFT JOIN amphurs oa ON charng.office_amphurs = oa.id ";
		$Query .= " LEFT JOIN tumbon ot ON charng.office_district = ot.id ";


		$Query .= " WHERE charng_code like '%".$id."%'" ; 
		$res= $this->db->query($Query)->result();
	    return $res;
	  }

}

?>