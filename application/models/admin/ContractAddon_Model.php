<?php
class ContractAddon_Model extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 

	public function WhereConditions($whereArr){
		$Where = " ";
		if(count($whereArr) > 0){
			$Where .= "WHERE ";
			foreach($whereArr as $key => $item){
				$Where .= ($key == 0)? $item : "AND ".$item;
			}
		}
		return $Where;
	}
	####  Addon contract #####
    public function GetContract($contract_code = null){
		$where = null;
		if(!empty($contract_code)){
			$where = "WHERE contract.contract_code ='".$contract_code."'";
		}

		$Query = "SELECT contract.*,";
		$Query .= "customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as customerAddr,";
		$Query .= "product.product_name, product.product_cate,";
        //$Query .= "product.product_version";
        $Query .= "product.product_detail,product_category.cate_name, brand.brand_name,";
		$Query .= "status.label, status.color, status.background_color, ";
		$Query .= "cusType.label as customerType_name, ";
		$Query .= "conType.label as contractType_name ";
		$Query .= " FROM contract ";
		$Query .= " LEFT JOIN customer ON contract.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_set product  ON contract.product_id = product.product_id";
		$Query .= " LEFT JOIN brand  ON contract.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN product_category ON contract.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON contract.status = status.id ";
		$Query .= " LEFT JOIN status cusType ON contract.customer_type = cusType.id  ";
		$Query .= " LEFT JOIN status conType ON contract.contract_type = conType.id  ";
		$Query .= " ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function GetContractAddon($addon_code = null, $contract_code = null){
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($addon_code)){
			$whereArr[] = "ca.addon_code ='".$addon_code."'";
		}
		if(!empty($contract_code)){
			$whereArr[] = "ca.contract_code ='".$contract_code."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);

		$Query = "SELECT ca.*, product.product_name";
		$Query .= " FROM contract_addon ca ";
		//$Query .= " LEFT JOIN contract ON ca.contract_code = ca.contract_code";
		$Query .= " LEFT JOIN product_set product  ON ca.product_id = product.product_id";
		$Query .= $WhereConditions;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function GetProduct($contract_code = null, $product_id = null){
		
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($contract_code)){
			$whereArr[] = "contract.contract_code ='".$contract_code."'";
		}
		if(!empty($product_id)){
			$whereArr[] = "p.product_id ='".$product_id."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);
		
		if(isset($WhereConditions)){ 
			$WhereConditions .= ' where p.product_id in(
				select product.product_id  from product_set product 
				JOIN product_set_sub psub  ON product.product_id = psub.product_set_id group by  product.product_id)';
				
		}else{
			$WhereConditions .= ' and p.product_id in(
				select product.product_id  from product_set product 
				JOIN product_set_sub psub  ON product.product_id = psub.product_set_id group by  product.product_id)';
		}
		
		$Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type" ;
		$Query .= " FROM product_set p " ;
		$Query .= " LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id";
		$Query .= " LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id";
		
		$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
		$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id";
		$Query .= $WhereConditions;

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function getProductSub($product_id = null){
		
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($product_id)){
			$whereArr[] = "p.product_id ='".$product_id."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);
		
		$Query = "SELECT p.*, psub.id as sub_id, psub.active_serial, psub.count " ;

		$Query .= " FROM product_set p " ;
		$Query .= " JOIN product_set_sub psub  ON p.product_id = psub.product_set_id";
		$Query .= $WhereConditions;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function CreatAddon($data) { 
        if ($this->db->insert("contract_addon", $data)) { 
            return true; 
        }
    }
	public function getAddonToGenCode($prefix = null){
		$where = null;
		if(!empty($prefix)){
			$where = "WHERE addon.addon_code LIKE '%".$prefix."%' ";
		}
		$Query = "select * from contract_addon addon ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function deleteAddonContract($id = null, $addon_code = null, $contract_code = null) { 
		$dels = $this->db->where('id', $id)
		->where('addon_code', $addon_code)
		->where('contract_code', $contract_code)
		->delete('contract_addon');
		if ($dels) {
			return true;
		} 
	}

	####  Addon Product #####
	public function getResProductCate($contract_code = null){
		$Query = "SELECT pc.*" ;
		$Query .= " FROM product_category pc " ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function GetContractAddonProduct($addon_product = null, $contract_code = null){
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($addon_product)){
			$whereArr[] = "cp.id ='".$addon_product."'";
		}
		if(!empty($contract_code)){
			$whereArr[] = "cp.contract_code ='".$contract_code."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);
		
		$Query = "SELECT cp.*, pm.product_master_name, st.label as status_name ";
		$Query .= " FROM contract_addon_product cp ";
		//$Query .= " LEFT JOIN contract ON ca.contract_code = ca.contract_code";
		$Query .= " INNER JOIN product_master pm  ON cp.product_master_id = pm.product_master_id";
		$Query .= " INNER JOIN status st  ON cp.type = st.id";
		$Query .= $WhereConditions;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getResProductMaster($productCate = null){
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($productCate)){
			$whereArr[] = "pm.product_master_cate ='".$productCate."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);

		$Query = "SELECT pm.*";
		$Query .= " FROM product_master pm ";
		$Query .= $WhereConditions;

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function getStatus($statusCate = null){
		$WhereConditions = '';
		$whereArr = array();
		if(isset($statusCate)){
			$whereArr[] = "st.stautus_category ='".$statusCate."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);

		$Query = "SELECT st.*" ;
		$Query .= " FROM status st " ;
		$Query .= $WhereConditions;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function CreatAddonProduct($data) { 
        if ($this->db->insert("contract_addon_product", $data)) { 
            return true; 
        }
    }
	public function getAddonProductToGenCode($prefix = null){
		$where = null;
		if(!empty($prefix)){
			$where = "WHERE cp.product_addon_code LIKE '%".$prefix."%' ";
		}
		$Query = "select * from contract_addon_product cp ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function deleteAddonProduct($id = null, $product_addon_code = null) { 
		$dels = $this->db->where('id', $id)->where('product_addon_code', $product_addon_code)->delete('contract_addon_product');
		if ($dels) {
			return true;
		} 
	}

	####  Contract Installment #####
	
	public function insertInstallment($data) { 
        if ($this->db->insert("contract_installment", $data)) { 
            return true; 
        }
    } 
	public function GetInstallment($contract_code = null){
		$where = null;
		if(!empty($contract_code)){
			$where = "where installment.contract_code ='".$contract_code."'";
		}
		$Query = "select installment.* FROM contract_installment installment ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
	public function deleteInstallment($id = null, $contract_code = null) { 
		$delsSerial = $this->db->where('id', $id)
			->where('contract_code', $contract_code)
			->delete('contract_installment');
		return true; 
	}

	####  Serial number #####
	public function getSerialNumber($contract_code = null, $product_id = null, $product_sub_id = null) {
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($contract_code)){
			$whereArr[] = "cs.contract_code ='".$contract_code."'";
		}
		if(!empty($product_id)){
			$whereArr[] = "cs.product_id ='".$product_id."'";
		}
		
		if(!empty($product_sub_id)){
			$whereArr[] = "cs.product_sub_id ='".$product_sub_id."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);

		$Query = "SELECT cs.*" ;
		$Query .= " FROM contract_serialnumber cs " ;
		$Query .= $WhereConditions ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function CreateSerialNumber($data) { 
        if ($this->db->insert("contract_serialnumber", $data)) { 
            return true; 
        }
    }
	public function deleteSerialNumber($id = null, $contract_code = null, $product_id = null, $product_sub_id = null) { 
		if(isset($id)){ $this->db->where('id', $id); }
		if(isset($contract_code)){ $this->db->where('contract_code', $contract_code); }
		if(isset($product_id)){ $this->db->where('product_id', $product_id);}
		if(isset($product_sub_id)){ $this->db->where('product_sub_id', $product_sub_id);}
		$this->db->delete('contract_serialnumber');
		return true; 
	} 
	public function getMainSerialNumber($contract_code = null, $product_id = null) {
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($product_id)){
			$whereArr[] = "pss.product_set_id ='".$product_id."'";
		}
		
		$WhereConditions = $this->WhereConditions($whereArr);
		$Query = "";
		$Query .= " SELECT pss.* , pm.product_master_name,st.label, pss.product_set_id as product_id, pss.id as product_sub_id";

		$Query .= " ,( ";
		$Query .= " SELECT count (id) ";
			$Query .= " FROM contract_serialnumber as serialnumber ";
			$Query .= " where serialnumber.contract_code ='".$contract_code."' and serialnumber.product_id = pss.product_set_id  and serialnumber.product_sub_id = pss.id ";
		$Query .= " )as serial_count ";

		$Query .= " FROM product_set_sub as pss";
		$Query .= " left join product_master pm on pss.product_master_id = pm.product_master_id";
		$Query .= " left join status st on pss.type = st.id ";
		$Query .= $WhereConditions;
		$Res= $this->db->query($Query);
		$data = $Res->result();


		// สินค้า เพิ่มเติม
		$WhereConditions = '';
		$whereArr = null;
		if(!empty($contract_code)){
			$whereArr[] = "cap.contract_code ='".$contract_code."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);
		$Q = "";
		//$Q .= " SELECT cap.* , pm.product_master_name,st.label, cap.product_master_id as product_id, cap.product_addon_code as product_sub_id";
		
		$Q .= " SELECT ";
		$Q .= " cap.product_addon_code as id ";
		//$Q .= " ,cap.product_set_id ";
		$Q .= " ,cap.product_master_id as product_set_id ";
		$Q .= " ,cap.product_master_id ";
		$Q .= " ,cap.type ";
		$Q .= " ,cap.count ";
		$Q .= " ,cap.active_serial ";
		$Q .= " ,cap.create_date as cdate ";
		$Q .= " ,cap.update_date as udate ";
		$Q .= " ,pm.product_master_name,st.label, cap.product_master_id as product_id, cap.product_addon_code as product_sub_id ";
		
		$Q .= " ,( ";
		$Q .= " SELECT count (id) ";
		$Q .= " FROM contract_serialnumber as serialnumber ";
		$Q .= " where serialnumber.contract_code ='".$contract_code."' and serialnumber.product_id = cap.product_master_id  and serialnumber.product_sub_id = cap.product_addon_code ";
		$Q .= " )as serial_count ";
		
		$Q .= " FROM contract_addon_product as cap";
		$Q .= " left join product_master pm on cap.product_master_id = pm.product_master_id";
		$Q .= " left join status st on cap.type = st.id ";
		$Q .= $WhereConditions;
		$Result= $this->db->query($Q)->result();
		foreach($Result as $item){
			array_push($data, $item);
		}
		
		return $data;
	}
	public function getAddonContract($contract_code = null) {
		$WhereConditions = '';
		$whereArr = array();
		if(!empty($contract_code)){
			$whereArr[] = "ca.contract_code ='".$contract_code."'";
		}
		$WhereConditions = $this->WhereConditions($whereArr);

		$Query = "";
		$Query .= " SELECT ca.* ";
		$Query .= " FROM contract_addon as ca";
		$Query .= $WhereConditions;

		$Res= $this->db->query($Query);
		$data = $Res->result();

		foreach($data as $item){
			//print_r( $item);echo'<br>';
			$WhereConditions = '';
			$whereArr = null;
			if(!empty($contract_code)){
				$whereArr[] = "pss.product_set_id ='".$item->product_id."'";
			}
			$WhereConditions = $this->WhereConditions($whereArr);
			$Q = "";
			$Q .= " SELECT pss.* , pm.product_master_name,st.label, pss.product_set_id as product_id, pss.id as product_sub_id";
		
			$Q .= " ,( ";
			$Q .= " SELECT count (id) ";
			$Q .= " FROM contract_serialnumber as serialnumber ";
			$Q .= " where serialnumber.contract_code ='".$item->addon_code."' and serialnumber.product_id = pss.product_set_id  and serialnumber.product_sub_id = pss.id ";
			$Q .= " )as serial_count ";

			$Q .= " FROM product_set_sub as pss";
			$Q .= " left join product_master pm on pss.product_master_id = pm.product_master_id";
			$Q .= " left join status st on pss.type = st.id ";
			$Q .= $WhereConditions;
			$Result= $this->db->query($Q);
			$item->productRes =  $Result->result();

			// สินค้า เพิ่มเติม
			$WhereConditions = '';
			$whereArr = null;
			if(!empty($contract_code)){
				$whereArr[] = "cap.contract_code ='".$item->addon_code."'";
			}
			$WhereConditions = $this->WhereConditions($whereArr);
			$Q = "";
			//$Q .= " SELECT cap.* , pm.product_master_name,st.label, cap.product_master_id as product_id, cap.product_addon_code as product_sub_id";
			
			$Q .= " SELECT ";
			$Q .= " cap.product_addon_code as id ";
			//$Q .= " ,cap.product_set_id ";
			$Q .= " ,cap.product_master_id as product_set_id ";
			$Q .= " ,cap.product_master_id ";
			$Q .= " ,cap.type ";
			$Q .= " ,cap.count ";
			$Q .= " ,cap.active_serial ";
			$Q .= " ,cap.create_date as cdate ";
			$Q .= " ,cap.update_date as udate ";
			$Q .= " ,pm.product_master_name,st.label, cap.product_master_id as product_id, cap.product_addon_code as product_sub_id ";
		
			$Q .= " ,( ";
			$Q .= " SELECT count (id) ";
			$Q .= " FROM contract_serialnumber as serialnumber ";
			$Q .= " where serialnumber.contract_code ='".$item->addon_code."' and serialnumber.product_id = cap.product_master_id  and serialnumber.product_sub_id = cap.product_addon_code ";
			$Q .= " )as serial_count ";	
		
			$Q .= " FROM contract_addon_product as cap";
			$Q .= " left join product_master pm on cap.product_master_id = pm.product_master_id";
			$Q .= " left join status st on cap.type = st.id ";
			$Q .= $WhereConditions;
			$ResultP= $this->db->query($Q)->result();
			foreach($ResultP as $itemP){
				array_push($item->productRes, $itemP);
			}
		}

		return $data;
	}
	public function UpdateSerial($data = null, $id = null, $contract_id = null, $product_id = null, $product_sub_id = null, $no = null) { 
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->where("contract_code", $contract_id); 
		$this->db->where("product_id", $product_id); 
		$this->db->where("product_sub_id", $product_sub_id); 
		$this->db->where("no", $no); 
		$this->db->update("contract_serialnumber", $data);
		return true;
	}

	public function GetInstallmentAddonID($addon_code = null){
        $where = null;
        if(!empty($addon_code)){
            $where = "WHERE ci.contract_code = '".$addon_code."' ";
        }
        $Query = "SELECT ci.* ";
		$Query .= " ,st.label ";
		$Query .= "FROM contract_installment ci ";
		$Query .= "left JOIN status st ON ci.status = st.id ";
		$Query .= " ".$where ;
        $res = $this->db->query($Query)->result();
        return $res;
    }
	
	public function serialNodata($code = null, $id = null){
		$serialNo ="select top 1 max(serial.no) as no FROM contract_addon  contract ";
		$serialNo .="JOIN contract_serialnumber serial ON contract.addon_code = serial.contract_code and contract.product_id = serial.product_id  ";
		$serialNo .="WHERE contract.addon_code = N'".$code."'  and serial.product_id = N'".$id."' ";
		$serialNoRes= $this->db->query($serialNo);
		$serialNodata = $serialNoRes->result();
	    return $serialNodata;
	}

	public function serialData($addon_code = null, $product_id = null, $no = null){
		$serial = "select serial.*, pm.product_master_name as productSubName, psub.count  ";
		//$serial .= "FROM contract_addon contract   ";
		$serial .= "FROM contract_serialnumber serial   ";
		//$serial .= "LEFT JOIN contract_serialnumber serial ON contract.addon_code = serial.contract_code and contract.product_id = serial.product_id " ; 
		$serial .= "JOIN product_set_sub psub ON serial.product_sub_id = psub.id  ";
		$serial .= "LEFT JOIN product_master pm ON psub.product_master_id = pm.product_master_id ";
		//$serial .= "WHERE contract.addon_code = N'".$addon_code."'  and serial.product_id = N'".$product_id."' ";
		$serial .= "WHERE serial.contract_code = N'".$addon_code."'  and serial.product_id = N'".$product_id."' ";
		$serial .= "order by psub.type ";
		$serialRes= $this->db->query($serial);
		$serialData = $serialRes->result();

		$ser = "";
		$ser .= "select serial.* ";
		$ser .= ", pm.product_master_name as productSubName ";
		$ser .= "FROM contract_addon_product contract ";
		$ser .= "LEFT JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_addon_code = serial.product_sub_id ";
		$ser .= "LEFT JOIN product_master pm ON contract.product_master_id = pm.product_master_id ";
		$ser .= "WHERE contract.contract_code = N'".$addon_code."' ";
		$serRes= $this->db->query($ser);
		$serData = $serRes->result();
		foreach($serData as $item){
			array_push($serialData,$item );
		}

	    return $serialData;
	}

	public function CreateSerialNumberAjax($data) { 
		$res = array();
		try { 
			if($this->db->insert_batch("contract_serialnumber", $data)){ 
				$res['status'] = true;
			}else{
				$res['status'] = false;
				$res['message'] = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
			}
		} catch (Exception $e) {
			$res['status'] = false;
			$res['message'] = $e->getMessage();
		}
		return $res;
    }

	public function EditSerialNumberAjax($data) { 
		$res = array();
		try {
			if($this->db->update_batch('contract_serialnumber', $data, 'id')){ 
				$res['status'] = true;
			}else{
				$res['status'] = false;
				$res['message'] = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
			}
		} catch (Exception $e) {
			$res['status'] = false;
			$res['message'] = $e->getMessage();
		}
		return $res;
    }
	
	public function DeleteSerialNumberAjax($contract_code, $product_id, $product_sub_id) { 
		$res = array();
		try {
			$delsSerial = $this->db->where('contract_code', $contract_code)
			->where('product_id', $product_id)
			->where('product_sub_id', $product_sub_id)
			->delete('contract_serialnumber');

			if($delsSerial){ 
				$res['status'] = true;
			}else{
				$res['status'] = false;
				$res['message'] = 'เกิดข้อผิดพลาดในการลบข้อมูล';
			}
		} catch (Exception $e) {
			$res['status'] = false;
			$res['message'] = $e->getMessage();
		}
		return $res;
	}















}

?>