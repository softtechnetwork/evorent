<?php
class  SerialNumber_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 

		public function TempTocombo(){
			// temp code to serial
			$QueryStr = "SELECT contract_code FROM product_serialnumber  group by contract_code";
			$ResSerial= $this->db->query($QueryStr);
			$dataSerial = $ResSerial->result();
			$whereArr = [];
			$tempCode  = [];
			foreach($dataSerial as $key => $items){
				if(!empty($items->contract_code)){
					array_push($tempCode, "N'".$items->contract_code."'");
				}
			}
			if(!empty($tempCode)){
				$tempCode = 'temp.temp_code not in('.join(',',$tempCode).')';
				array_push($whereArr, $tempCode);
			} 

			$where = '';
			// group where
			foreach($whereArr as $key => $items){
				if(!empty($items)){
					if($key == 0){
						$where = 'WHERE '.$items;
					}else{
						$where = ' AND '.$items;
					}
				}
			}


			$Query = "SELECT temp.*, ";
			$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail ";
			$Query .= " FROM temp ";
			$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id ".$where ;
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function getToList($Search = null){
			$where = null;
			// temp code to serial
			$QueryStr = "SELECT contract_code FROM product_serialnumber  group by contract_code";
			$ResSerial= $this->db->query($QueryStr);
			$dataSerial = $ResSerial->result();
			$whereArr = [];
			$tempCode  = [];
			foreach($dataSerial as $key => $items){
				if(!empty($items->contract_code)){
					array_push($tempCode, "N'".$items->contract_code."'");
				}
			}
			if(!empty($tempCode)){
				$tempCode = 'temp.temp_code in('.join(',',$tempCode).')';
				array_push($whereArr, $tempCode);
			} 

			// search
			if(!empty($Search)){
				$searchs = json_decode($Search);
				foreach($searchs as $key =>$items){
					switch( $key){
						case'search_text':
							if(!empty($items)){
								array_push($whereArr," temp.temp_code LIKE '%".$items."%' ");
							}
						break;
					}
				}
			}
			// group where
			foreach($whereArr as $key => $items){
				if(!empty($items)){
					if($key == 0){
						$where = 'WHERE '.$items;
					}else{
						$where .= ' AND '.$items;
					}
				}
			}
			
			$Query = "SELECT temp.temp_code, temp.customer_code, temp.product_id, temp.techn_code, temp.remark, temp.status, " ;
			$Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr, " ;
			$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail, product_category.cate_name, ";
			$Query .= " status.label, status.color, status.background_color " ;
			$Query .= " FROM temp ";
			$Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code";
			$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
			$Query .= " LEFT JOIN product_category  ON temp.product_cate = product_category.cate_id";
			$Query .= " LEFT JOIN status ON temp.status = status.id ".$where;

			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
			
		}

		public function GetContractByTemp($id){
			$where = '';
			if(!empty($id)){
				$where = "WHERE temp.temp_code LIKE '%".$id."%'";
			}
			$Query = "SELECT temp.*, ";
			$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail ";
			$Query .= " FROM temp ";
			$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id ".$where ;
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}
		
		public function GetSerialByTemp($id){
			$where = '';
			if(!empty($id)){
				$where = "WHERE serials.contract_code LIKE '%".$id."%'";
			}
			$Query = "SELECT serials.*,product_sub.product_sub_name";
			$Query .= " FROM product_serialnumber serials ";
			$Query .= " LEFT JOIN product_sub  ON serials.product_sub_id = product_sub.product_sub_id ".$where ;
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function GetMaxSerialByTemp($id){
			$where = '';
			if(!empty($id)){
				$where = "WHERE serials.contract_code LIKE '%".$id."%'";
			}
			$Query = "SELECT MAX(serials.no) as no FROM product_serialnumber serials ".$where ;
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function InsertSerial($data) { 
			if ($this->db->insert("product_serialnumber", $data)) { 
				return true; 
			}
		} 

		public function updateSerial($data,$contract_id, $product_id, $product_sub_id) { 
			$this->db->set($data); 
			$this->db->where("contract_code", $contract_id); 
			$this->db->where("product_id", $product_id); 
			$this->db->where("product_sub_id", $product_sub_id); 
			$this->db->update("product_serialnumber", $data); 
		}



/*
		public function brandTocombo(){
			$Query = "select * from brand";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function getToGenCode(){
			$Query = "select * from product";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}
		
		public function InstallmentTypeToGenCode(){
			$Query = "select * from installment_type";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		
		public function InstallmentTypeInsert($data) { 
			if ($this->db->insert("installment_type", $data)) { 
				return true; 
			}
		} 

		

		public function GetToEdit($id = null){
			$where = null;
			if(!empty($id)){
				$where = "where p.product_id = N'".$id."'";
			}

			$Query = "SELECT p.*, pc.cate_name, b.brand_name" ;
			$Query .= " FROM product p " ;
			$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
			$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id ".$where;
			
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function GetInstallmentTypeToEdit($id = null){
			$where = null;
			if(!empty($id)){
				$where = "where ist.product_id = N'".$id."'";
			}

			$Query = "SELECT ist.*" ;
			$Query .= " FROM installment_type ist ".$where;
			
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function update($data,$id) { 
			$this->db->set($data); 
			$this->db->where("product_id", $id); 
			$this->db->update("product", $data); 
		}

		public function DeleteProduct($id) { 

			if(!empty($id)){
				$this->db->where('product_id', $id);
				$this->db->delete('product');

				$this->db->where('product_id', $id);
				$this->db->delete('installment_type');
				return true;
			}
		} 
        */

}

?>