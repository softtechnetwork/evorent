<?php
class Agent_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  /*public function select($type = null, $val = null , $itemPerPage = null, $itemStt = null, $itemEnd = null){
		$where = null;
		if(!empty($val)){
			$where = "AND customer.".$type." like '%".$val."%' ";
		}


		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, customer.*, ";  
		$Query .= "status.status_code as statusid, status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color, ";
		$Query .= "inhabited.address ";
		$Query .= "from customer ";
		$Query .= "INNER JOIN status ON customer.status = status.id ";
		$Query .= "LEFT JOIN inhabited ON customer.addr = inhabited.inhabited_code ";
		$Query .= "WHERE customer.type = N'person' ".$where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
      */

      public function jsonAgent($search = null){
		$where = null;
        $data = array();
        if(!empty($search)){
            $obj = json_decode($search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'agent_name':
                        if(!empty($item)){
                            $string = "( agent.name like N'%".$item."%' or agent.sname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "agent.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
        
        $Where = "";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " Where ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }

        
		$Query = "SELECT  agent.* ";  
		$Query .= ", p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM agent ";
		$Query .= " LEFT JOIN provinces p ON agent.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON agent.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON agent.district_id = t.id ".$Where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

      public function getTogenID($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where agent.agent_code like N'%".$id."%'";
		}
		
		$Query = "select agent.*  from agent  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function getCharngByIdcard($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where agent.idcard like N'%".$id."%'";
		}
		$Query = "select agent.*  from agent  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function insert($data) { 
        if ($this->db->insert("agent", $data)) { 
            return true; 
        }
      } 

      public function getToEdit($id){
		$where = null;
		if(!empty($id)){
			$where = "where agent.agent_code like N'%".$id."%'";
		}

		$Query = "SELECT  agent.* ";  
		$Query .= ", p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM agent ";
		$Query .= " LEFT JOIN provinces p ON agent.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON agent.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON agent.district_id = t.id ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function getToPDF($id){
		$where = null;
		if(!empty($id)){
			$where = "where agent.agent_code like N'%".$id."%'";
		}
		$Query = "SELECT  agent.* ";  
		$Query .= ", p.province_name, a.amphur_name, t.district_name";
		$Query .= ", op.province_name as officeProvince, oa.amphur_name as officeAmphurs, ot.district_name as officeDistrict ";
		$Query .= " FROM agent ";
		$Query .= " LEFT JOIN provinces p ON agent.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON agent.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON agent.district_id = t.id ";

		$Query .= " LEFT JOIN provinces op ON agent.office_province = op.id ";
		$Query .= " LEFT JOIN amphurs oa ON agent.office_amphurs = oa.id ";
		$Query .= " LEFT JOIN tumbon ot ON agent.office_district = ot.id ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
    public function amphoe(){
		$Query = "select * from amphurs where amphur_name not like '%*%' order by amphur_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
    public function districs(){
		$Query = "select * from tumbon where district_name not like '%*%' order by district_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

    public function update($data,$id) { 
        $this->db->set($data); 
        $this->db->where("agent_code", $id); 
        $this->db->update("agent", $data); 
     } 

/*
	  public function selectAllItems($type = null, $val = null){
		$where = null;
		if(!empty($val)){
			$where = "where customer.".$type." like '%".$val."%' ";
		}
		$Query = "select COUNT(*) as allitems from customer INNER JOIN status ON customer.status = status.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function selectPremise($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where premise.ref_code = N'".$id."'";
		}
		
		$Query = "select premise.*  from premise  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }


      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
   
      */


}

?>