<?php
class  ContractSerial_Model extends CI_Model {

	function __construct() { 
		parent::__construct(); 
	} 

	public function TempTocombo(){
		// temp code to serial
		$QueryStr = "SELECT contract_code FROM product_serialnumber  group by contract_code";
		$ResSerial= $this->db->query($QueryStr);
		$dataSerial = $ResSerial->result();
		$whereArr = [];
		$tempCode  = [];
		foreach($dataSerial as $key => $items){
			if(!empty($items->contract_code)){
				array_push($tempCode, "N'".$items->contract_code."'");
			}
		}
		if(!empty($tempCode)){
			$tempCode = 'temp.temp_code not in('.join(',',$tempCode).')';
			array_push($whereArr, $tempCode);
		} 

		$where = '';
		// group where
		foreach($whereArr as $key => $items){
			if(!empty($items)){
				if($key == 0){
					$where = 'WHERE '.$items;
				}else{
					$where = ' AND '.$items;
				}
			}
		}


		$Query = "SELECT temp.*, ";
		$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail ";
		$Query .= " FROM temp ";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id ".$where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function getToList($Search = null){
		$where = null;
		// temp code to serial
		$QueryStr = "SELECT contract_code FROM contract_serialnumber  group by contract_code";
		$ResSerial= $this->db->query($QueryStr);
		$dataSerial = $ResSerial->result();
		$whereArr = [];
		$tempCode  = [];
		foreach($dataSerial as $key => $items){
			if(!empty($items->contract_code)){
				array_push($tempCode, "N'".$items->contract_code."'");
			}
		}
		if(!empty($tempCode)){
			$tempCode = 'temp.contract_code in('.join(',',$tempCode).')';
			array_push($whereArr, $tempCode);
		} 

		// search
		if(!empty($Search)){
			$searchs = json_decode($Search);
			foreach($searchs as $key =>$items){
				switch( $key){
					case'search_text':
						if(!empty($items)){
							array_push($whereArr," temp.contract_code LIKE '%".$items."%' ");
						}
					break;
				}
			}
		}
		// group where
		foreach($whereArr as $key => $items){
			if(!empty($items)){
				if($key == 0){
					$where = 'WHERE '.$items;
				}else{
					$where .= ' AND '.$items;
				}
			}
		}
		
		$Query = "SELECT temp.contract_code, temp.customer_code, temp.product_id, temp.techn_code, temp.remark, temp.status, " ;
		$Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr, " ;
		$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail, product_category.cate_name, ";
		$Query .= " status.label, status.color, status.background_color " ;
		$Query .= " FROM contract temp ";
		$Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_set product  ON temp.product_id = product.product_id";
		$Query .= " LEFT JOIN product_category  ON temp.product_cate = product_category.cate_id";
		$Query .= " LEFT JOIN status ON temp.status = status.id ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
		
	}

	public function GetContractByTemp($id){
		$where = '';
		if(!empty($id)){
			$where = "WHERE temp.contract_code LIKE '%".$id."%'";
		}
		$Query = "SELECT temp.*, ";
		$Query .= " product.product_name, product.product_cate, product.product_version, product.product_detail ";
		$Query .= " FROM contract temp ";
		$Query .= " LEFT JOIN product_set product  ON temp.product_id = product.product_id ".$where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	
	public function GetSerialByTemp($id){
		$where = '';
		if(!empty($id)){
			$where = "WHERE serials.contract_code LIKE '%".$id."%'";
		}
		$Query = "SELECT serials.*,pm.product_master_name ";
		$Query .= " FROM contract_serialnumber serials ";
		$Query .= " LEFT JOIN product_set_sub sub  ON serials.product_sub_id = sub.id " ;
		$Query .= " LEFT JOIN product_master pm  ON sub.product_master_id = pm.product_master_id " ;
		$Query .= " ".$where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	
	public function GetLastProductSub($id, $product_id, $sub_id){
		$where = '';
		if(!empty($id)){
			$where = "WHERE contract.contract_code = N'".$id."' and sets.product_id = N'".$product_id."'  and psub.id not in(".$sub_id.")";
		}

		$Query = "SELECT contract.contract_code, contract.product_id  ";
		$Query .= ", psub.id as product_sub_id ";
		//$Query .= ", (select top 1 sr.serial_number from contract_serialnumber sr where sr.contract_code = contract.contract_code and sr.product_id = sets.product_id and sr.product_sub_id = psub.id) as serial_number  ";
		//$Query .= ", (select top 1 sr.no from contract_serialnumber sr			   where sr.contract_code = contract.contract_code and sr.product_id = sets.product_id and sr.product_sub_id = psub.id) as no  ";
		//$Query .= ", pm.product_master_name  ";
		$Query .= "FROM contract  ";
		$Query .= "JOIN product_set sets ON contract.product_id = sets.product_id ";
		$Query .= "JOIN product_set_sub psub ON sets.product_id = psub.product_set_id  ";
		//$Query .= "JOIN product_master pm ON psub.product_master_id = pm.product_master_id ";
		$Query .= " ".$where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	
	
	public function GetSerialToAdd($contract_code = null, $product_id = null){

		$QueryNo = " SELECT top 1 max(serial.no) as no FROM contract  " ;
		$QueryNo .= "JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_id = serial.product_id   " ;
		$QueryNo .= "JOIN product_set_sub psub ON serial.product_sub_id = psub.id    " ;
		$QueryNo .= "JOIN product_master pm ON psub.product_master_id = pm.product_master_id  " ;
		$QueryNo .= "WHERE contract.contract_code = N'".$contract_code."'  and serial.product_id = N'".$product_id."' " ;
		$resNo = $this->db->query($QueryNo)->result();
		
		$Query = " SELECT psub.*, pm.product_master_name " ;
		$Query .= " FROM contract  ";
		$Query .= " JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_id = serial.product_id  " ;
		$Query .= " JOIN product_set_sub psub ON serial.product_sub_id = psub.id  " ;
		$Query .= " JOIN product_master pm ON psub.product_master_id = pm.product_master_id " ;
		$Query .= " WHERE contract.contract_code = N'".$contract_code."' and serial.product_id = N'".$product_id."' and serial.no =  N'".$resNo[0]->no."'  " ;
		$res = $this->db->query($Query)->result();
		return $res;
	}

	public function GetSerialNoMax($contract_code = null, $product_id = null){

		$QueryNo = " SELECT top 1 max(serial.no) as no FROM contract  " ;
		$QueryNo .= "JOIN contract_serialnumber serial ON contract.contract_code = serial.contract_code and contract.product_id = serial.product_id   " ;
		$QueryNo .= "JOIN product_set_sub psub ON serial.product_sub_id = psub.id    " ;
		$QueryNo .= "JOIN product_master pm ON psub.product_master_id = pm.product_master_id  " ;
		$QueryNo .= "WHERE contract.contract_code = N'".$contract_code."'  and serial.product_id = N'".$product_id."' " ;
		$resNo = $this->db->query($QueryNo)->result();
		return $resNo;
	}
	
	public function GetSubByProduct($product_id = null){
		$where = null;
		if(!empty($product_id)){
			$where = "WHERE sub.product_set_id LIKE '%".$product_id."%' ";
		}
		$Query = " SELECT sub.* ,pm.product_master_name" ;
		$Query .= " FROM product_set_sub sub ";
		$Query .= " LEFT JOIN product_master pm  ON sub.product_master_id = pm.product_master_id " ;
		$Query .= " ".$where ;
		$res = $this->db->query($Query)->result();
		return $res;
	}

	public function updateSerial($data,$contract_id, $product_id, $product_sub_id, $no) { 
		$this->db->set($data); 
		$this->db->where("contract_code", $contract_id); 
		$this->db->where("product_id", $product_id); 
		$this->db->where("product_sub_id", $product_sub_id); 
		$this->db->where("no", $no); 
		$this->db->update("contract_serialnumber", $data); 
	}
	public function updateSerialNew($data,$id, $contract_id, $product_id, $product_sub_id, $no) { 
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->where("contract_code", $contract_id); 
		$this->db->where("product_id", $product_id); 
		$this->db->where("product_sub_id", $product_sub_id); 
		$this->db->where("no", $no); 
		$this->db->update("contract_serialnumber", $data); 
	}

	public function GetMaxSerialByTemp($id){
		$where = '';
		if(!empty($id)){
			$where = "WHERE serials.contract_code LIKE '%".$id."%'";
		}
		$Query = "SELECT MAX(serials.no) as no FROM contract_serialnumber serials ".$where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function InsertSerial($data) { 
		if ($this->db->insert("contract_serialnumber", $data)) { 
			return true; 
		}
	}

	
	public function GetSerialByContract($contract = null, $productid = null){
		
		$res = array();
		$WhereConditions = '';
		$LeftJoin = '';
		$Selects = '';
		$whereArr = array();
		if(!empty($contract)){
			$whereArr[] = "serials.contract_code LIKE '%".$contract."%'";
		}
		$WhereConditions = $this->WhereAddConditions($whereArr);

		//---  main product ----//
		$whereMaxNo = " and serials.no = ( SELECT max(serial.no) FROM contract_serialnumber serial WHERE serial.contract_code = N'".$contract."') ";
		$QueryMainP = "SELECT serials.* ";
		$QueryMainP .= ",pm.product_master_name ";
		$QueryMainP .= ", st.label ";
		$QueryMainP .= "FROM contract_serialnumber serials ";
		$QueryMainP .= "JOIN product_set_sub sub  ON serials.product_sub_id = sub.id  ";
		$QueryMainP .= "LEFT JOIN product_master pm  ON sub.product_master_id = pm.product_master_id  ";
		$QueryMainP .= "LEFT JOIN status st  ON sub.type = st.id   ";
		$QueryMainP .= (!empty($WhereConditions))?$WhereConditions.$whereMaxNo:'';
		$QueryMainPRes= $this->db->query($QueryMainP);
		$ResData = $QueryMainPRes->result();
		//array_push($res, $QueryMainPData);

		//---  addon product ----//
		$QueryaddonP = "SELECT serials.* ";
		$QueryaddonP .= ",pm.product_master_name ";
		$QueryaddonP .= ", st.label ";
		$QueryaddonP .= "FROM contract_serialnumber serials ";
		$QueryaddonP .= "JOIN contract_addon_product sub  ON serials.product_sub_id = sub.product_addon_code  ";
		$QueryaddonP .= "LEFT JOIN product_master pm  ON sub.product_master_id = pm.product_master_id   ";
		$QueryaddonP .= "LEFT JOIN status st ON sub.type = st.id   ";
		$QueryaddonP .= $WhereConditions;
		$QueryaddonPRes= $this->db->query($QueryaddonP);
		$QueryaddonPData = $QueryaddonPRes->result();
		foreach($QueryaddonPData as $item){
			array_push($ResData, $item);
		}
		
		return $ResData;
	}
	public function WhereAddConditions($whereArr){
		$Where = " ";
		if(count($whereArr) > 0){
			$Where .= "WHERE ";
			foreach($whereArr as $key => $item){
				$Where .= ($key == 0)? $item : "AND ".$item;
			}
		}
		return $Where;
	}
}

?>