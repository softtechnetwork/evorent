<?php
class Loan_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select($Search = null, $itemStt= null , $itemEnd= null){
		$Where = $this->Conditions($Search);

		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY temp.temp_code ) AS RowNum, temp.*, " ;
		$Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as address, " ;
		$Query .= " product.product_name ";

		$Query .= " FROM temp ";
		$Query .= " LEFT JOIN customer ON temp.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
		$Query .= " LEFT JOIN status ON temp.status = status.id ";
		$Query .= " WHERE  status.status_code = 0 ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

	  public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "SELECT COUNT(*) as allitems from temp INNER JOIN customer ON temp.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN status ON temp.status = status.id ";
		$Query .= " WHERE  status.status_code = 0 ".$Where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
					case 'status':
                        if($item != null){
                            $string = "( status.id = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "temp.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " AND ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	  }


      public function selectTemp(){

		$where = null;
		/*if(!empty($val)){
			$where = "where ".$type." like '%".$val."%' ";
		}*/

		$Query = "SELECT temp.temp_code, temp.customer_code, temp.product_set, temp.techn_code, temp.remark, temp.status, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr " ;
        $Query .= " FROM temp ";
        $Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function selectTempOne($id = null){

		$where = null;
		if(!empty($id)){
			$where = "where loan.temp_code is null and temp.temp_code like '%".$id."%' and status.status_code = N'0'";
		}

		$Query = "SELECT temp.*, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr, " ;
		$Query .= " inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv, ";
        $Query .= " status.label, status.color, status.background_color " ;
        $Query .= " FROM temp ";
        $Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_install_set sets  ON temp.product_set = sets.id";
		$Query .= " LEFT JOIN product_install inst  ON sets.product_install_id = inst.id";
        $Query .= " INNER JOIN status ON temp.status = status.id ";
		$Query .= " LEFT JOIN loan ON temp.temp_code = loan.temp_code ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function TempOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where temp.temp_code ='".$id."'";
		}
		
		$Query = "select * from temp ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function insertLoan($data) { 
		
        if ($this->db->insert("loan", $data)) { 
            return true; 
        }
      } 
	  public function insertInstallment($data) { 
		
        if ($this->db->insert("installment", $data)) { 
            return true; 
        }
      } 

   
	  public function getLoanOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where loan.temp_code ='".$id."'";
		}
		$Query = "select loan.* ";
		$Query .= ", cus.firstname, cus.lastname, cus.addr, cus.idcard , cus.province_id, cus.amphurs_id, cus.tumbon_id, cus.zip_code, cus.birthday, cus.tel, cus.email, cus.sex ";
		$Query .= ", inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv ";

		$Query .= " FROM loan ";
		$Query .= " INNER JOIN customer cus  ON loan.customer_code = cus.customer_code";
		$Query .= " LEFT JOIN product_install_set sets  ON loan.product_set = sets.id";
		$Query .= " LEFT JOIN product_install inst  ON sets.product_install_id = inst.id";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getTempOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where temp.temp_code ='".$id."'";
		}
		$Query = "select temp.* ";
		$Query .= ", cus.firstname, cus.lastname, cus.addr, cus.idcard, cus.birthday, cus.tel, cus.email, cus.sex ";
		$Query .= ", product.product_name,brand.brand_name  ";
		$Query .= ", status.color,status.background_color, status.label, brand.brand_name ";

		$Query .= " FROM temp ";
		$Query .= " LEFT JOIN customer cus  ON temp.customer_code = cus.customer_code";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
		$Query .= " LEFT JOIN brand  ON temp.product_brand = brand.brand_id";
		$Query .= " LEFT JOIN status ON temp.status = status.id  ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getInstOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = " and intt.temp_code = N'".$id."'";
		}
		$Query = "select intt.*, status.label as status_label, status.background_color, status.color FROM installment intt";
		$Query .= " LEFT JOIN status ON intt.status_code = status.status_code ";
		$Query .= " WHERE status.stautus_category = N'210603'".$where;
		$Query .= " Order By intt.period";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function updatInstallment($data, $temp_code, $period) { 
      	
         $this->db->set($data); 
         //$this->db->where("loan_code", $loan_code); 
         $this->db->where("temp_code", $temp_code); 
         $this->db->where("period", $period); 
         $this->db->update("installment", $data); 
      } 


	  public function update($data,$customer_code) { 
      	
		$this->db->set($data); 
		$this->db->where("customer_code", $customer_code); 
		$this->db->update("customer", $data); 
	 } 
/*
      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
	  */

	  public function getCustomer(){
		$Query = "SELECT * FROM customer";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  public function getTemp(){
		$Query = "SELECT * FROM temp";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

	  public function selectOverDue($postData  = null, $itemPerPage = null, $itemStt = null, $itemEnd = null){
        $data = array();

        if(!empty($postData)){
            foreach($postData as $key => $item){
                switch($key){
                    case 'payment_duedate':
                    case 'installment_date':
                        if(!empty($item)){
                            $Date = explode(',', $item);
                            $stDate = trim($Date[0]," ");
                            $endDate = trim($Date[1]," ");
                            $data[$key] = "installment.".$key." BETWEEN convert(date,N'".$stDate."') AND convert(date,N'".$endDate."') ";
                        }
                    break;
                    case 'name':
                        if(!empty($item)){
                            $data[$key] = "( c.firstname like '%".$item."%' or c.lastname like '%".$item."%' )";
                        }
                    break;
                    case 'overdue':
                        if( ( $item == '10') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) > CONVERT(date,installment.extend_duedate) ))";
                        }
                        if( ( $item == '0') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) )";
                        }
                        if( ( $item == '1') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '2') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '3') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '4') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '5') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  $data[$key] = "installment.".$key." = N'".$item."'"; }
                    break;
                }
            }
        }

        $W = null;
        $Where = " Where st.stautus_category = N'210603'";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $W .= " AND ".$items;
                    $Where .= $W;
                    $round++;
                }
            }
        }
        
		$Query = "select * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER ( ORDER BY installment.temp_code ) AS RowNum, installment.*, ";  
        
        $Query .= "CASE  "; 
        $Query .= "WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) ";
        $Query .= "THEN 'เกินกำหนดชำระมาแล้ว '+ CAST( DATEDIFF(DAY,  CONVERT(date,dateadd(year,-543,installment.extend_duedate)),   CONVERT(date,GETDATE()) )AS nvarchar)+' วัน' ";
	
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 5 วันจะครบกำหนกชำระ' ";     
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 4 วันจะครบกำหนกชำระ' ";    
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 3 วันจะครบกำหนกชำระ' ";  
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 2 วันจะครบกำหนกชำระ' ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) THEN  N'อีก 1 วันจะครบกำหนกชำระ' ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) THEN  N'ครบกำหนดชำระแล้ว' ";
        $Query .= "ELSE '' END AS overdue,   ";
	
        $Query .= "CASE   ";
        $Query .= "WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) THEN 10 ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  5  ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  4 ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  3   ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  2  "; 
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  1 ";
        $Query .= "WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  )THEN  0 ";
        $Query .= "ELSE 1000 END AS overdue_code,  ";


        $Query .= "c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name, brand.brand_name,  " ;
        $Query .= "st.status_code as statusCode, st.stautus_category, st.label, st.detail, st.color, st.background_color,  " ;
        $Query .= "sms.cdate as sms_cdate, CASE WHEN  ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) ) THEN  1 ELSE 0 END AS sendsms " ;
        
		$Query .= " FROM installment ";
		$Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code "; 
		$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id";
        $Query .= " LEFT JOIN status st  ON installment.status_code = st.status_code";
        $Query .= " LEFT JOIN sms_history sms ON t.temp_code = sms.temp_code and installment.period = sms.period and ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) )";
        
        $Query .= " ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= " WHERE RowNum >= N'".$itemStt."' AND RowNum <= N'".$itemEnd."'  ORDER BY RowNum";
        $Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
	  
	  public function selectOverDueAllItems($postData = null, $itemPerPage = null, $itemStt = null, $itemEnd = null){
		$data = array();
        if(!empty($postData)){
            foreach($postData as $key => $item){
                switch($key){
                    case 'payment_duedate':
                    case 'installment_date':
                        if(!empty($item)){
                            $Date = explode(',', $item);
                            $stDate = trim($Date[0]," ");
                            $endDate = trim($Date[1]," ");
                            $data[$key] = "installment.".$key." BETWEEN convert(date,N'".$stDate."') AND convert(date,N'".$endDate."') ";
                        }
                    break;
                    case 'name':
                        if(!empty($item)){
                            $data[$key] = "( c.firstname like '%".$item."%' or c.lastname like '%".$item."%' )";
                        }
                    break;
                    case 'overdue':
                        if( ( $item == '10') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) > CONVERT(date,installment.extend_duedate) ))";
                        }
                        if( ( $item == '0') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) )";
                        }
                        if( ( $item == '1') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '2') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '3') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '4') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                        if( ( $item == '5') ){
                            $data[$key] = "(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )";
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  $data[$key]= "installment.".$key." = N'".$item."'"; }
                    break;
                }
            }
        }

        $W = null;
        $Where = " Where st.stautus_category = N'210603'";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $W .= " AND ".$items;
                    $Where .= $W;
                    $round++;
                }
            }
        }

		$Query = "select COUNT(*) as allitems from installment ";
		$Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code "; 
		$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id";
        $Query .= " LEFT JOIN status st  ON installment.status_code = st.status_code".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data;
	  }

}

?>