<?php
class ProductSet_Model extends CI_Model {

	function __construct() { 
		parent::__construct(); 
	} 

	public function productcateTocombo(){
		$Query = "select pc.*";
		$Query .= ", (select COUNT(product_master_cate) from product_master where product_master_cate = pc.cate_id group by product_master_cate ) as coundOfProductMaster";
		$Query .= " from product_category pc ";
		$Query .= " where cate_id is not null";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function statusTocombo($colums = null, $id = null){
		$Query = "select * from status where ".$colums." = N'".$id."'";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function brandTocombo(){
		$Query = "select brand.*";
		$Query .= ", (select COUNT(product_master_brand) from product_master where product_master_brand = brand.brand_id group by product_master_brand ) as coundOfProductMaster";
		$Query .= " from brand";
		$Query .= " where brand_id is not null";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function brandTocomboByProductCate($cateID = null){
		$Query = "select brand.*";
		$Query .= ", (select COUNT(product_master_brand) from product_master where product_master_cate = N'".$cateID."' and product_master_brand = brand.brand_id group by product_master_brand ) as coundOfProductMaster";
		$Query .= " from brand";
		$Query .= " where brand_id is not null";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function getProductSetToGenCode($id = null){
		$where = null;
		if(!empty($id)){
			//$where = "where product_id like N'%".$id."%'";
		}
		$Query = "select TOP (1) * from product_set ".$where." order by id desc";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	###########  List ##########
	public function getToList($Search = null){

		$where = null;
		if(!empty($Search)){
			$searchs = json_decode($Search);
			$conditions = array();
			foreach($searchs as $key =>$items){
				switch( $key){
					case'search_text':
						if(!empty($items)){
							array_push($conditions," (p.product_id LIKE '%".$items."%' OR p.product_name LIKE '%".$items."%')");
						}
					break;
					default:
						if(!empty($items)){
							array_push($conditions, "p.".$key." LIKE '%".$items."%'");
						}
					break;
				}
			}

			foreach($conditions as $key => $items){
				if($key == 0){
					$where .= ' WHERE '.$items;
				}else{
					$where .= ' AND '.$items;
				}
			}
		}

		$Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type" ;
		$Query .= " FROM product_set p " ;
		$Query .= " LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id";
		$Query .= " LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id";
		
		$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
		$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	
	###########  Create ##########
	public function getMasterProductById($id){
		$Query = "select * from product_master where product_master_id = N'".$id."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function InstallmentTypeToGenCode(){
		$Query = "select TOP (1) * from installment_type order by id desc";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function ProductSetInsert($data) { 
		if ($this->db->insert("product_set", $data)) { 
			return true; 
		}
	} 
	public function SubProductSetInsert($data) { 
		if ($this->db->insert("product_set_sub", $data)) { 
			return true; 
		}
	} 

	public function InstallmentTypeInsert($data) { 
		if ($this->db->insert("installment_type", $data)) { 
			return true; 
		}
	}
	
	public function getResProductMaster($brand, $cate) { 
		$Query = "select * from product_master where product_master_cate = N'".$cate."' and product_master_brand = N'".$brand."' ";
		$Res = $this->db->query($Query);
		return $Res->result();
	}

	public function getResContractDoc($data) { 
		$Query = "SELECT cd.* ";
		$Query .= " FROM contract_doc cd " ;
		$Query .= " LEFT JOIN status st ON cd.use_status = st.id";
		$Query .= " WHERE cd.contract_type = N'".$data."' and st.status_code != 1 " ;
		$Res = $this->db->query($Query);
		return $Res->result();
	}


	###########  Edit ##########
	public function GetToEdit($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where p.product_id = N'".$id."'";
		}

		$Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type" ;
		$Query .= ", st_installation.label as installation_type" ;
		$Query .= " FROM product_set p " ;
		
		$Query .= " LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id";
		$Query .= " LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id";
		$Query .= " LEFT JOIN status st_installation ON p.product_installation_type = st_installation.id";

		$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
		$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function GetProduct_Sub($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where p.product_set_id = N'".$id."'";
		}

		$Query = "SELECT p.* " ;
		$Query .= ",pm.product_master_name as product_name, pm.product_master_brand as  product_brand" ;
		$Query .= ",product_type.label as product_type " ;
		$Query .= ",b.brand_name as brand_name " ;

		$Query .= " FROM product_set_sub p " ;

		$Query .= " LEFT JOIN product_master pm ON p.product_master_id = pm.product_master_id";
		$Query .= " LEFT JOIN status product_type ON p.type = product_type.id";
		//$Query .= " LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id";
		//$Query .= " LEFT JOIN status st_installation ON p.product_installation_type = st_installation.id";

		//$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
		$Query .= " LEFT JOIN brand b ON pm.product_master_brand = b.brand_id ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function AllProduct_SubTocombo(){
		$Query = "select * from product_master ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	

	public function GetInstallmentTypeToEdit($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where ist.product_id = N'".$id."'";
		}

		$Query = "SELECT ist.*" ;
		$Query .= " FROM installment_type ist ";
		$Query .= " ".$where;
		$Query .= " order by ist.amount_installment ";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

	public function update($data,$id) { 
		$this->db->set($data); 
		$this->db->where("product_id", $id); 
		$this->db->update("product_set", $data); 
	}

	public function update_installment($data,$id) { 
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->update("installment_type", $data); 
	}

	public function update_Product_Sub($data,$id) { 
		$this->db->set($data); 
		$this->db->where("id", $id); 
		$this->db->update("product_set_sub", $data); 
	}

	###########  Delete ##########
	public function DeleteProductSet($id) { 

		if(!empty($id)){
			$this->db->where('product_id', $id);
			$this->db->delete('product_set');

			$this->db->where('product_set_id', $id);
			$this->db->delete('product_set_sub');

			$this->db->where('product_id', $id);
			$this->db->delete('installment_type');
			return true;
		}
	} 
	
	public function delProductSetInstallment($id) { 
		if(!empty($id)){
			$this->db->where('id', $id);
			$this->db->delete('installment_type');
			return true;
		}
	} 

	public function delProductSet_sub($id, $set_id) { 
		/*if(!empty($id)){
			$this->db->where('id', $id);
			$this->db->delete('product_set_sub');
			return true;
		}*/
		$this->db->where('id', $id);
		$this->db->delete('product_set_sub');

		/*$this->db->where('product_id', $set_id);
		$this->db->where('product_sub_id', $id);
		$this->db->delete('contract_serialnumber');*/
		return true;
	} 

}

?>