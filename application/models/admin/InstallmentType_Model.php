<?php
class InstallmentType_Model extends CI_Model {

		function __construct() { 
			parent::__construct(); 
		} 

		public function getToList($Search = null){

			$where = null;
			if(!empty($Search)){
				$where = " WHERE ist.product_id LIKE '%".$Search."%' ";
				/*$searchs = json_decode($Search);
				$conditions = array();
				foreach($searchs as $key =>$items){
					switch( $key){
						case'search_text':
							if(!empty($items)){
								array_push($conditions," (p.product_id LIKE '%".$items."%' OR p.product_name LIKE '%".$items."%')");
							}
						break;
						default:
							if(!empty($items)){
								array_push($conditions, "p.".$key." LIKE '%".$items."%'");
							}
						break;
					}
				}

				foreach($conditions as $key => $items){
					if($key == 0){
						$where .= ' WHERE '.$items;
					}else{
						$where .= ' AND '.$items;
					}
				}*/
			}

			$Query = "SELECT ist.*, p.product_name" ;
			$Query .= " FROM installment_type  ist " ;
			$Query .= " LEFT JOIN product p ON ist.product_id = p.product_id".$where;

			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function GetToEdit($id = null){
			$where = null;
			if(!empty($id)){
				$where = " WHERE ist.installment_type_id LIKE '%".$id."%'";
			}

			$Query = "SELECT ist.*, p.product_name" ;
			$Query .= " FROM installment_type  ist " ;
			$Query .= " LEFT JOIN product p ON ist.product_id = p.product_id".$where;
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function update($data,$id) { 
			$this->db->set($data); 
			$this->db->where("installment_type_id", $id); 
			$this->db->update("installment_type", $data); 
		}


		public function GetProductToCreate(){
			$Query = "select * from product";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function InstallmentTypeToGenCode(){
			$Query = "select * from installment_type";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function InstallmentTypeInsert($data) { 
			if ($this->db->insert("installment_type", $data)) { 
				return true; 
			}
		} 

		public function DeleteInstallmentType($id) { 
			if(!empty($id)){
				$this->db->where('installment_type_id', $id);
				$this->db->delete('installment_type');
				return true;
			}
		} 
		

}

?>