<?php
class Report_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  

	  public function select($postData  = null, $itemPerPage = null, $itemStt = null, $itemEnd = null){
        $data = array();

        if(!empty($postData)){
            foreach($postData as $key => $item){
                switch($key){
                    case 'payment_duedate':
                    case 'installment_date':
                        if(!empty($item)){
                            $Date = explode(',', $item);
                            $stDate = trim($Date[0]," ");
                            $endDate = trim($Date[1]," ");
                            $data[$key] = "installment.".$key." BETWEEN convert(date,N'".$stDate."') AND convert(date,N'".$endDate."') ";
                        }
                    break;
                    case 'name':
                        if(!empty($item)){
                            $data[$key] = "( c.firstname like '%".$item."%' or c.lastname like '%".$item."%' )";
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  $data[$key] = "installment.".$key." = N'".$item."'"; }
                    break;
                }
            }
        }

        $W = null;
        $Where = " Where st.stautus_category = N'210603'";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $W .= " AND ".$items;
                    $Where .= $W;
                    $round++;
                }
            }
        }
    
		$Query = "select * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER ( ORDER BY installment.temp_code ) AS RowNum, installment.*, ";  
        $Query .= "c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name, brand.brand_name,  " ;
        $Query .= "st.status_code as statusCode, st.stautus_category, st.label, st.detail, st.color, st.background_color  " ;
		$Query .= " FROM installment ";
		$Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code "; 
		$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id";
        $Query .= " LEFT JOIN status st  ON installment.status_code = st.status_code".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= " WHERE RowNum >= N'".$itemStt."' AND RowNum <= N'".$itemEnd."'  ORDER BY RowNum";
        $Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

	  public function selectAllItems($postData = null, $itemPerPage = null, $itemStt = null, $itemEnd = null){
		$data = array();
        if(!empty($postData)){
            foreach($postData as $key => $item){
                switch($key){
                    case 'payment_duedate':
                    case 'installment_date':
                        if(!empty($item)){
                            $Date = explode(',', $item);
                            $stDate = trim($Date[0]," ");
                            $endDate = trim($Date[1]," ");
                            $data[$key] = "installment.".$key." BETWEEN convert(date,N'".$stDate."') AND convert(date,N'".$endDate."') ";
                        }
                    break;
                    case 'name':
                        if(!empty($item)){
                            $data[$key] = "( c.firstname like '%".$item."%' or c.lastname like '%".$item."%' )";
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  $data[$key]= "installment.".$key." = N'".$item."'"; }
                    break;
                }
            }
        }

        $W = null;
        $Where = " Where st.stautus_category = N'210603'";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $W .= " AND ".$items;
                    $Where .= $W;
                    $round++;
                }
            }
        }

		$Query = "select COUNT(*) as allitems from installment ";
		$Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code "; 
		$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id";
        $Query .= " LEFT JOIN status st  ON installment.status_code = st.status_code".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data;
	  }

      public function getToExport($postData  = null){
        $data = array();

        if(!empty($postData)){
            foreach($postData as $key => $item){
                switch($key){
                    case 'payment_duedate':
                    case 'installment_date':
                        if(!empty($item)){
                            $Date = explode(',', $item);
                            $stDate = trim($Date[0]," ");
                            $endDate = trim($Date[1]," ");
                            $data[$key] = "installment.".$key." BETWEEN convert(date,N'".$stDate."') AND convert(date,N'".$endDate."') ";
                        }
                    break;
                    case 'name':
                        if(!empty($item)){
                            $data[$key] = "( c.firstname like '%".$item."%' or c.lastname like '%".$item."%' )";
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  $data[$key] = "installment.".$key." = N'".$item."'"; }
                    break;
                }
            }
        }

        $W = null;
        $Where = " Where st.stautus_category = N'210603'";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $W .= " AND ".$items;
                    $Where .= $W;
                    $round++;
                }
            }
        }
    
		
		$Query = "SELECT installment.*, ";  
        $Query .= "c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name, brand.brand_name,  " ;
        $Query .= "st.status_code as statusCode, st.stautus_category, st.label, st.detail, st.color, st.background_color  " ;
		$Query .= " FROM installment ";
		$Query .= " INNER JOIN temp t ON installment.temp_code = t.temp_code "; 
		$Query .= " INNER JOIN customer c ON installment.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id";
        $Query .= " LEFT JOIN status st  ON installment.status_code = st.status_code".$Where;
		
		//$Query .= " WHERE RowNum >= N'".$itemStt."' AND RowNum <= N'".$itemEnd."'  ORDER BY RowNum";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

      /* function selectTempByCustomer($customer = null){
        $Where = null;
        if(!empty($customer)){
            $Where = " Where temp.customer_code = N'".$customer."'";
        }
		$Query = "SELECT * FROM temp".$Where ;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }*/

	  public function getCustomer(){
		$Query = "SELECT * FROM customer";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }
      public function getTemp(){
		$Query = "SELECT * FROM contract";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

    /*
      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
	  */
}

?>