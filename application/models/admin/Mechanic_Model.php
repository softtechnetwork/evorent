<?php
class Mechanic_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  /*public function select($type = null, $val = null , $itemPerPage = null, $itemStt = null, $itemEnd = null){
		$where = null;
		if(!empty($val)){
			$where = "AND customer.".$type." like '%".$val."%' ";
		}


		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, customer.*, ";  
		$Query .= "status.status_code as statusid, status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color, ";
		$Query .= "inhabited.address ";
		$Query .= "from customer ";
		$Query .= "INNER JOIN status ON customer.status = status.id ";
		$Query .= "LEFT JOIN inhabited ON customer.addr = inhabited.inhabited_code ";
		$Query .= "WHERE customer.type = N'person' ".$where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
      */

      public function jsonMechanic($search = null){
		$where = null;
        $data = array();
        if(!empty($search)){
            $obj = json_decode($search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'charng_name':
                        if(!empty($item)){
                            $string = "( charng.name like N'%".$item."%' or charng.sname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "charng.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
        
        $Where = "";
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " Where ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }

        
		$Query = "SELECT  charng.* ";  
		$Query .= ", p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ".$Where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

      public function getTogenID($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where charng.charng_code like N'%".$id."%'";
		}
		
		$Query = "select charng.*  from charng  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function getCharngByIdcard($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where charng.idcard like N'%".$id."%'";
		}
		$Query = "select charng.*  from charng  ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
      

      public function insert($data) { 
        if ($this->db->insert("charng", $data)) { 
            return true; 
        }
      } 

      public function getToEdit($id){
		$where = null;
		if(!empty($id)){
			$where = "where charng.charng_code like N'%".$id."%'";
		}

		$Query = "SELECT  charng.* ";  
		$Query .= ", agent.name as agentName, agent.sname as agentSname, p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN agent ON charng.agent_code = agent.agent_code ";
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getToPDF($id){
		$Query = "SELECT charng.* ";
		$Query .= ", agent.name as agentName, agent.sname as agentSname, p.province_name, a.amphur_name, t.district_name ";
		$Query .= ", op.province_name as officeProvince, oa.amphur_name as officeAmphurs, ot.district_name as officeDistrict ";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN agent ON charng.agent_code = agent.agent_code ";
		
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ";

		$Query .= " LEFT JOIN provinces op ON charng.office_province = op.id ";
		$Query .= " LEFT JOIN amphurs oa ON charng.office_amphurs = oa.id ";
		$Query .= " LEFT JOIN tumbon ot ON charng.office_district = ot.id ";


		$Query .= " WHERE charng_code like '%".$id."%'" ; 
		$res= $this->db->query($Query)->result();
	    return $res;
	  }

    public function amphoe(){
		$Query = "select * from amphurs where amphur_name not like '%*%' order by amphur_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
    public function districs(){
		$Query = "select * from tumbon where district_name not like '%*%' order by district_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

    public function update($data,$id) { 
        $this->db->set($data); 
        $this->db->where("charng_code", $id); 
        $this->db->update("charng", $data); 
     } 

	 public function getAgentToCombo(){
		$Query = "select agent.* from agent ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getToDetail($id){
		$where = null;
		if(!empty($id)){
			$where = "where charng.charng_code like N'%".$id."%'";
		}

		$Query = "SELECT  charng.* ";  
		$Query .= ", agent.name as agentName, agent.sname as agentSname, p.province_name, a.amphur_name, t.district_name";
		$Query .= ", op.province_name as op_province_name, oa.amphur_name as op_amphur_name, ot.district_name as op_district_name ";
		$Query .= " FROM charng ";
		$Query .= " LEFT JOIN agent ON charng.agent_code = agent.agent_code ";
		$Query .= " LEFT JOIN provinces p ON charng.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON charng.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON charng.district_id = t.id ";
		$Query .= " LEFT JOIN provinces op ON charng.office_province = p.id ";
		$Query .= " LEFT JOIN amphurs oa ON charng.office_amphurs = a.id ";
		$Query .= " LEFT JOIN tumbon ot ON charng.office_district = t.id ";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectContract($id){
		
		$where = null;
		if(!empty($id)){
			$where = "where temp.sale_code = N'".$id."' or temp.techn_code = N'".$id."'";
		}
		
		$Query = "select temp.*, customer.firstname, customer.lastname,  status.color,status.background_color, status.label,product.product_name ";
		$Query .= " from temp  ";
		$Query .= " LEFT JOIN customer ON temp.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN product  ON temp.product_id = product.product_id";
		//$Query .= " LEFT JOIN product_install_set sete ON temp.product_set = sete.id";
		//$Query .= " LEFT JOIN product_install inst ON sete.product_install_id = inst.id";
		$Query .= " LEFT JOIN status ON temp.status = status.id ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getToChart($id){
		$normal_duedate = null;
		if(!empty($id)){
			$normal_duedate = "where installment.status_code = 1 and installment.temp_code = N'".$id."' and CONVERT(date,installment.installment_date) <= CONVERT(date,installment.extend_duedate) ";
		}

		$over_duedate = null;
		if(!empty($id)){
			$over_duedate = "where installment.status_code = 1 and installment.temp_code = N'".$id."' and CONVERT(date,installment.installment_date) > CONVERT(date,installment.extend_duedate) ";
		}

		$over_due = null;
		if(!empty($id)){
			$over_due = "where installment.status_code = 0 and installment.temp_code = N'".$id."' and ( CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate)) )  ";
		}

		$normal_due = null;
		if(!empty($id)){
			$normal_due = "where installment.status_code = 0 and installment.temp_code = N'".$id."' and (CONVERT(date,GETDATE()) <= CONVERT(date,dateadd(year,-543,installment.extend_duedate)) ) ";
		}

		$Query = "select ";
		$Query .= " ( select COUNT(*) from installment ".$normal_duedate." ) AS normal_duedate, "; 
		$Query .= " ( select COUNT(*) from installment ".$over_duedate." ) AS over_duedate, "; 
		$Query .= " ( select COUNT(*) from installment where installment.temp_code = N'".$id."' ) AS count_period, "; 
		$Query .= " ( select COUNT(*) from installment where installment.status_code = 1 and installment.temp_code = N'".$id."' ) AS count_installment, "; 
		$Query .= " ( select COUNT(*) from installment ".$over_due."  ) AS over_due, ";
		$Query .= " ( select COUNT(*) from installment ".$normal_due."  ) AS normal_due ";
	
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

/*
	  public function selectAllItems($type = null, $val = null){
		$where = null;
		if(!empty($val)){
			$where = "where customer.".$type." like '%".$val."%' ";
		}
		$Query = "select COUNT(*) as allitems from customer INNER JOIN status ON customer.status = status.id ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }
      */


}

?>