<?php
class ProductCategory_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 

      public function insert($data) { 
        if ($this->db->insert("product_category", $data)) { 
            return true; 
        }
      }

      public function update($data,$id) { 
        $this->db->set($data); 
        $this->db->where("id", $id); 
        $this->db->update("product_category", $data); 
     } 

      public function getToGenCode(){
        $Query = "SELECT * FROM product_category";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function getToList($Search){
        $where = null;
		if(!empty($Search)){
			$where = "WHERE cate_name like '%".$Search."%' ";
		}

        $Query = "SELECT * FROM product_category ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function getToEdit($id){
        $where = null;
		if(!empty($id)){
			$where = "WHERE id = N'".$id."' ";
		}

        $Query = "SELECT * FROM product_category ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function delet($id){
		if(!empty($id)){
            $this->db->where('id', $id);
            $this->db->delete('product_category');
            return true;
		}else{
            return false;
        }
      }
}

?>