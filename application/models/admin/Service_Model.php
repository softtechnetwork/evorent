<?php
class Service_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select($type = null, $val = null, $itemPerPage = null, $itemStt = null, $itemEnd = null){
		$where = null;
		if(!empty($val)){
			$where = " WHERE ".$type." like '%".$val."%' ";
		}

		$Query = "select * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER ( ORDER BY t.temp_code ) AS RowNum, ps.*,  c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr ";
		$Query .= ", product.product_name, brand.brand_name  " ;
		$Query .= " FROM product_service ps ";
		$Query .= " INNER JOIN temp t ON ps.temp_code = t.temp_code "; 
		//$Query .= " INNER JOIN loan l ON ps.loan_code = l.loan_code ";
		$Query .= " LEFT JOIN customer c ON t.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id ";
		$Query .= " ".$where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

	  public function selectAllItems($type = null, $val = null){
		$where = null;
		if(!empty($val)){
			$where = "where ".$type." like '%".$val."%' ";
		}
		$Query = "select COUNT(*) as allitems from product_service ps ";
		$Query .= " INNER JOIN temp t ON ps.temp_code = t.temp_code "; 
		//$Query .= " INNER JOIN loan l ON ps.loan_code = l.loan_code ";
		$Query .= " LEFT JOIN customer c ON t.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function selectOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = " WHERE ps.service_code like '%".$id."%' ";
		}
			
		$Query = "SELECT ps.*,  ";
		$Query .= " c.customer_code,c.firstname, c.lastname, c.tel, c.email, c.idcard,c.addr as address, ";
		$Query .= " product.product_name, brand.brand_name, ";
		$Query .= " t.addr as installationlocation ,t.product_price,t.product_brand,t.product_count,t.product_version, t.contract_date,t.payment_due,t.payment_start_date,t.monthly_rent,t.down_payment,t.rental_period " ;
		
		$Query .= " FROM product_service ps ";
		$Query .= " INNER JOIN temp t ON ps.temp_code = t.temp_code "; 
		//$Query .= " INNER JOIN loan l ON ps.loan_code = l.loan_code ";
		$Query .= " LEFT JOIN customer c ON t.customer_code = c.customer_code ";
		$Query .= " LEFT JOIN product  ON t.product_id = product.product_id ";
		$Query .= " LEFT JOIN brand  ON t.product_brand = brand.brand_id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
		
		
	  }

	  public function serviceAddress($id = null){
		$where = null;
		if(!empty($id)){ $where = "where inhabited_code ='".$id."'"; }
		$Query = "select inhabited.* , p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function serviceInstallationLocation($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where inhabited_code ='".$id."'";
		}
		$Query = "select inhabited.*, p.province_name, a.amphur_name, t.district_name";
		$Query .= " FROM inhabited ";
		$Query .= " LEFT JOIN provinces p ON inhabited.province_id = p.id ";
		$Query .= " LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id ";
		$Query .= " LEFT JOIN tumbon t ON inhabited.district_id = t.id ";
		$Query .= " ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }


	  public function selectHistory($id = null){
		$where = null;
		if(!empty($id)){
			$where = " WHERE service_code = N'".$id."' and contract_code is null ";
		}
			
		$Query = "SELECT * FROM service_history  ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;		
	  }

      public function selectTemp(){

		$where = null;
		/*if(!empty($val)){
			$where = "where ".$type." like '%".$val."%' ";
		}*/

		$Query = "SELECT temp.temp_code, temp.customer_code, temp.product_set, temp.techn_code, temp.remark, temp.status, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr " ;
        $Query .= " FROM temp ";
        $Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function selectTempOne($id = null){

		$where = null;
		if(!empty($id)){
			//$where = "where loan.temp_code is null and temp.temp_code like '%".$id."%' and status.status_code = N'0'";
			$where = "where temp.temp_code like '%".$id."%' ";
		}

		$Query = "SELECT temp.*, " ;
        $Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr, " ;
		$Query .= " inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv, ";
        $Query .= " status.label, status.color, status.background_color " ;
        $Query .= " FROM temp ";
        $Query .= " INNER JOIN customer ON temp.customer_code = customer.customer_code";
		$Query .= " LEFT JOIN product_install_set sets  ON temp.product_set = sets.id";
		$Query .= " LEFT JOIN product_install inst  ON sets.product_install_id = inst.id";
        $Query .= " INNER JOIN status ON temp.status = status.id ";
		$Query .= " LEFT JOIN loan ON temp.temp_code = loan.temp_code ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function TempOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where temp.temp_code ='".$id."'";
		}
		
		$Query = "select * from temp ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function insertLoan($data) { 
		
        if ($this->db->insert("loan", $data)) { 
            return true; 
        }
      } 

	  public function insert($data) { 
		
        if ($this->db->insert("product_service", $data)) { 
            return true; 
        }
      } 
	  
	  public function insertServiceHistory($data){ 
		
        if ($this->db->insert("service_history", $data)) { 
            return true; 
        }
      } 

   
	  public function getLoanOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where loan.temp_code ='".$id."'";
		}
		$Query = "select loan.* ";
		$Query .= ", cus.firstname, cus.lastname, cus.addr, cus.idcard , cus.province_id, cus.amphurs_id, cus.tumbon_id, cus.zip_code, cus.birthday, cus.tel, cus.email, cus.sex ";
		$Query .= ", inst.picode, inst.picode_ref_arm, inst.piname, inst.piabbrv ";

		$Query .= " FROM loan ";
		$Query .= " INNER JOIN customer cus  ON loan.customer_code = cus.customer_code";
		$Query .= " LEFT JOIN product_install_set sets  ON loan.product_set = sets.id";
		$Query .= " LEFT JOIN product_install inst  ON sets.product_install_id = inst.id";
		$Query .= " ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getInstOne($id = null){
		$where = null;
		if(!empty($id)){
			$where = " and intt.temp_code = N'".$id."'";
		}
		$Query = "select intt.*, status.label as status_label FROM installment intt";
		$Query .= " LEFT JOIN status ON intt.status = status.status_code ";
		$Query .= " WHERE status.stautus_category = N'Installment'".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

      public function updatInstallment($data, $loan_code, $temp_code, $period) { 
      	
         $this->db->set($data); 
         $this->db->where("loan_code", $loan_code); 
         $this->db->where("temp_code", $temp_code); 
         $this->db->where("period", $period); 
         $this->db->update("installment", $data); 
      } 


	  public function update($data,$customer_code) { 
      	
		$this->db->set($data); 
		$this->db->where("customer_code", $customer_code); 
		$this->db->update("customer", $data); 
	 } 
/*
      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
	  */

}

?>