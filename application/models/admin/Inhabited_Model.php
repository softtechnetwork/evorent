<?php
class Inhabited_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select($Search = null, $itemStt= null , $itemEnd= null ){
		
		/*$where = null;
		if(!empty($val)){
			$where = " WHERE ".$type." like '%".$val."%' ";
		}*/


		$Where = $this->Conditions($Search);

			
		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, ihb.*, " ;
		$Query .= " customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, " ;
		$Query .= " provinces.province_name, amphurs.amphur_name, tumbon.district_name ";

		$Query .= " FROM inhabited ihb ";
		$Query .= " INNER JOIN customer ON ihb.customer_code = customer.customer_code ";
		$Query .= " LEFT JOIN provinces ON ihb.province_id = provinces.id";
        $Query .= " LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id";
        $Query .= " LEFT JOIN tumbon ON ihb.district_id = tumbon.id";
        $Query .= " ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

      public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);

		$Query = "select COUNT(*) as allitems from inhabited ihb INNER JOIN customer ON ihb.customer_code = customer.customer_code ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "customer.".$key." like N'%".$item."%'");
                         }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " Where ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	  }

      public function customer_select(){
		$Query = "select customer.* from customer ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

    public function province(){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
    public function amphoeAll(){
		$Query = "select * from amphurs where amphur_name not like '%*%' order by amphur_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
    public function districsAll(){
		$Query = "select * from tumbon where district_name not like '%*%' order by district_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

    public function provincs($postData=array()){
		$response = array();
		if(isset($postData['province_id']) ){
			$Query = "select * from provinces order by id";
			$Res= $this->db->query($Query);
			$response = $Res->result();
			//$response = $Query;
		}
		return $response;
	}
    public function amphoe($postData=array()){
		$response = array();
		if(isset($postData['province_cod']) ){
			$Query = "select * from amphurs where provinces_id = ".$postData['province_cod']." and amphur_name not like '%*%' order by amphur_name";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
	}
    public function districs($DataAmphure=array()){
		$response = array();
	   if(isset($DataAmphure['amphue_cod']) ){
		   $Query = "select * from tumbon where amphurs_id = ".$DataAmphure['amphue_cod']." and district_name not like '%*%' order by district_name";
		   $Res= $this->db->query($Query);
		   $response = $Res->result();
	   }
	   return $response;
	}
    public function zipcodes($DataAmphure=array()){
		$response = array();
		if(isset($DataAmphure['district']) ){
			$Query = "select * from zipcode where provinces_id = ".$DataAmphure['province']." and amphurs_id = ".$DataAmphure['amphue']." and districts_id = ".$DataAmphure['district']." ";
			$Res= $this->db->query($Query);
			$response = $Res->result();
		}
		return $response;
    }


    public function selectByInhabited_code($id = null){
		$where = null;
		if(!empty($id)){
			$where = " WHERE inhabited_code like '%".$id."%' ";
		}
			
		$Query = "select * FROM inhabited ".$where;

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	  }

	public function inhabitedInsert($data) { 
        if ($this->db->insert("inhabited", $data)) { 
            return true; 
        }
    } 


	public function update($data,$inhabited_code) {      	
		$this->db->set($data); 
		$this->db->where("inhabited_code", $inhabited_code); 
		$this->db->update("inhabited", $data); 
	} 
    /*
      public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
	  */

}

?>