<?php
class Brand_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 

      public function insert($data) {
        if ($this->db->insert("brand", $data)) { 
            return true; 
        }
      }

      public function update($data,$id) { 
        $this->db->set($data); 
        $this->db->where("brand_id", $id); 
        $this->db->update("brand", $data); 
     } 

      public function getToGenCode(){
        $Query = "SELECT * FROM brand";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function getToList($Search = null){
        $where = null;
		if(!empty($Search)){
			$where = "WHERE brand_name like '%".$Search."%' ";
		}

        $Query = "SELECT * FROM brand ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function getToEdit($id){
        $where = null;
		if(!empty($id)){
			$where = "WHERE brand_id = N'".$id."' ";
		}

        $Query = "SELECT * FROM brand ".$where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
      }

      public function delet($id){
		if(!empty($id)){
            $this->db->where('brand_id', $id);
            $this->db->delete('brand');
            return true;
		}else{
            return false;
        }
      }
}

?>