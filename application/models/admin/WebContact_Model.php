<?php
class WebContact_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select($Search = null, $itemStt= null , $itemEnd= null){
		
		$Where = $this->Conditions($Search);
		$Query = "SELECT * from   ( ";
		$Query .= "SELECT ROW_NUMBER() OVER (  ORDER BY web_contact.id ) AS RowNum, web_contact.*, ";
		$Query .= "status.status_code as statusid, status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color ";
        
        $Query .= " FROM web_contact ";
        $Query .= " LEFT JOIN status ON web_contact.status = status.id ";
		$Query .= " ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";

		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectAllItems($Search = null){
		$Where = $this->Conditions($Search);
		$Query = "select COUNT(*) as allitems from web_contact ";
		$Query .= " ".$Where;
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	  }

	  public function Conditions($Search = null){
        
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'name':
                        if(!empty($item)){
                            $string = "( name like N'%".$item."%' or sname like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if($item != null){
                            $string = "( ".$key." like N'%".$item."%' )";
                            array_push($data,  $string);
                        }
                    break;
                }
            }
		}
		
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " WHERE ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	  }

	  public function selectOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where id ='".$id."'";
		}
		
		$Query = "select * from web_contact ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function getStatus($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where stautus_category ='".$id."'";
		}
		
		$Query = "select * from status ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  
	  public function requestStatus($id = null){
		$where = null;
		if(!empty($id)){
			$where = "where stautus_category ='".$id."'";
		}
		$Query = "select * from status ".$where ."  order by id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  

     /* public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } */
   
      public function update($data,$id) { 
      	
         $this->db->set($data); 
         $this->db->where("id", $id); 
         $this->db->update("web_contact", $data); 
      } 


}

?>