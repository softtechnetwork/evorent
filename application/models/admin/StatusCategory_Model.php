<?php
class StatusCategory_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  public function select(){

		$Query = "select * from status_category";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }

	  public function selectOne($id = null){
		
		$where = null;
		if(!empty($id)){
			$where = "where status_category_code like N'%".$id."%'";
		}
		
		$Query = "select * from status_category ".$where;
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	  }
	  public function insert($data) { 
		
        if ($this->db->insert("status_category", $data)) { 
            return true; 
        }
      } 

      /*public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } */
   
      public function update($data,$status_category_code) { 
      	
         $this->db->set($data); 
         $this->db->where("status_category_code", $status_category_code); 
         $this->db->update("status_category", $data); 
      } 


}

?>