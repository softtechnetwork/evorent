<?php class Sms_Model extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 
    
    public function selectToTable($Search = null, $itemStt= null , $itemEnd= null){
        
		$Where = $this->Conditions($Search);
        
		$Query = "select * from   ( ";
		$Query .= "select  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum, sms.*, " ;
		$Query .= " customer.firstname, customer.lastname, customer.email, customer.idcard, customer.addr as address " ;

		$Query .= " FROM sms_history sms ";
		$Query .= " LEFT JOIN customer ON sms.customer_code = customer.customer_code ";
		$Query .= " ".$Where;
		$Query .= ") AS RowConstrainedResult ";
		$Query .= "WHERE   RowNum >= ".(int)$itemStt." AND RowNum <= ".(int)$itemEnd."  ORDER BY RowNum";
       
		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}

    public function selectAllItems($Search = null){
        
		$Where = $this->Conditions($Search);
		$Query = "SELECT COUNT(*) as allitems from sms_history sms ";
		$Query .= " LEFT JOIN customer ON sms.customer_code = customer.customer_code ";
		$Query .= " ".$Where;
        
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return  $data ;
	}

    public function Conditions($Search = null){
       
		$Where = "";
		$data = array();
        $date = array();
        $date['stdate']='';
        $date['enddate']='';

		if(!empty($Search)){
            $obj = json_decode($Search);
           
            foreach($obj as $key => $item){
                switch($key){
                    case 'stdate':
                        if(!empty($item)){
                            $Date = explode('/', $item);
                            $date[$key]  = $Date[2].'-'.$Date[1].'-'.$Date[0];
                        }
                    break;
                    case 'enddate':
                        if(!empty($item)){
                            $Date = explode('/', $item);
                            $date[$key]  = $Date[2].'-'.$Date[1].'-'.$Date[0];
                        }
                    break;
                    default:
                        if(!empty($item)){  
                            array_push($data, "(sms.".$key." like N'%".$item."%')");
                         }
                    break;
                }
            }
		}
		
        
        if( $date['stdate'] != '' && $date['enddate'] !='' ){
            array_push($data, "(sms.cdate BETWEEN convert(date,N'".$date['stdate']."') AND convert(date,N'".$date['enddate']."')) ");
        }
		
        $round = 0;
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    if($round == 0){
                        $Where .= " WHERE ".$items;
                    }else{
                        $Where .= " AND ".$items;
                    }
                    $round++;
                }
            }
        }
	    return $Where;
	}

    
      public function getTempToGenCode(){
		$Query = "select * from sms_history ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function insertSMS($data) {
        if ($this->db->insert("sms_history", $data)) { 
            return true; 
        }
    } 
    

} ?>