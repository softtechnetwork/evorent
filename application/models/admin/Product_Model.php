<?php
class Product_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 

		public function productcateTocombo(){
			$Query = "select * from product_category where cate_id is not null";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function brandTocombo(){
			$Query = "select * from brand";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function getToGenCode(){
			$Query = "select * from product";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}
		
		public function InstallmentTypeToGenCode(){
			$Query = "select * from installment_type";
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function ProductInsert($data) { 
			if ($this->db->insert("product", $data)) { 
				return true; 
			}
		} 

		public function InstallmentTypeInsert($data) { 
			if ($this->db->insert("installment_type", $data)) { 
				return true; 
			}
		} 

		public function getToList($Search = null){

			$where = null;
			if(!empty($Search)){
				$searchs = json_decode($Search);
				$conditions = array();
				foreach($searchs as $key =>$items){
					switch( $key){
						case'search_text':
							if(!empty($items)){
								array_push($conditions," (p.product_id LIKE '%".$items."%' OR p.product_name LIKE '%".$items."%')");
							}
						break;
						default:
							if(!empty($items)){
								array_push($conditions, "p.".$key." LIKE '%".$items."%'");
							}
						break;
					}
				}

				foreach($conditions as $key => $items){
					if($key == 0){
						$where .= ' WHERE '.$items;
					}else{
						$where .= ' AND '.$items;
					}
				}
			}

			$Query = "SELECT p.*, pc.cate_name, b.brand_name" ;
			$Query .= " FROM product p " ;
			$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
			$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id".$where;

			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function GetToEdit($id = null){
			$where = null;
			if(!empty($id)){
				$where = "where p.product_id = N'".$id."'";
			}

			$Query = "SELECT p.*, pc.cate_name, b.brand_name" ;
			$Query .= " FROM product p " ;
			$Query .= " LEFT JOIN product_category pc ON p.product_cate = pc.cate_id";
			$Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id ".$where;
			
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function GetInstallmentTypeToEdit($id = null){
			$where = null;
			if(!empty($id)){
				$where = "where ist.product_id = N'".$id."'";
			}

			$Query = "SELECT ist.*" ;
			$Query .= " FROM installment_type ist ".$where;
			
			$Res= $this->db->query($Query);
			$data = $Res->result();
			return $data;
		}

		public function update($data,$id) { 
			$this->db->set($data); 
			$this->db->where("product_id", $id); 
			$this->db->update("product", $data); 
		}

		public function DeleteProduct($id) { 

			if(!empty($id)){
				$this->db->where('product_id', $id);
				$this->db->delete('product');

				$this->db->where('product_id', $id);
				$this->db->delete('installment_type');
				return true;
			}
		} 

}

?>