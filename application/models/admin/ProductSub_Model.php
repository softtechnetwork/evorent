<?php
class ProductSub_Model extends CI_Model {

    function __construct() { 
        parent::__construct(); 
    } 

    public function productTocombo(){
        $Query = "select * from product";
        $Res= $this->db->query($Query);
        $data = $Res->result();
        return $data;
    }

    public function getToGenCode(){
        $Query = "select * from product_sub";
        $Res= $this->db->query($Query);
        $data = $Res->result();
        return $data;
    }

    public function ProductSubInsert($data) { 
        if ($this->db->insert("product_sub", $data)) { 
            return true; 
        }
    } 

    public function getToList($Search = null){
        $where = null;
        if(!empty($Search)){
            $searchs = json_decode($Search);
            $conditions = array();
            foreach($searchs as $key =>$items){
                switch( $key){
                    case'search_text':
                        if(!empty($items)){
                            array_push($conditions," (ps.product_sub_id LIKE '%".$items."%' OR ps.product_sub_name LIKE '%".$items."%')");
                        }
                    break;
                    default:
                        if(!empty($items)){
                            array_push($conditions, "ps.".$key." LIKE '%".$items."%'");
                        }
                    break;
                }
            }
           
            foreach($conditions as $key => $items){
                if($key == 0){
                    $where .= ' WHERE '.$items;
                }else{
                    $where .= ' AND '.$items;
                }
            }
        }

        $Query = "SELECT ps.*, p.product_name, b.brand_name" ;
        $Query .= " FROM product_sub ps " ;
        $Query .= " LEFT JOIN product p ON ps.product_id = p.product_id";
        $Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id".$where;
        
        $Res= $this->db->query($Query);
        $data = $Res->result();
        return $data;
    }

    public function GetToEdit($id = null){
        $where = null;
        if(!empty($id)){
            $where = " WHERE ps.product_sub_id LIKE '%".$id."%'";
        }

        $Query = "SELECT ps.*, p.product_name, b.brand_name" ;
        $Query .= " FROM product_sub ps " ;
        $Query .= " LEFT JOIN product p ON ps.product_id = p.product_id";
        $Query .= " LEFT JOIN brand b ON p.product_brand = b.brand_id".$where;
        
        $Res= $this->db->query($Query);
        $data = $Res->result();
        return $data;
    }

    public function update($data,$id) { 
        $this->db->set($data); 
        $this->db->where("product_sub_id", $id); 
        $this->db->update("product_sub", $data); 
    }

    public function DeleteProductSub($id) { 
        if(!empty($id)){
            $this->db->where('product_sub_id', $id);
            $this->db->delete('product_sub');
            return true;
        }
    }

}

?>