<?php
class Calculate_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
      
	  public function product_set(){
		
		$Arr = array();
		$QueryProductSe = "select sets.masterproduct_id,  mast.product_parent_id FROM product_install_set sets " ;
        $QueryProductSe .= " INNER JOIN masterproduct mast ON sets.masterproduct_id = mast.id";
        $QueryProductSe .= " where mast.typeproduct_id = N'1'";

		$ResProductSet= $this->db->query($QueryProductSe)->result();
		$resArr = array();
		###### check serial number #########
		foreach($ResProductSet as $item){
			
			$getMainProduct = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$item->masterproduct_id."' and used_by is null";
			$ResMainProduct= $this->db->query($getMainProduct)->result();
			if($ResMainProduct != null){
				$array = array();
				
				$getSupProduct = "select id from masterproduct where  product_parent_id = N'".$item->masterproduct_id."'";
				$ResSupProduct= $this->db->query($getSupProduct)->result();
				if($ResSupProduct != null){
					$sub = array();
					$count = 0;
					foreach($ResSupProduct as $items){
						$getSerial = "select top(1) id, masterproduct_id, mp_serial_number from masterproduct_serialnumber where  masterproduct_id = N'".$items->id."' and used_by is null";
						$ResSupProducterial = $this->db->query($getSerial)->result();
						
						if($ResSupProducterial == null){
							$count++;
						}else{
							$subarray = (object) array(
								'id' => $ResSupProducterial[0]->masterproduct_id, 
								'serial' => $ResSupProducterial[0]->mp_serial_number
							);
							array_push($sub, $subarray);
						}
					}

					if($count == 0){
						$r = array(
							'main' =>  array('id' => $ResMainProduct[0]->masterproduct_id, 'serial' => $ResMainProduct[0]->mp_serial_number),
							'sub'=> (object)$sub
						);
						array_push($Arr,(object) $r);
					}
				}
				else{
					$r = array(
						'main' =>  array('id' => $ResMainProduct[0]->masterproduct_id, 'serial' => $ResMainProduct[0]->mp_serial_number),
						'sub'=>null
					);
					array_push($Arr, (object)$r);
				}

			}
		}
		
		############ get result #############
		$res = array();
		$mpid = '';
		$c = 1;
		foreach($Arr as $item){
			
			$Query = "select sets.id, sets.product_install_id, sets.masterproduct_id, sets.amount,sets.active_date,sets.expire_date" ;
			$Query .= " , inst.price_a,inst.price_b,inst.price_c,inst.price_d, inst.picode, inst.piname, inst.piabbrv ,inst.picode_ref_arm, inst.brand, inst.product_category_id" ;
			$Query .= " , mast.mpcode,mast.mpcode_ref_arm,mast.mpname,mast.mpabbrv,mast.brand,mast.product_category_id,mast.typeproduct_id,mast.product_parent_id,mast.mp_desc,mast.uom_id,mast.active,mast.tax_rate,mast.pic,mast.out_of_stock " ;
			//$Query .= " , null as mp_serial_number" ;

			$Query .= " , CASE	WHEN mast.id = N'".$item->main['id']."'    THEN '".json_encode($item)."'  ELSE ''   END  as mp_serial_number";

			$Query .= " , phi.product_hirepurchase_code,phi.formula_id,phi.product_install_id,phi.term_of_payment_start,phi.term_of_payment_end,phi.help_pay_installments_status,phi.holdonpayment,phi.installment_amount_per_installment,phi.interest " ;
			$Query .= " FROM product_install_set sets " ;
			
			//$Query .= " LEFT JOIN masterproduct_serialnumber mpserial  ON sets.id = mpserial.masterproduct_id";
			$Query .= " INNER JOIN product_install inst  ON sets.product_install_id = inst.id";
			$Query .= " INNER JOIN masterproduct mast ON sets.masterproduct_id = mast.id";
			$Query .= " LEFT JOIN product_hirepurchase_interest phi  ON inst.id = phi.product_install_id";
			$Query .= " WHERE mast.id = N'".$item->main['id']."' ";

			array_push($res, $this->db->query($Query)->result()[0]);
		}

	    return $res;
	  }
}

?>