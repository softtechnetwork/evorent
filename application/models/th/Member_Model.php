<?php
class Member_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 

      public function getRes($Search = null){
		
        $Where = $this->Conditions($Search);

        $Query = " select intt.* 
		, customer.idcard
		, status.label as status_label, status.background_color, status.color
		FROM contract_installment intt
		INNER JOIN customer ON intt.customer_code = customer.customer_code 
		LEFT JOIN status ON intt.status_code = status.status_code
		WHERE status.stautus_category = N'210603' ".$Where." Order By intt.period ";

		// $Query = "select intt.*";
        // $Query .= ", customer.idcard";
		// $Query .= ", status.label as status_label, status.background_color, status.color";
		// $Query .= " FROM installment intt";
        // $Query .= " INNER JOIN customer ON intt.customer_code = customer.customer_code ";
		// $Query .= " LEFT JOIN status ON intt.status_code = status.status_code ";
		// $Query .= " WHERE status.stautus_category = N'210603'".$Where;
		// $Query .= " Order By intt.period";
		$Res= $this->db->query($Query);
        $data = $Res->result();
	    return $data;
	  }

      public function Conditions($Search = null){
		$Where = "";
		$data = array();
		if(!empty($Search)){
			$obj = json_decode($Search);
            foreach($obj as $key => $item){
                switch($key){
                    case 'idcard':
                        if(!empty($item)){
                            $string = "( customer.idcard = N'".$item."' )";
                            array_push($data,  $string);
                        }
                    break;
                    default:
                        if(!empty($item) || $item != null){  
                            array_push($data, "intt.".$key." = N'".$item."'");
                         }
                    break;
                }
            }
		}
		
        if(!empty($data)){
            foreach( $data as $items ){
                if(!empty($items) ){
                    $Where .= " AND ".$items;
                }
            }
        }
	    return $Where;
	  }

}

?>