<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 

    public function res_profile($id_card){
        $Query = " SELECT c.* FROM customer c  WHERE c.idcard = '".$id_card."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function res_address($customer_code){
		$Query = "
            SELECT 
            inhabited.[inhabited_code]
            ,inhabited.[customer_code]
            ,inhabited.[category]
            ,inhabited.[building_type]
            ,inhabited.[company_name]
            ,inhabited.[village_name]
            ,inhabited.[address] as addr
            ,inhabited.[province_id]
            ,inhabited.[amphurs_id]
            ,inhabited.[district_id]
            ,inhabited.[zip_code]
            ,inhabited.[cdate]
            ,inhabited.[udate]
            , p.province_name, a.amphur_name, t.district_name
            FROM inhabited 
            LEFT JOIN provinces p ON inhabited.province_id = p.id 
            LEFT JOIN amphurs a ON inhabited.amphurs_id = a.id
            LEFT JOIN tumbon t ON inhabited.district_id = t.id
        
            WHERE inhabited.customer_code = '".$customer_code."'
             
        ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	// public function Conditions($Search = null){
	// 	$Where = "";
	// 	$data = array();
	// 	if(!empty($Search)){
	// 		$obj = json_decode($Search);
    //         foreach($obj as $key => $item){
    //             switch($key){
    //                 case 'name':
    //                     if(!empty($item)){
    //                         $string = "( customer.firstname like N'%".$item."%' or customer.lastname like N'%".$item."%' )";
    //                         array_push($data,  $string);
    //                     }
    //                 break;
	// 				case 'status':
    //                     if($item != null){
    //                         $string = "( status.id = N'".$item."' )";
    //                         array_push($data,  $string);
    //                     }
    //                 break;
    //                 default:
    //                     if(!empty($item) || $item != null){  
    //                         array_push($data, "contract.".$key." like N'%".$item."%'");
    //                      }
    //                 break;
    //             }
    //         }
	// 	}
		
		
    //     $round = 0;
    //     if(!empty($data)){
    //         foreach( $data as $items ){
    //             if(!empty($items) ){
    //                 if($round == 0){
    //                     $Where .= " Where ".$items;
    //                 }else{
    //                     $Where .= " AND ".$items;
    //                 }
    //                 $round++;
    //             }
    //         }
    //     }
	//     return $Where;
	// }
	


}

?>