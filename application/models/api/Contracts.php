<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contracts extends CI_Model {

	function __construct() { 
	    parent::__construct(); 
	} 

    public function res_contract($id_card){
		$Query = "
            SELECT 
            --ct.*, 
            ct.contract_code,
            ct.product_id,
            --ct.rental_period,
            --( select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is not null and status_code = '1' ) as paided ,
            --( select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is null and status_code = '0' ) as unpaided ,
            --( ct.monthly_rent  * (select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is null and status_code = '0') ) as unpaid_balance,
            
            (
                select COUNT(installment.id) FROM contract_installment installment  
				where contract_code = ct.contract_code  and status_code ='0' and ( CONVERT(date,installment.payment_duedate) < CONVERT(date,dateadd(year,+543,GETDATE())) )
            ) as overdue_period,
			(
                select case  when SUM(installment.installment_payment) is null then 0 else  SUM(installment.installment_payment) end FROM contract_installment installment  
				where contract_code = ct.contract_code  and status_code ='0' and ( CONVERT(date,installment.payment_duedate) < CONVERT(date,dateadd(year,+543,GETDATE())) )
            ) as overdue_balance,

            ps.product_name,
            pc.cate_name,
            status.label as contract_status,
            ct.status as contract_status_id
            
            FROM contract ct
            inner join customer c on ct.customer_code = c.customer_code
            inner join product_set ps on ct.product_id = ps.product_id
            inner join product_category pc on ps.product_cate = pc.cate_id 
            inner join status on ct.status = status.id
            
            
            WHERE c.idcard = '".$id_card."'
            
        ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function res_contract_code($id_card , $contract_code){
		$Query = "
            SELECT 
            --ct.*, 
            ct.contract_code,
            ct.rental_period,
            ct.product_id,
            ( select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is not null and status_code = '1' ) as paided ,
            ( select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is null and status_code = '0' ) as unpaided ,
            ( ct.monthly_rent  * (select count(*) from contract_installment  where contract_code = ct.contract_code and payment_amount is null and status_code = '0') ) as unpaid_balance,
            
            (
                select COUNT(installment.id) FROM contract_installment installment  
				where contract_code = ct.contract_code  and status_code ='0' and ( CONVERT(date,installment.payment_duedate) < CONVERT(date,dateadd(year,+543,GETDATE())) )
            ) as overdue_period,
			(
                select case  when SUM(installment.installment_payment) is null then 0 else  SUM(installment.installment_payment) end FROM contract_installment installment  
				where contract_code = ct.contract_code  and status_code ='0' and ( CONVERT(date,installment.payment_duedate) < CONVERT(date,dateadd(year,+543,GETDATE())) )
            ) as overdue_balance,

            ps.product_name,
            pc.cate_name,
            status.label as contract_status,
            ct.status as contract_status_id
            
            FROM contract ct
            inner join customer c on ct.customer_code = c.customer_code
            inner join product_set ps on ct.product_id = ps.product_id
            inner join product_category pc on ps.product_cate = pc.cate_id  
            inner join status on ct.status = status.id
            
            
            WHERE c.idcard = '".$id_card."' and contract_code ='".$contract_code."'
            
        ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    public function res_contract_installments($id_card, $contract_code){
		$Query = "
        SELECT contract_installment.*
        FROM contract_installment
        INNER JOIN customer on contract_installment.customer_code = customer.customer_code
        WHERE contract_installment.contract_code='".$contract_code."' and customer.idcard = '".$id_card."'
        ";
        //$Query = "SELECT * FROM contract_installment WHERE contract_code ='".$contract_code."' and customer_code = '".$customer_code."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
    
    public function res_contract_installments_all($contract_code){
		$Query = "
            SELECT ROW_NUMBER() OVER ( ORDER BY installment.contract_code ) AS RowNum, installment.*, 
            
            CASE  
            WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) 
            THEN 'เกิน '+ CAST( DATEDIFF(DAY,  CONVERT(date,dateadd(year,-543,installment.extend_duedate)),   CONVERT(date,GETDATE()) )AS nvarchar)+' วัน' 

            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 5 วัน'      
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 4 วัน'    
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 3 วัน'  
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 2 วัน' 
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) THEN  N'อีก 1 วัน' 
            WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) THEN  N'ครบกำหนดแล้ว'
            ELSE '' END AS overdue, 
            
            CASE
            WHEN installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) THEN 10
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  5
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  4
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  3
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  2
            WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  1 
            WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  )THEN ''
            ELSE 1000 END AS overdue_code,
            
            c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name, brand.brand_name,
            st.status_code as statusCode, st.stautus_category, st.label, st.detail, st.color, st.background_color,
            sms.cdate as sms_cdate, CASE WHEN  ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) ) THEN  1 ELSE 0 END AS sendsms
            
            FROM contract_installment installment
            INNER JOIN contract t ON installment.contract_code = t.contract_code
            INNER JOIN customer c ON installment.customer_code = c.customer_code
            LEFT JOIN product_set product  ON t.product_id = product.product_id
            LEFT JOIN brand  ON t.product_brand = brand.brand_id
            LEFT JOIN status st  ON installment.status_code = st.status_code
            LEFT JOIN sms_history sms ON t.contract_code = sms.temp_code and installment.period = sms.period and ( CONVERT(date,sms.cdate)   =  dateadd(DAY,+0,CONVERT(date,dateadd(year,+543,GETDATE()))) )
            WHERE  st.stautus_category = N'210603' and installment.contract_code = '".$contract_code."'
        ";
        //$Query = "SELECT * FROM contract_installment WHERE contract_code ='".$contract_code."' and customer_code = '".$customer_code."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}
}

?>