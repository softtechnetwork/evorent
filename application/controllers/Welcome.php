<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
    
        parent::__construct(); 
		// login     
		/*if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}*/

		redirect(base_url('./home'));
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('admin/Customer_Model');
		//$this->load->model('admin/Premise_Model');
		//$this->load->model('admin/Status_Model');

		$this->load->model('admin/Logs_Model');

		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	public function index()
	{
		//$this->load->view('welcome_message');
		$this->load->view('admin/header');
		$this->load->view('admin/temp_list');
        $this->load->view('admin/footer');
	}
}
