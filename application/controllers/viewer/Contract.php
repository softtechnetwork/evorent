<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('session');
		
		// login     
		if(!$this->session->userdata('viewerLog')){
			redirect(base_url('viewer/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->helper('file'); 
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Model     
        $this->load->model('admin/Temp_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Product_Model');
		$this->load->model('admin/Logs_Model');
		$this->load->model('admin/Premise_Model');

        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Service_Model');
        
		$this->load->model('viewer/Contract_Model');

		 // load db ( agent )
		 $this->load->config('externaldb');
		 $this->db_config = $this->config->item('agent_db');
		 //$this->connectSeminarDB();

		 
        
    } 

	##############  List #####################
	public function index()
	{
 		$data['resStatus'] = $this->Contract_Model->requestStatus('210602');
        
		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('viewer/header',$menu);
		$this->load->view('viewer/contract_list', $data);
        $this->load->view('viewer/footer');
	}
	public function contract_GetRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Contract_Model->getToList($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}
	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Contract_Model->selectAllItems($Search);
		echo json_encode($data);
	}



    #################  Create #####################
    public function created()
	{
		$data['resStatus'] = $this->Contract_Model->requestStatus('210602');
		$data['Customer'] =  $this->Contract_Model->CustomerToCombo();
		$data['ProductCate'] =  $this->Contract_Model->ProductCateToCombo();
		$data['CustoerType'] =  $this->Contract_Model->requestStatus('211001');
		$data['ContractType'] =  $this->Contract_Model->requestStatus('211003');
		$data['Broker'] =  $this->Contract_Model->CharngToCombo('broker',true);
		$data['Agent'] =  $this->Contract_Model->CharngToCombo('agent',true);
		$data['province'] = $this->Contract_Model->province();
		
        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_create',$data);
        $this->load->view('admin/footer');
	}
    public function GetProducts()
	{
		$product_cate = $this->input->post('product_cate');
		$customer_type = $this->input->post('customer_type');
		$contract_type = $this->input->post('contract_type');
		$data = $this->Contract_Model->GetProducts($product_cate, $customer_type, $contract_type);		
		echo json_encode($data);
	}
	public function getIhbByCus()
	{
		$postData = $this->input->post();
		$data = $this->Contract_Model->getIhbByCus($postData);
		echo json_encode($data);
	}
	public function GetInstallmentTypeByProductID()
	{
		$product_id = $this->input->post('product_id');
		$data = $this->Contract_Model->GetInstallmentTypeByProductID($product_id);		
		echo json_encode($data);
	}

    public function insert(){

		$prefixdate = date("ym"); 
		$prefixText = 'C';
		$prefix =  $prefixText.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Contract_Model->getContractToGenCode($prefix);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->contract_code,5,10));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		$date = date(date_format(date_create(),"Y-m-d H:i:s"));

		$installmentCheck = "[".$this->input->post('installmentCheck')."]";
		$rental_period = (int)json_decode($installmentCheck)[0]->amount_installment;
		$monthly_rent = (int)json_decode($installmentCheck)[0]->per_month_price;
		$data = array( 
			'contract_code' => $code, 
			'customer_code' => $this->input->post('customer'),
			'product_cate' => $this->input->post('product-cate'),
			'customer_type' => $this->input->post('customer-type'),
			'contract_type' => $this->input->post('contract-type'),
            'product_id' => $this->input->post('product'),
			'product_brand' => $this->input->post('product-brand-id'),
			'product_version' => $this->input->post('product-version'),
			'payment_due' => $this->input->post('payment-due'),
			'inhabited_name' => $this->input->post('inhabited-name'),
			'addr' => $this->input->post('inhabited'),
			'installment' => $installmentCheck,
			'down_payment' => $this->input->post('down-payment'),
			'advance_payment' => $this->input->post('advance-payment'),
			'statart_payment_amount' => $this->input->post('statart_payment_amount'),
			'installation_fee' => $this->input->post('installation-fee'),
			'product_value' => $this->input->post('product-value'),
			'sale_code' => $this->input->post('sale-id'), 
			'techn_code' => $this->input->post('techn-id'), 
			'remark' => $this->input->post('remark'),
			'status' => $this->input->post('status'),
			//'product_price' => $this->input->post('product-price'),
			'product_count' => $this->input->post('product-count'),
			'product_number' => $this->input->post('property-number'),
			//'product_serial' => $this->input->post('product-serial'),

			//'lease_date' => $date,
			
			'place_ins_size' => $this->input->post('place-ins-size'),
			'annual_rental_rate' => $this->input->post('annual-rental-rate'),
			
			//'monthly_rent' => $this->input->post('monthly-rent'),
			'monthly_rent' => $monthly_rent,
			//'monthly_plus_tax' => $this->input->post('monthly-plus-tax'),
			//'installment' => $this->input->post('installment'),
			//'rental_period' => $this->input->post('rental-period'),
			'rental_period' => $rental_period,
			'supporter' => (int)$this->input->post('supporter-check'),
            'cdate'=> $date,
            'udate'=> $date
        ); 
        
		if( $this->input->post('contract-do-date') != null){
			$data['contract_do_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-do-date')),"Y-m-d"));
		}
		if( $this->input->post('contract-date') != null){
			$data['contract_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-date')),"Y-m-d"));
		}
		if( $this->input->post('payment-start-date') != null){
			$data['payment_start_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
		}

		$PercenEndContract = $this->input->post('percen-end-contract');
		$ValueEndContract = ($data['monthly_rent'] * $PercenEndContract)/100;
		$data['percen_end_contract'] = $PercenEndContract;
		$data['value_end_contract'] = ceil($ValueEndContract);

		$this->Contract_Model->insert($data); 
		
		############# Create Installment ############
		$newStatus = $this->Contract_Model->getStatus((int)$data['status']);
		$newInstallent = $this->Contract_Model->getInstallment($data['contract_code']);
	   
		if($newStatus[0]->status_code == 0 && $newInstallent == null){ 
		   
		   $dataistall = array( 
			   //'loan_code' => $dataloan['loan_code'],
			   'contract_code' => $data['contract_code'],
			   'product_id' =>$data['product_id'],
			   'customer_code' => $data['customer_code'],
			   'cdate'=> $data['cdate'],
               'udate'=> $data['udate']
		   ); 

		   if((int)$data['payment_due'] < 10){ $data['payment_due'] = "0".$data['payment_due'];}

		   $cdated = date("Y-m", strtotime($data['payment_start_date']));
		   for ($i=1; $i <= (int)$rental_period ; $i++) { 
			   	######### Payment due date #########
			   	$payment_duedate = null;
			   	if((int)$i == 1){
				   if( $this->input->post('payment-start-date') != null){
					   $payment_duedate = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
				   }
			   	}else{
				   	$mont = $i-1;
					$montChange = date('Y-m', strtotime("+1 months ", strtotime($cdated)));
					$cdated = $montChange;
					$maxdate = date("Y-m-t", strtotime($montChange));
					$date = explode('-', $maxdate); 
					
					if($date[1] != '02'){
						$date[2] = $data['payment_due'];
					}else{
						if( (int)$data['payment_due'] < (int)$date[2]){
							$date[2] = $data['payment_due'];
						}
					}
					$payment_duedate = date(implode('-', $date));
			   	}

			   	######### Extend due date #######
			   	$extend_duedate = null;
				$payment_duedate_exp = explode('-', $payment_duedate);
			   	if($payment_duedate_exp[2] == '15'){
					$maxDays = date("Y-m-t",strtotime($payment_duedate));
					$date = explode('-', $maxDays); 
					if((int)$date[2] < 30){
						$payment_duedate_exp[2] = $date[2];
					}else{
						$payment_duedate_exp[2] = '30';
					}
					$extend_duedate = date(implode('-', $payment_duedate_exp));
				}else{
					$montChange = date('Y-m', strtotime("+1 months ", strtotime(date("Y-m", strtotime($payment_duedate)))));
					$date = explode('-', $montChange); 
					$date[2] = '15';
					$extend_duedate = date(implode('-', $date));
				}
			   
				$dataistall['status'] = $this->Contract_Model->requestStatus('210603', 0)[0]->id;
				$dataistall['status_code'] = 0;
				$dataistall['payment_amount'] = null;
				$dataistall['period'] = $i;
				$dataistall['payment_duedate'] = $payment_duedate;
				$dataistall['extend_duedate'] = $extend_duedate;
				$dataistall['installment_payment'] = $monthly_rent;

			   	$this->Contract_Model->insertInstallment($dataistall);
		   }
		}
		###########################################

		
		######## Create Serial ##########
		$SubByProduct = $this->Contract_Model->GetSubByProduct($data['product_id']);
		if($SubByProduct){
			foreach($SubByProduct as $item){
				for($i=1; $i <= $item->count; $i++){
					$serialnumber = array( 
						'contract_code' => $data['contract_code'],
						'product_id' => $item->product_set_id,
						'product_sub_id' => $item->id,
						'no' => 1,
						//'serial_number' => $this->input->post('serial_'.$item->product_sub_id),
						'cdate'=> $data['cdate'],
						'udate'=> $data['udate']
					);
					$this->Contract_Model->CreateSerial($serialnumber); 
				}
				
				/*$serialnumber = array( 
					'contract_code' => $data['contract_code'],
					'product_id' => $item->product_set_id,
					'product_sub_id' => $item->id,
					'no' => 1,
					//'serial_number' => $this->input->post('serial_'.$item->product_sub_id),
					'cdate'=> $data['cdate'],
                    'udate'=> $data['udate']
				);
				$this->Contract_Model->CreateSerial($serialnumber); */
			}
		}
		##################################


		################ Service ###################
		$newService = $this->Contract_Model->getService($data['contract_code']);
		if($newStatus[0]->status_code == 0 && $newService == null){ 
			$dataService = array( 
				//'loan_code' => $dataloan['loan_code'],
				'contract_code' => $data['contract_code'],
				'service_count' => 1,
				'service_round' => 0,
				'service_round_unit' => 'month',
				'cdate' =>$data['cdate'],
                'udate'=> $data['udate']
			);
			$this->Contract_Model->insertService($dataService);
		}
		###########################################
		
		
		######## Supporter ########## //ยังไม่สามารถให้แก้ไขข้อมูลได้
		if($data['supporter'] == 1){
			$dataSupporter = array( 
				'contract_code' => $data['contract_code'],
				'fname' => $this->input->post('name-supporter'),
				'lname' => $this->input->post('sname-supporter'),
				'idcard' => $this->input->post('idcard-supporter'),
				//'bdate' => $this->input->post('bdate-supporter'),
				'tel' => $this->input->post('mobile-supporter'),
				'addr' => $this->input->post('address-supporter'),
				'province_id' => $this->input->post('province-supporter'),
				'amphurs_id' => $this->input->post('amphurs-supporter'),
				'district_id' => $this->input->post('district-supporter'),
				'zip_code' => $this->input->post('zipcode-supporter'),
				'cdate'=> $data['cdate'],
                'udate'=> $data['udate']
			);
			##### Bdate ####
			$supporterBdate = $this->input->post('bdate-supporter');
			if( $supporterBdate != null){
				$dataSupporter['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $supporterBdate),"Y-m-d"));
			}
			$this->Contract_Model->insertSupporter($dataSupporter); 
		}
		##################################

        redirect( base_url('admin/contract') );
    }

    #### Edit ####
    public function edit($id = null)
    {
        $data['resStatus'] = $this->Status_Model->requestStatus('210602');
        $data['res'] = $this->Contract_Model->selectOne($id);
        $data['supporter'] = $this->Contract_Model->selectSupporter($id);

        $data['inhabited'] = $this->Contract_Model->getInhabitedEdit($data['res'][0]->addr);
        $data['province'] = $this->Contract_Model->province();

        $menu['mainmenu'] = 'contract';
        $menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
        $this->load->view('admin/contract_edit',$data);
        $this->load->view('admin/footer');
    }
	public function update(){

		$contract_code = $this->input->post('contract_code');
		$date = date("Y-m-d H:m:s");
		$product_id = $this->input->post('product_id');
		$data = array( 
			//'addr' => $this->input->post('inhabited'),
			//'payment_due' => $this->input->post('payment-due'),
			'advance_payment' => $this->input->post('advance-payment'),
			'installation_fee' => $this->input->post('installation-fee'),
			'product_value' => $this->input->post('product-value'),

			'inhabited_name' => $this->input->post('inhabited-name'),
			'place_ins_size' => $this->input->post('place-ins-size'),
			'annual_rental_rate' => $this->input->post('annual-rental-rate'),
			//'techn_code' => $this->input->post('sale-id'), 
			'remark' => $this->input->post('remark'),
			'status' => $this->input->post('estatus'),
            'udate'=>$date
         ); 


		######## Supporter ##########
		$oldSupport = (int)$this->input->post('supportered');
		$newSupport = (int)$this->input->post('supporter-check');
		
		$dataSupporter = array();
		if( $oldSupport == 0 && $newSupport == 1){
			$data['supporter'] = $newSupport;
			$dataSupporter['contract_code'] = $contract_code;
			$dataSupporter['fname'] = $this->input->post('name-supporter');
			$dataSupporter['lname'] = $this->input->post('sname-supporter');
			$dataSupporter['idcard'] = $this->input->post('idcard-supporter');
			$dataSupporter['tel'] = $this->input->post('mobile-supporter');
			$dataSupporter['addr'] = $this->input->post('address-supporter');
			$dataSupporter['province_id'] = $this->input->post('province-supporter');
			$dataSupporter['amphurs_id'] = $this->input->post('amphurs-supporter');
			$dataSupporter['district_id'] = $this->input->post('district-supporter');
			$dataSupporter['zip_code'] = $this->input->post('zipcode-supporter');
			$dataSupporter['cdate'] = $data['udate'];
		
			##### Bdate ####
			$supporterBdate = $this->input->post('bdate-supporter');
			if( $supporterBdate != null){
				$dataSupporter['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $supporterBdate),"Y-m-d"));
			}
		}
		##################################
        
		$this->Contract_Model->update($data,$contract_code); // update temp
		
		if( $oldSupport == 0 && $newSupport == 1){
			$this->Contract_Model->insertSupporter($dataSupporter);  // create supporter
		}


		 ############# Creat Installment ############
		 $newStatus = $this->Contract_Model->getStatus((int)$data['status']);
		 $newContract = $this->Contract_Model->getContractToGenCodes($contract_code);
		 $newInstallent = $this->Contract_Model->getInstallment($contract_code);
		
		if($newStatus[0]->status_code == 0 && $newInstallent == null){ 
			$dataistall = array( 
				//'loan_code' => $dataloan['loan_code'],
				'contract_code' => $newContract[0]->contract_code,
				'product_id' =>$newContract[0]->product_id,
				'customer_code' => $newContract[0]->customer_code,
				'cdate'=> $data['udate'],
                'udate'=> $data['udate']
			); 

			if((int)$newContract[0]->payment_due < 10){ $newContract[0]->payment_due = "0".$newContract[0]->payment_due;}

			for ($i=1; $i <= (int)$newContract[0]->rental_period ; $i++) { 
				$payment_duedate = null;
				//-------------- Payment due date --------------------//
				if((int)$i == 1){
					if( $this->input->post('payment-start-date') != null){
						$payment_duedate = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
					}
				}else{
					$mont = $i-1;
					$montChange = date('Y-m-d', strtotime("+".$mont." months ", strtotime($data['payment_start_date'])));
					$maxdate = date("Y-m-t", strtotime($montChange));
					$date = explode('-', $maxdate); 
					
					if((int)$data['payment_due'] < (int)$date[2]){
						$date[2] = $data['payment_due'] ;
					}
					//$payment_duedate = implode('-', $date);
					$payment_duedate = date(implode('-', $date));
				}
                $dataistall['status'] = $this->Contract_Model->requestStatus('210603', 0)[0]->id;
                $dataistall['status_code'] = 0;

				$dataistall['period'] = $i;
				$dataistall['payment_duedate'] = $payment_duedate;
				$dataistall['installment_payment'] = $newContract[0]->monthly_rent;
				
				$this->Contract_Model->insertInstallment($dataistall);
			}
		}

		 ###########################################

		 ################ Service ###################
		 $newService = $this->Contract_Model->getService($contract_code);
		 if($newStatus[0]->status_code == 0 && $newService == null){ 
			$dataService = array( 
				 //'loan_code' => $dataloan['loan_code'],
				 'contract_code' => $contract_code,
				 'service_count' => 1,
				 'service_round' => 0,
				 'service_round_unit' => 'month',
				 'cdate' =>$data['udate'],
                 'udate' =>$data['udate']
			);
			$this->Contract_Model->insertService($dataService);
		 }
		 ###########################################

		################# Create Logs ##############
		$LogsJSON = json_encode($data);
		$code = date("dmY-His");
		$logs_data = array( 
			//'img' => $img, 
			'logs_code' =>  "Logs".$code, 
			'ref_code' => $contract_code, 
			'category' => 'Temp', 
			'logs' => $LogsJSON, 
			'create_logs'=>$data['udate']
			
		);
		////$this->Logs_Model->insert($logs_data); 
		
		###########################################

        redirect(base_url('admin/contract'));
    }
    
	#### Detail ####
	public function detail($id = null)
	{
		$data['res'] = $this->Contract_Model->selectOneDeatil($id);
		$data['supporter'] = $this->Contract_Model->selectSupporter($id);

		$customer_addr = 'ที่อยู่ปัจจุบัน';
		if($data['res'][0]->type == 'corperation'){$customer_addr = 'ที่อยู่สถานที่ทำงาน';}
		$data['address'] = $this->Contract_Model->detailAddress($data['res'][0]->customer_code, $customer_addr);
		$data['installationLocation'] = $this->Contract_Model->detailInstallationLocation($data['res'][0]->installationlocation);

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('viewer/header',$menu);
		$this->load->view('viewer/contract_detail',$data);
        $this->load->view('viewer/footer');
	}

    #### Premise ####
	public function premise($id = null)
	{
		$data['res'] = $this->Contract_Model->selectOne($id);
        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('viewer/header',$menu);
		$this->load->view('viewer/contract_premise',$data);
        $this->load->view('viewer/footer');
	}

	public function contractPDF($mode = null, $id = null)
	{
		//เรียกใช้งาน Libary PDF    
		$this->load->library('Pdf');
		$res = $this->Contract_Model->selectOneDeatil($id);
		$subProduct = $this->Contract_Model->ProductSubByProductId($res[0]->contract_code, $res[0]->product_id);
		
		$address1 = $this->Contract_Model->detailAddress($res[0]->customer_code, 'ที่อยู่ตามบัตรประชาชน');
		$address2 = $this->Contract_Model->detailAddress($res[0]->customer_code,'ที่อยู่ปัจจุบัน');
		$address3 = $this->Contract_Model->detailAddress($res[0]->customer_code,'ที่อยู่สถานที่ทำงาน');

		$installationLocation = $this->Contract_Model->detailInstallationLocation($res[0]->installationlocation);
		
		$broker = $this->Contract_Model->CharngToPDF($res[0]->sale_code);
		$agent = $this->Contract_Model->CharngToPDF($res[0]->techn_code);
		

		$Supporter = null;
		if($res[0]->supporter == 1){
			$Supporter = $this->Contract_Model->selectSupporter($id);
		}

		// สร้าง object สำหรับใช้สร้าง pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
        // กำหนดรายละเอียดของ pdf
        
		//$pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide';
		
         
        // กำหนดข้อมูลที่จะแสดงในส่วนของ header และ footer
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// กำหนดรูปแบบของฟอนท์และขนาดฟอนท์ที่ใช้ใน header และ footer
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // กำหนดค่าเริ่มต้นของฟอนท์แบบ monospaced 
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // กำหนด margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // กำหนดการแบ่งหน้าอัตโนมัติ
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        // กำหนดรูปแบบการปรับขนาดของรูปภาพ 
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        // ---------------------------------------------------------

        // set default font subsetting mode
		$DateControlDoc = '';
        $pdf->setFontSubsetting(true);
		####### ประเภทสัญญาเป็น เช่าใช้ (1020 == เช่าใช้) #######
		if($res[0]->product_contract_type == '1020'){
			$DateControlDoc =  '10-06-2021';
			if($mode == 'origin'){
				$this->Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
				$this->Agreement($pdf, $DateControlDoc, $res);
				$this->Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
				if($Supporter != null){
					$this->Supporter($pdf, $DateControlDoc, $res, $Supporter);
				}
			}else{
				$this->ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
				$this->AgreementCoppy($pdf, $DateControlDoc, $res);	
				$this->ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
				
				if($Supporter != null){
					$this->SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter);	
				}
			}
		}

		####### หมวดสินค้าเป็น โซล่าห์เซลล์ (0012 == โซล่าห์เซลล์) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if($res[0]->product_cate == '0012' && $res[0]->product_contract_type == '1021'  ){
			// โซลาร์เซลล์ doc0010 / doc0011 / doc0012 / doc 0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);

		}
		####### หมวดสินค้าเป็น โซล่าห์ปั๊มส์ (0013 == โซล่าห์ปั๊มส์) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if($res[0]->product_cate == '0013' && $res[0]->product_contract_type == '1021'  ){
			// โซลาร์เซลล์ doc0010 / doc0011 / doc0012 / doc 0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);

		}
		####### หมวดสินค้าเป็น แอร์ (0007, 0011) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if(($res[0]->product_cate == '0007' || $res[0]->product_cate == '0011') && $res[0]->product_contract_type == '1021'){
			// แอร์ doc0014 / doc0012 / doc0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0014($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);

		}
		####### ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) และ เอกสารสัญญาทั่วไป (CD220304  == ทั่วไป) #######
		if($res[0]->product_contract_type == '1021' && $res[0]->product_contract_doc == 'CD220304'){
			// แอร์ doc0018 / doc0012 / doc0015 / doc0016
			$DateControlDoc =  '21-03-2022';
			$this->DOC0018($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
		}

		$file_name = 'สัญญาเลขที่-'.$res[0]->contract_code.'.pdf';
		$pdf->Output($file_name, 'I');
	}

	public function DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '28-03-2023';
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		$currentAddress = '';
		if($res[0]->type == 'person'){
			$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		}else{
			$currentAddress = $address3[0]->address.'  ตำบล/แขวง '.$address3[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address3[0]->amphur_name).'  จังหวัด'.$address3[0]->province_name.'  รหัสไปรษณีย์ '.$address3[0]->zip_code;
		}
		$pdf->Text(20, 97.5, $currentAddress);
		
		if(!empty($res[0]->statart_payment_amount)){
			$pdf->Text(126, 150.4,number_format($res[0]->statart_payment_amount));
			$pdf->Text(15, 159, $this->PriceToText($res[0]->statart_payment_amount));
		}

		$pdf->Text(145, 159, $res[0]->rental_period);

		//$pdf->Text(115, 177, $res[0]->productPricce);
		//$pdf->Text(15, 185.7, $this->PriceToText((int)$res[0]->productPricce));
		$pdf->Text(115, 185.7,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 194.5, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 210.6, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '28-03-2023';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$inhabited_name = (!empty($res[0]->inhabited_name))?$res[0]->inhabited_name:$fulName;
		$pdf->Text(25, 80, $inhabited_name);
		$pdf->Text(85, 97.5, $res[0]->contract_code);
		$pdf->Text(158, 97.5, $currentDate);
		$pdf->Text(75, 106.5, $fulName);

		$pdf->Text(138, 159.2, $fulName);
		$pdf->Text(35, 168, number_format($res[0]->place_ins_size));

		$pdf->Text(27, 203, $res[0]->contract_code);
		$pdf->Text(90, 203, $currentDate);

		$pdf->Text(34, 212, $currentDate);
		$new_date = date('d/m/Y', strtotime('+ 3 year', strtotime($res[0]->contract_do_date)));
		$pdf->Text(125, 212, $new_date);

		$rental_rate = '';
		$rental_rate_text = '';
		if($res[0]->annual_rental_rate == 0){
			$rental_rate = 'ไม่มีค่าใช้จ่าย';
			$rental_rate_text = 'ไม่มีค่าใช้จ่าย';
		}else{
			$rental_rate = number_format($res[0]->annual_rental_rate);
			$rental_rate_text = $this->PriceToText((int)$res[0]->annual_rental_rate);
		}
		$pdf->Text(27, 265, $rental_rate);
		$pdf->Text(117, 265, $rental_rate_text);

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(133, 78.3, $inhabited_name);
	}
	public function DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '22-10-2021';
		//$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0012/DOC0012('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		/*$lines = 75.6;
		for ($i=0; $i < count($subProduct) ; $i++) { 
			$pdf->Text(20, $lines, $subProduct[$i]->product_master_name);
			$lines=$lines+8.9;
		}*/
		
		$html = '<html><head>';
			$html .= '<style>

			.tableInstallment .InstHeader{
				background-color: #17a2b8 !important;
			}
			.tableInstallment .InstHeader .productSubNo, .tableInstallment .InstContent .productSubNo{
				width: 50px !important;
			}
			.tableInstallment .InstHeader .productSub, .tableInstallment .InstContent .productSub{
				width: 220px !important;
			}
			.tableInstallment .InstHeader .serialNumber, .tableInstallment .InstContent .serialNumber{
				width: 180px !important;
			}
			.tableInstallment .InstHeader .productSubCunt, .tableInstallment .InstContent .productSubCunt{
				width: 50px !important;
			}
			</style>';
			$html .= '</head><body id"tableInstallment">';
			$html .= '<br/><br/><br/>';

			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="1" border="1" style="border-color:red;">';
			$html .= '<tr class="InstHeader">  ';
			$html .= '<th class="productSubNo">ลำดับ</th> ';
			$html .= '<th class="productSub">อุปกรณ์</th> ';
			//$html .= '<th class="productSubCunt">จำนวน</th> ';
			$html .= '<th class="serialNumber">หมายเลขเครื่อง</th> ';
			$html .= '</tr>';

			$serialNodata = $this->Contract_Model->serialNodata($res[0]->contract_code, $res[0]->product_id);
			$serialData = $this->Contract_Model->serialData($res[0]->contract_code, $res[0]->product_id, $serialNodata[0]->no);
			foreach($serialData as $key => $res){
				$no = $key+1;
				$html.= '<tr class="InstContent">';
				$html.= '<td class="productSubNo">'.$no.'</td>';
				$html.= '<td class="productSub"  style="padding:50px !important;" >'.$res->productSubName.'</td>';
				//$html.= '<td class="productSubCunt">'.$res->count.'</td>';
				$html.= '<td class="serialNumber">'.$res->serial_number.'</td>';
				$html.= '</tr>';
			}

			$html.='</table>';
			$html .= '</body></html>';
			$pdf->writeHTMLCell(0, 0, 16, 60, $html, 0, 1, 0, true, 0, true);
			
		
	}
	public function DOC0014($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '10-10-2021';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		$pdf->Text(20, 97.5, $currentAddress);
		$pdf->Text(85, 150.5, $res[0]->rental_period);

		$pdf->Text(115, 177,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 185.7, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 131.3, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '22-10-2021';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0015/DOC0015('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(166, 50.5, $currentDate);

		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		#######  Serial number ######
		/*$serialStr = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);*/
		$pdf->Text(58, 84.5, $res[0]->product_name);

		####### Type of customer  #####
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
			//$pdf->Text(70, 137.5, $res[0]->office_name);
		}else{
			$cusType = 'นิติบุคคล';
			//$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);
		$pdf->Text(60, 111, $fulName);

		#######   Place of installation ########
		$inst = $installationLocation[0];
		$pdf->Text(67, 128.8, $inst->building_type);
		$pdf->Text(70, 137.5, $inst->company_name);
		$pdf->Text(150, 137.5, $inst->village_name);
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(31, 236.3, $fulName);
	}
	public function DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$istallation = $this->Contract_Model->GetInstallmentProductID($res[0]->contract_code);
		$istCount = count($istallation);
		$sumpayment = $istCount*$istallation[0]->installment_payment;
		$istRow = 24;
		$startRow = 0;
		
		$DateControlDoc =  '08-12-2021';
		
		if($istCount > $istRow ){
			$arrs = array(); 
			$countPage = ceil($istCount/24); #### หน้าละ 24 row ######
			
			for($p=1; $p <= $countPage; $p++) {
				$arrKey = array();
				for ($i = $startRow; $i < $p*$istRow ; $i++) { 
					if(!empty( $istallation[$i]->period)){
						$arrRes = array();    
						$payment_duedate = '';
						if(!empty($istallation[$i]->payment_duedate)){ $payment_duedate= date_format(date_create($istallation[$i]->payment_duedate),"d/m/Y");}
						
						$arrRes['period'] = (!empty( $istallation[$i]->period))? $istallation[$i]->period:'';
						$arrRes['inst_payment']= (!empty( $istallation[$i]->installment_payment))? $istallation[$i]->installment_payment:'' ;
						$arrRes['duedate']= $payment_duedate;
						array_push($arrKey, $arrRes);
					}
				}
				
				array_push($arrs, $arrKey);
				$startRow = ($istRow*$p);
			}
			foreach($arrs as $key =>  $item){
				
				$html = '<html><head>';
				$html .= '<style>
				.tableInstallment .InstHeader{
					background-color: #17a2b8 !important;
				}
				.tableInstallment .periodCol, .tableInstallment .InstContent .periodCol{
					text-align: center !important;
					width: 100px !important;
				}
				.tableInstallment .priceCol, .tableInstallment .InstContent .priceCol{
					text-align: right  !important;
					width: 180px !important;
				}
				.tableInstallment .dateCol, .tableInstallment .InstContent .dateCol{
					text-align: center  !important;
					width: 200px !important;
				}

				</style>';
				$html .= '</head><body>';
				$html .= '<br/><br/><br/>';

				$img_file;
				if($key == 0){
					$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p01.jpg');
				}else{
					$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p02.jpg');
				}
				
				$this->pdfHeaderPage($pdf, $img_file);
				$pdf->SetFont('thsarabun', '', 15, '', true);
				
				if($key == 0){
					$pdf->Text(168, 50.5, $istallation[0]->contract_code);
				}

				$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;" >';
				$html .= '<tr class="InstHeader">  ';
				$html .= '<th class="periodCol">งวด/เดือน</th> ';
				$html .= '<th class="priceCol">ค่าเช่า</th> ';
				$html .= '<th class="dateCol">วันที่ครบชำระ</th> ';
				//$html .= '<th>สถานะ</th>  ';
				$html .= '</tr>';

				foreach($item as $res){
					$inst_payments = number_format((int)$res['inst_payment']);
					$html.= '<tr class="InstContent">';
					$html.= '<td class="periodCol">'.$res['period'].'</td>';
					$html.= '<td class="priceCol">'.$inst_payments.' บาท</td>';
					$html.= '<td class="dateCol">'.$res['duedate'] .'</td>';
					//$html.= '<td  style"padding: .75rem;">'.$istallation[$i]->label.'</td>';
					$html.= '</tr>';
				}

				$html.='</table>';
				$html .= '</body></html>';
				
				//last page
				if(($countPage) == ($key+1)){
					$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
					$html .= '<tr class="">  ';
					$html .= '<th class="periodCol">รวม</th> ';
					$html .= '<th class="priceCol">'.number_format((int)$sumpayment).' บาท</th> ';
					$html .= '</tr>';
					$html.='</table>';
					//$html .= $sumpayment;
				}
				
				if($key == 0){
					$pdf->writeHTMLCell(0, 0, 20, 39, $html, 0, 1, 0, true, '', true);
				}else{
					$pdf->writeHTMLCell(0, 0, 20, 25, $html, 0, 1, 0, true, '', true);
				}
			}
		}else{
			
			$html = '<html><head>';
			$html .= '<style>

			.tableInstallment .InstHeader{
				background-color: #17a2b8 !important;
			}
			.tableInstallment .periodCol, .tableInstallment .InstContent .periodCol{
				text-align: center !important;
				width: 100px !important;
			}
			.tableInstallment .priceCol, .tableInstallment .InstContent .priceCol{
				text-align: right  !important;
				width: 180px !important;
			}
			.tableInstallment .dateCol, .tableInstallment .InstContent .dateCol{
				text-align: center  !important;
				width: 200px !important;
			}

			</style>';
			$html .= '</head><body id"tableInstallment">';
			$html .= '<br/><br/><br/>';

			$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p01.jpg');
			$this->pdfHeaderPage($pdf, $img_file);
			$pdf->SetFont('thsarabun', '', 15, '', true);
			$pdf->Text(168, 50.5, $res[0]->contract_code);

			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
			$html .= '<tr class="InstHeader">  ';
			$html .= '<th class="periodCol">งวด/เดือน</th> ';
			$html .= '<th class="priceCol">ค่าเช่า</th> ';
			$html .= '<th class="dateCol">วันที่ครบชำระ</th> ';
			$html .= '</tr>';

			foreach($istallation as $res){
				$duedates = '';
				if(!empty($res->payment_duedate)){ $duedates= date_format(date_create($res->payment_duedate),"d/m/Y");}
					
				$inst_paymented = number_format((int)$res->installment_payment);
				$html.= '<tr class="InstContent">';
				$html.= '<td class="periodCol">'.$res->period.'</td>';
				$html.= '<td class="priceCol">'.$inst_paymented.' บาท</td>';
				$html.= '<td class="dateCol">'.$duedates.'</td>';
				$html.= '</tr>';
			}

			$html.='</table>';
			$html .= '</body></html>';
			
			//last page
			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
			$html .= '<tr class="">  ';
			$html .= '<th class="periodCol">รวม</th> ';
			$html .= '<th class="priceCol">'.number_format((int)$sumpayment).' บาท</th> ';
			$html .= '</tr>';
			$html.='</table>';
			
			$pdf->writeHTMLCell(0, 0, 20, 39, $html, 0, 1, 0, true, '', true);
		}
	}
	public function DOC0018($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		$pdf->Text(20, 97.5, $currentAddress);
		$pdf->Text(105, 124, $res[0]->product_name);
		$pdf->Text(85, 159.5, $res[0]->rental_period);

		$pdf->Text(115, 186,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 194.7, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 131.3, $res[0]->firstname.' '.$res[0]->lastname);
	}
	



	public function Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/original/DOC0002/DOC0002('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(169, 50.5, $res[0]->contract_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		//######### Contract A2 ##########//

		$img_file = base_url('img/original/DOC0002/DOC0002('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_master_name.' : '.$item->serial_number : ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));


		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}
	public function ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/copy/DOC0002/DOC0002('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(169, 50.5, $res[0]->contract_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		

		//######### Contract A2 ##########//
		$img_file = base_url('img/copy/DOC0002/DOC0002('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_master_name.' : '.$item->serial_number : ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));

		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}
	public function Agreement($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/original/DOC0003/DOC0003('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		//######### Page 2 ##########//
		$img_file = base_url('img/original/DOC0003/DOC0003('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function AgreementCoppy($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/copy/DOC0003/DOC0003('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		//######### Page 2 ##########//
		$img_file = base_url('img/copy/DOC0003/DOC0003('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct, $agen)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/original/DOC0004/DOC0004('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}
	public function ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc , $subProduct, $agen)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/copy/DOC0004/DOC0004('.$DateControlDoc.')-01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}
	public function Supporter($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/original/DOC0009/DOC0009('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->contract_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}
	public function SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/copy/DOC0009/DOC0009('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->contract_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}

	
	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	}



	/*
	public function GetSubByProduct()
	{
		$product_id = $this->input->post('product_id');
		$data = $this->Temp_Model->GetSubByProduct($product_id);		
		echo json_encode($data);
	}
	
	public function getSparesByInstallId()
	{
		$id = $this->input->post('product_install_id');
		$data = $this->Temp_Model->getSparesByInstallId($id);
		echo json_encode($data);
	}

	function DateThai($strDate)
	{
		//$strYear = date("Y",strtotime($strDate))+543;
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		//$strHour= date("H",strtotime($strDate));
		//$strMinute= date("i",strtotime($strDate));
		//$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
	}
	*/

	public function pdfHeaderPage($pdf, $imgPath){
		$pdf->AddPage();
		$img_file = $imgPath;
		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->SetAutoPageBreak(TRUE, 0);
		// set the starting point for the page content
		$pdf->setPageMark();
	}
	
	public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			if(strpos($wording, $text) !== false){
				$temp = explode($text, $wording);
				$string = $temp[1];
			} else{
				$string = $wording;
			}
		}
		return $string ;
	}

	##########  convert int to thai text  ##########
	function PriceToText($amount_number)
	{
		$amount_number = number_format($amount_number, 2, ".","");
		$pt = strpos($amount_number , ".");
		$number = $fraction = "";
		if ($pt === false) 
			$number = $amount_number;
		else
		{
			$number = substr($amount_number, 0, $pt);
			$fraction = substr($amount_number, $pt + 1);
		}
		
		$ret = "";
		$baht = $this->ReadNumber ($number);
		if ($baht != "")
			$ret .= $baht . "บาท";
		
		$satang = $this->ReadNumber($fraction);
		if ($satang != "")
			$ret .=  $satang . "สตางค์";
		else 
			$ret .= "ถ้วน";
		return $ret;
	}
	function ReadNumber($number)
	{
		$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
		$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
		$number = $number + 0;
		$ret = "";
		if ($number == 0) return $ret;
		if ($number > 1000000)
		{
			$ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
			$number = intval(fmod($number, 1000000));
		}
		
		$divider = 100000;
		$pos = 0;
		while($number > 0)
		{
			$d = intval($number / $divider);
			$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
				((($divider == 10) && ($d == 1)) ? "" :
				((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
			$ret .= ($d ? $position_call[$pos] : "");
			$number = $number % $divider;
			$divider = $divider / 10;
			$pos++;
		}
		return $ret;
	}


	/////////////////////////////// External DB ////////////////////////////////////////////////
	public function ajaxTechn()
	{
		$technId = $this->input->post('val');
		$query = "select AgentCode,AgentName,AgentSurName from Agent where (AgentCode ='".$technId."')";
		$result = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
		
		if(sqlsrv_num_rows($result) > 0){
		
		  while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
			$ress = $row;
		  }
		}else{
		  $ress = false;
		}
		echo json_encode($ress);
	}
    
	private function connectSeminarDB(){
		$serverName = $this->db_config['production']['servername']; //serverName\instanceName
		$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
		$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);
		
		if(!$this->seminar_conn){
			echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
		}
	}
	public function closeConnectSeminarDB(){
		sqlsrv_close( $this->seminar_conn );
	}
	///////////////////////////////////////////////////////////////////////////////
    
}
