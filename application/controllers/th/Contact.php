<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->library('session');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('th/Contact_Model');
		$this->load->model('admin/Status_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file');
    } 

	
	public function index($id = null)
	{
		$menu['menu'] = 'contact';
		$data['product_name'] = $id;
        $this->load->view('th/header',$menu);
		$this->load->view('th/contact',$data);
        $this->load->view('th/footer');
	}

	public function intereste($id = null)
	{
		$menu['menu'] = 'contact';
		$data['product_name'] = $id;
        $this->load->view('th/header',$menu);
		$this->load->view('th/contact',$data);
        $this->load->view('th/footer');
	}

	public function sendContact()
	{
		$resStatus = $this->Status_Model->requestStatus('210604');
		$status = '';
		foreach($resStatus as $item){
			if($item->status_code == 0){
				$status = $item->id;
			}
		}
		$data = array( 
			'name' =>  $this->input->post('name'), 
            'sname' => $this->input->post('sname'), 
            'tel' => $this->input->post('tel'), 
			'email' => $this->input->post('email'),
			'topic' => $this->input->post('topic'),
			'message' => $this->input->post('message'),
			'status' => $status,
            'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         );
		 
		 $res = $this->Contact_Model->insert($data); 
		 if($res==true)
		 {
			$this->session->set_userdata('contact', true);
		 }
		 redirect('th/contact');
	}













}
