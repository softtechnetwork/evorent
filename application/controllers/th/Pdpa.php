<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdpa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Customer_Model');
		$this->load->model('admin/Premise_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Logs_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function privacypolicy()
	{
		//$data['res_customer'] = $this->Customer_Model->select($type, $val);
		//$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$menu['menu'] = '';

        $this->load->view('th/header',$menu);
		$this->load->view('th/privacypolicy');
        $this->load->view('th/footer');
	}
    public function termsservice()
	{
		//$data['res_customer'] = $this->Customer_Model->select($type, $val);
		//$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$menu['menu'] = '';

        $this->load->view('th/header',$menu);
		$this->load->view('th/termsservice');
        $this->load->view('th/footer');
	}














}
