<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('th/Member_Model');
        $this->load->model('admin/Customer_Model');
        $this->load->model('admin/Temp_Model');
        $this->load->model('admin/Contract_Model');
        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Status_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function index()
	{
        /*
        $data['resTemp'] = $this->Loan_Model->getTempOne($id);
		$data['resInst'] = $this->Loan_Model->getInstOne($id);
		$data['resStatus'] = $this->Status_Model->requestStatus('210603');
        */
		$menu['menu'] = 'member';

        $this->load->view('th/header',$menu);
		$this->load->view('th/member');
        $this->load->view('th/footer');
	}

    public function getRes()
	{
		$Search = $this->input->post('Search');	
		$data['res'] = $this->Member_Model->getRes($Search);

		if($data['res']){
			// $data['contract'] = $this->Temp_Model->selectOneDeatil($data['res'][0]->contract_code);
			// $data['address2'] = $this->Temp_Model->detailAddress($data['contract'][0]->customer_code,'ที่อยู่ปัจจุบัน');
			// $data['broker'] = $this->Temp_Model->CharngToPDF($data['contract'][0]->sale_code);
			// $data['agent'] = $this->Temp_Model->CharngToPDF($data['contract'][0]->techn_code);
			// $data['installationLocation'] = $this->Temp_Model->detailInstallationLocation($data['contract'][0]->installationlocation);

			$data['contract'] = $this->Contract_Model->selectOneDeatil($data['res'][0]->contract_code);
			$data['address2'] = $this->Contract_Model->detailAddress($data['contract'][0]->customer_code,'ที่อยู่ปัจจุบัน');
			$data['broker'] = $this->Contract_Model->CharngToPDF($data['contract'][0]->sale_code);
			$data['agent'] = $this->Contract_Model->CharngToPDF($data['contract'][0]->techn_code);
			$data['installationLocation'] = $this->Contract_Model->detailInstallationLocation($data['contract'][0]->installationlocation);

			if($data['contract'][0]->supporter == 1){
				$data['supporter'] = $this->Contract_Model->selectSupporter($data['contract'][0]->contract_code);
			}else{
				$data['supporter'] = null;
			}
		}
		/*
		$data['subProduct'] = $this->Temp_Model->SubByProductId($res[0]->temp_code, $res[0]->product_id);

		$data['address1'] = $this->Temp_Model->detailAddress($res[0]->customer_code, 'ที่อยู่ตามบัตรประชาชน');
		$data['address2'] = $this->Temp_Model->detailAddress($res[0]->customer_code,'ที่อยู่ปัจจุบัน');
		$data['address3'] = $this->Temp_Model->detailAddress($res[0]->customer_code,'ที่อยู่สถานที่ทำงาน');
		
		
		*/
		//$data = $this->Member_Model->getRes($Search);		
		echo json_encode($data);
	}














}
