<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Customer_Model');
		$this->load->model('admin/Premise_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Logs_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function index(){
        $menu['menu'] = 'product';

        $this->load->view('th/header',$menu);
		$this->load->view('th/product');
        $this->load->view('th/footer');
	}
	public function get_product(){ 
        $curent_date = date('Y-m-d H:i:s');
		$Query = " SELECT web_product.*, img.path  ";
		$Query .= " FROM web_product  ";
		$Query .= " INNER JOIN img ON web_product.product_set_id = img.ref_id   ";
		$Query .= " WHERE web_product.display_status = '1' and img.master_cate = 'product' and img_cate_id = '1'  ";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
    }
	public function detail($id = null){
		// $data['res'] = $this->getproductById($id);
		// $data['productRelate'] = $this->getproductRelateById($id, $data['res']['product_cate']);
		// $data['productImg'] = $this->getproductImgDetailById($id);
		$data['product_id'] = $id;

		$menu['menu'] = 'product';

        $this->load->view('th/header',$menu);
		$this->load->view('th/detail', $data);
        $this->load->view('th/footer');
	}
	public function get_product_once(){ 
		$product_id = $this->input->post('product_id');
        $curent_date = date('Y-m-d H:i:s');
		$Query = " SELECT web_product.*
			FROM web_product 
			WHERE web_product.product_set_id ='".$product_id."' and web_product.display_status = '1' 
		";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
    }
	public function get_product_image(){ 
		$product_id = $this->input->post('product_id');
        $curent_date = date('Y-m-d H:i:s');
		$Query = " SELECT web_product.*, img.path
			FROM web_product
			INNER JOIN img ON web_product.product_set_id = img.ref_id 
			WHERE web_product.product_set_id ='".$product_id."' and web_product.display_status = '1' and img.master_cate = 'product' and img_cate_id = '3'
		";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
    }
	public function get_product_relate(){ 
		$product_id = $this->input->post('product_id');
        $curent_date = date('Y-m-d H:i:s');

		$Resproduct = $this->db->query("SELECT * FROM product_set WHERE product_id ='".$product_id."'");
		$dataproduct = $Resproduct->result();

		$Query = "SELECT web_product.* , img.path
		FROM product_set
		INNER JOIN web_product ON product_set.product_id = web_product.product_set_id
		INNER JOIN img ON web_product.product_set_id = img.ref_id and web_product.display_status = '1' and img.master_cate = 'product' and img_cate_id = '1'
		WHERE product_set.product_cate ='".$dataproduct[0]->product_cate."'  and product_set.product_id != '".$dataproduct[0]->product_id."' ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
    }


	//### recommend products ###//
	
	public function get_recommend_products(){ 
        $curent_date = date('Y-m-d H:i:s');
		$Query = " SELECT web_product.*, img.path  
		FROM web_product  
		INNER JOIN img ON web_product.product_set_id = img.ref_id  
		WHERE web_product.display_status = '1' and img.master_cate = 'product' and img_cate_id = '2'  ";
		
		$Res= $this->db->query($Query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
    }



	##############  Product System ######################
	public function getproductById($id = null){
		$arr = null;
		$products = $this->products();
		foreach($products as $item){
			if($item['product_code'] == $id){
				$arr = $item;
			};
		}
		return $arr;
	}
	public function getproductRelateById($id = null, $cateid = null){
		$arr = array();
		$products = $this->products();
		foreach($products as $item){
			if($item['product_cate'] == $cateid && $item['product_code'] != $id){
				array_push($arr, $item);
			};
		}
		return $arr;
	}
	public function getproductImgDetailById($id = null){
		$arr = array();
		$img = $this->productImgDetail();
		
		foreach($img as $item){
			if($item['product_code'] == $id){
				array_push($arr, $item);
			};
		}

		####  จัดกรุ๊ป  #####
		$numGroup = 1;
		$round = 1;
		$max = 3;

		$res = array();
		$ressub = array();
		foreach($arr as $item){
			
			if($round <= $max){
				array_push($ressub, $item);
				$res[$numGroup] = $ressub;
			}

			if( $round == $max){
				$numGroup++;
				$round = 1;
				$ressub = array();
			}
			$round++;
		}
		return $res;
	}

	public function products(){
		$arr = array(
			0=> array(
				'id'=>'1',
				'product_code'=>'P001', // format P YY MM 001
				'product_cate'=>'C0001',
				'name'=>'เครื่องแยกน้ำ WE',
				'img'=>'shop_we.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_we.jpg',
				'detail'=>'เทคโนโลยี Ware Extractor ทำให้สามารถกรองอนุภาคที่มีขนาดเล็กกว่าเส้นผม ได้ถึง 0.0001 ไมครอน, กรองน้ำได้มากลดการซ่อมบำรุง ไส้กรองทนทาน ใช้งานได้นาน ช่วยประหยัดค่าใช้จ่าย'
			),
			1=> array(
				'id'=>'2',
				'product_code'=>'P002', // format P YY MM 001
				'product_cate'=>'C0001',
				'name'=>'เครื่องแยกน้ำ WE2',
				'img'=>'shop_we2.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_we2.jpg',
				'detail'=>'เทคโนโลยี Ware Extractor ทำให้สามารถกรองอนุภาคที่มีขนาดเล็กกว่าเส้นผม ได้ถึง 0.0001 ไมครอน, กำจัดตะกอนหินปูน แบคทีเรีย และโลหะหนักได้อย่างมีประสิทธิภาพ แบคทีเรีย 0.4~5 MM  ไวรัส 0.01~0.1μm  การฆ่าเชื้อ 0.001~0.01 MM  โลหะหนัก 0.0005~0.005 MM, แสดงค่าคุณภาพน้ำ เข้า/ออก, แยกน้ำสะอาด 64 ลูกบาศร์เซนติเมตร'
			),
			2=> array(
				'id'=>'3',
				'product_code'=>'P004', // format P YY MM 001
				'product_cate'=>'C0003',
				'name'=>'Air Inverter I10',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'Golden Fin ผิวเคลือบสารสีทอง เพิ่มประสิทธิภาพไหลเวียนของสารทำความเย็น และการระบายความร้อน, Inner Grooved Copper Tube ท่อทงแดง 100 เปอร์เซ็นต์ แบบเกลียว ทำให้การแลกเปลี่ยนความร้อนของน้ำยาแอร์ มีประสิทธิภาพมากขึ้น, Ice Clean Function ช่วยขจัดสิ่งสกปรก เชื้อรา และแบคทีเรีย'
			),
			3=> array(
				'id'=>'4',
				'product_code'=>'P005', // format P YY MM 001
				'product_cate'=>'C0003',
				'name'=>'Air Inverter I13',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'Golden Fin ผิวเคลือบสารสีทอง เพิ่มประสิทธิภาพไหลเวียนของสารทำความเย็น และการระบายความร้อน, Inner Grooved Copper Tube ท่อทงแดง 100 เปอร์เซ็นต์ แบบเกลียว ทำให้การแลกเปลี่ยนความร้อนของน้ำยาแอร์ มีประสิทธิภาพมากขึ้น, Ice Clean Function ช่วยขจัดสิ่งสกปรก เชื้อรา และแบคทีเรีย'
			),
			4=> array(
				'id'=>'5',
				'product_code'=>'P006', // format P YY MM 001
				'product_cate'=>'C0003',
				'name'=>'Air Inverter I18',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'Golden Fin ผิวเคลือบสารสีทอง เพิ่มประสิทธิภาพไหลเวียนของสารทำความเย็น และการระบายความร้อน, Inner Grooved Copper Tube ท่อทงแดง 100 เปอร์เซ็นต์ แบบเกลียว ทำให้การแลกเปลี่ยนความร้อนของน้ำยาแอร์ มีประสิทธิภาพมากขึ้น, Ice Clean Function ช่วยขจัดสิ่งสกปรก เชื้อรา และแบคทีเรีย'
			),
			5=> array(
				'id'=>'6',
				'product_code'=>'P007', // format P YY MM 001
				'product_cate'=>'C0003',
				'name'=>'Air Inverter I24',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'Golden Fin ผิวเคลือบสารสีทอง เพิ่มประสิทธิภาพไหลเวียนของสารทำความเย็น และการระบายความร้อน, Inner Grooved Copper Tube ท่อทงแดง 100 เปอร์เซ็นต์ แบบเกลียว ทำให้การแลกเปลี่ยนความร้อนของน้ำยาแอร์ มีประสิทธิภาพมากขึ้น, Ice Clean Function ช่วยขจัดสิ่งสกปรก เชื้อรา และแบคทีเรีย'
			),
			6=> array(
				'id'=>'7',
				'product_code'=>'P008', // format P YY MM 001
				'product_cate'=>'C0002',
				'name'=>'Air Fix Speed A10',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'ระบบ Self Cleaning Function ระบบที่ช่วยดึงฝุ่นที่เกาะอยู่ที่คอยล์เย็นออกมา ภายในระยะเวลาเพียง 20 นาที, มั่นใจในคุณภาพประหยัดไฟเบอร์ 5 ผ่านการทดสอบจากการไฟฟ้าฝ่ายผลิต รับประกัน 5 ปี, NFC แผ่นกรองอากาศมาตรฐานยุโรป ดักจับสิ่งแปลกปลอมที่มีขนาดเล็กในอากาศ แบคทีเรีย และไวรัสขนาด 0.1 ไมครอน ได้อย่างมีประสิทธิภาพ เพื่ออากาศที่บริสุทธิ์ในห้องของคุณ'
			),
			7=> array(
				'id'=>'8',
				'product_code'=>'P009', // format P YY MM 001
				'product_cate'=>'C0002',
				'name'=>'Air Fix Speed A13',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'ระบบ Self Cleaning Function ระบบที่ช่วยดึงฝุ่นที่เกาะอยู่ที่คอยล์เย็นออกมา ภายในระยะเวลาเพียง 20 นาที, มั่นใจในคุณภาพประหยัดไฟเบอร์ 5 ผ่านการทดสอบจากการไฟฟ้าฝ่ายผลิต รับประกัน 5 ปี, NFC แผ่นกรองอากาศมาตรฐานยุโรป ดักจับสิ่งแปลกปลอมที่มีขนาดเล็กในอากาศ แบคทีเรีย และไวรัสขนาด 0.1 ไมครอน ได้อย่างมีประสิทธิภาพ เพื่ออากาศที่บริสุทธิ์ในห้องของคุณ'
			),
			8=> array(
				'id'=>'9',
				'product_code'=>'P010', // format P YY MM 001
				'product_cate'=>'C0002',
				'name'=>'Air Fix Speed A18',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'ระบบ Self Cleaning Function ระบบที่ช่วยดึงฝุ่นที่เกาะอยู่ที่คอยล์เย็นออกมา ภายในระยะเวลาเพียง 20 นาที, มั่นใจในคุณภาพประหยัดไฟเบอร์ 5 ผ่านการทดสอบจากการไฟฟ้าฝ่ายผลิต รับประกัน 5 ปี, NFC แผ่นกรองอากาศมาตรฐานยุโรป ดักจับสิ่งแปลกปลอมที่มีขนาดเล็กในอากาศ แบคทีเรีย และไวรัสขนาด 0.1 ไมครอน ได้อย่างมีประสิทธิภาพ เพื่ออากาศที่บริสุทธิ์ในห้องของคุณ'
			),
			9=> array(
				'id'=>'10',
				'product_code'=>'P011', // format P YY MM 001
				'product_cate'=>'C0002',
				'name'=>'Air Fix Speed A24',
				'img'=>'shop_air.jpg',
				'imgpath'=>'./uploaded/evorentweb/shop_air.jpg',
				'detail'=>'ระบบ Self Cleaning Function ระบบที่ช่วยดึงฝุ่นที่เกาะอยู่ที่คอยล์เย็นออกมา ภายในระยะเวลาเพียง 20 นาที, มั่นใจในคุณภาพประหยัดไฟเบอร์ 5 ผ่านการทดสอบจากการไฟฟ้าฝ่ายผลิต รับประกัน 5 ปี, NFC แผ่นกรองอากาศมาตรฐานยุโรป ดักจับสิ่งแปลกปลอมที่มีขนาดเล็กในอากาศ แบคทีเรีย และไวรัสขนาด 0.1 ไมครอน ได้อย่างมีประสิทธิภาพ เพื่ออากาศที่บริสุทธิ์ในห้องของคุณ'
			),

			######### Solacell ###########
			10=> array(
				'id'=>'11',
				'product_code'=>'P012', // format P YY MM 001
				'product_cate'=>'C0004',
				'name'=>'Hybrid inverter single phase P16',
				'img'=>'p16-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/p16-600x800px.jpg',
				'detail'=>''
			),
			11=> array(
				'id'=>'12',
				'product_code'=>'P013', // format P YY MM 001
				'product_cate'=>'C0004',
				'name'=>'Hybrid inverter single phase P33',
				'img'=>'p33-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/p33-600x800px.jpg',
				'detail'=>''
			),
			12=> array(
				'id'=>'13',
				'product_code'=>'P014', // format P YY MM 001
				'product_cate'=>'C0004',
				'name'=>'Hybrid inverter single phase P50',
				'img'=>'p50-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/p50-600x800px.jpg',
				'detail'=>''
			),

			######### Solapump ###########
			13=> array(
				'id'=>'14',
				'product_code'=>'P015', // format P YY MM 001
				'product_cate'=>'C0005',
				'name'=>'Solapump C5',
				'img'=>'c5-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/c5-600x800px.jpg',
				'detail'=>'ป้องกันฝุ่นและน้ำมาตรฐาน IP65
				, บอกค่าแรงดันกระแสไฟฟ้าผ่านหน้าจอ LCD
				, มีระบบชาร์จไฟอัตโนมัติ
				, มีระบบสั่งการทำงาน เริ่มต้น-หยุดการทำงาน อัตโนมัติ
				, มีเทคโนโลยีการป้องกัน แรงดันไฟสูง-ต่ำ กระแสไฟฟ้าเกิน อุณหภูมิสูง
				, ใช้งานได้หลากหลาย เหมาะกับพื้นที่ที่ไม่สามารถหาไฟฟ้าหรือน้ำมันได้'
			),
			14=> array(
				'id'=>'15',
				'product_code'=>'P016', // format P YY MM 001
				'product_cate'=>'C0005',
				'name'=>'Solapump C7',
				'img'=>'c7-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/c7-600x800px.jpg',
				'detail'=>'ป้องกันฝุ่นและน้ำมาตรฐาน IP65
				, บอกค่าแรงดันกระแสไฟฟ้าผ่านหน้าจอ LCD
				, มีระบบชาร์จไฟอัตโนมัติ
				, มีระบบสั่งการทำงาน เริ่มต้น-หยุดการทำงาน อัตโนมัติ
				, มีเทคโนโลยีการป้องกัน แรงดันไฟสูง-ต่ำ กระแสไฟฟ้าเกิน อุณหภูมิสูง
				, ใช้งานได้หลากหลาย เหมาะกับพื้นที่ที่ไม่สามารถหาไฟฟ้าหรือน้ำมันได้'
			),

			
			15=> array(
				'id'=>'16',
				'product_code'=>'P017', // format P YY MM 001
				'product_cate'=>'C0005',
				'name'=>'Solapump S7',
				'img'=>'S7-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/s7-600x800px.jpg',
				'detail'=>''
			),
			16=> array(
				'id'=>'17',
				'product_code'=>'P018', // format P YY MM 001
				'product_cate'=>'C0005',
				'name'=>'Solapump S11',
				'img'=>'S11-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/s11-600x800px.jpg',
				'detail'=>''
			),
			17=> array(
				'id'=>'18',
				'product_code'=>'P019', // format P YY MM 001
				'product_cate'=>'C0005',
				'name'=>'Solapump S15',
				'img'=>'S15-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/s15-600x800px.jpg',
				'detail'=>''
			),

			###  Master TV  ####
			18=> array(
				'id'=>'19',
				'product_code'=>'P020', // format P YY MM 001
				'product_cate'=>'C0006',
				'name'=>'Master TV',
				'img'=>'mastertv600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/mastertv600x800px.jpg',
				'detail'=>''
			),
			
			###  Easy plug  ####
			19=> array(
				'id'=>'20',
				'product_code'=>'P021', // format P YY MM 001
				'product_cate'=>'C0007',
				'name'=>'Easy plug',
				'img'=>'easyplug-600x800px.jpg',
				'imgpath'=>'./uploaded/evorentweb/easyplug-600x800px.jpg',
				'detail'=>''
			),


		);
		return $arr;
	}

	public function productCate(){
		$arr = array(
			0=> array(
				'id'=>'1',
				'product_cate'=>'C0001',// format C 0001
				'name'=>'เครื่องแยกน้ำ',
				'detail'=>'เครื่องแยกน้ำ'
			),
			1=> array(
				'id'=>'2',
				'product_cate'=>'C0002',
				'name'=>'แอร์ Standard',
				'detail'=>'แอร์ Standard'
			),
			2=> array(
				'id'=>'3',
				'product_cate'=>'C0003',
				'name'=>'แอร์ Inverter',
				'detail'=>'แอร์ Inverter'
			),
			
			3=> array(
				'id'=>'4',
				'product_cate'=>'C0004',
				'name'=>'โซล่าเซลล์',
				'detail'=>'โซล่าเซลล์'
			),
			
			4=> array(
				'id'=>'5',
				'product_cate'=>'C0005',
				'name'=>'โซล่าปั๊ม',
				'detail'=>'โซล่าปั๊ม'
			),
			5=> array(
				'id'=>'6',
				'product_cate'=>'C0006',
				'name'=>'MASTER TV',
				'detail'=>'MASTER TV'
			),
			6=> array(
				'id'=>'7',
				'product_cate'=>'C0007',
				'name'=>'EASY PLUG',
				'detail'=>'EASY PLUG'
			)
		);
		return $arr;
	}

	public function productImgDetail(){
		$arr = array(
			0=> array(
				'id'=>'1',
				'product_code'=>'P001', // format P YY MM 001
				'name'=>'product_single_we_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we_01.jpg',
			),
			1=> array(
				'id'=>'2',
				'product_code'=>'P001', // format P YY MM 001
				'name'=>'product_single_we_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we_02.jpg',
			),
			/*2=> array(
				'id'=>'3',
				'product_code'=>'P001', // format P YY MM 001
				'name'=>'product_single_we_03.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we_03.jpg'
			),*/
			3=> array(
				'id'=>'4',
				'product_code'=>'P002', // format P YY MM 001
				'name'=>'product_single_we2_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we2_01.jpg',
			),
			4=> array(
				'id'=>'5',
				'product_code'=>'P002', // format P YY MM 001
				'name'=>'product_single_we2_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we2_02.jpg',
			),
			5=> array(
				'id'=>'6',
				'product_code'=>'P002', // format P YY MM 001
				'name'=>'product_single_we2_03.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we2_03.jpg',
			),
			6=> array(
				'id'=>'7',
				'product_code'=>'P002', // format P YY MM 001
				'name'=>'product_single_we2_04.jpg',
				'path'=>'./uploaded/evorentweb/product_single_we2_04.jpg',
			),

			
			7=> array(
				'id'=>'8',
				'product_code'=>'P003', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*8=> array(
				'id'=>'9',
				'product_code'=>'P003', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			9=> array(
				'id'=>'10',
				'product_code'=>'P004', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*10=> array(
				'id'=>'11',
				'product_code'=>'P004', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			11=> array(
				'id'=>'10',
				'product_code'=>'P005', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*12=> array(
				'id'=>'11',
				'product_code'=>'P005', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			13=> array(
				'id'=>'12',
				'product_code'=>'P006', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*14=> array(
				'id'=>'13',
				'product_code'=>'P006', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			15=> array(
				'id'=>'14',
				'product_code'=>'P007', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*16=> array(
				'id'=>'15',
				'product_code'=>'P007', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			17=> array(
				'id'=>'16',
				'product_code'=>'P008', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*18=> array(
				'id'=>'17',
				'product_code'=>'P008', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			19=> array(
				'id'=>'18',
				'product_code'=>'P009', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*20=> array(
				'id'=>'19',
				'product_code'=>'P009', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			21=> array(
				'id'=>'20',
				'product_code'=>'P010', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*22=> array(
				'id'=>'21',
				'product_code'=>'P010', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			23=> array(
				'id'=>'22',
				'product_code'=>'P011', // format P YY MM 001
				'name'=>'product_single_air.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air.jpg',
			),
			/*24=> array(
				'id'=>'23',
				'product_code'=>'P011', // format P YY MM 001
				'name'=>'product_single_air_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_air_02.jpg',
			),*/

			########## Solacell img ##########
			25=> array(
				'id'=>'24',
				'product_code'=>'P012', // format P YY MM 001
				'name'=>'product_single_p16.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p16.jpg',
			),
			26=> array(
				'id'=>'25',
				'product_code'=>'P012', // format P YY MM 001
				'name'=>'product_single_p16_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p16_01.jpg',
			),

			27=> array(
				'id'=>'26',
				'product_code'=>'P013', // format P YY MM 001
				'name'=>'product_single_p33.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p33.jpg',
			),
			28=> array(
				'id'=>'27',
				'product_code'=>'P013', // format P YY MM 001
				'name'=>'product_single_p33_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p33_01.jpg',
			),
			
			29=> array(
				'id'=>'28',
				'product_code'=>'P014', // format P YY MM 001
				'name'=>'product_single_p50.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p50.jpg',
			),


			/*30=> array(
				'id'=>'29',
				'product_code'=>'P012', // format P YY MM 001
				'name'=>'product_single_p16_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p16_02.jpg',
			),
			31=> array(
				'id'=>'30',
				'product_code'=>'P013', // format P YY MM 001
				'name'=>'product_single_p33_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p33_02.jpg',
			),
			32=> array(
				'id'=>'31',
				'product_code'=>'P014', // format P YY MM 001
				'name'=>'product_single_p50_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_p50_01.jpg',
			),*/


			########## Solapump img ##########
			33=> array(
				'id'=>'32',
				'product_code'=>'P015', // format P YY MM 001
				'name'=>'product_single_c5_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c5_01.jpg',
			),
			34=> array(
				'id'=>'33',
				'product_code'=>'P015', // format P YY MM 001
				'name'=>'product_single_c5_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c5_02.jpg',
			),
			35=> array(
				'id'=>'34',
				'product_code'=>'P015', // format P YY MM 001
				'name'=>'product_single_c5_c7_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c5_c7_02.jpg',
			),

			36=> array(
				'id'=>'35',
				'product_code'=>'P016', // format P YY MM 001
				'name'=>'product_single_c7_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c7_01.jpg',
			),
			37=> array(
				'id'=>'36',
				'product_code'=>'P016', // format P YY MM 001
				'name'=>'product_single_c7_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c7_02.jpg',
			),
			38=> array(
				'id'=>'37',
				'product_code'=>'P016', // format P YY MM 001
				'name'=>'product_single_c5_c7_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_c5_c7_02.jpg',
			),

			
			39=> array(
				'id'=>'38',
				'product_code'=>'P017', // format P YY MM 001
				'name'=>'product_single_s7.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s7.jpg',
			),
			40=> array(
				'id'=>'39',
				'product_code'=>'P017', // format P YY MM 001
				'name'=>'product_single_s7_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s7_01.jpg',
			),
			
			41=> array(
				'id'=>'40',
				'product_code'=>'P018', // format P YY MM 001
				'name'=>'product_single_s11.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s11.jpg',
			),
			43=> array(
				'id'=>'41',
				'product_code'=>'P018', // format P YY MM 001
				'name'=>'product_single_s11_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s11_01.jpg',
			),

			44=> array(
				'id'=>'42',
				'product_code'=>'P019', // format P YY MM 001
				'name'=>'product_single_s15.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s15.jpg',
			),
			45=> array(
				'id'=>'43',
				'product_code'=>'P019', // format P YY MM 001
				'name'=>'product_single_s15_01.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s15_01.jpg',
			),

			
			46=> array(
				'id'=>'44',
				'product_code'=>'P017', // format P YY MM 001
				'name'=>'product_single_s7_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s7_02.jpg',
			),
			
			47=> array(
				'id'=>'45',
				'product_code'=>'P018', // format P YY MM 001
				'name'=>'product_single_s11_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s11_02.jpg',
			),
			
			48=> array(
				'id'=>'46',
				'product_code'=>'P019', // format P YY MM 001
				'name'=>'product_single_s15_02.jpg',
				'path'=>'./uploaded/evorentweb/product_single_s15_02.jpg',
			),
			###  Master TV  ####
			49=> array(
				'id'=>'47',
				'product_code'=>'P020', // format P YY MM 001
				'name'=>'mastertv_01.jpg',
				'path'=>'./uploaded/evorentweb/mastertv_01.jpg',
			),
			50=> array(
				'id'=>'48',
				'product_code'=>'P020', // format P YY MM 001
				'name'=>'mastertv_02.jpg',
				'path'=>'./uploaded/evorentweb/mastertv_02.jpg',
			),
			51=> array(
				'id'=>'49',
				'product_code'=>'P020', // format P YY MM 001
				'name'=>'mastertv_03.jpg',
				'path'=>'./uploaded/evorentweb/mastertv_03.jpg',
			),

			###  Easy plug  ####
			52=> array(
				'id'=>'50',
				'product_code'=>'P021', // format P YY MM 001
				'name'=>'easyplug_01.jpg',
				'path'=>'./uploaded/evorentweb/easyplug_01.jpg',
			),
			53=> array(
				'id'=>'51',
				'product_code'=>'P021', // format P YY MM 001
				'name'=>'easyplug_02.jpg',
				'path'=>'./uploaded/evorentweb/easyplug_02.jpg',
			),
		);
		return $arr;
	}














}
