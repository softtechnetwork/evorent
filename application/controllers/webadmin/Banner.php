<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		// login     
		/*if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}*/

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('webadmin/Banner_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function index()
	{
		/*$type = $this->input->post('cusSearch-type');
		$cusSearch = $this->input->post('cusSearch');
		$cusStatus = $this->input->post('cusStatus');
		$val = null;
		if($type != ''){
			switch($type){
				case 'status':$val = $cusStatus; break;
				default: $val = $cusSearch; break;
			}
		}

		$data['res_customer'] = $this->Customer_Model->select($type, $val);
		$data['resStatus'] = $this->Status_Model->requestStatus('Customer');*/
		
        $this->load->view('webadmin/header');
		$this->load->view('webadmin/banner_list');
        $this->load->view('webadmin/footer');
	}
	public function create()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$data['province'] = $this->Customer_Model->province();
		//$data['amphurs'] = $this->Customer_Model->amphurs();
		//$data['district'] = $this->Customer_Model->district();
		$data['monthlyIncome'] = $this->monthlyIncome();
		$data['career'] = $this->career();

        $this->load->view('admin/header');
		$this->load->view('admin/customer_create',$data);
        $this->load->view('admin/footer');
	}

	public function edit($id = null)
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$data['res_customer'] = $this->Customer_Model->selectOne($id);
		//$data['province'] = $this->Customer_Model->province();
		//$data['amphurs'] = $this->Customer_Model->amphurs();
		//$data['district'] = $this->Customer_Model->district();
		
		$data['inhabited'] = $this->Customer_Model->getInhabited($id);
		$data['monthlyIncome'] = $this->monthlyIncome();
		$data['career'] = $this->career();
		
        $this->load->view('admin/header');
		$this->load->view('admin/customer_edit',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		
		$prefixdate = date("ym");
		$prefixchar = 'M';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res_customer = $this->Customer_Model->selectOne($prefixdate);
		if($res_customer){
			foreach($res_customer as $items){
				array_push($idArr, (int)substr($items->customer_code,5,9));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		
		$data = array( 
			'customer_code' => $code, 
			'idcard' =>  implode(preg_split('/[-]/',  $this->input->post('idcard'))),
            'firstname' => $this->input->post('name'), 
            'lastname' => $this->input->post('sname'), 
			//'addr' => $this->input->post('address'), 
			//'province_id' => $this->input->post('province'), 
			//'amphurs_id' => $this->input->post('amphurs'), 
			//'tumbon_id' => $this->input->post('district'), 
			//'zip_code' => $this->input->post('zipcode'), 
			//'birthday' => $this->input->post('bdate'), 
			//'office_name' => $this->input->post('name-office'),
			'type'=> 'person',
			'career' => $this->input->post('career-select'),
			'career_text' => $this->input->post('career'),
			'monthly_income' => $this->input->post('monthly-income'),
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			'sex' => $this->input->post('gender'),
			'status' => $this->input->post('status'),
			'remark' => $this->input->post('remark'),
            'created'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         ); 
		 
		$countArr = [];
		$ihb_code = '';
		$res_inhabited = $this->Customer_Model->getIhbByCode($prefixdate);
		if($res_inhabited){
			foreach($res_inhabited as $items){
				array_push($countArr, (int)substr($items->inhabited_code,5,9));
			}
			$ihb_code = MAX($countArr)+1;
		}else{
			$ihb_code = 1;
		}
		 $Inhabited = array( 
			'inhabited_code' => sprintf('A'.$prefixdate.'%05d',$ihb_code), 
			'customer_code' => $data['customer_code'],
			'category' => 'ที่อยู่ตามบัตรประชาชน',
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'),
            'cdate'=> $data['created']
         );  
		 $InhabitedCurrent = array( 
			'inhabited_code' => sprintf('A'.$prefixdate.'%05d',$ihb_code+1), 
			'customer_code' => $data['customer_code'],
			'category' => 'ที่อยู่ปัจจุบัน',
			'address' => $this->input->post('address-current'), 
			'province_id' => $this->input->post('province-current'), 
			'amphurs_id' => $this->input->post('amphurs-current'), 
			'district_id' => $this->input->post('district-current'), 
			'zip_code' => $this->input->post('zipcode-current'),
            'cdate'=> $data['created']
         );  

		$data['addr'] = $Inhabited['inhabited_code'];

		 $bdate = $this->input->post('bdate');
		 if($bdate != '' && $bdate != null){
			$data['birthday'] = date(date_format(DateTime::createFromFormat('d/m/Y', $bdate),"Y-m-d"));
		 }

		 // สถานี่ทำงาน
		 $officeName = $this->input->post('name-office');
		 $InhabitedOffice = null;
		 if($officeName != '' && $officeName != null){
			$data['office_name'] = $officeName;
			$data['phone'] = $this->input->post('tel-office');
			$InhabitedOffice = array( 
				'inhabited_code' => sprintf('A'.$prefixdate.'%05d',$ihb_code+2), 
				'customer_code' => $data['customer_code'],
				'category' => 'ที่อยู่สถานที่ทำงาน',
				'address' => $this->input->post('address-office'), 
				'province_id' => $this->input->post('province-office'), 
				'amphurs_id' => $this->input->post('amphurs-office'), 
				'district_id' => $this->input->post('district-office'), 
				'zip_code' => $this->input->post('zipcode-office'),
				//'tel' => $this->input->post('tel-office'),
				'cdate'=> $data['created']
			); 
		}
		

		$this->Customer_Model->insert($data); 
		$this->Customer_Model->insertInhabitd($Inhabited); 
		$this->Customer_Model->insertInhabitd($InhabitedCurrent); 
		if($officeName != '' && $officeName != null){
			$this->Customer_Model->insertInhabitd($InhabitedOffice); 
		}

        redirect('admin/customer');
    }

	public function update(){

		$customer_code = $this->input->post('customer_code');
		$premise_code = $this->input->post('premise_code');
        $data = array( 
            //'img' => $img, 
			//'idcard' => $this->input->post('idcard'), 
            'firstname' => $this->input->post('name'), 
            'lastname' => $this->input->post('sname'), 
			//'addr' => $this->input->post('address'),
			'career' => $this->input->post('career-select'),
			'career_text' => $this->input->post('career'),
			'monthly_income' => $this->input->post('monthly-income'),
			//'province_id' => $this->input->post('province'),
			//'amphurs_id' => $this->input->post('amphurs'), 
			//'tumbon_id' => $this->input->post('district'), 
			//'zip_code' => $this->input->post('zipcode'), 
			//'birthday' => $this->input->post('bdate'), 
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			'sex' => $this->input->post('gender'), 
			'status' => $this->input->post('status'),
			'remark' => $this->input->post('remark'),
            'updated'=>date("Y-m-d H:m:s")
			
         ); 


		 $bdate = $this->input->post('bdate');
		 if($bdate != ''){
			$data['birthday'] = date(date_format(DateTime::createFromFormat('d/m/Y', $bdate),"Y-m-d"));
		 }

		 $this->Customer_Model->update($data,$customer_code); 

		 ################# Create Logs ##############
		 $LogsJSON = json_encode($data);
		 $code = date("dmY-His");
		 $logs_data = array( 
            //'img' => $img, 
			'logs_code' =>  "Logs".$code, 
            'ref_code' => $customer_code, 
            'category' => 'Customer', 
			'logs' => $LogsJSON, 
            'create_logs'=>$data['updated']
        ); 
		 
		//$this->Logs_Model->insert($logs_data); 

        redirect('admin/customer');
    }

	public function detail($id = null)
	{
		$data['res_customer'] = $this->Customer_Model->selectOneDetail($id);
		$data['customer_temp'] = $this->Customer_Model->selectTemp($id);

		$data['address'] = $this->Customer_Model->detailAddress($id, 'ที่อยู่ปัจจุบัน');
		$data['monthlyIncome'] = $this->monthlyIncome();
		$data['career'] = $this->career();

        $this->load->view('admin/header');
		$this->load->view('admin/customer_detail', $data);
        $this->load->view('admin/footer');
	}

	public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Customer_Model->select($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Customer_Model->selectAllItems($Search);
		echo json_encode($data);
	}
	public function getBySearch()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->getBySearch($postData);
		echo json_encode($data);
	}
	public function getamphoe()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->amphoe($postData);
		echo json_encode($data);
	}

   	public function getdistric()
   	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->districs($postData);
		echo json_encode($data);
	}
  	public function getzipcode()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->zipcodes($postData);
		echo json_encode($data);
	}
	public function getCustomerByIdcard()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->CustomerByIdcard($postData);
		echo json_encode($data);
	}

	public function getInhabitedByCode()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->getInhabitedByCode($postData);
		echo json_encode($data);
	}

	public function getInhabitedByCodes()
	{
		$postData = $this->input->post();
		$data = $this->Customer_Model->getInhabitedByCodes($postData);
		echo json_encode($data);
	}

	public function premise($id = null)
	{
		$data['res'] = $this->Customer_Model->selectOne($id);
		/*
		$data['province'] = $this->Agent_Model->province();
        $data['amphoe'] = $this->Agent_Model->amphoe();
        $data['districs'] = $this->Agent_Model->districs();
		*/
		//$data['customer_premise'] = $this->Customer_Model->selectPremise($id);
		
        $this->load->view('admin/header');
		$this->load->view('admin/customer_premise',$data);
        $this->load->view('admin/footer');
	}

	public function monthlyIncome(){
		$arr = array(
			'0'=>'รายไดไม่เกิน 12,000 บาท',
			'1'=>'12,001-15,000 บาท',
			'2'=>'15,001-30,000 บาท',
			'3'=>'30,001-50,000 บาท',
			'4'=>'50,001-80,000 บาท',
			'5'=>'80,000 บาทขึ้นไป'
		);
		return $arr;
	}

	public function career(){
		$arr = array(
			'0'=>'พนักงานประจำ',
			'1'=>'พนักงานชั่วคราว',
			'2'=>'พนักงานรายสัญญา',
			'3'=>'พนักงานรายวัน',
			'4'=>'อิสระ',
			'5'=>'เจ้าของกิจการ',
			'6'=>'อื่นๆ'
		);
		return $arr;
	}


}
