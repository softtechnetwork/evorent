<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Productinstallments extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

     
		$this->load->library('form_validation');
		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $product_hirepurchase_interest = $this->product_hirepurchase_interest();
		// echo '<PRE>';
		// print_r($product_hirepurchase_interest);exit();
	   $data = array(
		   "product" => $product_hirepurchase_interest
	   );
	  

	  
	   $this->template['content'] = $this->load->view('backend/productinstallments/productinstallments_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	public function edit($id)
	{
          //$promotion = $this->promotion();
       /* if recieve post data from frontend then */
       $product_install_rel_hirepurchaseinterest = $this->product_install_rel_hirepurchaseinterest($id);
		//    echo '<PRE>';
		//    print_r($product_install_rel_hirepurchaseinterest);exit();
	   $formula = $this->formula();
	

       if($this->input->post(NULL,FALSE)){
	
            # obj (instalment_obj)
                //start_date datetime
                //end_date  datetime
                //start_payment_in_instalment int //ผ่อนตั้งแต่งวดที่เท่าไหร่
                //end_payment_in_instalment int // ถึงงวดที่เท่าไหร่
                //amount_installments_period_id int
                //interest  numeric(18, 2) //ดอกเบี้ย
                //down_payment_percent  numeric(18, 2) // เงื่อนไข ดาวน์กี่เปอ
                //is_downcondition int // สถานะกำกับว่าต้องดาวน์ไหม ถึงจะเข้าเงื่อนไข
				//holdonpayment = keyคือ group 
            #eof
            $product_hirepurchase_arr = array();
            // echo '<PRE>';
		    // print_r($this->input->post());exit();


            foreach($this->input->post() as $key => $post_value){
               
                # start instalments
                if (strpos($key, 'startpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr = $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "startpayment");
                    
                }
                # end instalments
                if (strpos($key, 'endpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "endpayment");
                    
                }
				 # end instalments
				 if (strpos($key, 'ishelppaygroup') !==  false) { 
                    // build group
					$post_value = empty($post_value) ? 0 : $post_value;
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "help_pay_installments_status");
                    
                }
                 # end instalments
                 if (strpos($key, 'interestgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "interest");
                    
                }
				# product hire purchase code 
				if (strpos($key, 'producthirepurchasecodegroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "product_hirepurchase_code");
                    
                }
				# product hire purchase id
				if (strpos($key, 'productrelint') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "product_hirepurchase_id");
                    
                }
				# end instalments
				if (strpos($key, 'instalment') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "instalment");
                    //exit();
                }
                
            }
			// echo '<PRE>';
			// print_r($this->input->post());exit();
			// echo '<PRE>';
			// print_r($product_hirepurchase_arr);exit();

		
			
            // ########## insert ##################################
			// code  PC + datetimenow + masterproductid + formulaid +  groupid;
			//echo '<PRE>';
			//print_r($product_hirepurchase_arr);exit();
				$holdonpayment = array();
				if(!empty($this->input->post('holdonpayment'))){
					$holdonpayment = $this->input->post('holdonpayment');
					
					$holdonpayment = json_decode($holdonpayment[key($holdonpayment)]);

				}
			
				$remove_group = array();
				
				if(!empty($this->input->post('removeGroupArray'))){
					$remove_grouparr = $this->input->post('removeGroupArray');
					
					$remove_group = array_filter(json_decode($remove_grouparr));

				}
				if(!empty($remove_group)){
					# Delete This Group
					$this->db->where('product_install_id', $this->input->post('product_install_id'));
					$this->db->where_not_in('id', $remove_group);
					$this->db->delete('product_hirepurchase_interest');
				}
				
			  //	echo '<PRE>';
			  //	print_r($product_hirepurchase_arr);exit();
              foreach($product_hirepurchase_arr as $pk => $pv){
				    # purpose : create json obj to insert to database
					$formula_id = $this->input->post('formula');
					$formula = $this->formula($formula_id);
					$formula_calcurate = json_decode($formula[key($formula)]->formula);
					
					$start_payment = isset($pv['startpayment']) ? intval($pv['startpayment']) : 1;
					$end_payment = intval($pv['endpayment']);
					
					$product_install_id = intval($this->input->post('product_install_id'));

					$formula_id = intval($this->input->post('formula'));

					$group_id = $pk;
					
					$save_array = array();
					$help_pay_installments_status = isset($pv['help_pay_installments_status'])? $pv['help_pay_installments_status'] : 0;
					$instalment = (int)$pv['instalment'];
					$product_install_id = $this->input->post('product_install_id');
					// interest
					//$interest = $pv['interest'];
					$interest = 0;
					
					// เตรียมข้อมูลเพื่อทำ array ชื่อ savearray ไว้ convert ข้อมูลและ save ลง  database 
					$save_array = $this->prepareBuildSaveArray($formula_calcurate 
					,$product_install_id
					,$holdonpayment , $group_id
					,$start_payment
					,$end_payment
					,$interest
					,$instalment);
					
					
					// echo '<PRE>';
					// print_r($product_hirepurchase_arr);exit();
					# product hirepurchase code 
					if(!isset($pv["product_hirepurchase_code"])){
						$product_hirepurchase_code =  "PC".strtotime(date('Y-m-d H:i:s')) . $product_install_id.$formula_id;
						# insert 
						$query = $this->db->insert('product_hirepurchase_interest',array(
							'formula_id'=> intval($formula_id),
							'product_hirepurchase_code' => $product_hirepurchase_code,
							'product_install_id' => intval($product_install_id),
							'term_of_payment_start' => $start_payment,
							'term_of_payment_end' => $end_payment,
							'help_pay_installments_status'=> intval($help_pay_installments_status),
							'holdonpayment' => ($holdonpayment[$group_id] > 0 ) ?  json_encode($holdonpayment[$group_id]) : "",
							'installment_amount_per_installment' => json_encode($save_array),
							'interest' => floatval($interest),
							'created'=>date('Y-m-d H:i:s')
						)); 
					}else{
						# update
					
						$product_hirepurchase_id = $pv["product_hirepurchase_id"];
						$query = $this->db->update('product_hirepurchase_interest',array(
							'formula_id'=> intval($formula_id),
							'product_install_id' => intval($product_install_id),
							'term_of_payment_start' => $start_payment,
							'term_of_payment_end' => $end_payment,
							'help_pay_installments_status'=> intval($help_pay_installments_status),
							'holdonpayment' => ($holdonpayment[$group_id] > 0 ) ?  json_encode($holdonpayment[$group_id]) : "",
							'installment_amount_per_installment' => json_encode($save_array),
							'interest' => floatval($interest),
							'created'=>date('Y-m-d H:i:s')
						),array('id'=>$product_hirepurchase_id)); 
					}
			  }
           // ####################################################
           $this->msg->add('Insert success','success');
		   redirect($this->uri->uri_string());
        
       }
       
       
       /* eof recieve post data from frontend then */

      $data = array(
		"product_install" => $this->product_install(),
		"product_install_rel_hirepurchaseinterest" => $product_install_rel_hirepurchaseinterest,
		"formula" => $formula,
      );

		//   echo '<PRE>';
		//   print_r($data["product_install_rel_hirepurchaseinterest"]);exit();

      
      $this->template['content'] = $this->load->view('backend/productinstallments/productinstallments_edit',$data, true);
      /** load javascript **/
      $this->_load_js();
      /** load external javascript */
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/productinstallments_edit.js');
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/productinstallments.js');
      $this->load->view('backend/template', $this->template);
     
      
	}

	public function delete(){
		$product_id = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
		# Delete This Product
		$this->db->where('product_install_id', $product_id);
		$this->db->delete('product_hirepurchase_interest');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}
	public function delete_img(){
		
		if(isset($_POST['filename'])){
			unlink("./uploaded/tmp_img/".$_POST['filename']);
		}
		
	}

	public function ajaxuploadtmpimg(){

		if(!is_dir('uploaded/tmp_img/')){
			mkdir('uploaded/tmp_img/',0777,true);
		}
		
		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmp_img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmp_img/')){
					rmdir('uploaded/tmp_img/');
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}

		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));

	}

	public function ajaxuploadimgOnActionEdit($masterproduct_id){
		// echo $masterproduct_id;exit();
		if(!is_dir('uploaded/tmp_img_edit/' . $masterproduct_id)){
			mkdir('uploaded/tmp_img_edit/' . $masterproduct_id,0777,true);
		}

		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmp_img_edit/'.$masterproduct_id;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmp_img_edit/' .$masterproduct_id)){
					rmdir('uploaded/tmp_img_edit/' .$masterproduct_id);
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}
		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));
		
	}

	public function ajaxretrievefilefromserver(){
			$product = $this->product($_POST['product_id']);
			$pic= array();
			foreach($product as $k => $value){
				if(!empty($value->pic)){
					foreach(json_decode($value->pic) as $picKey => $picvalue){
						$pic[] = $picvalue;
					}
				}
			}
			
			//print_r($product_id);exit();
			echo json_encode(array("data" => $pic));
	}
	public function create()
	{
       //$promotion = $this->promotion();
       /* if recieve post data from frontend then */
       $product_install = $this->product_install();
	   $formula = $this->formula();
	
	   
       if($this->input->post(NULL,FALSE)){
	
            # obj (instalment_obj)
                //start_date datetime
                //end_date  datetime
                //start_payment_in_instalment int //ผ่อนตั้งแต่งวดที่เท่าไหร่
                //end_payment_in_instalment int // ถึงงวดที่เท่าไหร่
                //amount_installments_period_id int
                //interest  numeric(18, 2) //ดอกเบี้ย
                //down_payment_percent  numeric(18, 2) // เงื่อนไข ดาวน์กี่เปอ
                //is_downcondition int // สถานะกำกับว่าต้องดาวน์ไหม ถึงจะเข้าเงื่อนไข
				//holdonpayment = keyคือ group 
            #eof
            $product_hirepurchase_arr = array();
			
            foreach($this->input->post() as $key => $post_value){
				
                # start instalments
                if (strpos($key, 'startpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr = $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "startpayment");
                    
                }
                # end instalments
                if (strpos($key, 'endpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "endpayment");
                    
                }
				 # end instalments
				 if (strpos($key, 'ishelppaygroup') !==  false) { 
                    // build group
					$post_value = empty($post_value) ? 0 : $post_value;
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "help_pay_installments_status");
                    
                }
                 # end instalments
                 if (strpos($key, 'interestgroup') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "interest");
                    
                }

				# end instalments
				if (strpos($key, 'instalment') !==  false) { 
                    // build group
                    $product_hirepurchase_arr =  $this->buildArrGroup($product_hirepurchase_arr , $key  , $post_value , "instalment");
                    //exit();
                }
                
            }
			
			

            // ########## insert ##################################
			// code  PC + datetimenow + masterproductid + formulaid +  groupid;
			//echo '<PRE>';
			//print_r($product_hirepurchase_arr);exit();
				$holdonpayment = array();
				if(!empty($this->input->post('holdonpayment'))){
					$holdonpayment = $this->input->post('holdonpayment');
					
					$holdonpayment = json_decode($holdonpayment[key($holdonpayment)]);

				}
				
				
              foreach($product_hirepurchase_arr as $pk => $pv){
				    # purpose : create json obj to insert to database
					$formula_id = $this->input->post('formula');
					$formula = $this->formula($formula_id);
					$formula_calcurate = json_decode($formula[key($formula)]->formula);
					
					$start_payment = isset($pv['startpayment']) ? intval($pv['startpayment']) : 1;
					$end_payment = intval($pv['endpayment']);

					$product_install_id = intval($this->input->post('product_install_id'));

					$formula_id = intval($this->input->post('formula'));

					$group_id = $pk;
					
					$save_array = array();
					$help_pay_installments_status = isset($pv['help_pay_installments_status'])? $pv['help_pay_installments_status'] : 0;
					$instalment = (int)$pv['instalment'];
					if(!empty($formula_calcurate)){
						$result = 0;
						foreach($formula_calcurate->formula as $fk => $fvalue){
							$calcurate = isset($fvalue->formula_1->calculate) ? $fvalue->formula_1->calculate : 0;
							$product_install = $this->product_install($this->input->post('product_install_id'));
							$product_price_a = $product_install[key($product_install)]->price_a;
							$product_price_b = $product_install[key($product_install)]->price_b;
							$product_price_c = $product_install[key($product_install)]->price_c;
							$product_price_d = $product_install[key($product_install)]->price_d;
							$tax_rate = $product_install[key($product_install)]->tax_rate;
							// check ว่ามีพักชำระหนี้ไหม
							$countholdonpayment = 0;
							if(isset($holdonpayment[$group_id])){
								$countholdonpayment = count($holdonpayment[$group_id]);
							}

							($product_price_a == null)?$product_price_a = 0.00:$product_price_a ;
							($product_price_b == null)?$product_price_b = 0.00:$product_price_b ;
							($product_price_c == null)?$product_price_c = 0.00:$product_price_c ;
							($product_price_d == null)?$product_price_d = 0.00:$product_price_d ;
							
							// ผ่อนกี่งวด
							$nofmonth_product_installments =$end_payment - $countholdonpayment;
							// interest
							//$interest = $pv['interest'];
							$interest = 0;
							//คำนวณยอดรวมต่อเดือน price(a)
							$result_a = $this->calcurate_result( $product_price_a , $calcurate ,$nofmonth_product_installments , $interest );
							
							//คำนวณยอดรวมต่อเดือน price(a)
							$result_b = $this->calcurate_result( $product_price_b , $calcurate ,$nofmonth_product_installments , $interest );
							
							//คำนวณยอดรวมต่อเดือน price(a)
							$result_c = $this->calcurate_result( $product_price_c , $calcurate ,$nofmonth_product_installments , $interest );
						
							//คำนวณยอดรวมต่อเดือน price(a)
							$result_d = $this->calcurate_result( $product_price_d , $calcurate ,$nofmonth_product_installments , $interest );
						
							
							//echo $calcurate;exit();
							//echo math_eval($calcurate);
						}
						
						for($i = $start_payment; $i<= $end_payment; $i ++){
							$save_array[$i]['product_price_a'] = floatval($product_price_a);
							$save_array[$i]['product_price_b'] = floatval($product_price_b);
							$save_array[$i]['product_price_c'] = floatval($product_price_c);
							$save_array[$i]['product_price_d'] = floatval($product_price_d);
							$save_array[$i]['interest'] = floatval($interest);
							$save_array[$i]['interest_a'] = floatval(number_format((float)$product_price_a * ( $interest / 100 ), 2 , '.' ,''));
							$save_array[$i]['interest_b'] =  floatval(number_format((float)$product_price_b * ( $interest / 100 ), 2 , '.' ,''));
							$save_array[$i]['interest_c'] =  floatval(number_format((float)$product_price_c * ( $interest / 100 ), 2 , '.' ,''));
							$save_array[$i]['interest_d'] =  floatval(number_format((float)$product_price_d * ( $interest / 100 ), 2 , '.' ,''));
							$tax_rate = floatval($tax_rate);
							$save_array[$i]['tax_rate'] = $tax_rate;
							
					
							// $save_array[$i]['product_price_a_vat'] = floatval($product_price_a) * ( $tax_rate / 100 );
							// $save_array[$i]['product_price_b_vat'] = floatval($product_price_b) * ( $tax_rate / 100 );
							// $save_array[$i]['product_price_c_vat'] = floatval($product_price_c) * ( $tax_rate / 100 );
							// $save_array[$i]['product_price_d_vat'] = floatval($product_price_d) * ( $tax_rate / 100 );

						//if($help_pay_installments_status  != 1){
							if($countholdonpayment > 0){
								if (in_array($i, $holdonpayment[$group_id]))
								{
									$save_array[$i] = $this->buildSaveArray($instalment, $save_array , 0 , 0 , 0 , 0 ,$i , $tax_rate);
								}else{
									// build save array 
									$save_array[$i] = $this->buildSaveArray($instalment, $save_array , $result_a , $result_b,$result_c,$result_d ,$i , $tax_rate);
								}
							}else{
								// build save array 
								$save_array[$i] = $this->buildSaveArray($instalment, $save_array , $result_a , $result_b,$result_c,$result_d ,$i , $tax_rate);
							}
						
						}
					}
					# product hirepurchase code 
					$product_hirepurchase_code =  "PC".strtotime(date('Y-m-d H:i:s')) . $product_install_id.$formula_id;
					# insert
					$query = $this->db->insert('product_hirepurchase_interest',array(
						'formula_id'=> intval($formula_id),
						'product_hirepurchase_code' => $product_hirepurchase_code,
						'product_install_id' => intval($product_install_id),
						'term_of_payment_start' => $start_payment,
						'term_of_payment_end' => $end_payment,
						'help_pay_installments_status'=> intval($help_pay_installments_status),
						'holdonpayment' => ($holdonpayment[$group_id] > 0 ) ?  json_encode($holdonpayment[$group_id]) : "",
						'installment_amount_per_installment' => json_encode($save_array),
						'interest' => floatval($pv['interest']),
						'created'=>date('Y-m-d H:i:s')
					));
			  }
           // ####################################################
           $this->msg->add('Insert success','success');
		   redirect($this->uri->uri_string());
        
       }
       
       
       /* eof recieve post data from frontend then */

      $data = array(
		"product_install" => $product_install,
		"formula" => $formula,
      );

	//   echo '<PRE>';
	//   print_r($product);exit();

      
      $this->template['content'] = $this->load->view('backend/productinstallments/productinstallments_create',$data, true);
      /** load javascript **/
      $this->_load_js();
      /** load external javascript */
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/productinstallments_create.js');
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/productinstallments.js');
      $this->load->view('backend/template', $this->template);
      
	}


	public function ajaxdrawformulagroup(){
		$formula_id = $_POST['formula_id'];
		$formula = $this->formula($formula_id);

		$data_sendback = array(
			"status" => true,
			"formula" => $formula[key($formula)],
		);
		echo json_encode($data_sendback);
	}
	public function slugify($text)
	{
		// replace all non letters or digits by _
		$text = preg_replace('/\W+/', '_', $text);

		// trim and lowercase
		$text = strtolower(trim($text, '_'));

		return $text;
	}


}
