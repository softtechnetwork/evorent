<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Productpart extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		//$this->load->helper('our_helper');

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $productpart = $this->product(null , null , 2);
	   //echo '<PRE>';
	   //print_r($productpart);exit();
	   $data = array(
		   "product" => $productpart
	   );

	 
	   $this->template['content'] = $this->load->view('backend/productpart/productpart_list',$data, true);
	   	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
	  
      
	}

    public function create()
	{
	  
	   //get main product type only 
       $product = $this->product(null , null , 1);
	   $warranty = $this->warranty();
	  
	   $uom = $this->uom();
	   
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		
            $mpcode = $this->input->post('mpcode');
			$mpname = $this->input->post('mpname');
			$mpabbrv = $this->input->post('mpabbrv');
			$uom = $this->input->post('uom');
			
			$mp_desc = $this->input->post('mpdesc');
			$file_dz = "";
			//	print_r($this->input->post('file_dz[]'));exit();
			$product_parent_id  = $this->input->post('product_parent_id');
			$main_product = $this->product($product_parent_id , null);
			$product_category_id = $product[key($product)]->product_category_id;

			$getlastedmpcode = $this->getLatestProductMPCODE(null , null , 2 , "PP");
			$mp_code = ($getlastedmpcode > 0 ) ? $getlastedmpcode + 1 : "981000001";
		   // ########## insert ##################################
		   $query = $this->db->insert('masterproduct',array(
				'mpcode' => "PP" .$mp_code,
				'mpname' => $this->input->post('mpname'),
				'mpabbrv' => $this->input->post('mpabbrv'),
				'product_category_id' => $product_category_id,
				'typeproduct_id' => 2,
				
				'uom_id' => $this->input->post('uom'),
				'price_a' => $this->input->post('price_a'),
				'price_b' => $this->input->post('price_b'),
				'price_c' => $this->input->post('price_c'),
				'price_d' => $this->input->post('price_d'),
				'mp_desc' => $this->input->post('mp_desc'),
				'warranty_id' => $this->input->post('warranty_id'),
				'brand' => $this->input->post('brand'),
				'created'=>date('Y-m-d H:i:s')
			)); 
		   // ####################################################
			$insert_id = $this->db->insert_id();
	
			// UPLOAD IMAGE
			$newarr = array(); 
		
			if(($this->input->post('file_dz[]') !== null)){
				
					if(!is_dir('uploaded/masterproduct/'.$insert_id.'')){
						mkdir('uploaded/masterproduct/'.$insert_id.'',0777,true);
					}
					clearDirectory('uploaded/masterproduct/'.$insert_id.'/');
					
					foreach($this->input->post('file_dz[]') as $file_key => $file_value){
						$newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
						$file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
						// move file to another directory 
						$file_value = str_replace(' ', '_', $file_value);
						if(rename("uploaded/tmp_img/".$file_value, 'uploaded/masterproduct/'.$insert_id.'/'.$newfilename.".".$file_ext)){
							// push newfilename to array
							$newarr[] = $newfilename.".".$file_ext;
						}
					
					}
					// update newpic
					$this->db->update('masterproduct',array(
						'pic'=> json_encode($newarr)
					),array('id'=>$insert_id));
					
			}
		

			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "product" => $product,
		   "uom" => $uom,
		   "warranty" => $warranty,
		   "product_category" => $this->product_category()
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productpart/productpart_create',$data, true);
	    /** load javascript **/
		
        
	   $this->_load_js();
	   $this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
   
	   $this->template['external_js'][] = base_url('./assete/js/page/backend/productpart_create.js');
	   
	   $this->load->view('backend/template', $this->template);
      

	  
	  
      
	}

	public function edit($id)
	{
      $mainproduct = $this->product(null , null , 1);
	  $product = $this->product($id , null , 2);
	  $warranty = $this->warranty();
	  $uom = $this->uom();
	 
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
			$masterproduct_id = $id;
            
			$mpname = $this->input->post('mpname');
			$mpabbrv = $this->input->post('mpabbrv');
		
			$uom = $this->input->post('uom');
			
			$mpdesc = $this->input->post('mpdesc');
           // $img = $this->findDefaultLink();
		   // ########## insert ##################################
		   $query = $this->db->update('masterproduct',array(
		
				'mpname' => $this->input->post('mpname'),
				'mpabbrv' => $this->input->post('mpabbrv'),
				'mp_desc' => $this->input->post('mp_desc'),
				'product_category_id' => $this->input->post('product_category_id'),
				'product_parent_id' => $this->input->post('product_parent_id'),
				'uom_id' => $this->input->post('uom'),
				'price_a' => $this->input->post('price_a'),
				'price_b' => $this->input->post('price_b'),
				'price_c' => $this->input->post('price_c'),
				'price_d' => $this->input->post('price_d'),
				'brand' => $this->input->post('brand'),
				'warranty_id' => $this->input->post('warranty_id'),
				'created'=>date('Y-m-d H:i:s')
			) ,array('id'=>$masterproduct_id) ); 
		   // ####################################################
		
			// UPLOAD IMAGE 
			
				// UPLOAD IMAGE
				$newarr = array(); 
				
				if(($this->input->post('file_dz[]') !== null)){
					
						if(!is_dir('uploaded/masterproduct/'.$masterproduct_id.'')){
							mkdir('uploaded/masterproduct/'.$masterproduct_id.'',0777,true);
						}
						//clearDirectory('uploaded/masterproduct/'.$masterproduct_id.'/');
						
						foreach($this->input->post('file_dz[]') as $file_key => $file_value){
							$newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
							$file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
							// move file to another directory 
							$file_value = str_replace(' ', '_', $file_value);
							# case : if already have file on tmp img edit then move to masterproduct directory
							if(file_exists("uploaded/tmp_img_edit/".$masterproduct_id.'/'.$file_value)){
								
								
								if(rename("uploaded/tmp_img_edit/".$masterproduct_id.'/'.$file_value, 'uploaded/masterproduct/'.$masterproduct_id.'/'.$newfilename.".".$file_ext)){
									// push newfilename to array
									$newarr[] = $newfilename.".".$file_ext;
								
								}
							}else{
							# case : if not found  this file should be on masterproduct directory 
								$newarr[] = $file_value;
							}
						
						}
						// update newpic
						$this->db->update('masterproduct',array(
							'pic'=> json_encode($newarr)
						),array('id'=>$masterproduct_id));
						
				}
			

				$this->msg->add('Insert success','success');
				redirect($this->uri->uri_string());
			// ###################################################

			$this->msg->add('Edit success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "mainproduct" => $mainproduct,
		   "product" => $product,
		   "uom" => $uom,
		   "warranty" => $warranty,
		   "product_category" => $this->product_category()
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productpart/productpart_edit',$data, true);
	    /** load javascript **/
		$this->_load_js();
		$this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
		$this->template['external_js'][] = base_url('./assete/js/page/backend/productpart_edit.js');
	   $this->load->view('backend/template', $this->template);
      
	}
	
	public function delete(){
		$product_id = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
		# Delete This Product
		$this->db->where('id', $product_id);
		$this->db->delete('masterproduct');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}
	



}
