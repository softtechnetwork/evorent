<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Productserialnumber extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('excel');
		$this->load->library("pagination");
		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	public function record_count($queryString = null){
		$query = $this->db->select('masterproduct.* , masterproduct_serialnumber.mp_serial_number , masterproduct_serialnumber.id as masterproduct_serial_id ')
		->from('masterproduct_serialnumber')
		->join('masterproduct' , 'masterproduct.id = masterproduct_serialnumber.masterproduct_id');
		if(!empty($queryString)){
			$query = $query->like('masterproduct.mpname', $queryString);
			$query = $query->or_like('masterproduct_serialnumber.mp_serial_number', $queryString);
		}
		// $query = $query->get();
		return $query->count_all_results();
		//return $this->db->count_all("masterproduct_serialnumber");
	}
	 
	
	public function fetch_product($limit, $start , $queryString = null  ) {
		$this->db->limit($limit, $start);
		//$query = $this->db->get("masterproduct_serialnumber");
	
		$query = $this->db->select('masterproduct.* , masterproduct_serialnumber.mp_serial_number , masterproduct_serialnumber.id as masterproduct_serial_id ')
		->from('masterproduct_serialnumber')
		->join('masterproduct' , 'masterproduct.id = masterproduct_serialnumber.masterproduct_id');
		
		if(!empty($queryString)){
			$query = $query->like('masterproduct.mpname', $queryString);
			$query = $query->or_like('masterproduct_serialnumber.mp_serial_number', $queryString);
		}
		$query = $query->get();
		

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			//echo '<PRE>';
			//print_r($data);exit();
			return $data;
		}
	
		return false;
	}
	   
	public function index()
	{
    
       //$productserialnumber = $this->productserialnumber();
	    // Search text
		$product = $this->product();
		$search_text = "";
		$postData = $this->input->post();
		if($this->input->post('submit') != NULL ){
				$search_text = $this->input->post('search');
			    $this->session->set_userdata(array("search"=>$search_text));
		}else{
			if($this->session->userdata('search') != NULL){
			
				if($this->input->post('search')){
				
					$search_text = $this->input->post('search');
					$this->session->set_userdata(array("search"=>$search_text));
				}else{
					//echo '5';exit();
					if(!empty($postData)){
						if(isset($postData['search'])){
							$this->session->set_userdata(array("search"=>$postData["search"]));
						}

					}else{
						$search_text = $this->session->userdata('search');
					}
				}
			}else{
				$search_text = $this->input->post('search');
				$this->session->set_userdata(array("search"=>$search_text));
			}
		}
		

		
	   $config = array();
	   $config["base_url"] = base_url() . "backend/productserialnumber/list";
	   if($this->input->post(NULL,FALSE)){
			
			// $config["total_rows"] = $this->record_count(strval($search_text));
			
	   }else{
	   		// $config["total_rows"] = $this->record_count(strval($search_text));
	   }
	   $config["total_rows"] = $this->record_count(strval($search_text));


	   // echo '<PRE>';
	   // print_r($this->input->post());exit();
	   $config["per_page"] = 20;
	   $config["uri_segment"] = 4;
	    
	    $config['full_tag_open'] = '<ul class="pagination" style="float:right !important;">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active" ><a class="" href="#" style="background-color:red;color:white;">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
	    $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	   // เช็ค ถ้ามากเกินจำนวนหน้า
	   $AllPage = $config['total_rows'] / $config['per_page'];
	   if($page > 0){
		   // all page ex: 400  / 20 ( page 2)
			$current_page = $page / $config['per_page'];
			$current_page = round($current_page);
			if($current_page > $AllPage){
				$page = 1;
				$config['uri_segment'] = 1;
			}
	   }

	   $this->pagination->initialize($config);
	 
	   
	  $data["results"] = $this->fetch_product($config["per_page"], $page , strval($search_text));
	   
	   //echo '<PRE>';
	   //print_r($data);exit(); 
	   
	   $data["links"] = $this->pagination->create_links();
	   $data["productserialnumber"] = $data["results"];
	   $data['total_rows'] = $config["total_rows"];
	   $data['search'] = $search_text;
	   $data["product"] = $product;
	   $this->template['content'] = $this->load->view('backend/productserialnumber/productserialnumber_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	public function list(){
		$config = $this->pagination;
		// send pagination
		$this->index();
	//	echo '<PRE>;
	//	print_r($t'
		
		
	}
    public function create()
	{
	   //echo '5';exit();
       $productserialnumber = $this->productserialnumber();
	   $product = $this->product(null , null , 3);
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
			$mp_serial_number_start = $this->input->post('mp_serial_number_start');
			$mp_serial_number_end = $this->input->post('mp_serial_number_end');
			if($mp_serial_number_end < $mp_serial_number_start){
				$this->msg->add('หมายเลขสิ้นสุดควรน้อยกว่าเริ่มต้น','error');
				redirect($this->uri->uri_string());
			}
		   // ########## insert ##################################
		   $i = $mp_serial_number_start;
		   while($i < $mp_serial_number_end){
				$query = $this->db->insert('masterproduct_serialnumber',array(
					'masterproduct_id'=>$this->input->post('masterproduct_id'),
					'mp_serial_number'=>$i,
					'created'=>date('Y-m-d H:i:s')
				)); 
				++$i;
		   }
		   // eof 
		//    $query = $this->db->insert('masterproduct_serialnumber',array(
		// 		'masterproduct_id'=>$this->input->post('masterproduct_id'),
		// 		'mp_serial_number'=>$this->input->post('mp_serial_number'),
		// 		'created'=>date('Y-m-d H:i:s')
		// 	)); 


			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "product" => $product,
		   "productserialnumber" => $productserialnumber,
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productserialnumber/productserialnumber_create',$data, true);
	      /** load javascript **/
		$this->_load_js();
		  /** load external javascript */
		  $this->template['external_js'][]  =  base_url('./assete/js/page/backend/productserialnumber.js');

	   $this->load->view('backend/template', $this->template);
      
	}

	public function create_newserialnumber(){
		
		$mp_serial_number_start = $this->input->post('mp_serial_number_start');
		$mp_serial_number_end = $this->input->post('mp_serial_number_end');
		if($mp_serial_number_end < $mp_serial_number_start){
			$this->msg->add('หมายเลขสิ้นสุดควรน้อยกว่าเริ่มต้น','error');
			redirect($this->uri->uri_string());
		}
		// ########## insert ##################################
		$i = $mp_serial_number_start;
		$insert_multiplerow = array();
		while($i < $mp_serial_number_end){
			$data_push = array(
				"masterproduct_id" => $this->input->post('masterproduct_id'),
				"mp_serial_number" => $i,
				"created" => date('Y-m-d H:i:s')
			);
			array_push($insert_multiplerow , $data_push);
			++$i;
		}
		
		# insert into db at once
		$this->db->insert_batch('masterproduct_serialnumber', $insert_multiplerow);

		$data_sendback = array(
			"status" => true,
			"message" => "บันทึกข้อมูลเสร็จสมบูรณ์"
		);
		echo json_encode($data_sendback);
	}
	public function edit($id)
	{
	   //echo '5';exit();
        $productserialnumber = $this->productserialnumber($id);
		//print_r($productserialnumber);exit();
		// typeproduct_id = 3 select all
		$product = $this->product(null , null , 3);
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		   // ########## insert ##################################
		   $query = $this->db->update('masterproduct_serialnumber',array(
				'masterproduct_id'=>$this->input->post('masterproduct_id'),
				'mp_serial_number'=>$this->input->post('mp_serial_number'),
				'created'=>date('Y-m-d H:i:s')
			) , array("id" => $id)); 


			$this->msg->add('Update success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */
	
	   $data = array(
		   "product" => $product,
		   "productserialnumber" => $productserialnumber,
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productserialnumber/productserialnumber_edit',$data, true);
	       /** load javascript **/
		$this->_load_js();
	   $this->load->view('backend/template', $this->template);
      
	}

	public function importFile(){
			
		   $path = $_FILES["file"]["tmp_name"];
		   $object = PHPExcel_IOFactory::load($path);
		   $distict_serial = array();

		   $status = true;
		   $message = ""; // send back message
		   
		   //$this->db->trans_start(); # Starting Transaction
		   //$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
			foreach($object->getWorksheetIterator() as $worksheet)
			{
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					//  echo $highestRow;exit();
					// row = 2 คือ row แรก ไม่รวม header 
					for($row=2; $row<=$highestRow; $row++)
					{
					$masterproduct_id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$mp_serial_number = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					// check before insert 
					$checkalreadyserial = $this->db->select('*')->from('masterproduct_serialnumber')
					->where('masterproduct_id',$masterproduct_id)
					->where('mp_serial_number',(string)$mp_serial_number)->get();
					if($checkalreadyserial->num_rows() > 0){
						$rows = $checkalreadyserial->row()->mp_serial_number;
						$distict_serial[] = $rows;
						$status = false; //status

						$message = "ERROR !! พบหมายเลขเครื่องซ้ำในระบบ"; // message send back
					}else{

						// ########## insert ##################################
						$this->db->insert('masterproduct_serialnumber',array(
							'masterproduct_id'=>intval($masterproduct_id),
							'mp_serial_number'   => (string)$mp_serial_number,
							'created'=>date('Y-m-d H:i:s')
						)); 
					}
	
		 		}
		 	}
			 //$this->db->trans_complete(); # Completing transaction
			 
			 if($status == true){
				//$this->db->trans_commit(); # commitment data when status true
			 }else{
				
				//$this->db->trans_rollback(); # roll back where status false
			 }

			 $data_sendback = array(
				"status" => $status,
				"message" => $message,
				"distict_serial" => count($distict_serial) > 0 ? json_encode($distict_serial) : false
			);
			echo json_encode($data_sendback);

			
		// 	$this->excel_import_model->insert($data);
	}

	public function exportFile(){
		$status = true;
		$message = "";
		try {
			// เรียนกใช้ PHPExcel  
			$objPHPExcel = new PHPExcel();   
			// เราสามารถเรียกใช้เป็น  $this->excel แทนก็ได้

			// กำหนดค่าต่างๆ ของเอกสาร excel
			$objPHPExcel->getProperties()->setCreator("Ninenik.com")  
										 ->setLastModifiedBy("Ninenik.com")  
										 ->setTitle("PHPExcel Test Document")  
										 ->setSubject("PHPExcel Test Document")  
										 ->setDescription("Test document for PHPExcel, generated using PHP classes.")  
										 ->setKeywords("office PHPExcel php")  
										 ->setCategory("Test result file");      
		 
			// กำหนดชื่อให้กับ worksheet ที่ใช้งาน  
			$objPHPExcel->getActiveSheet()->setTitle('Product Report');  
			   
			// กำหนด worksheet ที่ต้องการให้เปิดมาแล้วแสดง ค่าจะเริ่มจาก 0 , 1 , 2 , ......  
			$objPHPExcel->setActiveSheetIndex(0);        
										  
			// การจัดรูปแบบของ cell  
			$objPHPExcel->getDefaultStyle()  
									->getAlignment()  
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)  
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
									//HORIZONTAL_CENTER //VERTICAL_CENTER               
									 
			// จัดความกว้างของคอลัมน์
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);     
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);                                              
			 
			// กำหนดหัวข้อให้กับแถวแรก
			$objPHPExcel->setActiveSheetIndex(0)  
						->setCellValue('A1', 'ลำดับ')    
						->setCellValue('B1', 'รหัสสินค้า') 
						->setCellValue('C1', 'ชื่อสินค้า')  
						->setCellValue('D1', 'หมายเลขเครื่อง');

			$start_row=2; 
			$masterproduct_serialnumber = $this->db->select('masterproduct.* , masterproduct_serialnumber.mp_serial_number , masterproduct_serialnumber.id as masterproduct_serial_id ')
			->from('masterproduct_serialnumber')
			->join('masterproduct' , 'masterproduct.id = masterproduct_serialnumber.masterproduct_id')
			->get();

			if($masterproduct_serialnumber->num_rows() > 0){
				$i =0;
				$result_array = $masterproduct_serialnumber->result_array();
				while($i < count($result_array)){
					 // หากอยากจัดข้อมูลราคาให้ชิดขวา
					$objPHPExcel->getActiveSheet()
						->getStyle('C'.$start_row)
						->getAlignment()  
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);         
						
					// หากอยากจัดให้รหัสสินค้ามีเลย 0 ด้านหน้า และแสดง 3     หลักเช่น 001 002
					// $objPHPExcel->getActiveSheet()
					// 	->getStyle('B'.$start_row)
					// 	->getNumberFormat()
					// 	->setFormatCode('000');          
						
					// เพิ่มข้อมูลลงแต่ละเซลล์      
					if(isset($result_array[$i])){          
						$no = $i + 1;
						$objPHPExcel->setActiveSheetIndex(0)  
									->setCellValue('A'.$start_row, $no )  
									->setCellValue('B'.$start_row, $result_array[$i]['mpcode'])  
									->setCellValue('C'.$start_row, $result_array[$i]['mpname'])  
									->setCellValue('D'.$start_row, $result_array[$i]['mp_serial_number']);
					}
					++ $i;
					++ $start_row;
				}
				// กำหนดรูปแบบของไฟล์ที่ต้องการเขียนว่าเป็นไฟล์ excel แบบไหน ในที่นี้เป้นนามสกุล xlsx  ใช้คำว่า Excel2007
				// แต่หากต้องการกำหนดเป็นไฟล์ xls ใช้กับโปรแกรม excel รุ่นเก่าๆ ได้ ให้กำหนดเป็น  Excel5
				ob_start();
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  // Excel2007 (xlsx) หรือ Excel5 (xls)        
				
				$filename='Product-'.date("dmYHi").'.xlsx'; //  กำหนดชือ่ไฟล์ นามสกุล xls หรือ xlsx
				// บังคับให้ทำการดาวน์ดหลดไฟล์
				header('Content-Type: application/vnd.ms-excel'); //mime type
				header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
				ob_end_clean();     
				$objWriter->save('php://output'); // ดาวน์โหลดไฟล์รายงาน
				die($objWriter);

			}else{
				// status that return to frontend
				$status = false;
				// error message handle
				$message = "ไม่พบข้อมูล";
			}
            
        } catch (Exception $e) {
			// status that return to frontend
			$status = false;
            // error message handle
			$message = $e->getMessage();
        }

		$data_sendback = array(
			"status" => $status,
			"message" => $message,
		
		);
		echo json_encode($data_sendback);
	}
	public function productserialnumber($masterproduct_serialnumber_id = null){
		//echo '5';exit();
        $query = $this->db->select('masterproduct.* , masterproduct_serialnumber.mp_serial_number , masterproduct_serialnumber.id as masterproduct_serial_id ')
		->from('masterproduct_serialnumber')
		->join('masterproduct' , 'masterproduct.id = masterproduct_serialnumber.masterproduct_id')
		->limit(50);
		//$query = $query->where('masterproduct.product_parent_id');
        if($masterproduct_serialnumber_id != null)
        {
            $query = $query->where('masterproduct_serialnumber.id',$masterproduct_serialnumber_id);
        }
      
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return 0;
        }
    }
	
	public function processtbl(){
		echo '<PRE>';
		print_r($_POST);exit();
	}
	public function delete(){
		$mp_serial_number_id = isset($_POST["mp_serial_number_id"]) ? $_POST["mp_serial_number_id"] : 0;
		# Delete This Product
		$this->db->where('id', $mp_serial_number_id);
		$this->db->delete('masterproduct_serialnumber');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}

}
