<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Warranty extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct();         
    } 

	
	public function index()
	{
    
       $warranty = $this->warranty();
	   //echo '<PRE>';
	   //print_r($warranty);exit();
	   $warranty_type = $this->warranty_type();
	   $data = array(
		   "warranty" => $warranty,
		   "warranty_type" => $warranty_type
	   );


	   $this->template['content'] = $this->load->view('backend/warranty/warranty_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

    public function create()
	{
	   
       
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		   // ########## insert ##################################
		   $query = $this->db->insert('warranty',array(
				'value'=>$this->input->post('value'),
				'type'=>$this->input->post('type'),
				'created'=>date('Y-m-d H:i:s')
			)); 


			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "1" => 1
	   );

	  
	   $this->template['content'] = $this->load->view('backend/warranty/warranty_create',$data, true);
	      /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	public function edit($id)
	{
	      //echo '5';exit();
          $warranty = $this->warranty($id);
		
	   
          /* if recieve post data from frontend then */
          if($this->input->post(NULL,FALSE)){
              /* find default link  */
             // ########## update ##################################
            
              $query = $this->db->update('warranty',array(
		
                'value'=>$this->input->post('value'),
                'type'=>$this->input->post('type'),
                'updated'=>date('Y-m-d H:i:s')
			) ,array('id'=>$id) ); 
  
              $this->msg->add('Update success','success');
              redirect($this->uri->uri_string());
      
  
          }
          /* eof recieve post data from frontend then */
  
         $data = array(
             "warranty" => $warranty,
         );
  

	  
	   $this->template['content'] = $this->load->view('backend/warranty/warranty_edit',$data, true);
	       /** load javascript **/
		$this->_load_js();
	   $this->load->view('backend/template', $this->template);
      
	}
	
	
	public function delete(){
		$id = isset($_POST["id"]) ? $_POST["id"] : 0;
		# Delete This Product
		$this->db->where('id', $id);
		$this->db->delete('warranty');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}

	public function checkalreadyused(){
		$uom_id = isset($_POST["id"]) ? $_POST["id"] : 0;
		$mp_message_sendback = array();
		$pi_message_sendback = array();
		# case1: Check unit already used on masterproduct
		$masterproduct_result = $this->db->select('*')->from('masterproduct')->where('uom_id' , $uom_id)->get();
		# สถานะ
		$status = false;
		if($masterproduct_result->num_rows() > 0){
			$i =0;
			$masterproduct_array = $masterproduct_result->result_array();
			while($i < count($masterproduct_array))	{
				$mp_message_sendback[] =  $masterproduct_array[$i]['mpname'];
				++$i;
			}		
			$status = true;
		}

		# case2: Check unit already used on product install
		$product_install_result = $this->db->select('*')->from('product_install')->where('uom_id' , $uom_id)->get();
		if($product_install_result->num_rows() > 0){
			$i =0;
			$productinstall_array = $product_install_result->result_array();
			while($i < count($productinstall_array))	{
				$pi_message_sendback[] =  $productinstall_array[$i]['piname'];
				++$i;
			}		

			# สถานะ 
			$status = true;
		}

		$data_sendback = array(
			"status" => $status,
			"masterproduct" => json_encode($mp_message_sendback),
			"productinstall" => json_encode($pi_message_sendback),
		);
		echo json_encode($data_sendback);
	}

}
