<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Promotion extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		//$this->load->helper('our_helper');

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $promotion = $this->promotion();
	   $product = $this->product();
	   $data = array(
           "product" => $product,
		   "promotion" => $promotion
	   );
     


	  
	   $this->template['content'] = $this->load->view('backend/promotion/promotion_list',$data, true);
        /** load javascript **/
       $this->_load_js();
       
	   $this->load->view('backend/template', $this->template);
      
	}
    
    public function create()
	{
       $promotion = $this->promotion();
       /* if recieve post data from frontend then */
    
       if($this->input->post(NULL,FALSE)){
            # obj (instalment_obj)
                //start_date datetime
                //end_date  datetime
                //start_payment_in_instalment int //ผ่อนตั้งแต่งวดที่เท่าไหร่
                //end_payment_in_instalment int // ถึงงวดที่เท่าไหร่
                //amount_installments_period_id int
                //interest  numeric(18, 2) //ดอกเบี้ย
                //down_payment_percent  numeric(18, 2) // เงื่อนไข ดาวน์กี่เปอ
                //is_downcondition int // สถานะกำกับว่าต้องดาวน์ไหม ถึงจะเข้าเงื่อนไข
            #eof
            $promotion_group = array();
           
            foreach($this->input->post() as $key => $post_value){
               
                # start instalments
                if (strpos($key, 'startpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $promotion_group = $this->buildArrGroup($promotion_group , $key  , $post_value , "startpayment");
                    
                }
                # end instalments
                if (strpos($key, 'endpaymentininstalmentgroup') !==  false) { 
                    // build group
                    $promotion_group =  $this->buildArrGroup($promotion_group , $key  , $post_value , "endpayment");
                    
                }
                 # end instalments
                 if (strpos($key, 'interestgroup') !==  false) { 
                    // build group
                    $promotion_group =  $this->buildArrGroup($promotion_group , $key  , $post_value , "interest");
                    
                }
                
            }
           
            $cb_downpayment = "";
            if ($this->input->post('is_downcondition_group1') !== null) {
                $cb_downpayment = ($this->input->post('is_downcondition_group1') == true) ? 1 : 0 ;
            }else{
                $cb_downpayment = 0;
            }
           
         
            // ########## insert ##################################
              $query = $this->db->insert('promotion',array(
                  'promotion_name'=>$this->input->post('promotion_name_group1'),
                  'start_date' => $this->input->post('datetime_start'),
                  'end_date' => $this->input->post('datetime_end'),
                  'is_downpayment' => $cb_downpayment,
                  'downpayment_percent' => $this->input->post('down_payment_percent_group1'),
                  'instalment_obj' => json_encode($promotion_group),
                  'created'=>date('Y-m-d H:i:s')
              )); 
          // ####################################################
           
        
       }
       
       
       /* eof recieve post data from frontend then */

      $data = array(
          "promotion" => $promotion,
      );

      
      $this->template['content'] = $this->load->view('backend/promotion/promotion_create',$data, true);
      /** load javascript **/
      $this->_load_js();
      /** load external javascript */
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/promotion_create.js');
      $this->template['external_js'][]  =  base_url('./assete/js/page/backend/promotion.js');
      $this->load->view('backend/template', $this->template);
      
	}

    public function edit($id)
	{
        $promotion = $this->promotion($id);
      
        $data = array(
            "promotion" => $promotion[0],
        );
  
        
        $this->template['content'] = $this->load->view('backend/promotion/promotion_edit',$data, true);
        /** load javascript **/
        $this->_load_js();
        /** load external javascript */
      
        $this->template['external_js'][]  =  base_url('./assete/js/page/backend/promotion.js');
        $this->template['external_js'][]  =  base_url('./assete/js/page/backend/promotion_edit.js');
        $this->load->view('backend/template', $this->template);


    }


   
	public function promotion($id = null,$active = null){
        $query = $this->db->select('*')
		->from('promotion');
		
        if($id != null)
        {
            $query = $query->where('id',$id);
        }
        if($active != null)
        {
            // $query = $query->where('active',"1");
        }
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return false;
        }
    }


	public function productpart($id = null,$active = null){
        $query = $this->db->select('masterproduct.* , product_part.id as part_id , product_part.part_name,product_part.part_img')
		->from('masterproduct')
		->join('product_part' , 'masterproduct.id = product_part.masterproduct_id');

        if($id != null)
        {
            $query = $query->where('masterproduct.id',$id);
        }
        if($active != null)
        {
            $query = $query->where('active',"1");
        }
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return 0;
        }
    }





}
