<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Uom extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		//$this->load->helper('our_helper');

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $uom = $this->uom();
	   //echo '<PRE>';
	   //print_r($uom);exit();
	   $data = array(
		   "uom" => $uom
	   );

	//    echo '<PRE>';
	//    print_r($data);exit();
	   $this->template['content'] = $this->load->view('backend/uom/uom_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

    public function create()
	{
	   //echo '5';exit();
       $uom = $this->uom();
	  
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		   // ########## insert ##################################
		   $query = $this->db->insert('uom',array(
				'uom_nameth'=>$this->input->post('uom_nameth'),
				'uom_nameen'=>$this->input->post('uom_nameen'),
				'created'=>date('Y-m-d H:i:s')
			)); 


			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "uom" => $uom,
	   );

	  
	   $this->template['content'] = $this->load->view('backend/uom/uom_create',$data, true);
	      /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	public function edit($id)
	{
	      //echo '5';exit();
          $uom = $this->uom($id);
	  
	   
          /* if recieve post data from frontend then */
          if($this->input->post(NULL,FALSE)){
              /* find default link  */
             // ########## update ##################################
            
              $query = $this->db->update('uom',array(
		
                'uom_nameth'=>$this->input->post('uom_nameth'),
                'uom_nameen'=>$this->input->post('uom_nameen'),
                'updated'=>date('Y-m-d H:i:s')
			) ,array('id'=>$id) ); 
  
              $this->msg->add('Update success','success');
              redirect($this->uri->uri_string());
      
  
          }
          /* eof recieve post data from frontend then */
  
         $data = array(
             "uom" => $uom,
         );
  

	  
	   $this->template['content'] = $this->load->view('backend/uom/uom_edit',$data, true);
	       /** load javascript **/
		$this->_load_js();
	   $this->load->view('backend/template', $this->template);
      
	}
	
	
	public function delete(){
		$id = isset($_POST["id"]) ? $_POST["id"] : 0;
		# Delete This Product
		$this->db->where('id', $id);
		$this->db->delete('uom');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}

	public function checkalreadyused(){
		$uom_id = isset($_POST["id"]) ? $_POST["id"] : 0;
		$mp_message_sendback = array();
		$pi_message_sendback = array();
		# case1: Check unit already used on masterproduct
		$masterproduct_result = $this->db->select('*')->from('masterproduct')->where('uom_id' , $uom_id)->get();
		# สถานะ
		$status = false;
		if($masterproduct_result->num_rows() > 0){
			$i =0;
			$masterproduct_array = $masterproduct_result->result_array();
			while($i < count($masterproduct_array))	{
				$mp_message_sendback[] =  $masterproduct_array[$i]['mpname'];
				++$i;
			}		
			$status = true;
		}

		# case2: Check unit already used on product install
		$product_install_result = $this->db->select('*')->from('product_install')->where('uom_id' , $uom_id)->get();
		if($product_install_result->num_rows() > 0){
			$i =0;
			$productinstall_array = $product_install_result->result_array();
			while($i < count($productinstall_array))	{
				$pi_message_sendback[] =  $productinstall_array[$i]['piname'];
				++$i;
			}		

			# สถานะ 
			$status = true;
		}

		$data_sendback = array(
			"status" => $status,
			"masterproduct" => json_encode($mp_message_sendback),
			"productinstall" => json_encode($pi_message_sendback),
		);
		echo json_encode($data_sendback);
	}

}
