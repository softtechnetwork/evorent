<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Productinstall extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

     
		$this->load->library('form_validation');
		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $product_install = $this->product_install();
	   
	   $data = array(
		   "product_install" => $product_install
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productinstall/productinstall_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	
	public function delete(){
		$product_install_id = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
		# Delete This Product
		$this->db->where('id', $product_install_id);
		$this->db->delete('product_install');

		# Delete This Product
		$this->db->where('product_install_id', $product_install_id);
		$this->db->delete('product_install_set');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}
	public function delete_img(){
		
		if(isset($_POST['filename'])){
			unlink("./uploaded/tmpproductset_img/".$_POST['filename']);
		}
		
	}

	public function ajaxuploadtmpproductsetimg(){

		if(!is_dir('uploaded/tmpproductset_img/')){
			mkdir('uploaded/tmpproductset_img/',0777,true);
		}
		
		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmpproductset_img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmpproductset_img/')){
					rmdir('uploaded/tmpproductset_img/');
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}

		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));

	}

	public function ajaxuploadproductinstallimgOnActionEdit($masterproduct_id){
		// echo $masterproduct_id;exit();
		if(!is_dir('uploaded/tmp_productset_edit/' . $masterproduct_id)){
			mkdir('uploaded/tmp_productset_edit/' . $masterproduct_id,0777,true);
		}

		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmp_productset_edit/'.$masterproduct_id;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmp_productset_edit/' .$masterproduct_id)){
					rmdir('uploaded/tmp_productset_edit/' .$masterproduct_id);
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}
		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));
		
	}

	public function create()
	{
      
		$product = $this->product();
		$checkhaveserialnumber_arr = $this->checkhaveserialnumber();
	    if(!empty($product)){
			// recursive func
	     	//	$product = $this->recursiveproduct_delete($product , $checkhaveserialnumber_arr);
		}
		//echo '<PRE>';
		//print_r($product);exit();
		$uom = $this->uom();
		
	 
		 /* if recieve post data from frontend then */
		 if($this->input->post(NULL,FALSE)){
			 /* find default link  */
			
			// $picode_ref_arm = 
			// $piname = 
			// $piabbrv = 
			// $product_category_id = 
			// $price_a = 
			// $price_b = 
			// $price_c = 
			// $price_d = 
			// $pidesc = 
			// $uom = 
			// $active = 
			// $price = 
			// $tax_rate = 
			// $pic = 
			// $out_of_stock = 
			// $created = 
			$product_install_set = array();
            foreach($this->input->post() as $key => $post_value){
               
                # start instalments
                if (strpos($key, 'piddgroup') !==  false) { 
                    // build group
                    $product_install_set = $this->buildArrGroup($product_install_set , $key  , $post_value , "masterproduct_id");
                    
                }
                # end instalments
                if (strpos($key, 'amountgroup') !==  false) { 
                    // build group
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "amount");
                    
                }
				 # end instalments
				 if (strpos($key, 'activedategroup') !==  false) { 
                    // build group
					$post_value = empty($post_value) ? 0 : $post_value;
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "active_date");
                    
                }
                 # end instalments
                 if (strpos($key, 'expiredategroup') !==  false) { 
                    // build group
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "expire_date");
                } 
            }
			
			

			$getlastestproductset = $this->getLatestProductSetCode(null , null);
			
			if($getlastestproductset == 0){
				$getlastestproductset = "781000001";
			}else{
				$getlastestproductset = $getlastestproductset + 1;
			}

			 $file_dz = "";
			 //	print_r($this->input->post('file_dz[]'));exit();
			 
			// ########## insert ##################################
			$query = $this->db->insert('product_install',array(
				 'picode'=>"PI".$getlastestproductset,
				 'piname' => $this->input->post('piname'),
				 'piabbrv' => $this->input->post('piabbrv'),
				 'product_category_id' => $this->input->post('product_category_id'),
				 'uom_id' => $this->input->post('uom'),
				 'price_a' => $this->input->post('price_a'),
				 'price_b' => $this->input->post('price_b'),
				 'price_c' => $this->input->post('price_c'),
				 'price_d' => $this->input->post('price_d'),
				 'pidesc' => $this->input->post('pidesc'),
				 'brand' => $this->input->post('brand'),
				 'tax_rate' => '7',
				 'created'=>date('Y-m-d H:i:s')
			 )); 
			// ####################################################
			 $insert_id = $this->db->insert_id();
			 # insert product install 
			 foreach($product_install_set as $product_install_key => $value){
				// active and expire date
				$value["active_date"] = isset( $value["active_date"]) ?  $value["active_date"] : "";
				$value["expire_date"] = isset( $value["expire_date"]) ?  $value["expire_date"] : "";
				#  check row is exist before upload
				$check_exist = $this->db->select('*')->from('product_install_set')
				->where('masterproduct_id' , $value['masterproduct_id'])
				->where('product_install_id' , $insert_id)->get();

				if($check_exist->num_rows() == 0){
					$query = $this->db->insert('product_install_set',array(
						'masterproduct_id' => $value["masterproduct_id"],
						'product_install_id' => $insert_id,
						'amount' => $value["amount"],
						'active_date' => $value["active_date"],
						'expire_date' => $value["expire_date"],
						'created'=>date('Y-m-d H:i:s')
					)); 
				}
			 }

			 // UPLOAD IMAGE
			 $newarr = array(); 
		 
			 if(($this->input->post('file_dz[]') !== null)){
				 
					 if(!is_dir('uploaded/product_install/'.$insert_id.'')){
						 mkdir('uploaded/product_install/'.$insert_id.'',0777,true);
					 }
					 clearDirectory('uploaded/product_install/'.$insert_id.'/');
					 
					 foreach($this->input->post('file_dz[]') as $file_key => $file_value){
						 $newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
						 $file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
						 // move file to another directory 
						 $file_value = str_replace(' ', '_', $file_value);
						 if(rename("uploaded/tmpproductset_img/".$file_value, 'uploaded/product_install/'.$insert_id.'/'.$newfilename.".".$file_ext)){
							 // push newfilename to array
							 $newarr[] = $newfilename.".".$file_ext;
						 }
					 
					 }
					 // update newpic
					 $this->db->update('product_install',array(
						 'pic'=> json_encode($newarr)
					 ),array('id'=>$insert_id));
					 
			 }
		 
			 
			 $this->msg->add('Insert success','success');
			 redirect($this->uri->uri_string());
	 
 
		 }
		 /* eof recieve post data from frontend then */
		 
		$data = array(
			"product" => $product,
			"uom" =>   $this->uom(),
			"product_category" => $this->product_category()
		);
 
	   
		$this->template['content'] = $this->load->view('backend/productinstall/productinstall_create',$data, true);
		 /** load javascript **/
		 
		 
		$this->_load_js();
		$this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
		
		$this->template['external_js'][] = base_url('./assete/js/page/backend/productinstall.js');
		$this->template['external_js'][] = base_url('./assete/js/page/backend/productinstall_create.js');
		
		$this->load->view('backend/template', $this->template);
	   
	 }

	 public function ajaxuploadtmpimgset(){

		if(!is_dir('uploaded/tmpproductset_img/')){
			mkdir('uploaded/tmpproductset_img/',0777,true);
		}
		
		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmpproductset_img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmpproductset_img/')){
					rmdir('uploaded/tmpproductset_img/');
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}

		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));

	}



	 public function edit($id)
	 {
		$product = $this->product();
		$checkhaveserialnumber_arr = $this->checkhaveserialnumber();
		//check ว่าสินค้าตัวนี้มี serial number ไหม
	    if(!empty($product)){
			// recursive func
			//$product = $this->recursiveproduct_delete($product , $checkhaveserialnumber_arr);
		}
	
		
		$uom = $this->uom();
		$product_install = $this->product_install($id);
		$product_install_id =  $id;
		
		$product_install_set = $this->product_install_set($product_install_id);
		
		 /* if recieve post data from frontend then */
		 if($this->input->post(NULL,FALSE)){
	
			// ########## update ##################################
			
			$query = $this->db->update('product_install',array(
				'piname' => $this->input->post('piname'),
				'piabbrv' => $this->input->post('piabbrv'),
				'product_category_id' => $this->input->post('product_category_id'),
				'uom_id' => $this->input->post('uom'),
				'price_a' => $this->input->post('price_a'),
				'price_b' => $this->input->post('price_b'),
				'price_c' => $this->input->post('price_c'),
				'price_d' => $this->input->post('price_d'),
				'pidesc' => $this->input->post('pidesc'),
				'brand' => $this->input->post('brand'),
				'updated'=>date('Y-m-d H:i:s')
			),array('id'=>$id) );
			
			// ####################################################
			$product_install_set = array();
            foreach($this->input->post() as $key => $post_value){
               
                # start instalments
                if (strpos($key, 'piddgroup') !==  false) { 
                    // build group
                    $product_install_set = $this->buildArrGroup($product_install_set , $key  , $post_value , "masterproduct_id");
                    
                }
                # end instalments
                if (strpos($key, 'amountgroup') !==  false) { 
                    // build group
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "amount");
                    
                }
				 # end instalments
				 if (strpos($key, 'activedategroup') !==  false) { 
                    // build group
					$post_value = empty($post_value) ? 0 : $post_value;
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "active_date");
                    
                }
                 # end instalments
                 if (strpos($key, 'expiredategroup') !==  false) { 
                    // build group
                    $product_install_set =  $this->buildArrGroup($product_install_set , $key  , $post_value , "expire_date");
                } 
            }
			
			// clear all row before update
			$old_forchkresultarr = array();
			$old_productinstall = $this->db->select('masterproduct_id')->from('product_install_set')
			->Where('product_install_id' , $product_install_id)->get();
			if($old_productinstall->num_rows() > 0){
				$loop_i = 0;
				$oldresult_array= $old_productinstall->result_array();
				while($loop_i < count($oldresult_array)){
					$old_masterproduct_id = $oldresult_array[$loop_i]['masterproduct_id'];
					$old_forchkresultarr[$old_masterproduct_id] = $old_masterproduct_id; 
					++ $loop_i;
				}
				
			}
			
			 # insert product install 
			 foreach($product_install_set as $product_install_key => $value){
				 // active and expire date
				$value["active_date"] = isset( $value["active_date"]) ?  $value["active_date"] : "";
				$value["expire_date"] = isset( $value["expire_date"]) ?  $value["expire_date"] : "";
				#  check row is exist before upload
				$check_exist = $this->db->select('*')->from('product_install_set')
				->where('masterproduct_id' , $value['masterproduct_id'])
				->where('product_install_id' , $product_install_id)->get();
				# unset old key if none exist
				if(!empty($old_forchkresultarr)){
					if(isset($old_forchkresultarr[$value['masterproduct_id']])){
						unset($old_forchkresultarr[$value['masterproduct_id']]);
					}
				}	
				
				if($check_exist->num_rows() == 0){
					$query = $this->db->insert('product_install_set',array(
						'masterproduct_id' => $value["masterproduct_id"],
						'product_install_id' => $id,
						'amount' => $value["amount"],
						'active_date' => $value["active_date"],
						'expire_date' => $value["expire_date"],
						'created'=>date('Y-m-d H:i:s')
					)); 
				}else{
					$results = $check_exist->result();
					$row_id = $results[key($results)]->id;
					
					$query = $this->db->update('product_install_set',array(
						'amount' => $value["amount"],
						'active_date' => $value["active_date"],
						'expire_date' => $value["expire_date"]
					) ,array('id'=>$row_id) ); 
				}
				
			 }
			  
			 # product install set ของเก่า (ถ้ามี)
			 if(!empty($old_forchkresultarr)){
				foreach($old_forchkresultarr as $oldResultK => $oldResultValue){
					$this->db->select('*')->from('product_install_set')
					->Where('product_install_id' , $product_install_id)
					->Where('masterproduct_id' , $oldResultValue)
					->delete();
				}
			}	
			 // UPLOAD IMAGE 
			 # update product promotion if exist
			 $product_hirepurchase_interest = $this->db->select('*')->from('product_hirepurchase_interest')
			 ->where('product_install_id' , $product_install_id)->get();
			 
			 if($product_hirepurchase_interest->num_rows() > 0){
				 $product_hirepurchase_arr = $product_hirepurchase_interest->result_array();
				
				 foreach($product_hirepurchase_arr as $pk => $pv){
					 $formula_id = $pv['formula_id'];
					 $formula = $this->formula($formula_id);
					 $formula_calcurate = json_decode($formula[key($formula)]->formula);
					 $start_payment = isset($pv['term_of_payment_start']) ? intval($pv['term_of_payment_start']) : 1;
					 $end_payment = intval($pv['term_of_payment_end']);	
					 $group_id = $pk;
					 $interest = $pv['interest'];
					 $holdonpayment = array();
					 $save_array = array();

					 $help_pay_installments_status = isset($pv['help_pay_installments_status'])? $pv['help_pay_installments_status'] : 0;
					 if(!empty($pv["holdonpayment"])){
							 $holdonpayment_obj = json_decode($pv['holdonpayment']);
							 foreach($holdonpayment_obj as $hk => $hv){
								 $holdonpayment[$pk][]=   $hv;
							 }
					 }
					 
					 // เตรียมข้อมูลเพื่อทำ array ชื่อ savearray ไว้ convert ข้อมูลและ save ลง  database 
					 $save_array = $this->prepareBuildSaveArray($formula_calcurate 
					 ,$product_install_id
					 ,$holdonpayment , $group_id
					 ,$start_payment
					 ,$end_payment
					 ,$interest,null,null);
					 
					 # product hirepurchase code 
					if(!isset($pv["product_hirepurchase_code"])){
						$product_hirepurchase_code =  "PC".strtotime(date('Y-m-d H:i:s')) . $product_install_id.$formula_id;
						# insert 
						$query = $this->db->insert('product_hirepurchase_interest',array(
							'formula_id'=> intval($formula_id),
							'product_hirepurchase_code' => $product_hirepurchase_code,
							'product_install_id' => intval($product_install_id),
							'term_of_payment_start' => $start_payment,
							'term_of_payment_end' => $end_payment,
							'help_pay_installments_status'=> intval($help_pay_installments_status),
							'holdonpayment' => ($holdonpayment[$group_id] > 0 ) ?  json_encode($holdonpayment[$group_id]) : "",
							'installment_amount_per_installment' => json_encode($save_array),
							'interest' => floatval($interest),
							'created'=>date('Y-m-d H:i:s')
						)); 
					}else{
						# update
					
						$product_hirepurchase_id = $pv["id"];
						$query = $this->db->update('product_hirepurchase_interest',array(
							'formula_id'=> intval($formula_id),
							'product_install_id' => intval($product_install_id),
							'term_of_payment_start' => $start_payment,
							'term_of_payment_end' => $end_payment,
							'help_pay_installments_status'=> intval($help_pay_installments_status),
							'holdonpayment' => ($holdonpayment[$group_id] > 0 ) ?  json_encode($holdonpayment[$group_id]) : "",
							'installment_amount_per_installment' => json_encode($save_array),
							'interest' => floatval($interest),
							'created'=>date('Y-m-d H:i:s')
						),array('id'=>$product_hirepurchase_id)); 
					}

					 
				 }
				}
				 // UPLOAD IMAGE
				 $newarr = array(); 
				 
				 if(($this->input->post('file_dz[]') !== null)){
					 
						 if(!is_dir('uploaded/product_install/'.$product_install_id.'')){
							 mkdir('uploaded/product_install/'.$product_install_id.'',0777,true);
						 }
						 //clearDirectory('uploaded/product_install/'.$product_install_id.'/');
						 
						 foreach($this->input->post('file_dz[]') as $file_key => $file_value){
							 $newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
							 $file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
							 // move file to another directory 
							 $file_value = str_replace(' ', '_', $file_value);
							 # case : if already have file on tmp img edit then move to product_install directory
							 if(file_exists("uploaded/tmp_productset_edit/".$product_install_id.'/'.$file_value)){
								 
								 
								 if(rename("uploaded/tmp_productset_edit/".$product_install_id.'/'.$file_value, 'uploaded/product_install/'.$product_install_id.'/'.$newfilename.".".$file_ext)){
									 // push newfilename to array
									 $newarr[] = $newfilename.".".$file_ext;
								 
								 }
							 }else{
							 # case : if not found  this file should be on product_install directory 
								 $newarr[] = $file_value;
							 }
						 
						 }
						 // update newpic
						 $this->db->update('product_install',array(
							 'pic'=> json_encode($newarr)
						 ),array('id'=>$product_install_id));
						 
				 }
			 
 
				 $this->msg->add('Insert success','success');
				 redirect($this->uri->uri_string());
			 // ###################################################
 
			 $this->msg->add('Edit success','success');
			 redirect($this->uri->uri_string());
	 
 
		 }
		 /* eof recieve post data from frontend then */
		 
		$data = array(
			"product" => $product,
			"uom" => $this->uom(),
			"product_install" => $product_install,
			"product_install_set" => $product_install_set,
			"product_category" => $this->product_category()
		);
 
	   
		 
		$this->template['content'] = $this->load->view('backend/productinstall/productinstall_edit',$data, true);
		 /** load javascript **/
		 
		 
		$this->_load_js();
		$this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
		
		$this->template['external_js'][] = base_url('./assete/js/page/backend/productinstall.js');
		$this->template['external_js'][] = base_url('./assete/js/page/backend/productinstall_edit.js');
		
		$this->load->view('backend/template', $this->template);
	   
	 }
 

	

}
