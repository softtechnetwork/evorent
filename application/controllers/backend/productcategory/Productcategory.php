<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Productcategory extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		//$this->load->helper('our_helper');

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $productcategory = $this->productcategory();
	 
	   $data = array(
		   "productcategory" => $productcategory
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productcategory/productcategory_list',$data, true);
	    /** load javascript **/
		$this->_load_js();
	   $this->load->view('backend/template', $this->template);
      
	}

    public function create()
	{
	   //echo '5';exit();
       $productcategory = $this->productcategory();
	   
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		   // ########## insert ##################################
		   $query = $this->db->insert('product_category',array(
				'cate_name'=>$this->input->post('cate_name'),
				'created'=>date('Y-m-d H:i:s')
			)); 


			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "productcategory" => $productcategory,
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productcategory/productcategory_create',$data, true);
	  /** load javascript **/
		$this->_load_js();
	   $this->load->view('backend/template', $this->template);
      
	}

	public function edit($id)
	{
	   //echo '5';exit();
       $productcategory = $this->productcategory($id);
	   
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
		   // ########## insert ##################################
		   $query = $this->db->update('product_category',array(
				'cate_name'=>$this->input->post('cate_name'),
				'created'=>date('Y-m-d H:i:s')
			),array('id'=>$id)); 


			$this->msg->add('Update success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "productcategory" => $productcategory[key($productcategory)],
	   );

	  
	   $this->template['content'] = $this->load->view('backend/productcategory/productcategory_edit',$data, true);
	   /** load javascript **/
		$this->_load_js();
		$this->load->view('backend/template', $this->template);
      
	}

	public function delete(){
		$cate_id = $_POST["cate_id"];
		# Delete This Product
		$this->db->where('id', $cate_id);
		$this->db->delete('product_category');


		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);
	}
	


	private function uploadProductPicture($data = array()){
		$product_part_id = $data['id'];
		
		if(!is_dir('uploaded/product_part/'.$product_part_id.'')){
			mkdir('uploaded/product_part/'.$product_part_id.'',0777,true);
		}
		clearDirectory('uploaded/product_part/'.$product_part_id.'/');
		//echo $product_part_id;exit();									 
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/product_part/'.$product_part_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('cover_image')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');

			if(file_exists('uploaded/product_part/'.$product_part_id)){
					rmdir('uploaded/product_part/'.$product_part_id);
			}
		
		}else{
			$data_upload = array('upload_data' => $this->upload->data());

			$this->db->update('product_part',array(
				'part_img'=>$data_upload['upload_data']['file_name']
			),array('id'=>$product_part_id));
			
		}
}


}
