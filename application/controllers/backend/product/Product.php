<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Product extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

     

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
    
       $product = $this->product();
	   
	   $data = array(
		   "product" => $product
	   );

	  
	   $this->template['content'] = $this->load->view('backend/product/product_list',$data, true);
	    /** load javascript **/
		$this->_load_js();

	   $this->load->view('backend/template', $this->template);
      
	}

	public function edit($id)
	{
      
       $product = $this->product($id);
	   $uom = $this->uom();
	   $warranty = $this->warranty();
	   $get_product_part_not_used  = $this->product(null , null , 2 ,1  );
	   $tbl_productpart_rel_with_this = $this->product(null , null , 2 ,null , $id );
	
	 
	   if(!empty($tbl_productpart_rel_with_this)){
		   if(!empty($get_product_part_not_used)){
		   	$get_product_part_not_used = array_merge($get_product_part_not_used,$tbl_productpart_rel_with_this);
		   }else{
			$get_product_part_not_used  = $tbl_productpart_rel_with_this;
		   }
	   }
	   $checkhaveserialnumber_arr = $this->checkhaveserialnumber();
	    if(!empty($get_product_part_not_used)){
			// recursive func
			$get_product_part_not_used = $this->recursiveproduct_delete($get_product_part_not_used , $checkhaveserialnumber_arr);
		}
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
			$masterproduct_id = $id;
            $mpcode = $this->input->post('mpcode');
			$mpname = $this->input->post('mpname');
			$mpabbrv = $this->input->post('mpabbrv');
			$product_category_id = $this->input->post('product_category_id');
			$uom = $this->input->post('uom');
			$price = $this->input->post('price');
			$mpdesc = $this->input->post('mpdesc');
			// ######################## อะไหล่สินค้า  #########################
			$product_part = array();
			foreach($this->input->post() as $key => $post_value){
				
				# group ของ อะไหล่
				if (strpos($key, 'piddgroup') !==  false) { 
					// build group
					$product_part = $this->buildArrGroup($product_part , $key  , $post_value , "product_part_id");
					
				}
				
			}
			// ####################################################
			$insert_id = $id;
			#case : อัพเดท id อะไหล่ให้ผูกกับ id สินค้า
			# select product part ทุกตัวที่ผูกกับ product นี้ update ความสัมพันธ์เป็น null ก่อนค่อยอัพเดทตัวใหม่
			$productpart_rel_master = $this->db->select('*')->from('masterproduct')->where('product_parent_id' , $id)->get();
			if($productpart_rel_master->num_rows() > 0){
				foreach($productpart_rel_master->result() as $prmkey => $prmvalue){
					$this->db->update('masterproduct',array(
						'product_parent_id'=> ''
					),array('id'=>$prmvalue->id));
				}
			}
			if(!empty($product_part)){
			
				foreach($product_part as $partkey => $partvalue){
					$product_part_id = $partvalue['product_part_id'];
					
					$this->db->update('masterproduct',array(
						'product_parent_id'=> $insert_id
					),array('id'=>$product_part_id));
				}
			}
			
			//##############################################################

				
           // $img = $this->findDefaultLink();
		   // ########## insert ##################################
		   $query = $this->db->update('masterproduct',array(
		
				'mpname' => $this->input->post('mpname'),
				'mpabbrv' => $this->input->post('mpabbrv'),
				'mp_desc' => $this->input->post('mp_desc'),
				'product_category_id' => $this->input->post('product_category_id'),
				'uom_id' => $this->input->post('uom'),
				'price_a' => $this->input->post('price_a'),
				'price_b' => $this->input->post('price_b'),
				'price_c' => $this->input->post('price_c'),
				'price_d' => $this->input->post('price_d'),
				'brand' => $this->input->post('brand'),
				'warranty_id' => $this->input->post('warranty_id'),
				'created'=>date('Y-m-d H:i:s')
			) ,array('id'=>$masterproduct_id) ); 
		   // ####################################################
		
			// UPLOAD IMAGE 
			
				// UPLOAD IMAGE
				$newarr = array(); 
				
				if(($this->input->post('file_dz[]') !== null)){
					
						if(!is_dir('uploaded/masterproduct/'.$masterproduct_id.'')){
							mkdir('uploaded/masterproduct/'.$masterproduct_id.'',0777,true);
						}
						//clearDirectory('uploaded/masterproduct/'.$masterproduct_id.'/');
						
						foreach($this->input->post('file_dz[]') as $file_key => $file_value){
							$newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
							$file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
							// move file to another directory 
							$file_value = str_replace(' ', '_', $file_value);
							# case : if already have file on tmp img edit then move to masterproduct directory
							if(file_exists("uploaded/tmp_img_edit/".$masterproduct_id.'/'.$file_value)){
								
								
								if(rename("uploaded/tmp_img_edit/".$masterproduct_id.'/'.$file_value, 'uploaded/masterproduct/'.$masterproduct_id.'/'.$newfilename.".".$file_ext)){
									// push newfilename to array
									$newarr[] = $newfilename.".".$file_ext;
								
								}
							}else{
							# case : if not found  this file should be on masterproduct directory 
								$newarr[] = $file_value;
							}
						
						}
						// update newpic
						$this->db->update('masterproduct',array(
							'pic'=> json_encode($newarr)
						),array('id'=>$masterproduct_id));
						
				}
			

				$this->msg->add('Update success','success');
				redirect($this->uri->uri_string());
			// ###################################################

			$this->msg->add('Edit success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "product" => $product,
		   "uom" => $uom,
		   "warranty" => $warranty,
		   "tbl_productpart" => $get_product_part_not_used,
		   "tbl_productpart_rel_with_this" => $tbl_productpart_rel_with_this,
		   "product_category" => $this->product_category()
	   );

	  
	   $this->template['content'] = $this->load->view('backend/product/product_edit',$data, true);
	    /** load javascript **/
		$this->_load_js();
		$this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
		$this->template['external_js'][] = base_url('./assete/js/page/backend/product_edit.js');
		$this->template['external_js'][] = base_url('./assete/js/page/backend/product.js');
	   $this->load->view('backend/template', $this->template);
      
	}

	public function delete(){
		$product_id = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
		# Delete This Product
		$this->db->where('id', $product_id);
		$this->db->delete('masterproduct');

		$data_sendback = array(
			"status" => true
		);
		echo json_encode($data_sendback);

	}
	public function delete_img(){
		
		if(isset($_POST['filename'])){
			unlink("./uploaded/tmp_img/".$_POST['filename']);
		}
		
	}

	public function ajaxuploadtmpimg(){

		if(!is_dir('uploaded/tmp_img/')){
			mkdir('uploaded/tmp_img/',0777,true);
		}
		
		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmp_img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmp_img/')){
					rmdir('uploaded/tmp_img/');
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}

		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));

	}

	public function ajaxuploadimgOnActionEdit($masterproduct_id){
		// echo $masterproduct_id;exit();
		if(!is_dir('uploaded/tmp_img_edit/' . $masterproduct_id)){
			mkdir('uploaded/tmp_img_edit/' . $masterproduct_id,0777,true);
		}

		$server_id = md5(strtotime(date('Y-m-d H:i:s')));
		$filename = $_FILES["file"]["name"];
		$fileArr = explode("." , $filename);
		$config['file_name'] = $filename;
		$config['upload_path'] = 'uploaded/tmp_img_edit/'.$masterproduct_id;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		
		$file_ext = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			/** if type : create */
			if(file_exists('uploaded/tmp_img_edit/' .$masterproduct_id)){
					rmdir('uploaded/tmp_img_edit/' .$masterproduct_id);
			}	
		}
		else{
			// $data_upload = array('upload_data' => $this->upload->data());
			// $this->db->update('masterproduct',array(
			// 	'pic'=>$data_upload['upload_data']['file_name']
			// ),array('id'=>$masterproduct_id));
			
		}
		echo json_encode(array("status" => true , "filename" => $fileArr[0] , "file_ext"=> $file_ext ,"id" => $server_id));
		
	}

	
    public function create()
	{
      
       $product = $this->product();
	   $uom = $this->uom();
	   $warranty = $this->warranty();
	   $get_product_part_not_used  = $this->product(null , null , 2 , 1);
	   $checkhaveserialnumber_arr = $this->checkhaveserialnumber();
	   if(!empty($get_product_part_not_used)){
		   // recursive func
		   $get_product_part_not_used = $this->recursiveproduct_delete($get_product_part_not_used , $checkhaveserialnumber_arr);
	   }
	 
	   
        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
			
            $mpcode = $this->input->post('mpcode');
			$mpname = $this->input->post('mpname');
			$mpabbrv = $this->input->post('mpabbrv');
			$product_category_id = $this->input->post('product_category_id');
			$uom = $this->input->post('uom');
			$price = $this->input->post('price');
			$mp_desc = $this->input->post('mpdesc');
			$getlastedmpcode = $this->getLatestProductMPCODE(null , null , 1 , "MP");
		
			$mp_code = ($getlastedmpcode > 0 ) ? $getlastedmpcode + 1 : "881000001";

			$file_dz = "";
			//	print_r($this->input->post('file_dz[]'));exit();
			
			// ####################################################
			$product_part = array();
            foreach($this->input->post() as $key => $post_value){
               
                # group ของ อะไหล่
                if (strpos($key, 'piddgroup') !==  false) { 
                    // build group
                    $product_part = $this->buildArrGroup($product_part , $key  , $post_value , "product_part_id");
                    
                }
               
            }
			

		   // ########## insert ##################################
		   $query = $this->db->insert('masterproduct',array(
				'mpcode'=>"MP".$mp_code,
				'mpname' => $this->input->post('mpname'),
				'mpabbrv' => $this->input->post('mpabbrv'),
				'product_category_id' => $this->input->post('product_category_id'),
				'typeproduct_id'=> 1,
				'uom_id' => $this->input->post('uom'),
				'price_a' => $this->input->post('price_a'),
				'price_b' => $this->input->post('price_b'),
				'price_c' => $this->input->post('price_c'),
				'price_d' => $this->input->post('price_d'),
				'mp_desc' => $this->input->post('mp_desc'),
				'brand' => $this->input->post('brand'),
				'warranty_id' => $this->input->post('warranty_id'),
				'created'=>date('Y-m-d H:i:s')
			)); 
		   // ####################################################
			$insert_id = $this->db->insert_id();
			#case : อัพเดท id อะไหล่ให้ผูกกับ id สินค้า
			if(!empty($product_part)){
				foreach($product_part as $partkey => $partvalue){
					$product_part_id = $partvalue['product_part_id'];
					
					$this->db->update('masterproduct',array(
						'product_parent_id'=> $insert_id
					),array('id'=>$product_part_id));
				}
			}
			
			// UPLOAD IMAGE
			$newarr = array(); 
		
			if(($this->input->post('file_dz[]') !== null)){
				
					if(!is_dir('uploaded/masterproduct/'.$insert_id.'')){
						mkdir('uploaded/masterproduct/'.$insert_id.'',0777,true);
					}
					clearDirectory('uploaded/masterproduct/'.$insert_id.'/');
					
					foreach($this->input->post('file_dz[]') as $file_key => $file_value){
						$newfilename = md5(strtotime(date('Y-m-d H:i:s'))) . rand(0, 1000);
						$file_ext = pathinfo($file_value, PATHINFO_EXTENSION);
						// move file to another directory 
						$file_value = str_replace(' ', '_', $file_value);
						if(rename("uploaded/tmp_img/".$file_value, 'uploaded/masterproduct/'.$insert_id.'/'.$newfilename.".".$file_ext)){
							// push newfilename to array
							$newarr[] = $newfilename.".".$file_ext;
						}
					
					}
					// update newpic
					$this->db->update('masterproduct',array(
						'pic'=> json_encode($newarr)
					),array('id'=>$insert_id));
					
			}
		

			$this->msg->add('Insert success','success');
			redirect($this->uri->uri_string());
	

        }
        /* eof recieve post data from frontend then */

	   $data = array(
		   "product" => $product,
		   "uom" => $uom,
		   "warranty" => $warranty,
		   "tbl_productpart" => $get_product_part_not_used,
		   "product_category" => $this->product_category()
	   );

	  
	   $this->template['content'] = $this->load->view('backend/product/product_create',$data, true);
	    /** load javascript **/
		
        
	   $this->_load_js();
	   $this->template['external_js'][] = base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.js');
   
	   $this->template['external_js'][] = base_url('./assete/js/page/backend/product_create.js');
	   $this->template['external_js'][] = base_url('./assete/js/page/backend/product.js');
	   $this->load->view('backend/template', $this->template);
      
	}






	

}
