<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Customer_purchase extends Backend_controller {
	public $template;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

   

		/** load default template */
	

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('Home_Model');
        
    } 

	
	public function index()
	{
	
       $customer_purchase = $this->customer_purchase();
	   
	   $data = array(
		   "customer_purchase" => $customer_purchase
	   );

	  
	   $this->template['content'] = $this->load->view('backend/customerpurchase/customer_purchase_list',$data, true);
	   $this->load->view('backend/template', $this->template);
      
	}

	public function customer_purchase($id = null,$active = null){
        $query = $this->db->select('customer.* ,customer_hirepurchase_detail.amount_installments , amount_installments_period_id as period_id ')
		->from('customer')
		->join('customer_hirepurchase_detail', 'customer.id = customer_hirepurchase_detail.customer_id');
        if($id != null)
        {
            $query = $query->where('customer.id',$id);
        }
        if($active != null)
        {
            $query = $query->where('active',"1");
        }
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return 0;
        }
    }
}
