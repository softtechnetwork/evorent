<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

       
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');

        $this->load->library('form_validation');
        $this->load->library('session');
       // $this->load->library('encrypt');
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
        $this->load->model('admin/StatusCategory_Model');
        $this->load->model('admin/Service_Model');
        $this->load->model('admin/User_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
		
        
    } 

	public function index()
	{
        //$this->load->view('admin/header');
		$this->load->view('admin/login');
        //$this->load->view('admin/footer');
	}

    public function regis()
	{
        //$this->load->view('admin/header');
		$this->load->view('admin/regis');
        //$this->load->view('admin/footer');
	}
    public function validate_regis()
	{
        
        $this->form_validation->set_rules('Username','Username','required|trim');
        $this->form_validation->set_rules('Email','Email','required|trim|valid_email');
        $this->form_validation->set_rules('Pass','Password','required');
        
        
        
            if($this->form_validation->run() == true){ 
                $email = $this->input->post('Email');

                $checkLogin = $this->User_Model->getRowsEmail($email); 
                
                if($checkLogin){ 
                    $data['error_msg'] = 'Email is already in use.';
                }else{
                    $userData = array( 
                        'username' => strip_tags($this->input->post('Username')), 
                        'email' => strip_tags($email), 
                        'password' => md5($this->input->post('Pass')), 
                        'cdate' => date("Y-m-d H:m:s")
                    );
                    $result = $this->User_Model->insert($userData);
                    if($result){
                        $this->session->set_userdata('success_msg', 'Your account registration has been successful. Please login to your account.'); 
                        redirect(base_url('admin/user/login')); 
                    }else{
                        $data['error_msg'] = 'Some problems occured, please try again.'; 
                        $this->session->set_flashdata('massage',$result );
                        redirect(base_url('admin/user/login')); 
                    }
                }
            }else{ 
                $data['error_msg'] = 'Please fill all the mandatory fields.'; 
            } 
       
        $this->load->view('admin/regis', $data); 
	}

    public function login()
	{
        //$this->load->view('admin/header');
		$this->load->view('admin/login');
        //$this->load->view('admin/footer');
	}
    public function validate_login()
	{
       
        if($this->session->userdata('success_msg')){ 
            $data['success_msg'] = $this->session->userdata('success_msg'); 
            $this->session->unset_userdata('success_msg'); 
        } 
        if($this->session->userdata('error_msg')){ 
            $data['error_msg'] = $this->session->userdata('error_msg'); 
            $this->session->unset_userdata('error_msg'); 
        } 
       
        $this->form_validation->set_rules('Useremail','Email','required|trim|valid_email');
        $this->form_validation->set_rules('Password','Password','required');
        

        if($this->form_validation->run() == true){ 
            
            $email = $this->input->post('Useremail');
            $password =  md5($this->input->post('Password')); 

            $checkLogin = $this->User_Model->getRows($email, $password); 
            if($checkLogin){ 
                $this->session->set_userdata('isUserLoggedIn', TRUE); 
                $this->session->set_userdata('userId', $checkLogin[0]['id']); 
                $this->session->set_userdata('userType', $checkLogin[0]['type']); 
                $this->session->set_userdata('userName', $checkLogin[0]['username']); 
                redirect(base_url('admin/dashboard'));
            }else{ 
                $data['error_msg'] = 'Wrong email or password, please try again.'; 
            } 
        }else{ 
            $data['error_msg'] = 'Please fill all the mandatory fields.'; 
        } 
        $this->load->view('admin/login', $data); 
	}

    public function logout($user = null, $pass=null)
	{
        $this->session->unset_userdata('isUserLoggedIn'); 
        $this->session->unset_userdata('userId'); 
        $this->session->unset_userdata('userType'); 
        $this->session->unset_userdata('userName');
        $this->session->sess_destroy(); 
        redirect(base_url('admin/user/login')); 
	}


}
