<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inhabited extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('session');
		
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->helper('file'); 
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Temp_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Product_Model');
		$this->load->model('admin/Logs_Model');
		$this->load->model('admin/Premise_Model');

        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Service_Model');
		$this->load->model('admin/Customer_Model');
        $this->load->model('admin/Inhabited_Model');
    } 

	
	public function index()
	{
        $menu['mainmenu'] = 'inhabited';
		$menu['submenu'] = 'inhabited';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/inhabited_list');
        $this->load->view('admin/footer');
	}
	public function getInhabitedList()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');
		$data = $this->Inhabited_Model->select($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}
	
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, ihb.*, 
		customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, 
		provinces.province_name, amphurs.amphur_name, tumbon.district_name 
		FROM inhabited ihb 
		INNER JOIN customer ON ihb.customer_code = customer.customer_code 
		LEFT JOIN provinces ON ihb.province_id = provinces.id
		LEFT JOIN amphurs ON ihb.amphurs_id = amphurs.id
		LEFT JOIN tumbon ON ihb.district_id = tumbon.id ";
        //print_r($str_query);
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}

	public function getInhabitedAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Inhabited_Model->selectAllItems($Search);
		echo json_encode($data);
	}

    public function getamphoe(){
		$postData = $this->input->post();
		$data = $this->Inhabited_Model->amphoe($postData);
		echo json_encode($data);
	}
   	public function getdistric(){
		$postData = $this->input->post();
		$data = $this->Inhabited_Model->districs($postData);
		echo json_encode($data);
	}
  	public function getzipcode(){
		$postData = $this->input->post();
		$data = $this->Inhabited_Model->zipcodes($postData);
		echo json_encode($data);
	}

	#### Create ####
	public function create(){
		$data['resCustomer'] =  $this->Inhabited_Model->customer_select();
		$data['province'] = $this->Inhabited_Model->province();

        $menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'inhabited';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/inhabited_create',$data);
        $this->load->view('admin/footer');
	}
	public function insert(){
		
		$prefixdate = date("ym");
		$prefixchar = 'A';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Inhabited_Model->selectByInhabited_code($prefix);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->inhabited_code,5,9));
			}
			$code = sprintf($prefix.'%04d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%04d',1);
		}

		$date = date("Y-m-d H:m:s");
		$data = array( 
			'inhabited_code' => $code, 
			'customer_code' => $this->input->post('customer'),
			'category' => $this->input->post('type'),
			'building_type' => $this->input->post('building-type'),
			'company_name' => $this->input->post('company-name'),
			'village_name' => $this->input->post('village-name'),
			'address' => $this->input->post('address'),
			'province_id' => $this->input->post('province'),
			'amphurs_id' => $this->input->post('amphurs'),
			'district_id' => $this->input->post('district'),
			'zip_code' => $this->input->post('zipcode'),
            'cdate'=>$date
        );
		$this->Inhabited_Model->inhabitedInsert($data);
        redirect( base_url('admin/inhabited'));
    }


	#### Edit ####
	public function edit($id = null)
	{
		$data['resInhabited'] =  $this->Inhabited_Model->selectByInhabited_code($id);
        $data['province'] = $this->Inhabited_Model->province();
        $data['amphoe'] = $this->Inhabited_Model->amphoeAll();
        $data['districs'] = $this->Inhabited_Model->districsAll();

        $menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'inhabited';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/inhabited_edit',$data);
        $this->load->view('admin/footer');
	}
	public function update(){

        $inhabited_code = $this->input->post('inhabited_code');
		$date = date("Y-m-d H:m:s");
		$data = array( 
			//'inhabited_code' => $this->input->post('inhabited_code'),
			//'customer_code' => $this->input->post('customer_code'),
			'building_type' => $this->input->post('building-type'),
			'company_name' => $this->input->post('company-name'),
			'village_name' => $this->input->post('village-name'),
            'address' => $this->input->post('address'),
			'province_id' => $this->input->post('province'),
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'),
			'zip_code' => $this->input->post('zipcode'),
            'udate'=>$date
         ); 
		$this->Inhabited_Model->update($data,$inhabited_code);
        redirect( base_url('admin/inhabited'));
    }

}
