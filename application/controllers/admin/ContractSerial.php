<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ContractSerial extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductSub_Model');
		$this->load->model('admin/Temp_Model');
        $this->load->model('admin/SerialNumber_Model');
        $this->load->model('admin/ContractSerial_Model');
    } 

	public function index(){
        //$data['product'] = $this->ProductSub_Model->productTocombo();

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractSerial';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractSerial_list');
        $this->load->view('admin/footer');
	}

    public function getResSerial(){
		$Search = $this->input->post('Search');
		$data = $this->ContractSerial_Model->getToList($Search);		
		echo json_encode($data);
	}

	public function edit($id = null){
		$data['resTemp'] = $this->ContractSerial_Model->GetContractByTemp($id);
		
		$this->updateLastSerial($id); //update serialnumber to equae product set sub

		$data['resSerial'] = $this->ContractSerial_Model->GetSerialByContract($id);
		$data['resSubproduct'] =  $this->ContractSerial_Model->GetSerialToAdd($id, $data['resTemp'][0]->product_id);
		$data['GetSerialNoMax'] =  $this->ContractSerial_Model->GetSerialNoMax($id, $data['resTemp'][0]->product_id);

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractSerial';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractSerial_edit',$data);
        $this->load->view('admin/footer');
	}
	public function updateLastSerial($contract_id = null){
		
		$strSupID = '';
		$resSerial = $this->ContractSerial_Model->GetSerialByTemp($contract_id);
		foreach($resSerial as $key => $item){
			$strSupID .= ($key > 0)? ", N'".$item->product_sub_id."'": "N'".$item->product_sub_id."'";
		}

		$product_id = $resSerial[0]->product_id;

		$GetSerialNoMax =  $this->ContractSerial_Model->GetSerialNoMax($contract_id, $product_id);
		$LastProductSub = $this->ContractSerial_Model->GetLastProductSub($contract_id, $product_id,  $strSupID);
		if( count($LastProductSub) > 0 ){
			foreach($LastProductSub as $items){
				
				$data = array( 
					'contract_code' => $contract_id,
					'product_id' => $product_id,
					'product_sub_id' => $items->product_sub_id,
					//'serial_number' => $this->input->post('serial_'.$items->id),
					//'remark' => $this->input->post('remark_'.$items->id),
					'no' => $GetSerialNoMax[0]->no,
					'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s")),
					'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
				);
				$this->ContractSerial_Model->InsertSerial($data);
			}
		}
	}

    public function update(){
        $contract_id = $this->input->post('contract-id');
        $product_id = $this->input->post('product_id');

		$resSerial = $this->ContractSerial_Model->GetSubByProduct($product_id);
		foreach($resSerial as $item):
			$no = $this->input->post('no_'.$item->id);
			$data = array(
				'serial_number' => $this->input->post('serial_'.$item->id),
				'no'=> $no,
				'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			$this->ContractSerial_Model->updateSerial($data, $contract_id, $product_id, $item->id, $no);
		endforeach;
        redirect(base_url('admin/contractSerial'));
    }

	public function updateSerial(){
        $contract_id = $this->input->post('contract_code');
        $serial_number = $this->input->post('serial-number');
        $serial_remark = $this->input->post('serial-remark');
        $product_id = $this->input->post('serial_product_id');
        $serial_id = $this->input->post('serial_id');
        $product_sub_id = $this->input->post('product_sub_id');
        $no = $this->input->post('product_no');

		$obj = array(
			'serial_number' => $serial_number,
			'remark' => $serial_remark,
			'udate'=> date(date_format(date_create(),"Y-m-d H:i:s")),
		);

		$this->ContractSerial_Model->updateSerialNew($obj, $serial_id, $contract_id, $product_id, $product_sub_id, $no);
        redirect(base_url('admin/contractSerial/edit/'.$contract_id));
    }
	
	public function addition($id = null){
		$data['resTemp'] = $this->ContractSerial_Model->GetContractByTemp($id);
		$data['resSerial'] = $this->ContractSerial_Model->GetSerialByTemp($id);
		$data['resSubproduct'] =  $this->ContractSerial_Model->GetSerialToAdd($id, $data['resTemp'][0]->product_id);

        $menu['mainmenu'] = 'contractSerial';
		$menu['submenu'] = 'contractSerial';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractSerial_addition',$data);
        $this->load->view('admin/footer');
	}
	public function additional(){
		$contract_id = $this->input->post('contract-id');
		$product_id = $this->input->post('product_id');
		$productSub = $this->ContractSerial_Model->GetSerialToAdd($contract_id, $product_id);
		$resMaxSerial = $this->ContractSerial_Model->GetMaxSerialByTemp($contract_id);
		foreach($productSub as $key => $items){
			$data = array( 
				'contract_code' => $contract_id,
				'product_id' => $product_id,
				'product_sub_id' => $items->id,
				'serial_number' => $this->input->post('serial_'.$items->id),
				'remark' => $this->input->post('remark_'.$items->id),
				'no' => $resMaxSerial[0]->no + 1,
				'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);

			$this->ContractSerial_Model->InsertSerial($data);
		}

		redirect(base_url('admin/contractSerial'));
    }

	public function GetSubByProduct()
	{
		$product_id = $this->input->post('product_id');
		$data = $this->ContractSerial_Model->GetSubByProduct($product_id);		
		echo json_encode($data);
	}

   /*
    public function create(){
        $data['TempTocombo'] = $this->SerialNumber_Model->TempTocombo();
        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractSerial';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractSerial_create',$data);
        $this->load->view('admin/footer');
	}
	public function insert(){
		$product_id = $this->input->post('product_id');
		$productSub = $this->Temp_Model->GetSubByProduct($product_id);	
		foreach($productSub as $key => $items){
			$data = array( 
				'contract_code' =>  $this->input->post('contract-id'),
				'product_id' => $product_id,
				'product_sub_id' => $items->product_sub_id,
				'serial_number' => $this->input->post('serial_'.$items->product_sub_id),
				'no' => 1,
				'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			$this->SerialNumber_Model->InsertSerial($data);
		}	
        redirect ( base_url('admin/contractSerial') );
    }

    public function DeleteProductSub(){
        $id = $this->input->post('id');
		$data = $this->ProductSub_Model->DeleteProductSub($id);
		echo json_encode($data);
    }
	*/

    

}
