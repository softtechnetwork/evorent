<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContractInst extends CI_Controller {
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
     	$this->load->helper('file'); 
		$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Temp_Model');
        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Service_Model');
		$this->load->model('admin/Sms_Model');

		$this->load->model('admin/ContractService_Model');
		$this->load->model('admin/Contract_Model');
		$this->load->model('admin/ContractInstallment_Model');

		//เรียกใช้งาน config SMS     
		$this->load->config('sms');
		

		// load db ( agent )
		/*$this->load->config('externaldb');
		$this->db_config = $this->config->item('agent_db');
		$this->connectSeminarDB();*/
      
    } 

	
	public function Installment_list($contract_code = null)
	{
		$data['contract_code'] =$contract_code;
		$data['getStatus'] = $this->Status_Model->getStatus('210603');		
        $data['resStatus'] = $this->Status_Model->requestStatus('210603');
		//$data['results'] = $this->ContractInstallment_Model->getInstallments($contract_code, null);	

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractInst_list',$data);
        $this->load->view('admin/footer');
	}
	public function getInstallment()
	{
		$contract_code = $this->input->post('contract_code');
		$obj = (object)$this->input->post('search');
		$resSerial = $this->ContractInstallment_Model->getInstallments($contract_code, $obj);	
		echo json_encode($resSerial);
	}



	public function AddonInstallment_list($contract_code = null, $addon_code = null)
	{
		$data['contract_code'] =$contract_code;
		$data['addon_code'] =$addon_code;
		$data['getStatus'] = $this->Status_Model->getStatus('210603');		
        $data['resStatus'] = $this->Status_Model->requestStatus('210603');

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractAddonInst_list',$data);
        $this->load->view('admin/footer');

	}
	public function getAddonInstallment()
	{
		$contract_code = $this->input->post('contract_code');
		$obj = (object)$this->input->post('search');
		$resSerial = $this->ContractInstallment_Model->getAddonInstallments($contract_code, $obj);	
		echo json_encode($resSerial);
	}

	public function updatContractIns(){
		
		$date = date("Y-m-d H:m:s");
		$direct_url = $this->input->post('direct-url');
		$contract_code = $this->input->post('contract_code');
		$loan_code = $this->input->post('loan-code');
		$period = $this->input->post('inst-period');
		$inst_payment = (int)$this->input->post('inst-payment');
		$payment = (int)$this->input->post('payment');

		$installment_date = $this->input->post('inst-date');
		if(!empty($installment_date)){
			$temp = explode('/',$installment_date) ; 
            $installment_date = $temp[2].'-'.$temp[1].'-'.$temp[0];
		}

		$data = array( 
			//'loan_code' => $this->input->post('inst-period'), 
			'payment_amount' => $inst_payment, 
			'installment_date' => $installment_date,
			'status_code' => $this->input->post('inst-status'),
            'remark' => $this->input->post('inst-remark'),
            'udate'=>$date
        );

		########### ส่วนต่าง ค่างวด ############
		$diff = 0;
		$newPeriod = $period;
		$dataNewPeriod = array();

		if($inst_payment > $payment){
			$diff = $inst_payment - $payment;
			$newPeriod++;

			$dataNewPeriod['installment_payment'] = $payment-$diff;
			$dataNewPeriod['udate'] = $date;
        }else if($inst_payment < $payment){
			$diff = $payment - $inst_payment;
			$newPeriod++;

			$dataNewPeriod['installment_payment'] = $payment+$diff;
			$dataNewPeriod['udate'] = $date;
        }

		$data['amount_deff'] = $diff;

		$this->ContractInstallment_Model->updatInstallment($data, $contract_code, $period);

		if($inst_payment != $payment){
			$this->ContractInstallment_Model->updatInstallment($dataNewPeriod, $contract_code, $newPeriod);
        }

		redirect( base_url($direct_url));
    }

	public function sendSMS(){
		
		$date = date("Y-m-d H:m:s");
		$direct_url = $this->input->post('direct-url');
		$customer = $this->input->post('customer-sms');
		$customer_code = $this->input->post('customer-code');
		$temp_code = $this->input->post('temp-code-sms');
		$period = $this->input->post('inst-period-sms');
		$rental_period = $this->input->post('rental_period');
		$payment = $this->input->post('inst-payment-sms');
		$duedate = $this->input->post('duedate-sms');
		$keywords = $this->input->post('keywords-sms');
		$tel = $this->input->post('tel-sms');
		$cate = $this->input->post('cate');
		
		// $mess = 'เรียนลูกค้าอีโวเร้นท์ '.$customer. PHP_EOL ;
		$mess = 'เรียนลูกค้าอีโวเร้นท์ '.PHP_EOL ;
		// $mess .= $keywords. PHP_EOL ; 
		$temp_code_sms = mb_substr($temp_code, 0, 3).'xxxx'.substr($temp_code, 7);
		$mess .= 'เลขที่สัญญา '.$temp_code_sms. '  งวดที่ '.$period.'/'.$rental_period. PHP_EOL ;
		$mess .= 'ยอดเรียกเก็บ '.$payment.' บาท '. PHP_EOL ;
		$mess .= 'กำหนดชำระ '.$duedate. PHP_EOL ;
		$mess .= PHP_EOL ;
		$mess .= 'ลูกค้าสามารถโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์ '. PHP_EOL  ;
		$mess .= 'ธนาคารกสิกรไทย สาขาแม็กซ์แวลู พัฒนาการ '. PHP_EOL  ;
		$mess .= 'เลขที่บัญชี 095-1-36903-9  '. PHP_EOL  ;
		$mess .= 'ชื่อบัญชี บริษัท อีโวเร้นท์ จำกัด '. PHP_EOL  ;
		// $mess .= PHP_EOL ;
		// $mess .= 'หรือติดต่ออีโวเร้นท์  '. PHP_EOL  ;
		// $mess .= 'เบอร์โทร : 097-162-8565 '. PHP_EOL  ;
		// $mess .= 'Line ID : @evorent'. PHP_EOL ;
		// $mess .= PHP_EOL ;
		// $mess .= 'ขออภัยหากท่านชำระแล้ว ';
		
		$obj = new stdClass();
		$obj->telephone_number = $tel;
		$obj->message = $mess;

		////$sms_config_ants = $this->config->item('ANTS');
		$sms_config_ants = $this->config->item('evorent');
		$sendsms = $this->sendnotifymessage($obj, $sms_config_ants);
		
		if($sendsms['status']){
			###### create sms_code #####
			$prefixdate = date("ym"); 
			$prefixText = 'S';
			$prefix =  $prefixText.$prefixdate;
			$idArr = [];
			$code = '';
			$res = $this->Sms_Model->getTempToGenCode($prefix);
			if($res){
				foreach($res as $items){
					array_push($idArr, (int)substr($items->sms_code,5,10));
				}
				$code = sprintf($prefix.'%03d',MAX($idArr)+1);
			}else{
				$code = sprintf($prefix.'%03d',1);
			}

			###### create date format #####
			$date = explode('-', date("Y-m-d")) ;
			$date[0] = (int)$date[0]+543;

			$data = array( 
				'sms_code' => $code, 
				'customer_code' => $customer_code, 
				'temp_code' => $temp_code,
				'period' => (int)$period,
				'message' => $mess,
				'tel' => $tel,
				'status' => $sendsms['status'],
				'cdate'=> implode('-', $date)
			);
			
			$this->Sms_Model->insertSMS($data);

		}else{
			echo "<script type='text/javascript'> window.alert('Sending sms fail. Please try again.');</script>";
		}
		redirect( base_url($direct_url));
		//redirect(base_url($cate));
    }
	public function sendnotifymessage($requestCriteria, $config_ants){
    
  
		$telephone_number = $requestCriteria->telephone_number;
		//echo $telephone_number;exit();
		if(substr($telephone_number, 0,1) == '0'){
			$telephone_number = '66'.substr($telephone_number, 1,9);
		}
		
		// $data = array(
		//   'from'=>$config_ants['Sender'],
		//   'to'=>$telephone_number,
		//   'text'=>$requestCriteria->message
		// );
  
		$data = [
		  "messages" => [
			[
			//   "from" => $config_ants['Sender'],
			  "from" => 'Evorent',
			  "destinations" => [
				[
				  "to" => $telephone_number
				]
			  ],
			  "text" => $requestCriteria->message
			]
		  ]
		];
  
		// echo json_encode($data);exit;
  
		if($this->curlSendSMSAntsV2($data, $config_ants)){
		  return array(
			'status'=>true,
			'result_code'=>'success',
			'result_desc'=>'Send sms success'
		  );
  
		}else{
		  return array(
			'status'=>false,
			'result_code'=>'error',
			'result_desc'=>'Can not send sms,Please try again'
		  );
  
		}
	}
	public function  curlSendSMSAntsV2($data = [], $config_ants){

		$authorize = "";
		$authorize = "Basic ".base64_encode($config_ants['Username'].':'.$config_ants['Password']);
		$ch = curl_init();   
		try{
			$config_ants['Url_v2'] = 'https://api-service.ants.co.th/sms/send';
			curl_setopt($ch,CURLOPT_URL,$config_ants['Url_v2']);
			// if(ENVIRONMENT == 'production'){
			//   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// }
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Authorization:'.$authorize
			));
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
			curl_setopt($ch,CURLOPT_POST,1); 
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
			
			$Result = curl_exec($ch);
			$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
			curl_close($ch);
			//print_r($Result);exit;
			$decode_result = json_decode($Result);
			// if($decode_result->messages[0]->status->groupId == '1'){
			//   return true;
			// }else{
			//   return false;
			// }
			return true;
		} catch( Exception $e ){
		
		  return false;
		
		}
	}


		// ##########  Remark #####//
		public function get_remark_result(){
			$contract_code = $this->input->post('contract_code');
			$curent_date = date('Y-m-d H:i:s');
			$str_query = "SELECT * FROM contract_remark WHERE contract_code='".$contract_code."' ORDER BY update_date DESC";
			$Res= $this->db->query($str_query);
			$data = $Res->result();
			$res['datas'] = $data;
			$res['response'] = 'ค้นหาข้อมูลสำเร็จ';
			echo json_encode($res);
		}
		public function create($contract_code){
			$menu['mainmenu'] = 'contract';
			$menu['submenu'] = 'contractCus';
			$data['id'] = null;
			$data['contract_code'] = $contract_code;
			//$data['contract'] = $this->db->query("SELECT contract_code FROM contract")->result();
			$this->load->view('admin/header',$menu);
			$this->load->view('admin/contractinst_remark_create',$data);
			$this->load->view('admin/footer');
		}
		public function edit($id, $contract_code){
			$menu['mainmenu'] = 'contract';
			$menu['submenu'] = 'contractCus';
			$data['id'] = $id;
			$data['contract_code'] = $contract_code;
			//$data['contract'] = $this->db->query("SELECT contract_code FROM contract")->result();
			$this->load->view('admin/header',$menu);
			$this->load->view('admin/contractinst_remark_edit',$data);
			$this->load->view('admin/footer');
		}
		public function get_result_onc(){
			$id = $this->input->post('id');
			$curent_date = date('Y-m-d H:i:s');
			$str_query = "SELECT * FROM contract_remark WHERE id='".$id."'";
			$Res= $this->db->query($str_query);
			$data = $Res->result();
			$res['datas'] = $data;
			$res['response'] = 'ค้นหาข้อมูลสำเร็จ';
			echo json_encode($res);
		}
		public function actions(){
			$res = new stdClass();
			$curent_date = Date('Y-m-d H:i:s');
			$action = $this->input->post('action');
			$id = $this->input->post('id');
			$contract_code = $this->input->post('contract_code');
			$remark = $this->input->post('remark');
	
			$obj = array( 
				'contract_code' => $contract_code, 
				'remark' => $remark,
				'admin' =>  $this->session->userdata('userName')
			); 
			// print_r($obj); exit();
	
			//###  INSERT ###//
			if($action == 'insert'){
				$obj['create_date'] = $curent_date;
				$obj['update_date'] = $curent_date;
				$this->db->insert('contract_remark', $obj);
				$id = $this->db->insert_id();
	
				$res->status = true;
				$res->datas = base_url('admin/Installments/'.$contract_code);
				$res->massege = 'บันทึกสำเร็จ';
				$res->status_code = '000';
			}
	
			//###  UPDATE ###//
			if($action == 'update'){
				$obj['update_date'] = $curent_date;
				$this->db->where("id", $id);
				$this->db->update("contract_remark", $obj); 
				
				$res->status = true;
				$res->datas = base_url('admin/Installments/'.$contract_code);
				$res->massege = 'บันทึกสำเร็จ';
				$res->status_code = '000';
			}
	
			// $res->status = true;
			// $res->datas = base_url('admin/course');
			// $res->massege = 'บันทึกสำเร็จ';
			// $res->status_code = '000';
			echo json_encode($res);
		}

		// ##########  SMS #####//
		public function get_sms_result(){
			$contract_code = $this->input->post('contract_code');
			$curent_date = date('Y-m-d H:i:s');
			$str_query = "SELECT top 10 * FROM sms_history WHERE temp_code='".$contract_code."' ORDER BY cdate DESC";
			$Res= $this->db->query($str_query);
			$data = $Res->result();
			$res['datas'] = $data;
			$res['response'] = 'ค้นหาข้อมูลสำเร็จ';
			echo json_encode($res);
		}
		public function sms_create($contract_code){
			$menu['mainmenu'] = 'contract';
			$menu['submenu'] = 'contractCus';
			$data['id'] = null;
			$data['contract_code'] = $contract_code;
			//$data['contract'] = $this->db->query("SELECT contract_code FROM contract")->result();
			$this->load->view('admin/header',$menu);
			$this->load->view('admin/contractinst_remark_create',$data);
			$this->load->view('admin/footer');
		}
		public function sms_edit($id, $contract_code){
			$menu['mainmenu'] = 'contract';
			$menu['submenu'] = 'contractCus';
			$data['id'] = $id;
			$data['contract_code'] = $contract_code;
			//$data['contract'] = $this->db->query("SELECT contract_code FROM contract")->result();
			$this->load->view('admin/header',$menu);
			$this->load->view('admin/contractinst_remark_edit',$data);
			$this->load->view('admin/footer');
		}
		public function get_sms_result_onc(){
			$id = $this->input->post('id');
			$curent_date = date('Y-m-d H:i:s');
			$str_query = "SELECT * FROM contract_remark WHERE id='".$id."'";
			$Res= $this->db->query($str_query);
			$data = $Res->result();
			$res['datas'] = $data;
			$res['response'] = 'ค้นหาข้อมูลสำเร็จ';
			echo json_encode($res);
		}
		public function sms_actions(){
			$res = new stdClass();
			$curent_date = Date('Y-m-d H:i:s');
			$action = $this->input->post('action');
			$id = $this->input->post('id');
			$contract_code = $this->input->post('contract_code');
			$remark = $this->input->post('remark');
	
			$obj = array( 
				'contract_code' => $contract_code, 
				'remark' => $remark,
				'admin' =>  $this->session->userdata('userName')
			); 
			// print_r($obj); exit();
	
			//###  INSERT ###//
			if($action == 'insert'){
				$obj['create_date'] = $curent_date;
				$obj['update_date'] = $curent_date;
				$this->db->insert('contract_remark', $obj);
				$id = $this->db->insert_id();
	
				$res->status = true;
				$res->datas = base_url('admin/Installments/'.$contract_code);
				$res->massege = 'บันทึกสำเร็จ';
				$res->status_code = '000';
			}
	
			//###  UPDATE ###//
			if($action == 'update'){
				$obj['update_date'] = $curent_date;
				$this->db->where("id", $id);
				$this->db->update("contract_remark", $obj); 
				
				$res->status = true;
				$res->datas = base_url('admin/Installments/'.$contract_code);
				$res->massege = 'บันทึกสำเร็จ';
				$res->status_code = '000';
			}
	
			// $res->status = true;
			// $res->datas = base_url('admin/course');
			// $res->massege = 'บันทึกสำเร็จ';
			// $res->status_code = '000';
			echo json_encode($res);
		}
	
	
	
}