<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
        $this->load->model('admin/StatusCategory_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
		
        
    } 

	public function index()
	{
		$data['res'] = $this->Status_Model->select();
		$data['statusCate'] = $this->Status_Model->statusCate();

        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'status';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/status_list',$data);
        $this->load->view('admin/footer');
	}

	public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Status_Model->selected($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Status_Model->selectAllItems($Search);
		echo json_encode($data);
	}

	public function create()
	{
        //$data['category'] = $this->StatusCategory_Model->select();
		$data['category'] = $this->Status_Model->statusCate();
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'status';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/status_create',$data);
        $this->load->view('admin/footer');
	}

	public function edit($id = null)
	{
        $data['category'] = $this->StatusCategory_Model->select();
		$data['res'] = $this->Status_Model->selectOne((int)$id);
		
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'status';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/status_edit',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		$code = date("dmY-His");
		$cate = $this->input->post('cate');
		$statusCate = $this->Status_Model->getStatus($cate);
		
		$prefix = 0;
		if($statusCate != null){
			$arr  = array();
			foreach($statusCate as $item){
				array_push($arr, $item->status_code);
			}
			$prefix = max($arr)+1; 
		}

		$data = array( 
			'status_code' =>  $prefix, 
            'stautus_category' => $cate, 
			'label' =>  $this->input->post('st'), 
            'detail' => $this->input->post('stdetail'), 
            'color' => $this->input->post('color'), 
            'background_color' => $this->input->post('bgcolor'), 
            'cdate'=>date("Y-m-d H:m:s")
         );
		$this->Status_Model->insert($data); 
		
        redirect('admin/status');
    }

	public function update(){
		$id = $this->input->post('id');
        $data = array( 
			'stautus_category' => $this->input->post('cate'), 
			'label' =>  $this->input->post('st'), 
            'detail' => $this->input->post('stdetail'), 
            'color' => $this->input->post('color'), 
            'background_color' => $this->input->post('bgcolor'), 
            'udate'=>date("Y-m-d H:m:s")
         ); 

		 $this->Status_Model->update($data,$id); 

        redirect('admin/status');
    }
}
