<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class InstallmentType extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/InstallmentType_Model');
    } 

	public function index(){
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'installmentType';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/installmentType_list');
        $this->load->view('admin/footer');
	}

	public function edit($id = null){
        $data['res'] = $this->InstallmentType_Model->GetToEdit($id);
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'installmentType';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/installmentType_edit', $data);
        $this->load->view('admin/footer');
	}

	public function update(){
        $id = $this->input->post('id');
		$data = array( 
            'amount_installment' => $this->input->post('amount_installment'),
            'pay_per_month' => $this->input->post('pay_per_month'),
            'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
        );
		$this->InstallmentType_Model->update($data, $id);
		
        redirect('admin/installmentType');
    }

    public function create(){
        $data['product'] = $this->InstallmentType_Model->GetProductToCreate();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'installmentType';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/installmentType_create',$data);
        $this->load->view('admin/footer');
	}

    public function insert(){
        $idArr = [];
		$nume = 0;
		$res = $this->InstallmentType_Model->InstallmentTypeToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)$items->installment_type_id);
			}
			$nume = MAX($idArr);
		}
        
		$data = array( 
			'installment_type_id' =>   sprintf('%03d',$nume+1) ,
			'product_id' => $this->input->post('product-id'),
			'amount_installment' => $this->input->post('amount_installment'),
			'pay_per_month' => $this->input->post('pay_per_month'),
			'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
		);

		$this->InstallmentType_Model->InstallmentTypeInsert($data);

        redirect('admin/installmentType');
    }

    public function DeleteInstallmentType(){
        $id = $this->input->post('id');
		$data = $this->InstallmentType_Model->DeleteInstallmentType($id);
		echo json_encode($data);
    }

    public function getResInstallmentType(){
		$Search = $this->input->post('Search');
		$data = $this->InstallmentType_Model->getToList($Search);		
		echo json_encode($data);
	}

}
