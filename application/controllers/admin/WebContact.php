<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebContact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/WebContact_Model');
        $this->load->model('admin/Status_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
		
        
    } 

	public function index()
	{
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webContact';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webcontact_list');
        $this->load->view('admin/footer');
	}

	public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->WebContact_Model->select($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->WebContact_Model->selectAllItems($Search);
		echo json_encode($data);
	}

	public function edit($id = null)
	{
        $data['resStatus'] = $this->Status_Model->requestStatus('210604');
        $data['res'] = $this->WebContact_Model->selectOne((int)$id);
		
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webContact';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webcontact_edit',$data);
        $this->load->view('admin/footer');
	}

	public function update(){
		$id = $this->input->post('id');
        $data = array( 
			'status' => $this->input->post('status'), 
            'udate'=>date("Y-m-d H:m:s")
         ); 
		 $this->WebContact_Model->update($data,$id); 

        redirect('admin/webContact');
    }

	public function contract_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
		$Query = "SELECT ROW_NUMBER() OVER (  ORDER BY web_contact.id ) AS RowNum, web_contact.*, status.status_code as statusid, 
		status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color
		FROM web_contact 
		LEFT JOIN status ON web_contact.status = status.id ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}

	
	public function contract_delete(){
		$id = $this->input->post('id');
        $res = new stdClass();
		$Res= $this->db->query("DELETE FROM web_contact WHERE id = '".$id."' ");
		// $data = $Res->result();
		$res->status = true;
		echo json_encode($res);
	}
}
