<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
     	$this->load->helper('file'); 
		$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Temp_Model');
        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Service_Model');
		$this->load->model('admin/Sms_Model');

		//เรียกใช้งาน config SMS     
		$this->load->config('sms');
		

		// load db ( agent )
		/*$this->load->config('externaldb');
		$this->db_config = $this->config->item('agent_db');
		$this->connectSeminarDB();*/
      
    } 

	
	public function index()
	{
		$type = $this->input->post('cusSearch-type');
		$cusSearch = $this->input->post('cusSearch');
		$cusStatus = $this->input->post('cusStatus');
		
		$val = null;
		if($type != ''){
			switch($type){
				case 'status':$val = $cusStatus; break;
				default: $val = $cusSearch; break;
			}
		}
		//$data['res'] = $this->Loan_Model->select();

        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'loan';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/loan_list');
        $this->load->view('admin/footer');
	}
	
	public function getTempOne()
	{
		$id = $this->input->post('id');
		$data = $this->Loan_Model->selectTempOne($id);		
		echo json_encode($data);
	}

	public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Loan_Model->select($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Loan_Model->selectAllItems($Search);
		echo json_encode($data);
	}


	#### Create ####
	public function create()
	{
		$data['resTemp'] = $this->Loan_Model->selectTemp();
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'loan';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/loan_create',$data);
        $this->load->view('admin/footer');
	}
	public function insert(){
		$code = date("dmY-His");
		$date = date("Y-m-d H:m:s");
		$temp_code = $this->input->post('contract_id');
		$resTemp = $this->Loan_Model->TempOne($temp_code);

		$dataloan = array( 
			'loan_code' => "L-".$code, 
			'temp_code' => $resTemp[0]->temp_code, 
			'customer_code' => $resTemp[0]->customer_code,
			'product_price' => $resTemp[0]->product_price,
            'product_set' => $resTemp[0]->product_set,
			'product_count' => $resTemp[0]->product_count,
			'product_brand' => $resTemp[0]->product_brand,
			'product_version' => $resTemp[0]->product_version,
			'product_serial' => $resTemp[0]->product_serial,
			
			'contract_date' => $resTemp[0]->contract_date,
			'payment_start_date' => $resTemp[0]->payment_start_date,
			'payment_due' => $resTemp[0]->payment_due,
			'monthly_rent' => $resTemp[0]->monthly_rent,
			'monthly_plus_tax' => $resTemp[0]->monthly_plus_tax,
			'rental_period' => $resTemp[0]->rental_period,
			'advance_payment' => $resTemp[0]->advance_payment,
			'installation_fee' => $resTemp[0]->installation_fee,
			'techn_code' => $resTemp[0]->techn_code, 
            'cdate'=>$date
        );
		$this->Loan_Model->insertLoan($dataloan);
		
		
		################ Service ###################
		$dataService = array( 
			'loan_code' => $dataloan['loan_code'],
			'temp_code' => $dataloan['temp_code'],
			'service_count' => 1,
			'service_round' => 0,
            'service_round_unit' => 'month',
            'cdate' =>$dataloan['cdate']
        );
		$this->Service_Model->insert($dataService);


		########## insertInstallment ###############
		$myJSON = json_decode($resTemp[0]->installment);
		$dataistall = array( 
			'loan_code' => $dataloan['loan_code'],
			'temp_code' => $dataloan['temp_code'],
            'product_set' =>$dataloan['product_set'],
			'customer_code' => $dataloan['customer_code'],
            'cdate'=> $dataloan['cdate']
        ); 
		
		if((int)$dataloan['payment_due'] < 10){ $dataloan['payment_due'] = "0".$dataloan['payment_due'];}
		
		foreach($myJSON as $val){
			$payment_duedate = null;

			//-------------- Payment due date --------------------//
			if((int)$val->period == 1){
				$payment_duedate = $dataloan['payment_start_date'];
			}else{
				$mont = $val->period-1;
				$montChange = date('Y-m-d', strtotime("+".$mont." months ".$dataloan['payment_due']." day ", strtotime($dataloan['payment_start_date'])));
				
				$date = explode('-', $montChange); 
				$date[2] = $dataloan['payment_due'] ;
				$payment_duedate = implode('-', $date);
			}
			//---------------------------------------------------//
			

			//-------------- พักชำระ  --------------------//
			if($val->per_month_price_include_vat == 0){
				$dataistall['status'] = 2;
				$dataistall['payment_amount'] = 0;
				//$dataistall['installment_date'] = $dataloan['cdate'];

			}else{
				$dataistall['status'] = 0;
				$dataistall['payment_amount'] = null;
				//$dataistall['installment_date'] = null;
			}
			//-------------------------------------------//

			$dataistall['period'] = $val->period;
			$dataistall['payment_duedate'] = $payment_duedate;
			$dataistall['installment_payment'] = $val->per_month_price_include_vat;
			$this->Loan_Model->insertInstallment($dataistall); 
		}
		redirect('admin/loan');
    }
	
	#### Update ####
	public function edit($id = null)
	{
		//$data['resLoan'] = $this->Loan_Model->getLoanOne($id);
		$data['resTemp'] = $this->Loan_Model->getTempOne($id);
		$data['resInst'] = $this->Loan_Model->getInstOne($id);
		$data['resStatus'] = $this->Status_Model->requestStatus('210603');
		//print_r($data['resInst']);

		$menu['mainmenu'] = 'home';
		$menu['submenu'] = 'loan';
		$this->load->view('admin/header',$menu);
		$this->load->view('admin/loan_edit',$data);
        $this->load->view('admin/footer');

	}
	public function updatInstallment(){
		
		$date = date("Y-m-d H:m:s");
		$temp_code = $this->input->post('temp-code');
		$loan_code = $this->input->post('loan-code');
		$period = $this->input->post('inst-period');

		$installment_date = $this->input->post('inst-date');
		if(!empty($installment_date)){
			$temp = explode('/',$installment_date) ; 
            $installment_date = $temp[2].'-'.$temp[1].'-'.$temp[0];
		}
		$data = array( 
			//'loan_code' => $this->input->post('inst-period'), 
			'payment_amount' => $this->input->post('inst-payment'), 
			'installment_date' => $installment_date,
			'status_code' => $this->input->post('inst-status'),
            'remark' => $this->input->post('inst-remark'),
            'udate'=>$date
        );
		
		$this->Loan_Model->updatInstallment($data, $loan_code, $temp_code, $period); 

		
		redirect('admin/loan/edit/'.$temp_code);
    }

	#### Detail ####
	public function detail($id = null)
	{
		//$data['res'] = $this->Temp_Model->selectOneDeatil($id);
		$data['res'] = $this->Loan_Model->getTempOne($id);
		$data['supporter'] = $this->Temp_Model->selectSupporter($id);
		$data['resInst'] = $this->Loan_Model->getInstOne($id);
		$data['supporter'] = $this->Temp_Model->selectSupporter($id);

		$data['address'] = $this->Temp_Model->detailAddress($data['res'][0]->customer_code, 'ที่อยู่ปัจจุบัน');
		$data['installationLocation'] = $this->Temp_Model->addrProductInstall($data['res'][0]->addr);

		$menu['mainmenu'] = 'home';
		$menu['submenu'] = 'loan';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/loan_detail',$data);
		$this->load->view('admin/footer');
	}


	public function overdue()
	{
		$data['getCustomer'] = $this->Loan_Model->getCustomer();
        $data['getTemp'] = $this->Loan_Model->getTemp();
        $data['getStatus'] = $this->Status_Model->getStatus('210603');		
        $data['resStatus'] = $this->Status_Model->requestStatus('210603');

        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'loan';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/loan_overdue_list',$data);
        $this->load->view('admin/footer');
	}

	public function getOverDueInstallment()
	{
		$searchArray = $this->input->post('searchArray');
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Loan_Model->selectOverDue($searchArray, $itemPerPage, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getOverDueInstallmentAll()
	{
        $searchArray = $this->input->post('searchArray');
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Loan_Model->selectOverDueAllItems($searchArray, $itemPerPage, $itemStt, $itemEnd);
		echo json_encode($data);
	}

	public function updatOverDueInstallment(){
		
		$date = date("Y-m-d H:m:s");
		$temp_code = $this->input->post('temp-code');
		$loan_code = $this->input->post('loan-code');
		$period = $this->input->post('inst-period');
		$inst_payment = (int)$this->input->post('inst-payment');
		$payment = (int)$this->input->post('payment');

		$installment_date = $this->input->post('inst-date');
		if(!empty($installment_date)){
			$temp = explode('/',$installment_date) ; 
            $installment_date = $temp[2].'-'.$temp[1].'-'.$temp[0];
		}

		$data = array( 
			//'loan_code' => $this->input->post('inst-period'), 
			'payment_amount' => $inst_payment, 
			'installment_date' => $installment_date,
			'status_code' => $this->input->post('inst-status'),
            'remark' => $this->input->post('inst-remark'),
            'udate'=>$date
        );

		########### ส่วนต่าง ค่างวด ############
		$diff = 0;
		$newPeriod = $period;
		$dataNewPeriod = array();

		if($inst_payment > $payment){
			$diff = $inst_payment - $payment;
			$newPeriod++;

			$dataNewPeriod['installment_payment'] = $payment-$diff;
			$dataNewPeriod['udate'] = $date;
        }else if($inst_payment < $payment){
			$diff = $payment - $inst_payment;
			$newPeriod++;

			$dataNewPeriod['installment_payment'] = $payment+$diff;
			$dataNewPeriod['udate'] = $date;
        }

		$data['amount_deff'] = $diff;

		$this->Loan_Model->updatInstallment($data, $temp_code, $period);

		if($inst_payment != $payment){
			$this->Loan_Model->updatInstallment($dataNewPeriod, $temp_code, $newPeriod);
        }

		redirect('admin/loan/overdue');
    }

	public function sendSMS(){
		
		$date = date("Y-m-d H:m:s");
		$customer = $this->input->post('customer-sms');
		$customer_code = $this->input->post('customer-code');
		$temp_code = $this->input->post('temp-code-sms');
		$period = $this->input->post('inst-period-sms');
		$payment = $this->input->post('inst-payment-sms');
		$duedate = $this->input->post('duedate-sms');
		$keywords = $this->input->post('keywords-sms');
		$tel = $this->input->post('tel-sms');
		$cate = $this->input->post('cate');
		
		$mess = 'เรียนลูกค้าอีโวเร้นท์ '.$customer. PHP_EOL ;
		$mess .= $keywords. PHP_EOL ; 
		$mess .= 'เลขที่สัญญา '.$temp_code. '  งวดที่ '.$period. PHP_EOL ;
		$mess .= 'ยอดรวมที่ต้องชำระ '.$payment.' บาท '. PHP_EOL ;
		$mess .= 'กำหนดชำระ '.$duedate. PHP_EOL ;
		$mess .= PHP_EOL ;
		$mess .= 'ลูกค้าสามารถโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์ '. PHP_EOL  ;
		$mess .= 'ธนาคารกสิกรไทย สาขาแม็กซ์แวลู พัฒนาการ '. PHP_EOL  ;
		$mess .= 'เลขที่บัญชี 095-1-36903-9  '. PHP_EOL  ;
		$mess .= 'ชื่อบัญชี บริษัท อีโวเร้นท์ จำกัด '. PHP_EOL  ;
		$mess .= PHP_EOL ;
		$mess .= 'หรือติดต่ออีโวเร้นท์  '. PHP_EOL  ;
		$mess .= 'เบอร์โทร : 097-162-8565 '. PHP_EOL  ;
		$mess .= 'Line ID : @evorent'. PHP_EOL ;
		$mess .= PHP_EOL ;
		$mess .= 'ขออภัยหากท่านชำระแล้ว ';
		
		$obj = new stdClass();
		$obj->telephone_number = $tel;
		$obj->message = $mess;
		////$sms_config_ants = $this->config->item('ANTS');
		$sms_config_ants = $this->config->item('evorent');
		$sendsms = $this->sendnotifymessage($obj, $sms_config_ants);
		
		if($sendsms['status']){
			###### create sms_code #####
			$prefixdate = date("ym"); 
			$prefixText = 'S';
			$prefix =  $prefixText.$prefixdate;
			$idArr = [];
			$code = '';
			$res = $this->Sms_Model->getTempToGenCode($prefix);
			if($res){
				foreach($res as $items){
					array_push($idArr, (int)substr($items->sms_code,5,10));
				}
				$code = sprintf($prefix.'%03d',MAX($idArr)+1);
			}else{
				$code = sprintf($prefix.'%03d',1);
			}

			###### create date format #####
			$date = explode('-', date("Y-m-d")) ;
			$date[0] = (int)$date[0]+543;

			$data = array( 
				'sms_code' => $code, 
				'customer_code' => $customer_code, 
				'temp_code' => $temp_code,
				'period' => (int)$period,
				'message' => $mess,
				'tel' => $tel,
				'status' => $sendsms['status'],
				'cdate'=> implode('-', $date)
			);
			
			$this->Sms_Model->insertSMS($data);

		}else{
			echo "<script type='text/javascript'> window.alert('Sending sms fail. Please try again.');</script>";
		}
		redirect(base_url($cate));
    }

	public function sendnotifymessage($requestCriteria, $config_ants){
    
  
		$telephone_number = $requestCriteria->telephone_number;
		//echo $telephone_number;exit();
		if(substr($telephone_number, 0,1) == '0'){
			$telephone_number = '66'.substr($telephone_number, 1,9);
		}
		
		$data = array(
		  'from'=>$config_ants['Sender'],
		  'to'=>$telephone_number,
		  'text'=>$requestCriteria->message
		);
  
		$data = [
		  "messages" => [
			[
			  "from" => $config_ants['Sender'],
			  "destinations" => [
				[
				  "to" => $telephone_number
				]
			  ],
			  "text" => $requestCriteria->message
			]
		  ]
		];
  
		// echo json_encode($data);exit;
  
		if($this->curlSendSMSAntsV2($data, $config_ants)){
		  return array(
			'status'=>true,
			'result_code'=>'success',
			'result_desc'=>'Send sms success'
		  );
  
		}else{
		  return array(
			'status'=>false,
			'result_code'=>'error',
			'result_desc'=>'Can not send sms,Please try again'
		  );
  
		}
	}

	public function  curlSendSMSAntsV2($data = [], $config_ants){

		$authorize = "";
		$authorize = "Basic ".base64_encode($config_ants['Username'].':'.$config_ants['Password']);
		$ch = curl_init();   
		try{
			  curl_setopt($ch,CURLOPT_URL,$config_ants['Url_v2']);
			  // if(ENVIRONMENT == 'production'){
			  //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  // }
			  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type:application/json',
				'Authorization:'.$authorize
			  ));
			  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
			  curl_setopt($ch,CURLOPT_POST,1); 
			  curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
			  
			  $Result = curl_exec($ch);
			  $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
			  curl_close($ch);
			  //print_r($Result);exit;
			  $decode_result = json_decode($Result);
			  // if($decode_result->messages[0]->status->groupId == '1'){
			  //   return true;
			  // }else{
			  //   return false;
			  // }
			  return true;
		} catch( Exception $e ){
		
		  return false;
		
		}
	}

	

	
}
