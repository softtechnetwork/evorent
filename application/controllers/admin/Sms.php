<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('admin/Customer_Model');
		//$this->load->model('admin/Premise_Model');
		$this->load->model('admin/Sms_Model');
		$this->load->model('admin/Loan_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
		//เรียกใช้งาน config SMS     
		$this->load->config('sms');
    } 

	
	public function index()
	{
        $menu['mainmenu'] = 'sms';
		$menu['submenu'] = 'sms';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/sms_list');
        $this->load->view('admin/footer');
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$curent_year = date('Y')+543;
		// $str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum, sms.*, 
		// customer.firstname, customer.lastname, customer.email, customer.idcard, customer.addr as address 
		// FROM sms_history sms 
		// LEFT JOIN customer ON sms.customer_code = customer.customer_code 
		// WHERE YEAR(sms.cdate) = '".$curent_year."' ";
		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum,
		sms.sms_code ,sms.customer_code ,sms.temp_code ,sms.period
		,sms.message
		--,'' as message
		,sms.tel ,sms.status,sms.cdate
		,customer.firstname, customer.lastname, customer.email, customer.idcard, customer.addr as address 
		FROM sms_history sms 
		LEFT JOIN customer ON sms.customer_code = customer.customer_code 
		WHERE YEAR(sms.cdate) = '".$curent_year."' ";
		
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}

    public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Sms_Model->selectToTable($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

    public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Sms_Model->selectAllItems($Search);
		echo json_encode($data);
	}


	//###  send_sms  ###//
	public function send_sms(){
        $menu['mainmenu'] = 'sms';
		$menu['submenu'] = 'send';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/sms_send_list');
        $this->load->view('admin/footer');
	}
	public function get_send_resulted(){
		// $curent_date = date('Y-m-d H:i:s');
		// $curent_year = date('Y')+543;
		$curent_year = date('Y');

		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum ,sms.*
		FROM sms_send_history sms
		WHERE YEAR(sms.cdate) = '".$curent_year."' ";
		
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
	public function get_template_to_selection(){
		// $curent_date = date('Y-m-d H:i:s');
		// $curent_year = date('Y')+543;
		$curent_year = date('Y');

		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum ,sms.*
		FROM sms_template sms WHERE sms.status = '1' ";
		
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
	public function sending_sms(){
		$res = new stdClass(); 
        $post = $this->input->post();

		$obj = new stdClass();
		$obj->telephone_number = $post['tel'];
		$obj->message = $post['message'];
		$sms_config_ants = $this->config->item('evorent');
		$sendsms = $this->sendnotifymessage($obj, $sms_config_ants);
		
		if($sendsms["status"]){
			$obj = array( 
				'tel' => $post['tel'],
				'name' =>  $post['name'],
				'message' => $post['message'],
				'cdate' => date(date_format(date_create(),"Y-m-d H:i:s")),
				'udate' => date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			$this->db->insert("sms_send_history", $obj);

			$res->status = true;
			$res->url = base_url('admin/sms/sms_send_history');
			$res->massege = $sendsms["result_desc"];
			$res->status_code = '000';
		}else{

			$res->status = false;
			$res->url = base_url('admin/sms/sms_send_history');
			$res->massege = $sendsms["result_desc"];
			$res->status_code = '001';
		}
		echo json_encode($res);
	}
	public function sendnotifymessage($requestCriteria, $config_ants){
		$telephone_number = $requestCriteria->telephone_number;
		if(substr($telephone_number, 0,1) == '0'){
			$telephone_number = '66'.substr($telephone_number, 1,9);
		}
		
		// $data = array(
		//   'from'=>$config_ants['Sender'],
		//   'to'=>$telephone_number,
		//   'text'=>$requestCriteria->message
		// );

		$data = [
		  "messages" => [
			[
			//   "from" => $config_ants['Sender'],
			  "from" => 'Evorent',
			  "destinations" => [
				[
				  "to" => $telephone_number
				]
			  ],
			  "text" => $requestCriteria->message
			]
		  ]
		];

		if($this->curlSendSMSAntsV2($data, $config_ants)){
		  return array(
			'status'=>true,
			'result_code'=>'success',
			'result_desc'=>'Send sms success'
		  );
  
		}else{
		  return array(
			'status'=>false,
			'result_code'=>'error',
			'result_desc'=>'Can not send sms,Please try again'
		  );
  
		}
	}
	public function  curlSendSMSAntsV2($data = [], $config_ants){
  
		$authorize = "";
		$authorize = "Basic ".base64_encode($config_ants['Username'].':'.$config_ants['Password']);
		$ch = curl_init();   
		try{
			$config_ants['Url_v2'] = 'https://api-service.ants.co.th/sms/send';
			curl_setopt($ch,CURLOPT_URL,$config_ants['Url_v2']);
			// if(ENVIRONMENT == 'production'){
			//   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// }
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Authorization:'.$authorize
			));
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
			curl_setopt($ch,CURLOPT_POST,1); 
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
			
			$Result = curl_exec($ch);
			$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
			curl_close($ch);
			$decode_result = json_decode($Result);
			// if($decode_result->messages[0]->status->groupId == '1'){
			//   return true;
			// }else{
			//   return false;
			// }
			return true;
		} catch( Exception $e ){
		
		  return false;
		
		}
	}

	//###  TEmplate  ###//
	public function sms_template(){
        $menu['mainmenu'] = 'sms';
		$menu['submenu'] = 'template';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/sms_template_list');
        $this->load->view('admin/footer');
	}
	public function get_template_resulted(){
		// $curent_date = date('Y-m-d H:i:s');
		// $curent_year = date('Y')+543;
		$curent_year = date('Y');

		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY sms.cdate ) AS RowNum ,sms.*
		FROM sms_template sms";
		
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
	public function sms_template_action(){
		$res = new stdClass(); 
        $post = $this->input->post();

		$obj = array( 
			'name' =>  $post['name'],
			'message' => $post['message'],
			'status' => $post['status']
		);

		if($post['method'] == 'insert'){
			$obj['cdate'] = date(date_format(date_create(),"Y-m-d H:i:s"));
			$obj['udate'] = date(date_format(date_create(),"Y-m-d H:i:s"));
			$this->db->insert("sms_template", $obj);

			$res->status = true;
			$res->url = base_url('admin/sms/sms_template');
			$res->massege = 'เพิ่มข้อมูล สำเร็จ.';
			$res->status_code = '000';
        }

        if($post['method'] == 'update'){
            $obj['udate'] = date(date_format(date_create(),"Y-m-d H:i:s"));
            $this->db->set($obj); 
            $this->db->where("id", $post['id']);
            $this->db->update("sms_template", $obj); 

			$res->status = true;
			$res->url = base_url('admin/sms/sms_template');
			$res->massege = 'แก้ไขข้อมูล สำเร็จ.';
			$res->status_code = '000';
        }

		echo json_encode($res);
	}
}
