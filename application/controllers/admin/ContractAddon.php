<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContractAddon extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('session');
		
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->helper('file'); 
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Model   
        $this->load->model('admin/ContractAddon_Model');
        $this->load->model('admin/Contract_Model');
    }

	#######  Addon Contract #######
	public function list($contract_code = null)
	{
 		$data['res'] = $this->ContractAddon_Model->GetContract($contract_code);
        $data['inhabited'] = $this->Contract_Model->getInhabitedEdit($data['res'][0]->addr);

		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_addon_list', $data);
        $this->load->view('admin/footer');
	}
    public function GetContract()
	{
		$contract_code = $this->input->post('contract_code');
		$data = $this->ContractAddon_Model->GetContract($contract_code);		
		echo json_encode($data);
	}
	public function GetContractAddon()
	{
		$addon_code = $this->input->post('addon_code');
		$contract_code = $this->input->post('contract_code');
		$data = $this->ContractAddon_Model->GetContractAddon($addon_code, $contract_code);		
		echo json_encode($data);
	}
	public function GetInstallment($contract_code)
	{
		$data = $this->ContractAddon_Model->GetInstallment($contract_code);
		return $data;
	}
	public function GetProduct()
	{
		$contract_code = $this->input->post('contract_code');
		$product_id = $this->input->post('product_id');
		$data = $this->ContractAddon_Model->GetProduct($contract_code, $product_id);		
		echo json_encode($data);
	}
	public function CreatAddon()
	{
		try {
			//--------- Gen Addon code ----------//
			$prefix =  "AC-".date("ym");
			$idArr = [];
			$addon_code = '';
			$addonResults = $this->ContractAddon_Model->getAddonToGenCode();
			if($addonResults){
				foreach($addonResults as $items){array_push($idArr, (int)substr($items->addon_code,7));}
				//$code = sprintf($prefix.'%05d',MAX($idArr)+1);
				$addon_code = ($prefix.(MAX($idArr)+1));
			}else{
				//$code = sprintf($prefix.'%05d',1);
				$addon_code = ($prefix.'1');
			}
			//--------------------------------//
			$date = date(date_format(date_create(),"Y-m-d H:i:s"));
			$contract_code = $this->input->post('contract_code');
			$product_id = $this->input->post('addon-contract-products');
			$monthly_rent = $this->input->post('addonmonthly_rent');
			$rental_period = $this->input->post('addonrental_period');
			$addon_payment_start_date = date(date_format(DateTime::createFromFormat('d/m/Y',$this->input->post('addon-payment-start-date')),"Y-m-d"));
			$data = array( 
				'addon_code' => $addon_code,
				'contract_code' => $contract_code, 
				'product_id' => $product_id,
				'monthly_rent' => $monthly_rent,
				'rental_period' => $rental_period,
				'payment_start_date' => $addon_payment_start_date,
				'create_date'=> $date,
				'update_date'=> $date
			);
			
			
			$resInst = $this->InstallmentArr($data);
			//$SubByProduct = $this->Contract_Model->GetSubByProduct($product_id);
			
			$resContract = $this->ContractAddon_Model->CreatAddon($data);	
			if($resContract){
				// Installment
				if($resInst){
					foreach($resInst as $item){
						$this->ContractAddon_Model->insertInstallment($item);
					}
				}

				//Serial number
				// if($SubByProduct){
				// 	foreach($SubByProduct as $item){
				// 		if($item->active_serial){
				// 			for($i =1; $i<=$item->count; $i++){
				// 				$resSerialArr = $this->CreatSerialNumber($data['addon_code'], $item->product_set_id, $item->id, $data['create_date'], $data['update_date']);
				// 				$this->ContractAddon_Model->CreateSerialNumber($resSerialArr); 
				// 			}
				// 		}
				// 	}
				// }
			}
			redirect(base_url('admin/contractaddon/'.$contract_code));
			return true;
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}
	}
	public function InstallmentArr($addon)
	{
		try {
			$addon = (object)$addon;
			$data = $this->ContractAddon_Model->GetContract($addon->contract_code)[0];
			$res = array();

			############# Create Installment ############
			$dataistall = array( 
				'contract_code' => $addon->addon_code,
				'product_id' =>$addon->product_id,
				'customer_code' => $data->customer_code,
				'cdate'=> $addon->create_date,
				'udate'=> $addon->update_date
			);
			
			$data->payment_due = sprintf("%02d", $data->payment_due); //$data->payment_due = sprintf("%02d", 25);   สำหรับทดสอบ
			$cdated = date("Y-m", strtotime($addon->payment_start_date));
			for ($i=1; $i <= (int)$addon->rental_period ; $i++) { 
				######### Payment due date #########
				$payment_duedate = null;
				if((int)$i == 1){
					if( $addon->payment_start_date != null){
						$payment_duedate = date($addon->payment_start_date);
					}
				}else{
					$mont = $i-1;
					$montChange = date('Y-m', strtotime("+1 months ", strtotime($cdated)));
					$cdated = $montChange;
					$maxdate = date("Y-m-t", strtotime($montChange));
					
					$date = explode('-', $maxdate); 
					if($date[1] != '02'){
						$date[2] = $data->payment_due;
					}else{
						((int)$data->payment_due < (int)$date[2]) ? $date[2] = $data->payment_due : $date[2] = '28';
						//if((int)$data->payment_due < (int)$date[2]){$date[2] = $data->payment_due;}
					}
					$payment_duedate = date(implode('-', $date));
				}

				######### Extend due date #######
				$extend_duedate = null;
				$payment_duedate_exp = explode('-', $payment_duedate);
				if($payment_duedate_exp[2] == '15'){
					$maxDays = date("Y-m-t",strtotime($payment_duedate));
					$date = explode('-', $maxDays); 
					if((int)$date[2] < 30){
						if($date[1] != '02'){
							$payment_duedate_exp[2] = $date[2];
						}else{
							$payment_duedate_exp[2] = '28';
						}
					}else{
						$payment_duedate_exp[2] = '30';
					}
					$extend_duedate = date(implode('-', $payment_duedate_exp));
				}else{
					$montChange = date('Y-m', strtotime("+1 months ", strtotime(date("Y-m", strtotime($payment_duedate)))));
					$date = explode('-', $montChange); 
					$date[2] = '15';
					$extend_duedate = date(implode('-', $date));
				}
			
				$dataistall['status'] = $this->Contract_Model->requestStatus('210603', 0)[0]->id;
				$dataistall['status_code'] = 0;
				$dataistall['payment_amount'] = null;
				$dataistall['period'] = $i;
				$dataistall['payment_duedate'] = $payment_duedate;
				$dataistall['extend_duedate'] = $extend_duedate;
				$dataistall['installment_payment'] = $addon->monthly_rent;
				array_push($res,$dataistall);
			}
			return $res;
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}
		
	}
	public function delContract()
	{
		$code = '';
		try {
			$date = date(date_format(date_create(),"Y-m-d H:i:s"));
			$data = json_decode($this->input->post('data'));
			
			//$resSubProduct = $this->ContractAddon_Model->getProductSub($data->product_id);
			$AddonProduct = $this->ContractAddon_Model->GetContractAddonProduct(null, $data->addon_code);
			$resInstall = $this->ContractAddon_Model->GetInstallment($data->addon_code);
			$resSerial = $this->ContractAddon_Model->getSerialNumber($data->addon_code, null, null);
			
			$resDel = $this->ContractAddon_Model->deleteAddonContract($data->id, $data->addon_code, $data->contract_code);
			if ($resDel) {

				// del Addon Product
				if($AddonProduct){
					foreach($AddonProduct as $item){
						$this->ContractAddon_Model->deleteAddonProduct($item->id, $item->product_addon_code);
					}
				}

				// del installment
				if($resSerial){
					foreach($resInstall as $item){
						$this->ContractAddon_Model->deleteInstallment($item->id, $item->contract_code);
					}
				}

				// del serial number
				if($resSerial){
					foreach($resSerial as $item){
						$resDelSerial = $this->ContractAddon_Model->deleteSerialNumber($item->id, $item->contract_code, $item->product_id, $item->product_sub_id);
					}
				}
			}
			return true;
			$code = $data->contract_code;
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}
	}


	#######  Addon Product #######  
	public function getResProductCate()
	{
		$contract_code = $this->input->post('contract_code');
		$data = $this->ContractAddon_Model->getResProductCate($contract_code);		
		echo json_encode($data);
	}
	public function GetContractAddonProduct()
	{
		$addon_product = $this->input->post('addon_product');
		$contract_code = $this->input->post('contract_code');
		$contract_addon = $this->input->post('contract_addon');
		$data = $this->ContractAddon_Model->GetContractAddonProduct($addon_product, $contract_code, $contract_addon);		
		echo json_encode($data);
	}
	public function getResProductMaster()
	{
		$productCate = $this->input->post('productCate');
		$data = $this->ContractAddon_Model->getResProductMaster($productCate);		
		echo json_encode($data);
	}
	public function getStatus()
	{
		$statusCate = $this->input->post('statusCate');
		$data = $this->ContractAddon_Model->getStatus($statusCate);		
		echo json_encode($data);
	}
	public function CreatAddonProduct()
	{
		try {
			$date = date(date_format(date_create(),"Y-m-d H:i:s"));
			$addon_pcate = $this->input->post('addon-pcate');
			$addon_pmaster = $this->input->post('addon-pmaster');
			$addon_pcount = $this->input->post('addon-pcount');
			$contract_code = $this->input->post('contract_code');
			$contract_addon = $this->input->post('contract_addon');
			$addon_pstatus = $this->input->post('addon-pstatus');
			$active_serial = ($this->input->post('active-serial') == 'on')? true : false;
			 
			######## Create Contract addon poroduct ##########
			//--------- Gen Addon code ----------//
			$prefix =  date("ym");   // "PC".date("ym");
			$idArr = [];
			$code = '';
			$addonResults = $this->ContractAddon_Model->getAddonProductToGenCode();
			if($addonResults){
				foreach($addonResults as $items){array_push($idArr, (int)substr($items->product_addon_code,4));}
				//$code = sprintf($prefix.'%05d',MAX($idArr)+1);
				$code = ($prefix.(MAX($idArr)+1));
			}else{
				$code = ($prefix.'1');
			}

			$addonProduct_obj = array( 
				'product_addon_code' => $code,
				'contract_code' => (!empty($contract_code) && !empty($contract_addon)) ?$contract_addon : $contract_code,
				//'product_set_id' => '',
				'product_master_id' => $addon_pmaster,
				'count' => $addon_pcount,
				'type' => $addon_pstatus,
				'active_serial' => $active_serial,
				'create_date'=> $date,
				'update_date'=> $date
			);
			
			$data = $this->ContractAddon_Model->CreatAddonProduct($addonProduct_obj); 
			if($data){
				// if($active_serial){
				// 	//######## Create Serial ##########
				// 	for($i=1; $i <= $addonProduct_obj['count']; $i++){
				// 		$obj = (object)$addonProduct_obj;
				// 		$resSerialArr = $this->CreatSerialNumber($obj->contract_code, $obj->product_master_id, $obj->product_addon_code, $obj->create_date, $obj->update_date );
				// 		$this->ContractAddon_Model->CreateSerialNumber($resSerialArr);
				// 	}
				// 	//##################################
				// }
				redirect(base_url('admin/contract/edit/'.$contract_code));
			}
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}
	}
	public function delProductMaster()
	{
		try {
			$data = json_decode($this->input->post('data'));
			$resSerial = $this->ContractAddon_Model->getSerialNumber($data->contract_code, $data->product_master_id, $data->product_addon_code);
			$resDel = $this->ContractAddon_Model->deleteAddonProduct($data->id, $data->product_addon_code);
			if ($resDel) {
				if(count($resSerial) > 0){
					foreach($resSerial as $item){
						$resDelSerial = $this->ContractAddon_Model->deleteSerialNumber($item->id, $item->contract_code, $item->product_id, $item->product_sub_id);
					}
				}
				redirect(base_url('admin/contract/edit/'.$data->contract_code));
			}
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}
	}
	
	#######  Serial number ####### 
	public function Serial($contract_code = null, $product_id= null)
	{
		$data['contract_code'] = $contract_code;
		$data['mainProduct'] = $this->ContractAddon_Model->getMainSerialNumber($contract_code, $product_id);
		$data['addonContract'] = $this->ContractAddon_Model->getAddonContract($contract_code);
		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractCus';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_serial', $data);
        $this->load->view('admin/footer');
	}
	public function getAddonProductSerialNumber($contract_code = null, $product_id= null)
	{
		$resulte = array();
		$resProductCate = $this->ContractAddon_Model->GetContractAddonProduct(null, $contract_code);
		if(count($resProductCate ) > 0){
			foreach($resProductCate as $item){
				for($i=1; $i <= $item->count; $i++){
					array_push($resulte,$item);
				}
				
			}
		}
		
		print_r($resulte); echo'<br>';
		exit();

        return $resulte;
	}
	public function CreatSerialNumber($contract_code = null, $product_id = null, $product_sub_id = null, $cdate = null, $udate = null )
	{
		try {
			$serialnumber = array( 
				'contract_code' => $contract_code,
				'product_id' => $product_id,
				'product_sub_id' => $product_sub_id,
				'no' => 1,
				//'serial_number' => $this->input->post('serial_'.$item->product_sub_id),
				'cdate'=> $cdate,
				'udate'=> $udate
			);
			return $serialnumber;
		}catch (Exception $e) {
			// this will not catch DB related errors. But it will include them, because this is more general. 
			return log_message('error: ',$e->getMessage());;
		}

	}
	public function getResSerials()
	{
		$contract_code = $this->input->post('contract_code');
		$data = json_decode($this->input->post('data'));
		$resSerial = $this->ContractAddon_Model->getSerialNumber($contract_code, $data->product_id, $data->product_sub_id);
		echo json_encode($resSerial);
	}
	public function UpdateSerial()
	{
		$result = json_decode($this->input->post('result')) ;
		$serial = $this->input->post('serial');
		$remark = $this->input->post('remark');
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));
		$data = array( 
			'serial_number' => $serial,
			'remark' => $remark,
			'udate'=> $date
		);
		$resSerial = $this->ContractAddon_Model->UpdateSerial($data, $result->id, $result->contract_code, $result->product_id, $result->product_sub_id, $result->no);
		echo json_encode($resSerial);
	}

	public function CreatSerialAjax()
	{
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));
		$result = $this->input->post('obj');
		$obj = array();
		foreach ($result as $k => $value) {
			$_obj = array(
				'contract_code' => $value['contract_code'],
				'product_id' => $value['product_id'],
				'product_sub_id' => $value['product_sub_id'],
				'serial_number' => $value['serial_number'],
				'remark' => $value['remark'],
				'cdate' => $date,
				'udate'=> $date
			);
			array_push($obj, $_obj);
		}
		$responsive = '';
		$inserRes = $this->ContractAddon_Model->CreateSerialNumberAjax($obj);
		if ($inserRes['status']) { 
			$responsive = $this->responsive();//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}else{
			$responsive = $this->responsive(001, false, $inserRes['message'], $message='Fails.');//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}
		echo json_encode($responsive);
	}
	public function getSerialsByProductsub()
	{
		$contract_code = $this->input->post('contract_code');
		$product_id = $this->input->post('product_id');
		$product_sub_id = $this->input->post('product_sub_id');
		$resSerial = $this->ContractAddon_Model->getSerialNumber($contract_code, $product_id, $product_sub_id);
		echo json_encode($resSerial);
	}
	public function EditSerialAjax()
	{
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));
		$result = $this->input->post('obj');
		$obj = array();
		foreach ($result as $k => $value) {
			$_obj = array(
				'id' => $value['id'],
				'serial_number' => $value['serial_number'],
				'remark' => $value['remark'],
				'udate'=> $date
			);
			array_push($obj, $_obj);
		}
		$responsive = '';
		$inserRes = $this->ContractAddon_Model->EditSerialNumberAjax($obj);
		if ($inserRes['status']) { 
			$responsive = $this->responsive();//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}else{
			$responsive = $this->responsive(001, false, $inserRes['message'], $message='Fails.');//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}
		echo json_encode($responsive);
	}
	public function DelSerialAjax()
	{
		$contract_code = $this->input->post('contract_code');
		$product_id = $this->input->post('product_id');
		$product_sub_id = $this->input->post('product_sub_id');

		$responsive = '';
		$delRes = $this->ContractAddon_Model->DeleteSerialNumberAjax($contract_code, $product_id, $product_sub_id);
		if ($delRes['status']) { 
			$responsive = $this->responsive($status_code=200, $status=true, $data='ลบข้อมูลสำเร็จ', $message='Success fully');//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}else{
			$responsive = $this->responsive(001, false, $delRes['message'], $message='Fails.');//defult = ($status_code=200, $status=true, $data=null, $message='Success fully')
		}
		echo json_encode($responsive);
	}

	/*###  contract document ###*/
	
	public function contractPDF($mode = null, $id = null, $addon_code = null)
	{
		//เรียกใช้งาน Libary PDF    
		$this->load->library('Pdf');
		$res = $this->Contract_Model->selectOneDeatil($id);
		$subProduct = $this->Contract_Model->ProductSubByProductId($res[0]->contract_code, $res[0]->product_id);
		
		$address1 = $this->Contract_Model->detailAddress($res[0]->customer_code, 'ที่อยู่ตามบัตรประชาชน');
		$address2 = $this->Contract_Model->detailAddress($res[0]->customer_code,'ที่อยู่ปัจจุบัน');
		$address3 = $this->Contract_Model->detailAddress($res[0]->customer_code,'ที่อยู่สถานที่ทำงาน');

		$installationLocation = $this->Contract_Model->detailInstallationLocation($res[0]->installationlocation);
		
		$broker = $this->Contract_Model->CharngToPDF($res[0]->sale_code);
		$agent = $this->Contract_Model->CharngToPDF($res[0]->techn_code);
		

		$Supporter = null;
		if($res[0]->supporter == 1){
			$Supporter = $this->Contract_Model->selectSupporter($id);
		}

		// สร้าง object สำหรับใช้สร้าง pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
        // กำหนดรายละเอียดของ pdf
        
		//$pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide';
		
         
        // กำหนดข้อมูลที่จะแสดงในส่วนของ header และ footer
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// กำหนดรูปแบบของฟอนท์และขนาดฟอนท์ที่ใช้ใน header และ footer
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // กำหนดค่าเริ่มต้นของฟอนท์แบบ monospaced 
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // กำหนด margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // กำหนดการแบ่งหน้าอัตโนมัติ
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        // กำหนดรูปแบบการปรับขนาดของรูปภาพ 
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        // ---------------------------------------------------------

        // set default font subsetting mode
		$DateControlDoc = '';
        $pdf->setFontSubsetting(true);
		####### ประเภทสัญญาเป็น เช่าใช้ (1020 == เช่าใช้) #######
		if($res[0]->product_contract_type == '1020'){
			$DateControlDoc =  '10-06-2021';
			if($mode == 'origin'){
				$this->Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
				$this->Agreement($pdf, $DateControlDoc, $res);
				$this->Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
				if($Supporter != null){
					$this->Supporter($pdf, $DateControlDoc, $res, $Supporter);
				}
			}else{
				$this->ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
				$this->AgreementCoppy($pdf, $DateControlDoc, $res);	
				$this->ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
				
				if($Supporter != null){
					$this->SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter);	
				}
			}
		}

		####### หมวดสินค้าเป็น โซล่าห์เซลล์ (0012 == โซล่าห์เซลล์) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if($res[0]->product_cate == '0012' && $res[0]->product_contract_type == '1021'  ){
			// โซลาร์เซลล์ doc0010 / doc0011 / doc0012 / doc 0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);

		}
		####### หมวดสินค้าเป็น โซล่าห์ปั๊มส์ (0013 == โซล่าห์ปั๊มส์) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if($res[0]->product_cate == '0013' && $res[0]->product_contract_type == '1021'  ){
			// โซลาร์เซลล์ doc0010 / doc0011 / doc0012 / doc 0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);

		}
		####### หมวดสินค้าเป็น แอร์ (0007, 0011) และ ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) #######
		if(($res[0]->product_cate == '0007' || $res[0]->product_cate == '0011') && $res[0]->product_contract_type == '1021'){
			// แอร์ doc0014 / doc0012 / doc0015 / doc0016
			$DateControlDoc =  '05-10-2021';
			$this->DOC0014($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);

		}
		####### ประเภทสัญญาเป็น เช่าซื้อ (1021 == เช่าซื้อ) และ เอกสารสัญญาทั่วไป (CD220304  == ทั่วไป) #######
		if($res[0]->product_contract_type == '1021' && $res[0]->product_contract_doc == 'CD220304'){
			// แอร์ doc0018 / doc0012 / doc0015 / doc0016
			//$DateControlDoc =  '21-03-2022';
			$DateControlDoc =  '07-07-2022';
			$this->DOC0018($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);
			$this->DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode);
			$this->DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code);
		}

		$file_name = 'สัญญาเลขที่-'.$res[0]->contract_code.'.pdf';
		$pdf->Output($file_name, 'I');
	}

	public function DOC0010($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		$currentAddress = '';
		if($res[0]->type == 'person'){
			$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		}else{
			$currentAddress = $address3[0]->address.'  ตำบล/แขวง '.$address3[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address3[0]->amphur_name).'  จังหวัด'.$address3[0]->province_name.'  รหัสไปรษณีย์ '.$address3[0]->zip_code;
		}
		$pdf->Text(20, 97.5, $currentAddress);
		$pdf->Text(85, 150.5, $res[0]->rental_period);

		//$pdf->Text(115, 177, $res[0]->productPricce);
		//$pdf->Text(15, 185.7, $this->PriceToText((int)$res[0]->productPricce));
		$pdf->Text(115, 177,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 185.7, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0010/DOC0010('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 210.6, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function DOC0011($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '08-12-2021';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$inhabited_name = (!empty($res[0]->inhabited_name))?$res[0]->inhabited_name:$fulName;
		$pdf->Text(25, 80, $inhabited_name);
		$pdf->Text(85, 97.5, $res[0]->contract_code);
		$pdf->Text(158, 97.5, $currentDate);
		$pdf->Text(75, 106.5, $fulName);

		$pdf->Text(138, 159.2, $fulName);
		$pdf->Text(35, 168, number_format($res[0]->place_ins_size));

		$pdf->Text(27, 203, $res[0]->contract_code);
		$pdf->Text(90, 203, $currentDate);

		$pdf->Text(34, 212, $currentDate);
		$new_date = date('d/m/Y', strtotime('+ 3 year', strtotime($res[0]->contract_do_date)));
		$pdf->Text(125, 212, $new_date);

		$rental_rate = '';
		$rental_rate_text = '';
		if($res[0]->annual_rental_rate == 0){
			$rental_rate = 'ไม่มีค่าใช้จ่าย';
			$rental_rate_text = 'ไม่มีค่าใช้จ่าย';
		}else{
			$rental_rate = number_format($res[0]->annual_rental_rate);
			$rental_rate_text = $this->PriceToText((int)$res[0]->annual_rental_rate);
		}
		$pdf->Text(27, 265, $rental_rate);
		$pdf->Text(117, 265, $rental_rate_text);

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0011/DOC0011('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(133, 78.3, $inhabited_name);
	}
	public function DOC0012($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code)
	{
		$serialNodata = $this->ContractAddon_Model->serialNodata($addon_code, $res[0]->product_id);
		$serialData = $this->ContractAddon_Model->serialData($addon_code, $res[0]->product_id, $serialNodata[0]->no);
		
		$istCount = count($serialData);
		$istRow = 25;
		$startRow = 0;
		$arrs = array(); 
		$countPage = ceil($istCount/$istRow); 

		for($p=1; $p <= $countPage; $p++) {
			$arrKey = array();
			for ($i = $startRow; $i < $p*$istRow ; $i++) { 
				$arrRes = array(); 
				$arrRes['no'] = $i+1;
				if(!empty( $serialData[$i]->productSubName)){
					$arrRes['productSubName'] = (!empty( $serialData[$i]->productSubName))? $serialData[$i]->productSubName:'';
					$arrRes['serial_number']= (!empty( $serialData[$i]->serial_number))? $serialData[$i]->serial_number:'' ;
					array_push($arrKey, $arrRes);
				}
			}
			array_push($arrs, $arrKey);
			$startRow = ($istRow*$p);
		}
		foreach($arrs as $key =>  $item){
			$html = '<html><head>';
			$html .= '<style>

			.tableInstallment .InstHeader{
				background-color: #17a2b8 !important;
			}
			.tableInstallment .InstHeader .productSubNo, .tableInstallment .InstContent .productSubNo{
				width: 50px !important;
			}
			.tableInstallment .InstHeader .productSub, .tableInstallment .InstContent .productSub{
				width: 220px !important;
			}
			.tableInstallment .InstHeader .serialNumber, .tableInstallment .InstContent .serialNumber{
				width: 180px !important;
			}
			.tableInstallment .InstHeader .productSubCunt, .tableInstallment .InstContent .productSubCunt{
				width: 50px !important;
			}
			</style>';
			$html .= '</head><body id"tableInstallment">';
			$html .= '<br/><br/><br/>';

			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="1" border="1" style="border-color:red;">';
			$html .= '<tr class="InstHeader">  ';
			$html .= '<th class="productSubNo">ลำดับ</th> ';
			$html .= '<th class="productSub">อุปกรณ์</th> ';
			//$html .= '<th class="productSubCunt">จำนวน</th> ';
			$html .= '<th class="serialNumber">หมายเลขเครื่อง</th> ';
			$html .= '</tr>';

			$DateControlDoc =  '22-10-2021';
			$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0012/DOC0012('.$DateControlDoc.').jpg');
			$this->pdfHeaderPage($pdf, $img_file);
			$pdf->SetFont('thsarabun', '', 15, '', true);
			$pdf->Text(169, 50.5, $res[0]->contract_code);

			foreach($item as $val){
				$html.= '<tr class="InstContent">';
				$html.= '<td class="productSubNo">'.$val['no'].'</td>';
				$html.= '<td class="productSub"  style="padding:50px !important;" >'.$val['productSubName'].'</td>';
				//$html.= '<td class="productSubCunt">'.$res->count.'</td>';
				$html.= '<td class="serialNumber">'.$val['serial_number'].'</td>';
				$html.= '</tr>';
			}

			$html.='</table>';
			$html .= '</body></html>';
			$pdf->writeHTMLCell(0, 0, 16, 60, $html, 0, 1, 0, true, 0, true);
			
			$pdf->SetY(-15);
			//$pdf->Cell(0, 10, 'หน้า '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Cell(0, 10, 'หน้า '.($key+1).'/'.$countPage, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		}
			
		
	}
	public function DOC0014($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '10-10-2021';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		$pdf->Text(20, 97.5, $currentAddress);
		$pdf->Text(85, 150.5, $res[0]->rental_period);

		$pdf->Text(115, 177,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 185.7, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0014/DOC0014('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 131.3, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function DOC0015($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		$DateControlDoc =  '22-10-2021';
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0015/DOC0015('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(166, 50.5, $currentDate);

		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		#######  Serial number ######
		/*$serialStr = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);*/
		$pdf->Text(58, 84.5, $res[0]->product_name);

		####### Type of customer  #####
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
			//$pdf->Text(70, 137.5, $res[0]->office_name);
		}else{
			$cusType = 'นิติบุคคล';
			//$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);
		$pdf->Text(60, 111, $fulName);

		#######   Place of installation ########
		$inst = $installationLocation[0];
		$pdf->Text(67, 128.8, $inst->building_type);
		$pdf->Text(70, 137.5, $inst->company_name);
		$pdf->Text(150, 137.5, $inst->village_name);
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(31, 236.3, $fulName);
	}
	public function DOC0016($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode, $addon_code)
	{
		//$istallation = $this->Contract_Model->GetInstallmentProductID($res[0]->contract_code);
		$istallation = $this->ContractAddon_Model->GetInstallmentAddonID($addon_code);
		$istCount = count($istallation);
		$sumpayment = $istCount*$istallation[0]->installment_payment;
		$istRow = 24;
		$startRow = 0;
		
		$DateControlDoc =  '08-12-2021';
		
		if($istCount > $istRow ){
			$arrs = array(); 
			$countPage = ceil($istCount/24); #### หน้าละ 24 row ######
			
			for($p=1; $p <= $countPage; $p++) {
				$arrKey = array();
				for ($i = $startRow; $i < $p*$istRow ; $i++) { 
					if(!empty( $istallation[$i]->period)){
						$arrRes = array();    
						$payment_duedate = '';
						if(!empty($istallation[$i]->payment_duedate)){ $payment_duedate= date_format(date_create($istallation[$i]->payment_duedate),"d/m/Y");}
						
						$arrRes['period'] = (!empty( $istallation[$i]->period))? $istallation[$i]->period:'';
						$arrRes['inst_payment']= (!empty( $istallation[$i]->installment_payment))? $istallation[$i]->installment_payment:'' ;
						$arrRes['duedate']= $payment_duedate;
						array_push($arrKey, $arrRes);
					}
				}
				
				array_push($arrs, $arrKey);
				$startRow = ($istRow*$p);
			}
			foreach($arrs as $key =>  $item){
				
				$html = '<html><head>';
				$html .= '<style>
				.tableInstallment .InstHeader{
					background-color: #17a2b8 !important;
				}
				.tableInstallment .periodCol, .tableInstallment .InstContent .periodCol{
					text-align: center !important;
					width: 100px !important;
				}
				.tableInstallment .priceCol, .tableInstallment .InstContent .priceCol{
					text-align: right  !important;
					width: 180px !important;
				}
				.tableInstallment .dateCol, .tableInstallment .InstContent .dateCol{
					text-align: center  !important;
					width: 200px !important;
				}

				</style>';
				$html .= '</head><body>';
				$html .= '<br/><br/><br/>';

				$img_file;
				if($key == 0){
					$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p01.jpg');
				}else{
					$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p02.jpg');
				}
				
				$this->pdfHeaderPage($pdf, $img_file);
				$pdf->SetFont('thsarabun', '', 15, '', true);
				
				if($key == 0){
					$pdf->Text(168, 50.5, $istallation[0]->contract_code);
				}

				$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;" >';
				$html .= '<tr class="InstHeader">  ';
				$html .= '<th class="periodCol">งวด/เดือน</th> ';
				$html .= '<th class="priceCol">ค่าเช่า</th> ';
				$html .= '<th class="dateCol">วันที่ครบชำระ</th> ';
				//$html .= '<th>สถานะ</th>  ';
				$html .= '</tr>';

				foreach($item as $res){
					$inst_payments = number_format((int)$res['inst_payment']);
					$html.= '<tr class="InstContent">';
					$html.= '<td class="periodCol">'.$res['period'].'</td>';
					$html.= '<td class="priceCol">'.$inst_payments.' บาท</td>';
					$html.= '<td class="dateCol">'.$res['duedate'] .'</td>';
					//$html.= '<td  style"padding: .75rem;">'.$istallation[$i]->label.'</td>';
					$html.= '</tr>';
				}

				$html.='</table>';
				$html .= '</body></html>';
				
				//last page
				if(($countPage) == ($key+1)){
					$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
					$html .= '<tr class="">  ';
					$html .= '<th class="periodCol">รวม</th> ';
					$html .= '<th class="priceCol">'.number_format((int)$sumpayment).' บาท</th> ';
					$html .= '</tr>';
					$html.='</table>';
					//$html .= $sumpayment;
				}
				
				if($key == 0){
					$pdf->writeHTMLCell(0, 0, 20, 39, $html, 0, 1, 0, true, '', true);
				}else{
					$pdf->writeHTMLCell(0, 0, 20, 25, $html, 0, 1, 0, true, '', true);
				}
			}
		}else{
			
			$html = '<html><head>';
			$html .= '<style>

			.tableInstallment .InstHeader{
				background-color: #17a2b8 !important;
			}
			.tableInstallment .periodCol, .tableInstallment .InstContent .periodCol{
				text-align: center !important;
				width: 100px !important;
			}
			.tableInstallment .priceCol, .tableInstallment .InstContent .priceCol{
				text-align: right  !important;
				width: 180px !important;
			}
			.tableInstallment .dateCol, .tableInstallment .InstContent .dateCol{
				text-align: center  !important;
				width: 200px !important;
			}

			</style>';
			$html .= '</head><body id"tableInstallment">';
			$html .= '<br/><br/><br/>';

			$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0016/DOC0016('.$DateControlDoc.')_p01.jpg');
			$this->pdfHeaderPage($pdf, $img_file);
			$pdf->SetFont('thsarabun', '', 15, '', true);
			$pdf->Text(168, 50.5, $res[0]->contract_code);

			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
			$html .= '<tr class="InstHeader">  ';
			$html .= '<th class="periodCol">งวด/เดือน</th> ';
			$html .= '<th class="priceCol">ค่าเช่า</th> ';
			$html .= '<th class="dateCol">วันที่ครบชำระ</th> ';
			$html .= '</tr>';

			foreach($istallation as $res){
				$duedates = '';
				if(!empty($res->payment_duedate)){ $duedates= date_format(date_create($res->payment_duedate),"d/m/Y");}
					
				$inst_paymented = number_format((int)$res->installment_payment);
				$html.= '<tr class="InstContent">';
				$html.= '<td class="periodCol">'.$res->period.'</td>';
				$html.= '<td class="priceCol">'.$inst_paymented.' บาท</td>';
				$html.= '<td class="dateCol">'.$duedates.'</td>';
				$html.= '</tr>';
			}

			$html.='</table>';
			$html .= '</body></html>';
			
			//last page
			$html .= '<table class="tableInstallment" cellspacing="0" cellpadding="2" border="1" style="border-color:red;">';
			$html .= '<tr class="">  ';
			$html .= '<th class="periodCol">รวม</th> ';
			$html .= '<th class="priceCol">'.number_format((int)$sumpayment).' บาท</th> ';
			$html .= '</tr>';
			$html.='</table>';
			
			$pdf->writeHTMLCell(0, 0, 20, 39, $html, 0, 1, 0, true, '', true);
		}
	}
	public function DOC0018($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter, $mode)
	{
		
		$fulName = $res[0]->firstname.' '.$res[0]->lastname;
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(169, 50.5, $res[0]->contract_code);

		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(160, 59.5, $currentDate);
		
		$pdf->Text(20, 89, $res[0]->firstname.' '.$res[0]->lastname);
		
		$customer_addr = 'ที่อยู่ปัจจุบัน';
		if($res[0]->type == 'corperation'){
			$customer_addr = 'ที่อยู่สถานที่ทำงาน';
			$address2 = $this->Contract_Model->detailAddress($res[0]->customer_code, $customer_addr);
		}
		$currentAddress = $address2[0]->address.'  ตำบล/แขวง '.$address2[0]->district_name.'  อำเภอ/เขต '.$this->RemoveText('อำเภอ',$address2[0]->amphur_name).'  จังหวัด'.$address2[0]->province_name.'  รหัสไปรษณีย์ '.$address2[0]->zip_code;
		$pdf->Text(20, 97.5, $currentAddress);
		$pdf->Text(105, 124, $res[0]->product_name);
		$pdf->Text(85, 159.5, $res[0]->rental_period);

		$pdf->Text(115, 186,number_format($res[0]->value_end_contract));
		$pdf->Text(15, 194.7, $this->PriceToText($res[0]->value_end_contract));

		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p03.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p04.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		
		$img_file = base_url('uploaded/ContractDoc/All/'.$mode.'/DOC0018/DOC0018('.$DateControlDoc.')_p05.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(30, 131.3, $res[0]->firstname.' '.$res[0]->lastname);
	}
	



	public function Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/original/DOC0002/DOC0002('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(169, 50.5, $res[0]->contract_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		//######### Contract A2 ##########//

		$img_file = base_url('img/original/DOC0002/DOC0002('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_master_name.' : '.$item->serial_number : ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));


		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}
	public function ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/copy/DOC0002/DOC0002('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(169, 50.5, $res[0]->contract_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		

		//######### Contract A2 ##########//
		$img_file = base_url('img/copy/DOC0002/DOC0002('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_master_name.' : '.$item->serial_number : ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));

		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}
	public function Agreement($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/original/DOC0003/DOC0003('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		//######### Page 2 ##########//
		$img_file = base_url('img/original/DOC0003/DOC0003('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function AgreementCoppy($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/copy/DOC0003/DOC0003('.$DateControlDoc.')_p01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);

		//######### Page 2 ##########//
		$img_file = base_url('img/copy/DOC0003/DOC0003('.$DateControlDoc.')_p02.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct, $agen)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/original/DOC0004/DOC0004('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}
	public function ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc , $subProduct, $agen)
	{
		//######### Contract A1 ##########//
		$img_file = base_url('img/copy/DOC0004/DOC0004('.$DateControlDoc.')-01.jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(155, 61.3, $res[0]->contract_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_master_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_master_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}
	public function Supporter($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/original/DOC0009/DOC0009('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->contract_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}
	public function SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$img_file = base_url('img/copy/DOC0009/DOC0009('.$DateControlDoc.').jpg');
		$this->pdfHeaderPage($pdf, $img_file);
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->contract_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}

	
	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	}
	public function pdfHeaderPage($pdf, $imgPath){
		$pdf->AddPage();
		$img_file = $imgPath;
		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->SetAutoPageBreak(TRUE, 0);
		// set the starting point for the page content
		$pdf->setPageMark();
	}
	
	public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			if(strpos($wording, $text) !== false){
				$temp = explode($text, $wording);
				$string = $temp[1];
			} else{
				$string = $wording;
			}
		}
		return $string ;
	}

	##########  convert int to thai text  ##########
	function PriceToText($amount_number)
	{
		$amount_number = number_format($amount_number, 2, ".","");
		$pt = strpos($amount_number , ".");
		$number = $fraction = "";
		if ($pt === false) 
			$number = $amount_number;
		else
		{
			$number = substr($amount_number, 0, $pt);
			$fraction = substr($amount_number, $pt + 1);
		}
		
		$ret = "";
		$baht = $this->ReadNumber ($number);
		if ($baht != "")
			$ret .= $baht . "บาท";
		
		$satang = $this->ReadNumber($fraction);
		if ($satang != "")
			$ret .=  $satang . "สตางค์";
		else 
			$ret .= "ถ้วน";
		return $ret;
	}
	function ReadNumber($number)
	{
		$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
		$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
		$number = $number + 0;
		$ret = "";
		if ($number == 0) return $ret;
		if ($number > 1000000)
		{
			$ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
			$number = intval(fmod($number, 1000000));
		}
		
		$divider = 100000;
		$pos = 0;
		while($number > 0)
		{
			$d = intval($number / $divider);
			$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
				((($divider == 10) && ($d == 1)) ? "" :
				((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
			$ret .= ($d ? $position_call[$pos] : "");
			$number = $number % $divider;
			$divider = $divider / 10;
			$pos++;
		}
		return $ret;
	}

	function responsive($status_code = null, $status = true, $data = null, $message = null)
	{
		$res = array(
			'status_code' => '200',
			'status' => true,
			'data' => 'บันทึกข้อมูลสำเร็จ',
			'message' => 'Success fully'
		);
		if(!empty($status_code)){$res['status_code'] = $status_code;}
		if(!$status){$res['status'] = $status;}
		if(!empty($data)){$res['data'] = $data;}
		if(!empty($message)){$res['message'] = $message;}
		return $res;
	}

}
