<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatusCategory extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/StatusCategory_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
		
        
    } 

	public function index()
	{
		$data['res'] = $this->StatusCategory_Model->select();

        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'statusCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/statusCategory_list',$data);
        $this->load->view('admin/footer');
	}
	public function create()
	{
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'statusCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/statusCategory_create');
        $this->load->view('admin/footer');
	}

	public function edit($id = null)
	{
		$data['res'] = $this->StatusCategory_Model->selectOne($id);
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'statusCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/statusCategory_edit',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){

		$prefixdate = date("ym");
		$prefixchar = '';
		$prefix =  $prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->StatusCategory_Model->selectOne($prefixdate);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->status_category_code,5,9));
			}
			$code = sprintf($prefix.'%02d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%02d',1);
		}
		
		$data = array( 
			'status_category_code' => $code, 
			'label' =>  $this->input->post('stcate'), 
            'detail' => $this->input->post('stdetail'), 
            'cdate'=>date("Y-m-d H:m:s")
         ); 
		
		$this->StatusCategory_Model->insert($data); 
		
        redirect('admin/statusCategory');
    }

	public function update(){
		$id = $this->input->post('id');
        $data = array( 
			'label' =>  $this->input->post('stcate'), 
            'detail' => $this->input->post('stdetail'), 
            'udate'=>date("Y-m-d H:m:s")
         ); 

		 $this->StatusCategory_Model->update($data,$id); 

        redirect('admin/statusCategory');
    }
}
