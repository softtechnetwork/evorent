<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        //$this->load->model('admin/Customer_Model');
		//$this->load->model('admin/Premise_Model');
		//$this->load->model('admin/Status_Model');

		$this->load->model('admin/Logs_Model');

		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function index()
	{
		$type = $this->input->post('cusSearch-type');
		$cusSearch = $this->input->post('cusSearch');
		$cusStatus = $this->input->post('cusStatus');
		$val = null;
		if($type != ''){
			switch($type){
				case 'status':$val = $cusStatus; break;
				default: $val = $cusSearch; break;
			}
		}

		//$data['res_customer'] = $this->Customer_Model->select($type, $val);
		//$data['resStatus'] = $this->Status_Model->customer_select();
		
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'logs';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/logs_list');
        $this->load->view('admin/footer');
	}
	public function create()
	{
		$data['resStatus'] = $this->Status_Model->customer_select();
		$data['province'] = $this->Customer_Model->province();
		$data['amphurs'] = $this->Customer_Model->amphurs();
		$data['district'] = $this->Customer_Model->district();
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'logs';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/customer_create',$data);
        $this->load->view('admin/footer');
	}

	public function edit($id = null)
	{
		$data['resStatus'] = $this->Status_Model->customer_select();
		$data['res_customer'] = $this->Customer_Model->selectOne($id);
		$data['province'] = $this->Customer_Model->province();
		$data['amphurs'] = $this->Customer_Model->amphurs();
		$data['district'] = $this->Customer_Model->district();
		
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'logs';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/customer_edit',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		$code = date("dmY-His");
		$data = array( 
			'customer_code' => "Cus".$code, 
			'idcard' =>  implode(preg_split('/[-]/',  $this->input->post('idcard'))), 
            'firstname' => $this->input->post('name'), 
            'lastname' => $this->input->post('sname'), 
			'addr' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'tumbon_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			//'birthday' => $this->input->post('bdate'), 
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			'sex' => $this->input->post('gender'),
			'status' => $this->input->post('status'),
            'created'=>date("Y-m-d H:m:s")
         ); 

		 $bdate = $this->input->post('bdate');
		 if($bdate != ''){
			$data['birthday'] = $bdate;
		 }

		$this->Customer_Model->insert($data); 
			
			//$config['upload_path']   = 'uploaded/customer/'; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
			$config['allowed_types'] = 'jpeg|jpg|png'; //รูปแบบไฟล์ที่ อนุญาตให้ Upload ได้
			$config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
			$config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
			$config['max_height']    = 0;  //ขนาดความสูงสูงสดุ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
			//$config['encrypt_name']  = true; //กำหนดเป็น true ให้ระบบ เปลียนชื่อ ไฟล์  อัตโนมัติ  ป้องกันกรณีชื่อไฟล์ซ้ำกัน

			// file name
			$img_id = array("contract1","contract2","contract3","contract4","idcardPremise","houseregisPremise","idcardSupporter","houseregisSupporter");
			
			foreach($img_id as $items){
				if (!empty($_FILES[$items]['name'])) {
					$_FILES[$items]['name'] = $items."_".$code.".".pathinfo($_FILES[$items]['name'], PATHINFO_EXTENSION);
				}
			}
						
			$premise = array( 
				'premise_code' => "Premise".$code, 
				'customer_code' => $data['customer_code'], 
				'contract1' => $_FILES['contract1']['name'], 
				'contract2' => $_FILES['contract2']['name'], 
				'contract3' => $_FILES['contract3']['name'],  
				'contract4' => $_FILES['contract4']['name'], 
				'idcardPremise' => $_FILES['idcardPremise']['name'],  
				'houseRegisPremise' => $_FILES['houseregisPremise']['name'],  
				'idcardSupporter' => $_FILES['idcardSupporter']['name'], 
				'houseRegisSupporter' => $_FILES['houseregisSupporter']['name'], 
				'cdate'=>$data['created']
			 ); 
			 
			$this->Premise_Model->insert($premise); 

				$dir = "uploaded/customer/".$data['customer_code'];
				
				mkdir($dir,0777,true); // create folders
				$config['upload_path']   = $dir; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
				$this->load->library('upload', $config);
				
				foreach($img_id as $items){
					if ($this->upload->do_upload($items)) {
						$path = $this->upload->data();
					}else{
						/*echo $this->upload->display_errors();*/
					}
				}
		
        redirect('admin/customer');
    }

	public function update(){

		$customer_code = $this->input->post('customer_code');
		$premise_code = $this->input->post('premise_code');
        $data = array( 
            //'img' => $img, 
			//'idcard' => $this->input->post('idcard'), 
            'firstname' => $this->input->post('name'), 
            'lastname' => $this->input->post('sname'), 
			'addr' => $this->input->post('address'), 
			
			'province_id' => $this->input->post('province'),
			'amphurs_id' => $this->input->post('amphurs'), 
			'tumbon_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			//'birthday' => $this->input->post('bdate'), 
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			'sex' => $this->input->post('gender'), 
			'status' => $this->input->post('status'),
            'updated'=>date("Y-m-d H:m:s")
			
         ); 
		 

		 $bdate = $this->input->post('bdate');
		 if($bdate != ''){
			$data['birthday'] = $bdate;
		 }
		 
		 $this->Customer_Model->update($data,$customer_code); 

		//$config['upload_path']   = 'uploaded/customer/'; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
		$config['allowed_types'] = 'jpeg|jpg|png'; //รูปแบบไฟล์ที่ อนุญาตให้ Upload ได้
		$config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
		$config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
		$config['max_height']    = 0;
		$config["overwrite"]     = true;  
			//$config['encrypt_name']  = true; //กำหนดเป็น true ให้ระบบ เปลียนชื่อ ไฟล์  อัตโนมัติ  ป้องกันกรณีชื่อไฟล์ซ้ำกัน

		
		$img_id = array("contract1","contract2","contract3","contract4","idcardPremise","houseregisPremise","idcardSupporter","houseregisSupporter");
		
		$code = explode('Cus', $customer_code);
		$dir = "uploaded/customer/".$customer_code;

		$premise = array( 'udate'=>$data['updated'] ); 
		foreach($img_id as $items){
			if (!empty($_FILES[$items]['name'])) {
				$_FILES[$items]['name'] = $items."_".$code[1].".".pathinfo($_FILES[$items]['name'], PATHINFO_EXTENSION);
				$premise[$items] = $_FILES[$items]['name'];
			}
		}

		$this->Premise_Model->update($premise,$premise_code);
		
		foreach($img_id as $items){
			if (!empty($_FILES[$items]['name'])) {
				$config['upload_path']   = $dir; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
				$this->load->library('upload', $config);
				if ($this->upload->do_upload($items)) {
					$path = $this->upload->data();
				}else{
					//echo $this->upload->display_errors();
				}

			}
		}
        redirect('admin/customer');
    }

	public function detail($id = null)
	{
        $menu['mainmenu'] = 'setting';
		$menu['submenu'] = 'logs';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/customer_detail');
        $this->load->view('admin/footer');
	}



	public function getRes()
	{
		$type = $this->input->post('type');
		$cate = $this->input->post('cate');
		$val = str_replace(' ', '', $this->input->post('val'));

		
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Logs_Model->select($cate, $type, $val, $itemPerPage, $itemStt, $itemEnd);
		echo json_encode($data);
	}

	public function getResAll()
	{
		$type = $this->input->post('type');
		$cate = $this->input->post('cate');
		$val = str_replace(' ', '', $this->input->post('val'));

		$data = $this->Logs_Model->selectAllItems($cate, $type, $val);
		echo json_encode($data);
	}
	
}
