<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Agent extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 
		date_default_timezone_set("Asia/Bangkok"); // set timeZone
		//เรียกใช้งาน library
		//$this->load->library('mpdf');
		//$this->load->library('M_pdf');
		//$this->load->library('pdf');
		//$this->load->library('excel');

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
		$this->load->model('admin/Mechanic_Model');
        $this->load->model('admin/Agent_Model');
    } 

	public function index()
	{
		$menu['mainmenu'] = 'agent';
		$menu['submenu'] = 'agent';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/agent_list');
        $this->load->view('admin/footer');
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT  agent.* 
		, p.province_name, a.amphur_name, t.district_name
		FROM agent 
		LEFT JOIN provinces p ON agent.province_id = p.id 
		LEFT JOIN amphurs a ON agent.amphurs_id = a.id 
		LEFT JOIN tumbon t ON agent.district_id = t.id ";
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}

	public function create()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$data['province'] = $this->Agent_Model->province();
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'agent';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/agent_create',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		
		$prefixdate = date("ym");
		$prefixchar = 'A';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Agent_Model->getTogenID($prefixdate);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->agent_code,5,9));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		$data = array( 
			'agent_code' => $code, 
			'idcard' =>  implode(preg_split('/[-]/',  $this->input->post('idcard'))),
            'name' => $this->input->post('name'), 
            'sname' => $this->input->post('sname'), 
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			
			'office_name' => $this->input->post('name-office'),
			'office_address' => $this->input->post('address-office'),
			'office_province' => $this->input->post('province-office'),
			'office_amphurs' => $this->input->post('amphurs-office'),
			'office_district' => $this->input->post('district-office'),
			'office_zipcode' => $this->input->post('zipcode-office'), 
			'phone' => $this->input->post('tel-office'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
            'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         ); 
	
		 $bdate = $this->input->post('bdate');
		 if($bdate != '' && $bdate != null){
			$data['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $bdate),"Y-m-d"));
		 }

		 $create_date = $this->input->post('create-date');
		 if($create_date != '' && $create_date != null){
			$data['create_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $create_date),"Y-m-d"));
		 }else{
			$data['create_date'] = date(date_format(date_create(),"Y-m-d"));
		 }
        
         $this->Agent_Model->insert($data); 
        redirect('admin/agent');
    }

	
	public function getCharngByIdcard()
	{
		$idcard = $this->input->post('idcard');
		$data = $this->Agent_Model->getCharngByIdcard($idcard);		
		echo json_encode($data);
	}

	public function jsonAgent()
	{
		$search = $this->input->post('Search');
		//$itemPerPage = (int)$this->input->post('itemPerPage');
		//$itemStt = (int)$this->input->post('itemStt');
		//$itemEnd = (int)$this->input->post('itemEnd');

		//$data = $this->Mechanic_Model->Mechanic_Model($searchArray, $itemPerPage, $itemStt, $itemEnd);		
		$data = $this->Agent_Model->jsonAgent($search);		
		echo json_encode($data);
	}

	public function edit($id = null)
	{
		$data['res'] = $this->Agent_Model->getToEdit($id);
		$data['province'] = $this->Agent_Model->province();
        $data['amphoe'] = $this->Agent_Model->amphoe();
        $data['districs'] = $this->Agent_Model->districs();
		//$data['customer_premise'] = $this->Customer_Model->selectPremise($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'agent';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/agent_edit',$data);
        $this->load->view('admin/footer');
	}

	public function update(){
		
		$data = array( 
			'name' => $this->input->post('name'), 
            'sname' => $this->input->post('sname'), 
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			
			'office_name' => $this->input->post('name-office'),
			'office_address' => $this->input->post('address-office'),
			'office_province' => $this->input->post('province-office'),
			'office_amphurs' => $this->input->post('amphurs-office'),
			'office_district' => $this->input->post('district-office'),
			'office_zipcode' => $this->input->post('zipcode-office'), 
			'phone' => $this->input->post('tel-office'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
            'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         );
		 
		 $create_date = $this->input->post('create-date');
		 if($create_date != '' && $create_date != null){
			$data['create_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $create_date),"Y-m-d"));
		 }else{
			$data['create_date'] = date(date_format(DateTime::createFromFormat('d/m/Y',$this->converYYToyy()),"Y-m-d"));
		 }

		$agent_code = $this->input->post('agent_code');
        
		$this->Agent_Model->update($data,$agent_code);
        redirect('admin/agent');
    }

	public function PDF($id = null)
	{
		//เรียกใช้งาน Libary PDF    
		$this->load->library('Pdf');
		$agent = $this->Agent_Model->getToPDF($id);

		// สร้าง object สำหรับใช้สร้าง pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
        // กำหนดรายละเอียดของ pdf
        
		//$pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide';
		
         
        // กำหนดข้อมูลที่จะแสดงในส่วนของ header และ footer
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// กำหนดรูปแบบของฟอนท์และขนาดฟอนท์ที่ใช้ใน header และ footer
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // กำหนดค่าเริ่มต้นของฟอนท์แบบ monospaced 
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // กำหนด margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // กำหนดการแบ่งหน้าอัตโนมัติ
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        // กำหนดรูปแบบการปรับขนาดของรูปภาพ 
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        // ---------------------------------------------------------
         
        // set default font subsetting mode
		$DateControlDoc =  '(10-06-2021)';
        $pdf->setFontSubsetting(true);
		
		$this->Agent($pdf,$agent, $DateControlDoc);
		$this->AgentCoppy($pdf, $agent, $DateControlDoc);
			
		$file_name = $agent[0]->agent_code.'.pdf';
		$pdf->Output($file_name, 'I');
	}

	public function Agent($pdf, $agent, $DateControlDoc)
	{
		//######### Agent A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0008/DOC0008'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		if($agent != null){
			$pdf->SetFont('thsarabun', '', 15, '', true);
			$pdf->Text(153, 50.7, date_format(date_create($agent[0]->create_date),"d/m/Y"));
			$pdf->Text(22, 98, $agent[0]->name);
			$pdf->Text(90, 98, $agent[0]->sname);

			$bDate = date_create($agent[0]->bdate);
			$pdf->Text(170, 98, date_format($bDate,"d/m/Y"));
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(19, 107, $age);
			
			$pdf->Text(85, 107, $this->textFormat($agent[0]->idcard,'','-'));
			$pdf->Text(35, 115.6, $agent[0]->address);
			$pdf->Text(28, 124.5, $agent[0]->district_name);
			$pdf->Text(88, 124.5, $this->RemoveText('อำเภอ',$agent[0]->amphur_name));
			$pdf->Text(150, 124.5, $agent[0]->province_name);

			$pdf->Text(62, 133.3, $agent[0]->office_name);
			$pdf->Text(20, 142, $agent[0]->office_address);
			$pdf->Text(28, 150.7, $agent[0]->officeDistrict);
			$pdf->Text(96, 150.7, $agent[0]->officeAmphurs);
			$pdf->Text(155, 150.7, $agent[0]->officeProvince);
		}
		//######### Agent A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0008/DOC0008'.$DateControlDoc.'_p02.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		if($agent != null){
			$pdf->SetFont('thsarabun', '', 15, '', true);
			$pdf->Text(125, 69.8, $agent[0]->name.' '.$agent[0]->sname);
		}
	}

	public function AgentCoppy($pdf, $agent, $DateControlDoc)
	{
		//######### Agent B1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0008/DOC0008'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		if($agent != null){
			$pdf->Text(153, 50.7, date_format(date_create($agent[0]->create_date),"d/m/Y"));
			$pdf->Text(22, 98, $agent[0]->name);
			$pdf->Text(90, 98, $agent[0]->sname);

			$bDate = date_create($agent[0]->bdate);
			$pdf->Text(170, 98, date_format($bDate,"d/m/Y"));
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(19, 107, $age);
			
			$pdf->Text(85, 107, $this->textFormat($agent[0]->idcard,'','-'));
			$pdf->Text(35, 115, $agent[0]->address);
			$pdf->Text(28, 124, $agent[0]->district_name);
			$pdf->Text(88, 124, $this->RemoveText('อำเภอ',$agent[0]->amphur_name));
			$pdf->Text(150, 124, $agent[0]->province_name);

			$pdf->Text(62, 133, $agent[0]->office_name);
			$pdf->Text(20, 142, $agent[0]->office_address);
			$pdf->Text(28, 150, $agent[0]->officeDistrict);
			$pdf->Text(96, 150, $agent[0]->officeAmphurs);
			$pdf->Text(155, 150, $agent[0]->officeProvince);
		}
		//######### Agent B2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0008/DOC0008'.$DateControlDoc.'_p02.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
	}

	public function premise($id = null)
	{
		$data['res'] = $this->Agent_Model->getToEdit($id);
		/*
		$data['province'] = $this->Agent_Model->province();
        $data['amphoe'] = $this->Agent_Model->amphoe();
        $data['districs'] = $this->Agent_Model->districs();
		*/
		//$data['customer_premise'] = $this->Customer_Model->selectPremise($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'agent';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/agent_premise',$data);
        $this->load->view('admin/footer');
	}

	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	}

	public function converYYToyy($date = null)
	{
		$yy = ((int)date("Y")+543);
		$mm = (date("m"));
		$dd = (date("d"));
		return $dd.'/'.$mm.'/'.$yy;
	}

	public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			$temp = explode($text, $wording);
			$string = $temp[1];
		}
		return $string ;
	 }

}
