<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ProductMaster extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        //$this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductMaster_Model');
    } 

    public function index(){
		$data['productCate'] = $this->ProductMaster_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductMaster_Model->brandTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productmaster';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_master_list',$data);
        $this->load->view('admin/footer');
	}
    public function getResProduct(){
		$Search = $this->input->post('Search');
		$data = $this->ProductMaster_Model->getToList($Search);		
		echo json_encode($data);
	}

    ############  Create #############
    public function create(){
        $data['productCate'] = $this->ProductMaster_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductMaster_Model->brandTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productmaster';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_master_create',$data);
        $this->load->view('admin/footer');
	}
    public function insert(){

        $prefixdate = date("ym");
		$prefixchar = 'MP-';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->ProductMaster_Model->getToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->product_master_id,7,8));
			}
			$code = sprintf($prefix.'%02d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%02d',1);
		}
		
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));
		$produc_data = array( 
			'product_master_id' =>   $code ,
            'product_master_name' => $this->input->post('product-master-name'),
            'product_master_cate' => $this->input->post('product-master-cate'),
            'product_master_brand' => $this->input->post('product-master-brand'),
            'product_master_version' => $this->input->post('product-master-version'),
			'product_master_ref' => $this->input->post('product-master-ref'),
            'product_master_price' => $this->input->post('product-master-price'),
            'product_master_detail' => $this->input->post('product-master-detail'),
            'cdate'=> $date,
            'udate'=> $date
        );

		$this->ProductMaster_Model->ProductInsert($produc_data);

		// ประเภท การผ่อนชำระ
        /*
        $InstallmentAmount = $this->input->post('installment-format-amount');
        $idArr = [];
		$nume = 0;
		$res = $this->Product_Model->InstallmentTypeToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)$items->installment_type_id);
			}
			$nume = MAX($idArr);
		}
        
        for($i = 1; $i <= (int)$InstallmentAmount; $i++){
            $installment_type_data = array( 
                'installment_type_id' =>   sprintf('%03d',$nume+$i) ,
                'product_id' => $code,
                'amount_installment' => $this->input->post('installment-amount'.$i),
                'pay_per_month' => $this->input->post('installment-pay'.$i),
                'cdate'=> $date
            );

			$this->Product_Model->InstallmentTypeInsert($installment_type_data);
        }
        */
		
        redirect( base_url('/admin/productMaster'));
    }

    ############  Edit #############
    public function edit($id = null){
		$data['productCate'] = $this->ProductMaster_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductMaster_Model->brandTocombo();

        $data['res'] = $this->ProductMaster_Model->GetToEdit($id);
        //$data['installentType'] = $this->ProductMaster_Model->GetInstallmentTypeToEdit($id);

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productmaster';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_master_edit', $data);
        $this->load->view('admin/footer');
	}
    public function update(){
        $id = $this->input->post('id');
		$produc_data = array( 
            'product_master_brand' => $this->input->post('product-master-brand'),
            'product_master_name' => $this->input->post('product-master-name'),
            'product_master_version' => $this->input->post('product-master-version'),
            'product_master_ref' => $this->input->post('product-master-ref'),
            'product_master_price' => $this->input->post('product-master-price'),
            'product_master_detail' => $this->input->post('product-master-detail'),
            'udate'=>date("Y-m-d H:m:s")
        );

		$this->ProductMaster_Model->update($produc_data, $id);
		
        redirect( base_url('/admin/productMaster'));
    }


    ############  Delete #############
    public function DeleteProduct(){
        $id = $this->input->post('id');
		$data = $this->ProductMaster_Model->DeleteProduct($id);
		echo json_encode($data);
    }


	public function ProductMaster_get(){
		$curent_date = Date('Y-m-d H:i:s');
		$res = new stdClass();
        
        $Query = "SELECT p.*, pc.cate_name, b.brand_name
        FROM product_master p 
        LEFT JOIN product_category pc ON p.product_master_cate = pc.cate_id
        LEFT JOIN brand b ON p.product_master_brand = b.brand_id";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	  }

}