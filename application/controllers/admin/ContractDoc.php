<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ContractDoc extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        //$this->load->model('admin/Brand_Model');
        //$this->load->model('admin/Product_Model');
        //$this->load->model('admin/ProductMaster_Model');
        $this->load->model('admin/ProductSet_Model');
        $this->load->model('admin/ContractDoc_Model');
    } 

    ############  List  #############
	public function index(){
		$data['productCate'] = $this->ProductSet_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductSet_Model->brandTocombo();
        
        $data['resContractDoc'] = $this->ContractDoc_Model->getResContractDoc();

        $menu['mainmenu'] = 'contractDoc';
		$menu['submenu'] = 'contractDoc';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractDoc_list',$data);
        $this->load->view('admin/footer');
	}
    

    ############  Create  #############

    public function create(){
		$data['contractType'] = $this->ContractDoc_Model->statusTocombo('stautus_category','211003');
		$data['useStatus'] = $this->ContractDoc_Model->statusTocombo('stautus_category','211005');

        $menu['mainmenu'] = 'contractDoc';
		$menu['submenu'] = 'contractDoc';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractDoc_create',$data);
        $this->load->view('admin/footer');
	}

    public function insert(){
		
		$code = '';
        $prefixdate = date("ym");
        $prefixText = 'CD';
        $prefix = $prefixText.$prefixdate;
		$res = $this->ContractDoc_Model->getContractDocToGenCode();
		if($res){
			$code = sprintf($prefix.'%02d',(int)substr($res[0]->contract_id,6,8) +1);
		}else{
			$code = sprintf($prefix.'%02d',1);
		}
        
        ####  product doc ####
        $date = date(date_format(date_create(),"Y-m-d H:i:s"));
        $contract_name = $this->input->post('contract_name');
        $contract_type = $this->input->post('contract_type');
        $contracDoc_data = array( 
            'contract_id' => $code,
            'contract_type' => $contract_type,
            'contract_name' => $contract_name,
            'contract_detail' => $this->input->post('contract_detail'),
            'use_status' => $this->input->post('use_status'),
            'cdate'=> $date,
            'udate'=> $date
            
        ); 
        $this->ContractDoc_Model->ContractDocInsert($contracDoc_data);


        ####  product img ####
        $resContract_type = $this->ContractDoc_Model->getContractType($contract_type);
		$coundFile = count($_FILES['contract_file']['name']);
        for($i = 0; $i < $coundFile; $i++){
            $_FILES['file']['name']       = $_FILES['contract_file']['name'][$i];
            $_FILES['file']['type']       = $_FILES['contract_file']['type'][$i];
            $_FILES['file']['tmp_name']   = $_FILES['contract_file']['tmp_name'][$i];
            $_FILES['file']['error']      = $_FILES['contract_file']['error'][$i];
            $_FILES['file']['size']       = $_FILES['contract_file']['size'][$i];

            ##### File upload configuration #####
            //$config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
            $config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
            $config['max_height']    = 0;  //ขนาดความสูงสูงสดุ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
            $config["overwrite"] = 	true;

            ##### create dir #####
            $uploadPath = 'uploaded/ContractDoc/'.$resContract_type[0]->label.'/'.$contract_name;
            if (!file_exists($uploadPath)) {
               mkdir($uploadPath,0777,true); // create main folder
            }

            ##### create img to dir ####
            $config['upload_path'] = $uploadPath; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('file')) {
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                
                ########  insert img #######
                $contracDoc_img = array( 
                    'contract_id' => $contracDoc_data['contract_id'],
                    'name' => $filename,
                    'path' => $uploadPath.'/'.$filename,
                    'order_sort' => $i,
                    'cdate'=> $date,
                    'udate'=> $date
                ); 
                $this->ContractDoc_Model->ContractImgInsert($contracDoc_img);
            }else{
                echo $this->upload->display_errors();
            }
        }

        redirect(base_url('admin/contractDoc'));
    }




    ############  Edit  #############
    public function edit($id = null){
        $data['res'] = $this->ContractDoc_Model->GetToEdit($id);
        $data['contractImg'] = $this->ContractDoc_Model->GetContractImgToEdit($data['res'][0]->contract_id);
        $data['contractType'] = $this->ContractDoc_Model->statusTocombo('stautus_category','211003');
		$data['useStatus'] = $this->ContractDoc_Model->statusTocombo('stautus_category','211005');

        $menu['mainmenu'] = 'contractDoc';
		$menu['submenu'] = 'contractDoc';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractDoc_edit', $data);
        $this->load->view('admin/footer');
	}

    public function update(){
        $id = $this->input->post('id');
        $date = date(date_format(date_create(),"Y-m-d H:i:s"));

        $contracDoc_data = array( 
            'contract_id' => $id,
            'contract_type' => $this->input->post('contract_type'),
            'contract_name' => $this->input->post('contract_name'),
            'contract_detail' => $this->input->post('contract_detail'),
            'use_status' => $this->input->post('use_status'),
            'udate'=> $date
        ); 
		$this->ContractDoc_Model->update($contracDoc_data, $id);
        redirect(base_url('admin/ContractDoc'));
    }


    ############  Delete  #############
    /*public function DeleteProduct(){
        $id = $this->input->post('id');
		$data = $this->Product_Model->DeleteProduct($id);
		echo json_encode($data);
    }*/

    public function delProductSetInstallment(){
        $id = $this->input->post('id');
		$data = $this->ProductSet_Model->delProductSetInstallment($id);
		echo json_encode($data);
    }

    public function delProductSet_sub(){
        $id = $this->input->post('id');
		$data = $this->ProductSet_Model->delProductSet_sub($id);
		echo json_encode($data);
    }


}
