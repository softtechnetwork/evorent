<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContractService extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
        $this->load->model('admin/StatusCategory_Model');
        //$this->load->model('admin/Service_Model');
        $this->load->model('admin/ContractService_Model');
		$this->load->model('admin/Contract_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 

		date_default_timezone_set("Asia/Bangkok"); // set timeZone
    } 

	public function index()
	{
        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractService';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractService_list');
        $this->load->view('admin/footer');
	}
	public function getRes()
	{
		$type = $this->input->post('type');
		$Search = $this->input->post('search');

		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');
		
		$data = $this->ContractService_Model->select($type, $Search, $itemPerPage, $itemStt, $itemEnd);		
		echo json_encode($data);
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT ROW_NUMBER() OVER ( ORDER BY t.contract_code ) AS RowNum, ps.*
		,c.firstname, c.lastname, c.tel, c.email, c.idcard 
		, product.product_name, brand.brand_name 
		FROM contract_service ps
		INNER JOIN contract t ON ps.contract_code = t.contract_code
		LEFT JOIN customer c ON t.customer_code = c.customer_code
		LEFT JOIN product_set product  ON t.product_id = product.product_id 
		LEFT JOIN brand  ON t.product_brand = brand.brand_id";
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
	public function getResAll()
	{
		$type = $this->input->post('type');
		$Search = $this->input->post('Search');
		
		$data = $this->ContractService_Model->selectAllItems($type, $Search);
		echo json_encode($data);
	}

    public function view($id = null)
	{
        $data['serviceRES'] = $this->ContractService_Model->selectOne($id);
		$data['history'] = $this->ContractService_Model->selectHistory($id, $data['serviceRES'][0]->contract_code);

		
		$data['address'] = $this->ContractService_Model->detailAddress($data['serviceRES'][0]->customer_code, 'ที่อยู่ปัจจุบัน');
		$data['installationLocation'] = $this->ContractService_Model->addrProductInstall($data['serviceRES'][0]->installationlocation);

        $menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractService';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contractService_view',$data);
        $this->load->view('admin/footer');
	}


	public function insertServiceHistory(){
		//$code = date("dmY-His");
		$id = $this->input->post('service-id');
		$contract_code = $this->input->post('contract_code');
		$name = $this->input->post('service-history-name');
		$date = $this->input->post('service-history-date');
		$tachn = $this->input->post('service-history-tachn');
		$remark = $this->input->post('service-history-remark');

		$data = array( 
			'service_code' => $id,
            'contract_code' => $contract_code,
			'name' =>  $name, 
            'service_date' => $date, 
			'service_by' =>  $tachn, 
            'remark' => $remark, 
            'cdate' => date(date_format(date_create(),"Y-m-d H:i:s")),
            'udate' => date(date_format(date_create(),"Y-m-d H:i:s"))
         );

		$this->ContractService_Model->insertServiceHistory($data); 
        redirect('admin/contractService/view/'.$id);
    }












	/*public function create()
	{
        $data['resTemp'] = $this->Service_Model->selectTemp();
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'service';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/service_create',$data);
        $this->load->view('admin/footer');
	}*/

    /*public function getTempOne()
	{
		$id = $this->input->post('id');
		$data = $this->Service_Model->selectTempOne($id);		
		echo json_encode($data);
	}*/

	

	/*public function insert(){
		$code = date("dmY-His");
		$cate = $this->input->post('cate');
		$statusCate = $this->Status_Model->getStatus($cate);
		
		$prefix = 0;
		if($statusCate != null){
			$arr  = array();
			foreach($statusCate as $item){
				array_push($arr, $item->status_code);
			}
			$prefix = max($arr)+1; 
		}

		$data = array( 
			'status_code' =>  $prefix, 
            'stautus_category' => $cate, 
			'label' =>  $this->input->post('st'), 
            'detail' => $this->input->post('stdetail'), 
            'color' => $this->input->post('color'), 
            'background_color' => $this->input->post('bgcolor'), 
            'cdate'=>date("Y-m-d H:m:s")
         );
		$this->Status_Model->insert($data); 
		
        redirect('admin/status');
    }*/

	/*public function update(){
		$id = $this->input->post('id');
        $data = array( 
			'stautus_category' => $this->input->post('cate'), 
			'label' =>  $this->input->post('st'), 
            'detail' => $this->input->post('stdetail'), 
            'color' => $this->input->post('color'), 
            'background_color' => $this->input->post('bgcolor'), 
            'udate'=>date("Y-m-d H:m:s")
         ); 

		 $this->Status_Model->update($data,$id); 

        redirect('admin/status');
    }*/
}
