<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class WebBanner extends CI_Controller {
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductMaster_Model');
        $this->load->model('admin/ProductSet_Model');
    } 

    ############  List  #############
	public function index(){
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webBanner';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webbanner');
        $this->load->view('admin/footer');
	}
    public function banner_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        //$product_id = $this->input->post('name');
        // $desc = $this->input->post('desc');
        $Query = "SELECT * FROM web_banner ORDER BY sortting asc";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}
	public function update_sorting(){
        $res = new stdClass();
        $curent_date = Date('Y-m-d H:i:s');
		
		$id = $this->input->post('id');
        $sortable = $this->input->post('sortable');
		
        $obj = array( 
			'sortting' => $sortable,
		); 

		$obj['update_date'] = date(date_format(date_create(),"Y-m-d H:i:s"));
        $this->db->where("id", $id);
        $this->db->update("web_banner", $obj); 

		$res->status = true;
		//$res->datas = base_url('admin/product_cate_edit/'.$cate_id);
		$res->datas = base_url('admin/WebBanner');
		$res->massege = 'บันทึกสำเร็จ';
		$res->status_code = '000';

		echo json_encode($res);
	}


    
    public function banner_manage($banner_id = null){
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webBanner';

        $data['banner_id'] = $banner_id;

        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webbanner_manage',$data);
        $this->load->view('admin/footer');
	}
    public function banner_get_once(){
        $curent_date = Date('Y-m-d H:i:s');
        $banner_id = $this->input->post('banner_id');
        
        $Query = " SELECT * FROM web_banner WHERE id = '".$banner_id."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}
    public function banner_actions(){
        $res = new stdClass();
        $curent_date = Date('Y-m-d H:i:s');
        $id = $this->input->post('id');
		$method = $this->input->post('method');
		$title = $this->input->post('title');
		$link = $this->input->post('link');
		$detail = $this->input->post('detail');
		$status = $this->input->post('status');
		$img = $this->input->post('img');

        $obj = array( 
			'name' => $title, 
			'link' => $link,
			'detail' => $detail,
			'display_status' => $status,
			'create_date'=>date(date_format(date_create(),"Y-m-d H:i:s")),
			'update_date'=>date(date_format(date_create(),"Y-m-d H:i:s"))
		); 

        if($method == 'insert'){
            $upload = $this->do_upload('input-img');
            if($upload->status){
                $obj['img'] = $upload->message;
                $this->db->insert("web_banner", $obj);
            }
        }

        if($method == 'update'){
            $upload = $this->do_upload('input-img');
            if($upload->status){
                $obj['img'] = $upload->message;
            }
            $this->db->set($obj); 
            $this->db->where("id", $id);
            $this->db->update("web_banner", $obj); 
        }

        $redirected = 'admin/WebBanner/';
		redirect(base_url($redirected));
	}

    public function do_upload($files){
        $res = new stdClass();
        $config['allowed_types'] = 'jpeg|jpg|png'; //รูปแบบไฟล์ที่ อนุญาตให้ Upload ได้
        $config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config['max_height']    = 0;  //ขนาดความสูงสูงสดุ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config["overwrite"] = 	true;
        //$config['encrypt_name']  = true; //กำหนดเป็น true ให้ระบบ

        ##### name img ####
        $today = strtotime(date("d-m-Y H:i:s")); 
		if (!empty($_FILES[$files]['name'])) {
			$_FILES[$files]['name'] = $today.".".pathinfo($_FILES[$files]['name'], PATHINFO_EXTENSION);
		}
        ##### create path ####
		$upload_path = "./uploaded/ImageUpload/banner/";
		if (!file_exists($upload_path)) {
			if (!mkdir($upload_path, 0777, true)) {//0777
				die($upload_path.' Failed to create folders...');
			}
        }

        ##### create img to dir ####
        $fullPath = $upload_path.'/'.$_FILES[$files]['name'];
		$config['upload_path'] = $upload_path; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload($files)) {
			$path = $this->upload->data();
            $res->status = true;
            $res->message = $fullPath;
		}else{
            $res->status = false;
            $res->message = $this->upload->display_errors();
            $res->message = $fullPath;
		}

        return $res;
    }

}
