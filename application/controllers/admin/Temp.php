<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temp extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('session');
		
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->helper('file'); 
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Temp_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Product_Model');
		$this->load->model('admin/Logs_Model');
		$this->load->model('admin/Premise_Model');

        $this->load->model('admin/Loan_Model');
		$this->load->model('admin/Service_Model');

		 // load db ( agent )
		 $this->load->config('externaldb');
		 $this->db_config = $this->config->item('agent_db');
		 $this->connectSeminarDB();

		 
        
    } 

	
	public function index()
	{
 		$data['resStatus'] = $this->Status_Model->requestStatus('210602');
        
		$menu['mainmenu'] = 'home';
		$menu['submenu'] = 'temp';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/temp_list', $data);
        $this->load->view('admin/footer');
	}
	
	public function TempGetRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Temp_Model->getToList($Search, $itemStt, $itemEnd);		
		echo json_encode($data);
	}
	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Temp_Model->selectAllItems($Search);
		echo json_encode($data);
	}

	public function GetProducts()
	{
		$productCateId = $this->input->post('productCateId');
		$data = $this->Temp_Model->GetProducts($productCateId);		
		echo json_encode($data);
	}
	
	public function GetSubByProduct()
	{
		$product_id = $this->input->post('product_id');
		$data = $this->Temp_Model->GetSubByProduct($product_id);		
		echo json_encode($data);
	}

	public function GetInstallmentTypeByProductID()
	{
		$product_id = $this->input->post('product_id');
		$data = $this->Temp_Model->GetInstallmentTypeByProductID($product_id);		
		echo json_encode($data);
	}

	########## Create ##########
	public function created()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('210602');
		$data['Customer'] =  $this->Temp_Model->CustomerToCombo();
		$data['ProductCate'] =  $this->Temp_Model->ProductCateToCombo();

		$data['Broker'] =  $this->Temp_Model->CharngToCombo('broker',true);
		$data['Agent'] =  $this->Temp_Model->CharngToCombo('agent',true);

		$data['province'] = $this->Temp_Model->province();
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'temp';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/temp_create',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){

		$prefixdate = date("ym"); 
		$prefixText = 'C';
		$prefix =  $prefixText.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Temp_Model->getTempToCreatGenCode($prefix);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->temp_code,5,10));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		$date = date(date_format(date_create(),"Y-m-d H:i:s"));

		$installmentCheck = "[".$this->input->post('installmentCheck')."]";
		$rental_period = (int)json_decode($installmentCheck)[0]->amount_installment;
		$monthly_rent = (int)json_decode($installmentCheck)[0]->per_month_price;
		$data = array( 
			'temp_code' => $code, 
			'customer_code' => $this->input->post('customer'),
			'product_cate' => $this->input->post('product-cate'),
            'product_id' => $this->input->post('product'),
			'product_brand' => $this->input->post('product-brand-id'),
			'product_version' => $this->input->post('product-version'),
			'payment_due' => $this->input->post('payment-due'),
			'addr' => $this->input->post('inhabited'),
			'installment' => $installmentCheck,
			'down_payment' => $this->input->post('down-payment'),
			'advance_payment' => $this->input->post('advance-payment'),
			'installation_fee' => $this->input->post('installation-fee'),
			'sale_code' => $this->input->post('sale-id'), 
			'techn_code' => $this->input->post('techn-id'), 
			'remark' => $this->input->post('remark'),
			'status' => $this->input->post('status'),
			//'product_price' => $this->input->post('product-price'),
			'product_count' => $this->input->post('product-count'),
			'product_number' => $this->input->post('property-number'),
			//'product_serial' => $this->input->post('product-serial'),

			//'lease_date' => $date,
			
			
			//'monthly_rent' => $this->input->post('monthly-rent'),
			'monthly_rent' => $monthly_rent,
			//'monthly_plus_tax' => $this->input->post('monthly-plus-tax'),
			//'installment' => $this->input->post('installment'),
			//'rental_period' => $this->input->post('rental-period'),
			'rental_period' => $rental_period,
			'supporter' => (int)$this->input->post('supporter-check'),
            'cdate'=> $date
        ); 

		if( $this->input->post('contract-do-date') != null){
			$data['contract_do_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-do-date')),"Y-m-d"));
		}
		if( $this->input->post('contract-date') != null){
			$data['contract_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-date')),"Y-m-d"));
		}
		if( $this->input->post('payment-start-date') != null){
			$data['payment_start_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
		}
		
		$this->Temp_Model->insert($data); 
		
		############# Creat Installment ############
		$newStatus = $this->Temp_Model->getStatus((int)$data['status']);
		$newInstallent = $this->Temp_Model->getInstallment($data['temp_code']);
	   
		if($newStatus[0]->status_code == 0 && $newInstallent == null){ 
		   
		   $dataistall = array( 
			   //'loan_code' => $dataloan['loan_code'],
			   'temp_code' => $data['temp_code'],
			   'product_id' =>$data['product_id'],
			   'customer_code' => $data['customer_code'],
			   'cdate'=> $data['cdate']
		   ); 

		   if((int)$data['payment_due'] < 10){ $data['payment_due'] = "0".$data['payment_due'];}

		   $cdated = date("Y-m", strtotime($data['payment_start_date']));
		   for ($i=1; $i <= (int)$rental_period ; $i++) { 
			   	######### Payment due date #########
			   	$payment_duedate = null;
			   	if((int)$i == 1){
				   if( $this->input->post('payment-start-date') != null){
					   $payment_duedate = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
				   }
			   	}else{
				   	$mont = $i-1;
					$montChange = date('Y-m', strtotime("+1 months ", strtotime($cdated)));
					$cdated = $montChange;
					$maxdate = date("Y-m-t", strtotime($montChange));
					$date = explode('-', $maxdate); 
					
					if($date[1] != '02'){
						$date[2] = $data['payment_due'];
					}else{
						if( (int)$data['payment_due'] < (int)$date[2]){
							$date[2] = $data['payment_due'];
						}
					}
					$payment_duedate = date(implode('-', $date));
			   	}

			   	######### Extend due date #######
			   	$extend_duedate = null;
				$payment_duedate_exp = explode('-', $payment_duedate);
			   	if($payment_duedate_exp[2] == '15'){
					$maxDays = date("Y-m-t",strtotime($payment_duedate));
					$date = explode('-', $maxDays); 
					if((int)$date[2] < 30){
						$payment_duedate_exp[2] = $date[2];
					}else{
						$payment_duedate_exp[2] = '30';
					}
					$extend_duedate = date(implode('-', $payment_duedate_exp));
				}else{
					$montChange = date('Y-m', strtotime("+1 months ", strtotime(date("Y-m", strtotime($payment_duedate)))));
					$date = explode('-', $montChange); 
					$date[2] = '15';
					$extend_duedate = date(implode('-', $date));
				}
			   
				$dataistall['status'] = $this->Temp_Model->requestStatus('210603', 0)[0]->id;
				$dataistall['status_code'] = 0;
				$dataistall['payment_amount'] = null;
				$dataistall['period'] = $i;
				$dataistall['payment_duedate'] = $payment_duedate;
				$dataistall['extend_duedate'] = $extend_duedate;
				$dataistall['installment_payment'] = $monthly_rent;
			   
			   	$this->Temp_Model->insertInstallment($dataistall);
		   }
		}
		###########################################

		
		######## Create Serial ##########
		$SubByProduct = $this->Temp_Model->GetSubByProduct($data['product_id']);
		if($SubByProduct){
			foreach($SubByProduct as $item){
				$serialnumber = array( 
					'contract_code' => $data['temp_code'],
					'product_id' => $item->product_id,
					'product_sub_id' => $item->product_sub_id,
					'no' => 1,
					//'serial_number' => $this->input->post('serial_'.$item->product_sub_id),
					'cdate'=> $data['cdate']
				);
				$this->Temp_Model->CreateSerial($serialnumber); 
			}
		}
		/*else{
			$serialnumber = array( 
				'contract_code' => $data['temp_code'],
				'product_id' => $data['product_id'],
				'no' => 1,
				//'serial_number' => $this->input->post('serial'),
				'cdate'=> $data['cdate']
			); 
			$this->Temp_Model->CreateSerial($serialnumber); 
		}*/
		##################################


		################ Service ###################
		$newService = $this->Temp_Model->getService($data['temp_code']);
		if($newStatus[0]->status_code == 0 && $newService == null){ 
			$dataService = array( 
				//'loan_code' => $dataloan['loan_code'],
				'temp_code' => $data['temp_code'],
				'service_count' => 1,
				'service_round' => 0,
				'service_round_unit' => 'month',
				'cdate' =>$data['cdate']
			);
			$this->Service_Model->insert($dataService);
		}
		###########################################
		
		
		######## Supporter ########## //ยังไม่สามารถให้แก้ไขข้อมูลได้
		if($data['supporter'] == 1){
			$dataSupporter = array( 
				'temp_code' => $data['temp_code'],
				'fname' => $this->input->post('name-supporter'),
				'lname' => $this->input->post('sname-supporter'),
				'idcard' => $this->input->post('idcard-supporter'),
				//'bdate' => $this->input->post('bdate-supporter'),
				'tel' => $this->input->post('mobile-supporter'),
				'addr' => $this->input->post('address-supporter'),
				'province_id' => $this->input->post('province-supporter'),
				'amphurs_id' => $this->input->post('amphurs-supporter'),
				'district_id' => $this->input->post('district-supporter'),
				'zip_code' => $this->input->post('zipcode-supporter'),
				'cdate'=> $data['cdate']
			);
			##### Bdate ####
			$supporterBdate = $this->input->post('bdate-supporter');
			if( $supporterBdate != null){
				$dataSupporter['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $supporterBdate),"Y-m-d"));
			}
			$this->Temp_Model->insertSupporter($dataSupporter); 
		}
		##################################
		
        redirect('admin/temp');
    }
	###########################

	#### Edit ####
	public function edit($id = null)
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('210602');
		$data['res'] = $this->Temp_Model->selectOne($id);
		$data['supporter'] = $this->Temp_Model->selectSupporter($id);

		$data['inhabited'] = $this->Temp_Model->getInhabitedEdit($data['res'][0]->addr);
		$data['province'] = $this->Temp_Model->province();

		$menu['mainmenu'] = 'home';
		$menu['submenu'] = 'temp';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/temp_edit',$data);
		$this->load->view('admin/footer');
	}

	public function update(){

		$temp_code = $this->input->post('temp_code');
		$date = date("Y-m-d H:m:s");
		$product_id = $this->input->post('product_id');
		$data = array( 
			//'addr' => $this->input->post('inhabited'),
			//'payment_due' => $this->input->post('payment-due'),
			'advance_payment' => $this->input->post('advance-payment'),
			'installation_fee' => $this->input->post('installation-fee'),
			//'techn_code' => $this->input->post('sale-id'), 
			'remark' => $this->input->post('remark'),
			'status' => $this->input->post('estatus'),
            'udate'=>$date
         ); 

		 /*
		 if( $this->input->post('contract-do-date') != null){
			$data['contract_do_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-do-date')),"Y-m-d"));
		 }
		 if( $this->input->post('contract-date') != null){
			$data['contract_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('contract-date')),"Y-m-d"));
		 }
		 if( $this->input->post('payment-start-date') != null){
			$data['payment_start_date'] = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
		 }
		 */


		######## Supporter ##########
		$oldSupport = (int)$this->input->post('supportered');
		$newSupport = (int)$this->input->post('supporter-check');
		
		$dataSupporter = array();
		if( $oldSupport == 0 && $newSupport == 1){
			$data['supporter'] = $newSupport;
			$dataSupporter['temp_code'] = $temp_code;
			$dataSupporter['fname'] = $this->input->post('name-supporter');
			$dataSupporter['lname'] = $this->input->post('sname-supporter');
			$dataSupporter['idcard'] = $this->input->post('idcard-supporter');
			$dataSupporter['tel'] = $this->input->post('mobile-supporter');
			$dataSupporter['addr'] = $this->input->post('address-supporter');
			$dataSupporter['province_id'] = $this->input->post('province-supporter');
			$dataSupporter['amphurs_id'] = $this->input->post('amphurs-supporter');
			$dataSupporter['district_id'] = $this->input->post('district-supporter');
			$dataSupporter['zip_code'] = $this->input->post('zipcode-supporter');
			$dataSupporter['cdate'] = $data['udate'];
		
			##### Bdate ####
			$supporterBdate = $this->input->post('bdate-supporter');
			if( $supporterBdate != null){
				$dataSupporter['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $supporterBdate),"Y-m-d"));
			}
		}
		##################################

		$this->Temp_Model->update($data,$temp_code); // update temp
		
		if( $oldSupport == 0 && $newSupport == 1){
			$this->Temp_Model->insertSupporter($dataSupporter);  // create supporter
		}


		 ############# Creat Installment ############
		 $newStatus = $this->Temp_Model->getStatus((int)$data['status']);
		 $newTemp = $this->Temp_Model->getTemp($temp_code);
		 $newInstallent = $this->Temp_Model->getInstallment($temp_code);
		
		 if($newStatus[0]->status_code == 0 && $newInstallent == null){ 
			//$myJSON = json_decode($newTemp[0]->installment);
			$dataistall = array( 
				//'loan_code' => $dataloan['loan_code'],
				'temp_code' => $newTemp[0]->temp_code,
				'product_id' =>$newTemp[0]->product_id,
				'customer_code' => $newTemp[0]->customer_code,
				'cdate'=> $data['udate']
			); 

			if((int)$newTemp[0]->payment_due < 10){ $newTemp[0]->payment_due = "0".$newTemp[0]->payment_due;}

			for ($i=1; $i <= (int)$newTemp[0]->rental_period ; $i++) { 
				$payment_duedate = null;
				//-------------- Payment due date --------------------//
				if((int)$i == 1){
					if( $this->input->post('payment-start-date') != null){
						$payment_duedate = date(date_format(DateTime::createFromFormat('d/m/Y', $this->input->post('payment-start-date')),"Y-m-d"));
					}
				}else{
					$mont = $i-1;
					$montChange = date('Y-m-d', strtotime("+".$mont." months ", strtotime($data['payment_start_date'])));
					$maxdate = date("Y-m-t", strtotime($montChange));
					$date = explode('-', $maxdate); 
					
					if((int)$data['payment_due'] < (int)$date[2]){
						$date[2] = $data['payment_due'] ;
					}
					//$payment_duedate = implode('-', $date);
					$payment_duedate = date(implode('-', $date));
				}
				//---------------------------------------------------//
				
				//-------------- พักชำระ  --------------------//				
				//if($myJSON[0]->per_month_price_include_vat == 0){
					//$dataistall['status'] = $this->Temp_Model->requestStatus('210603', 2)[0]->id;
					//$dataistall['status_code'] = 2;
					//$dataistall['payment_amount'] = 0;
					////$dataistall['installment_date'] = $dataloan['cdate'];
	
				//}else{
					$dataistall['status'] = $this->Temp_Model->requestStatus('210603', 0)[0]->id;
					$dataistall['status_code'] = 0;
					//$dataistall['payment_amount'] = null;
					//$dataistall['installment_date'] = null;
				//}
				//-------------------------------------------//

				$dataistall['period'] = $i;
				$dataistall['payment_duedate'] = $payment_duedate;
				$dataistall['installment_payment'] = $newTemp[0]->monthly_rent;
				
				$this->Temp_Model->insertInstallment($dataistall);
			}
		 }
		 ###########################################

		 ################ Service ###################
		 $newService = $this->Temp_Model->getService($temp_code);
		 if($newStatus[0]->status_code == 0 && $newService == null){ 
			 $dataService = array( 
				 //'loan_code' => $dataloan['loan_code'],
				 'temp_code' => $temp_code,
				 'service_count' => 1,
				 'service_round' => 0,
				 'service_round_unit' => 'month',
				 'cdate' =>$data['udate']
			 );
			 $this->Service_Model->insert($dataService);
		 }
		 ###########################################

		################# Create Logs ##############
		$LogsJSON = json_encode($data);
		$code = date("dmY-His");
		$logs_data = array( 
			//'img' => $img, 
			'logs_code' =>  "Logs".$code, 
			'ref_code' => $temp_code, 
			'category' => 'Temp', 
			'logs' => $LogsJSON, 
			'create_logs'=>$data['udate']
			
		);
		////$this->Logs_Model->insert($logs_data); 
		
		###########################################

        redirect('admin/temp');
    }

	public function premise($id = null)
	{
		$data['res'] = $this->Temp_Model->selectOne($id);
		/*
		$data['province'] = $this->Agent_Model->province();
        $data['amphoe'] = $this->Agent_Model->amphoe();
        $data['districs'] = $this->Agent_Model->districs();
		*/
		//$data['customer_premise'] = $this->Customer_Model->selectPremise($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'temp';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/temp_premise',$data);
        $this->load->view('admin/footer');
	}

	public function contractPDF($id = null)
	{
		//เรียกใช้งาน Libary PDF    
		$this->load->library('Pdf');
		$res = $this->Temp_Model->selectOneDeatil($id);
		$subProduct = $this->Temp_Model->SubByProductId($res[0]->temp_code, $res[0]->product_id);

		$address1 = $this->Temp_Model->detailAddress($res[0]->customer_code, 'ที่อยู่ตามบัตรประชาชน');
		$address2 = $this->Temp_Model->detailAddress($res[0]->customer_code,'ที่อยู่ปัจจุบัน');
		$address3 = $this->Temp_Model->detailAddress($res[0]->customer_code,'ที่อยู่สถานที่ทำงาน');
		
		$installationLocation = $this->Temp_Model->detailInstallationLocation($res[0]->installationlocation);
		
		$broker = $this->Temp_Model->CharngToPDF($res[0]->sale_code);
		$agent = $this->Temp_Model->CharngToPDF($res[0]->techn_code);

		$Supporter = null;
		if($res[0]->supporter == 1){
			$Supporter = $this->Temp_Model->selectSupporter($id);
		}
		// สร้าง object สำหรับใช้สร้าง pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
        // กำหนดรายละเอียดของ pdf
        
		//$pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide';
		
         
        // กำหนดข้อมูลที่จะแสดงในส่วนของ header และ footer
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// กำหนดรูปแบบของฟอนท์และขนาดฟอนท์ที่ใช้ใน header และ footer
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // กำหนดค่าเริ่มต้นของฟอนท์แบบ monospaced 
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // กำหนด margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // กำหนดการแบ่งหน้าอัตโนมัติ
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        // กำหนดรูปแบบการปรับขนาดของรูปภาพ 
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        // ---------------------------------------------------------
         
        // set default font subsetting mode
		$DateControlDoc =  '(10-06-2021)';
        $pdf->setFontSubsetting(true);
		
		$this->Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
		$this->ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter);
		$this->Agreement($pdf, $DateControlDoc, $res);
		$this->AgreementCoppy($pdf, $DateControlDoc, $res);	
		$this->Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
		$this->ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc, $subProduct,$agent );
		
		if($Supporter != null){
			$this->Supporter($pdf, $DateControlDoc, $res, $Supporter);
			$this->SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter);	
		}
			
		$file_name = $res[0]->temp_code.'.pdf';
		$pdf->Output($file_name, 'I');
	}

	public function Contract($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//เลขบัตรประจำตัวประชาชน '.textFormat($res[0]->idcard,'','-'));

		//######### Contract A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0002/DOC0002'.$DateControlDoc.'_p01.jpg');
		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->SetAutoPageBreak(TRUE, 0);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(169, 50.5, $res[0]->temp_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		//######### Contract A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0002/DOC0002'.$DateControlDoc.'_p02.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->SetAutoPageBreak(TRUE, 0);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_sub_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_sub_name.' : '.$item->serial_number : ', '.$item->product_sub_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));


		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}
	public function ContractCoppy($pdf, $res, $address1, $address2, $installationLocation, $address3, $DateControlDoc, $subProduct, $Supporter)
	{
		//เลขบัตรประจำตัวประชาชน '.textFormat($res[0]->idcard,'','-'));

		//######### Contract A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0002/DOC0002'.$DateControlDoc.'_p01.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->SetAutoPageBreak(TRUE, 0);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(169, 50.5, $res[0]->temp_code);
		$currentDate = date_format(date_create($res[0]->contract_do_date),"d/m/Y");
		$pdf->Text(131, 62.7, $currentDate);
		
		$pdf->Text(41, 89.3, $res[0]->firstname.' '.$res[0]->lastname);

		
		$bDate = date_create($res[0]->birthday);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(100, 89.3, $age);

		$pdf->Text(150, 89.3, date_format($bDate,"d/m/Y"));
		$pdf->Text(50, 98.2, $this->textFormat($res[0]->idcard,'','-'));
		$pdf->Text(13, 115.8, $address1[0]->address);
		$pdf->Text(35, 133.4, $address1[0]->district_name);
		$pdf->Text(125, 133.4, $this->RemoveText('อำเภอ',$address1[0]->amphur_name));
		$pdf->Text(25, 142.3, $address1[0]->province_name);
		$pdf->Text(105, 142.3, $address1[0]->zip_code);
		$pdf->Text(85, 151.1, $res[0]->tel);
		$pdf->Text(13, 168.7, $address2[0]->address);
		$pdf->Text(35, 186.3, $address2[0]->district_name);
		$pdf->Text(125, 186.3, $this->RemoveText('อำเภอ',$address2[0]->amphur_name));
		$pdf->Text(25, 195.2, $address2[0]->province_name);
		$pdf->Text(105, 195.3, $address2[0]->zip_code);
		$pdf->Text(85, 204, $res[0]->tel);

		if($address3 != null){
			$pdf->Text(75, 212.8, $res[0]->office_name);
			$pdf->Text(13, 221.6, $address3[0]->address);
			$pdf->Text(35, 239.2, $address3[0]->district_name);
			$pdf->Text(130, 239.2, $this->RemoveText('อำเภอ',$address3[0]->amphur_name));
			$pdf->Text(25, 248.1, $address3[0]->province_name);
			$pdf->Text(130, 248.1, $address3[0]->zip_code);
			$pdf->Text(30, 256.8, $res[0]->phone);
		}

		$pdf->Text(34, 265.7, $res[0]->career_text); 
		$pdf->Text(30, 274.6, number_format($res[0]->monthly_income)); 
		
		

		//######### Contract A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0002/DOC0002'.$DateControlDoc.'_p02.jpg');
		
		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(36, 47.6, $res[0]->product_name);
		$pdf->Text(103, 47.6, $res[0]->product_count);
		$pdf->Text(149, 47.6, $res[0]->brand_name);
		$pdf->Text(17, 56.4, $res[0]->product_version);

		$serialStr0 = '';
		$serialStr1 = '';
		foreach($subProduct as $i => $item){
			if($i == 0){
				$serialStr0 .= $item->product_sub_name.' : '.$item->serial_number;
			}else{
				$serialStr1 .= ($i == 1)?$item->product_sub_name.' : '.$item->serial_number : ', '.$item->product_sub_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(80, 56.4, $serialStr0);
		$pdf->Text(16, 65.2, $serialStr1);

		$pdf->Text(52, 73.9, date_format(date_create($res[0]->contract_date),"d/m/Y"));
		$pdf->Text(133, 73.9, date_format(date_create($res[0]->payment_start_date),"d/m/Y"));
		$pdf->Text(38, 82.8, $res[0]->rental_period);
		$pdf->Text(45, 91.8, number_format($res[0]->monthly_rent));
		$pdf->Text(40, 100.6, $res[0]->payment_due);
		$pdf->Text(95, 127.1, ($res[0]->advance_payment == 0 && $res[0]->advance_payment == null)? '-': number_format($res[0]->advance_payment));
		$pdf->Text(55, 135, '-');
		$pdf->Text(63, 162.2, ($res[0]->installation_fee == 0 && $res[0]->installation_fee == null)? '-': number_format($res[0]->installation_fee));

		$inst = $installationLocation[0];
		$addInstall = $inst->address.'  แขวง/ตำบล '.$inst->district_name.'    เขต/อำเภอ '.$this->RemoveText('อำเภอ',$inst->amphur_name).'    จังหวัด'.$inst->province_name.'    รหัสไปรษณีย์  '.$inst->zip_code;
		$pdf->Text(13, 200.3, $addInstall);
		$pdf->Text(36, 248.4, $res[0]->firstname.' '.$res[0]->lastname);
		if($Supporter != null){
			$pdf->Text(36, 271.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);
		}
	}

	public function Agreement($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0003/DOC0003'.$DateControlDoc.'_p01.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();


		//######### Page 2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0003/DOC0003'.$DateControlDoc.'_p02.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}
	public function AgreementCoppy($pdf, $DateControlDoc, $res)
	{
		//######### Page 1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0003/DOC0003'.$DateControlDoc.'_p01.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();


		//######### Page 2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0003/DOC0003'.$DateControlDoc.'_p02.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(29, 235.1, $res[0]->firstname.' '.$res[0]->lastname);
	}

	public function Receipt($pdf, $res, $installationLocation, $DateControlDoc, $subProduct, $agen)
	{
		//เลขบัตรประจำตัวประชาชน '.textFormat($res[0]->idcard,'','-'));

		//######### Contract A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0004/DOC0004'.$DateControlDoc.'.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);

		$pdf->Text(155, 61.3, $res[0]->temp_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_sub_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_sub_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}

	public function ReceiptCoppy($pdf, $res, $installationLocation, $DateControlDoc , $subProduct, $agen)
	{
		//เลขบัตรประจำตัวประชาชน '.textFormat($res[0]->idcard,'','-'));

		//######### Contract A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0004/DOC0004'.$DateControlDoc.'-01.jpg');
		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		
		$pdf->Text(155, 61.3, $res[0]->temp_code);
		$pdf->Text(57, 75.8, $res[0]->cate_name);
		$pdf->Text(109, 75.8, $res[0]->brand_name);
		$pdf->Text(153, 75.8, $res[0]->product_version);

		$serialStr = '';
		foreach($subProduct as $i => $item){
			
			if($i == 0){
				$serialStr .= $item->product_sub_name.' : '.$item->serial_number;
			}else{
				$serialStr .= ', '.$item->product_sub_name.' : '.$item->serial_number;
			}
		}
		$pdf->Text(67, 84.5, $serialStr);
		
		$cusType = '';
		if($res[0]->type == 'person'){
			$cusType = 'บุคคล';
		}else{
			$cusType = 'นิติบุคคล';
			$pdf->Text(70, 137.5, $res[0]->firstname);
		}
		$pdf->Text(60, 102.3, $cusType);

		$pdf->Text(65, 111, $res[0]->firstname.' '.$res[0]->lastname);

		$inst = $installationLocation[0];
		$pdf->Text(15, 146.3, $inst->address);
		$pdf->Text(34, 155.1, $inst->district_name);
		$pdf->Text(125, 155.1, $this->RemoveText('อำเภอ',$inst->amphur_name));
		$pdf->Text(34, 164, $inst->province_name);
		$pdf->Text(125, 164, $inst->zip_code);
		$pdf->Text(27, 172.8, $res[0]->phone);
		$pdf->Text(86, 172.8, $res[0]->tel);

		$pdf->Text(27, 236.3, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(123, 236.3, $agen[0]->name.' '.$agen[0]->sname);
	}

	public function Supporter($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0009/DOC0009'.$DateControlDoc.'.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();

		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->temp_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}
	public function SupporterCoppy($pdf, $DateControlDoc, $res, $Supporter)
	{
		//######### Page 1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0009/DOC0009'.$DateControlDoc.'.jpg');

		// -- set new background ---
		// get the current page break margin
		$bMargin = $pdf->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $pdf->getAutoPageBreak();
		// disable auto-page-break
		$pdf->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'image_demo.jpg';
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$pdf->setPageMark();

		$pdf->SetFont('thsarabun', '', 15, '', true);
		$pdf->Text(70, 62.5, $res[0]->temp_code);
		$pdf->Text(90, 71.5, $res[0]->firstname.' '.$res[0]->lastname);
		$pdf->Text(25, 80.3, $Supporter[0]->fname.' '.$Supporter[0]->lname);

		$bDate = date_create($Supporter[0]->bdate);
		$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
		$pdf->Text(175, 80.3, $age);
		$pdf->Text(55, 89, $this->textFormat($Supporter[0]->idcard,'','-'));
		$pdf->Text(20, 98, $Supporter[0]->addr);
		$pdf->Text(30, 106.8, $Supporter[0]->district_name);
		$pdf->Text(125, 106.8, $this->RemoveText('อำเภอ',$Supporter[0]->amphur_name));
		$pdf->Text(30, 115.7, $Supporter[0]->province_name);
		$pdf->Text(26, 235.5, $Supporter[0]->fname.' '.$Supporter[0]->lname);
	}

	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	 }
	 public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			$temp = explode($text, $wording);
			$string = $temp[1];
		}
		return $string ;
	 }

















































	
	public function getBycustomerCode()
	{
		$id = $this->input->post('customer_code');
		$data = $this->Temp_Model->getInhabited($id);
		echo json_encode($data);
	}

	public function getIhbByCus()
	{
		$postData = $this->input->post();
		$data = $this->Temp_Model->getIhbByCus($postData);
		echo json_encode($data);
	}

	#### Detail ####
	public function detail($id = null)
	{
		//$data['resStatus'] = $this->Status_Model->temp_select();
		$data['res'] = $this->Temp_Model->selectOneDeatil($id);
		//$data['resInst'] = $this->Temp_Model->getInstOne($id);
		$data['supporter'] = $this->Temp_Model->selectSupporter($id);
		
		$data['address'] = $this->Temp_Model->detailAddress($data['res'][0]->customer_code, 'ที่อยู่ปัจจุบัน');
		$data['installationLocation'] = $this->Temp_Model->detailInstallationLocation($data['res'][0]->installationlocation);

		//$data['resCustomer'] =  $this->Temp_Model->customer_selectOne($id);
		//$data['resProduct'] =  $this->Product_Model->product_set();

        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'temp';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/temp_detail',$data);
        $this->load->view('admin/footer');
	}

	

	public function getSparesByInstallId()
	{
		$id = $this->input->post('product_install_id');
		$data = $this->Temp_Model->getSparesByInstallId($id);
		echo json_encode($data);
	}

	function DateThai($strDate)
	{
		//$strYear = date("Y",strtotime($strDate))+543;
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		//$strHour= date("H",strtotime($strDate));
		//$strMinute= date("i",strtotime($strDate));
		//$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
	}
	
	/////////////////////////////// External DB ////////////////////////////////////////////////
	public function ajaxTechn()
	{
		$technId = $this->input->post('val');
		$query = "select AgentCode,AgentName,AgentSurName from Agent where (AgentCode ='".$technId."')";
		$result = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
		
		if(sqlsrv_num_rows($result) > 0){
		
		  while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
			$ress = $row;
		  }
		}else{
		  $ress = false;
		}
		echo json_encode($ress);
	}

	private function connectSeminarDB(){
		$serverName = $this->db_config['production']['servername']; //serverName\instanceName
		$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
		$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);
		
		if(!$this->seminar_conn){
			echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
		}
	}
	public function closeConnectSeminarDB(){
		sqlsrv_close( $this->seminar_conn );
	}
	///////////////////////////////////////////////////////////////////////////////

}
