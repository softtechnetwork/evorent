<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class WebProduct extends CI_Controller {
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductMaster_Model');
        $this->load->model('admin/ProductSet_Model');
    } 

    ############  List  #############
	public function index(){
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webProduct';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webproduct');
        $this->load->view('admin/footer');
	}
    public function product_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        //$product_id = $this->input->post('name');
        // $desc = $this->input->post('desc');
        $Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type
        , web_product.title , web_product.display_status
        FROM product_set p 
        LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id
        LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id
        LEFT JOIN product_category pc ON p.product_cate = pc.cate_id
        LEFT JOIN brand b ON p.product_brand = b.brand_id
        LEFT JOIN web_product ON p.product_id = web_product.product_set_id
        ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}


    
    public function product_manage($productSet_id = null){
        $menu['mainmenu'] = 'web';
		$menu['submenu'] = 'webProduct';

        $data['productSet_id'] = $productSet_id;

        $this->load->view('admin/header',$menu);
		$this->load->view('admin/webproduct_manage',$data);
        $this->load->view('admin/footer');
	}
    public function product_get_once(){
        $curent_date = Date('Y-m-d H:i:s');
        $productSet_id = $this->input->post('productSet_id');
        
        $Query = " SELECT web_product.* FROM product_set p 
		INNER JOIN web_product ON p.product_id = web_product.product_set_id
        WHERE web_product.product_set_id = '".$productSet_id."' 
        ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}
    public function product_actions($productSet_id = null){
        $res = new stdClass();
        $curent_date = Date('Y-m-d H:i:s');
        $id = $this->input->post('id');
		$method = $this->input->post('method');
		$productSet_id = $this->input->post('productSet_id');
		$title = $this->input->post('title');
		$detail = $this->input->post('detail');
		$status = $this->input->post('status');

        $obj = array(
			'title' => $title,  
			'detail' => $detail,  
			'display_status' => $status
		); 

        if($method == 'insert'){
            $obj['product_set_id'] = $productSet_id;
            $obj['cdate'] = $curent_date;
            $this->db->insert("web_product", $obj);
        }else{
            $obj['udate'] = $curent_date;

            $this->db->set($obj); 
            $this->db->where("id", $id); 
            $this->db->where("product_set_id", $productSet_id); 
            $this->db->update("web_product", $obj); 
        }

        $res->status = true;
        $res->datas = $productSet_id;
        echo json_encode($res);
	}

}
