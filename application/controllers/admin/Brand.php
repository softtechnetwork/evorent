<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Brand extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/Brand_Model');

        
    } 

	public function index()
	{
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'brand';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/brand_list');
        $this->load->view('admin/footer');
	}
    public function create()
	{
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'brand';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/brand_create');
        $this->load->view('admin/footer');
	}
    public function edit($id = null)
	{
        $date['res'] = $this->Brand_Model->getToEdit($id);

		$menu['mainmenu'] = 'product';
		$menu['submenu'] = 'brand';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/brand_edit', $date);
        $this->load->view('admin/footer');
	}

    public function insert(){
		$idArr = [];
		$code = '';
		$res = $this->Brand_Model->getToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)$items->brand_id);
			}
			$code = sprintf('%03d',MAX($idArr)+1);
		}else{
			$code = sprintf('%03d',1);
		}
		
		$data = array( 
			'brand_id' =>   $code ,
            'brand_name' => $this->input->post('brand-name'),
            'brand_detail' => $this->input->post('brand-detail'),
            'cdate'=>date("Y-m-d H:m:s")
         );

		$this->Brand_Model->insert($data);
		
        redirect('admin/brand');
    }

    public function update(){
        $id = $this->input->post('brand-id');
		$data = array( 
            'brand_name' => $this->input->post('brand-name'),
			'brand_detail' => $this->input->post('brand-detail'),
            'udate'=>date("Y-m-d H:m:s")
         );
		$this->Brand_Model->update($data, $id);
		
        redirect('admin/brand');
    }

    public function delProductBrand(){
        $id = $this->input->post('id');
		$data = $this->Brand_Model->delet($id);
		echo json_encode($data);
    }
    public function getResBrand()
	{
		$Search = $this->input->post('Search');
		$data = $this->Brand_Model->getToList($Search);		
		echo json_encode($data);
	}
	
	public function brand_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
		$Query = "SELECT * FROM brand ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}

}
