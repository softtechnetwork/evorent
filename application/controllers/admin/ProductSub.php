<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ProductSub extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductSub_Model');
    } 

	public function index(){
        $data['product'] = $this->ProductSub_Model->productTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productSub';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/productSub_list',$data);
        $this->load->view('admin/footer');
	}

    public function create(){
        $data['product'] = $this->ProductSub_Model->productTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productSub';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/productSub_create',$data);
        $this->load->view('admin/footer');
	}

    public function getResProductSub(){
		$Search = $this->input->post('Search');
		$data = $this->ProductSub_Model->getToList($Search);		
		echo json_encode($data);
	}

    public function edit($id = null){
        $data['res'] = $this->ProductSub_Model->GetToEdit($id);
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productSub';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/productSub_edit', $data);
        $this->load->view('admin/footer');
	}

    public function insert(){
		$idArr = [];
		$code = '';
        $prefixText = 'PS';
		$res = $this->ProductSub_Model->getToGenCode();
		if($res){
			foreach($res as $items){
                $temp = explode($prefixText, $items->product_sub_id);
				array_push($idArr, (int)$temp[1]);
			}
			$code = sprintf($prefixText.'%04d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefixText.'%04d',1);
		}
		
		$data = array( 
			'product_sub_id' =>   $code ,
            'product_sub_name' => $this->input->post('productSub-name'),
            'product_sub_version' => $this->input->post('productSub-version'),
            'product_sub_ref' => $this->input->post('productSub-ref'),
            'product_sub_detail' => $this->input->post('productSub-detail'),
            'product_id' => $this->input->post('product'),
            'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
        );

		$this->ProductSub_Model->ProductSubInsert($data);
        redirect('admin/productSub');
    }

    public function update(){
        $id = $this->input->post('id');
		$data = array( 
            'product_sub_name' => $this->input->post('productSub-name'),
            'product_sub_version' => $this->input->post('productSub-version'),
            //'product_sub_ref' => $this->input->post('productSub-ref'),
            'product_sub_detail' => $this->input->post('productSub-detail'),
            'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
        );
		$this->ProductSub_Model->update($data, $id);
        redirect('admin/productSub');
    }

    public function DeleteProductSub(){
        $id = $this->input->post('id');
		$data = $this->ProductSub_Model->DeleteProductSub($id);
		echo json_encode($data);
    }

    

}
