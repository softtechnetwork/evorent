<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
    } 

	public function index(){
		$data['productCate'] = $this->Product_Model->productcateTocombo();
        $data['productBrand'] = $this->Product_Model->brandTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'product';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_list',$data);
        $this->load->view('admin/footer');
	}

    public function create(){
        $data['productCate'] = $this->Product_Model->productcateTocombo();
        $data['productBrand'] = $this->Product_Model->brandTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'product';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_create',$data);
        $this->load->view('admin/footer');
	}

    public function edit($id = null){
		$data['productCate'] = $this->Product_Model->productcateTocombo();
        $data['productBrand'] = $this->Product_Model->brandTocombo();

        $data['res'] = $this->Product_Model->GetToEdit($id);
        $data['installentType'] = $this->Product_Model->GetInstallmentTypeToEdit($id);

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'product';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_edit', $data);
        $this->load->view('admin/footer');
	}

    public function insert(){
		
		$idArr = [];
		$code = '';
        $prefixText = 'P';
		$res = $this->Product_Model->getToGenCode();
		if($res){
			foreach($res as $items){
                $temp = explode('P', $items->product_id);
				array_push($idArr, (int)$temp[1]);
			}
			$code = sprintf($prefixText.'%03d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefixText.'%03d',1);
		}
		
		
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));
		$produc_data = array( 
			'product_id' =>   $code ,
            'product_name' => $this->input->post('product-name'),
            'product_cate' => $this->input->post('product-cate'),
            'product_brand' => $this->input->post('product-brand'),
            'product_version' => $this->input->post('product-version'),
			'product_ref' => $this->input->post('product-ref'),
            'product_detail' => $this->input->post('product-detail'),
            'cdate'=> $date
        );

		$this->Product_Model->ProductInsert($produc_data);

		// ประเภท การผ่อนชำระ
        $InstallmentAmount = $this->input->post('installment-format-amount');
        $idArr = [];
		$nume = 0;
		$res = $this->Product_Model->InstallmentTypeToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)$items->installment_type_id);
			}
			$nume = MAX($idArr);
		}
        
        for($i = 1; $i <= (int)$InstallmentAmount; $i++){
            $installment_type_data = array( 
                'installment_type_id' =>   sprintf('%03d',$nume+$i) ,
                'product_id' => $code,
                'amount_installment' => $this->input->post('installment-amount'.$i),
                'pay_per_month' => $this->input->post('installment-pay'.$i),
                'cdate'=> $date
            );

			$this->Product_Model->InstallmentTypeInsert($installment_type_data);
        }
		
        redirect('admin/product');
    }

    public function update(){
        $id = $this->input->post('id');
		$produc_data = array( 
            'product_name' => $this->input->post('product-name'),
            'product_version' => $this->input->post('product-version'),
            'product_detail' => $this->input->post('product-detail'),
            'udate'=>date("Y-m-d H:m:s")
        );

		$this->Product_Model->update($produc_data, $id);
		
        redirect('admin/product');
    }

    public function DeleteProduct(){
        $id = $this->input->post('id');
		$data = $this->Product_Model->DeleteProduct($id);
		echo json_encode($data);
    }

    public function getResProduct(){
		$Search = $this->input->post('Search');
		$data = $this->Product_Model->getToList($Search);		
		echo json_encode($data);
	}

}
