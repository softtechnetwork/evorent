<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ProductCategory extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model  
        $this->load->model('admin/ProductCategory_Model');
        
    } 

	public function index()
	{
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/pcategory_list');
        $this->load->view('admin/footer');
	}
    public function create()
	{
        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/pcategory_create');
        $this->load->view('admin/footer');
	}
    public function edit($id = null)
	{
        $date['res'] = $this->ProductCategory_Model->getToEdit($id);

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productCategory';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/pcategory_edit', $date);
        $this->load->view('admin/footer');
	}

    public function insert(){
		$idArr = [];
		$code = '';
		$res = $this->ProductCategory_Model->getToGenCode();
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)$items->cate_id);
			}
			$code = sprintf('%04d',MAX($idArr)+1);
		}else{
			$code = sprintf('%04d',1);
		}
		
		$data = array( 
			'cate_id' =>   $code ,
            'cate_name' => $this->input->post('category-name'),
            'cate_detail' => $this->input->post('category-detail'),
            'created'=>date("Y-m-d H:m:s")
         );
		$this->ProductCategory_Model->insert($data);
		
        redirect('admin/productCategory');
    }

    public function update(){
        $id = $this->input->post('category-id');
		$data = array( 
            'cate_name' => $this->input->post('category-name'),
            'cate_detail' => $this->input->post('category-detail'),
            'updated'=>date("Y-m-d H:m:s")
         );
		$this->ProductCategory_Model->update($data, $id);
		
        redirect('admin/productCategory');
    }

    public function delProductCategory(){
        $id = $this->input->post('id');
		$data = $this->ProductCategory_Model->delet($id);
		echo json_encode($data);
    }
    


    public function getResCategory()
	{
		$Search = $this->input->post('Search');
		$data = $this->ProductCategory_Model->getToList($Search);		
		echo json_encode($data);
	}

      
	public function ProductCategory_get(){
		$curent_date = Date('Y-m-d H:i:s');
		$res = new stdClass();
		$Query = "SELECT * FROM product_category ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	  }
}
