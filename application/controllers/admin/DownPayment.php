<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class DownPayment extends Backend_controller {

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        
    } 

    public function down_payment(){

        $downpayment = $this->input->post('downPayment');
		$product_install_id = $this->input->post('product_install_id');
		$product_count = $this->input->post('product_count');
		
		# update product promotion if exist
		$product_hirepurchase_interest = $this->db->select('*')->from('product_hirepurchase_interest')
		->where('product_install_id' , $product_install_id)->get();
		
		if($product_hirepurchase_interest->num_rows() > 0){
			$product_hirepurchase_arr = $product_hirepurchase_interest->result_array();
			foreach($product_hirepurchase_arr as $pk => $pv){
				$formula_id = $pv['formula_id'];
				$formula = $this->formula($formula_id);
				$formula_calcurate = json_decode($formula[key($formula)]->formula);
				$start_payment = isset($pv['term_of_payment_start']) ? intval($pv['term_of_payment_start']) : 1;
				$end_payment = intval($pv['term_of_payment_end']);
				$group_id = $pk;
				$interest = $pv['interest'];
				$holdonpayment = array();
				$save_array = array();

				$help_pay_installments_status = isset($pv['help_pay_installments_status'])? $pv['help_pay_installments_status'] : 0;
				if(!empty($pv["holdonpayment"])){
					$holdonpayment_obj = json_decode($pv['holdonpayment']);
					foreach($holdonpayment_obj as $hk => $hv){
						$holdonpayment[$pk][]= $hv;
					}
				}
			}
			$save_array = $this->prepareBuildSaveArray($formula_calcurate,$product_install_id,$holdonpayment , $group_id,$start_payment,$end_payment,$interest ,$downpayment,$product_count);
			echo json_encode($save_array);
		}
        
	  }

	  public function down_payments(){
        $downpayment = $this->input->post('downPayment');
		$product_install_id = $this->input->post('product_install_id');
		$product_count = $this->input->post('product_count');
		
		# update product promotion if exist
		$product_hirepurchase_interest = $this->db->select('*')
		->from('product_hirepurchase_interest')
		->where('product_install_id' , $product_install_id)
		->order_by('term_of_payment_end','desc')
		->get();
		
		$arr = [];
		if($product_hirepurchase_interest->num_rows() > 0){
			$product_hirepurchase_arr = $product_hirepurchase_interest->result_array();
			foreach($product_hirepurchase_arr as $pk => $pv){
				$arr[intval($pv['term_of_payment_end'])] = $pv['installment_amount_per_installment'] ;
			}
			echo json_encode($arr);
		}
	  }
}