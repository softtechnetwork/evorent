<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Report extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

		//เรียกใช้งาน library
		//$this->load->library('mpdf');
		//$this->load->library('M_pdf');
		// $this->load->library('pdf');
		//$this->load->library('excel');

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
        $this->load->model('admin/StatusCategory_Model');
        $this->load->model('admin/Service_Model');
        $this->load->model('admin/Report_Model');
    } 

	public function index()
	{
		$data['getCustomer'] = $this->Report_Model->getCustomer();
        $data['getTemp'] = $this->Report_Model->getTemp();
        $data['getStatus'] = $this->Status_Model->getStatus('210603');		
        
        $menu['mainmenu'] = 'report';
		$menu['submenu'] = 'overdue';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/report_list',$data);
        $this->load->view('admin/footer');
	}
	public function getInstallment()
	{
		$searchArray = $this->input->post('searchArray');
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Report_Model->select($searchArray, $itemPerPage, $itemStt, $itemEnd);		
		echo json_encode($data);
	}

	public function getInstallmentAll()
	{
        $searchArray = $this->input->post('searchArray');
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Report_Model->selectAllItems($searchArray, $itemPerPage, $itemStt, $itemEnd);
		echo json_encode($data);
	}

	public function getToExport()
	{
		$searchArray = $this->input->post('searchArray');
		$data = $this->Report_Model->getToExport($searchArray);		
		echo json_encode($data);
	}

    public function exportToPDF()
	{
		$data  = json_decode($this->input->post('expPdf'));
		$html = '<table cellspacing="0" cellpadding="4" border=".01" style="font-size: 10pt;">
					<tr style="border-bottom: 1px solid; background-color: #00000033;margin: 95px !important;">
						<th style="width:5%;">ลำดับ</th>
						<th style="width:13%;">รหัสลูกค้า </th>
						<th style="width:16%;">ลูกค้า </th>
						<th style="width:15%;">เลขที่สัญญา</th>
						<th style="width:8%;">วันที่ต้องชำระ</th>
						<th style="width:7%;">สถานะ</th>
						<th style="width:8%;">วันที่ชำระ </th>
						<th style="width:8%;">จำนวนที่ชำระ</th>
						<th style="width:10%;">สินค้า </th>
						<th style="width:10%;">หมายเหตุ </th>
					</tr>';
			
				$num = 1;
				foreach($data as $tem){
					$installment_date = '';
					(!empty($tem->installment_date))?$installment_date = date_format(date_create($tem->installment_date),"Y-m-d"):$installment_date = '';

					$payment_duedate = '';
					(!empty($tem->payment_duedate))?$payment_duedate = date_format(date_create($tem->payment_duedate),"Y-m-d"):$payment_duedate = '';

					$html .= '<tr style="border-bottom: 1px solid;">
						<td>'.$num.'</td>
						<td>'.$tem->customer_code.' </td>
						<td>'.$tem->firstname.' '.$tem->lastname.'</td>
						<td>'.$tem->temp_code.'</td>
						<td>'.$payment_duedate.'</td>
						<td>'.$tem->label.'</td>
						<td>'.$installment_date.'</td>
						<td>'.$tem->payment_amount.'</td>
						<td>'.$tem->product_name.'</td>
						<td>'.$tem->remark.'</td>
					</tr>';
					$num++;
				}
		$html .= '</table>';

		$this->load->library('Pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		 
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('THSarabun', '', 13, '', true);

		//$pdf->SetMargins(left, TOP, RIGHT);
		$pdf->SetMargins(5, 15, 5, true);
		
		$pdf->AddPage();
		
		// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 0, 0, true, '', true);
		$file_name = 'InstallmentReport.pdf';
		$pdf->Output($file_name, 'I');
	}
	public function exportToExcel()
	{
		$data  = json_decode($this->input->post('expExcell'));
		try {
			// เรียนกใช้ PHPExcel  
			$objPHPExcel = new PHPExcel();   
			// เราสามารถเรียกใช้เป็น  $this->excel แทนก็ได้

			// กำหนดค่าต่างๆ ของเอกสาร excel
			$objPHPExcel->getProperties()
			->setCreator("Ninenik.com")  
			->setLastModifiedBy("Ninenik.com")  
			->setTitle("PHPExcel Test Document")  
			->setSubject("PHPExcel Test Document")  
			->setDescription("Test document for PHPExcel, generated using PHP classes.")  
			->setKeywords("office PHPExcel php")  
			->setCategory("Test result file");      
		 
			// กำหนดชื่อให้กับ worksheet ที่ใช้งาน  
			$objPHPExcel->getActiveSheet()->setTitle('Product Report');  
			   
			// กำหนด worksheet ที่ต้องการให้เปิดมาแล้วแสดง ค่าจะเริ่มจาก 0 , 1 , 2 , ......  
			$objPHPExcel->setActiveSheetIndex(0);        
										  
			// การจัดรูปแบบของ cell  
			$objPHPExcel->getDefaultStyle()  
						->getAlignment()  
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)  
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
						//HORIZONTAL_CENTER //VERTICAL_CENTER               
									 
			// จัดความกว้างของคอลัมน์
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);     
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);                                              
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);     
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);         
			 
			// กำหนดหัวข้อให้กับแถวแรก
			$objPHPExcel->setActiveSheetIndex(0)  
						->setCellValue('A1', 'รหัสลูกค้า')    
						->setCellValue('B1', 'ลูกค้า') 
						->setCellValue('C1', 'เลขที่สัญญา')
						->setCellValue('D1', 'วันที่ครบกำหนดชำระ')
						->setCellValue('E1', 'สถานะ')
						->setCellValue('F1', 'วันที่ชำระ')
						->setCellValue('G1', 'จำนวนที่ชำระ')
						->setCellValue('H1', 'สินค้า')
						->setCellValue('I1', 'หมายเหตุ');
						//->setCellValue('D1', 'เลขที่สัญญา');

			$start_row=2; 
			if($data > 0){
				$i =0;
				//$result_array = $masterproduct_serialnumber->result_array();
				foreach($data as $item){
					
					$objPHPExcel->getActiveSheet()
						->getStyle('C'.$start_row)
						->getAlignment()  
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 

					if(isset($item)){          
						$no = $i + 1;

						$installment_date = '';
						if(!empty($item->installment_date)){$installment_date =date('Y-m-d',strtotime($item->installment_date)); }

						$objPHPExcel->setActiveSheetIndex(0)  
									->setCellValue('A'.$start_row, $item->customer_code )  
									->setCellValue('B'.$start_row, $item->firstname." ".$item->lastname)  
									->setCellValue('C'.$start_row, $item->temp_code)
									->setCellValue('D'.$start_row, $item->payment_duedate) 
									->setCellValue('E'.$start_row, $item->label) 
									->setCellValue('F'.$start_row, $installment_date) 
									->setCellValue('G'.$start_row, $item->payment_amount) 
									->setCellValue('H'.$start_row, $item->product_name) 
									->setCellValue('I'.$start_row, $item->remark);  
									//->setCellValue('D'.$start_row, $item->temp_code);
					}
					
					++ $i;
					++ $start_row;
					//exit();
				}
				
				// กำหนดรูปแบบของไฟล์ที่ต้องการเขียนว่าเป็นไฟล์ excel แบบไหน ในที่นี้เป้นนามสกุล xlsx  ใช้คำว่า Excel2007
				// แต่หากต้องการกำหนดเป็นไฟล์ xls ใช้กับโปรแกรม excel รุ่นเก่าๆ ได้ ให้กำหนดเป็น  Excel5
				ob_start();
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  // Excel2007 (xlsx) หรือ Excel5 (xls) 
				
				$filename=date("dmYHi").'.xlsx'; //  กำหนดชือ่ไฟล์ นามสกุล xls หรือ xlsx
				// บังคับให้ทำการดาวน์ดหลดไฟล์
				header('Content-Type: application/vnd.ms-excel'); //mime type
				header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
				ob_end_clean();     
				$objWriter->save('php://output'); // ดาวน์โหลดไฟล์รายงาน
				die();
				//die($objWriter);

			}else{
				// status that return to frontend
				$status = false;
				// error message handle
				$message = "ไม่พบข้อมูล";
			}
            
        } catch (Exception $e) {
			// status that return to frontend
			$status = false;
            // error message handle
			$message = $e->getMessage();
        }
	}


	################  Overdue  ######################
	public function overdue()
	{
		$data['getCustomer'] = $this->Report_Model->getCustomer();
        $data['getTemp'] = $this->Report_Model->getTemp();
        $data['getStatus'] = $this->Status_Model->getStatus('210603');		
        
        $menu['mainmenu'] = 'report';
		$menu['submenu'] = 'overdue';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/report_overdue_list',$data);
        $this->load->view('admin/footer');
	}

	public function getcontractBycustomer()
	{
		$customer_code = $this->input->post('customer_code');
		$condition = ( !empty($customer_code))? " WHERE customer_code = '".$customer_code."'" : $customer_code;
		
		$Query = "SELECT contract_code FROM contract ".$condition;
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}
	public function getOverDueInstallment()
	{
		$search = $this->input->post();
		$datas = $this->getoverdue_datas($search);
		echo json_encode($datas);
	}
	public function getoverdue_datas($search, $stp = null,  $enp = null)
	{
		//ini_set('memory_limit','524288');  
		// ini_set('sqlsrv.ClientBufferMaxKBSize','524288'); 
		//  ini_set('pdo_sqlsrv.client_buffer_max_kb_size','524288');
		//### Condition ###//
		$where = "";
		$where_arr = array();
		if(!empty($search['customer_code'])){ array_push($where_arr, "installment.customer_code = '".$search['customer_code']."' ");}
		if(!empty($search['contract_code'])){ array_push($where_arr, "installment.contract_code = '".$search['contract_code']."' ");}
		if($search['status_code'] != ''){ array_push($where_arr, "installment.status_code = '".$search['status_code']."' ");}

		if( !empty($search['overdue']) == 'x'){array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) > CONVERT(date,installment.extend_duedate) ))" );}
		if( !empty($search['overdue']) == 'xx'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) )");}
		if( !empty($search['overdue']) == 'i'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )");}
		if( !empty($search['overdue']) == 'ii'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )");}
		if( !empty($search['overdue']) == 'ii'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )");}
		if( !empty($search['overdue']) == 'iv'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )");}
		if( !empty($search['overdue']) == 'v'){ array_push($where_arr,"(installment.status_code = 0 and (  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  ) )");}
				
		if( !empty($search['stduedate']) && !empty($search['enduedate'])){
            array_push($where_arr, "installment.payment_duedate BETWEEN convert(date,N'".$search['stduedate']."') AND convert(date,N'".$search['enduedate']."') ");
        }
		if( !empty($search['stinstallmentdate']) && !empty($search['endinstallmentdate'])){
            array_push($where_arr, "installment.installment_date BETWEEN convert(date,N'".$search['stinstallmentdate']."') AND convert(date,N'".$search['endinstallmentdate']."') ");
        }

		if(count($where_arr) > 0){
			$where = "  WHERE st.stautus_category = N'210603' and ".implode(' and ', $where_arr);
		}
		
		//### Query ###//
		$Query = "SELECT ROW_NUMBER() OVER ( ORDER BY installment.contract_code, installment.period ) AS RowNum, installment.*, 
		CASE 
		WHEN  installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  )
		THEN  'เกินกำหนดชำระมาแล้ว '+ CAST( DATEDIFF(DAY,  CONVERT(date,dateadd(year,-543,installment.extend_duedate)),   CONVERT(date,GETDATE()) )AS nvarchar)+' วัน' 
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 5 วันจะครบกำหนกชำระ'
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 4 วันจะครบกำหนกชำระ' 
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 3 วันจะครบกำหนกชำระ'
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  N'อีก 2 วันจะครบกำหนกชำระ' 
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)  =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  ) THEN  N'อีก 1 วันจะครบกำหนกชำระ'
		WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  ) THEN  N'ครบกำหนดชำระแล้ว' 
		ELSE '' END AS overdue, 
		
		CASE  
		WHEN  installment.status_code = 0 and (  CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,installment.extend_duedate))  ) THEN 10 
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+5,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  5
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+4,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  4
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+3,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  3  
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+2,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  2  
		WHEN  installment.status_code = 0 and(  CONVERT(date,installment.payment_duedate)   =  dateadd(DAY,+1,CONVERT(date,dateadd(year,+543,GETDATE())))  )THEN  1 
		WHEN  installment.status_code = 0 and(  CONVERT(date,dateadd(year,+543,GETDATE())) between CONVERT(date,installment.payment_duedate) and CONVERT(date,installment.extend_duedate)  )THEN  0 
		ELSE 1000 END AS overdue_code, 
		
		c.firstname, c.lastname, c.tel, c.email, c.idcard, c.addr,  product.product_name, brand.brand_name,  
		st.status_code as statusCode, st.stautus_category, st.label, st.detail, st.color, st.background_color,
		contractst.label as contract_status, contractst.stautus_category as contract_stautus_category,  contractst.color as contract_color, contractst.background_color as contract_background_color
		FROM contract_installment installment 
		INNER JOIN contract t ON installment.contract_code = t.contract_code
		INNER JOIN customer c ON installment.customer_code = c.customer_code 
		LEFT JOIN product_set product  ON t.product_id = product.product_id 
		LEFT JOIN brand  ON t.product_brand = brand.brand_id
		LEFT JOIN status st  ON installment.status_code = st.status_code
		LEFT JOIN status contractst  ON t.status = contractst.id";
		$Query .= $where;
		if(!empty($stp) && !empty($enp)){
			$Query .= " And ((installment.period >= '".$stp."') and(installment.period <= '".$enp."'))";
		}
        $Res = $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function getOverDueToExport()
	{
		$search = $this->input->post();
		$data = $this->getoverdue_datas($search);		
		echo json_encode($data);
	}
    public function exportOverDueToPDF()
	{
		$this->load->library('Pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('THSarabun', '', 13, '', true);
		
		$pdf->SetMargins(17, 15, 0, true);//$pdf->SetMargins(left, TOP, RIGHT);
		$search = $this->input->post();
		$datas = $this->getoverdue_datas($search);
		$rowsPpages = 30;
		$all_page = ceil(count($datas)/$rowsPpages);
		
		for ($i=1; $i <= $all_page ; $i++) { 

			$stp = (($i*$rowsPpages)+1)-$rowsPpages;  
			$enp = $i*$rowsPpages;
			$res = $this->getoverdue_datas($search, $stp, $enp);
			
			//$pdf->SetMargins(10, 20, 10, true);
			$header = '
			<div style="text-align: center;">
				<label style="font-size: 15px;"><strong>ตารางการผ่อนชำระ เลขที่สัญญา '.$datas[0]->contract_code.'</strong></label>
				<div style=" line-height: 10%;"></div>
				<label> '.$datas[0]->firstname.' '.$datas[0]->lastname.' ( '.$datas[0]->product_name.' )</label>
			</div>
			<div style=" line-height: 5%;"></div>
			<table cellspacing="0" cellpadding="4" border=".01" top="100" style="margin-top: 10rem; font-size: 10px; width:100%;">
				<tr style="border-bottom: 1px solid; background-color: #4B5F71; color: #ECF0F1; margin: 95px !important;">
					<th style="width:5%;text-align: center;" >งวดที่</th>
					<!--<th style="text-align: center;">รหัสลูกค้า </th>-->
					<th style="text-align: center;">วันที่ต้องชำระ</th>
					<th style="text-align: center;">วันที่สิ้นสุดชำระ</th>
					<th >สถานะ</th>
					<!--<th >การค้างชำระ</th>-->
					<th style="text-align: center;">วันที่ชำระ </th>
					<th >จำนวนที่ชำระ</th>
				</tr>';
			
			$tr = '';
			$footer = '</table>';
			foreach($res as $k =>$tem){
				// Drawing table tr
				$installment_date = (!empty($tem->installment_date))?  date_format(date_create($tem->installment_date),"d/m/Y"):  '';
				$payment_duedate =  (!empty($tem->payment_duedate))?  date_format(date_create($tem->payment_duedate),"d/m/Y"): '';
				$extend_duedate =  (!empty($tem->extend_duedate))?  date_format(date_create($tem->extend_duedate),"d/m/Y"): '';

				$payment_amount = ($tem->payment_amount !='')?number_format($tem->payment_amount):'';
				$tr .= '<tr style="border-bottom: 1px solid;">
				<td style="text-align: center;">'.$tem->period.'</td>
				<!--<td style="text-align: center;">'.$tem->customer_code.' </td>-->
				<td style="text-align: center;">'.$payment_duedate.'</td>
				<td style="text-align: center;">'.$extend_duedate.'</td>
				<td >'.$tem->label.'</td>
				<!--<td>'.$tem->overdue.'</td>-->
				<td style="text-align: center;">'.$installment_date.'</td>
				
				<td>'.$payment_amount.'</td>
				</tr>';
			}
			$html = $header.$tr.$footer;
			$pdf->AddPage();
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 0, 0, true, '', true);// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
		}
		$pdf->lastPage();
		$files_name = 'เลขที่สัญญา '.$datas[0]->contract_code.'.pdf';
		$pdf->Output($files_name, 'I');
	}
	public function exportOverDueToExcel()
	{
		//$data  = json_decode($this->input->post('expExcell'));
		$search = $this->input->post();
		$datas = $this->getoverdue_datas($search);
		try {
			// เรียนกใช้ PHPExcel  
			$objPHPExcel = new PHPExcel();   
			// เราสามารถเรียกใช้เป็น  $this->excel แทนก็ได้

			// กำหนดค่าต่างๆ ของเอกสาร excel
			$objPHPExcel->getProperties()
			->setCreator("Ninenik.com")  
			->setLastModifiedBy("Ninenik.com")  
			->setTitle("PHPExcel Test Document")  
			->setSubject("PHPExcel Test Document")  
			->setDescription("Test document for PHPExcel, generated using PHP classes.")  
			->setKeywords("office PHPExcel php")  
			->setCategory("Test result file");      
		 
			// กำหนดชื่อให้กับ worksheet ที่ใช้งาน  
			$objPHPExcel->getActiveSheet()->setTitle('Product Report');  
			   
			// กำหนด worksheet ที่ต้องการให้เปิดมาแล้วแสดง ค่าจะเริ่มจาก 0 , 1 , 2 , ......  
			$objPHPExcel->setActiveSheetIndex(0);        
										  
			// การจัดรูปแบบของ cell  
			$objPHPExcel->getDefaultStyle()  
						->getAlignment()  
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)  
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
						//HORIZONTAL_CENTER //VERTICAL_CENTER               
									 
			// จัดความกว้างของคอลัมน์
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);     
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);                                              
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);       
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);     
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);         
			 
			// กำหนดหัวข้อให้กับแถวแรก
			$objPHPExcel->setActiveSheetIndex(0)  
						->setCellValue('A1', 'รหัสลูกค้า')    
						->setCellValue('B1', 'ลูกค้า') 
						->setCellValue('C1', 'เลขที่สัญญา')
						->setCellValue('D1', 'วันที่ครบกำหนดชำระ')
						->setCellValue('E1', 'สถานะ')
						->setCellValue('F1', 'การค้างชำระ')
						->setCellValue('G1', 'วันที่ชำระ')
						->setCellValue('H1', 'จำนวนที่ชำระ')
						->setCellValue('I1', 'สินค้า');
						//->setCellValue('J1', 'หมายเหตุ');
						//->setCellValue('D1', 'เลขที่สัญญา');

			$start_row=2; 
			if($datas > 0){
				$i =0;
				//$result_array = $masterproduct_serialnumber->result_array();
				foreach($data as $item){
					
					$objPHPExcel->getActiveSheet()
						->getStyle('C'.$start_row)
						->getAlignment()  
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 

					if(isset($item)){          
						$no = $i + 1;

						$installment_date = '';
						if(!empty($item->installment_date)){$installment_date =date('Y-m-d',strtotime($item->installment_date)); }

						$objPHPExcel->setActiveSheetIndex(0)  
									->setCellValue('A'.$start_row, $item->customer_code )  
									->setCellValue('B'.$start_row, $item->firstname." ".$item->lastname)  
									->setCellValue('C'.$start_row, $item->contract_code)
									->setCellValue('D'.$start_row, $item->payment_duedate) 
									->setCellValue('E'.$start_row, $item->label) 
									->setCellValue('F'.$start_row, $item->overdue) 
									->setCellValue('G'.$start_row, $installment_date) 
									->setCellValue('H'.$start_row, $item->payment_amount) 
									->setCellValue('I'.$start_row, $item->product_name);
									//->setCellValue('J'.$start_row, $item->remark);  
									//->setCellValue('D'.$start_row, $item->temp_code);
					}
					
					++ $i;
					++ $start_row;
					//exit();
				}
				
				// กำหนดรูปแบบของไฟล์ที่ต้องการเขียนว่าเป็นไฟล์ excel แบบไหน ในที่นี้เป้นนามสกุล xlsx  ใช้คำว่า Excel2007
				// แต่หากต้องการกำหนดเป็นไฟล์ xls ใช้กับโปรแกรม excel รุ่นเก่าๆ ได้ ให้กำหนดเป็น  Excel5
				ob_start();
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  // Excel2007 (xlsx) หรือ Excel5 (xls) 
				
				$filename=date("dmYHi").'.xlsx'; //  กำหนดชือ่ไฟล์ นามสกุล xls หรือ xlsx
				// บังคับให้ทำการดาวน์ดหลดไฟล์
				header('Content-Type: application/vnd.ms-excel'); //mime type
				header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
				ob_end_clean();     
				$objWriter->save('php://output'); // ดาวน์โหลดไฟล์รายงาน
				die();
				//die($objWriter);

			}else{
				// status that return to frontend
				$status = false;
				// error message handle
				$message = "ไม่พบข้อมูล";
			}
            
        } catch (Exception $e) {
			// status that return to frontend
			$status = false;
            // error message handle
			$message = $e->getMessage();
        }
	}

    /*public function getTempByCustomer()
	{
        $customer = $this->input->post('customer');
		
		$data = $this->Report_Model->selectTempByCustomer($customer);
		echo json_encode($data);
	}*/


	################  Contract  ######################
	public function contract()
	{
		$data['getCustomer'] = $this->Report_Model->getCustomer();
        $data['getTemp'] = $this->Report_Model->getTemp();
        //$data['getStatus'] = $this->Status_Model->getStatus('210603');		
        
        $menu['mainmenu'] = 'report';
		$menu['submenu'] = 'contract';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/report_contract',$data);
        $this->load->view('admin/footer');
	}
	public function getContract()
	{
		$search = $this->input->post();
		$datas = $this->getContract_datas($search);
		echo json_encode($datas);
	}
	public function getContract_datas($search, $stp = null,  $enp = null)
	{
		$where = "";
		$where_arr = array();
		if(!empty($search['customer_code'])){ array_push($where_arr, "customer.customer_code = '".$search['customer_code']."' ");}
		if(!empty($search['contract_code'])){ array_push($where_arr, "c.contract_code = '".$search['contract_code']."' ");}
		if(!empty($search['stcontractdate'])){ array_push($where_arr, "c.contract_date = convert(date,N'".$search['stcontractdate']."') ");}
		if(count($where_arr) > 0){
			$where = " WHERE ".implode(' and ', $where_arr);
		}
		
		$Query = " SELECT * FROM ( 
			SELECT ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum,c.*, 
			customer.firstname, customer.lastname, customer.tel, customer.email, customer.idcard, customer.addr as CusAddr, 
			product.product_name, product.product_cate as productCate, product.product_version as productVersion, product.product_detail, brand.brand_name, 
			status.label, status.color, status.background_color 
			FROM contract c
			INNER JOIN customer ON c.customer_code = customer.customer_code
			LEFT JOIN product_set product  ON c.product_id = product.product_id
			LEFT JOIN brand ON c.product_brand = brand.brand_id 
			LEFT JOIN status ON c.status = status.id ";
		$Query .= $where;
		$Query .= ' ) RES ';
		if(!empty($stp) && !empty($enp)){
			$Query .= " WHERE ((RowNum >= '".$stp."') and(RowNum <= '".$enp."'))";
		}

		$Res= $this->db->query($Query);
		$data = $Res->result();
		return $data;
	}
	public function exportContractToPDF()
	{
		$this->load->library('Pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('THSarabun', '', 13, '', true);
		
		$pdf->SetMargins(13, 10, 0, true);//$pdf->SetMargins(left, TOP, RIGHT);
		$search = $this->input->post();
		$datas = $this->getContract_datas($search);
		$rowsPpages = 30;
		$all_page = ceil(count($datas)/$rowsPpages);

		for ($i=1; $i <= $all_page ; $i++) { 

			$stp = (($i*$rowsPpages)+1)-$rowsPpages;  
			$enp = $i*$rowsPpages;
			$res = $this->getContract_datas($search, $stp, $enp);
			$header = '
			<div style="text-align: center;"><label style="font-size: 15px;"><strong>สัญญาลูกค้า</strong></label> </div>
			<div style=" line-height: 5%;"></div>

			<table cellspacing="0" cellpadding="4" border=".01" style="margin-top: 10rem; font-size: 10px; width:100%;">
				<tr style="border-bottom: 1px solid; background-color: #00000033;margin: 95px !important;">
					<th style="width:4.5%;">ลำดับ</th>
					<th style="width:8.5%;">เลขที่สัญญา </th>
					<!--<th style="width:8.5%;">รหัสลูกค้า </th>-->
					<th style="width:20%;">ลูกค้า</th>
					<th style="width:30%;">สินค้า</th>
					<!--<th style="width:7%;">ยี่ห้อ</th>-->
					<th style="width:11%;">ราคาผ่อนต่อเดือน</th>
					<th style="width:10%;">วันที่เริ่มต้นสัญญา </th>
					<th style="width:10%;">เบอร์โทรศัพท์</th>
				</tr>';
			$tr = '';
			$footer = '</table>';

			foreach($res as $k =>$tem){
				$contract_date = (!empty($tem->contract_date))? date_format(date_create($tem->contract_date),"d/m/Y"): '';

				$tr .= '<tr style="border-bottom: 1px solid;">
					<td style="text-align: center;">'.$tem->RowNum.'</td>
					<td>'.$tem->contract_code.'</td>
					<!--<td>'.$tem->customer_code.' </td>-->
					<td>'.$tem->firstname.' '.$tem->lastname.'</td>
					<td>'.$tem->product_name.'</td>
					<!--<td>'.$tem->brand_name.'</td>-->
					<td>'.number_format($tem->monthly_rent).'</td>
					<td style="text-align: center;">'.$contract_date.'</td>
					<td>'.$tem->tel.'</td>
				</tr>';
			}
			$html = $header.$tr.$footer;
			$pdf->AddPage();
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 0, 0, true, '', true);// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
		}
		$pdf->lastPage();
		$files_name = 'สัญญาลูกค้า.pdf';
		$pdf->Output($files_name, 'I');
		
	}

}
