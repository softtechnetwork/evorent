<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class ProductSet extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductMaster_Model');
        $this->load->model('admin/ProductSet_Model');
    } 

    ############  List  #############
	public function index(){
		$data['productCate'] = $this->ProductSet_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductSet_Model->brandTocombo();

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productset';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_set_list',$data);
        $this->load->view('admin/footer');
	}
    
    public function getResProductSet(){
		$Search = $this->input->post('Search');
		$data = $this->ProductSet_Model->getToList($Search);		
		echo json_encode($data);
	}


    ############  Create  #############
    public function getResProductMaster(){
		$brand = $this->input->post('brand');
        $cate = $this->input->post('cate');
        
		$data = $this->ProductSet_Model->getResProductMaster($brand, $cate);
        echo json_encode($data);
	}

    public function getResProductBrand(){
        $cate = $this->input->post('cate');
		$data = $this->ProductSet_Model->brandTocomboByProductCate($cate);
        echo json_encode($data);
	}

    public function getResContractDoc(){
		$type = $this->input->post('type');
		$data = $this->ProductSet_Model->getResContractDoc($type);		
		echo json_encode($data);
	}

    public function create(){
        $data['productCate'] = $this->ProductSet_Model->productcateTocombo();
        //$data['productBrand'] = $this->ProductSet_Model->brandTocombo();

		$data['productType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211001');
		$data['installationType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211002');
		$data['contractType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211003');

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productset';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_set_create',$data);
        $this->load->view('admin/footer');
	}

    public function insert(){
		$idArr = [];
		$code = '';
        $prefixdate = date("ym");
        $prefixText = 'PS';
        $prefix = $prefixText.$prefixdate;
		$res = $this->ProductSet_Model->getProductSetToGenCode();
		if($res){
			$code = sprintf($prefix.'%03d',(int)substr($res[0]->product_id,6,9) +1);
		}else{
			$code = sprintf($prefix.'%03d',1);
		}
		$date = date(date_format(date_create(),"Y-m-d H:i:s"));

        ####  product set ####
		$produc_data = array( 
			'product_id' =>   $code,
            'product_cate' => $this->input->post('product-cate'),
            'product_brand' => $this->input->post('product-brand'),
            'product_version' => $this->input->post('product-version'),
            'product_name' => $this->input->post('product-name'),
            'product_pricce' => $this->input->post('product-pricce'),
            'product_installation_type' => $this->input->post('installation-type'),
            'product_installation_fee' => $this->input->post('installation-fee'),
            'product_customer_type' => $this->input->post('customer-type'),
            'product_contract_type' => $this->input->post('contract-type'),
            'product_contract_doc' => $this->input->post('contract-doc'),
			//'product_ref' => $this->input->post('product-ref'),
            'product_detail' => $this->input->post('product-detail'),
            'product_cdate'=> $date,
            'product_udate'=> $date
        );
		$this->ProductSet_Model->ProductSetInsert($produc_data);

        #### sub product set ####
        $productSet_obj = json_decode($this->input->post('product-master-json'));
        $type_sub_product_set = $this->ProductSet_Model->statusTocombo('stautus_category','211004');
        $num = 1;
        foreach($productSet_obj as $items){
            //$master_p = $this->ProductSet_Model->getMasterProductById($items);
            $sub_produc_set = array(
                'product_set_id' => $produc_data['product_id'],
                'product_master_id'=> $items,
                'type' => $type_sub_product_set[0]->id,
                'cdate'=> $date,
                'udate'=> $date
            );
            $this->ProductSet_Model->SubProductSetInsert($sub_produc_set);
        }

        #### installment type ####
        $InstallmentAmount = $this->input->post('installment-format-amount');
        for($i = 1; $i <= (int)$InstallmentAmount; $i++){
            $idArr = [];
            $prefixdate = date("ym");
            $prefixText = 'IT';
            $prefix = $prefixText.$prefixdate;
            $nume = '';
            $res = $this->ProductSet_Model->InstallmentTypeToGenCode();
            if($res){
                $nume = sprintf($prefix.'%03d',(int)substr($res[0]->installment_type_id,6,9) +1);
            }else{
                $nume = sprintf($prefix.'%03d',1);
            }

            $installment_type_data = array( 
                'installment_type_id' => $nume,
                'product_id' => $code,
                'amount_installment' => (int)$this->input->post('installment-amount'.$i),
                'pay_per_month' => (int)$this->input->post('installment-pay'.$i),
                'cdate'=> $date,
                'udate'=> $date
            );
			$this->ProductSet_Model->InstallmentTypeInsert($installment_type_data);
        }
        redirect(base_url('admin/productSet'));
    }




    ############  Edit  #############
    public function edit($id = null){
		//$data['productCate'] = $this->ProductSet_Model->productcateTocombo();
        $data['productBrand'] = $this->ProductSet_Model->brandTocombo();

		$data['productType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211001');
		$data['installationType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211002');
        //$data['res_installationType'] = $this->ProductSet_Model->statusTocombo('id','211002');
		$data['contractType'] = $this->ProductSet_Model->statusTocombo('stautus_category','211003');

        $data['product_sub_type'] = $this->ProductSet_Model->statusTocombo('stautus_category','211004');
        $data['product_master_all'] = $this->ProductSet_Model->AllProduct_SubTocombo();

        $data['res'] = $this->ProductSet_Model->GetToEdit($id);
        $data['res_installationType'] = $this->ProductSet_Model->statusTocombo('id',$data['res'][0]->product_installation_type);
        $data['product_sub'] = $this->ProductSet_Model->GetProduct_Sub($data['res'][0]->product_id);
        $data['product_installment_type'] = $this->ProductSet_Model->GetInstallmentTypeToEdit($data['res'][0]->product_id);
        
		$data['contractDoc'] = $this->ProductSet_Model->getResContractDoc($data['res'][0]->product_contract_type);
        //$data['installentType'] = $this->Product_Model->GetInstallmentTypeToEdit($id);

        $data['activeSerial'] = (object)array(["val" => 1,"name" =>'มี'],["val"=> 0,"name" => 'ไม่มี']);

        $menu['mainmenu'] = 'product';
		$menu['submenu'] = 'productset';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/product_set_edit', $data);
        $this->load->view('admin/footer');
	}

    public function update(){
        $id = $this->input->post('id');
		$produc_data = array(
            //'product_brand' => $this->input->post('product-brand'),
            'product_version' => $this->input->post('product-version'),
            'product_name' => $this->input->post('product-name'),
            'product_pricce' => $this->input->post('product-pricce'),
            'product_installation_type' => $this->input->post('installation-type'),
            'product_installation_fee' => $this->input->post('installation-fee'),
            'product_customer_type' => $this->input->post('customer-type'),
            'product_contract_type' => $this->input->post('contract-type'),
            'product_contract_doc' => $this->input->post('contract-doc'),
            'product_detail' => $this->input->post('product-detail'),
            'product_udate'=>date("Y-m-d H:m:s")
        );

		$this->ProductSet_Model->update($produc_data, $id);
        redirect(base_url('admin/productSet'));
    }

    public function edit_installment(){
        $id = $this->input->post('productSet-installment-edit-id');
        $product_id = $this->input->post('product-id-edit');
		$produc_data = array( 
            'amount_installment' => $this->input->post('amount_installment'),
            'pay_per_month' => $this->input->post('pay_per_month'),
            'udate'=>date("Y-m-d H:m:s")
        );
		$this->ProductSet_Model->update_installment($produc_data, $id);
        redirect(base_url('admin/productSet/edit/'.$product_id));
    }

    public function add_installment(){
         #### installment type ####
         $date = date(date_format(date_create(),"Y-m-d H:i:s"));
         $InstallmentAmount = $this->input->post('installment-format-amount');
         $id = $this->input->post('id');
         for($i = 1; $i <= (int)$InstallmentAmount; $i++){
             $idArr = [];
             $prefixdate = date("ym");
             $prefixText = 'IT';
             $prefix = $prefixText.$prefixdate;
             $nume = '';
             $res = $this->ProductSet_Model->InstallmentTypeToGenCode();
             if($res){
                 $nume = sprintf($prefix.'%03d',(int)substr($res[0]->installment_type_id,6,9) +1);
             }else{
                 $nume = sprintf($prefix.'%03d',1);
             }
 
             $installment_type_data = array( 
                 'installment_type_id' => $nume,
                 'product_id' => $id ,
                 'amount_installment' => $this->input->post('installment-amount'.$i),
                 'pay_per_month' => $this->input->post('installment-pay'.$i),
                 'cdate'=> $date,
                 'udate'=> $date
             );
             $this->ProductSet_Model->InstallmentTypeInsert($installment_type_data);
         }
        redirect(base_url('admin/productSet/edit/'.$id));
    }

    public function edit_sub_product(){
        $date = date(date_format(date_create(),"Y-m-d H:i:s"));
        $type = $this->input->post('product-sub-type-edit');
        $id = $this->input->post('productSet-sub-edit-id');
        $product_id = $this->input->post('product-id-edit');
        $count = $this->input->post('productSub-count-edit');
        $active_serial = ($this->input->post('edit-active-serial'))? true : false;
        $sub_produc_set = array( 
            'type' => $type,
            'count' => $count,
            'active_serial' => $active_serial,
            'udate'=> $date
        );
        
		$this->ProductSet_Model->update_Product_Sub($sub_produc_set, $id);
        redirect(base_url('admin/productSet/edit/'.$product_id));
    }

    public function add_sub_product(){
        $date = date(date_format(date_create(),"Y-m-d H:i:s"));
        $product_set_id = $this->input->post('product_set_id');
        $product_master_id = $this->input->post('product_master_id');
        $type = $this->input->post('product-sub-type');
        $counts = $this->input->post('productSub-count');
        $active_serial = ($this->input->post('add-active-serial') == 'on')? true : false;

        /*$productSet_obj = json_decode($this->input->post('product-master-json'));
        foreach($productSet_obj as $items){
            $sub_produc_set = array( 
                'product_set_id' => $product_set_id,
                'product_master_id'=> $items,
                'type' => $type,
                'cdate'=> $date,
                'udate'=> $date
            );
            $this->ProductSet_Model->SubProductSetInsert($sub_produc_set);
        }*/
        $sub_produc_set = array( 
            'product_set_id' => $product_set_id,
            'product_master_id'=> $product_master_id,
            'type' => $type,
            'count'=> (int)$counts,
            'active_serial' => $active_serial,
            'cdate'=> $date,
            'udate'=> $date
        );
        $this->ProductSet_Model->SubProductSetInsert($sub_produc_set);
        redirect(base_url('admin/productSet/edit/'.$product_set_id));
    }




    ############  Delete  #############
    public function DeleteProductSet(){
        $id = $this->input->post('id');
		$data = $this->ProductSet_Model->DeleteProductSet($id);
		echo json_encode($data);
    }

    public function delProductSetInstallment(){
        $id = $this->input->post('id');
		$data = $this->ProductSet_Model->delProductSetInstallment($id);
		echo json_encode($data);
    }

    public function delProductSet_sub(){
        $productSet_sub_id = $this->input->post('productSet_sub_id');
        $productSet_id = $this->input->post('productSet_id');
		$data = $this->ProductSet_Model->delProductSet_sub($productSet_sub_id, $productSet_id);
		echo json_encode($data);
    }

    
	public function ProductSet_get(){
		$curent_date = Date('Y-m-d H:i:s');
		$res = new stdClass();
        
        $Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type
        FROM product_set p 
        LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id
        LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id
        LEFT JOIN product_category pc ON p.product_cate = pc.cate_id
        LEFT JOIN brand b ON p.product_brand = b.brand_id";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	  }


}
