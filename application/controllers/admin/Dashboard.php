<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Dashboard_Model');
		$this->load->model('admin/Sms_Model');
		//เรียกใช้งาน config SMS     
		$this->load->config('sms');
        
    } 

	
	public function index()
	{
        $menu['mainmenu'] = 'dashboard';
		$menu['submenu'] = 'dashboard';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
	}

	public function DashboardWebContractRes()
	{
		$data = $this->Dashboard_Model->DashboardWebContractRes();	// get data
		
		#### set data  #####
		$res = array ();
		$res['total'] = count($data);
		$res['datas'] = array();

		$reads = 0;
		$unreads = 0;
		foreach($data as $items){
			if($items->status_code == 0){$unreads++;}else{$reads++;}
		}
		
		$res['legend'] = ['ดำเนินการแล้ว', 'ยังไม่ได้ดำเนินการ'];
		
		$res_reads =  ['value' => $reads, 'name' => 'ดำเนินการแล้ว'];
		$resunreads =  ['value' => $unreads, 'name' => 'ยังไม่ได้ดำเนินการ'];
		array_push($res['datas'],$res_reads);
		array_push($res['datas'],$resunreads);
		
		echo json_encode($res);
	}

	public function DashboardAllContractRes()
	{
		$data = $this->getAllContractRes();	// get data
		#### set data  #####
		$res = array ();
		$res['total'] = 0;
		$res['datas'] = array();
		$res['legend'] = array();

		$reads = 0;
		$unreads = 0;
		foreach($data as $items){
			$res['total'] += $items->value;
			array_push($res['legend'],$items->name);
			array_push($res['datas'],$items);
		}
		echo json_encode($res);
	}
	public function getAllContractRes(){
		$Query = "SELECT count(contract.id) as [value], status.label as [name], status.background_color  FROM contract INNER JOIN status on contract.status = status.id group by contract.status, status.label, status.background_color  order by value";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
	}

	public function getContractRes(){
		$Query = "SELECT *  FROM contract  order by contract_code";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}

	public function getPaymentAmountRetrospect(){

		$cy = date("Y",strtotime(date("Y-m-d")))+543;
		$cm = date("m",strtotime(date("Y-m-d")));
		$cdate = $cy.'-'.$cm ;

		$edate = date("Y-m-t", strtotime($cdate));
		$sdate = date("Y-m-01", strtotime("-12 month", strtotime($cdate)));

		$Query = "SELECT DATEPART(Year, installment_date) Year, DATEPART(Month, installment_date) Month, sum(payment_amount)  payment_amount
		FROM contract_installment 
		WHERE CONVERT(DATETIME, installment_date, 102) BETWEEN CONVERT(DATETIME, '".$sdate."', 102) AND CONVERT(DATETIME, '".$edate."', 102)
		GROUP BY DATEPART(Year, installment_date), DATEPART(Month, installment_date)
		order BY DATEPART(Year, installment_date), DATEPART(Month, installment_date)";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}

	public function InstallmentContractEchardBar()
	{
		$contract_code = $this->input->post('contract_code');
		#### select data  #####
		$Query = "SELECT count(contract.contract_code) as [value], status.label as [name], status.background_color  
		FROM contract_installment  contract
		INNER JOIN status on contract.status_code = status.status_code  AND status.stautus_category = '210603' 
		WHERE contract.contract_code = '".$contract_code."'
		GROUP BY contract.status_code, contract.status, status.label, status.background_color 
		order by value";
		$Res= $this->db->query($Query);
		$data = $Res->result();

		#### set data  #####
		$res = array ();
		$res['total'] = 0;
		$res['datas'] = array();
		$res['legend'] = array();

		$reads = 0;
		$unreads = 0;
		foreach($data as $items){
			$res['total'] += $items->value;
			array_push($res['legend'],$items->name);
			array_push($res['datas'],$items);
		}
		echo json_encode($res);
	}


	public function DashboardInstallmentRes()
	{
		$data = $this->Dashboard_Model->DashboardInstallmentRes();	// get data
		
		#### set data  #####
		$res = array ();
		$res['total'] = count($data);
		$res['labels'] = ['ใกล้ครบกำหนดชำระ 5 วัน', 'ใกล้ครบกำหนดชำระ 4 วัน', 'ใกล้ครบกำหนดชำระ 3 วัน', 'ใกล้ครบกำหนดชำระ 2 วัน', 'ใกล้ครบกำหนดชำระ 1 วัน','ครบกำหนดชำระ' ,'เกินกำหนดชำระ'];
		
		$over0 = 0; 
		$over1 = 0; 
		$over2 = 0; 
		$over3 = 0;
		$over4 = 0; 
		$over5 = 0; 
		$over10 = 0;

		foreach($data as $items){
			switch($items->overdue_code){
				case 0: $over0++;
					break;
				case 1: $over1++;
					break;
				case 2: $over2++;
					break;
				case 3: $over3++;
					break;
				case 4: $over4++;
					break;
				case 5: $over5++;
					break;
				case 10: $over10++;
					break;
			}
		}
		
		$res['datas'] = [$over5,$over4,$over3,$over2,$over1,$over0,$over10];
		
		echo json_encode($res);
	}

	public function TableInstallmentRes()
	{
		$data = $this->Dashboard_Model->TableInstallmentRes();	// get data
		echo json_encode($data);
	}
	
	public function TableInstallmentResPie()
	{
		$contract_code = $this->input->post('contract_code');
		$data = $this->Dashboard_Model->TableInstallmentResPie($contract_code);		
		echo json_encode($data);
	}

	
	public function sendSMS(){
		
		$date = date("Y-m-d H:m:s");
		$customer = $this->input->post('customer-sms');
		$customer_code = $this->input->post('customer-code');
		$temp_code = $this->input->post('temp-code-sms');
		$period = $this->input->post('inst-period-sms');
		$payment = $this->input->post('inst-payment-sms');
		$duedate = $this->input->post('duedate-sms');
		$keywords = $this->input->post('keywords-sms');
		$tel = $this->input->post('tel-sms');
		
		$mess = 'เรียนลูกค้าอีโวเร้นท์ '.$customer. PHP_EOL ;
		$mess .= $keywords. PHP_EOL ; 
		$mess .= 'เลขที่สัญญา '.$temp_code. '  งวดที่ '.$period. PHP_EOL ;
		$mess .= 'ยอดรวมที่ต้องชำระ '.$payment.' บาท '. PHP_EOL ;
		$mess .= 'กำหนดชำระ '.$duedate. PHP_EOL ;
		$mess .= PHP_EOL ;
		$mess .= 'ลูกค้าสามารถโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์ '. PHP_EOL  ;
		$mess .= 'ธนาคารกสิกรไทย สาขาแม็กซ์แวลู พัฒนาการ '. PHP_EOL  ;
		$mess .= 'เลขที่บัญชี 095-1-36903-9  '. PHP_EOL  ;
		$mess .= 'ชื่อบัญชี บริษัท อีโวเร้นท์ จำกัด '. PHP_EOL  ;
		$mess .= PHP_EOL ;
		$mess .= 'หรือติดต่ออีโวเร้นท์  '. PHP_EOL  ;
		$mess .= 'เบอร์โทร : 097-162-8565 '. PHP_EOL  ;
		$mess .= 'Line ID : @evorent'. PHP_EOL ;
		$mess .= PHP_EOL ;
		$mess .= 'ขออภัยหากท่านชำระแล้ว ';
		
		$obj = new stdClass();
		$obj->telephone_number = $tel;
		$obj->message = $mess;
		////$sms_config_ants = $this->config->item('ANTS');
		$sms_config_ants = $this->config->item('evorent');
		$sendsms = $this->sendnotifymessage($obj, $sms_config_ants);
		
		###### create sms_code #####
		$prefixdate = date("ym"); 
		$prefixText = 'S';
		$prefix =  $prefixText.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Sms_Model->getTempToGenCode($prefix);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->sms_code,5,10));
			}
			$code = sprintf($prefix.'%03d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%03d',1);
		}

		###### create date format #####
		$date = explode('-', date("Y-m-d")) ;
		$date[0] = (int)$date[0]+543;

		$data = array( 
			'sms_code' => $code, 
			'customer_code' => $customer_code, 
			'temp_code' => $temp_code,
			'period' => (int)$period,
			'message' => $mess,
			'tel' => $tel,
            'status' => $sendsms['status'],
            'cdate'=> implode('-', $date)
        );
		
		$this->Sms_Model->insertSMS($data);

		redirect('admin/dashboard');
    }

	public function sendnotifymessage($requestCriteria, $config_ants){
    
  
		$telephone_number = $requestCriteria->telephone_number;
		//echo $telephone_number;exit();
		if(substr($telephone_number, 0,1) == '0'){
			$telephone_number = '66'.substr($telephone_number, 1,9);
		}
		
		$data = array(
		  'from'=>$config_ants['Sender'],
		  'to'=>$telephone_number,
		  'text'=>$requestCriteria->message
		);
  
		$data = [
		  "messages" => [
			[
			  "from" => $config_ants['Sender'],
			  "destinations" => [
				[
				  "to" => $telephone_number
				]
			  ],
			  "text" => $requestCriteria->message
			]
		  ]
		];
  
		// echo json_encode($data);exit;
  
		if($this->curlSendSMSAntsV2($data, $config_ants)){
		  return array(
			'status'=>true,
			'result_code'=>'success',
			'result_desc'=>'Send sms success'
		  );
  
		}else{
		  return array(
			'status'=>false,
			'result_code'=>'error',
			'result_desc'=>'Can not send sms,Please try again'
		  );
  
		}
	}

	public function  curlSendSMSAntsV2($data = [], $config_ants){

		$authorize = "";
		$authorize = "Basic ".base64_encode($config_ants['Username'].':'.$config_ants['Password']);
		$ch = curl_init();   
		try{
			  curl_setopt($ch,CURLOPT_URL,$config_ants['Url_v2']);
			  // if(ENVIRONMENT == 'production'){
			  //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  // }
			  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type:application/json',
				'Authorization:'.$authorize
			  ));
			  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
			  curl_setopt($ch,CURLOPT_POST,1); 
			  curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
			  
			  $Result = curl_exec($ch);
			  $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
			  curl_close($ch);
			  //print_r($Result);exit;
			  $decode_result = json_decode($Result);
			  // if($decode_result->messages[0]->status->groupId == '1'){
			  //   return true;
			  // }else{
			  //   return false;
			  // }
			  return true;
		} catch( Exception $e ){
		
		  return false;
		
		}
	}

	

}
