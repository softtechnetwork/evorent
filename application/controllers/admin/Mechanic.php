<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Mechanic extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 
		date_default_timezone_set("Asia/Bangkok"); // set timeZone
		//เรียกใช้งาน library
		//$this->load->library('mpdf');
		//$this->load->library('M_pdf');
		//$this->load->library('pdf');
		//$this->load->library('excel');

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
		$this->load->model('admin/Mechanic_Model');
    } 

	public function index()
	{
		//$data['getCustomer'] = $this->Report_Model->getCustomer();
        //$data['getTemp'] = $this->Report_Model->getTemp();
        //$data['getStatus'] = $this->Status_Model->getStatus('Installment');		
        
        $menu['mainmenu'] = 'mechanic';
		$menu['submenu'] = 'mechanic';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/mechanic_list');
        $this->load->view('admin/footer');
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT  charng.* 
		, p.province_name, a.amphur_name, t.district_name
		FROM charng 
		LEFT JOIN provinces p ON charng.province_id = p.id 
		LEFT JOIN amphurs a ON charng.amphurs_id = a.id 
		LEFT JOIN tumbon t ON charng.district_id = t.id";
		
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}

	public function create()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('Customer');
		$data['province'] = $this->Mechanic_Model->province();
		$data['agent'] = $this->Mechanic_Model->getAgentToCombo();
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'mechanic';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/mechanic_create',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		
		$prefixdate = date("ym");
		$prefixchar = 'T';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res = $this->Mechanic_Model->getTogenID($prefixdate);
		if($res){
			foreach($res as $items){
				array_push($idArr, (int)substr($items->charng_code,5,9));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		$agent = $this->input->post('agent');
		$broker = $this->input->post('broker');
		$data = array( 
			'charng_code' => $code, 
			'idcard' =>  implode(preg_split('/[-]/',  $this->input->post('idcard'))),
			'agent_code' => $this->input->post('agent_code'), 
            'name' => $this->input->post('name'), 
            'sname' => $this->input->post('sname'), 
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			
			'office_name' => $this->input->post('name-office'),
			'office_taxpayer'=> $this->input->post('taxpayer-office'),
			'office_address' => $this->input->post('address-office'),
			'office_province' => $this->input->post('province-office'),
			'office_amphurs' => $this->input->post('amphurs-office'),
			'office_district' => $this->input->post('district-office'),
			'office_zipcode' => $this->input->post('zipcode-office'), 
			'phone' => $this->input->post('tel-office'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
			'ref_id' => $this->input->post('ref_code'),
			'agent' => ($agent == 'on')?true:false,
			'broker' => ($broker == 'on')?true:false,
            'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         ); 
	
		 $bdate = $this->input->post('bdate');
		 if($bdate != '' && $bdate != null){
			$data['bdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $bdate),"Y-m-d"));
		 }

		 $agent_create_date = $this->input->post('agent-create-date');
		 if($agent_create_date != '' && $agent_create_date != null){
			$data['agent_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $agent_create_date),"Y-m-d"));
		 }else{
			$data['agent_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y',$this->converYYToyy()),"Y-m-d"));
		 }

		 $abroker_create_date = $this->input->post('broker-create-date');
		 if($abroker_create_date != '' && $abroker_create_date != null){
			$data['broker_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $abroker_create_date),"Y-m-d"));
		 }else{
			$data['broker_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y',$this->converYYToyy()),"Y-m-d"));
		 }

		$this->Mechanic_Model->insert($data); 
        redirect('admin/mechanic');
    }

	
	public function getCharngByIdcard()
	{
		$idcard = $this->input->post('idcard');
		$data = $this->Mechanic_Model->getCharngByIdcard($idcard);		
		echo json_encode($data);
	}

	public function jsonMechanic()
	{
		$search = $this->input->post('Search');
		//$itemPerPage = (int)$this->input->post('itemPerPage');
		//$itemStt = (int)$this->input->post('itemStt');
		//$itemEnd = (int)$this->input->post('itemEnd');

		//$data = $this->Mechanic_Model->Mechanic_Model($searchArray, $itemPerPage, $itemStt, $itemEnd);		
		$data = $this->Mechanic_Model->jsonMechanic($search);		
		echo json_encode($data);
	}

	public function edit($id = null)
	{
		$data['res'] = $this->Mechanic_Model->getToEdit($id);
		$data['province'] = $this->Mechanic_Model->province();
        $data['amphoe'] = $this->Mechanic_Model->amphoe();
        $data['districs'] = $this->Mechanic_Model->districs();
		//$data['customer_premise'] = $this->Customer_Model->selectPremise($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'mechanic';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/mechanic_edit',$data);
        $this->load->view('admin/footer');
	}

	public function update(){
		$agent = $this->input->post('agent');
		$broker = $this->input->post('broker');
		
		$data = array( 
			'name' => $this->input->post('name'), 
            'sname' => $this->input->post('sname'), 
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'), 
			
			'office_name' => $this->input->post('name-office'),
			'office_taxpayer'=> $this->input->post('taxpayer-office'),
			'office_address' => $this->input->post('address-office'),
			'office_province' => $this->input->post('province-office'),
			'office_amphurs' => $this->input->post('amphurs-office'),
			'office_district' => $this->input->post('district-office'),
			'office_zipcode' => $this->input->post('zipcode-office'), 
			'phone' => $this->input->post('tel-office'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
			'ref_id' => $this->input->post('ref_code'),
			'agent' => ($agent == 'on')?true:false,
			'broker' => ($broker == 'on')?true:false,
            'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         );

		 $agent_create_date = $this->input->post('agent-create-date');
		 if($agent_create_date != '' && $agent_create_date != null){
			$data['agent_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $agent_create_date),"Y-m-d"));
		 }else{
			$data['agent_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y',$this->converYYToyy()),"Y-m-d"));
		 }

		 $abroker_create_date = $this->input->post('broker-create-date');
		 if($abroker_create_date != '' && $abroker_create_date != null){
			$data['broker_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y', $abroker_create_date),"Y-m-d"));
		 }else{
			$data['broker_cdate'] = date(date_format(DateTime::createFromFormat('d/m/Y',$this->converYYToyy()),"Y-m-d"));
		 }
		 
		 $charng_code = $this->input->post('charng_code');
		$this->Mechanic_Model->update($data,$charng_code);
        redirect('admin/mechanic');
    }

	public function converYYToyy($date = null)
	{
		$yy = ((int)date("Y")+543);
		$mm = (date("m"));
		$dd = (date("d"));
		return $dd.'/'.$mm.'/'.$yy;
	}

	public function PDF($id = null)
	{
		//เรียกใช้งาน Libary PDF    
		$this->load->library('Pdf');

		// สร้าง object สำหรับใช้สร้าง pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
        // กำหนดรายละเอียดของ pdf
        
		//$pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide';
		
         
        // กำหนดข้อมูลที่จะแสดงในส่วนของ header และ footer
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// กำหนดรูปแบบของฟอนท์และขนาดฟอนท์ที่ใช้ใน header และ footer
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // กำหนดค่าเริ่มต้นของฟอนท์แบบ monospaced 
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // กำหนด margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // กำหนดการแบ่งหน้าอัตโนมัติ
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        // กำหนดรูปแบบการปรับขนาดของรูปภาพ 
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        // ---------------------------------------------------------
         
        // set default font subsetting mode
		$DateControlDoc =  '(10-06-2021)';
        $pdf->setFontSubsetting(true);

		$res = $this->Mechanic_Model->getToPDF($id);
		
		if($res[0]->broker == 1){
			$this->Broker($pdf, $res, $DateControlDoc);
			$this->BrokerCoppy($pdf, $res, $DateControlDoc);
		}

		if($res[0]->agent == 1){
			$this->Agent($pdf, $res, $DateControlDoc);
			$this->AgentCoppy($pdf, $res, $DateControlDoc);
		}

		$file_name = $id.'.pdf';
		$pdf->Output($file_name, 'I');
	}

	public function Broker($pdf, $res, $DateControlDoc)
	{
		//######### Broker A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0007/DOC0007'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		if($res != null){
			$FullName = $res[0]->agentName.' '.$res[0]->agentSname;
			$pdf->Text(90, 62.5, $FullName);
			$pdf->Text(57, 71.5, $res[0]->agent_code);

			$pdf->Text(25, 89.2, $res[0]->name);
			$pdf->Text(90, 89.2, $res[0]->sname);

			$bDate = date_create($res[0]->bdate);
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(148, 89.2, $age);

			$pdf->Text(50, 98, $this->textFormat($res[0]->idcard,'','-'));
			$pdf->Text(35, 106.7, $res[0]->address);
			$pdf->Text(28, 115.5, $res[0]->district_name);
			$pdf->Text(88, 115.5, $this->RemoveText('อำเภอ',$res[0]->amphur_name));
			$pdf->Text(150, 115.5, $res[0]->province_name);

			$pdf->Text(62, 124.5, $res[0]->office_name);
			$pdf->Text(20, 133.3, $res[0]->office_address);
			$pdf->Text(28, 142, $res[0]->officeDistrict);
			$pdf->Text(96, 142, $this->RemoveText('อำเภอ',$res[0]->officeAmphurs));
			$pdf->Text(155, 142, $res[0]->officeProvince);
		}
		//######### Broker A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0007/DOC0007'.$DateControlDoc.'_p02.jpg');

		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		if($res != null){
			$pdf->Text(95, 47, $this->DateThai($res[0]->broker_cdate));
			
			$FullName = $res[0]->name.' '.$res[0]->sname;
			$pdf->Text(27, 122.8, $FullName);
		}
	}

	public function BrokerCoppy($pdf, $res, $DateControlDoc)
	{
		//######### Broker A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0007/DOC0007'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		if($res != null){
			$FullName = $res[0]->agentName.' '.$res[0]->agentSname;
			$pdf->Text(90, 62.5, $FullName);
			$pdf->Text(57, 71.5, $res[0]->agent_code);

			$pdf->Text(25, 89.2, $res[0]->name);
			$pdf->Text(90, 89.2, $res[0]->sname);

			$bDate = date_create($res[0]->bdate);
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(148, 89.2, $age);

			$pdf->Text(50, 98, $this->textFormat($res[0]->idcard,'','-'));
			$pdf->Text(35, 106.7, $res[0]->address);
			$pdf->Text(28, 115.5, $res[0]->district_name);
			$pdf->Text(88, 115.5, $this->RemoveText('อำเภอ',$res[0]->amphur_name));
			$pdf->Text(150, 115.5, $res[0]->province_name);

			$pdf->Text(62, 124.5, $res[0]->office_name);
			$pdf->Text(20, 133.3, $res[0]->office_address);
			$pdf->Text(28, 142, $res[0]->officeDistrict);
			$pdf->Text(96, 142, $this->RemoveText('อำเภอ',$res[0]->officeAmphurs));
			$pdf->Text(155, 142, $res[0]->officeProvince);
		}
		//######### Broker A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0007/DOC0007'.$DateControlDoc.'_p02.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		if($res != null){
			$pdf->Text(95, 47, $this->DateThai($res[0]->broker_cdate));

			$FullName = $res[0]->name.' '.$res[0]->sname;
			$pdf->Text(27, 122.8, $FullName);
		}
	}

	public function Agent($pdf, $res, $DateControlDoc)
	{
		//######### Agent A1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0006/DOC0006'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		if($res != null){
			$agentFullName = $res[0]->agentName.' '.$res[0]->agentSname;
			$pdf->Text(90, 62.7, $agentFullName);
			$pdf->Text(57, 71.5, $res[0]->agent_code);

			$pdf->Text(25, 89.2, $res[0]->name);
			$pdf->Text(90, 89.2, $res[0]->sname);

			$bDate = date_create($res[0]->bdate);
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(148, 89.2, $age);

			$pdf->Text(50, 98, $this->textFormat($res[0]->idcard,'','-'));
			$pdf->Text(35, 106.6, $res[0]->address);
			$pdf->Text(28, 115.5, $res[0]->district_name);
			$pdf->Text(88, 115.5, $this->RemoveText('อำเภอ',$res[0]->amphur_name));
			$pdf->Text(150, 115.5, $res[0]->province_name);

			$pdf->Text(62, 124.5, $res[0]->office_name);
			$pdf->Text(20, 133.1, $res[0]->office_address);
			$pdf->Text(28, 142, $res[0]->officeDistrict);
			$pdf->Text(96, 142, $this->RemoveText('อำเภอ',$res[0]->officeAmphurs));
			$pdf->Text(155, 142, $res[0]->officeProvince);
		}
		//######### Agent A2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0006/DOC0006'.$DateControlDoc.'_p02.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);

		//######### Agent A3 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0006/DOC0006'.$DateControlDoc.'_p03.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		//######### Agent A4 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0006/DOC0006'.$DateControlDoc.'_p04.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		//######### Agent A5 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/original/DOC0006/DOC0006'.$DateControlDoc.'_p05.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);

		if($res != null){
			$pdf->Text(95, 108.5, $this->DateThai($res[0]->agent_cdate));
			$FullName = $res[0]->name.' '.$res[0]->sname;
			$pdf->Text(27, 184.5, $FullName);
		}
	}

	public function AgentCoppy($pdf, $res, $DateControlDoc)
	{
		//######### Agent B1 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0006/DOC0006'.$DateControlDoc.'_p01.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		$pdf->setPageMark();
		$pdf->SetFont('thsarabun', '', 15, '', true);
		if($res != null){
			$agentFullName = $res[0]->agentName.' '.$res[0]->agentSname;
			$pdf->Text(90, 62.7, $agentFullName);
			$pdf->Text(57, 71.5, $res[0]->agent_code);

			$pdf->Text(25, 89.2, $res[0]->name);
			$pdf->Text(90, 89.2, $res[0]->sname);

			$bDate = date_create($res[0]->bdate);
			$age = ( ((int)date("Y")+543)-(int)date_format($bDate,"Y") );
			$pdf->Text(148, 89.2, $age);

			$pdf->Text(50, 98, $this->textFormat($res[0]->idcard,'','-'));
			$pdf->Text(35, 106.6, $res[0]->address);
			$pdf->Text(28, 115.5, $res[0]->district_name);
			$pdf->Text(88, 115.5, $this->RemoveText('อำเภอ',$res[0]->amphur_name));
			$pdf->Text(150, 115.5, $res[0]->province_name);

			$pdf->Text(62, 124.5, $res[0]->office_name);
			$pdf->Text(20, 133.1, $res[0]->office_address);
			$pdf->Text(28, 142, $res[0]->officeDistrict);
			$pdf->Text(96, 142, $this->RemoveText('อำเภอ',$res[0]->officeAmphurs));
			$pdf->Text(155, 142, $res[0]->officeProvince);
		}
		//######### Agent B2 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0006/DOC0006'.$DateControlDoc.'_p02.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);

		//######### Agent B3 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0006/DOC0006'.$DateControlDoc.'_p03.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		//######### Agent B4 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0006/DOC0006'.$DateControlDoc.'_p04.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		//######### Agent B5 ##########//
		$pdf->AddPage();
		$img_file = base_url('img/copy/DOC0006/DOC0006'.$DateControlDoc.'_p05.jpg');
		$bMargin = $pdf->getBreakMargin();
		$auto_page_break = $pdf->getAutoPageBreak();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
		
		if($res != null){
			$pdf->Text(95, 108.5, $this->DateThai($res[0]->agent_cdate));
			$FullName = $res[0]->name.' '.$res[0]->sname;
			$pdf->Text(27, 184.5, $FullName);
		}
	}

	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	 }

	function DateThai($strDate)
	{
		//$strYear = date("Y",strtotime($strDate))+543;
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		//$strHour= date("H",strtotime($strDate));
		//$strMinute= date("i",strtotime($strDate));
		//$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
	}

	public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			$temp = explode($text, $wording);
			$string = $temp[1];
		}
		return $string ;
	 }
	
	/*
	public function getInstallmentAll()
	{
        $searchArray = $this->input->post('searchArray');
		$itemPerPage = (int)$this->input->post('itemPerPage');
		$itemStt = (int)$this->input->post('itemStt');
		$itemEnd = (int)$this->input->post('itemEnd');

		$data = $this->Report_Model->selectAllItems($searchArray, $itemPerPage, $itemStt, $itemEnd);
		echo json_encode($data);
	}
	
	public function getTempByCustomer()
	{
        $customer = $this->input->post('customer');
		
		$data = $this->Report_Model->selectTempByCustomer($customer);
		echo json_encode($data);
	}*/

	#############  Detail ###############

	public function detail($id = null)
	{
		$data['res'] = $this->Mechanic_Model->getToDetail($id);
		$data['province'] = $this->Mechanic_Model->province();
        $data['amphoe'] = $this->Mechanic_Model->amphoe();
        $data['districs'] = $this->Mechanic_Model->districs();
		$data['charng_contract'] = $this->Mechanic_Model->selectContract($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'mechanic';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/mechanic_detail',$data);
        $this->load->view('admin/footer');
	}

	public function getToChart()
	{
		$contract_code = $this->input->post('contract_code');
		$data = $this->Mechanic_Model->getToChart($contract_code);		
		echo json_encode($data);
	}

}
