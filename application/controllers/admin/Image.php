<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class Image extends CI_Controller {
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductMaster_Model');
        $this->load->model('admin/ProductSet_Model');
    } 

    ############  List  #############
	public function index(){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'image';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/image_list');
        $this->load->view('admin/footer');
	}
    public function upload(){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'imageupload';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/image_upload');
        $this->load->view('admin/footer');
	}



    ############  Image product  #############
	public function imageproduct(){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'imageproduct';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/imageproduct');
        $this->load->view('admin/footer');
	}
    public function imageproduct_manage($productSet_id = null){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'imageproduct';

        $data['productSet_id'] = $productSet_id;

        $this->load->view('admin/header',$menu);
		$this->load->view('admin/imageproduct_manage',$data);
        $this->load->view('admin/footer');
	}
    public function imageproduct_upload(){
        $arr = $this->input->post();
        // $form_obj = json_encode($arr['form']);
        // $xx = serialize($form_obj);

        $ref_id = $arr['ref_id'];
        $master_cate_key = $arr['master_cate_key'];
        $namcate_ide = $arr['namcate_ide'];
        $file = $arr['file'];
        $master_cate = $arr['master_cate'];

        $this->do_upload($file);
	}
    public function imageproduct_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        //$product_id = $this->input->post('name');
        // $desc = $this->input->post('desc');
        $Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type
        FROM product_set p 
        LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id
        LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id
        LEFT JOIN product_category pc ON p.product_cate = pc.cate_id
        LEFT JOIN brand b ON p.product_brand = b.brand_id ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}
    public function imageproduct_once(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $productSet_id = $this->input->post('productSet_id');
        $Query = "SELECT p.*, pc.cate_name, b.brand_name, st_customer.label as customer_type, st_contract.label as contract_type
        FROM product_set p 
        LEFT JOIN status st_customer ON p.product_customer_type = st_customer.id
        LEFT JOIN status st_contract ON p.product_contract_type = st_contract.id
        LEFT JOIN product_category pc ON p.product_cate = pc.cate_id
        LEFT JOIN brand b ON p.product_brand = b.brand_id
        WHERE p.product_id = '".$productSet_id."' ";

		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
		//return $data;
	}
    public function imageproduct_category_get_key(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $method = $this->input->post('method');
        // $desc = $this->input->post('desc');

        $Query = " SELECT img_cate.*,  img_master_cate.name as master_name
        FROM [dbo].[img_cate]
        INNER JOIN [dbo].[img_master_cate] on img_cate.img_master_cate_key = img_master_cate.[key]
        WHERE img_cate.img_master_cate_key = '".$method."'
        ";
		$Respons = $this->db->query($Query);
        $data = $Respons->result();
        echo json_encode($data);
	}



    public function do_upload(){
            
        $config['allowed_types'] = 'jpeg|jpg|png'; //รูปแบบไฟล์ที่ อนุญาตให้ Upload ได้
        $config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config['max_height']    = 0;  //ขนาดความสูงสูงสดุ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
        $config["overwrite"] = 	true;
        //$config['encrypt_name']  = true; //กำหนดเป็น true ให้ระบบ เปลียนชื่อ ไฟล์  อัตโนมัติ  ป้องกันกรณีชื่อไฟล์ซ้ำกัน
    

        $ref_id = $this->input->post('ref_id');
		$master_cate = $this->input->post('master_cate_key');
		$img_cate_id = $this->input->post('cate_id');	
        //$files = $this->input->post('filename');
        $imgno = $this->input->post('imgno');
        $files = 'input-'.$master_cate.'-'.$img_cate_id.'-'.$imgno;
        
        
		//$files = "input-".$element_id.$no;
        //print_r($files);
        $obj = array(
			'ref_id' =>$ref_id,  
			'master_cate' => $master_cate, 
			'img_cate_id' => $img_cate_id,
			'cdate'=>date(date_format(date_create(),"Y-m-d H:i:s")),
			'udate'=>date(date_format(date_create(),"Y-m-d H:i:s"))
		); 
        
        ##### name img ####
		if (!empty($_FILES[$files]['name'])) {
            $fname = $ref_id.'-'.$img_cate_id.'-'.$imgno;
			$_FILES[$files]['name'] = $fname.".".pathinfo($_FILES[$files]['name'], PATHINFO_EXTENSION);
			//$premise['img'] = $_FILES[$files]['name'];
		}

        ##### create path ####
		$upload_path = "./uploaded/ImageUpload/".$master_cate."/".$ref_id."/".$img_cate_id;
		if (!file_exists($upload_path)) {
			if (!mkdir($upload_path, 0777, true)) {//0777
				die($upload_path.' Failed to create folders...');
			}
        }
		
		$fullPath = $upload_path.'/'.$_FILES[$files]['name'];
		$obj['path'] = $fullPath;

        ##### create img to dir ####
		$config['upload_path'] = $upload_path; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload($files)) {
			$path = $this->upload->data();
            
            ### insert premise ###
            $this->db->insert("img", $obj);
            //if ($this->db->insert("img", $obj)) {  return true; }
		}else{
			echo $this->upload->display_errors();
			exit();
		}
		### redirect to list ###
        $redirected = 'admin/Image/imageproduct_manage/'.$ref_id;
		redirect(base_url($redirected));
    }
    public function del_img(){
        $res = new stdClass();
		$data = $this->input->post();

        $this->db->where('id', $data['id']);
        $this->db->where('ref_id', $data['ref_id']);
        $this->db->where('master_cate', $data['master_cate']);
        $this->db->where('img_cate_id', $data['img_cate_id']);
        //$this->db->delete('img');

        if ($this->db->delete('img')) { 
			if (file_exists($data['path'])) {
				unlink($data['path']);
			}
            $res->status = true;
            //$redirected = 'admin/Image/imageproduct_manage/'.$data['ref_id'];
            $redirected = $data['ref_id'];
            $res->datas = $redirected;
        }else{
            $res->status = false;
            $res->message = '';
        }
        
		echo json_encode($res);
    }
    public function getImg()
	{
		$ref_id = $this->input->post('ref_id');
		$master_cate = $this->input->post('master_cate');
		$img_cate_id = $this->input->post('img_cate_id');		

        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $Query = "SELECT img.* FROM img 
        WHERE img.ref_id = '".$ref_id."' AND master_cate = '".$master_cate."' AND  img_cate_id = '".$img_cate_id."' ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
		echo json_encode($data);
	}



    //##### Category ######//
    public function category(){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'imagecate';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/image_category');
        $this->load->view('admin/footer');
	}
    public function category_insert(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $master_cate = $this->input->post('master_cate');
        $name = $this->input->post('name');
        $desc = $this->input->post('desc');
        
        //### insert #####/
        $Query = " INSERT INTO [dbo].[img_cate] ([img_master_cate_key],[name],[desc],[cdate],[udate]) VALUES ('".$master_cate."', '".$name."', '".$desc."', '".$curent_date."', '".$curent_date."') ";
		$Respons = $this->db->query($Query);
        if($Respons){
            $res->status = true;
            $res->dates = null;
            $res->message = 'success fully';
            $res->desc = 'เพิ่มข้อมูลสำเร็จ';
        }else{
            $res->status = false;
            $res->dates = null;
            $res->message = 'unsuccess fully';
            $res->desc = 'เพิ่มข้อมูลไม่สำเร็จ';
        }
        echo json_encode($res);
	}
    public function category_update(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $desc = $this->input->post('desc');
        
        //### insert #####/
        $Query = " UPDATE [dbo].[img_cate] SET [desc] = '".$desc."', [udate] = '".$curent_date."' WHERE id ='".$id."' ";
		$Respons = $this->db->query($Query);
        if($Respons){
            $res->status = true;
            $res->dates = null;
            $res->message = 'success fully';
            $res->desc = 'แก้ไขข้อมูลสำเร็จ';
        }else{
            $res->status = false;
            $res->dates = null;
            $res->message = 'unsuccess fully';
            $res->desc = 'แก้ไขข้อมูลไม่สำเร็จ';
        }
        echo json_encode($res);
	}
    public function category_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        // $name = $this->input->post('name');
        // $desc = $this->input->post('desc');

        $Query = " SELECT img_cate.*,  img_master_cate.name as master_name
        FROM [dbo].[img_cate]
        INNER JOIN [dbo].[img_master_cate] on img_cate.img_master_cate_key = img_master_cate.[key]
        ";
		$Respons = $this->db->query($Query);
        $data = $Respons->result();
        echo json_encode($data);
	}

    //##### Master Category ######//
    public function mastercate(){
        $menu['mainmenu'] = 'image';
		$menu['submenu'] = 'imagemastercate';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/image_mastercate');
        $this->load->view('admin/footer');
	}
    public function mastercate_insert(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $key = $this->input->post('key');
        $name = $this->input->post('name');
        $desc = $this->input->post('desc');
        
        //### insert #####/
        $Query = " INSERT INTO [dbo].[img_master_cate] ([key],[name],[desc],[cdate],[udate]) VALUES ('".$key."','".$name."', '".$desc."', '".$curent_date."', '".$curent_date."') ";
		$Respons = $this->db->query($Query);
        if($Respons){
            $res->status = true;
            $res->dates = null;
            $res->message = 'success fully';
            $res->desc = 'เพิ่มข้อมูลสำเร็จ';
        }else{
            $res->status = false;
            $res->dates = null;
            $res->message = 'unsuccess fully';
            $res->desc = 'เพิ่มข้อมูลไม่สำเร็จ';
        }
        echo json_encode($res);
	}
    public function mastercate_update(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        $id = $this->input->post('id');
        $key = $this->input->post('key');
        $name = $this->input->post('name');
        $desc = $this->input->post('desc');
        
        //### insert #####/
        $Query = " UPDATE [dbo].[img_master_cate] SET [desc] = '".$desc."', [udate] = '".$curent_date."' WHERE id ='".$id."' ";
		$Respons = $this->db->query($Query);
        if($Respons){
            $res->status = true;
            $res->dates = null;
            $res->message = 'success fully';
            $res->desc = 'แก้ไขข้อมูลสำเร็จ';
        }else{
            $res->status = false;
            $res->dates = null;
            $res->message = 'unsuccess fully';
            $res->desc = 'แก้ไขข้อมูลไม่สำเร็จ';
        }
        echo json_encode($res);
	}
    public function mastercate_get(){
        $curent_date = Date('Y-m-d H:i:s');
        $res = new stdClass();
        // $name = $this->input->post('name');
        // $desc = $this->input->post('desc');

        $Query = " SELECT * FROM [dbo].[img_master_cate]";
		$Respons = $this->db->query($Query);
        $data = $Respons->result();
        echo json_encode($data);
	}
    
    




























}
