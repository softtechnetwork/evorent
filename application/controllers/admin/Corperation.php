<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corperation extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Corperation_Model');
		$this->load->model('admin/Customer_Model');
		$this->load->model('admin/Premise_Model');
		$this->load->model('admin/Status_Model');
		$this->load->model('admin/Logs_Model');
		//เรียกใช้งาน Class helper 
     	$this->load->helper('file'); 
    } 

	
	public function index()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('210601');
        
		$menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'corperation';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/corperation_list', $data);
        $this->load->view('admin/footer');
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT  ROW_NUMBER() OVER ( ORDER BY customer.customer_code ) AS RowNum, customer.*,
		status.status_code as statusid, status.status_code, status.stautus_category, status.label, status.detail, status.color, status.background_color, 
		inhabited.address 
		from customer
		INNER JOIN status ON customer.status = status.id
		LEFT JOIN inhabited ON customer.addr = inhabited.inhabited_code
		WHERE customer.type = N'corperation' ";

		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}

	public function getRes()
	{
		$Search = $this->input->post('Search');
		$itemStt = $this->input->post('itemStt');
		$itemEnd = $this->input->post('itemEnd');		

		$data = $this->Corperation_Model->select($Search, $itemStt, $itemEnd);			
		echo json_encode($data);
	}
	public function getResAll()
	{
		$Search = $this->input->post('Search');
		$data = $this->Corperation_Model->selectAllItems($Search);
		echo json_encode($data);
	}
    
	public function create()
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('210601');
		$data['province'] = $this->Corperation_Model->province();
        
		$menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'corperation';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/corperation_create',$data);
        $this->load->view('admin/footer');
	}

	public function insert(){
		
		$prefixdate = date("ym");
		$prefixchar = 'M';
		$prefix =  $prefixchar.$prefixdate;
		$idArr = [];
		$code = '';
		$res_customer = $this->Customer_Model->selectOne($prefixdate);
		if($res_customer){
			foreach($res_customer as $items){
				array_push($idArr, (int)substr($items->customer_code,5,9));
			}
			$code = sprintf($prefix.'%05d',MAX($idArr)+1);
		}else{
			$code = sprintf($prefix.'%05d',1);
		}

		$data = array( 
			'customer_code' => $code, 
			'idcard' =>  implode(preg_split('/[-]/',  $this->input->post('idcard'))),
            'firstname' => $this->input->post('name'), 
            'lastname' => '', 
			//'addr' => $this->input->post('address'), 
			//'province_id' => $this->input->post('province'), 
			//'amphurs_id' => $this->input->post('amphurs'), 
			//'tumbon_id' => $this->input->post('district'), 
			//'zip_code' => $this->input->post('zipcode'), 
			//'birthday' => $this->input->post('bdate'), 

			//'career' => $this->input->post('career-select'),
			//'career_text' => $this->input->post('career'),
			//'monthly_income' => $this->input->post('monthly-income'),
			'type'=> 'corperation',
			'phone' => implode(preg_split('/[-]/', $this->input->post('phone'))), 
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			//'sex' => $this->input->post('gender'),
			'status' => $this->input->post('status'),
			'remark' => $this->input->post('remark'),
            'created'=> date(date_format(date_create(),"Y-m-d H:i:s"))
         ); 

		 
		$countArr = [];
		$ihb_code = '';
		$res_inhabited = $this->Customer_Model->getIhbByCode($prefixdate);
		if($res_inhabited){
			foreach($res_inhabited as $items){
				array_push($countArr, (int)substr($items->inhabited_code,5,9));
			}
			$ihb_code = MAX($countArr)+1;
		}else{
			$ihb_code = 1;
		}
		$Inhabited = array( 
			'inhabited_code' => sprintf('A'.$prefixdate.'%05d',$ihb_code), 
			'customer_code' => $data['customer_code'],
			'category' => 'ที่อยู่สถานที่ทำงาน',
			'address' => $this->input->post('address'), 
			'province_id' => $this->input->post('province'), 
			'amphurs_id' => $this->input->post('amphurs'), 
			'district_id' => $this->input->post('district'), 
			'zip_code' => $this->input->post('zipcode'),
            'cdate'=> $data['created']
         );  

		 $data['addr'] = $Inhabited['inhabited_code'];

		$this->Corperation_Model->insert($data); 
		$this->Corperation_Model->insertInhabitd($Inhabited); 
		//$this->Customer_Model->insertInhabitd($InhabitedCurrent); 
		
        redirect('admin/corperation');
    }

	public function edit($id = null)
	{
		$data['resStatus'] = $this->Status_Model->requestStatus('210601');
		//$data['province'] = $this->Customer_Model->province();
		//$data['amphurs'] = $this->Customer_Model->amphurs();
		//$data['district'] = $this->Customer_Model->district();

		$data['res_customer'] = $this->Corperation_Model->selectOne($id);
		$data['inhabited'] = $this->Corperation_Model->getInhabited($id);
		$data['customer_premise'] = $this->Corperation_Model->selectPremise($id);
		//$data['monthlyIncome'] = $this->monthlyIncome();
		//$data['career'] = $this->career();
		
        $menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'corperation';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/corperation_edit',$data);
        $this->load->view('admin/footer');
	}

	public function update(){

		$customer_code = $this->input->post('customer_code');
		
		$data = array( 
            'firstname' => $this->input->post('name'), 
			'phone' => implode(preg_split('/[-]/', $this->input->post('phone'))), 
			'tel' => implode(preg_split('/[-]/', $this->input->post('tel'))), 
			'email' => $this->input->post('email'), 
			'status' => $this->input->post('status'),
			'remark' => $this->input->post('remark'),
            'updated'=> date(date_format(date_create(),"Y-m-d H:i:s"))
        ); 
		
		$this->Corperation_Model->update($data,$customer_code); 

		 ################# Create Logs ##############
		 $LogsJSON = json_encode($data);
		 $code = date("dmY-His");
		 $logs_data = array( 
            //'img' => $img, 
			'logs_code' =>  "Logs".$code, 
            'ref_code' => $customer_code, 
            'category' => 'Customer', 
			'logs' => $LogsJSON, 
            'create_logs'=>$data['updated']
			
        ); 
		 
		//$this->Logs_Model->insert($logs_data); 

        redirect('admin/corperation');
    }

	public function detail($id = null)
	{
		$data['inhabited'] = $this->Corperation_Model->getInhabited($id);
		$data['customer_premise'] = $this->Corperation_Model->selectPremise($id);

		$data['res_customer'] = $this->Corperation_Model->selectOneDetail($id);
		$data['customer_premise'] = $this->Corperation_Model->selectPremise($id);
		$data['customer_temp'] = $this->Corperation_Model->selectTemp($id);

        $menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'corperation';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/corperation_detail', $data);
        $this->load->view('admin/footer');
	}

	
	public function premise($id = null)
	{
		$data['res'] = $this->Corperation_Model->selectOne($id);
        
		$menu['mainmenu'] = 'customer';
		$menu['submenu'] = 'corperation';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/corperation_premise',$data);
        $this->load->view('admin/footer');
	}
/*
	public function monthlyIncome(){
		$arr = array(
			'0'=>'รายไดไม่เกิน 12,000 บาท',
			'1'=>'12,001-15,000 บาท',
			'2'=>'15,001-30,000 บาท',
			'3'=>'30,001-50,000 บาท',
			'4'=>'50,001-80,000 บาท',
			'5'=>'80,000 บาทขึ้นไป'
		);
		return $arr;
	}

	public function career(){
		$arr = array(
			'0'=>'พนักงานประจำ',
			'1'=>'พนักงานชั่วคราว',
			'2'=>'พนักงานรายสัญญา',
			'3'=>'พนักงานรายวัน',
			'4'=>'อิสระ',
			'5'=>'เจ้าของกิจการ',
			'6'=>'อื่นๆ'
		);
		return $arr;
	}
*/

public function getToPieChart()
	{
		$contract_code = $this->input->post('contract_code');
		// $data = $this->Customer_Model->getToPieChart($contract_code);		
		// echo json_encode($data);

		$normal_duedate = null;
		if(!empty($contract_code)){
			$normal_duedate = "where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$contract_code."' and CONVERT(date,contract_installment.installment_date) <= CONVERT(date,contract_installment.extend_duedate) ";
		}

		$over_duedate = null;
		if(!empty($contract_code)){
			$over_duedate = "where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$contract_code."' and CONVERT(date,contract_installment.installment_date) > CONVERT(date,contract_installment.extend_duedate) ";
		}

		$over_due = null;
		if(!empty($contract_code)){
			$over_due = "where contract_installment.status_code = 0 and contract_installment.contract_code = N'".$contract_code."' and ( CONVERT(date,GETDATE()) > CONVERT(date,dateadd(year,-543,contract_installment.extend_duedate)) )  ";
		}

		$normal_due = null;
		if(!empty($contract_code)){
			$normal_due = "where contract_installment.status_code = 0 and contract_installment.contract_code = N'".$contract_code."' and (CONVERT(date,GETDATE()) <= CONVERT(date,dateadd(year,-543,contract_installment.extend_duedate)) ) ";
		}

		$Query = "select ";
		$Query .= " ( select COUNT(*) from contract_installment ".$normal_duedate." ) AS normal_duedate, "; 
		$Query .= " ( select COUNT(*) from contract_installment ".$over_duedate." ) AS over_duedate, "; 
		$Query .= " ( select COUNT(*) from contract_installment where contract_installment.contract_code = N'".$contract_code."' ) AS count_period, "; 
		$Query .= " ( select COUNT(*) from contract_installment where contract_installment.status_code = 1 and contract_installment.contract_code = N'".$contract_code."' ) AS count_installment, "; 
		$Query .= " ( select COUNT(*) from contract_installment ".$over_due."  ) AS over_due, ";
		$Query .= " ( select COUNT(*) from contract_installment ".$normal_due."  ) AS normal_due ";

        $Res= $this->db->query($Query);
		$data = $Res->result();

		echo json_encode($data);
	}

}
