<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Backend_controller.php';
class SerialNumber extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 

        //เรียกใช้งาน Customer_Model 
        $this->load->model('admin/ProductCategory_Model');
        $this->load->model('admin/Brand_Model');
        $this->load->model('admin/Product_Model');
        $this->load->model('admin/ProductSub_Model');
		$this->load->model('admin/Temp_Model');
        $this->load->model('admin/SerialNumber_Model');
    } 

	public function index(){
        //$data['product'] = $this->ProductSub_Model->productTocombo();

        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'serialNumber';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/serialNumber_list');
        $this->load->view('admin/footer');
	}

   
    public function create(){
        $data['TempTocombo'] = $this->SerialNumber_Model->TempTocombo();
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'serialNumber';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/serialNumber_create',$data);
        $this->load->view('admin/footer');
	}
	public function insert(){
		$product_id = $this->input->post('product_id');
		$productSub = $this->Temp_Model->GetSubByProduct($product_id);	
		foreach($productSub as $key => $items){
			$data = array( 
				'contract_code' =>  $this->input->post('contract-id'),
				'product_id' => $product_id,
				'product_sub_id' => $items->product_sub_id,
				'serial_number' => $this->input->post('serial_'.$items->product_sub_id),
				'no' => 1,
				'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			$this->SerialNumber_Model->InsertSerial($data);
		}	
        redirect('admin/serialNumber');
    }
    
 
    public function getResSerial(){
		$Search = $this->input->post('Search');
		$data = $this->SerialNumber_Model->getToList($Search);		
		echo json_encode($data);
	}
	
	public function edit($id = null){
		$data['resTemp'] = $this->SerialNumber_Model->GetContractByTemp($id);
		$data['resSerial'] = $this->SerialNumber_Model->GetSerialByTemp($id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'serialNumber';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/serialNumber_edit',$data);
        $this->load->view('admin/footer');
	}
    public function update(){
        $contract_id = $this->input->post('contract-id');
        $product_id = $this->input->post('product_id');

		$resSerial = $this->Temp_Model->GetSubByProduct($product_id);
		foreach($resSerial as $item):
			$data = array(
				'serial_number' => $this->input->post('serial_'.$item->product_sub_id),
				'udate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			$this->SerialNumber_Model->updateSerial($data, $contract_id, $product_id, $item->product_sub_id);
		endforeach;
        redirect('admin/serialNumber');
    }
	
	public function addition($id = null){
		$data['resTemp'] = $this->SerialNumber_Model->GetContractByTemp($id);
		$data['resSerial'] = $this->SerialNumber_Model->GetSerialByTemp($id);
		$data['resSubporduct'] =  $this->Temp_Model->GetSubByProduct($data['resTemp'][0]->product_id);
		
        $menu['mainmenu'] = 'home';
		$menu['submenu'] = 'serialNumber';
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/serialNumber_additio',$data);
        $this->load->view('admin/footer');
	}

	public function additional(){
		$contract_id = $this->input->post('contract-id');
		$product_id = $this->input->post('product_id');
		$productSub = $this->Temp_Model->GetSubByProduct($product_id);
		$resMaxSerial = $this->SerialNumber_Model->GetMaxSerialByTemp($contract_id);
		foreach($productSub as $key => $items){
			$data = array( 
				'contract_code' =>  $this->input->post('contract-id'),
				'product_id' => $product_id,
				'product_sub_id' => $items->product_sub_id,
				'serial_number' => $this->input->post('serial_'.$items->product_sub_id),
				'remark' => $this->input->post('remark_'.$items->product_sub_id),
				'no' => $resMaxSerial[0]->no + 1,
				'cdate'=> date(date_format(date_create(),"Y-m-d H:i:s"))
			);
			
			$this->SerialNumber_Model->InsertSerial($data);
		}
	
        redirect('admin/serialNumber');
    }

    public function DeleteProductSub(){
        $id = $this->input->post('id');
		$data = $this->ProductSub_Model->DeleteProductSub($id);
		echo json_encode($data);
    }

    

}
