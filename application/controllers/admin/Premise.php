<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Premise extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 
		date_default_timezone_set("Asia/Bangkok"); // set timeZone
		//เรียกใช้งาน library
		//$this->load->library('mpdf');
		//$this->load->library('M_pdf');
		//$this->load->library('pdf');
		//$this->load->library('excel');

        //เรียกใช้งาน Customer_Model     
        $this->load->model('admin/Status_Model');
		$this->load->model('admin/Mechanic_Model');
        $this->load->model('admin/Agent_Model');
		$this->load->model('admin/Premise_Model');
    } 

	public function insertpremise(){
	
        
		$config['allowed_types'] = 'jpeg|jpg|png'; //รูปแบบไฟล์ที่ อนุญาตให้ Upload ได้
		$config['max_size']      = 0; //ขนาดไฟล์สูงสุดที่ Upload ได้ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
		$config['max_width']     = 0; //ขนาดความกว้างสูงสุด (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
		$config['max_height']    = 0;  //ขนาดความสูงสูงสดุ (กรณีไม่จำกัดขนาด กำหนดเป็น 0)
		$config["overwrite"] = 	true;
		//$config['encrypt_name']  = true; //กำหนดเป็น true ให้ระบบ เปลียนชื่อ ไฟล์  อัตโนมัติ  ป้องกันกรณีชื่อไฟล์ซ้ำกัน
		
		$refcode = $this->input->post('refcode');
		$element_id = $this->input->post('elementID');
		$mode = $this->input->post('mode');
		$no = $this->input->post('no');
		$files = "input-".$element_id.$no;
		
		$premise = array(
			'ref_code' =>$refcode,  
			'mode' => $mode, 
			'element_id' => $element_id,
			'no' => $no,
			'cdate'=>date(date_format(date_create(),"Y-m-d H:i:s"))
		); 

		##### name img ####
		if (!empty($_FILES[$files]['name'])) {
			$_FILES[$files]['name'] = $element_id.$no.".".pathinfo($_FILES[$files]['name'], PATHINFO_EXTENSION);
			$premise['img'] = $_FILES[$files]['name'];
		}

		##### create dir ####
		/*
		$mainDir = "./uploaded/premise/".$mode;
		if (!file_exists($mainDir)) {
			if (!mkdir($mainDir, 0777, true)) {//0777
				die($mainDir.' Failed to create folders...');
			}
        }

		$idDir = $mainDir."/".$refcode;
		if (!file_exists($idDir)) {
			if (!mkdir($idDir, 0777, true)) {//0777
				die($idDir.' Failed to create folders...');
			}
        }

		$subDir = $idDir."/".$element_id;
		if (!file_exists($subDir)) {
			if (!mkdir($subDir, 0777, true)) {//0777
				die($subDir.' Failed to create folders...');
			}
        }
		*/
		##### create path ####
		$upload_path = "./uploaded/premise/".$mode."/".$refcode."/".$element_id;
		if (!file_exists($upload_path)) {
			if (!mkdir($upload_path, 0777, true)) {//0777
				die($upload_path.' Failed to create folders...');
			}
        }
		
		$fullPath = $upload_path.'/'.$premise['img'];
		$premise['path'] = $fullPath;

		##### create img to dir ####
		$config['upload_path'] = $upload_path; //Folder สำหรับ เก็บ ไฟล์ที่  Upload
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload($files)) {
			$path = $this->upload->data();
		}else{
			echo $this->upload->display_errors();
			exit();
		}
	
		### insert premise ###
		$this->Premise_Model->insertPremis($premise); 
		
		### redirect to list ###
        $redirected = 'admin/'.$mode.'/premise/'.$refcode;
		redirect(base_url($redirected));
		//redirect('admin/agent/premise/'.$refcode);
    }

	public function getpremise()
	{
		$id = $this->input->post('ref_code');
		$elementID = $this->input->post('element_id');
		$data = $this->Premise_Model->getPremise($id, $elementID);		
		echo json_encode($data);
	}

	public function delpremise()
	{
		$premise_code = $this->input->post('premise_code');
		$path = $this->input->post('path');
		$url = $this->input->post('url');
		$res = $this->Premise_Model->delPremis($premise_code, $path);	
		
		$data = array();
		$data['status'] = $res;
		$data['path'] = $url;
		echo json_encode($data); //return
	}


}
