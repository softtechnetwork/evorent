<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ContractRemark extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct() { 
    
        parent::__construct(); 
		// login     
		if(!$this->session->userdata('isUserLoggedIn')){
			redirect(base_url('admin/user/login'));
		}
     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
        $this->load->helper('file'); 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 
		date_default_timezone_set("Asia/Bangkok"); // set timeZone
		//เรียกใช้งาน library
		//$this->load->library('mpdf');
		//$this->load->library('M_pdf');
		//$this->load->library('pdf');
		//$this->load->library('excel');

        //เรียกใช้งาน Customer_Model     
        // $this->load->model('admin/Status_Model');
		// $this->load->model('admin/Mechanic_Model');
        // $this->load->model('admin/Agent_Model');
    } 

	public function index()
	{
		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractRemark';
        $data['contract'] = $this->db->query("SELECT contract_code FROM contract")->result();
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_remark_list',$data);
        $this->load->view('admin/footer');
	}
	public function get_resulted()
	{
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT * FROM contract_remark ORDER BY update_date DESC ";
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
	public function get_result_onc()
	{
        $id = $this->input->post('id');
		$curent_date = date('Y-m-d H:i:s');
		$str_query = "SELECT * FROM contract_remark WHERE id='".$id."'";
		$Res= $this->db->query($str_query);
		$data = $Res->result();
		$res['datas'] = $data;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
		echo json_encode($res);
	}
    
	public function create()
	{
		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractRemark';
        $data['id'] = null;
		$sql = "SELECT contract.contract_code, contract.customer_code, customer.firstname+' '+customer.lastname as fullname    
		FROM contract 
		LEFT JOIN customer ON contract.customer_code = customer.customer_code";
        $data['contract'] = $this->db->query($sql)->result();
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_remark_create',$data);
        $this->load->view('admin/footer');
	}
    
	public function edit($id)
	{
		$menu['mainmenu'] = 'contract';
		$menu['submenu'] = 'contractRemark';
        $data['id'] = $id;
        $sql = "SELECT contract.contract_code, contract.customer_code, customer.firstname+' '+customer.lastname as fullname    
		FROM contract 
		LEFT JOIN customer ON contract.customer_code = customer.customer_code";
        $data['contract'] = $this->db->query($sql)->result();
        $this->load->view('admin/header',$menu);
		$this->load->view('admin/contract_remark_edit',$data);
        $this->load->view('admin/footer');
	}
    
	public function actions(){
        $res = new stdClass();
        $curent_date = Date('Y-m-d H:i:s');
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		$contract_code = $this->input->post('contract_code');
		$remark = $this->input->post('remark');

        $obj = array( 
			'contract_code' => $contract_code, 
			'remark' => $remark,
            'admin' =>  $this->session->userdata('userName')
		); 
		// print_r($obj); exit();

		//###  INSERT ###//
        if($action == 'insert'){
			$obj['create_date'] = $curent_date;
			$obj['update_date'] = $curent_date;
			$this->db->insert('contract_remark', $obj);
   			$id = $this->db->insert_id();

			$res->status = true;
			$res->datas = base_url('admin/ContractRemark');
			$res->massege = 'บันทึกสำเร็จ';
			$res->status_code = '000';
        }

		//###  UPDATE ###//
        if($action == 'update'){
			$obj['update_date'] = $curent_date;
            $this->db->where("id", $id);
            $this->db->update("contract_remark", $obj); 
			
			$res->status = true;
			$res->datas = base_url('admin/ContractRemark');
			$res->massege = 'บันทึกสำเร็จ';
			$res->status_code = '000';
        }

		// $res->status = true;
		// $res->datas = base_url('admin/course');
		// $res->massege = 'บันทึกสำเร็จ';
		// $res->status_code = '000';
		echo json_encode($res);
	}

















	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	}

	public function converYYToyy($date = null)
	{
		$yy = ((int)date("Y")+543);
		$mm = (date("m"));
		$dd = (date("d"));
		return $dd.'/'.$mm.'/'.$yy;
	}

	public function RemoveText($text, $wording) {
		$string = '';
		if(!empty($wording)){
			$temp = explode($text, $wording);
			$string = $temp[1];
		}
		return $string ;
	 }

}
