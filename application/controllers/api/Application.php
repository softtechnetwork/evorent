<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');
//class Application extends CI_Controller {
class Application extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    
        parent::__construct(); 

		$this->load->library('session');
		$this->load->library('functions');
		
		// login     
		// if(!$this->session->userdata('isUserLoggedIn')){
		// 	redirect(base_url('admin/user/login'));
		// }

		date_default_timezone_set("Asia/Bangkok"); // set timeZone

        //set header
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header('Content-Type: application/json');
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

     	//เรียกใช้งาน Class helper     
        $this->load->helper('url'); 
      	$this->load->helper('form');
		$this->load->helper('file');
        $this->load->helper('date'); 
 
     	//เรียกใช้งาน Class database     
        $this->load->database(); 
        $this->authen = $this->load->database('authen', TRUE);

        //เรียกใช้งาน Model  
        $this->load->model('api/Contracts');
        $this->load->model('api/Profile');
    } 


    // for test
    public function get_test_post(){  
    
        $obj = [];
        $obj['contract_code'] = 5;
        $obj['contract_status'] = 5;

        //print_r($res_); echo '----';
        
        $res['result_code'] = '200';
        $res['datas'] = $obj;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
        
        echo json_encode( $res );
        //$this->response($res);
        exit();
    }

	public function login_post(){   

        $res = $this->functions->respons();
        $id_card = $this->input->post('id_card');
        $tel = $this->input->post('tel');

        if(empty($id_card) || empty($tel)){
            $res['code'] = '000';
            $res['result_code'] = '302';
            $res['status'] = true;
            $res['response'] = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $res['messsage'] = 'Unsuccess fully.';

            echo json_encode( $res );
            exit();
            //$this->response($res);
        }
        
        $res_contract = $this->report_user($id_card, $tel);
		if($res_contract){
            $gen_authen = $this->functions->encode_authen($id_card, $tel);
            $report_authen = $this->report_authen($id_card);
            if($report_authen){
                $this->update_authen($gen_authen);
            }else{
                $this->insert_authen($gen_authen);
            }

            $res_authen = $this->report_authen($id_card);
            $expire = $this->report_expire_authen($res_authen[0]->authen, 'login');
            
            echo json_encode( $expire );
            exit();
            //$this->response($expire);
		}else{
			$res['code'] = "001";
            $res['status'] = false;
            $res['result_code'] = '302';
            $res['response'] = 'กรุณาตรวจสอบเลขบัตรประชาชน หรือ เบอร์โทร';
            $res['messsage'] = 'Unsuccess fully.';
            
            echo json_encode( $res );
            exit();
            //$this->response($res);
		}
    }
    public function get_contract_post(){  
        $auth = $this->authentication();

        $res = $this->functions->respons();
        $id_card = $auth->datas->idcard;

        if(empty($id_card)){
            $res->code = '000';
            $res->result_code = '302';
            $res->status = true;
            $res->response = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $res->messsage = 'Unsuccess fully.';
            $this->response($res);
            exit();
        }
        
        $res_contract = $this->Contracts->res_contract($id_card);
        $res_ = [];
        foreach($res_contract as $item){
			//$item->unpaid_balance =  number_format((float)$item->unpaid_balance, 0, '.', ',');
			//$item->overdue_balance =  number_format((float)$item->overdue_balance, 0, '.', ',');

            $item->img =  '';
            $img = $this->get_product_image($item->contract_code, $item->product_id);
            if($img){
                $item->img =  base_url($img[0]->path);
            }
            
            $obj = new stdClass();
            $obj->contract_code = $item->contract_code;
            $obj->contract_status = $item->contract_status;
            $obj->contract_status_id = $item->contract_status_id;
            $obj->overdue_period = (string)$item->overdue_period;
            $obj->overdue_balance = number_format((float)$item->overdue_balance, 0, '.', ',');
            $obj->product_name = $item->product_name;
            $obj->cate_name = $item->cate_name;
            $obj->img = $item->img;

            array_push($res_, $obj);
		}

        $res['datas'] = $res_;
        $res['result_code'] = '200';
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';

        echo json_encode( $res );
        //$this->response($res);
        exit();
    }
    public function get_product_image($contract_code, $product_id){ 
        $curent_date = date('Y-m-d H:i:s'); 
        $Query = "   SELECT img.*, [contract].contract_code FROM [contract] 
        INNER JOIN img ON contract.product_id = img.ref_id 
        WHERE  contract.contract_code = '".$contract_code."' and img.ref_id = '".$product_id."' AND master_cate ='product' and img_cate_id = '4'  ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;  
    }
	public function get_contract_code_post(){   
        $auth = $this->authentication();

        $res = $this->functions->respons();
        $id_card = $auth->datas->idcard;
        $contract_code = $this->input->post('contract_code');

        if(empty($id_card) || empty($contract_code)){
            $res->code = '000';
            $res->status = true;
            $res->result_code = '302';
            $res->response = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $res->messsage = 'Unsuccess fully.';
            $this->response($res);
            exit();
        }
        
        $res_contract = $this->Contracts->res_contract_code($id_card , $contract_code);
        $obj = new stdClass();
		foreach($res_contract as $item){
			$item->unpaid_balance =  number_format((float)$item->unpaid_balance, 0, '.', ',');
			$item->overdue_balance =  number_format((float)$item->overdue_balance, 0, '.', ',');
            $item->img = '';
            $img = $this->get_product_image($item->contract_code, $item->product_id);
            if($img){
                $item->img =  base_url($img[0]->path);
            }

            $obj->contract_code = $item->contract_code;
            $obj->contract_status = $item->contract_status;
            $obj->contract_status_id = $item->contract_status_id;
            $obj->rental_period = (string)$item->rental_period;
            $obj->paided = (string)$item->paided;
            $obj->unpaided = (string)$item->unpaided;
            $obj->unpaid_balance = $item->unpaid_balance;
            $obj->overdue_balance = $item->overdue_balance;
            $obj->overdue_period = number_format((float)$item->overdue_period, 0, '.', ',');
            $obj->product_name = $item->product_name;
            $obj->cate_name = $item->cate_name;
            $obj->img = $item->img;
		}
        $res['datas'] = $obj;
        //$res['datas'] = $res_contract[0];
        $res['result_code'] = '200';
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
        //$this->response($res);
        
        echo json_encode( $res );
        exit();
    }
    public function get_contract_installments_post(){   
        $auth = $this->authentication();
        
        $res = $this->functions->respons();
        $id_card = $auth->datas->idcard;
        //$customer_code = $this->input->post('customer_code');
        $contract_code = $this->input->post('contract_code');

        if(empty($contract_code)){
            $res->code = '000';
            $res->status = true;
            $res->result_code = '302';
            $res->response = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $res->messsage = 'Unsuccess fully.';
            $this->response($res);
            exit();
        }
        
        $contract_installments = $this->Contracts->res_contract_installments($id_card, $contract_code);
        $curent_date = date("Y-m-d",strtotime(date('Y')+543));
        $res_ = [];
		foreach($contract_installments as $item){
            $obj = new stdClass();
            if(strtotime($item->payment_duedate) <= strtotime($curent_date)){
                // $item->id = (string)$item->id;
                // $item->period = sprintf("%02d", $item->period );
                // $item->payment_amount =  number_format($item->payment_amount, 0, '.', ',');
                // $item->amount_deff =  number_format($item->amount_deff, 0, '.', ',');
                // $item->installment_payment =  number_format((float)$item->installment_payment, 0, '.', ',');
                // $item->status = ($item->status_code == 1) ? 'ชำระแล้ว' : 'ยังไม่ชำระ';
                // $item->status_code = (string)$item->status_code;
                // $item->payment_duedate_thai = $this->functions->DateThai($item->payment_duedate);


                $obj->id = (string)$item->id;
                $obj->contract_code = $item->contract_code;
                $obj->product_id = $item->product_id;
                $obj->customer_code = $item->customer_code;
                $obj->period = sprintf("%02d", $item->period );
                $obj->installment_payment =  number_format((float)$item->installment_payment, 0, '.', ',');
                $obj->payment_duedate = $item->payment_duedate;
                $obj->extend_duedate = $item->extend_duedate;
                $obj->payment_amount =  number_format($item->payment_amount, 0, '.', ',');
                $obj->amount_deff =  number_format($item->amount_deff, 0, '.', ',');
                $obj->installment_date = $item->installment_date;
                $obj->status = ($item->status_code == 1) ? 'ชำระแล้ว' : 'ยังไม่ชำระ';
                $obj->status_code = (string)$item->status_code;
                $obj->remark = $item->remark;
                $obj->cdate = $item->cdate;
                $obj->udate = $item->udate;
                $obj->payment_duedate_thai = $this->functions->DateThai($item->payment_duedate);
                array_push($res_, $obj);
            }
		}
        //$res['datas'] = $contract_installments;
        $res['datas'] = $res_;
        $res['result_code']= '200';
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
        //$this->response($res);
        
        echo json_encode( $res );
    }
    public function get_contract_installments_all_post(){   
        $auth = $this->authentication();
        
        $res = $this->functions->respons();
        $id_card = $auth->datas->idcard;
        //$customer_code = $this->input->post('customer_code');
        $contract_code = $this->input->post('contract_code');

        if(empty($contract_code)){
            $res->code = '000';
            $res->status = true;
            $res->result_code = '302';
            $res->response = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $res->messsage = 'Unsuccess fully.';
            $this->response($res);
            exit();
        }
        
        $contract_installments = $this->Contracts->res_contract_installments_all($contract_code);
        $curent_date = date("Y-m-d",strtotime(date('Y')+543));
        $res_ = [];
		foreach($contract_installments as $item){
            $obj = new stdClass();
            //if(strtotime($item->payment_duedate) <= strtotime($curent_date)){
                // $item->id = (string)$item->id;
                // $item->period = sprintf("%02d", $item->period );
                // $item->payment_amount =  number_format($item->payment_amount, 0, '.', ',');
                // $item->amount_deff =  number_format($item->amount_deff, 0, '.', ',');
                // $item->installment_payment =  number_format((float)$item->installment_payment, 0, '.', ',');
                // $item->status = ($item->status_code == 1) ? 'ชำระแล้ว' : 'ยังไม่ชำระ';
                // $item->status_code = (string)$item->status_code;
                // $item->payment_duedate_thai = $this->functions->DateThai($item->payment_duedate);


                $obj->id = (string)$item->id;
                $obj->contract_code = $item->contract_code;
                $obj->product_id = $item->product_id;
                $obj->customer_code = $item->customer_code;
                $obj->period = sprintf("%02d", $item->period );
                $obj->installment_payment =  number_format((float)$item->installment_payment, 0, '.', ',');
                $obj->payment_duedate = $item->payment_duedate;
                $obj->extend_duedate = $item->extend_duedate;
                $obj->payment_amount =  number_format($item->payment_amount, 0, '.', ',');
                $obj->amount_deff =  number_format($item->amount_deff, 0, '.', ',');
                $obj->installment_date = $item->installment_date;
                //$obj->status = ($item->status_code == 1) ? 'ชำระแล้ว' : 'ยังไม่ชำระ';
                $obj->overdue_text = $item->overdue;
                $obj->status = $item->label;
                $obj->status_code = (string)$item->status_code;
                $obj->remark = $item->remark;
                $obj->cdate = $item->cdate;
                $obj->udate = $item->udate;
                $obj->payment_duedate_thai = $this->functions->DateThai($item->payment_duedate);
                array_push($res_, $obj);
            //}
		}
        //$res['datas'] = $contract_installments;
        $res['datas'] = $res_;
        $res['result_code'] = '200';
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
        //$this->response($res);
        
        echo json_encode( $res );
    }
    public function get_profile_post()
    {   
        $auth = $this->authentication();
        $res = $this->functions->respons();
        $id_card = $auth->datas->idcard;
        
        $res_profile = $this->Profile->res_profile($id_card);
        if($res_profile){
            $res_address = $this->Profile->res_address($res_profile[0]->customer_code);
            foreach($res_address as $item){
                $item->province_id = (string)$item->province_id;
                $item->amphurs_id = (string)$item->amphurs_id;
                $item->district_id = (string)$item->district_id;
                $item->addr = (string)$item->addr;
            }
            $res_profile[0]->id = (string)$res_profile[0]->id;
            $res_profile[0]->career = (string)$res_profile[0]->career;
            $res_profile[0]->monthly_income = number_format((float)$res_profile[0]->monthly_income, 0, '.', ',');;
            $res_profile[0]->status = (string)$res_profile[0]->status;
            $res_profile[0]->address = $res_address;
        }
        
        $res['result_code'] = '200';
        $res['datas'] = $res_profile;
        $res['response'] = 'ค้นหาข้อมูลสำเร็จ';
        //$this->response($res);
        
        echo json_encode( $res );
        exit();
    }

    // #### Device token ###/
    public function update_device_token_post(){   
        $res = $this->functions->respons();
        $customer_code = $this->input->post('customer_code');
        $device_token = $this->input->post('device_token');
        $os = $this->input->post('os');
        $device = $this->report_device($customer_code);
		if($device){
            //if($device[0]->device_token != $device_token){
                $this->update_device($customer_code, $device_token, $os);
            //}
            $res['code'] = "000";
            $res['status'] = true;
            $res['result_code'] = '200';
            $res['response'] = 'อัพเดต device token สำเร็จ';
            $res['messsage'] = 'success fully.';
		}else{
            $this->insert_device($customer_code, $device_token,$os);
			$res['code'] = "000";
            $res['status'] = true;
            $res['result_code'] = '200';
            $res['response'] = 'เพิ่ม device token สำเร็จ';
            $res['messsage'] = 'success fully.';
		}
        
        echo json_encode( $res );
        exit();
    }
    public function report_device($customer_code){  
        $curent_date = date('Y-m-d H:i:s'); 
        $Query = " SELECT * FROM device_token WHERE customer_code ='".$customer_code."' ";
		$Res= $this->authen->query($Query);
		$data = $Res->result();
	    return $data;
    }
    public function insert_device($customer_code, $device_token, $os){  
        $curent_date = date('Y-m-d H:i:s'); 
        $Query = " INSERT INTO device_token (device_token, os,  customer_code, create_date, update_date) VALUES ('".$device_token."','".$os."','".$customer_code."','".$curent_date."','".$curent_date."' )";
		$Res= $this->authen->query($Query);
		$data = $Res;
	    return $data;
    }
    public function update_device($customer_code, $device_token, $os){  
        $curent_date = date('Y-m-d H:i:s'); 
        $Query = " UPDATE device_token SET device_token = '".$device_token."' ,update_date = '".$curent_date."', os = '".$os."' WHERE customer_code = '".$customer_code."' ";
        $Res= $this->authen->query($Query);
		$data = $Res;
	    return $data;
    }


    // #### Authentication ###/
    public function report_user($id_card, $tel){  
        $curent_date = date('Y-m-d H:i:s'); 
        $Query = " SELECT * FROM customer WHERE idcard ='".$id_card."' AND tel ='".$tel."'  ";
		$Res= $this->db->query($Query);
		$data = $Res->result();
	    return $data;
    }
    public function report_authen($id_card){  
        $curent_date = date('Y-m-d H:i:s');
        $Query = " SELECT * FROM authent WHERE idcard ='".$id_card."' ";
		$Res= $this->authen->query($Query);
		$data = $Res->result();
	    return $data;
    }
    public function update_authen($authen){  
        $curent_date = date('Y-m-d H:i:s'); 
        $decode = $this->functions->decode_authen($authen);
        $Query = " UPDATE [dbo].[authent] SET [authen] = '".$authen."' ,[udate] = '".$curent_date."' WHERE idcard = '".$decode->idcard."' ";
        $Res= $this->authen->query($Query);
		$data = $Res;
	    return $data;
    }
    public function insert_authen($authen){  
        $curent_date = date('Y-m-d H:i:s'); 
        $decode = $this->functions->decode_authen($authen);
        $Query = " INSERT INTO [dbo].[authent] ([device_id],[idcard],[authen],[facebook],[email],[cdate]) VALUES (null,'".$decode->idcard."','".$authen."',null, null, '".$curent_date."')";
		$Res= $this->authen->query($Query);
		$data = $Res;
	    return $data;
    }
    public function report_expire_authen($token, $method = null){
        
        $res_authen = $this->functions->decode_authen($token);
        
        $res = new stdClass();
        $obj = new stdClass();
        $obj->authen = $token;
        $obj->idcard = $res_authen->idcard;
        $obj->tel = $res_authen->tel;

        if($method == 'login'){
            $res->status = true;
            $res->result_code = '200';
            $res->datas = $obj;
            $res->response = 'เข้าสู่ระบบสำเร็จ';
            $res->messsage = 'success fully.';
        }else{
            $expire_date = $res_authen->expire;
            $cdate_authen = new DateTime(date('Y-m-d H:i:s'));
            $expire_date = new DateTime($res_authen->expire);
            if($cdate_authen < $expire_date){
                $res->status = true;
                $res->result_code = '200';
                $res->datas = $obj;
                $res->response = 'เข้าสู่ระบบสำเร็จ';
                $res->messsage = 'success fully.';
            }else{
                $res->status = false;
                $res->result_code = '200';
                $res->response = 'กรุณาเข้าสู่ระบบ';
                $res->messsage = 'unsuccess fully.';
            }
        }
        return $res;
    }
    public function authentication(){  
        $res = new stdClass();
        $reportAuth = $this->functions->report_authentication();
        if($reportAuth->status){
            $res = $this->report_expire_authen($reportAuth->token);
            if(!$res->status){
                $this->response($res);
                exit();
            }else{
                return $res;
            }
        }else{
            $res->code = '000';
            $res->status = true;
            $res->response = 'XXXXXX';
            $res->messsage = 'Unsuccess fully.';
            $this->response($res);
            exit();
        }
    }

}?>
