<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

require_once(APPPATH. "libraries/Technicial/SmsLibrary.php");

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class SmsModule extends REST_Controller {

    function __construct(){
        // Construct the parent class
        //echo '5';exit();
        parent::__construct();
        
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function OcsSendSMS_post(){
        
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');


                    // $log_file_path = $this->createLogFilePath('request ocs send sms');
                    // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($request) . "\n";
                    // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                    // unset($file_content);

                    //echo sha1('123456');exit;

                    
                    $this->load->library('requestAuthenOcsAPI');

                    
                    
                    $requestAuthenAPI = requestAuthenOcsAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('OcsSendSMS');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "error";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        // echo json_encode($error);

                        // http_response_code(400);
                        $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__ocsSendSMSAnts($requestCriteria);
                        // echo json_encode($output);
                        // http_response_code(200);


                        $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
            
        }

    }


    public function TestSendSmsV2_post(){
        
        if($this->post('request') || file_get_contents('php://input')){
            
            $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

            
            $this->load->library('requestAuthenOcsAPI');

            
           
            $requestAuthenAPI = requestAuthenOcsAPI::get_instance();
            $requestCriteria = new stdClass();
            $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

            $requestAuthenAPI->set_requireMethod('OcsSendSMS');
            if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                $error = array();
                $error['status'] = false;
                $error['result_code'] = "error";
                $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                
                $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
            }else{

                $output = $this->__TestSendSmsV2($requestCriteria);
                $this->set_response($output,  REST_Controller::HTTP_OK);

            }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
            
        }
    }

      public function sendnotifymessage_post(){
        
        if($this->post('request') || file_get_contents('php://input')){
            
            $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

            
            $this->load->library('requestAuthenOcsAPI');

            
           
            $requestAuthenAPI = requestAuthenOcsAPI::get_instance();
            $requestCriteria = new stdClass();
            $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

            $requestAuthenAPI->set_requireMethod('OcsSendSMS');
            if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                $error = array();
                $error['status'] = false;
                $error['result_code'] = "error";
                $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                
                $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
            }else{

                $output = $this->__sendnotifymessage($requestCriteria);
                $this->set_response($output,  REST_Controller::HTTP_OK);

            }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
            
        }
    }


    public function OcsSendSMSV2_post(){
        
        if($this->post('request') || file_get_contents('php://input')){
            
            $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

            
            $this->load->library('requestAuthenOcsAPI');

            
            
            $requestAuthenAPI = requestAuthenOcsAPI::get_instance();
            $requestCriteria = new stdClass();
            $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

            $requestAuthenAPI->set_requireMethod('OcsSendSMS');
            if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                $error = array();
                $error['status'] = false;
                $error['result_code'] = "error";
                $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                
                $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
            }else{

                $output = $this->__ocsSendSMSAntsV2($requestCriteria);
                $this->set_response($output,  REST_Controller::HTTP_OK);

            }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
            
        }
    }

    public function TestOcsSendSMS_get(){
        $request = $this->input->get();

        //print_r($request);exit;
    }

    public function OcsSendSMSAnts_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');


                    // $log_file_path = $this->createLogFilePath('request ocs send sms');
                    // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($request) . "\n";
                    // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                    // unset($file_content);

                    //echo sha1('123456');exit;

                    
                    $this->load->library('requestAuthenOcsAPI');





                    
                    $requestAuthenAPI = requestAuthenOcsAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('OcsSendSMSAnts');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "error";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        // echo json_encode($error);

                        // http_response_code(400);
                        $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__ocsSendSMSAnts($requestCriteria);
                        // echo json_encode($output);
                        // http_response_code(200);


                        $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
            
        }
    }


    private function __ocsSendSMS($requestCriteria){
        $output = array();
        $SmsLibrary = new SmsLibrary();
        $output = $SmsLibrary->ocsSendSMS($requestCriteria);
        return $output;
    }
    private function __ocsSendSMSAnts($requestCriteria){
        $output = array();
        $SmsLibrary = new SmsLibrary();
        $output = $SmsLibrary->ocsSendSMSAnts($requestCriteria);
        return $output;
    }

    private function __ocsSendSMSAntsV2($requestCriteria){
        $output = array();
        $SmsLibrary = new SmsLibrary();
        $output = $SmsLibrary->ocsSendSMSAntsV2($requestCriteria);
        return $output;
    }
    
    
    private function __TestSendSmsV2($requestCriteria){
        $output = array();
        $SmsLibrary = new SmsLibrary();
        $output = $SmsLibrary->TestSendSmsV2($requestCriteria);
        return $output;
    }

    private function __sendnotifymessage($requestCriteria){
        $output = array();
        $SmsLibrary = new SmsLibrary();
        $output = $SmsLibrary->sendnotifymessage($requestCriteria);
        return $output;
    }

    private function createLogFilePath($filename = '') {
        $log_path = './application/logs/criteria_log';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }
}