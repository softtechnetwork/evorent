<?php


class SmsLibrary{

    private $ci;
    private $crm_db_conn = "";
    private $sms_config = array();
    private $sms_config_ants = array();
    function __construct() {
        $this->ci =& get_instance();
        $this->ci->load->config('sms');
        $this->sms_config = $this->ci->config->item('SMSMKT');

        $this->sms_config_ants = $this->ci->config->item('ANTS');

        // return array(
      //   'status'=>false,
      //   'result_code'=>'error',
      //   'result_desc'=>'Can not send sms,Please try again'
      // );
        //$this->ci->load->helper(array('technicial'));
        //print_r($this->ci->config->item('SMSMKT'));
       
    }

   	public function ocsSendSMS($requestCriteria){
      // return array(
      //   'status'=>false,
      //   'result_code'=>'error',
      //   'result_desc'=>'Can not send sms,Please try again'
      // );

   		if(property_exists($requestCriteria, "test") && $requestCriteria->test == true){
   			print_r($requestCriteria);exit;
   		}
   		//exit;
   		//print_r($requestCriteria);exit;
   		$data = array(
   			'message'=>$requestCriteria->message.$requestCriteria->otp_code,
   			'telephone_number'=>$requestCriteria->telephone_number
   		);

   		if($this->curlSendSMSMKT($data)){
   			return array(
   				'status'=>true,
   				'result_code'=>'success',
   				'result_desc'=>'Send sms success'
   			);
   		}else{
   			return array(
   				'status'=>false,
   				'result_code'=>'error',
   				'result_desc'=>'Can not send sms,Please try again'
   			);
   		}

   		//print_r($data);
   	}

    public function ocsSendSMSAnts($requestCriteria){
      //print_r($requestCriteria);exit;
      if(property_exists($requestCriteria, "test") && $requestCriteria->test == true){
        print_r($requestCriteria);exit;
      }

      $telephone_number = $requestCriteria->telephone_number;

      if(substr($telephone_number, 0,1) == '0'){
          $telephone_number = '66'.substr($telephone_number, 1,9);
      }

      $data = array(
        'from'=>$this->sms_config_ants['Sender'],
        'to'=>$telephone_number,
        'text'=>$requestCriteria->message.$requestCriteria->otp_code
      );

      if($this->curlSendSMSAnts($data)){
        return array(
          'status'=>true,
          'result_code'=>'success',
          'result_desc'=>'Send sms success'
        );

      }else{
        return array(
          'status'=>false,
          'result_code'=>'error',
          'result_desc'=>'Can not send sms,Please try again'
        );

      }


    }


    
    public function sendnotifymessage($requestCriteria){
    
  
       $telephone_number = $requestCriteria->telephone_number;
       //echo $telephone_number;exit();
       if(substr($telephone_number, 0,1) == '0'){
           $telephone_number = '66'.substr($telephone_number, 1,9);
       }
       
       $data = array(
         'from'=>$this->sms_config_ants['Sender'],
         'to'=>$telephone_number,
         'text'=>$requestCriteria->message
       );
 
       $data = [
         "messages" => [
           [
             "from" => $this->sms_config_ants['Sender'],
             "destinations" => [
               [
                 "to" => $telephone_number
               ]
             ],
             "text" => $requestCriteria->message
           ]
         ]
       ];
 
       // echo json_encode($data);exit;
 
       if($this->curlSendSMSAntsV2($data)){
         return array(
           'status'=>true,
           'result_code'=>'success',
           'result_desc'=>'Send sms success'
         );
 
       }else{
         return array(
           'status'=>false,
           'result_code'=>'error',
           'result_desc'=>'Can not send sms,Please try again'
         );
 
       }
     }
     
     public function TestSendSmsV2($requestCriteria){
    
   
      $telephone_number = $requestCriteria->telephone_number;
      //echo $telephone_number;exit();
      if(substr($telephone_number, 0,1) == '0'){
          $telephone_number = '66'.substr($telephone_number, 1,9);
      }
      


      $data = array(
        'from'=>$this->sms_config_ants['Sender'],
        'to'=>$telephone_number,
        'text'=>$requestCriteria->message.$requestCriteria->otp_code
      );

      $data = [
        "messages" => [
          [
            "from" => $this->sms_config_ants['Sender'],
            "destinations" => [
              [
                "to" => $telephone_number
              ]
            ],
            "text" => $requestCriteria->message.$requestCriteria->otp_code
          ]
        ]
      ];

      // echo json_encode($data);exit;

      if($this->curlSendSMSAntsV2($data)){
        return array(
          'status'=>true,
          'result_code'=>'success',
          'result_desc'=>'Send sms success'
        );

      }else{
        return array(
          'status'=>false,
          'result_code'=>'error',
          'result_desc'=>'Can not send sms,Please try again'
        );

      }
    }

    public function ocsSendSMSAntsV2($requestCriteria){
     // echo '5';exit();

      $telephone_number = $requestCriteria->telephone_number;
      //echo $telephone_number;exit();
      if(substr($telephone_number, 0,1) == '0'){
          $telephone_number = '66'.substr($telephone_number, 1,9);
      }

      $data = array(
        'from'=>$this->sms_config_ants['Sender'],
        'to'=>$telephone_number,
        'text'=>$requestCriteria->message.$requestCriteria->otp_code
      );

      $data = [
        "messages" => [
          [
            "from" => $this->sms_config_ants['Sender'],
            "destinations" => [
              [
                "to" => $telephone_number
              ]
            ],
            "text" => $requestCriteria->message.$requestCriteria->otp_code
          ]
        ]
      ];

      // echo json_encode($data);exit;

      if($this->curlSendSMSAntsV2($data)){
        return array(
          'status'=>true,
          'result_code'=>'success',
          'result_desc'=>'Send sms success'
        );

      }else{
        return array(
          'status'=>false,
          'result_code'=>'error',
          'result_desc'=>'Can not send sms,Please try again'
        );

      }
    }

   	private function curlSendSMSMKT($data = array()){

   		//print_r($data);exit;
   		//return true;
   		//print_r($this->sms_config);exit;

   		$Username   = $this->sms_config['Username'];                 
        $Password   = $this->sms_config['Password'];                 
        $PhoneList  = $data['telephone_number'];
        $Message        = urlencode($data['message']);
        //¶éÒä¿Åì à»ç¹ UTF-8 ãËéá¡éä¢´Ñ§¹Õé  $Message   =urlencode(iconv("UTF-8","TIS-620","·´ÊÍº¡ÒÃÊè§¢éÍ¤ÇÒÁ¼èÒ¹ API ´éÇÂÀÒÉÒ PHP"));
        $Sender     = $this->sms_config['Sender'];
        
        $Parameter  =   "User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";

        switch (ENVIRONMENT) {
        	case 'development':
        		$API_URL = "http://member.smsmkt.com/SMSLink/SendMsg/index.php";
        	break;
        	case 'production':
        		$API_URL = "https://member.smsmkt.com/SMSLink/SendMsg/index.php";
        	break;
        	
        	default:
        		# code...
        	break;
        }

        $ch = curl_init();   
        curl_setopt($ch,CURLOPT_URL,$API_URL);

        if(ENVIRONMENT == 'production'){
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch,CURLOPT_POST,1); 
        curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);
        
        $Result = curl_exec($ch);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);

        $exResult = explode(',', $Result);

        $strStatus = $exResult[0];

        if($strStatus == 'Status=0'){
        	return true;
        }else{
        	return false;
        }


   	}

    private function curlSendSMSAnts($data = array()){

      
      
        $authorize = "";
        $authorize = "Basic ".base64_encode($this->sms_config_ants['Username'].':'.$this->sms_config_ants['Password']);

        $ch = curl_init();   
        curl_setopt($ch,CURLOPT_URL,$this->sms_config_ants['Url']);

        // if(ENVIRONMENT == 'production'){
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type:application/json',
          'Authorization:'.$authorize
        ));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch,CURLOPT_POST,1); 
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
        // curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
        // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
        
       
        $Result = curl_exec($ch); 

         if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            print_r($error_msg);exit;
        }
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch); 

        //print_r($Result);exit;
        $decode_result = json_decode($Result);

       // print($decode_result);exit;



        return true;
      


    }

  

    public function  curlSendSMSAntsV2($data = []){

      $authorize = "";
      $authorize = "Basic ".base64_encode($this->sms_config_ants['Username'].':'.$this->sms_config_ants['Password']);
      $ch = curl_init();   
      try{
            curl_setopt($ch,CURLOPT_URL,$this->sms_config_ants['Url_v2']);
            // if(ENVIRONMENT == 'production'){
            //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // }
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type:application/json',
              'Authorization:'.$authorize
            ));
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($ch,CURLOPT_POST,1); 
            curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
            
            $Result = curl_exec($ch);
            $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
            curl_close($ch);
            //print_r($Result);exit;
            $decode_result = json_decode($Result);
            // if($decode_result->messages[0]->status->groupId == '1'){
            //   return true;
            // }else{
            //   return false;
            // }
            return true;
      } catch( Exception $e ){
      
        return false;
      
      }
  }

    private function checkAntsMessageLogs($decode_result){

        $url = 'http://api.ants.co.th/sms/1/reports?messageId='.$decode_result->messages[0]->messageId;

        //echo $url;exit;

        $authorize = "";
        $authorize = "Basic ".base64_encode($this->sms_config_ants['Username'].':'.$this->sms_config_ants['Password']);

        //  Initiate curl
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type:application/json',
          'Authorization:'.$authorize
        ));
        // Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);

        // Will dump a beauty json :3
        var_dump(json_decode($result, true));
        exit;

        
    }


    private function curlSendSMSAntsV3($data = array()){

      $this->ci->load->config('sms');
      $sms_config_ants = $this->ci->config->item('ANTS');

      $authorize = "";
      $authorize = "Basic ".base64_encode($sms_config_ants['Username'].':'.$sms_config_ants['Password']); 


      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $sms_config_ants['Url'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/json",
          "Authorization: ".$authorize.""
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl); 

      $decode_result = json_decode($response); 

      return true;
      echo $response;exit;



      //echo $authorize;exit;

      $ch = curl_init();   
      curl_setopt($ch,CURLOPT_URL,$sms_config_ants['Url']);

      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type:application/json',
        'Authorization:'.$authorize
      ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
      curl_setopt($ch,CURLOPT_POST,1); 
      curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
      curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
      
     
      $Result = curl_exec($ch); 

      $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
      curl_close($ch);
      $decode_result = json_decode($Result); 


      if(isset($_GET['test']) && $_GET['test'] == 'test'){

      }

      return true;
    
  }




}