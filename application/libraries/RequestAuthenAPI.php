<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of requestAuthenAPI
 *
 * @author sethawich
 */
class RequestAuthenAPI {

    private static $instance = NULL;
    private static $authen;
    private static $requireMethod;
    
    private $requestorId  = '';
    private $requestorPwd = '';
    private $apiKey = '';
    private $agentId = '';
    private $isUsedCredit = true;
    private $minimumBalance = 0;
    private $totalBalance = 0;
    private $totalCredit = 0;
    private $totalDebit = 0;
    private $currency = 'THB';
    
    var $returnAsObj;
    
    
    private static $username = "PsiTDC";
    private static $password = "GwROBxrhCu4rrpUV";
    private static $secretkey = "rso9Dw";

    /**
     *
     * @return RequestAuthenAPI
     */
    public static function get_instance() {
        $CI =& get_instance();
        //var_dump($CI);
        // $configApiAuthentication = $CI->config->item('apiAuthentication');
        // self::$username = $configApiAuthentication['username'];
        // self::$password = $configApiAuthentication['password'];
        // self::$secretkey = $configApiAuthentication['secretkey'];
        if (self::$instance == NULL) {
            self::$instance = new RequestAuthenAPI();
        }

        self::$authen = array(
            'username' => self::$username,
            'password' => self::$password,
           'secretkey' => self::$secretkey
        );

        return self::$instance;
    }

    /**
     * @param string $method
     */
    public function set_requireMethod($method) {
        self::$requireMethod = $method;
    }

    /**
     * @param stdObj $request
     * @return bool
     */
    public function is_Authen($request) {

        $arHeader = getallheaders();

        // print_r($arHeader);exit;

        if(isset($arHeader['username']) && isset($arHeader['password']) && isset($arHeader['secretkey']) && isset($arHeader['function'])){
            $request->{'authentication'} = new StdClass();
            $request->{'authentication'}->{'username'} = $arHeader['username'];
            $request->{'authentication'}->{'password'} = $arHeader['password'];
            $request->{'authentication'}->{'secretkey'} = $arHeader['secretkey'];
            $request->{'authentication'}->{'function'} = $arHeader['function'];
            // print_r($request);exit;
        }
        // print_r(getallheaders());exit;
        
        if(is_object($request)){
                if(!property_exists($request, 'authentication')){
                   return FALSE;
                }else if(!property_exists($request, 'authentication') && !property_exists($request->authentication, 'username')){
                   return FALSE; 
                }else if(!property_exists($request, 'authentication') && !property_exists($request->authentication, 'password')){
                    return FALSE;
                }else if(!property_exists($request, 'authentication') && !property_exists($request->authentication, 'secretkey')){
                    return FALSE;
                }
        }else{
                 return FALSE;
        }
        
        $AuthenLogin = array(
            'username' => $request->authentication->username,
            'password' => $request->authentication->password,
            'secretkey' => $request->authentication->secretkey
        );
        // print_r($AuthenLogin);exit;
       $requestMethod = $request->authentication->function;
       
        if(!empty(array_diff_assoc($AuthenLogin, self::$authen))){
            return FALSE;
        }
        
        if ($requestMethod != self::$requireMethod) {
            return FALSE;
        }

        return TRUE;
    }
    
    
    public function validateAuthen($request) {
        $query = 'SELECT * FROM `agent` WHERE requestor_id = "'.  $request->Authentication->Username .'" AND requestor_password= "' . md5($request->Authentication->Password) . '"  AND status = "1";';
	$is_agent = false;
        
        //echo $query;
        //echo $request->Authentication->Password; exit();
        
        $agents = HotelWholesaleHotelTable::getInstance()
                  ->getConnection()
                  ->execute($query)
                  ->fetchAll();        
        //var_dump($agents); exit();        
        foreach ($agents as $agent) {
            $agentHash = md5($agent['requestor_id'].$agent['requestor_password'].$agent['code']);
            
           
            
            if ($agentHash == $request->Authentication->Hash) {
                
                $this->requestorId = $request->Authentication->Username;
                $this->requestorPwd = $request->Authentication->Password;
                $this->apiKey = $agent['code'];
                $this->agentId = $agent['id'];
                
                $this->isUsedCredit = $agent['credit_enable'];
                $this->totalCredit = $agent['credit_amount'];
                $this->totalDebit = $agent['debit_amount'];
                $this->totalBalance = ($this->isUsedCredit)?($this->totalCredit - $this->totalDebit):9999999;
                $this->minimumBalance = $agent['minimum_balance'];
                $this->currency = $agent['currency'];
                
                $is_agent =true;
                break;
            }  
        }
        
        if (property_exists($request->Authentication, 'ReturnAsObj')) {
            if($request->Authentication->ReturnAsObj){
                $this->returnAsHotelObj = true;
            }
        }
        
        return $is_agent;       
    }
    
    
    public function checkAgentAccessPermission() {
        if($this->isUsedCredit){
            if($this->totalBalance >= $this->minimumBalance && $this->minimumBalance > 0){
                return true;
            }elseif($this->totalBalance > 0 && $this->minimumBalance == 0){
                return true;
            }else{
                //must deposit credit
                return false;
            }
        }else{
            return true;
        }
    }
    
    public function checkCreditEnable() {
        return $this->isUsedCredit;
    }    
    
    public function getBalance() {
        return $this->totalBalance;
    }
    
    public function getCurrency(){
        return $this->currency;
    }
        
    public function getCrebitAmount() {
        return $this->totalCredit;
    }
        
    public function getDebitAmount() {
        return $this->totalDebit;
    }
        
    public function updateAgentDebit($product, $method, $productRsvn) {
        $agent = AgentTable::getInstance()->find($this->agentId);
        
        if($agent){
            
            $debit_amount = $agent->debit_amount;

            switch($product){
                case 'Hotel':

                    $hotelRsvn = $productRsvn;

                    //Transaction
                    $paymentTransactionAmount = $hotelRsvn->HotelPaymentTransactions[0]->PaymentTransaction->payment_amount;
                    $paymentTransactionCurrency = $hotelRsvn->HotelPaymentTransactions[0]->PaymentTransaction->payment_currency_code;


                    if($agent->currency == 'THB'){
                        $paymentAmount = currency_converter_with_exchange_rate($paymentTransactionAmount, $hotelRsvn->exchange_rate, $hotelRsvn->exchange_rate_thb);
                        $paymentAmount = round($paymentAmount, 2);                            
                    }else{
                        $paymentAmount = currency_converter_with_exchange_rate($paymentTransactionAmount, $hotelRsvn->exchange_rate, 1);
                        $paymentAmount = round($paymentAmount, 2);
                    }

                    switch($method){
                        case 'Booking':    
                            $agent->debit_amount = $debit_amount+$paymentAmount;
                            $agent->save();

                            $filename = APPPATH . 'logs/log_agent_id'. $this->agentId . '_' . date('Ym') . '.txt';
                            $fp = fopen($filename, 'a+');
                            fwrite($fp, "Time :: " . date('Y-m-d H:i:s') . " \n");
                            fwrite($fp, 'Rsvn Id: ' . $hotelRsvn->id . " \n");
                            fwrite($fp, 'Debit Amount: ' . $debit_amount . " \n");
                            fwrite($fp, 'Paid Amount: ' . $paymentAmount . " \n");
                            fwrite($fp, 'Total Debit Amount: ' . ($debit_amount+$paymentAmount) . " \n");
                            fwrite($fp, 'Currency: ' . $agent->currency . " \n\n\n");
                            fclose($fp);

                            unset($debit_amount);

                            break;

                        case 'Cancel':

                            if(@$hotelRsvn->cancellation_amount == 0){       //cancellation_amount is USD currency
                                //Free Cancel, refund 100%
                                $cancelChargeAmount = 0;
                                $refundAmount = $paymentAmount; 
                            }else{
                                //Charge  
                                if($agent->currency == 'THB'){
                                    $cancelChargeAmount = currency_converter_with_exchange_rate($hotelRsvn->cancellation_amount, 1, $hotelRsvn->exchange_rate_thb);                                    
                                }else{
                                    //USD
                                    $cancelChargeAmount = currency_converter_with_exchange_rate($hotelRsvn->cancellation_amount, $hotelRsvn->exchange_rate, 1);                                    
                                } 
                                
                                if($cancelChargeAmount >= $paymentAmount){
                                    $refundAmount = 0;
                                }else{
                                    $refundAmount = $paymentAmount-$cancelChargeAmount;
                                }
                            }

                            $agent->debit_amount = $debit_amount-$refundAmount;
                            $agent->save();

                            $filename = APPPATH . 'logs/log_agent_id'. $this->agentId . '_' . date('Ym') . '.txt';
                            $fp = fopen($filename, 'a+');
                            fwrite($fp, "Time :: " . date('Y-m-d H:i:s') . " \n");
                            fwrite($fp, 'Rsvn Id: ' . $hotelRsvn->id . " \n");
                            fwrite($fp, 'Debit Amount: ' . $debit_amount . " \n");
                            fwrite($fp, 'Cancel Amount: ' . $cancelChargeAmount . " \n");
                            fwrite($fp, 'Refund Amount: ' . $refundAmount . " \n");
                            fwrite($fp, 'Total Debit Amount: ' . ($debit_amount-$refundAmount) . " \n");
                            fwrite($fp, 'Currency: ' . $agent->currency . " \n\n\n");
                            fclose($fp);

                            unset($debit_amount);
                            unset($cancelAmount);
                            unset($refundAmount);

                            break;

                        default:
                            $filename = APPPATH . 'logs/log_agent_balance_' . date('Ymd') . '.txt';
                            $fp = fopen($filename, 'a+');
                            fwrite($fp, "Time :: " . date('H:i:s') . " \n");
                            fwrite($fp, 'Agent Id: ' . $this->agentId . " \n");
                            fwrite($fp, 'Method: ' . $method . " \n\n\n");
                            fclose($fp);
                            return array('error'=>array('code'=>405, 'message'=>'Unknow Method To Update Agent Balance'));
                    }

                    unset($paymentAmount);
                    unset($paymentTransactionAmount);
                    unset($paymentTransactionCurrency);

                    break;

                case 'Flight':
                    break;

                case 'Car':
                    break;

                case 'Tour':
                    break;

                case 'Ticket':
                    break;

                default:
                    $filename = APPPATH . 'logs/log_agent_balance_' . date('Ymd') . '.txt';
                    $fp = fopen($filename, 'a+');
                    fwrite($fp, "Time :: " . date('H:i:s') . " \n");
                    fwrite($fp, 'Agent Id: ' . $this->agentId . " \n");
                    fwrite($fp, 'Product: ' . $product . " \n");
                    fwrite($fp, 'Method: ' . $method . " \n\n\n");
                    fclose($fp);
                    return array('error'=>array('code'=>405, 'message'=>'Unknow Method To Update Agent Balance'));
            }
        }else{
            $filename = APPPATH . 'logs/log_agent_balance_' . date('Ymd') . '.txt';
            $fp = fopen($filename, 'a+');
            fwrite($fp, "Time :: " . date('H:i:s') . " \n");
            fwrite($fp, 'Agent Id: ' . $this->agentId . " \n");
            fwrite($fp, 'Product: ' . $product . " \n");
            fwrite($fp, 'Method: ' . $method . " \n");
            fwrite($fp, "Error Message: Unknow Agent To Update Agent Balance \n\n\n");
            fclose($fp);
            return array('error'=>array('code'=>405, 'message'=>'Unknow Agent To Update Agent Balance'));
        }
    }
    
    
    
    

    /**
     * set requestCriteria 
     * 
     * @param Array $request
     * @param String $request
     * 
     * @return JSONObj $requestCriteria
     */
    public function set_requestCriteria($request) {

        if (is_array($request)) {
            $requestCriteria = json_decode(json_encode($request));
        }
        if (is_string($request)) {
            $requestCriteria = json_decode(urldecode($request));
                        $CI =& get_instance();
                         $configAuthen = $CI->config->item('apiAuthentication');
                         if($CI->input->get('example') == "true"){
                             $requestCriteria->authentication->username = $configAuthen['username'];
                             $requestCriteria->authentication->password = $configAuthen['password'];
                             $requestCriteria->authentication->secretkey = $configAuthen['secretkey'];
                         }else{
                             $requestCriteria = $requestCriteria;
                         }
        }
        

        return $requestCriteria;
    }

    public function get_returnAsObj() {

        return $this->returnAsObj;
    }
        
     public function getAgentId() {
         return $this->agentId;
     }
    
}

?>
