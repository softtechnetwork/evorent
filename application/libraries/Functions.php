<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() { 
    

		

		

     	//เรียกใช้งาน Class helper     
        // $this->load->helper('url'); 
      	// $this->load->helper('form');
		// $this->load->helper('file'); 
		// $this->load->helper('date'); 
 
     	//เรียกใช้งาน Class database     
		//  $this->load->database(); 
		//  $this->authen = $this->load->database('authen', TRUE);

        //เรียกใช้งาน Model  
        // $this->load->model('api/Contracts');
        // $this->load->model('api/Profile');
    } 


	########## object respons ##########
    public function respons() {
        $res = [
            'code'=> '200',
            'status'=>true,
            'datas'=> null,
            'messsage' => 'success fully.',
            'response' => '',
        ];
        return $res;
    }

	########## format id cart ##########
	public function textFormat( $text, $pattern, $ex) {
		$cid = ( $text == '' ) ? '0000000000000' : $text;
		$pattern = ( $pattern == '' ) ? '_-____-_____-__-_' : $pattern;
		$p = explode( '-', $pattern );
		$ex = ( $ex == '' ) ? '-' : $ex;
		$first = 0;
		$last = 0;
		for ( $i = 0; $i <= count( $p ) - 1; $i++ ) {
		   $first = $first + $last;
		   $last = strlen( $p[$i] );
		   $returnText[$i] = substr( $cid, $first, $last );
		}
	  
		return implode( $ex, $returnText );
	}

	function numberT0digit($number)
	{
		return intval( ($number*100) )/100;
	}

	##########  convert int to thai text  ##########
	function PriceToText($amount_number)
	{
		$amount_number = number_format($amount_number, 2, ".","");
		$pt = strpos($amount_number , ".");
		$number = $fraction = "";
		if ($pt === false) 
			$number = $amount_number;
		else
		{
			$number = substr($amount_number, 0, $pt);
			$fraction = substr($amount_number, $pt + 1);
		}
		
		$ret = "";
		$baht = $this->ReadNumber ($number);
		if ($baht != "")
			$ret .= $baht . "บาท";
		
		$satang = $this->ReadNumber($fraction);
		if ($satang != "")
			$ret .=  $satang . "สตางค์";
		else 
			$ret .= "ถ้วน";
		return $ret;
	}

	########## convert number to thai text ##########
	function ReadNumber($number)
	{
		$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
		$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
		$number = $number + 0;
		$ret = "";
		if ($number == 0) return $ret;
		if ($number > 1000000)
		{
			$ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
			$number = intval(fmod($number, 1000000));
		}
		
		$divider = 100000;
		$pos = 0;
		while($number > 0)
		{
			$d = intval($number / $divider);
			$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
				((($divider == 10) && ($d == 1)) ? "" :
				((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
			$ret .= ($d ? $position_call[$pos] : "");
			$number = $number % $divider;
			$divider = $divider / 10;
			$pos++;
		}
		return $ret;
	}

	##########  convert date to thai date ##########
	function DateThai($strDate)
	{
		//$strYear = date("Y",strtotime($strDate))+543;
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		//return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
		return "$strDay $strMonthThai $strYear";
	}

	########## encode_authen ##########
	function encode_authen($id_card, $tel){
		//$curent_date = date('Y-m-d H:i:s');
		$curent_date = Date('Y-m-d H:i:s', strtotime('+1 day'));
		$format = $id_card.'.'.$curent_date.'.'.$tel;
		$base_64 = base64_encode($format);
		return $base_64;
	}
	########## decode_authen ##########
	function decode_authen($authen){
		$decode = base64_decode($authen);
		$res = explode('.', $decode);
		$obj = new stdClass();
		$obj->idcard = $res[0];
		$obj->expire = $res[1];
		$obj->tel = $res[2];
		return $obj;
	}
	########## encode_authen ##########
	function report_authentication(){
		$obj = new stdClass();
        $authen=array();
        foreach (getallheaders() as $key => $value) {
			if($key == 'Authorization'){
				$authen['Authorization'] = $value;
			}
			if($key == 'authorization'){
				$authen['Authorization'] = $value;
			}
        }

		if($authen){
			$arr = explode(' ',$authen['Authorization']);
			$obj->status = true;
			$obj->token = $arr[1];

		}else{
			$obj->status = false;
		}
		return $obj;
	}
}
