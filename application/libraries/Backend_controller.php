<?php
include_once APPPATH . 'libraries/template.php';
     class Backend_controller extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $admin_data = array();
             private $conn_mysql;
             public $event_ability;
             public $template = array();
            
        
         public function __construct() {
                    parent::__construct();

                    // login     
                    if(!$this->session->userdata('isUserLoggedIn')){
                        redirect(base_url('admin/user/login'));
                    }

                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                      	//เรียกใช้งาน Class helper     
                    $this->load->helper('url'); 
                    $this->load->helper('form');

                  //เรียกใช้งาน Class database     
                   $this->load->database(); 
                   $this->load->helper('our_helper');
                

                    $this->load->library(array('Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','cookie'));
                    
                    //$this->template->set_template('template');

                    $this->template();
                    

                    // $this->_load_js();
                    // $this->_load_css();

                  
        }

        public function template() {
            // making temlate and send data to view.
            //$this->template['header']   = $this->load->view('backend/header', $this->data, true);
            $this->template['header']   = $this->load->view('admin/header', $this->data, true);

            //$this->template['left']   = $this->load->view('backend/left', $this->data, true);
            //$this->template['content'] = $this->load->view($this->middle, $this->data, true);
            $this->template['external_js'] = array();
            $this->template['footer'] = $this->load->view('backend/footer', $this->data, true);
            //$this->load->view('backend/template', $this->template);
          }
        

        /**
         * load javascript
         */
        public function _load_js() {
        

            $this->template['external_js'][] = base_url('./assete/admin/vendors/jquery/dist/jquery.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/fastclick/lib/fastclick.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/nprogress/nprogress.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/iCheck/icheck.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/jquery.hotkeys/jquery.hotkeys.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/google-code-prettify/src/prettify.js');
            $this->template['external_js'][] = base_url('./assete/admin/build/js/custom.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/production/js/moment/moment.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/bootstrap-daterangepicker/daterangepicker.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');

            // datatable
            // $this->template['external_js'][] = base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/css/jquery.dataTables.min.css');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/js/jquery.dataTables.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/js/dataTables.rowReorder.min.js');
            $this->template['external_js'][] = base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/js/dataTables.responsive.min.js');
            
            $this->template['external_js'][] = 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js';
        }

        

        
        /**
         * load style sheet
         */
        public function _load_css() {            
            $this->template->stylesheet->add(base_url('assets/vendor/fontawesome-free/css/all.min.css'));

            $this->template->stylesheet->add('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i');
            
            $this->template->stylesheet->add(base_url('assets/css/sb-admin-2.min.css')); 
            $this->template->stylesheet->add(base_url('assets/css/additional_style.css'));
        }

        public function loadSweetAlert(){
            $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        }

        public function loadDataTableCSS(){
            $this->template->stylesheet->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.css'));
        }
        public function loadDataTableJS(){
            $this->template->javascript->add(base_url('assets/vendor/datatables/jquery.dataTables.js'));
            $this->template->javascript->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.js'));
            $this->template->javascript->add(base_url('assets/js/sb-admin-datatables.min.js'));
        }

        public function loadImageUploadStyle(){
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_common.css'));
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_hover.css'));

        }
        public function loadImageUploadScript(){
            $this->template->javascript->add(base_url('assets/js/fileupload_script.js'));
        }

        public function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/bootstrapValidator.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/language/th_TH.js'));
        }

        protected function loadBootstrapFileuploadMasterScript(){
         //$this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/plugins/sortable.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/fileinput.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/locales/th.js'));

        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/fa/theme.js'));
        }
        protected function loadBootstrapFileuploadMasterStyle(){
            $this->template->stylesheet->add(base_url('assets/css/bootstrap-fileupload-master/fileinput.min.css'));

            $this->template->stylesheet->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.css'));
        }

        public function _checkAlready_signin(){
            if(!$this->session->userdata('admin_data')){
                redirect(base_url());
            }
        }
        public function _getAdminData(){
            if($this->session->userdata('admin_data') && empty($this->admin_data)){
                $this->admin_data = $this->session->userdata('admin_data');
            }

        }
        public function getData($key = null) {
            if (is_null($key) || empty($key))
                return $this->data;
            else
                return $this->data[$key];
        }

        public function setData($key, $data) {
            $this->data[$key] = $data;
        }
        public function getController() {
            return $this->controller;
        }

        public function setController($controller) {
            $this->setData('controller', $controller);
            $this->controller = $controller;
        }

        public function getMethod() {
            return $this->method;
        }

        public function setMethod($method) {
            $this->setData('method', $method);
            $this->method = $method;
        }

        public function config_page($total, $cur_page) {

        
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="page-item">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="paging_btn">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="active"><a class="page-link">';
                $config['cur_tag_close'] = '</a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="paging_btn">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 5;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
            }

            public function product($id = null,$active = null,$typeproduct_id = null , $isused_from_anotherproduct = null , $product_parent_id = null){
                
                $query = $this->db->select('masterproduct.* , product_category.cate_name')
                ->from('masterproduct')
                ->join('product_category' , 'masterproduct.product_category_id = product_category.id' , 'left');
                if($typeproduct_id == null || $typeproduct_id == 1){
                    $query = $query->where('typeproduct_id',"1");
                }else if($typeproduct_id == 3){
                    // select all
                }
                else{
                    $query = $query->where('typeproduct_id', $typeproduct_id);
                    
                }

                #case: ถ้าประเภทสินค้าเป็นอะไหล่ และต้องไม่เป็นอะไหล่ของสินค้าอื่น
                if($typeproduct_id == 2 && ($isused_from_anotherproduct != null && $isused_from_anotherproduct == 1)){
                    $query = $query->where('product_parent_id');
                }

                if($product_parent_id != null){
                    //echo $product_parent_id;exit();
                    $query = $query->where('masterproduct.product_parent_id' , $product_parent_id);
                    
                }

                if($id != null)
                {
                    $query = $query->where('masterproduct.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('active',"1");
                }
                $results = $query->get();
               // 
                
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }

            public function product_install($id = null,$active = null){
                $query = $this->db->select('product_install.*')
                ->from('product_install');
              
               
                if($id != null)
                {
                    $query = $query->where('product_install.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('active',"1");
                }
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }
            public function product_hirepurchase_interest($id = null,$active = null){
            
                $query = 'SELECT r1.* FROM (
                    SELECT "product_install"."id", "product_install"."piabbrv"
                    , "product_install"."piname", "product_install"."picode"
                    , "product_install"."price_a"
                    , count(product_install_id) as installment_obj
                    , "product_install"."out_of_stock" 
                
                    FROM "product_install" LEFT JOIN "product_hirepurchase_interest"
                    ON "product_install"."id" = 
                    "product_hirepurchase_interest"."product_install_id" 
                    GROUP BY "product_install"."id", "product_install"."piname"
                    , "product_install"."picode", "product_install"."piabbrv"
                    , "product_install"."price_a", "product_install"."out_of_stock"
                ) r1
                where r1.installment_obj > 0';

                if($id != null)
                {
                    $query .= " and r1.id = '".$id."'";
                }
            
                //$results = $query->get();
                $results = $this->db->query($query);
            
                if($results->num_rows() > 0){
                    
                    return $results->result();
                }else{
                    return 0;
                }
            }

            public function product_install_rel_hirepurchaseinterest($id = null,$active = null){
                $query = $this->db->select(' product_install.* , product_hirepurchase_interest.*')
                ->from('product_install')
                ->join('product_hirepurchase_interest' , 'product_install.id = product_hirepurchase_interest.product_install_id');
               
                if($id != null)
                {
                    $query = $query->where('product_install.id',$id);
                }
               
                $results = $query->get();
                //echo '<PRE>';
                //print_r($results->result());exit();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }


            public function product_install_set($id = null,$active = null){
                $query = $this->db->select(' masterproduct.* , product_install_set.*')
                ->from('masterproduct')
                ->join('product_install_set' , 'product_install_set.masterproduct_id = masterproduct.id');
               
                if($id != null)
                {
                    $query = $query->where('product_install_set.product_install_id',$id);
                }
               
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }


            public function product_category($id = null,$active = null){
                $query = $this->db->select('*')
                ->from('product_category');
                
                if($id != null)
                {
                    $query = $query->where('product_category.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('active',"1");
                }
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }
            public function slugify($text)
            {
                // replace all non letters or digits by _
                $text = preg_replace('/\W+/', '_', $text);
        
                // trim and lowercase
                $text = strtolower(trim($text, '_'));
        
                return $text;
            }
        
            public function ajaxretrievefilefromserver(){
                $product = $this->product($_POST['product_id'] , null , $_POST['typeproduct_id']);
                
              
                $pic= array();
                foreach($product as $k => $value){
                    if(!empty($value->pic)){
                        foreach(json_decode($value->pic) as $picKey => $picvalue){
                            $pic[] = $picvalue;
                        }
                    }
                }
                
                //print_r($product_id);exit();
                echo json_encode(array("data" => $pic));
           }

           public function ajaxretrieveproductsetfilefromserver(){
            $product = $this->product_install($_POST['product_id'] , null);
                
              
            $pic= array();
            foreach($product as $k => $value){
                if(!empty($value->pic)){
                    foreach(json_decode($value->pic) as $picKey => $picvalue){
                        $pic[] = $picvalue;
                    }
                }
            }
            
            //print_r($product_id);exit();
            echo json_encode(array("data" => $pic));
           }

            private function uploadProductPicture($data = array()){
                    $masterproduct_id = $data['id'];
                    $type=  $data['type'];
                    $files = $data['files'];
                    $images = array();
                    if(!is_dir('uploaded/masterproduct/'.$masterproduct_id.'')){
                        mkdir('uploaded/masterproduct/'.$masterproduct_id.'',0777,true);
                    }
                    
                    clearDirectory('uploaded/masterproduct/'.$masterproduct_id.'/');
            
                    $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                    $config['upload_path'] = 'uploaded/masterproduct/'.$masterproduct_id.'/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '5000';
                    $this->load->library('upload',$config);
                    if(!$this->upload->do_upload('cover_image[]')){
                        $error = array('error' => $this->upload->display_errors());
                        $this->msg->add($error['error'], 'error');
                        /** if type : create */
                        if(file_exists('uploaded/masterproduct/'.$masterproduct_id)){
                                rmdir('uploaded/masterproduct/'.$masterproduct_id);
                        }
                    
                    
                    }
                    else{
                        $data_upload = array('upload_data' => $this->upload->data());
                    
                        $this->db->update('masterproduct',array(
                            'pic'=>$data_upload['upload_data']['file_name']
                        ),array('id'=>$masterproduct_id));
                        
                    }
           }

           public function formula($id = null,$active = null){
            $query = $this->db->select('formula.*')
            ->from('formula');
            if($id != null)
            {
                $query = $query->where('formula.id',$id);
            }
            if($active != null)
            {
                $query = $query->where('formula.active',"1");
            }
            $results = $query->get();
            if($results->num_rows() > 0){
                return $results->result();
            }else{
                return 0;
            }
        }
    
    
            public function getLatestProductMPCODE($id = null , $active = null  ,$typeproduct_id  = null , $replaceString = null){
                // SELECT top 1000 REPLACE(mpcode, 'MP', '') as mp_code
                // FROM masterproduct
                // where typeproduct_id = 1
                // order by mp_code desc ;
                $query = $this->db->select("masterproduct.id,masterproduct.mpcode")
                ->from("masterproduct");
                
                if($id != null)
                {
                    $query = $query->where('masterproduct.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('active',"1");
                }
                if($typeproduct_id != null){
                    $query = $query->where('typeproduct_id' ,$typeproduct_id);
                }
        
                $query = $query->order_by('id',"DESC");
                $query = $query->limit(1);
            
                $results = $query->get();
            
                if($results->num_rows() > 0){
                    
                    $mpcode = substr($results->row()->mpcode, 2);
                    return $mpcode;
                   // return $results->row()->mp_code;
                }else{
                    return 0;
                }
                
        
            }

            public function getLatestProductSetCode($id = null , $active = null  ){
                // SELECT top 1000 REPLACE(mpcode, 'MP', '') as mp_code
                // FROM masterproduct
                // where typeproduct_id = 1
                // order by mp_code desc ;
                $query = $this->db->select("*")
                ->from("product_install");
                
                if($id != null)
                {
                    $query = $query->where('product_install.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('product_install.active',"1");
                }
                
        
                $query = $query->order_by('product_install.id',"DESC");
                $query = $query->limit(1);
            
                $results = $query->get();
            
                if($results->num_rows() > 0){
                    
                    $picode = substr($results->row()->picode, 2);
                    return $picode;
                   // return $results->row()->mp_code;
                }else{
                    return 0;
                }
                
        
            }


            public function productpart($id = null,$active = null){
                $query = $this->db->select('masterproduct.* , product_category.cate_name')
                ->from('masterproduct')
                ->join('product_category' , 'masterproduct.product_category_id = product_category.id');


                $query = $query->where('typeproduct_id' , 2);
        
                if($id != null)
                {
                    $query = $query->where('masterproduct.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('masterproduct.active',"1");
                }
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }

            public function buildArrGroup($promotion_group , $key  , $post_value , $childkey){
                $exp = explode("_" ,$key);
                $group_id = $exp[1];
                
                    if(!isset($promotion_group[$group_id])){
                        $promotion_group[$group_id] = array();
                    }
                $promotion_group[$group_id][$childkey] = $post_value;
        
                return $promotion_group;
            }


            public function productcategory($id = null,$active = null){
                $query = $this->db->select('*')
                ->from('product_category');
        
                if($id != null)
                {
                    $query = $query->where('product_category.id',$id);
                }
                if($active != null)
                {
                    $query = $query->where('product_category.active',"1");
                }
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }


            public function uom($id = null){
                $query = $this->db->select('*')
                ->from('uom');
        
                if($id != null)
                {
                    $query = $query->where('id',$id);
                }
              
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }

            public function warranty($id = null , $active = null){
                $query = $this->db->select('*')
                ->from('warranty');
        
                if($id != null)
                {
                    $query = $query->where('id',$id);
                }
              
                $results = $query->get();
                if($results->num_rows() > 0){
                    return $results->result();
                }else{
                    return 0;
                }
            }
            
            public function warranty_type($period = null){
                $warranty_type = array("month" => "เดือน" , "day" => "วัน", "year" => "ปี");
                if($period == null){
                    return $warranty_type;
                }
                return $warranty_type[$period];
            }           


            public function checkhaveserialnumber($id = null,$active = null){
            
                $query = '
                        SELECT count(masterproduct_id) as count_masterproduct_id
                        , masterproduct_id
                        from masterproduct_serialnumber
                        group by masterproduct_id
                ';

                if($id != null)
                {
                    $query .= " and id = '".$id."'";
                }
            
                //$results = $query->get();
                $results = $this->db->query($query);
            
                if($results->num_rows() > 0){
                    
                    return $results->result_array();
                }else{
                    return 0;
                }
            }

            
	public function recursiveproduct_delete($product , $checkhaveserialnumber_arr) 
	{
			$x =0;
			$product_arr = $product;
			$count_product = count($product);
					
			while($x < $count_product){
				$foundkey = array_search($product[$x]->id, array_column($checkhaveserialnumber_arr, 'masterproduct_id'));
				
				if(is_numeric($foundkey)){
					// do something
				
				}else{
				
					unset($product_arr[$x]);
					$this->recursiveproduct_delete(array_values($product_arr) , $checkhaveserialnumber_arr);
				}
				++ $x;
			}
			
			//print_r($product);exit();
			return $product_arr;

	}
    
    public function prepareBuildSaveArray($formula_calcurate 
    ,$product_install_id
    ,$holdonpayment , $group_id
    ,$start_payment
    ,$end_payment
    ,$interest
    ,$instalment
    ,$downpayment = null
    ,$amount = null){
        if(!empty($formula_calcurate)){
            $result = 0;
            $result_array = array();
            foreach($formula_calcurate->formula as $fk => $fvalue){
                $calcurate = isset($fvalue->formula_1->calculate) ? $fvalue->formula_1->calculate : 0;
                $product_install = $this->product_install($product_install_id);
                // echo '<PRE>';
                // print_r($product_install);exit();
                $product_price_a = $product_install[key($product_install)]->price_a;
                $product_price_b = isset($product_install[key($product_install)]->price_b) ? $product_install[key($product_install)]->price_b : 0;
                $product_price_c = isset($product_install[key($product_install)]->price_c) ? $product_install[key($product_install)]->price_c : 0;
                $product_price_d = isset($product_install[key($product_install)]->price_d) ? $product_install[key($product_install)]->price_d : 0;
                //ถ้าจำนวนมากกว่า 1
                $product_price_perunit_a = $product_price_a;
                $product_price_perunit_b = $product_price_b;
                $product_price_perunit_c = $product_price_c;
                $product_price_perunit_d = $product_price_d;
             
                if($amount > 1){
                  
                    if($product_price_a > 0){
                        $product_price_a = $product_price_a * intval($amount);
                    }
                    if($product_price_b > 0){
                        $product_price_b = $product_price_b * intval($amount);
                    }
                    if($product_price_c > 0){
                        $product_price_c = $product_price_c * intval($amount);
                    }
                    if($product_price_d > 0){
                        $product_price_d = $product_price_d * intval($amount);
                    }
                }

                //วางเงินดาวน์
                if($downpayment > 0){
                    if($product_price_a > 0){
                        $product_price_a =  $product_price_a - $downpayment;
                    }
                    if($product_price_b > 0){
                        $product_price_b =  $product_price_b - $downpayment;
                    }
                    if($product_price_c > 0){
                        $product_price_c =  $product_price_c - $downpayment;
                    }
                    if($product_price_d > 0){
                        $product_price_d =  $product_price_d - $downpayment;
                    }
                }
                $tax_rate = $product_install[key($product_install)]->tax_rate;
                // check ว่ามีพักชำระหนี้ไหม
                $countholdonpayment = 0;
                if(isset($holdonpayment[$group_id])){
                    $countholdonpayment = count($holdonpayment[$group_id]);
                }
                
                // ผ่อนกี่งวด
                $nofmonth_product_installments =$end_payment - $countholdonpayment;
                //ถ้าส่งดอกเบี้ยมาเป็น array
                # declare variable for test เรื่องดอกเบี้ย
                //$interest = array(2,2,3,3,3,3,3,3,3,3,3,3,3);
                if(is_array($interest)){
                    if(!empty($interest)){
                        foreach($interest as $ink => $ins){
                            
                             //คำนวณยอดรวมต่อเดือน price(a)
                            $result_array[$ink]['result_a'] = $this->calcurate_result( $product_price_a , $calcurate ,$nofmonth_product_installments , $ins );
                            
                            //คำนวณยอดรวมต่อเดือน price(b)
                            $result_array[$ink]['result_b'] = $this->calcurate_result( $product_price_b , $calcurate ,$nofmonth_product_installments , $ins );
                        
                            //คำนวณยอดรวมต่อเดือน price(c)
                            $result_array[$ink]['result_c'] = $this->calcurate_result( $product_price_c , $calcurate ,$nofmonth_product_installments , $ins );

                            //คำนวณยอดรวมต่อเดือน price(d)
                            $result_array[$ink]['result_d'] = $this->calcurate_result( $product_price_d , $calcurate ,$nofmonth_product_installments , $ins );
                            
                        }
                    }
                }else{
        
                    //คำนวณยอดรวมต่อเดือน price(a)
                    $result_a = $this->calcurate_result( $product_price_a , $calcurate ,$nofmonth_product_installments , $interest );
                    
                    //คำนวณยอดรวมต่อเดือน price(b)
                    $result_b = $this->calcurate_result( $product_price_b , $calcurate ,$nofmonth_product_installments , $interest );
                
                    //คำนวณยอดรวมต่อเดือน price(c)
                    $result_c = $this->calcurate_result( $product_price_c , $calcurate ,$nofmonth_product_installments , $interest );

                    //คำนวณยอดรวมต่อเดือน price(d)
                    $result_d = $this->calcurate_result( $product_price_d , $calcurate ,$nofmonth_product_installments , $interest );
                    
                }
                
                //echo '<PRE>';
                //print_r($result_array);exit();
                //echo $calcurate;exit();
                //echo math_eval($calcurate);
            }

            for($i = $start_payment; $i<= $end_payment; $i ++){
                $interest_value = 0;
                # check ถ้า ดอกเบี้ยเป็น array ต้องคำนวณรายงวด
                if(is_array($interest)){
                    # case: ดอกเบี้ยไม่เหมือนกัน ex.งวด1=2% งวด2=3%
                    $keyOfIdent = ($i > 0 ) ? $i - 1 : 0;
                    $result_a = $result_array[$keyOfIdent]['result_a'];
                    $result_b = $result_array[$keyOfIdent]['result_b'];
                    $result_c = $result_array[$keyOfIdent]['result_c'];
                    $result_d =  $result_array[$keyOfIdent]['result_d'];
                    $interest_value = $interest[$keyOfIdent];
                       
                }
                else{
                    # case: ดอกเบี้ยทุกงวดเหมือนกันหมด ex. ดอกเบี้ย3%
                    $interest_value = $interest;
                }
             
                $save_array[$i]['product_price_a'] = floatval($product_price_a);
                $save_array[$i]['product_price_b'] = floatval($product_price_b);
                $save_array[$i]['product_price_c'] = floatval($product_price_c);
                $save_array[$i]['product_price_d'] = floatval($product_price_d);
                // ราคาต่อ/หน่วย
              
                $save_array[$i]['product_price_perunit_a'] = floatval($product_price_perunit_a);
                $save_array[$i]['product_price_perunit_b'] = floatval($product_price_perunit_b);
                $save_array[$i]['product_price_perunit_c'] = floatval($product_price_perunit_c);
                $save_array[$i]['product_price_perunit_d'] = floatval($product_price_perunit_d);
                $save_array[$i]['amount'] = ( $amount > 1 ) ? $amount : 1;
                // eof
                $save_array[$i]['interest'] = floatval($interest_value);
                $save_array[$i]['interest_a'] = floatval(number_format((float)$product_price_a * ( $interest_value / 100 ), 2 , '.' ,''));
                $save_array[$i]['interest_b'] =  floatval(number_format((float)$product_price_b * ( $interest_value / 100 ), 2 , '.' ,''));
                $save_array[$i]['interest_c'] =  floatval(number_format((float)$product_price_c * ( $interest_value / 100 ), 2 , '.' ,''));
                $save_array[$i]['interest_d'] =  floatval(number_format((float)$product_price_d * ( $interest_value / 100 ), 2 , '.' ,''));
                $tax_rate = floatval($tax_rate);
                $save_array[$i]['tax_rate'] = $tax_rate;

                //if($help_pay_installments_status  != 1){
                if($countholdonpayment > 0){
                    if (in_array($i, $holdonpayment[$group_id]))
                    {
                        $save_array[$i] = $this->buildSaveArray($instalment, $save_array , 0 , 0 , 0 , 0 ,$i , $tax_rate);
                    }else{
                        // build save array 
                        $save_array[$i] = $this->buildSaveArray($instalment, $save_array , $result_a , $result_b,$result_c,$result_d ,$i , $tax_rate);
                    }
                }else{
                    // build save array 
                    $save_array[$i] = $this->buildSaveArray($instalment, $save_array , $result_a , $result_b,$result_c,$result_d ,$i , $tax_rate);
                }
        
            }
        }
       
        return $save_array;
    }

    public function buildSaveArray($instalment, $save_array , $result_a , $result_b,$result_c,$result_d ,$i , $tax_rate){
		$result_a =floatval(number_format((float)$result_a, 2, '.', '')); 
		$result_b =floatval(number_format((float)$result_b, 2, '.', '')); 
		$result_c = floatval(number_format((float)$result_c, 2, '.', ''));
		$result_d =floatval(number_format((float)$result_d, 2, '.', '')); 
		# ยอดผ่อนต่อเดือนยังไม่รวมภาษีมูลค่าเพิ่ม 
		//$save_array[$i]['per_month_price_a'] = $result_a;
        $save_array[$i]['per_month_price_a'] = $instalment;
		$save_array[$i]['per_month_price_b'] = $result_b;
		$save_array[$i]['per_month_price_c'] = $result_c;
		$save_array[$i]['per_month_price_d'] = $result_d;
        //print_r($result_a);exit();
		
		# ภาษีมูลค่าเพิ่มต่อเดือน
		//$save_array[$i]['per_month_price_a_vat'] =  (floatval($result_a) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_a_vat'] =  $instalment;
		$save_array[$i]['per_month_price_b_vat'] =  (floatval($result_b) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_c_vat'] =  (floatval($result_c) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_d_vat'] =  (floatval($result_d) * ( $tax_rate / 100 ));
		# ยอดผ่อนต่อเดือนรวมภาษีมูลค่าเพิ่ม
		//$save_array[$i]['per_month_price_a_include_vat'] = floatval($result_a) + (floatval($result_a) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_a_include_vat'] = $instalment;
		$save_array[$i]['per_month_price_b_include_vat'] = floatval($result_b) + (floatval($result_b) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_c_include_vat'] = floatval($result_c) + (floatval($result_c) * ( $tax_rate / 100 ));
		$save_array[$i]['per_month_price_d_include_vat'] = floatval($result_d) + (floatval($result_d) * ( $tax_rate / 100 ));
		
        
		return $save_array[$i];
	}

	public function calcurate_result( $product_price , $calcurate ,$nofmonth_product_installments , $interest ){

			// if($help_pay_installments_status != 1){
			# replace string in word to 
			if (strpos($calcurate, '$product_price') !==  false) { 
				$calcurate = str_replace('$product_price',$product_price,$calcurate);
			}
			if (strpos($calcurate, '$nofmonth_product_installments') !==  false) { 
				$calcurate = str_replace('$nofmonth_product_installments',$nofmonth_product_installments,$calcurate);
			}
			if (strpos($calcurate, '$interest') !==  false) { 
				$calcurate = str_replace('$interest',$interest,$calcurate);
			}
			//}else{
			//$calcurate = 0;
			//}

			//คำนวณผ่อนชำระต่อเดือน
			eval( '$result = (' . $calcurate. ');' );
			return $result;

	}

}