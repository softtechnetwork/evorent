    <style>
        .dowload-panel{padding-right: 4%; padding-top: 3%; width: 100%;}
        .dowload-button{vertical-align: middle;text-decoration: none; background-color: #2bbec0;color: #fff;padding: 1%;float: right;border-radius: 3px;}
    </style>
    <section class="bg-light py-5">
        <div class="container">
            <div class="row py-3">

                <?php if($id == 2){ ?>
                    <div class="col-md-12 text-white bg-white">
                        <div class="dowload-panel">
                            <a class="dowload-button" 
                            href="<?=base_url('./uploaded/evorentweb/request_doc/rent/DOC0001(10-06-2021).pdf');?>" 
                            download="ใบขอเช่าใช้สินค้า" >
                                <i class='fas fa-download'></i> ดาวน์โหลดใบขอเช่าใช้สินค้า
                            </a>
                        </div>
                    </div>
                <?php }else{ ?> 
                    <div class="col-md-12 text-white bg-white">
                        <div class="dowload-panel">
                            <a class="dowload-button"
                            href="<?=base_url("./uploaded/evorentweb/request_doc/buy/DOC0013(18-04-2023).pdf");?>" 
                            download="ใบขอเช่าซื้อสินค้า">
                                <i class='fas fa-download'></i> ดาวน์โหลดใบขอเช่าสินค้า
                            </a>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-md-12 text-white bg-white">
                    <?php
                    $img_src = '';  
                    switch($id){
                        case 0 : $img_src = base_url('./uploaded/evorentweb/leaflet_a5.jpg'); break;
                        case 1 : $img_src = base_url('./uploaded/evorentweb/leaflet_a5.jpg'); break;
                        case 2 : $img_src = base_url('./uploaded/evorentweb/rent.png'); break;
                        case 3 : $img_src = base_url('./uploaded/evorentweb/buy.jpg'); break;
                        default: $img_src = base_url('./uploaded/evorentweb/leaflet_a5.jpg'); break;
                    }
                    ?> 
                    <img src="<?=$img_src;?>" alt="About Hero" style="width:100%;">
                </div>
            </div>
        </div>
    </section>
    <!-- Close Banner -->


    <!--End Brands-->