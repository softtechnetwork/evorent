
    <!-- Start Footer -->
    <footer class="bg-dark " id="tempaltemo_footer">
        <div class="container">
            <div class="row">

                <div class="col-md-4 pt-5">
                    <!--<h2 class="h2 text-success border-bottom pb-3 border-light logo">ที่อยู่</h2>-->
                    <h2 class="h2 text-light border-bottom pb-3 border-light">ที่อยู่สำนักงาน</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li>
                            <img src="<?php echo base_url('./uploaded/evorentweb/pin_icon.png');?>" class="card-img-top" style="width: 1.3rem;">
                            <!--<i class="fas fa-map-marker-alt fa-fw"></i>-->
                            <!-- อาคารพีเอสไอ สำนักงานใหญ่ (สาขาพัฒนาการ) ชั้น 4 ให้บริการ วันจันทร์ - ศุกร์ เวลา 08.00-17.00 น. -->
                            อาคารพีเอสไอ สำนักงานใหญ่ (สาขารามคำแหง) ชั้น 3 ให้บริการ วันจันทร์ - ศุกร์ เวลา 08.00-17.00 น.
                            เลขที่ 2 ซอย รามคำแหง 159 แขวงราษฎร์พัฒนา เขตสะพานสูง กรุงเทพมหานคร 10240
                        </li>
                        <li>
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d124019.6725781463!2d100.56964213764101!3d13.741757617367178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d61c9f4f05a63%3A0xc05c5316ec7a2835!2zUFNJIOC4quC4s-C4meC4seC4geC4h-C4suC4meC5g-C4q-C4jeC5iA!5e0!3m2!1sth!2sth!4v1624849568015!5m2!1sth!2sth" height="100" style="border:0; width:100%;" allowfullscreen="" loading="lazy"></iframe> -->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15499.143389261333!2d100.6982528!3d13.7917795!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d63edf86d23e7%3A0xd223326d4bc5a67a!2z4Lio4Li54LiZ4Lii4LmM4Lia4Lij4Li04LiB4Liy4LijIFBTSSDguKrguLLguILguLLguKPguLLguKHguITguLPguYHguKvguIcxNTk!5e0!3m2!1sth!2sth!4v1688353195348!5m2!1sth!2sth" width="100" style="border:0; width:100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">ติดต่อเรา</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li>
                            <img src="<?php echo base_url('./uploaded/evorentweb/phone_icon.png');?>" class="card-img-top" style="width: 1.3rem;">
                            <a class="text-decoration-none" href="tel:09 7162 8565">09 7162 8565</a>
                        </li>
                        <li>
                            <img src="<?php echo base_url('./uploaded/evorentweb/line_icon.png');?>" class="card-img-top" style="width: 1.3rem;">
                            <a class="text-decoration-none" href="https://line.me/ti/p/~@evorent">@evorent</a>
                        </li>
                        
                        <li>
                            <img src="<?php echo base_url('./uploaded/evorentweb/fb_icon.png');?>" class="card-img-top" style="width: 1.3rem;">
                            <a class="text-decoration-none" href="https://www.facebook.com/evorentofficial/">evorentofficial</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">เมนู</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li><a class="text-decoration-none" href="<?php echo base_url('./');?>">หน้าหลัก</a></li>
                        <li><a class="text-decoration-none" href="<?php echo base_url('./th/about');?>">เกี่ยวกับเรา</a></li>
                        <li><a class="text-decoration-none" href="<?php echo base_url('./th/product');?>">สินค้า</a></li>
                        <li><a class="text-decoration-none" href="<?php echo base_url('./th/contact');?>">ติดต่อเรา</a></li>
                        <li><a class="text-decoration-none" href="<?php echo base_url('./th/member');?>">สำหรับสมาชิก</a></li>
                    </ul>
                </div>

            </div>

            <!--
            <div class="row text-light mb-4">
                <div class="col-12 mb-3">
                    <div class="w-100 my-3 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="http://facebook.com/"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.instagram.com/"><i class="fab fa-instagram fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://twitter.com/"><i class="fab fa-twitter fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.linkedin.com/"><i class="fab fa-linkedin fa-lg fa-fw"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-auto">
                    <label class="sr-only" for="subscribeEmail">Email address</label>
                    <div class="input-group mb-2">
                        <input type="text" class="form-control bg-dark border-light" id="subscribeEmail" placeholder="Email address">
                        <div class="input-group-text btn-success text-light">Subscribe</div>
                    </div>
                </div>
            </div>
            -->
        </div>

        <div class="w-100 bg-black py-3">
            <div class="container">
                <div class="row text-center pt-2">
                    <div class="col-12">
                        <!--<p class="text-left text-light"> 2021 (บริษัท อีโวเร้นท์ จำกัด) | Designed by <span style="font-size: 1rem;">(บริษัท ซอฟท์เทค เน็ตเวิร์ค จำกัด)</span>-->  <!--<a rel="sponsored" href="https://templatemo.com" target="_blank">บริษัท ซอฟท์เทค เน็ตเวิร์ค</a></p>-->
                        <p class="text-left text-light"> 
                            2021 EVORENT CO., LTD. | Designed by <span style="font-size: 1rem;">SOFTTECH NETWORK COMPANY LIMITED</span>  
                        </p>
                        <p class="text-left text-light"> 
                            <a rel="sponsored" href="<?=base_url('./privacypolicy')?>" target="_blank">นโยบายความเป็นส่วนตัว</a> | <a rel="sponsored" href="<?=base_url('./termsservice')?>" target="_blank">ข้อตกลงและเงื่อนไขในการให้บริการเว็บไซต์</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <!-- Start Script -->
    <script src="<?php echo base_url('./assete/web/js/jquery-migrate-1.2.1.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/web/js/bootstrap.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/web/js/templatemo.js');?>"></script>
    <script src="<?php echo base_url('./assete/web/js/custom.js');?>"></script>
    <!-- End Script -->
</body>

</html>