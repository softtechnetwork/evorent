<!-- Start Content -->
<style>
        
    .carousel-indicators li{ margin-top: auto !important; }


    @media (max-width: 575px){
        .carousel-control-next, .carousel-control-prev{font-size: 11px !important;}
        .home-packag-panel p, .home-packag-panel a{font-size: 13px !important;  margin-bottom: .5rem !important;}
        .product-name{font-size: 11px !important;}
    }

    @media (min-width: 576px){
        .carousel-control-next, .carousel-control-prev{font-size: 16px !important;}
        .product-name{font-size: 16px !important;}
    }
    @media (min-width: 1400px){}

</style>
<!-- Start Banner Hero -->
<div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
    <input type="hidden" id="base_url" value="<?=base_url()?>">
    <ol class="carousel-indicators" id="indicators-banner-position"></ol>
    <div class="carousel-inner" id="img-banner-position"></div>
    
    <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
        <i class="fas fa-chevron-left"></i>
    </a>
    <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
        <i class="fas fa-chevron-right"></i>
    </a>
    
</div>
<!-- End Banner Hero -->

    <div class="container py-5">
        <input type="hidden" id="base_url" value="<?=base_url()?>">
        <div class="row">
            <!--
            <div class="col-lg-3">
                <h1 class="h2 pb-4">หมวดหมู่</h1>
                <ul class="list-unstyled templatemo-accordion">
                    <li class="pb-3">
                        <a class="collapsed d-flex justify-content-between h3 text-decoration-none" href="#">
                            Gender
                            <i class="fa fa-fw fa-chevron-circle-down mt-1"></i>
                        </a>
                        <ul class="collapse show list-unstyled pl-3">
                            <li><a class="text-decoration-none" href="#">Men</a></li>
                            <li><a class="text-decoration-none" href="#">Women</a></li>
                        </ul>
                    </li>
                    <li class="pb-3">
                        <a class="collapsed d-flex justify-content-between h3 text-decoration-none" href="#">
                            Sale
                            <i class="pull-right fa fa-fw fa-chevron-circle-down mt-1"></i>
                        </a>
                        <ul id="collapseTwo" class="collapse list-unstyled pl-3">
                            <li><a class="text-decoration-none" href="#">Sport</a></li>
                            <li><a class="text-decoration-none" href="#">Luxury</a></li>
                        </ul>
                    </li>
                    <li class="pb-3">
                        <a class="collapsed d-flex justify-content-between h3 text-decoration-none" href="#">
                            Product
                            <i class="pull-right fa fa-fw fa-chevron-circle-down mt-1"></i>
                        </a>
                        <ul id="collapseThree" class="collapse list-unstyled pl-3">
                            <li><a class="text-decoration-none" href="#">Bag</a></li>
                            <li><a class="text-decoration-none" href="#">Sweather</a></li>
                            <li><a class="text-decoration-none" href="#">Sunglass</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            -->


            <div class="row">
                <div class="col-lg-12 m-auto"><h1>สินค้า</h1></div>
            </div>
            <div class="col-lg-12">
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-inline shop-top-menu pb-3 pt-1">
                            <li class="list-inline-item">
                                <a class="h3 text-dark text-decoration-none mr-3" href="#">All</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="h3 text-dark text-decoration-none mr-3" href="#">Men's</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="h3 text-dark text-decoration-none" href="#">Women's</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 pb-4">
                        <div class="d-flex">
                            <select class="form-control">
                                <option>Featured</option>
                                <option>A to Z</option>
                                <option>Item</option>
                            </select>
                        </div>
                    </div>
                </div>
                 -->
                
                <div class="row" id="products"> </div>
                <!--
                <div div="row">
                    <ul class="pagination pagination-lg justify-content-end">
                        <li class="page-item disabled">
                            <a class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0" href="#" tabindex="-1">1</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href="#">3</a>
                        </li>
                    </ul>
                </div>
                -->
            </div>

        </div>
    </div>
    <!-- End Content -->

    <!-- Start Brands -->
    <!--
    <section class="bg-light py-5">
        <div class="container my-4">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">Our Brands</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        Lorem ipsum dolor sit amet.
                    </p>
                </div>
                <div class="col-lg-9 m-auto tempaltemo-carousel">
                    <div class="row d-flex flex-row">
                        <div class="col-1 align-self-center">
                            <a class="h1" href="#multi-item-example" role="button" data-bs-slide="prev">
                                <i class="text-light fas fa-chevron-left"></i>
                            </a>
                        </div>
                        <div class="col">
                            <div class="carousel slide carousel-multi-item pt-2 pt-md-0" id="multi-item-example" data-bs-ride="carousel">
                                <div class="carousel-inner product-links-wap" role="listbox">

                                    <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1 align-self-center">
                            <a class="h1" href="#multi-item-example" role="button" data-bs-slide="next">
                                <i class="text-light fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->
    <!--End Brands-->
    <script>

        var base_url = $('#base_url').val();
        var res = getproducts(base_url);
        drawproduct(base_url, res);
        function getproducts(base_url){
            var res = null;
            $.ajax({
                url: base_url+'/th/Product/get_product', //ทำงานกับไฟล์นี้
                data: '',  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function drawproduct(base_url, res){
            
            str = '';
            $.each( res.datas, function( key, value ) {
                str += '<div class="col-sm-6 col-md-3">';
                str += '<div class="card mb-4 product-wap rounded-0" style="text-align: center;">';
                str += '<a href="'+base_url+'./product/detail/'+value.product_set_id+' " class="h3 text-decoration-none">';
                str += '<div class="card rounded-0">';
                str += '<img class="card-img rounded-0 img-fluid" src="'+base_url+value.path+' ">';
                str += '<div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">';
                str += '</div>';
                str += '</div>';
                str += '<div class="card-body">'+value.title+'</div>';
                str += '</a>';
                str += '</div>';
                str += '</div>';
                str += '';
            });

            $('#products').append(str);
            console.log(str);
        }

        draw_banners(base_url);
        function get_banner(base_url){
            var res = null;
            $.ajax({
                url: base_url+'/th/Home/get_banners', //ทำงานกับไฟล์นี้
                data: '',  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function draw_banners(base_url){
            var banners = get_banner(base_url);
            if(banners.datas.length > 0){
                var str = '';
                var indicators = '';
                $.each( banners.datas, function( key, value ) {
                    var actived = (key == 0)?"active":"";
                    indicators += '<li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="'+key+'" class="'+actived+'" ></li>';
                    
                    var imgPath = base_url+value.img;
                    str += '<div class="carousel-item '+actived+'" style="text-align: center; background-color: #fff;">';
                    //str += '<a href="<?php echo base_url('./home/detail/0');?>">';
                    str += '<img class="img-fluid" src="'+imgPath+'" alt="">';
                    //str += '</a>';
                    str += '</div>';
                });
                $('#indicators-banner-position').append(indicators);
                $('#img-banner-position').append(str);
                $('#template-mo-zay-hero-carousel').show();
            }else{
                $('#template-mo-zay-hero-carousel').hide();
            }
        }
    </script>
