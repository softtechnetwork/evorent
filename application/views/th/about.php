<section class="bg-success py-5">
        <div class="container">
            <div class="row align-items-center py-5">
                <div class="col-md-8 text-white">
                    <h1>เกี่ยวกับเรา</h1>
                    <p>บริษัท อีโวเร้นท์ จำกัด เป็น บริษัทในเครือข่าย บริษัท ซอฟท์เทค เน็ตเวิร์ค    มุ่งบริการให้เช่า  อุปกรณ์ไฟฟ้า อุปกรณ์อิเล็กทรอนิกส์ทุกชนิด รวมทั้งชิ้นส่วนอะไหล่   ที่มีคุณภาพที่ดีให้กับลูกค้าและเหมาะสม พร้อมกับบริการติดตั้งส่งมอบสินค้าอย่างรวดเร็ว  และให้คำแนะนำสินค้าอย่างถูกต้องและตรงไปตรงมาเพื่อให้ลูกค้าเกิดความพึ่งพอใจอย่างเป็นที่สุด ก่อนตกลงเช่าสินค้า ตลอดจนถึงบริการหลังการติดตั้งและคำแนะนำต่าง ๆ ของสินค้า บริษัท อีโวเร้นท์ จำกัด ก่อตั้งเมื่อ 18 มีนาคม 2564   เป็นบริการเช่าที่รวมเครื่องใช้ไฟฟ้า ไอทีและเทคโนโลยีดิจิตอลครบวงจร ที่ราคาถูกและบริการหลังการขายที่ลูกค้าพึ่งพอใจดีเยี่ยมตลอดมา  มีบริการเปิดให้เช่าสินค้าทั้งในกรุงเทพ ปริมณฑลและต่างจังหวัด
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo base_url('./uploaded/evorentweb/about-hero.png');?>" alt="About Hero" style="width: 100%;">
                    <!--<img src="<?php echo base_url('./assete/web/img/about-hero.svg');?>" alt="About Hero">-->
                </div>
            </div>
        </div>
    </section>
    <!-- Close Banner -->

    <!-- Start Section -->
    <section class="container py-5">
        <div class="row text-center">
            <div class="col-lg-9 m-auto">
                <h1 class="h1">บริการ</h1>
                <p>เรามุ่งให้บริการเช่า อุปกรณ์ไฟฟ้า อุปกรณ์อิเล็กทรอนิกส์ทุกชนิด รวมทั้งชิ้นส่วนอะไหล่ ที่มีคุณภาพที่ดี ราคาที่เหมาะสม
                และบริการส่งมอบพร้อมติดตั้งสินค้าให้กับลูกค้า
                </p>
            </div>
        </div>
        <div class="row  text-center pt-5 pb-3 ">

            <div class="col-md-3 m-auto">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fa fa-money-check-alt fa-lg"></i></div>
                    <h2 class="h5 mt-4 text-center">บริการเช่าสินค้า</h2>
                </div>
            </div>
<!--
            <div class="col-md-3 m-auto">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fas fa-exchange-alt"></i></div>
                    <h2 class="h5 mt-4 text-center">Shipping & Return</h2>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fa fa-percent"></i></div>
                    <h2 class="h5 mt-4 text-center">Promotion</h2>
                </div>
            </div>
            

            <div class="col-md-4">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fa fa-user"></i></div>
                    <h2 class="h5 mt-4 text-center">24 Hours Service</h2>
                </div>
            </div>
            -->
            
        </div>
    </section>
    <!-- End Section -->

    <!-- Start Brands -->
    <!--
    <section class="bg-light py-5">
        <div class="container my-4">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">Our Brands</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        Lorem ipsum dolor sit amet.
                    </p>
                </div>
                <div class="col-lg-9 m-auto tempaltemo-carousel">
                    <div class="row d-flex flex-row">
                       
                        <div class="col-1 align-self-center">
                            <a class="h1" href="#templatemo-slide-brand" role="button" data-bs-slide="prev">
                                <i class="text-light fas fa-chevron-left"></i>
                            </a>
                        </div>
                        
                        <div class="col">
                            <div class="carousel slide carousel-multi-item pt-2 pt-md-0" id="templatemo-slide-brand" data-bs-ride="carousel">
                                <div class="carousel-inner product-links-wap" role="listbox">

                                    <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_01.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_02.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_03.png');?>" alt="Brand Logo"></a>
                                            </div>
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="<?php echo base_url('./assete/web/img/brand_04.png');?>" alt="Brand Logo"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1 align-self-center">
                            <a class="h1" href="#templatemo-slide-brand" role="button" data-bs-slide="next">
                                <i class="text-light fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->
    <!--End Brands-->