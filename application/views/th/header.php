<!DOCTYPE html>
<html lang="en">

<head>
    <title>EVORENT</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="<?=base_url('./uploaded/evorentweb/apple-icon.png');?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('./uploaded/evorentweb/favicon.ico');?>">

    <!-- <link rel="stylesheet" href="<?=base_url('./assete/web/css/bootstrap.min.css');?>"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url('./assete/web/css/templatemo.css');?>">
    <link rel="stylesheet" href="<?=base_url('./assete/web/css/custom.css');?>">

    <!-- Load fonts style after rendering the layout styles -->
    <!-- <link rel="stylesheet" href="<?=base_url('./assete/web/css/font.css');?>"> -->
    <!-- <link rel="stylesheet" href="<?=base_url('./assete/web/css/fontawesome.min.css');?>"> -->
    <link rel="stylesheet" href="<?=base_url('./assete/web/css/new-font.css');?>">
    
    <!-- Load jquery script -->
    <!-- <script src="<?=base_url('./assete/web/js/jquery-1.11.0.min.js');?>"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- <script src="http://code.jquery.com/jquery-1.11.1.js"></script> -->
    <script src="<?=base_url('./assete/web/js/custom.js');?>"></script>
   
    <!--
    TemplateMo 559 Zay Shop
    https://templatemo.com/tm-559-zay-shop
    -->
</head>


<body>
    <!-- Start Top Nav -->
    <!--
    <nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
        <div class="container text-light">
            <div class="w-100 d-flex justify-content-between">
                <div>
                    <i class="fa fa-envelope mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:info@company.com">info@company.com</a>
                    <i class="fa fa-phone mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="tel:010-020-0340">010-020-0340</a>
                </div>
                <div>
                    <a class="text-light" href="https://fb.com/templatemo" target="_blank" rel="sponsored"><i class="fab fa-facebook-f fa-sm fa-fw me-2"></i></a>
                    <a class="text-light" href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram fa-sm fa-fw me-2"></i></a>
                    <a class="text-light" href="https://twitter.com/" target="_blank"><i class="fab fa-twitter fa-sm fa-fw me-2"></i></a>
                    <a class="text-light" href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin fa-sm fa-fw"></i></a>
                </div>
            </div>
        </div>
    </nav>
    -->
    <!-- Close Top Nav -->


    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-light shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <!--<a class="navbar-brand text-success logo h1 align-self-center" href="<?php echo base_url('./home');?>">Zay</a>-->
            <div class="col-3 col-md-1">
                <a class="navbar-brand text-success logo h1 align-self-center mb-1" href="<?php echo base_url('./home');?>">
                    <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/apple-icon.png');?>" alt="">
                </a>
            </div>
            <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link <?=($menu == 'home')? 'active-menu':'';?>" href="<?php echo base_url('./home');?>">หน้าหลัก</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=($menu == 'about')? 'active-menu':'';?>" href="<?php echo base_url('./about');?>">เกี่ยวกับเรา</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=($menu == 'product')? 'active-menu':'';?>" href="<?php echo base_url('./product');?>">สินค้า</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=($menu == 'contact')? 'active-menu':'';?>" href="<?php echo base_url('./contact');?>">ติดต่อเรา</a>
                        </li>
                        <li class="nav-item">
                            <!-- <a class="nav-link <?=($menu == 'member')? 'active-menu':'';?>" href="<?php echo base_url('./member');?>">สำหรับสมาชิก</a>https://evorent.co.th/web/#/ -->
                            <a class="nav-link" target='_blank' href="https://evorent.co.th/web/#/">สำหรับสมาชิก</a>
                        </li>
                    </ul>
                </div>
                <!--
                <div class="navbar align-self-center d-flex">
                    <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputMobileSearch" placeholder="Search ...">
                            <div class="input-group-text">
                                <i class="fa fa-fw fa-search"></i>
                            </div>
                        </div>
                    </div>
                    <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                        <i class="fa fa-fw fa-search text-dark mr-2"></i>
                    </a>
                    <a class="nav-icon position-relative text-decoration-none" href="#">
                        <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
                        <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">7</span>
                    </a>
                    <a class="nav-icon position-relative text-decoration-none" href="#">
                        <i class="fa fa-fw fa-user text-dark mr-3"></i>
                        <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">+99</span>
                    </a>
                </div>
                -->
            </div>
        </div>
    </nav>
    <!-- Close Header -->

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>




