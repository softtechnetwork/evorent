    <style>
        .home-packag-panel{
            box-shadow: 0px 0px 8px 1px #4a4e5180 !important;
        }
        .home-packag a{
            position: absolute;
            top: 100%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%); color: white;
            font-size: 16px;
            padding: 5px;
            border: 1px solid #ededed;
            font-weight: bold !important;
            width: 65%;
            vertical-align: middle;text-decoration: none; color: #fff;padding: 1%;float: right;border-radius: 3px;
        }
        .carousel-indicators li{
            margin-top: auto !important;
        }


        @media (max-width: 575px){
            .carousel-control-next, .carousel-control-prev{font-size: 11px !important;}
            .home-packag-panel p, .home-packag-panel a{font-size: 13px !important;  margin-bottom: .5rem !important;}
            .product-name{font-size: 11px !important;}
        }

        @media (min-width: 576px){
            .carousel-control-next, .carousel-control-prev{font-size: 16px !important;}
            .product-name{font-size: 16px !important;}
        }
        @media (min-width: 1400px){
           
        }

    </style>
    <!-- Start Banner Hero -->
    <div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <input type="hidden" id="base_url" value="<?=base_url()?>">
        <ol class="carousel-indicators" id="indicators-banner-position">
            <!-- <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active" ></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li> -->
            <!-- <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="3"></li> -->
        </ol>
        
        <!-- <div class="carousel-inner"> -->
            
            <!-- <div class="carousel-item active">
                <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/banner_main.jpg');?>" alt="">
            </div> -->
            <!-- <div class="carousel-item active"> -->
                <!-- <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/banner_slide_solarcell.jpg');?>" alt=""> -->
                <!--
                <div class="container">
                    <div class="row p-1">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/banner_slide_evorent.jpg');?>" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left align-self-center">
                                <h1 class="h1 text-success"><b>Zay</b> eCommerce</h1>
                                <h3 class="h2">Tiny and Perfect eCommerce Template</h3>
                                <p>
                                    Zay Shop is an eCommerce HTML5 CSS template with latest version of Bootstrap 5 (beta 1). 
                                    This template is 100% free provided by <a rel="sponsored" class="text-success" href="https://templatemo.com" target="_blank">TemplateMo</a> website. 
                                    Image credits go to <a rel="sponsored" class="text-success" href="https://stories.freepik.com/" target="_blank">Freepik Stories</a>,
                                    <a rel="sponsored" class="text-success" href="https://unsplash.com/" target="_blank">Unsplash</a> and
                                    <a rel="sponsored" class="text-success" href="https://icons8.com/" target="_blank">Icons 8</a>.
                                </p>
                            </div>
                        </div>
                    </div> 
                </div>
                -->
            <!-- </div> -->
            <!-- <div class="carousel-item">
                <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/banner_slide_solarpump.jpg');?>" alt="">
            </div> -->
            <!-- <div class="carousel-item"> -->
                <!--<a href="<?php echo base_url('./home/detail/0');?>">-->
                    <!-- <img class="img-fluid" src="<?php echo base_url('./uploaded/evorentweb/banner_slide_master_tv.jpg');?>" alt=""> -->
                <!--</a>-->
            <!-- </div> -->
        <!-- </div> -->
        <div class="carousel-inner" id="img-banner-position"></div>
        
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
        
    </div>
    <!-- End Banner Hero -->


    <!-- Start Categories of The Month -->
    <!--
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Categories of The Month</h1>
                <p>
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="<?php echo base_url('./assete/web/img/category_img_01.jpg');?>" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">Watches</h5>
                <p class="text-center"><a class="btn btn-success">Go Shop</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="<?php echo base_url('./assete/web/img/category_img_02.jpg');?>" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Shoes</h2>
                <p class="text-center"><a class="btn btn-success">Go Shop</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="<?php echo base_url('./assete/web/img/category_img_03.jpg');?>" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Accessories</h2>
                <p class="text-center"><a class="btn btn-success">Go Shop</a></p>
            </div>
        </div>
    </section>
    -->
    <!-- End Categories of The Month -->


    <!-- Start Featured Category Product -->
    <section class="bg-white">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-8 m-auto">
                    <!-- <h1>เลือกแพ็กเกจ</h1>
                    <p>เรามีรูปแบบการเช่าสองแพ็กเกจตามสไตล์ตามที่คุณต้องการ เลือกแพ็กเกจตามอิสระภาพการเช่าของคุณ</p> -->
                    <h1>หลักฐานการเช่าสินค้า</h1>
                    <p>เอกสารและหลักฐานที่ใช้ในการประกอบการพิจารณา</p>
                </div>
            </div>
            <div class="row justify-content-center text-center">
                <!--<div class="col-6 col-md-4 mb-4 text-white home-packag" >
                    <div class="card h-100 py-4 px-2 home-packag-panel" style="background-color: #00b2b5;">
                        <h3 class ="text-center">เช่าใช้</h3>
                        <img src="<?=base_url('uploaded/evorentweb/rent_image.png');?>" alt="" style="width:100%;">
                        <p>เอกสารและหลักฐานที่ใช้ในการประกอบการพิจารณา</p>
                        <a href="<?=base_url('./home/detail/2');?>" style="background-color: #1ec8cb;vertical-align: middle;">เพิ่มเติม <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>-->
                <div class="col-8 col-xl-4 col-lg-4 col-md-5 col-sm-8 mb-4 text-white home-packag">
                    <div class="card h-100 py-4 px-2 home-packag-panel" style="background-color: #007cc3;">
                        <!-- <h3 class ="text-center">เช่าซื้อ</h3> -->
                        <!-- <img src="<?=base_url('uploaded/evorentweb/buy_image.png');?>" alt="" style="width:100%;"> -->
                        <img src="<?=base_url('uploaded/evorentweb/png.png');?>" alt="" style="width:100%;">
                        <!-- <p>เอกสารและหลักฐานที่ใช้ในการประกอบการพิจารณา</p> -->
                        <a href="<?=base_url('./home/detail/3');?>" style="background-color: #2497d9;vertical-align: middle;">เพิ่มเติม <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Product -->

    <!-- Start Featured Product -->
    <div id="recommend-products"></div>
    <!-- <section class="bg-light">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto"><h1>สินค้าแนะนำ</h1></div>
            </div>
            <div class="row">
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('./product/detail/P001');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/we.jpg');?>" class="card-img-top" alt="...">
                       
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">เครื่องแยกน้ำ WE</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('./product/detail/P002');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/we2.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">เครื่องแยกน้ำ WE2</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P006');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/air.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Air Inverter I24</label>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P012');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/p16.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Hybrid inverter single phase P16</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P013');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/p33.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Hybrid inverter single phase P33</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P014');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/p50.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Hybrid inverter single phase P50</label>
                            </div>
                        </a>
                    </div>
                </div>

                
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P015');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/c5.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Solapump C5</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P016');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/c7.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Solapump C7</label>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P017');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/s7.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Solapump S7</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P018');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/s11.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Solapump S11</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P019');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/s15.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Solapump S15</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P020');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/hilight_products_01.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Master TV</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo base_url('th/product/detail/P021');?>">
                            <img src="<?php echo base_url('./uploaded/evorentweb/easyplug-products.jpg');?>" class="card-img-top" alt="...">
                            <div class="card-body text-center p-2">
                                <label class="text-decoration-none text-dark product-name">Easy Plug</label>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- End Featured Product -->

    <script>
        var base_url = $('#base_url').val();
        draw_banners(base_url);
        draw_recommend_product(base_url);
        function get_recommend_products(base_url){
            var res = null;
            $.ajax({
                url: base_url+'/th/Product/get_recommend_products', //ทำงานกับไฟล์นี้
                data: '',  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function draw_recommend_product(base_url){
            var res = get_recommend_products(base_url);
            str = '';
            if(res.datas.length > 0){
                str += '<section class="bg-light">';
                str += '<div class="container py-5">';
                str += '<div class="row text-center py-3">';
                str += '<div class="col-lg-6 m-auto"><h1>สินค้าแนะนำ</h1></div></div>';
                str += '<div class="row">';
                $.each( res.datas, function( key, value ) {

                    str += '<div class="col-6 col-md-4 mb-4">';
                    str += '<div class="card h-100">';
                    str += '<a href="'+base_url+'./product/detail/'+value.product_set_id+'">';
                    str += '<img src="'+base_url+value.path+'" class="card-img-top" alt="...">';
                    str += '<div class="card-body text-center p-2">';
                    str += '<label class="text-decoration-none text-dark product-name">'+value.title+'</label>';
                    str += '</div>';
                    str += '</a>';
                    str += '</div>';
                    str += '</div>';
                });
                str += '</div>';
                str += '</div>';
                str += '</section>';
            }
            $('#recommend-products').append(str);
        }

        
        function get_banner(base_url){
            var res = null;
            $.ajax({
                url: base_url+'/th/Home/get_banners', //ทำงานกับไฟล์นี้
                data: '',  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function draw_banners(base_url){
            var banners = get_banner(base_url);
            if(banners.datas.length > 0){
                var str = '';
                var indicators = '';
                $.each( banners.datas, function( key, value ) {
                    var actived = (key == 0)?"active":"";
                    indicators += '<li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="'+key+'" class="'+actived+'" ></li>';
                    
                    var imgPath = base_url+value.img;
                    str += '<div class="carousel-item '+actived+'" style="text-align: center; background-color: #fff;">';
                    //str += '<a href="<?php echo base_url('./home/detail/0');?>">';
                    str += '<img class="img-fluid" src="'+imgPath+'" alt="">';
                    //str += '</a>';
                    str += '</div>';
                });
                $('#indicators-banner-position').append(indicators);
                $('#img-banner-position').append(str);
                $('#template-mo-zay-hero-carousel').show();
            }else{
                $('#template-mo-zay-hero-carousel').hide();
            }
        }
    </script>
