<?php
if($this->session->userdata('contact')) { 
    $this->session->unset_userdata('contact'); 
?>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <!--
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
-->
      <div class="modal-body" style="text-align:center;">รับเรื่องเรียบร้อยแล้ว รอเจ้าหน้าที่ติดต่อกลับ</div>
      <div class="modal-footer">
      <div class="col-md-6 m-auto text-center">   
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
        </div> 
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(window).load(function () {
        $('#exampleModal').modal('show');
    });
</script>
 <?php } ?>
    <!-- Start Content Page -->
    <div class="container-fluid bg-light py-5">
        <div class="col-md-6 m-auto text-center">
            <h1 class="h1">ติดต่อเรา</h1>
            <!-- <p> อาคารพีเอสไอ สำนักงานใหญ่ (สาขาพัฒนาการ) ชั้น 4 ให้บริการ วันจันทร์ - ศุกร์ เวลา 08.00-17.00 น.</p> -->
            <p>อาคารพีเอสไอ สำนักงานใหญ่ (สาขารามคำแหง) ชั้น 3 ให้บริการ วันจันทร์ - ศุกร์ เวลา 08.00-17.00 น.</p>
            <p>เลขที่ 2 ซอย รามคำแหง 159 แขวงราษฎร์พัฒนา เขตสะพานสูง กรุงเทพมหานคร 10240</p>
        </div>     
    </div>
    <div class="col-md-12">
        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d124019.6725781463!2d100.56964213764101!3d13.741757617367178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d61c9f4f05a63%3A0xc05c5316ec7a2835!2zUFNJIOC4quC4s-C4meC4seC4geC4h-C4suC4meC5g-C4q-C4jeC5iA!5e0!3m2!1sth!2sth!4v1624849568015!5m2!1sth!2sth" height="460" style="border:0; width:100%;" allowfullscreen="" loading="lazy"></iframe> -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15499.143389261333!2d100.6982528!3d13.7917795!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d63edf86d23e7%3A0xd223326d4bc5a67a!2z4Lio4Li54LiZ4Lii4LmM4Lia4Lij4Li04LiB4Liy4LijIFBTSSDguKrguLLguILguLLguKPguLLguKHguITguLPguYHguKvguIcxNTk!5e0!3m2!1sth!2sth!4v1688353195348!5m2!1sth!2sth" height="460"  style="border:0; width:100%;" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div> 



    <!-- Start Contact -->
    <div class="container py-5" id="contact-form">
        <div class="row py-5">
            <form id="contact-form" class="col-md-9 m-auto" method="post" role="form" action="<?php echo base_url('th/contact/sendContact');?>">
                <div class="row">
                    <h4>หากคุณมีคำถามหรือข้อสงสัยเกี่ยวกับการบริการเช่าสินค้า สามารถกรอกรายละเอียดเพื่อ ติดต่อเรา</h4>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">ชื่อ<span style="color:red;">*</span></label>
                        <input type="text" class="form-control mt-1" id="name" name="name"  placeholder="..." required>
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">นามสกุล<span style="color:red;">*</span></label>
                        <input type="text" class="form-control mt-1" id="sname" name="sname" placeholder="..." required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">โทรศัพท์มือถือ<span style="color:red;">*</span></label>
                        <input type="tel" class="form-control mt-1" id="tel" name="tel"  pattern="[0-9]{3}[0-9]{3}[0-9]{4}" required>
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">อีเมล์</label>
                        <input type="email" class="form-control mt-1" id="email" name="email" placeholder="...">
                    </div>
                </div>
                <div class="row">
                    <div class=" col-md-3 mb-3">
                        <label for="topic">หัวข้อ</label>
                        <select class="form-control mt-1" id="topic" name="topic" style="appearance: button;-webkit-appearance: button;-moz-appearance: button;" required>
                            <option value="" selected='false' disabled>เลือกหัวข้อ</option>
                            <!-- <option value="ติดต่อเช่าใช้สินค้า">ติดต่อเช่าใช้สินค้า</option> -->
                            <option value="ติดต่อเช่าสินค้า">ติดต่อเช่าสินค้า</option>
                            <option value="ติดต่อสอบถามข้อมูล">ติดต่อสอบถามข้อมูล</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-md-12 mb-3">
                        <label for="inputmessage">เนื้อหา<span style="color:red;">*</span></label>
                        <textarea class="form-control mt-1" id="message" name="message" placeholder="..." rows="8"  required><?php if($product_name != null){ echo'สนใจสินค้ารหัส '.$product_name;}?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-end mt-2">
                        <button type="submit" class="btn btn-success btn-lg px-3">ติดต่อเรา</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End Contact -->
    <script>
    $('#contact-form').submit(function() {
        var confirmPanel = confirm("คุณต้องการที่จะส่งข้อความเพื่อติดต่อเรา จริงหรือไม่");
        if (confirmPanel == true) {
            return true;
        }else{
            return false;
        }
        //return false;
    });
    </script>