<style>
</style>
    <!-- Open Content -->
    <section id="installment-section" class="bg-light content-padding-bottom">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h4>ค้นหาข้อมูลการผ่อนรายเดือน:</h4>
                            <div class="row">
                                <div class="form-group col-sm-6 col-md-6 col-lg-4 b-3">
                                    <label for="inputname">หมายเลขบัตรประชาชน</label>
                                    <!--<input type="text" tabindex="1"  placeholder="x-xxxxx-xxxxx-xx-x" onkeyup="autoTab(this)"  minlength="13" maxlength="20" class="form-control mt-1" id="idcard" name="idcard" required>-->
                                    <input type="text" maxlength="13" class="form-control mt-1" id="idcard" name="idcard" placeholder="กรุณากรอกข้อมูล">
                                </div>
                                <div class="form-group col-sm-6 col-md-6 col-lg-4 mb-3">
                                    <label for="inputemail">หมายเลขสัญญา</label>
                                    <input type="email" maxlength="10" class="form-control mt-1" id="contract_code" name="contract_code" placeholder="กรุณากรอกข้อมูล">
                                </div>
                                <div class="form-group col-sm-6 col-md-6 col-lg-4 mb-3">
                                    <input  id="base_url" value="<?php echo base_url();?>" type="hidden" >
                                    <button type="submit" id="button-search" class="btn btn-success btn-lg mt-4">ค้นหาข้อมูล</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row installment-display" id="installment-panel">
                
               
            </div>
            <div class="row installment-display text-center" id="installment-panel-empty">
                <div class="col-lg-12 mt-1">
                    <div class="card">
                        <div class="card-body">
                            <h4>ไม่พบข้อมูล</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Close Content -->
    <script>

    


    var base_url = $('#base_url').val();
  
    var getRes = base_url+"th/member/getRes";

    var searchEle = ["#idcard", "#contract_code"];
    var resObj = {"getRes" :getRes, "elementTable":"#installment-table tbody"};
  
    EleChange(searchEle);
    $('#button-search').click(function(){
        
        EleChange(searchEle);
        var valids  = 0;
        $.each(searchEle, function (i, val) {
            /*if($(val).val() != '' && val == '#idcard'){
                var tmp = $(val).val().split("-");  
                var res = '';
                $.each(tmp, function (i, val) {
                    res += val;
                });
                $(val).val(res);
            }*/

            if($(val).val() == ''){
                $(val).addClass("input-valid");
                valids++;
            }
        });

        if(valids == 0){
            getWebInstallment(resObj, searchEle);
        }
    });

    </script>