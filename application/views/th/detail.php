
    <!-- Open Content -->
    <input type="hidden" id="base_url" value="<?=base_url()?>">
    <input type="hidden" id="product_id" value="<?=$product_id?>">
    <section class="bg-light">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-5 mt-5">
                    <div class="card mb-3">
                        <img class="card-img img-fluid" src="" alt="Card image cap" id="product-detail">
                    </div>
                    <div class="row">
                        <div class="col-1 align-self-center">
                            <a href="#multi-item-example" role="button" data-bs-slide="prev">
                                <i class="text-dark fas fa-chevron-left"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                        </div>
                        <div id="multi-item-example" class="col-10 carousel slide carousel-multi-item" data-bs-ride="carousel">
                            <div class="carousel-inner product-links-wap" role="listbox" id="small-img">
                            </div>
                        </div>
                        <div class="col-1 align-self-center">
                            <a href="#multi-item-example" role="button" data-bs-slide="next">
                                <i class="text-dark fas fa-chevron-right"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 mt-5">
                    <div class="card">
                        <div class="card-body detail-position">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Close Content -->

    <!-- Start Article -->
    <!-- <?php if(count($productRelate) > 0){ ?>
        <section class="py-5">
            <div class="container">
                <div class="row text-left p-2 pb-3">
                    <h4>สินค้าที่เกี่ยวข้อง</h4>
                </div>
                <div id="carousel-related-product">
                    <div class="row">
                        <?php foreach($productRelate as $item){ ?>
                            <div class="col-sm-6 col-md-3">
                                <div class="card mb-4 product-wap rounded-0" style="text-align: center;">
                                    <a href="<?php echo base_url('./product/detail/').$item['product_code'];?>" class="h3 text-decoration-none">
                                        <div class="card rounded-0">
                                            <img class="card-img rounded-0 img-fluid" src="<?php echo base_url($item['imgpath']);?>">
                                            <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                                            </div>
                                        </div>
                                        <div class="card-body"><?=$item['name']?></div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?> -->
    <!-- End Article -->

    <div id="product-relate-position"></div>

    <script>
        var base_url = $('#base_url').val();
        var product_id = $('#product_id').val();
        var res = getproducts(base_url, product_id);
        var images = getproductsimg(base_url, product_id);
        
        drawproductdetail(base_url, res, images);
        function getproducts(base_url, product_id){
            var res = null;
            $.ajax({
                url: base_url+'/th/Product/get_product_once', //ทำงานกับไฟล์นี้
                data: {'product_id' : product_id},  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function getproductsimg(base_url, product_id){
            var res = null;
            $.ajax({
                url: base_url+'/th/Product/get_product_image', //ทำงานกับไฟล์นี้
                data: {'product_id' : product_id},  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function drawproductdetail(base_url, res, images){
            
            // img once
            var imgonce = base_url+images.datas[0].path+'?rand=' + Math.random();
            $('img#product-detail').attr("src",imgonce);

            // img small
            var $str_small_img = '';
            $str_small_img += '<div class="carousel-item active">';
            $str_small_img += '<div class="row">';
            $.each( images.datas, function( key, value ) {
                var imgs = base_url+value.path;
                $str_small_img += '<div class="col-4">';
                $str_small_img += '<a href="#">';
                $str_small_img += '<img class="card-img img-fluid" src="'+imgs+'?rand=' + Math.random()+'" alt="Product Image '+key+'">';
                $str_small_img += '</a>';
                $str_small_img += '</div>';
            });
            $str_small_img += '</div>';
            $str_small_img += '</div>';
            $('#small-img').append($str_small_img);

            // detail
            res.datas[0].detail +='<a class="btn btn-success btn-lg px-3" href="'+base_url+'/contact/'+res.datas[0].product_set_id+'/#contact-form">สนใจสินค้า</a>';
            $('.detail-position').append(res.datas[0].detail);
        }

        var productsrelate = getproductsrelate(base_url, product_id);
        drawproductrelate(base_url, productsrelate);
        function getproductsrelate(base_url, product_id){
            var res = null;
            $.ajax({
                url: base_url+'/th/Product/get_product_relate', //ทำงานกับไฟล์นี้
                data: {'product_id' : product_id},  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    res = data;
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            return res;
        }
        function drawproductrelate(base_url, productsrelate){
            if(productsrelate.datas.length > 0){
            
                // img small
                var $str_product_relate = '';
                $str_product_relate += ' <section class="py-5"> ' ;
                $str_product_relate += ' <div class="container"> ' ;
                $str_product_relate += ' <div class="row text-left p-2 pb-3"> <h4>สินค้าที่เกี่ยวข้อง</h4> </div> ' ;
                $str_product_relate += ' <div id="carousel-related-product"> ' ;
                $str_product_relate += ' <div class="row"> ' ;
                $.each( productsrelate.datas, function( key, value ) {
                    var imgs = base_url+value.path;
                    $str_product_relate += '<div class="col-sm-6 col-md-3"> ' ;
                    $str_product_relate += ' <div class="card mb-4 product-wap rounded-0" style="text-align: center;"> ' ;
                    $str_product_relate += ' <a href="'+base_url+'./product/detail/'+value.product_set_id+'" class="h3 text-decoration-none"> ' ;
                    $str_product_relate += ' <div class="card rounded-0"> ' ;
                    $str_product_relate += ' <img class="card-img rounded-0 img-fluid" src="'+base_url+value.path+'"> ' ;
                    $str_product_relate += ' <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center"> ' ;
                    $str_product_relate += ' </div> ' ;
                    $str_product_relate += ' </div> ' ;
                    $str_product_relate += ' <div class="card-body">'+value.title+'</div> ' ;
                    $str_product_relate += ' </a> ' ;
                    $str_product_relate += ' </div> ' ;
                    $str_product_relate += ' </div> ' ;
                    
                });
                $str_product_relate += ' </div> ' ;
                $str_product_relate += ' </div> ' ;
                $str_product_relate += ' </div> ' ;
                $str_product_relate += ' </section> ' ;
                $('#product-relate-position').append($str_product_relate);
            }
        }
    </script>
