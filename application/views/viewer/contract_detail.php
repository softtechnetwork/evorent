<div class="clearfix"></div>
<div class="row">
    <div class="col-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รายละเอียดสัญญาลูกค้า <small><?=$res[0]->contract_code;?></small></h2>
                <!-- <ul class="nav navbar-right panel_toolbox">
                <li><a href="<?php echo base_url('viewer/contract/contractPDF/').$res[0]->contract_code;?>" class="btn btn-dark" target="_blank"><i class="fa fa-print"></i> Print สัญญา</a></li>
                </ul> -->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row invoice-info">
                    <div class="col-4 invoice-col">
                        <address>
                            <h5><strong>ข้อมูลลูกค้า</strong></h5>
                            <strong>รหัส :</strong> <?=$res[0]->customer_code;?>
                            <br><strong>หมายเลขบัตรประชาชน :</strong> <?=$res[0]->idcard;?>
                            <br><strong>ชื่อ :</strong> <?=$res[0]->firstname;?> <?=$res[0]->lastname;?>
                            <br><strong>ที่อยู่ปัจจุบัน :</strong> <?=$address[0]->address;?>
                            <br><strong>ตำบล / แขวง :</strong> <?=$address[0]->district_name;?> 
                            <br><strong>อำเภอ / เขต :</strong> <?=$address[0]->amphur_name;?> 
                            <br><strong>จังหวัด :</strong> <?=$address[0]->province_name;?>
                            <br><strong>รหัสไปรษณีย์ :</strong> <?=$address[0]->zip_code;?>
                            <br><strong>Phone :</strong> <?=$res[0]->tel;?> 
                            <br><strong>Email :</strong> <?=$res[0]->email;?> 
                        </address>
                    </div>
                    <div class="col-5 invoice-col">
                        <address>
                            <h5><strong>ข้อมูลสินค้า</strong></h5>
                            <strong>หมายเลขทรัพย์สิน :</strong> <?=$res[0]->product_number;?>
                            <br><strong>สินค้า :</strong> <?=$res[0]->product_name;?>
                            <!--<br><strong>ราคา :</strong> <?=$res[0]->product_price;?><?php echo str_repeat("&nbsp;", 20);?><strong>จำนวน :</strong> <?=$res[0]->product_count;?>-->
                            <!--<br><strong>จำนวน :</strong> <?=$res[0]->product_count;?>-->
                            
                            <br><strong>ยี่ห้อ :</strong> <?=$res[0]->brand_name;?>
                            <br><strong>รุ่น :</strong> <?=$res[0]->product_version;?>
                            <br><strong>วันที่เริ่มสัญญา :</strong> <?=$res[0]->contract_date;?>
                            <br><strong>วันที่เริ่มชำระงวดแรก :</strong> <?=$res[0]->payment_start_date;?>
                            <br><strong>ค่าผ่อนรายเดือน :</strong> <?=$res[0]->monthly_rent;?>
                            <br><strong>ระยะเวลาการเช่า :</strong> <?=$res[0]->rental_period;?> งวด <?php echo str_repeat("&nbsp;", 8);?>เงินดาวน์ : <?=$res[0]->down_payment;?>
                            <br><strong>สถานะ :</strong> <span style=" padding: 0px 25px; color: <?=$res[0]->color;?>; background-color: <?=$res[0]->background_color;?>;"><?=$res[0]->label;?></span>
                            <br><strong>สถานที่ติดตั้งสินค้า :</strong> <?=$installationLocation[0]->address;?> ตำบล / แขวง  <?=$installationLocation[0]->district_name;?> อำเภอ / เขต <?=$installationLocation[0]->amphur_name;?>  จังหวัด <?=$installationLocation[0]->province_name;?> รหัสไปรษณีย์ <?=$installationLocation[0]->zip_code;?>
                        </address>
                    </div>
                    <div class="col-3 invoice-col">
                        <?php 
                        if($res[0]->supporter == 1 && $supporter != null){?>
                        <address>
                            <h5><strong>ข้อมูลผู้คำ้ประกัน</strong></h5>
                            <strong>ชื่อ :</strong> <?=$supporter[0]->fname?> <?=$supporter[0]->lname;?> 
                            <br><strong>ที่อยู่ปัจจุบัน :</strong> <?=$supporter[0]->addr;?>
                            <br><strong>ตำบล / แขวง :</strong> <?=$supporter[0]->district_name;?> 
                            <br><strong>อำเภอ / เขต :</strong> <?=$supporter[0]->amphur_name;?> 
                            <br><strong>จังหวัด :</strong> <?=$supporter[0]->province_name;?>
                            <br><strong>รหัสไปรษณีย์ :</strong> <?=$supporter[0]->zip_code;?>
                            <br><strong>Phone :</strong> <?=$supporter[0]->tel;?>
                        </address>
                        <?php  }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>