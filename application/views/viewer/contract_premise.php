
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>หลักฐานสัญญา <small>รหัส: <?=$res[0]->contract_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอกสาร/สัญญา<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-contract"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปถ่าย Statment<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-statment"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>บัตรประชาชน ผู้ค้ำประกัน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-cardSupporter"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>ทะเบียนบ้าน ผู้ค้ำประกัน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-houseRegisSupporter"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปถ่ายที่พัก<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-residence"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอกสารการติดตั้ง<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-installdoc"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอการค่าเช่าพื้นที่ในสัญญา<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-rentaldoc"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอกสารการซื้อ(เอกสารจาก PSI)<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-purchasedoc"></div>
            </div>
        </div>
    </div>
</div>
<!--<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปถ่าย Statment<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-statment"></div>
            </div>
        </div>
    </div>
</div>-->

<!-- Premise Model -->
<div id="modelPlace"></div>

<script>
    var base_url = "<?php echo base_url(); ?>";
    var refCode  = "<?php echo $res[0]->contract_code; ?>";
    var mode = "contract";
    var url_getRes = base_url+"viewer/premise/getpremise"; // get premise
    var insertController = base_url+"viewer/premise/insertpremise"; // insert premise

    getpremised(url_getRes, refCode, "#position-contract", insertController, mode); // หลักฐาน เอกสาร/สัญญา
    getpremised(url_getRes, refCode, "#position-cardSupporter", insertController, mode);// หลักฐาน บัตรประชาชน ผู้ค้ำประกัน
    getpremised(url_getRes, refCode, "#position-houseRegisSupporter", insertController, mode);// หลักฐาน ทะเบียนบ้าน ผู้ค้ำประกัน
    
    getpremised(url_getRes, refCode, "#position-residence", insertController, mode);// หลักฐาน รูปถ่ายที่พัก
    getpremised(url_getRes, refCode, "#position-statment", insertController, mode);// หลักฐาน รูปถ่าย Statment
    
    getpremised(url_getRes, refCode, "#position-purchasedoc", insertController, mode);// หลักฐาน รูปถ่าย Statment
    getpremised(url_getRes, refCode, "#position-installdoc", insertController, mode);// หลักฐาน รูปถ่าย Statment
    getpremised(url_getRes, refCode, "#position-rentaldoc", insertController, mode);// หลักฐาน รูปถ่าย Statment

    //AddPremise('.premise-add');
    viewPremise('.premise-view','#modelPlace');
    
    // var delController = base_url+"viewer/premise/delpremise"; // del premise
    // delPremise('.premise-del', delController);




    function getpremised(url, refcode, replacePosition, insertController, mode){
        var temp = replacePosition.split("-"); // find element id
        var res = getResPremised(url, refcode, temp[1]);
        drawPremised(res, refcode, replacePosition, temp[1], insertController, mode);
    }

    function getResPremised(url, refcode, elementId){
        
        var res = null;
        $.ajax({
            url: url, //ทำงานกับไฟล์นี้
            data: {
                "ref_code" : refcode,
                "element_id" : elementId
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });
        return res;
    }

    function drawPremised(data, refcode, replacePosition, elementId, insertController, mode){
        $(replacePosition).html(null);
        var srting = '';
        var maxItem = data.length+1;
        //var mainUrl = window.location.origin+'/hirepurchase';
        var mainUrl = window.location.origin;

        if(window.location.hostname == 'localhost'){mainUrl = mainUrl+'/evorent'}
    
        $.each(data, function (i, val) {
            var imgPath = mainUrl+'/'+val['path'];
            var imgId = val['ref_code']+'-'+val['element_id']+'-'+val['no'];
            srting += '<div class="col-md-2 col-sm-2 premise-items">';
            //srting += '<span id="'+val['premise_code']+'" src="'+val['path']+'" url="'+window.location.href+'"  class="thumnails-premise-del premise-del">X</span>';
            srting += '<img class="thumnails-premise premise-view" id="'+imgId+'" src="'+imgPath+'" alt="image" style="border: 1px #08080759 solid;" />';
            srting += '</div>';
        });
        // srting += '<div class="col-md-2 col-sm-2 premise-items">';
        // srting += '<form id="'+elementId+maxItem+'-form" class="" action="'+insertController+'" method="post"  enctype="multipart/form-data" novalidate>';
        // srting += '<input id="refcode" name="refcode" type="hidden" value="'+refcode+'"/>';
        // srting += '<input id="mode" name="mode" type="hidden" value="'+mode+'"/>';
        // srting += '<input id="no" name="no" type="hidden" value="'+maxItem+'"/>';
        // srting += '<input id="elementID" name="elementID" type="hidden" value="'+elementId+'"/>';
        // srting += '<img id="'+elementId+maxItem+'" browsid="input-'+elementId+maxItem+'" class="thumnails-premise premise-add" src="'+mainUrl+'/uploaded/DocumentTh.png" alt="image" style="border: 1px #08080759 solid;" />';
        // srting += '<input id="input-'+elementId+maxItem+'" name="input-'+elementId+maxItem+'" type="file" required="required"  style="display:none" >';
        // srting += '</form>';
        // srting += '</div>';

        $(replacePosition).append(srting);
    }



</script>