<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="บริษัท อีโวเร้นท์ จำกัด เป็น บริษัทในเครือข่าย บริษัท ซอฟท์เทค เน็ตเวิร์ค มุ่งบริการให้เช่า อุปกรณ์ไฟฟ้า อุปกรณ์อิเล็กทรอนิกส์ทุกชนิด รวมทั้งชิ้นส่วนอะไหล่ ที่มีคุณภาพที่ดีให้กับลูกค้าและเหมาะสม พร้อมกับบริการติดตั้งส่งมอบสินค้าอย่างรวดเร็ว และให้คำแนะนำสินค้าอย่างถูกต้องและตรงไปตรงมาเพื่อให้ลูกค้าเกิดความพึ่งพอใจอย่างเป็นที่สุด ก่อนตกลงเช่าซื้อสินค้า ตลอดจนถึงบริการหลังการติดตั้งและคำแนะนำต่าง ๆ ของสินค้า บริษัท อีโวเร้นท์ จำกัด ก่อตั้งเมื่อ 18 มีนาคม 2564 เป็นบริการเช่าซื้อที่รวมเครื่องใช้ไฟฟ้า ไอทีและเทคโนโลยีดิจิตอลครบวงจร ที่ราคาถูกและบริการหลังการขายที่ลูกค้าพึ่งพอใจดีเยี่ยมตลอดมา มีบริการเปิดให้เช่าซื้อสินค้าทั้งในกรุงเทพ ปริมณฑลและต่างจังหวัด">
    <!-- <meta name="robots" content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"> -->
    <meta property="og:site_name" content="EVORENT">
    <title>Document</title>

    <!-- admin theme(gentelella) https://colorlib.com/polygon/gentelella/  -->
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
 
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css');?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url('./assete/admin/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('./assete/admin/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
	  <link href="<?php echo base_url('./assete/admin/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url('./assete/admin/vendors/google-code-prettify/bin/prettify.min.css');?>" rel="stylesheet">


    <!-- dropzone css js!-->
    <link href="<?php echo base_url('./assete/admin/vendors/dropzone-master/dist/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/css/jquery.dataTables.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/css/rowReorder.dataTables.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/admin/vendors/datatables/DataTables-1.10.23/css/responsive.dataTables.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-select/css/bootstrap-select.min.css');?>"rel="stylesheet">

    <link href="<?php echo base_url('./assete/admin/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');?>" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url('./assete/admin/build/css/custom.min.css');?>" rel="stylesheet"> 
    <link href="<?php echo base_url('./assete/css/bootstrap-select.min.css');?>" rel="stylesheet">  
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">-->
    <link href="<?php echo base_url('./assete/css/datepicker.css');?>" rel="stylesheet" media="screen"> 
    <link href="<?php echo base_url('./assete/css/customs.css');?>" rel="stylesheet">  
    
    <!-- jQuery -->
    <script src="<?php echo base_url('./assete/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/moment/min/moment.min.js');?>"></script>

    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker-thai.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/locales/bootstrap-datepicker.th.js');?>"></script>

    <!-- ECharts -->
    <script src="<?php echo base_url('./assete/admin/vendors/echarts/dist/echarts.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/echart_custom.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/validate.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/admin_custom.js');?>"></script>

    <!-- bootstrap-multiselec -->
    <link rel="stylesheet" href="<?php echo base_url('./assete/admin/jquery-multiselect/css/bootstrap-multiselect.css');?>" type="text/css">
    <script type="text/javascript" src="<?php echo base_url('./assete/admin/jquery-multiselect/js/bootstrap-multiselect.js');?>"></script>

</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('viewer/contract');?>" class="site_title"><i class="fa fa-paw"></i> <span>EVORENT</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?=$this->session->userdata('userName');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $userType = $this->session->userdata('userType');?>
           
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- menu for viewer -->
                
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'contract'){echo'active';}?> ">
                      <a href="<?=base_url('viewer/contract');?>"><i class="fa fa-file-text" aria-hidden="true"></i>สัญญาลูกค้า</a>
                    </li>
                  </ul>

                    <!-- <ul class="nav side-menu">
                      <li class=" <?php if($mainmenu == 'contract'){echo'active';}?> ">
                        <a><i class="fa fa-file"></i> จัดการสัญญา <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: <?php if($mainmenu == 'contract'){echo'block';}else{echo'none';}?>;">
                          <li <?php if($submenu == 'contractCus'){echo'class="current-page"';}?>><a href="<?=base_url('viewer/contract');?>">สัญญาลูกค้า</a></li>
                          <li <?php if($submenu == 'contractService'){echo'class="current-page"';}?>><a href="<?=base_url('viewer/contractService');?>">ประวัติการซ่อมบำรุง</a></li>
                        </ul>
                      </li>
                    </ul> -->
                
              </div>
            </div>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt=""><?php echo $this->session->userdata('userName'); ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <!--<a class="dropdown-item"  href="javascript:;"> Profile</a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">50%</span>
                          <span>Settings</span>
                        </a>
                      <a class="dropdown-item"  href="javascript:;">Help</a>-->
                      <a class="dropdown-item"  href="<?php echo base_url('viewer/user/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
  
                  <!--
                    <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  -->
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

        
