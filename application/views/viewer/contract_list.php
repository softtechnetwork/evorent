<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ค้นหาข้อมูลสัญญาลูกค้า</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="contract_code">รหัสสัญญา</label>
                    <input id="contract_code" name="contract_code" type="text" class="form-control" placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="name">ชื่อลูกค้า</label>
                    <input id="name" name="name" type="text" class="form-control"  placeholder="..." />
                </div>
                <div class ="col-md-2">
                    <label for="name">สถานะ</label>
                    <select name='status' id='status' class="form-control">
                        <option value="" selected='false' disabled>เลือกสะถานะ</option>
                        <?php foreach ($resStatus as $item) : ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="button-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
                <!-- <div class ="col-md-2  navbar-right "  style=" padding-top: 1.65rem;">
                    <a class="btn btn-success" href="<?php echo base_url('viewer/contract/created');?>"  style="color: #ffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มสัญญาลูกค้า
                    </a>
                </div> -->
            </div>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลสัญญาลูกค้า <small></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="contract-table" class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">รหัสสัญญา </th>
                        <th class="column-title">ชื่อลูกค้า </th>
                        <th class="column-title">เบอร์โทรศัพท์ </th>
                        <th class="column-title">สินค้า </th>
                        
                        <th class="column-title">ประเภทสัญญา </th>
                        <!--<th class="column-title">หมายเลขทรัพย์สิน </th>-->
                        <th class="column-title" style="text-align: center;">สถานะ </th>
                        <th class="column-title no-link last"  style="text-align: center;width: 15rem;">
                            <span class="nobr">รายละเอียด</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="pagination" page="1" ></div>
            
        </div>
    </div>
</div>

<div class="row">
    <!-- Modal Add Sub Product -->
    <div class="modal fade" id="contractMode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">เลือกเอกสาร</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--<div class="modal-body"></div>-->
                <div class="modal-footer" style="justify-content: center !important;">
                    <a id="coppy-pdf" class="btn btn-info text-center" href="" target="_blank" style="">คู่ฉบับ</a>
                    <a id="origin-pdf" class="btn btn-primary text-center" href="" target="_blank" style="">ต้นฉบับ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!---- End Content ------>
<script>

    var base_url = $('input[name="base_url"]').val();
    var getRes = base_url+"viewer/contract/contract_GetRes";
    var getAll = base_url+"viewer/contract/getResAll";

    var searchEle = ["#contract_code", "#name", "#status"];
    var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
    var resObj = {"getRes" :getRes, "elementTable":"#contract-table tbody"};
    var allObj = {"getAll":getAll, "functionResName":"Contract_DrawList"};

    Contract_DrawList(resObj, searchEle, pageObj);
    function Contract_DrawList(resObj, searchEle, pageObj){
        var searchJson = SearchJson(searchEle);
        $(resObj["elementTable"]).html(null);
        $.ajax({
            url: resObj["getRes"], //ทำงานกับไฟล์นี้
            data: { 'Search':searchJson, 'itemStt':pageObj['itemStt'], 'itemEnd':pageObj['itemEnd']},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                var tr = null;
                if(data.length > 0){
                    $.each(data, function (i, val) {
                        var no = i+1;
                        tr += '<tr class="even pointer">';
                        tr += '<td class="a-center ">'+no+'</td>';
                        tr += '<td>'+val['contract_code']+'</td>';
                        tr += '<td>'+val['firstname']+' '+val['lastname']+'</td>';
                        tr += '<td>'+val['tel']+'</td>';
                        tr += '<td>'+val['product_name']+'</td>';

                        var contractType_name = '';
                        if(val['contractType_name'] != null && val['contractType_name'] !=''){ contractType_name = val['contractType_name'];}
                        tr += '<td>'+contractType_name+'</td>';

                        //tr += '<td>'+val['product_number']+'</td>';
                        tr += '<td class=" last"  style="text-align: center;">';
                        tr +=     '<button type="button" class="btn btn-round" style="background-color: '+val['background_color']+'; color: '+val['color']+'; font-size: 12px; padding: 0 10px; margin-bottom: inherit;margin-right: inherit;"> '+val['label']+'</button>';
                        tr += '</td>';
                        tr += '<td class=" last"  style="text-align: center;">';
                        // tr +=   '<a class=" col-sm-12 col-md-12 col-lg-3" href="'+base_url+'admin/contract/edit/'+val['contract_code']+' " style="padding-right: 3px;padding-left: 3px;" data-toggle="tooltip" title="แก้ไข">';
                        // tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                        // tr +=   '</a>';
                        
                        tr +=   '<a class="btn btn-round btn-info" href="'+base_url+'viewer/contract/detail/'+val['contract_code']+' " style="font-size: 13px; padding: 0 15px; margin-bottom: inherit;" data-toggle="tooltip" title="รายละเอียด">';
                        tr +=       '<i class="fa fa-file-text-o"></i>';
                        tr +=   '</a>';
                        
                        // tr +=   '<a class=" col-sm-12 col-md-12 col-lg-3" href="'+base_url+'admin/contractaddon/'+val['contract_code']+' " style="padding-right: 3px;padding-left: 3px;" data-toggle="tooltip" title="ส่วนเสริมสัญญา">';
                        // tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i></button>';
                        // tr +=   '</a>';



                        tr +=   '<a class="btn btn-round btn-success" href="'+base_url+'viewer/contract/premise/'+val['contract_code']+' " style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;" data-toggle="tooltip" title="หลักฐาน">';
                        tr +=       '<i class="fa fa-file-image-o"></i>';
                        tr +=   '</a>';

                        var jsonContractPDF = "{'url':'"+base_url+"viewer/contract/contractPDF/', 'id':'"+val['contract_code']+"'}";
                        tr +=     '<button type="button" onclick="contractPDF_fn('+jsonContractPDF+')" class="btn btn-round btn-dark" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"  data-toggle="tooltip" title="เอกสารสัญญา"><i class="fa fa-print"></i></button>';
                        
                        tr += '</td>';
                        tr += '</tr>';
                    });
                    $(resObj["elementTable"]).append(tr);
                }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
    }

    Contract_Pagination(allObj, searchEle, pageObj, resObj);

    function Contract_Pagination(allObj, searchEle, pageObj, resObj){
        $(pageObj['pagination']).html(null);
        $(pageObj['pagination']).attr('page', 1);
        var numAll = getNumAll(allObj["getAll"], searchEle);
       
        var AllPage = Math.ceil(numAll/pageObj['ItemPerPage']) //ปัดขึ้น;
        //-----------html pagination------------//
    
        $(pageObj['pagination']).html(null);

        var btnPage = '';
        btnPage += '<div class="row">';
        btnPage += '<div  class="col-8">';
        btnPage += '<button id="suspend-left"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-left"></i></button>';
        for (let index = 1; index <= AllPage; index++) {
            btnPage += '<button id="'+index+'"  type="button" class="btn btn-warning btn-paginations ">'+index+'</button>';
        }
        btnPage += '<button id="suspend-next"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-right"></i></button>';
        btnPage += '</div>';
        btnPage += '<div  class="col-4">';
        
        btnPage += '<div  id="currentPage" style="float: right;"> <strong>Page :</strong> 1   <strong style="margin-left: 5px;">Of :</strong> '+AllPage+'  <strong style=" margin-left: 15px;">Item </strong> : '+numAll+'</div>';


        btnPage += '</div>';
        btnPage += '</div>';
        
        $(pageObj['pagination']).append(btnPage);
        if(AllPage > 1){
            $("#suspend-next").removeAttr('disabled');
        }
        $('#1').addClass('focus-paginations');

        //-----------btn click------------//
        $('.btn-paginations').click(function(){
            var paged = parseInt($("#pagination").attr("page"));
            if(paged != this.id){
                switch(this.id){
                    case'suspend-left': paged = parseInt(paged)-1; break;
                    case'suspend-next':paged = parseInt(paged)+1; break;
                    default: paged = parseInt(this.id); break;
                }

                $("#pagination").attr("page",paged);
                
                if(paged == 1 ){
                    $("#suspend-left").attr('disabled','disabled');
                    $("#suspend-next").removeAttr('disabled');
                }

                if( paged > 1 && paged < AllPage){
                    $("#suspend-left").removeAttr('disabled');
                    $("#suspend-next").removeAttr('disabled');
                }

                if( paged == AllPage){
                    $("#suspend-left").removeAttr('disabled');
                    $("#suspend-next").attr('disabled','disabled');
                }

                $('.btn-paginations').removeClass('focus-paginations');
                $('#'+paged).addClass('focus-paginations');

                $('#currentPage').html('<strong>Page :</strong> '+paged+ ' <strong style="margin-left: 5px;">Of :</strong> '+AllPage+ ' <strong style="margin-left: 15px;">Item :</strong> '+numAll);



                pageObj['itemStt'] = ((pageObj['ItemPerPage']*paged)-pageObj['ItemPerPage'])+1;
                pageObj['itemEnd'] = pageObj['ItemPerPage']*paged;

                window[allObj["functionResName"]](resObj, searchEle, pageObj); //call res functions
            }


        });
    }
    function getNumAll(url_getAll, searchEle){
        var numAll = null;
        var searchJson = SearchJson(searchEle);
        $.ajax({
            url: url_getAll, //ทำงานกับไฟล์นี้
            data: {  'Search':searchJson },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {numAll = data[0].allitems;
            },
            error: function(xhr, status, exception) {  }
        });
        return numAll;
    }

    function contractPDF_fn(param){
        $('#contractMode .modal-footer #coppy-pdf').attr('href',param.url+'coppy/'+param.id);
        $('#contractMode .modal-footer #origin-pdf').attr('href',param.url+'origin/'+param.id);
        $('#contractMode').modal();
    }

    $('#button-search').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        Contract_DrawList(resObj, searchEle, pageObj);
        Contract_Pagination(allObj, searchEle, pageObj, resObj);
    });
    
    $('#button-reset').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        $.each(searchEle, function (i, val) {
            $(val).val('');
        });
        Contract_DrawList(resObj, searchEle, pageObj);
        Contract_Pagination(allObj, searchEle, pageObj, resObj);
    });

</script>
    