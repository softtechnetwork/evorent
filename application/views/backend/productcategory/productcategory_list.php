
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>ประเภทสินค้า<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
     
        <li>
            <a href="<?php echo base_url('backend/productcategory/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                  
                    <th class="column-title">ประเภทสินค้า</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="3">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
            <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                <?php $i = 0;$count =0;?>
                <?php while ($i < count($productcategory)) { ?>
                    <?php $count = 1+$i;?>
                            <tr class="even pointer">
                        <?php 
                        // echo getproductcategorypicture($productcategory[$i]);
                        ?>
                                <td class=" "><?php echo $productcategory[$i]->cate_name;?></td>
                 
                                <td class=" last">
                                        <a href="<?php echo base_url('backend/productcategory/edit/' . $productcategory[$i]->id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                        <a href="#" onclick="ajaxDeleteproductcategory(<?php echo $productcategory[$i]->id;?>);">
                                            <button type="button" class="btn btn-round btn-danger" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-trash"></i> Delete</button>
                                        </a>
                                    </td>
                            </tr>
                    <?php ++$i ?>
               <?php } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
    
<script type="text/javascript">
    
    $( document ).ready(function() {
      
    });
    
    var base_url = $('input[name="base_url"]').val();
    function ajaxDeleteproductcategory(cate_id){
            $.ajax({
                type: "POST",
                url:  base_url + "/backend/productcategory/delete/" + cate_id,
                data: {cate_id: cate_id},
                success: function(data,status,xhr){
                        var data = JSON.parse(data);
                        if(data.status == true){
                            alert('Delete Succuss !!');
                            window.location.reload(true);
                        }

                }
            });

        }
  </script>