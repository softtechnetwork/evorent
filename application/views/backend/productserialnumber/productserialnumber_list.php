
<!----  Content ------>
<div class="clearfix"></div>
<style>
.pagination {
  /*display: inline-block;*/
}

.pagination a {
  color: black;
  background-color: paleturquoise;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
}
li.active{
  background-color:burlywoord !important;
}

.pagination a:hover:not(.active) {background-color: #ddd;}


</style>
<?php echo form_open_multipart('',array('name'=>'form_serialnumber' , 'id'=>'form_serialnumber'))?>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">

        <h2>หมายเลขเครื่อง<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
        <input type="hidden" name="base_url" value="<?php echo base_url();?>">
        
        <!-- <li style="margin-right: 3px;">
                <select name='dropdownSelectType' id='dropdownSelectType' class="form-control">
                    <option value="productname" >ชื่อสินค้า</option>	
                    <option value="productserialcode" >หมายเลขเครื่อง</option>
                </select>
        </li> -->
        <?php
        //echo '<PRE>';
        //print_r($_POST);
        ?>
        <li style="margin-right: 3px;">
            <!-- <input name="search" id="search" value='<?= $search ?>' type="text"  class="form-control" > -->
            <input list="browsers" name="search" class="form-control"  id="search" value='<?= $search ?>' />
            <datalist id="browsers">
                <?php foreach($product as $k => $v){
                ?>
                <option value="<?php echo $v->mpname;?>">
                <?php
                }
                ?>
                 
            </datalist>
            
        </li>
        <input id="button-search" type='submit' class="btn btn-info" style="border-radius: inherit;" value="Search">
       
        <li style="margin-right: 3px;">
            <a href="#" onclick="importExcel();"  class="form-control" style="color: white;background: #26B99A;border: 1px solid #169F85;">
                <i class="fa fa-plus"></i> Import Excel
            </a>
        </li>&nbsp;
        <li>
            <a href="<?php echo base_url('backend/productserialnumber/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
       
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <?php
            echo "<div align='right' class='text-danger'>ผลลัพท์การค้นหา: ".number_format($total_rows,0,",",",")."</div>";
        ?>
        <table  id="tablestructure" class="display nowrap"  style="width:100%">
            <thead style="   background-color:#405467;color:white;">
                <tr class="headings">
                    <th class="column-title">ชื่อสินค้า</th>
                    <th class="column-title">หมายเลขเครื่อง</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
           
                <?php $i = 0;$count =0;?>
                
                <?php if(!empty($productserialnumber)) {?>
                <?php while ($i < count($productserialnumber)) { ?>
                    <?php $count = 1+$i;?>
                            <tr class="even pointer">
                                <td class=" "><?php echo $productserialnumber[$i]->mpname;?></td>
                                <td class=" "><?php echo $productserialnumber[$i]->mp_serial_number;?></td>
                 
                                <td class=" last">
                                        <a href="<?php echo base_url('backend/productserialnumber/edit/' . $productserialnumber[$i]->masterproduct_serial_id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                        <a href="#" onclick="ajaxDeleteproductserialnumber(<?php echo $productserialnumber[$i]->masterproduct_serial_id;?>);">
                                            <button type="button" class="btn btn-round btn-danger" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-trash"></i> Delete</button>
                                        </a>
                                    </td>
                            </tr>
                    <?php ++$i ?>
               <?php } ?>
               <?php } ?>
          
            
            </tbody>
        </table>
        </div>
                
        <div class="col-md-12" style=""><?php echo $links; ?></div>
    </div>
    </div>
</div>
<?php echo form_close();?>
<!---- End Content ------>
<!-- import excel modal -->
<div id="importExcel" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Import Excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- <form action="<?php echo base_url();?>backend/import" method="post" enctype="multipart/form-data"> -->
      <?php echo form_open('',array('id'=>'form_importfile','method'=>'POST', 'enctype'=>'multipart/form-data'))?>
                <div class="modal-body">
                    <p>ตัวอย่าง format การนำข้อมูลเข้าระบบ</p>
                    <!-- <div class="profile_pic"> -->
                        <img src="<?php echo base_url('./assete/admin/production/images/exam_excel.png');?>" alt="..." class=" ">
                    <!-- </div> -->
                    <input type="file" name="file" id="file" value="" />
                
                </div>
                <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
        <?php echo form_close();?>
    </div>
  </div>
</div>

<!-- eof -->
  <script type="text/javascript">
var base_url = $('input[name="base_url"]').val();
//dtDraw();
//รอเขียนเรื่องสิทธิ์
var user_id = "1"
function ajaxDeleteproductserialnumber(mp_serial_number_id){
        $.ajax({
            type: "POST",
            url:  base_url + "/backend/productserialnumber/delete/" + mp_serial_number_id,
            data: {mp_serial_number_id: mp_serial_number_id},
            success: function(data,status,xhr){
                    var data = JSON.parse(data);
                    if(data.status == true){
                        alert('Delete Succuss !!');
                        window.location.reload(true);
                    }

                }
            });

        }
//submit

$("#form_serialnumber").submit(function (event) {
    event.preventDefault(); 
    var searchString = $('#searchStr').val();
    // append attribute
    $(this).append("<input type='hidden'  name='serialnumberSearch' value='" +searchString+ "'>");

    $(this).unbind('submit').submit(); 

});
//import file submit
$("#form_importfile").submit(function (e) {
    var form = $('#form_importfile')[0];
    var formData = new FormData(form);
    console.log(formData);
    e.preventDefault();
        $.ajax({
        type: "POST",
        url:  base_url + "/backend/productserialnumber/importfile/" + 1,
        processData: false,
        contentType: false,
        data: formData,
        success: function(data,status,xhr){
                var data = JSON.parse(data);
                var alertPromt = "";
                
                if(data.status == true){
                    
                    alert('Import Data Succuss !!');
                    window.location.reload(true);
                }else{
                    alertPromt += data.message;
                    $.each(JSON.parse(data.distict_serial), function( index, value ) {
                        alertPromt += "\n" + value;
                    });
                    alert(alertPromt);
                    window.location.reload(true);

                }
                
                

            }
        });    
});

function importExcel(){
    $('#importExcel').modal('show');
}

// function exportExcel(){
//     $.ajax({
//         type: "POST",
//         url:  base_url + "/backend/productserialnumber/exportFile/" + user_id,
//         processData: false,
//         contentType: false,
//         data: {user_id : user_id},
//         success: function(data,status,xhr){
//                 var data = JSON.parse(data);
//                 var alertPromt = "";
//         }
//     });  
// }


// function dtDraw(){

// var columns = [  
//                 { "width": "30%" , "class": "text-left"},
//                 { "width": "35%" , "class": "text-left"},
//                 { "width": "35%"  , "class": "text-center"},           
//               ]

// //
// var dataTable_ = $('#tablestructure').DataTable( {
//     "processing": true,
//     "bDestroy": true,
//     "bPaginate":true,
//     "bFilter":true,
//     "bInfo" : false,
//     "searching": true,
//     "rowReorder": {
//         selector: 'td:nth-child(0)'
//     },
//     "processing": true,
//     "serverSide": true,
//     "ajax": base_url + "/backend/productserialnumber/processtbl",
//     "deferLoading": 57
//     "responsive": true,
//     "aoColumns": [
//         { "sType": "numeric" },
//         null,
//         null,
//         null
//     ],
//     initComplete: function(){
                 
//      } ,
//     "columns": columns
// });

// return dataTable_;
// }

</script>