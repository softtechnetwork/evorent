<div class="clearfix"></div>
<style>
#overlay{	
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;  
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% { 
    transform: rotate(360deg); 
  }
}
.is-hide{
  display:none;
}
</style>
<div id="overlay" align="center">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>หมายเลขเครื่อง (เพิ่ม)</h2>
                <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul> -->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                <?php echo form_open_multipart('',array('id'=>'create_masterproductserialnumber','name'=>'create-productpart-product'))?>
                
                    <!-- <span class="section">MasterProduct</span> -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6">
                         

                            <select id="masterproduct_id" name="masterproduct_id" class="selectpicker" data-live-search="true" title="Please select">
                            <?php foreach($product as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo ($value->typeproduct_id == 1) ? "(สินค้าหลัก) ".$value->mpname : "(อะไหล่) ". $value->mpname;?></option>
                                <?php }?>
                            </select>

                        </div>
                    </div>

                     <!-- <span class="section">MasterProduct</span> -->
                     <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Serial Number<span class="required">*</span></label>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control"   type="number" id="serial_start" name="mp_serial_number_start"  onchange="onchangestart(this.value)" placeholder="ตัวอย่าง . 1000" required="" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control"  type="number"  id="serial_end" name="mp_serial_number_end" onchange="onchangeend(this.value)"  placeholder="ตัวอย่าง. 1500" required="" />
                            </div>
                        </div>
                    </div>
                
                  
               
                  
                   
                
             
         
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script>
$("#create_masterproductserialnumber").submit(function (e) {
    var form = $('#create_masterproductserialnumber')[0];
    var formData = new FormData(form);
    var base_url = $('input[name="base_url"]').val();
    console.log(formData);
    e.preventDefault();
    $("#overlay").fadeIn(300);
        $.ajax({
        type: "POST",
        url:  base_url + "/backend/productserialnumber/create_newserialnumber",
        processData: false,
        contentType: false,
        data: formData,
        success: function(data,status,xhr){
                var data = JSON.parse(data);
                var alertPromt = "";
                $("#overlay").fadeOut(300);
                if(data.status == true){
                    alert('Import Data Succuss !!');
                    window.location.reload(true);
                }else{
                    alert('กรุณาตรวจสอบข้อมูล');

                }
                
                

            }
        });    
});
</script>