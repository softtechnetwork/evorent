<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>หมายเลขเครื่อง (แก้ไข)</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
               
                $product_serial_number = $productserialnumber[key($productserialnumber)];
           
                ?>
                <?php echo form_open_multipart('',array('name'=>'create-productpart-product'))?>
                   
                    <!-- <span class="section">MasterProduct</span> -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <!-- <select name='masterproduct_id' class="form-control">
                                <?php foreach($product as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>" <?php echo ($product_serial_number->id == $value->id) ? "selected" : ""; ?>><?php echo $value->mpname;?></option>
                                <?php }?>
                            </select> -->
                            <select id="masterproduct_id" name="masterproduct_id" class="selectpicker" data-live-search="true" title="Please select">
                            <?php foreach($product as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>" <?php echo ($product_serial_number->id == $value->id) ? "selected" : ""; ?>><?php echo ($value->typeproduct_id == 1) ? "(สินค้าหลัก)".$value->mpname : "(อะไหล่)". $value->mpname;?></option>
                                <?php }?>
                            </select>

                        </div>
                    </div>

                     <!-- <span class="section">MasterProduct</span> -->
                     <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Serial Number<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="mp_serial_number" placeholder="885010002 " value="<?php echo $product_serial_number->mp_serial_number;?>" required="" />
                        </div>
                    </div>
                
                  
               
                  
                   
                
             
         
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


