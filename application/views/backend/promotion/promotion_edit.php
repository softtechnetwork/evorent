<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Promotion <small>(โปรโมชั่น)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <?php echo form_open_multipart('',array('name'=>'create-productpart-product'))?>
                   
                    <!-- <span class="section">MasterProduct</span> -->
                    <span id="group_1" class="interest_group">
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อโปรโมชั่น<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control"   id="promotion_name_group1" name="promotion_name_group1" placeholder="ผ่อน 0% นาน 12 เดือน " value="<?php echo $promotion->promotion_name;?>" required="" />
                            </div>
                        </div>
                
                        <div class="field item  form-group ">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">ช่วงเวลาเริ่มต้น - สิ้นสุด<span class="required">*</span></label>
                        
                          

                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                        <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="datetime_start" value="<?php echo $promotion->start_date;?>" name="datetime_start" placeholder="" aria-describedby="datetime_start">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="datetime_start" class="sr-only">(success)</span>
                                        </div>
                                        </div>
                                    </div>
                                </fieldset>
                               
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                        <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="datetime_end"  value="<?php echo $promotion->end_date;?>" name="datetime_end" placeholder="" aria-describedby="datetime_end">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="datetime_end" class="sr-only">(success)</span>
                                        </div>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>
                        <div class="field item form-group">
                            <label for="is_downcondition_group1" class="col-form-label col-md-3 col-sm-3  label-align">โปรโมชั่นนี้ต้องวางเงินดาวน์หรือไม่ ?<span class=""></span></label>
                            <p style="padding-left: 30px;padding-top:5px;">
                            <input type="checkbox" name="is_downcondition_group1"  onclick="cb_downpayment_onchange($(this) , 'group1');" id="is_downcondition_group1" value="" class="form-check-input" /> 
                        </div>
      
                        <div class="field item form-group" id="down_payment_group1" style="display:none;">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">เงินดาวน์ (%)</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="number" class='number' id="down_payment_percent_group1"  step="0.1" name="down_payment_percent_group1"   placeholder="0.25"  />
                            </div>
                        </div>
                        <fieldset class=" text-white bg-info "  id="fieldset_interest">
                            <!-- <div class="field item form-group mt-2" id="d_payment_in_instalment_group1" style="">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">เริ่มผ่อนงวดที่เท่าไหร่ - ถึงงวดเท่าไหร่</label>
                                <div class="col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="start_payment_in_instalment_group1"  name="startpaymentininstalmentgroup_1" data-validate-minmax="1,999999"  placeholder="1" required="" />
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="end_payment_in_instalment_group1"  name="endpaymentininstalmentgroup_1" data-validate-minmax="1,999999"  placeholder="12" required="" />
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <span class="btn btn-success" style="cursor:pointer;" onclick="createInterestGroup();"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                </div>
                            </div>

                            <div class="field item form-group" id="d_interest_group1" style="">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">ดอกเบี้ย (%)</label>
                                <div class="col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="interest_group1"  name="interestgroup_1" step=".01" placeholder="0.25" required="" />
                                </div>
                            
                            </div> -->


                        </fieldset>
        
                        <!-- # obj (instalment_obj)
                            //start_date datetime
                            //end_date  datetime
                            //start_payment_in_instalment int //ผ่อนตั้งแต่งวดที่เท่าไหร่
                            //end_payment_in_instalment int // ถึงงวดที่เท่าไหร่
                            //amount_installments_period_id int
                            //interest  numeric(18, 2) //ดอกเบี้ย
                            //down_payment_percent  numeric(18, 2) // เงื่อนไข ดาวน์กี่เปอ
                            //is_downcondition int // สถานะกำกับว่าต้องดาวน์ไหม ถึงจะเข้าเงื่อนไข
                        #eof -->

                
                    </span>
         
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script>
var promotion  = <?php echo json_encode($promotion)?>;
</script>