
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>โปรโมชั่น<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
     
        <li>
            <a href="<?php echo base_url('backend/promotion/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th>ลำดับ</th>
                    <th class="column-title">โปรโมชั่น</th>
                    <th>ระยะเวลาโปรโมชั่น</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
           
                <?php $i = 0;$count =0;?>
                <?php if(!empty($promotion)) {?>
                    <?php while ($i < count($promotion)) { ?>
                        <?php $count = 1+$i;?>
                                <tr class="even pointer">
                                   
                                    <td><?php echo $i + 1;?></td>
                                    <td class=" "><?php echo $promotion[$i]->promotion_name;?></td>
                                    <td class=" "><?php echo date_format(date_create($promotion[$i]->start_date),"Y/m/d") .' ถึง ' .date_format(date_create($promotion[$i]->end_date),"Y/m/d");?></td>
                                    <td class=" last">
                                        <a href="<?php echo base_url('backend/promotion/edit/' . $promotion[$i]->id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                    </td>
                                </tr>
                        <?php ++$i ?>
                    <?php } ?>
                <?php } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  