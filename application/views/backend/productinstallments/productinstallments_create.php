<div class="clearfix"></div>
<style>

table.dataTable thead tr {
    background-color:#405467;color:white;
    height: 75px ;
}
td{
    color:black !important;
}
</style>
<div class="spinner-border" id="loadingDiv" style="display:none;"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>จัดการผ่อนชำระสินค้า <small>(เพิ่ม)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php echo form_open_multipart('',array('name'=>'create-productinstallments-product' , 'id'=>'create_productinstallments_product'))?>
               <input type="hidden" id="holdonpayment" name="holdonpayment[]" value="">
                <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <!-- <span class="section">MasterProduct</span> -->
                    <span id="group_1" class="interest_group">
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้าตั้งขาย</label>
                            <div class="col-md-6 col-sm-6">
                                <select id="product_install_id" name='product_install_id' class="form-control">
                                <?php
                                //echo '<PRE>';
                                //print_r($product);exit();
                                ?>
                                <?php foreach($product_install as $key => $value ) {?>
                                        <option value="<?php echo $value->id;?>" data-price="<?php echo $value->price_a;?>"><?php echo $value->piname;?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                          
                            <label class="col-form-label col-md-3 col-sm-3  label-align">คำนวณดอกเบี้ย*</label>
                            <div class="col-md-6 col-sm-6 mt-2">
                                <?php foreach($formula as $fk => $fv){?>
                                    <input id="formula_<?php echo $fv->id;?>" name="formula" type="radio" class="" onchange="display_group(<?php echo $fv->id;?>)"  value="<?php echo $fv->id;?>" <?php echo $this->form_validation->set_radio('formula', $fv->id); ?> />
                                    <label for="formula_<?php echo $fv->id;?>" class=""><?php echo $fv->math_formula_name;?></label>
                                    &nbsp;

                                <?php } ?>
                            </div>
                        </div>
                        <fieldset  class="col-12" id="formula_group">

                        </fieldset>
                       
                        <fieldset class=" text-white bg-info col-12"  id="fieldset_interest">
                            <div class="field item form-group mt-2" id="d_payment_in_instalment_group1" style="">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">ผ่อนทั้งหมดกี่งวด</label>
                                <!-- <div class="col-md-2 col-sm-2" > -->
                                    <input class="form-control" type="hidden" class='number' id="start_payment_in_instalment_group1" value="1"  name="startpaymentininstalmentgroup_1" data-validate-minmax="1,999999"  placeholder="1" required="" />
                                <!-- </div> -->
                                <div class="col-10 col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="end_payment_in_instalment_group1"  name="endpaymentininstalmentgroup_1"  value="18" min="1" step="1" required="" />
                                </div>
                                
                                
                                <div class="col-2 col-md-2 col-sm-2">
                                    <span class="btn btn-success" style="cursor:pointer;" onclick="createInterestGroup();"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                </div>
                                
                                
                            </div>
                            <!--------------------------------------------->
                            <div class="field item form-group mt-2" id="d_payment_in_instalment_group1" style="">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">งวดละ </label>
                                <div class="col-10 col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="instalment_1" name="instalment_1" value="500" min="1" required="" />
                                </div>
                                <label class="col-form-label col-md-3 col-sm-3">บาท</label>
                            </div>
                            <!--------------------------------------------->
                            <div class="field item form-group">
                                    <label for="ishelppaygroup_1" class="col-form-label col-9 col-md-3 col-sm-3  label-align">ต้องการพักชำระหนี้ ?<span class=""></span></label>
                                    <label style="padding-left: 30px;padding-top:5px;">
                                        <input type="checkbox" name="ishelppaygroup_1"  onclick="cb_helppay_onchange($(this) , '1');" id="ishelppaygroup_1" value="1" class="form-check-input col-3" /> 
                                    </label>
                            </div>
                            <span id="spanholdonpaymentgroup_1">
                             
                            </span>
                            <div class="field item form-group" id="d_interest_group1" style="">
                                <!--
                                <label class="col-form-label col-md-3 col-sm-3  label-align">ดอกเบี้ย (%)</label>
                                <div class="col-md-2 col-sm-2">
                                    <input class="form-control" type="number" class='number' id="interest_group1" onchange="checkinterest(this.value,1)" onkeyup="checkinterest(this.value,1)" value="'+value+'" name="interestgroup_1"   name="interestgroup_1" step=".01" placeholder="0.25" required="" />
                                </div>
                                <span id="buttoncalculategroup_1" class="btn btn-secondary text-center d-flex text-white" onclick="drawtablecalculate(1)" style="cursor:pointer;">คำนวณยอดที่ต้องผ่อน</span>
                               -->
                            </div>


                        </fieldset>
                     
                        <fieldset class=" text-white  "  id="fieldset_productinstallments" style="display:none;">
                            <div class="table-responsive mt-2">
                                <!-- <table class="table table-striped table-hover dt-responsive display nowrap" id="tablestructure" cellspacing="0">
                                -->
                                <table  id="tablestructure" class="display nowrap"  style="width:100%">
                                <thead style="   background-color:#405467;color:black;">
                                    <!--
                                        <tr>
                                        <th ><?php echo __('งวดที่')?></th>
                                        <th class="no-sort"><?php echo __('ราคาตั้งขาย ')?></th>
                                        <th class="no-sort"><?php echo __('ดอกเบี้ย')?></th>
                                        <th class="no-sort"><?php echo __('ยอดที่ต้องผ่อนชำระ/เดือน(ไม่รวมvat)')?></th>
                                        <th class="no-sort"><?php echo __('ยอดที่ต้องผ่อนชำระ/เดือน(รวมvat)')?></th>
                                    </tr>-->
                                    
                                    
                                    <tr>
                                        <th ><?php echo __('งวดที่')?></th>
                                        <th class="no-sort"><?php echo __('ยอดที่ต้องผ่อนชำระ/เดือน(ไม่รวมvat)')?></th>
                                        <th class="no-sort"><?php echo __('ยอดที่ต้องผ่อนชำระ/เดือน(รวมvat)')?></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyData" >                  
                                </tbody>
                                <tfoot id="footerData" >
                                  
                                </tfoot>
                                </table>
                            </div>
                        </fieldset>
                      
                        <!-- # obj (instalment_obj)
                            //start_date datetime
                            //end_date  datetime
                            //start_payment_in_instalment int //ผ่อนตั้งแต่งวดที่เท่าไหร่
                            //end_payment_in_instalment int // ถึงงวดที่เท่าไหร่
                            //amount_installments_period_id int
                            //interest  numeric(18, 2) //ดอกเบี้ย
                            //down_payment_percent  numeric(18, 2) // เงื่อนไข ดาวน์กี่เปอ
                            //is_downcondition int // สถานะกำกับว่าต้องดาวน์ไหม ถึงจะเข้าเงื่อนไข
                        #eof -->

                
                    </span>
         
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3 text-center">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var formula_obj = <?php echo json_encode($formula);?>;
    var product_install = <?php echo json_encode($product_install);?>;
</script>
