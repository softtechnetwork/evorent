

<!----  Content ------>
<style>
table.dataTable thead tr {
    background-color:#405467;color:white;
}
</style>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>จัดการผ่อนชำระสินค้า<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
     
        <li>
            <a href="<?php echo base_url('backend/productinstallments/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table id="tablestructure" class="display nowrap"  style="width:100%">
            <thead>
                <tr class="headings">
                    <th class="column-title" style="width: 35px !important;">ลำดับ </th>
                    <th class="column-title">รหัสสินค้า </th>
                    <th class="column-title">ชื่อสินค้า</th>
                    <th class="column-title">ชื่อย่อ</th>
                  
                    <!-- <th class="column-title">สถานะสินค้าคงเหลือ</th> -->
                    <!-- <th class="column-title">ราคาตั้งต้น</th> -->
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
            <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                <?php $i = 0;$count =0;?>
                <?php if(!empty($product)){?>
                    <?php while ($i < count($product)) { ?>
                        <?php $count = 1+$i;?>
                                <tr class="even pointer">
                             
                                    <td class=" "><?php echo $count;?></td>
                                    <td class=" "><?php echo $product[$i]->picode;?></td>
                                    <td class=" "><?php echo $product[$i]->piname;?></td>
                                    <td class=" "><?php echo $product[$i]->piabbrv;?></td>
                                    
                                    <!-- <td class=" "><?php echo ($product[$i]->out_of_stock == 0 ) ? "คงเหลือสินค้า" : "หมดสต็อก";?></td>
                                     -->
                                    <!-- <td class="a-right a-right "><?php echo ($product[$i]->price_a > 0 ) ? number_format($product[$i]->price_a,2) : 0;?></td>
                                     -->
                                    <td class=" last">
                                        <a href="<?php echo base_url('backend/productinstallments/edit/' . $product[$i]->id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                        <a href="#" onclick="ajaxDeleteproductinstallments(<?php echo $product[$i]->id;?>);">
                                            <button type="button" class="btn btn-round btn-danger" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-trash"></i> Delete</button>
                                        </a>
                                    </td>
                                </tr>
                        <?php ++$i ?>
                <?php } ?>
               <?php } ?>
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  
  <script type="text/javascript">
    $( document ).ready(function() {
     

    });
    var base_url = $('input[name="base_url"]').val();
    function ajaxDeleteproductinstallments(product_id){
            $.ajax({
                type: "POST",
                url:  base_url + "/backend/productinstallments/delete/" + product_id,
                data: {product_id: product_id},
                success: function(data,status,xhr){
                        var data = JSON.parse(data);
                        if(data.status == true){
                            alert('Delete Succuss !!');
                            window.location.reload(true);
                        }

                }
            });

        }

        var dataTable_ = $('#tablestructure').DataTable( {
        "processing": true,
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":true,
        "bInfo" : false,
        "searching": true,
        
      
        // "responsive": true,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
       
        initComplete: function(){
                     
         } ,
       
        
    });
  </script>