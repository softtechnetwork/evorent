<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Product Part <small>(อะไหล่สินค้า)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <?php echo form_open_multipart('',array('name'=>'create_product_part',"id"=>"create_product_part"))?>
                    <div id="hidden_div"></div>
                    
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                   
                  
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้าหลัก</label>
                        <div class="col-md-6 col-sm-6">
                            
                            <select id="product_parent_id" name='product_parent_id' class="selectpicker" data-live-search="true" title="Please select">
                            <?php foreach($product as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->mpname;?></option>
                                <?php }?>
                            </select>

                            
                        </div>
                    </div> -->
               

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่ออะไหล่<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="mpname" placeholder="คอยล์เย็น" required="" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อย่อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="mpabbrv" placeholder="กล้อง OCS รุ่น 2"  />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="brand" placeholder="Maxtech"  />
                        </div>
                    </div>

               
                    
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='mpgroupproduct' class="form-control">
                                <?php foreach($product_category as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->cate_name;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                -->

                
             
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วยขาย</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="uom" placeholder="ตัว"  />
                        </div>
                    </div> -->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วย</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='uom' class="form-control">
                                <?php foreach($uom as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->uom_nameth;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    
               
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา A</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' name="price_a" data-validate-minmax="0,999999"></div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา B</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' name="price_b" data-validate-minmax="0,999999"></div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา C</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' name="price_c" data-validate-minmax="0,999999"></div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา D</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' name="price_d" data-validate-minmax="0,999999"></div>
                    </div>
                    <?php
                    $warranty_type = array("month" => "เดือน" , "day" => "วัน", "year" => "ปี")
                    ?>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประกันสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='warranty_id' class="form-control">
                                <?php foreach($warranty as $key => $v ) {?>
                                    <option value="<?php echo $v->id;?>"><?php echo $v->value." ".$warranty_type[$v->type];?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">รูป*</label>
                        <!-- <div class=" col-md-6 col-sm-6 file-loading">
                            <?php echo  form_upload('cover_image[]', '', 'multiple="multiple" accept="image/*"');?>
                        </div> -->
                        <div class="dropzone col-md-6 col-sm-6  clsbox" id="dropzone_uploadimg">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด</label>
                        <div class="col-md-6 col-sm-6"><textarea  class="form-control"  name='mp_desc'></textarea></div>
                    </div>  
                    
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3 text-center">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>



