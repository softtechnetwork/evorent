
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>Customer<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
        <li>
            <a href="<?php echo base_url('admin/customer/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <!--<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>-->
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
            <tr class="headings">
                <th class="column-title">ลำดับ</th>
                <th class="column-title">ชื่อ </th>
                <th class="column-title">ที่อยู่ </th>
                <th class="column-title">หมายเลขบัตรประชาชน </th>
                <th class="column-title">เบอร์โทรศัพท์ </th>
                <th class="column-title">อีเมล์ </th>
                <th class="column-title">เพศ </th>
                <th class="column-title no-link last"><span class="nobr">Action</span>
                </th>
                <th class="bulk-actions" colspan="7">
                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                </th>
            </tr>
            </thead>

            <tbody>
                <?php  for ($x = 1; $x <= 10; $x++) { ?> 
                    
                    <tr class="even pointer">
                        <td class="a-center "><?php echo $x; ?></td>
                        <td class=" ">121000040</td>
                        <td class=" ">May 23, 2014 11:47:56 PM </td>
                        <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                        <td class=" ">John Blank L</td>
                        <td class=" ">Paid</td>
                        <td class="a-right a-right ">$7.45</td>
                        <td class=" last">
                            <a href="#">
                                <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                            </a>
                        </td>
                    </tr>
                    
                <?php } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  