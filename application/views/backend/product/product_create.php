<div class="clearfix"></div>
<style>

table.dataTable thead tr {
    background-color:#405467;color:white;
    height: 75px ;
}
td{
    color:black !important;
}
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>MasterProduct <small>(สินค้าหลัก)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <?php echo form_open_multipart('',array('name'=>'create-master-product',"id"=>"create_master_product"))?>
                    <div id="hidden_div"></div>
                    
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <!-- <span class="section">MasterProduct</span> -->
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้า<span class="required">(ถ้ามี)</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" data-validate-length-range="9"  name="mpcode" placeholder="885010001" />
                        </div>
                    </div> -->
                    <span id="group_1" class="fieldset_productinstall">
                        <fieldset class=" text-white  "  id="fieldset_productinstall">
                            
                          <div class="table-responsive mt-2">
                                <table  id="tablestructure" class="display nowrap"  style="display:none;width:100%">
                                <thead style="   background-color:#405467;color:white;">
                                    <tr>
                                  
                                        <th><?php echo __('รหัสอะไหล่ในชุด')?></th>
                                        <th><?php echo __('ราคาตั้งต้น')?></th>
                                        <!-- <th><?php echo __('ราคา B')?></th>
                                        <th><?php echo __('ราคา C')?></th>
                                        <th><?php echo __('ราคา D')?></th> -->
                                        <th><span class="btn btn-success" style="cursor:pointer;font-size:small;" onclick="addproductpart_newrow();">เพิ่มข้อมูล</span></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyData" >                  
                                </tbody>
                                </table>
                            </div>

                        </fieldset>
                        <br>
                  
                    </span>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="mpname" placeholder="HIKVISION กล้องวงจรปิด 2 ล้าน พิกเซล " required="" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า(ย่อ)</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="mpabbrv" placeholder="กล้อง OCS รุ่น 2"  />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="brand" placeholder="Maxtech"  />
                        </div>
                    </div>

               
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='product_category_id' class="form-control">
                                <?php foreach($product_category as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->cate_name;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
               

                
             
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วยขาย</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="uom" placeholder="ตัว"  />
                        </div>
                    </div> -->

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วย</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='uom' class="form-control">
                                <?php foreach($uom as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->uom_nameth;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
               

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคาตั้งต้น</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_a"  name="price_a" data-validate-minmax="0,999999" >
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_a"></label>
                        </div>
                    </div>
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา B </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_b"  name="price_b" data-validate-minmax="0,999999" >
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_b"></label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา C </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_c"  name="price_c" data-validate-minmax="0,999999" ></div>
                            <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_c"></label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา D </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_d"  name="price_d" data-validate-minmax="0,999999" ></div>
                            <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_d"></label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_d"></label>
                        </div>
                    </div> -->
                    
                    <?php
                    $warranty_type = array("month" => "เดือน" , "day" => "วัน", "year" => "ปี")
                    ?>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประกันสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='warranty_id' class="form-control">
                                <?php foreach($warranty as $key => $v ) {?>
                                    <option value="<?php echo $v->id;?>"><?php echo $v->value." ".$warranty_type[$v->type];?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">รูป*</label>
                        <!-- <div class=" col-md-6 col-sm-6 file-loading">
                            <?php echo  form_upload('cover_image[]', '', 'multiple="multiple" accept="image/*"');?>
                        </div> -->
                        <div class="dropzone col-md-6 col-sm-6  clsbox" id="dropzone_uploadimg">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด</label>
                        <div class="col-md-6 col-sm-6"><textarea  class="form-control"  name='mp_desc'></textarea></div>
                    </div>  
                    <div class="ln_solid ">
                        <div class="form-group ">
                            <div class="col-md-6 offset-md-3 text-center">
                                <button type='submit' class="btn btn-primary ">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
var product = <?php echo json_encode($tbl_productpart);?>;


</script>


