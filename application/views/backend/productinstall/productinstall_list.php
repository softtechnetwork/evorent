
<!----  Content ------>
<style>
table.dataTable thead tr {
    background-color:#405467;color:white;
}
</style>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>ติดตั้งโครงสร้างสินค้า<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
     
        <li>
            <a href="<?php echo base_url('backend/productinstall/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table id="tablestructure" class="display nowrap"  style="width:100%">
            <thead>
                <tr class="headings">
                    <th class="column-title" style="width: 35px !important;">ลำดับ</th>
                    <th class="column-title">รูปภาพ</th>
                    <th class="column-title">รหัสสินค้า </th>
                    <th class="column-title">ชื่อสินค้า</th>
                    <th class="column-title">ยี่ห้อ</th>
                    <th class="column-title">รุ่น</th>
                   
                    <!-- <th class="column-title">สถานะสินค้าคงเหลือ</th> -->
                    <th class="column-title">ราคาตั้งต้น</th>
                    <!-- <th class="column-title">ราคา B</th>
                    <th class="column-title">ราคา C</th>
                    <th class="column-title">ราคา D</th> -->
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
            <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                <?php $i = 0;$count =0;?>
                <?php
                if(!empty($product_install)){
                ?>
                    <?php while ($i < count($product_install)) { ?>
                        <?php $count = 1+$i;?>
                                <tr class="even pointer">
                                    <td class=" "><?php echo $count;?></td>
                                    <?php
                                    $pic = !empty($product_install[$i]->pic) ? json_decode($product_install[$i]->pic) : "";
                                    if(!empty($pic)){
                                        if(count($pic) > 0){
                                                //  $product_install[$i]->pic = array_shift(array_values($pic));
                                                // set default pic
                                                $product_install[$i]->pic = $pic[0];
                                        }
                                    }
                                    ?>
                                    <td><a href="javascript:void(0);" class="pop"><img src="<?php echo getproductinstallpicture($product_install[$i])?>" width="50" height="50"></a></td>
                                    <td class=" "><?php echo $product_install[$i]->picode;?></td>
                                    <td class=" "><?php echo $product_install[$i]->piname;?></td>
                                    <td class=" "><?php echo $product_install[$i]->brand;?></td>
                                    <td class=" "><?php echo $product_install[$i]->piabbrv;?></td>
                
                                    <!-- <td class=" "><?php echo ($product_install[$i]->out_of_stock == 0 ) ? "คงเหลือสินค้า" : "หมดสต็อก";?></td>
                                    -->
                                    <td class="a-right a-right "><?php echo ($product_install[$i]->price_a > 0 ) ? number_format($product_install[$i]->price_a,2) : 0;?></td>
                                    <!-- <td class="a-right a-right "><?php echo ($product_install[$i]->price_b > 0 ) ? number_format($product_install[$i]->price_b,2) : 0;?></td>
                                    <td class="a-right a-right "><?php echo ($product_install[$i]->price_c > 0 ) ? number_format($product_install[$i]->price_c,2) : 0;?></td>
                                    <td class="a-right a-right "><?php echo ($product_install[$i]->price_d > 0 ) ? number_format($product_install[$i]->price_d,2) : 0;?></td>
                                     -->
                                    <td class=" last">
                                        <a href="<?php echo base_url('backend/productinstall/edit/' . $product_install[$i]->id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                        <a href="#" onclick="ajaxDeleteproductinstall(<?php echo $product_install[$i]->id;?>);">
                                            <button type="button" class="btn btn-round btn-danger" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-trash"></i> Delete</button>
                                        </a>
                                    </td>
                                </tr>
                        <?php ++$i ?>
                <?php } ?>
               <?php } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  
  <script type="text/javascript">
    
    $( document ).ready(function() {
      
    });
    
    var base_url = $('input[name="base_url"]').val();
    function ajaxDeleteproductinstall(product_id){
            $.ajax({
                type: "POST",
                url:  base_url + "/backend/productinstall/delete/" + product_id,
                data: {product_id: product_id},
                success: function(data,status,xhr){
                        var data = JSON.parse(data);
                        if(data.status == true){
                            alert('Delete Succuss !!');
                            window.location.reload(true);
                        }

                }
            });

        }

        
        var dataTable_ = $('#tablestructure').DataTable( {
        "processing": true,
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":true,
        "bInfo" : false,
        "searching": true,
        
      
        // "responsive": true,
        rowReorder: {
            selector: 'td:nth-child(0)'
        },
        responsive: true,
       
        initComplete: function(){
                     
         } ,
       
        
    });
  </script>