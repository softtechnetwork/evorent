<div class="clearfix"></div>
<style>
.dropdown-menu {
    position: static;
    z-index:10001 !important;
}

/* .table-responsive {
    width: 100%;
  margin-bottom: 15px;
   overflow-y: visible;  // Add overflow-y visible
   overflow-x: scroll; 
  -ms-overflow-style: -ms-autohiding-scrollbar;
  border: 1px solid #ddd;
  -webkit-overflow-scrolling: touch; */
} */
td{
    color:black !important;
}
</style>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>ติดตั้งโครงสร้างสินค้า<small>(เพิ่ม)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content col-md-12">
            
                <?php echo form_open_multipart('',array('name'=>'create-master-productinstall',"id"=>"create_master_productinstall"))?>
                    <div id="hidden_div"></div>
                    
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <!-- <span class="section">MasterProduct</span> -->
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้า<span class="required">(ถ้ามี)</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" data-validate-length-range="9"  name="mpcode" placeholder="885010001" />
                        </div>
                    </div> -->
                <!-- div: product detail -->
                <div class="col-md-12 col-sm-12">    
             
                    <span id="group_1" class="fieldset_productinstall">
                        <fieldset class=" text-white  "  id="fieldset_productinstall">
                            
                          <div class="table-responsive mt-2">
                                <table class="table table-bordered" id="tablestructure" cellspacing="0">
                                <thead>
                                    <tr>
                                  
                                        <th><?php echo __('รหัสสินค้าในชุด')?></th>
                                        <th><?php echo __('ราคาตั้งต้น')?></th>
                                        <!-- <th ><?php echo __('ราคาB/หน่วย')?></th>
                                        <th><?php echo __('ราคาC/หน่วย')?></th>
                                        <th><?php echo __('ราคาD/หน่วย')?></th> -->
                                        <th><?php echo __('จำนวน')?></th>
                                    
                                        <!-- <th><?php echo __('Active Date')?></th>
                                        <th><?php echo __('Expire Date')?></th> -->
                                        <th><span class="btn btn-success" style="cursor:pointer;font-size:small;" onclick="addnewrow();">เพิ่มข้อมูล</span></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyData" >                  
                                </tbody>
                                </table>
                            </div>

                        </fieldset>
        
                  
                    </span>

                    <div class="field item form-group mt-2">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control"   name="piname" placeholder="HIKVISION กล้องวงจรปิด 2 ล้าน พิกเซล " required="" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า(ย่อ)</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control"   name="piabbrv" placeholder="กล้อง OCS รุ่น 2"  />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="brand" placeholder="Maxtech" />
                        </div>
                    </div>


               
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <select name='product_category_id' class="form-control">
                                <?php foreach($product_category as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->cate_name;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
               

                
             
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วย</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='uom' class="form-control">
                                <?php foreach($uom as $key => $value ) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->uom_nameth;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                                    


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_a"  name="price_a" data-validate-minmax="0,999999" >
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_a"></label>
                        </div>
                    </div>
                    <!-- <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา B </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_b"  name="price_b" data-validate-minmax="0,999999" >
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_b"></label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา C </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_c"  name="price_c" data-validate-minmax="0,999999" ></div>
                            <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_c"></label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา D </label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="number" class='number' id="price_d"  name="price_d" data-validate-minmax="0,999999" ></div>
                            <div class="col-md-3 col-sm-3">
                            <label class="col-form-label col-md-3 col-sm-3  label-align" id="label_sum_price_d"></label>
                        </div>
                    </div> -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">รูป*</label>
                        <!-- <div class=" col-md-8 col-sm-8 file-loading">
                            <?php echo  form_upload('cover_image[]', '', 'multiple="multiple" accept="image/*"');?>
                        </div> -->
                        <div class="dropzone col-md-8 col-sm-8  clsbox" id="dropzone_uploadimg">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด</label>
                        <div class="col-md-8 col-sm-8"><textarea  class="form-control"  name='pidesc'></textarea></div>
                    </div>  
                </div>
                 <!-- eof div:product detail -->

                 <!-- div: product install -->
                
                <!-- eof div : product install -->
               
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3 text-center">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>

           
        </div>
    </div>
</div>

<script type="text/javascript">
var product = <?php echo json_encode($product);?>;

</script>

