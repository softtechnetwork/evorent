<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>uom (แก้ไข)</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <?php echo form_open_multipart('',array('name'=>'create-productpart-product'))?>
                   
                    <?php
                    $uom = $uom[key($uom)];
                    ?>

                     <!-- <span class="section">MasterProduct</span> -->
                     <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วย (th)<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="uom_nameth" placeholder="ตัว " value="<?php echo $uom->uom_nameth;?>" required="" />
                        </div>
                    </div>
                
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หน่วย (en)<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control"   name="uom_nameen" placeholder="unit " value="<?php echo $uom->uom_nameen;?>" required="" />
                        </div>
                    </div>

         
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script>

</script>