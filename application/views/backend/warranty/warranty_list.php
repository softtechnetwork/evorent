
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>ประเภทการรับประกันสินค้า<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
        <input type="hidden" name="base_url" value="<?php echo base_url();?>">
        <li>
            <a href="<?php echo base_url('backend/warranty/create');?>" class="collapse-link" style="color: #415468;">
                <i class="fa fa-plus"></i> Add
            </a>
        </li>
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

        </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                  
               
                    <th class="column-title">ระยะเวลารับประกันสินค้า</th>
                   
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
            </thead>

            <tbody>
           
                <?php $i = 0;$count =0;?>
                <?php while ($i < count($warranty)) { ?>
                    <?php $count = 1+$i;?>
                            <tr class="even pointer">
                        <?php 
                        // echo getuompicture($warranty[$i]);
                       
                        $warranty_type_ = $warranty_type[$warranty[$i]->type];
                        ?>
                                <td class=" "><?php echo  $warranty[$i]->value . ' ' . $warranty_type_?></td>
                                
                 
                                <td class=" last">
                                        <a href="<?php echo base_url('backend/warranty/edit/' . $warranty[$i]->id);?>">
                                            <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                                        </a>
                                        <!-- <a href="#" onclick="ajaxDeleteuom(<?php echo $warranty[$i]->id;?>);">
                                            <button type="button" class="btn btn-round btn-danger" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-trash"></i> Delete</button>
                                        </a> -->
                                    </td>
                            </tr>
                    <?php ++$i ?>
               <?php } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  <script type="text/javascript">
var base_url = $('input[name="base_url"]').val();
function ajaxDeleteuom(uom_id){
 
        $.ajax({
                type: "POST",
                url:  base_url + "/backend/uom/checkalreadyused/" + uom_id,
                data: {id: uom_id},
                success: function(data,status,xhr){
                    var data = JSON.parse(data);
                    if(data.status == true){
                            var masterproduct_parse  = JSON.parse(data.masterproduct);
                            var productinstall_parse = JSON.parse(data.productinstall);
                            var alertPromt = "";
                            alertPromt += "ไม่สามารถลบหน่วยขายนี้ได้ เนื่องจากพบสินค้าที่ผูกกับหน่วยการขายชนิดนี้อยู่"
                            //เช็คว่ามีสินค้าผูกอยู่กับหน่วยการขายนี้ไหม
                            if(masterproduct_parse.length > 0){
                                if(masterproduct_parse.length > 10) {
                                    alertPromt += "\nสินค้า : " + masterproduct_parse.length + ' รายการ';
                                }else{
                                    alertPromt += "\nสินค้า : "
                                    for (var i=0; i<masterproduct_parse.length; i++)
                                    {
                                        
                                        alertPromt += "\n"+ "  " + (i + 1) +")" +  masterproduct_parse[i];
                                    }
                                }
                            }
                            if(productinstall_parse.length > 0){
                                if(productinstall_parse.length > 10) {
                                    alertPromt += "\nสินค้าตั้งขาย" + productinstall_parse.length + " รายการ";
                                }else{
                                    alertPromt += "\nสินค้าตั้งขาย : "
                                    for (var i=0; i<productinstall_parse.length; i++)
                                    {
                                      
                                        alertPromt += "\n"+"  " + (i + 1 )+ ")" + productinstall_parse[i];
                                    }
                                }
                            }

                            // ข้อความแจ้งเตือน
                            alert(alertPromt);
                            
                        }else{
                                $.ajax({
                                    type: "POST",
                                    url:  base_url + "/backend/uom/delete/" + uom_id,
                                    data: {id: uom_id},
                                    success: function(data,status,xhr){
                                            var data = JSON.parse(data);
                                            if(data.status == true){
                                                alert('Delete Succuss !!');
                                                window.location.reload(true);
                                            }

                                        }
                                    });

                                }
                        

                    }
            });

        }
        


   


</script>