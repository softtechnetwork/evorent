<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
                <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                </div>
       </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>ประเภทการรับประกันสินค้า (เพิ่ม)</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <?php echo form_open_multipart('',array('name'=>'create-productpart-product'))?>
                <?php
                    $warranty = $warranty[key($warranty)];
                    ?>
             

                     <!-- <span class="section">MasterProduct</span> -->
                     <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จำนวน<span class="required">*</span></label>
                        <div class="col-md-2 col-sm-6">
                            <input class="form-control"   name="value" placeholder="3 "  value="<?php echo $warranty->value;?>" required="" />
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <select name='type' class="form-control">
                                <option value="day"  <?php echo $warranty->type == "day" ? "seleted" : "";?>>วัน</option>
                                <option value="month" <?php echo $warranty->type == "month" ? "seleted" : "";?> >เดือน</option>
                            </select>
                        </div>
                    </div>
                
           
                    <div class="ln_solid">
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <button type='submit' class="btn btn-primary">Submit</button>
                                <button type='reset' class="btn btn-success">Reset</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script>

</script>