<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>


    <link href="<?php echo base_url('./assete/webadmin/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
 
    <link href="<?php echo base_url('./assete/webadmin/vendors/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/webadmin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css');?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url('./assete/webadmin/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('./assete/webadmin/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
	  <link href="<?php echo base_url('./assete/webadmin/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url('./assete/webadmin/vendors/google-code-prettify/bin/prettify.min.css');?>" rel="stylesheet">


    <!-- dropzone css js!-->
    <link href="<?php echo base_url('./assete/webadmin/vendors/dropzone-master/dist/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/webadmin/vendors/dropzone-master/dist/min/dropzone.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/webadmin/vendors/datatables/DataTables-1.10.23/css/jquery.dataTables.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/webadmin/vendors/datatables/DataTables-1.10.23/css/rowReorder.dataTables.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/webadmin/vendors/datatables/DataTables-1.10.23/css/responsive.dataTables.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('./assete/webadmin/vendors/bootstrap-select/css/bootstrap-select.min.css');?>"rel="stylesheet">

    <link href="<?php echo base_url('./assete/webadmin/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');?>" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url('./assete/webadmin/build/css/custom.min.css');?>" rel="stylesheet"> 
    <link href="<?php echo base_url('./assete/css/bootstrap-select.min.css');?>" rel="stylesheet">  
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">-->
    <link href="<?php echo base_url('./assete/css/datepicker.css');?>" rel="stylesheet" media="screen"> 
    <link href="<?php echo base_url('./assete/css/customs.css');?>" rel="stylesheet">  
    
    <!-- jQuery -->
    <script src="<?php echo base_url('./assete/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/webadmin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.js');?>"></script>
    <script src="<?php echo base_url('./assete/webadmin/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/webadmin/vendors/moment/min/moment.min.js');?>"></script>

    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker-thai.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/locales/bootstrap-datepicker.th.js');?>"></script>

    <script src="<?php echo base_url('./assete/js/validate.js');?>"></script>

</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/temp');?>" class="site_title"><i class="fa fa-paw"></i> <span>Web admin</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php //$userType = $this->session->userdata('userType');?>
           
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!--<h3>General</h3>-->

                <!-- menu for admin -->
                <!--<?php //if($userType == 'admin') {  ?>
                  <ul class="nav side-menu">
                    <li class="active">
                      <a><i class="fa fa-home"></i> หน้าหลัก <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: block;">
                        <li><a href="<?php echo base_url('admin/customer');?>">ข้อมูลลูกค้า</a></li>
                        <li><a href="<?php echo base_url('admin/inhabited');?>">ที่อยู่/สถานที่ติดตั้งสินค้า</a></li>
                        <li><a href="<?php echo base_url('admin/temp');?>">ข้อมูลสัญญาลูกค้า</a></li>
                        <li><a href="<?php echo base_url('admin/loan');?>">ข้อมูลการผ่อนสินค้า</a></li>
                        <li><a href="<?php echo base_url('admin/service');?>">ประวัติการซ่อมบำรุง</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-file-text" aria-hidden="true"></i> รายงาน <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('admin/report');?>">รายงาน การผ่อนชำระ</a></li>
                      </ul>
                    </li>
                  </ul>
                <?php  //} ?>
                -->

                <!-- menu for superadmin -->
                <?php //if($userType == 'superadmin') {  ?>
                  <ul class="nav side-menu">
                    <li class="active">
                      <a><i class="fa fa-home"></i> หน้าหลัก <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: block;">
                        <li><a href="<?php echo base_url('webadmin/banner');?>">Banner</a></li>
                        <li><a href="<?php echo base_url('webadmin/recommend');?>">สินค้าแนะนำ</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-file-text" aria-hidden="true"></i> เกี่ยวกับเรา <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('webadmin/report');?>">เกี่ยวกับเรา</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-list-alt" aria-hidden="true"></i> จัดการสินค้า <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('webadmin/product');?>">สินค้า</a></li>
                        <li><a href="<?php echo base_url('webadmin/productCategory');?>">หมวดหมู่สินค้า</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-file-text" aria-hidden="true"></i> การติดต่อ <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('webadmin/contact');?>">ข้อมูลการติดต่อ</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-cog" aria-hidden="true"></i> ตั้งค่า <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('admin/status');?>">สถานะ</a></li>
                        <li><a href="<?php echo base_url('admin/logs');?>">Update Logs</a></li>
                      </ul>
                    </li>
                  </ul>
                <?php //} ?>

              </div>
              
            </div>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt=""><?php echo $this->session->userdata('userName'); ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="<?php echo base_url('admin/user/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

        
