<style>
    /*@media (max-width: 575px){
        .label-align {
            text-align: left;
        }
    }*/
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Calculater</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row" style=" margin-bottom: 1rem;">
                    <div class="col-sm-5 col-md-5">
                        <label class="col-form-label">สินค้า</label>
                        <input class="form-control" type="text"  id="product" name="product" autocomplete="off"/>
                        <input type="hidden" id="product_set" name="product_set"/>
                        <input type="hidden" id="product_master_id" name="product_master_id"/>
                        <input type="hidden" id="product_install_id" name="product_install_id"/>
                    </div>
                    
                    <div class="col-sm-3 col-md-3">
                        <label class="col-form-label label-align">ราคา</label>
                        <select class="form-control" id="product-price" name="product-price" ></select> 
                    </div>
                    
                    <div class="col-sm-2 col-md-2">
                        <label class="col-form-label  label-align">จำนวน</label>
                        <input class="form-control" type="number"  id="product-count" name="product-count" min="1" value="1"/>
                    </div>
                </div>
                <div class="row" style=" margin-bottom: 1rem;">
                    <div class="col-sm-3 col-md-3">
                        <label class="col-form-label label-align">เงินดาวน์</label>
                        <input class="form-control" type="number" min="0" value="0"  id="down-payment" name="down-payment" />
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <label class="col-form-label label-align">จำนวนงวด</label>
                        <input class="form-control"  min="1"  type="number" value="12" id="rental-period" name="rental-period"/>
                    </div>
                </div>
                
                <div class="row" style=" margin-bottom: 0rem;">
                    <div class="col-md-2 col-sm-2" style=" padding-top: calc(.375rem + 1px); padding-bottom: calc(.375rem + 1px);">
                        <label class="col-form-label label-align">พักชำระหนี้ ?</label>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="suspend-payment">
                            <label class="form-check-label" for="suspend-payment">Checked</label>
                        </div>
                    </div>
                </div>
                <div class="row" id="suspend-element">
                    <div class="col-md-9 col-sm-9" >
                        <input type="hidden" id="suspend-count" />
                        <div class="row">
                            <div class="col-12" style="padding: 3px 10px; ">
                                <button id="suspend-plus"  type='button' class="btn btn-success"><i class="fa fa-plus"></i></button>
                                <button id="suspend-minus"  type='button' class="btn btn-warning"><i class="fa fa-minus"></i></button>
                            </div>
                            <div  class="col-12" id="suspend-plus-period" style="padding: 3px;"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-5 col-md-5">
                        <button id="down-payment-calc"  type='button' class="btn btn-info">คำนวน ตารางผ่อนชำระ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ตารางผ่อนชำระ</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="field item form-group">
                    <div class="table-responsive"> 
                        <table class="table table-striped jambo_table bulk_action" id="installment-table">
                            <thead class="headings">
                                <tr>
                                    <th class="column-title">งวด</th>
                                    <th class="column-title">ดอกเบี้ย</th>
                                    <th class="column-title">ราคาผ่อน</th>
                                    <th class="column-title">ภาษีมูลค่าเพิ่ม</th>
                                    <th class="column-title">ราคาผ่อนบวก ภาษีมูลค่าเพิ่ม</th>
                                </tr>
                            </thead>
                            <tbody> </tbody>
                        </table>
                    </div>
                    <input type="hidden" id="installment" name="installment"/>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("#suspend-element").hide();
    $("#suspend-payment").change(function() {
        if(this.checked) {  $("#suspend-element").show(); }else{ $("#suspend-element").hide(); }
    });

    $("#suspend-plus-period").html(null);
    $("#suspend-minus").prop('disabled', true);
    var SuspendCount = 1;
    $("#suspend-plus").click(function() {
        $("#suspend-minus").prop('disabled', false);
        if(SuspendCount <= 5){
            var element = "<input class='form-control' id='"+SuspendCount+"'  min='0'  type='number' value='0' name='suspend["+SuspendCount+"]' style='width: 3rem;float: left;margin: 3px 5px;'/>";
            $("#suspend-plus-period").append(element);
            $("#suspend-count").val(SuspendCount);
            if(SuspendCount == 5 ){$("#suspend-plus").prop('disabled', true);}
        }
        SuspendCount++;
    });

    $("#suspend-minus").click(function() {
        var suspend_count = $("#suspend-count").val();
        if(suspend_count >= 1 && suspend_count <= 5){
            $("#"+suspend_count).remove();
            $("#suspend-plus").prop('disabled', false);

            var c = suspend_count-1;
            $("#suspend-count").val(c);
            if(c == 0){$("#suspend-minus").prop('disabled', true);}
            SuspendCount = suspend_count;
        }
    });

	var base_url = "<?php echo base_url(); ?>";
    var arrayProduct = <?php echo json_encode($resProduct); ?>;
    jsonProduct(arrayProduct);
    function jsonProduct(arrayProduct){
        var products = [];
        //var ArrProducts = [];
        $.each(arrayProduct, function (i,items) {
            products.push({'value': items.piname,'data': items.id});
        });

        $('#product').autocomplete({
            lookup: products,
            onSelect: function (suggestion) {
                $.each(arrayProduct, function (i,items) {
                    if(items.id == suggestion.data){
                        var priced = [];
                        if(items.price_a > 0){priced.push({key: 'product_price_a', val: items.price_a });}
                        if(items.price_b > 0){priced.push({key: 'product_price_b', val: items.price_b });}
                        if(items.price_c > 0){priced.push({key: 'product_price_c', val: items.price_c });}
                        if(items.price_d > 0){priced.push({key: 'product_price_d', val: items.price_d });}
                        
                        $('#product-price').html(null); 
                        $.each(priced, function (i,items) {
                            if(items.val != null){ $('#product-price').append(`<option key="${items.key}" value="${items.val}">  ${items.val}  </option>`); }
                        });

                        $('#product_master_id').val(items.masterproduct_id);
                        $('#product_install_id').val(items.product_install_id);

                        //$('#product-brand').val(items.brand);
                        
                        //$('#product-version').val(items.piabbrv);
                        
                        //$('#product-serial').val(items.mp_serial_number);
                        
                    }
                });
                   
                $('#product_set').val(suggestion.data);
            }
        });
    };

    function writeInstallTable(data, tmpPrice){
        var td = null;
        var installmentJson  = [];
        $('#installment-table > tbody').html(null);       
        $.each(data, function (i, val) {
            var keys = Object.keys(val).find(key => val[key] === parseInt(tmpPrice));
            var interest, per_month_price, per_month_price_vat, per_month_price_include_vat, monthly_rent, monthly_plus_tax = null;
           
            switch(keys){
                case'product_price_a':
                    monthly_rent = parseFloat(val.per_month_price_a_include_vat).toFixed(2);
                    monthly_plus_tax = parseFloat(val.per_month_price_a_vat).toFixed(2);

                    interest = parseFloat(val.interest_a).toFixed(2); 
                    per_month_price = parseFloat(val.per_month_price_a).toFixed(2);
                    per_month_price_vat = parseFloat(val.per_month_price_a_vat).toFixed(2);
                    per_month_price_include_vat = parseFloat(val.per_month_price_a_include_vat).toFixed(2);
                    break;
                case'product_price_b':
                    monthly_rent = parseFloat(val.per_month_price_b_include_vat).toFixed(2);
                    monthly_plus_tax = parseFloat(val.per_month_price_b_vat).toFixed(2);

                    interest = parseFloat(val.interest_b).toFixed(2); 
                    per_month_price = parseFloat(val.per_month_price_b).toFixed(2);
                    per_month_price_vat = parseFloat(val.per_month_price_b_vat).toFixed(2);
                    per_month_price_include_vat = parseFloat(val.per_month_price_b_include_vat).toFixed(2);
                    break;
                case'product_price_c':
                    monthly_rent = parseFloat(val.per_month_price_c_include_vat).toFixed(2);
                    monthly_plus_tax = parseFloat(val.per_month_price_c_vat).toFixed(2);

                    interest = parseFloat(val.interest_c).toFixed(2); 
                    per_month_price = parseFloat(val.per_month_price_c).toFixed(2);
                    per_month_price_vat = parseFloat(val.per_month_price_c_vat).toFixed(2);
                    per_month_price_include_vat = parseFloat(val.per_month_price_c_include_vat).toFixed(2);
                    break;
                case'product_price_d':
                    monthly_rent = parseFloat(val.per_month_price_d_include_vat).toFixed(2);
                    monthly_plus_tax = parseFloat(val.per_month_price_d_vat).toFixed(2);

                    interest = parseFloat(val.interest_d).toFixed(2); 
                    per_month_price = parseFloat(val.per_month_price_d).toFixed(2);
                    per_month_price_vat = parseFloat(val.per_month_price_d_vat).toFixed(2);
                    per_month_price_include_vat = parseFloat(val.per_month_price_d_include_vat).toFixed(2);
                    break;
            }

           
            $('#monthly-rent').val(monthly_rent);

            $('#monthly-plus-tax').val(monthly_plus_tax);

            td += "<tr><td>"+i+"</td><td>"+interest+"</td><td>"+per_month_price+"</td><td>"+per_month_price_vat+"</td><td>"+per_month_price_include_vat+"</td></tr>";
            
            installmentJson.push({ 'period': i, 'interest': interest, 'per_month_price': per_month_price, 'per_month_price_vat': per_month_price_vat,'per_month_price_include_vat': per_month_price_include_vat});
            
        }); 
       
        $('#installment').val(JSON.stringify(installmentJson));
        $('#installment-table > tbody').append(td);
        
    };

    //################  Down payment #############//
    $("#down-payment-calc").on("click", function(e) {
        var suspend = [];
        var holdonpayment = [];
        if($("#suspend-payment").prop('checked')){
            $('input[name^="suspend"]').each(function() {
                if($(this).val() > 0){suspend.push(parseInt($(this).val())); }
            });
            holdonpayment[0] = suspend.filter(function(itm, i, a) { return i == a.indexOf(itm);});
        }

        var down_payment = $("#down-payment").val();
        var product_install_id = $("#product_install_id").val();
        var product_count = $("#product-count").val();
        var product_period = $("#rental-period").val();

        $.ajax({
            url: base_url+"sale/downPayment/down_payment", //ทำงานกับไฟล์นี้
            data:  {
                'downPayment':down_payment,
                'product_install_id':product_install_id,
                'product_count':product_count,
                'product_period':product_period,
                'holdonpayment':holdonpayment
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                var option = $('#product-price option:selected').attr('key');
                //################### Installment Table ###########################//
                writeInstallTable(data, data[1][option]);
                //################### Installment Table ###########################//
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        
    });


</script>



