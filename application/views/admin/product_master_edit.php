<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขสินค้า</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-product-master-edit" action="<?php echo base_url('/admin/productMaster/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" value="<?=$res[0]->cate_name;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <select class="form-control" name="product-master-brand"  id="product-master-brand" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกยี่ห้อสินค้า</option>
                                <?php foreach ($productBrand as $item) : ?>
                                    <option value="<?= $item->brand_id;?>" <?php if($res[0]->product_master_brand == $item->brand_id){echo 'selected="selected"';} ?>><?= $item->brand_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-master-name" id="product-name" value="<?=$res[0]->product_master_name;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อรุ่นสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-master-version" id="product-version" value="<?=$res[0]->product_master_version;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้าอ้างอิง</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-master-ref" id="product-ref"  value="<?=$res[0]->product_master_ref;?>"/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคาสินค้า</label>
                        <div class="col-md-3 col-sm-3  ">
                            <input type="number" min="0" class="form-control" name="product-master-price" id="product-master-price" value="<?=$res[0]->product_master_price;?>"/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="product-detail" name="product-master-detail" class="form-control" rows="4" cols="50"><?=$res[0]->product_master_detail;?></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <!-- รูปแบบการผ่อนชำระ -->
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class="col-md-6 col-sm-6  ">
                            <table id="product-table" class="table table-striped jambo_table bulk_action" >
                                <thead>
                                    <tr class="">
                                        <th class="column-title" style="width: 3rem">ลำดับ</th>
                                        <th class="column-title" style="width: 8rem">รหัส</th>
                                        <th class="column-title">จำนวนงวด </th>
                                        <th class="column-title">จำนวนที่ต้องผ่อนต่องวด </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($installentType as $nume => $items):?>
                                    <tr class="">
                                        <td class="column-title" ><?=$nume+1;?></td>
                                        <td class="column-title" ><?=$items->installment_type_id?></td>
                                        <td class="column-title"><?=$items->amount_installment?></th>
                                        <td class="column-title"><?=$items->pay_per_month?></td>
                                    </tr>
                                <?php endforeach?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                    <div id="installment-format"></div>
                    <div class="ln_solid"> </div>
                    -->
                    <!-- รูปแบบการผ่อนชำระ -->
                    
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?=$res[0]->product_master_id;?>">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/productMaster');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไขสินค้า</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var submitRequireEle = ["#product-master-name","#product-master-version"];
    ProductMasterEditSubmit(submitRequireEle, "#form-product-master-edit");
    ProductMasterEleChange(submitRequireEle); 
</script>


