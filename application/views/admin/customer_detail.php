<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 1px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
</style>


<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
      <div class="x_panel">
          <div class="x_title">
              <h2>รายละเอียดข้อมูลลูกค้า<small><?=$res_customer[0]->customer_code;?></small></h2>
              <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <address>
                    <br>ชื่อ : <?=$res_customer[0]->firstname;?> <?=$res_customer[0]->lastname;?>
                    <br>หมายเลขบัตรประชาชน : <?=$res_customer[0]->idcard;?>
                    <br>อาชีพ : <?=$res_customer[0]->career_text;?>
                    <br>รายได้ต่อเดือน : <?=$res_customer[0]->monthly_income;?>
                    <br>ชื่อบริษัทที่ทำงาน : <?=$res_customer[0]->office_name;?>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                  <address>
                      <br>ที่อยู่ปัจจุบัน : <?=$address[0]->address;?>
                      <br>ตำบล / แขวง : <?=$address[0]->district_name;?> 
                      <br>อำเภอ / เขต : <?=$address[0]->amphur_name;?> 
                      <br>จังหวัด : <?=$address[0]->province_name;?>
                      <br>รหัสไปรษณีย์ : <?=$address[0]->zip_code;?>
                  </address>
              </div>
              <div class="col-sm-4 invoice-col">
                <address>
                  <br>เพศ : <?php if($res_customer[0]->sex == 'M'){echo "ชาย";}else{echo "หญิง";}?> 
                  <br>Phone : <?=$res_customer[0]->tel;?> 
                  <br>Email : <?=$res_customer[0]->email;?>
                  <br>สถานะ : <span style=" padding: 0px 25px; color: <?=$res_customer[0]->color;?>; background-color: <?=$res_customer[0]->background_color;?>;"><?=$res_customer[0]->label;?></span>
                </address>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
<div class="row">
  <div class="col-md-7">
      <div class="x_panel">
        <div class="x_title">
            <h2>ตารางการทำสัญญา</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="table-responsive">
            <table id="customer_res" class="table table-striped jambo_table bulk_action" >
              <thead>
                <tr class="headings">
                    <th class="column-title">ลำดับ</th>
                    <th class="column-title">สัญญา </th>
                    <th class="column-title">สินค้า </th>
                    <th class="column-title">สถานะ </th>
                    <th class="column-title"></th>
                </tr>
              </thead>
              <tbody>
                <?php $nume = 1; foreach($customer_temp as $item){?>  
                  <tr class="headings">
                      <td class="column-title"><?=$nume?></td>
                      <td class="column-title"><?=$item->contract_code?> </td>
                      <td class="column-title"><?=$item->product_name?>  </td>
                      <td class="column-title"><span style=" padding: 0px 25px; color: <?=$item->color;?>; background-color: <?=$item->background_color;?>;"><?=$item->label;?></span> </td>
                      <td><button attr-data="<?=$item->contract_code?>" type="button" class="btn btn-round btn-warning contracted" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;" data-toggle="tooltip" title="" data-original-title="สัญญา <?=$item->contract_code?>">         <i class="fa fa-pie-chart"></i>     </button></td>
                    </tr>
                <?php $nume ++;} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>

  
  <div class="col-md-5 col-sm-5  ">
    <div class="x_panel">
      <div class="x_title">
        <h2> สัญญา <small id="contract_pie"></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="echart_pies" style="height:350px;"></div>
      </div>
    </div>
  </div>
  
</div>


<script>
  CustomerPieChart('echart_pies', null);
  $('.contracted').click(function () {
    var base_url = "<?php echo base_url(); ?>";
    var url = base_url+"admin/customer/getToPieChart";
    var contract_code = $(this).attr('attr-data');

    var chart_res = GetCustomerContractToPieChart(url, contract_code);
      CustomerPieChart('echart_pies', chart_res);
      $('#contract_pie').html(contract_code);
  });
</script>