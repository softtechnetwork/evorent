<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Image Master <small>Category</small></h2>
        <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
        <ul class="nav navbar-right panel_toolbox">
          <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
          
          <li>
              <button id="add-img-cate-btn" type="button" class="btn" style="color: #466889;" data-toggle="modal" data-target="#AddModalCenter"><i class="fa fa-plus"></i> เพิ่ม Image Master Category</button>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <!-- <p class="text-muted font-13 m-b-30">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code></p> -->
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                
                  <thead>
                    <tr class="headings">
                      <th class="column-title" style="width: 1rem;">ลำดับ</th>
                      <!-- <th class="column-title" style="width: 1rem;">รหัส</th> -->
                      <th class="column-title" style="width: 2rem;">คีย์</th>
                      <th class="column-title">ชื่อ</th>
                      <th class="column-title">รายละเอียด</th>
                      <th class="column-title no-link last" style="text-align: center;"></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>



  <!-- Add MOdal -->
  <div class="modal fade" id="AddModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #2A3F54; color: #E7E7E7; font-weight: 100;">
          <h6 class="modal-title" id="exampleModalLongTitle">เพิ่ม Image Master Category</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="name">คีย์</label>
              <input type="text" class="form-control" id="key" name="key">
            </div>
            <div class="form-group">
              <label for="name">ชื่อ</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="desc">รายละเอียด</label>
              <textarea class="form-control" id="desc"  name="desc" rows="3"></textarea>
            </div>
            <div class="form-group">
              <p style="color: #ff8100; margin-bottom: unset;">หมายเหตุ : Image Master Category ไม่สามารถลบได้เนื่องจาก นำไปสร้าง key เพื่อใช้งานเกี่ยวกับรูปภาพ.</p>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"style="width: 15%;">ปิด</button>
          <button type="button" class="btn btn-primary add-img-category-save" style="width: 15%;">บันทึก</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit MOdal -->
  <div class="modal fade" id="EditModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #2A3F54; color: #E7E7E7; font-weight: 100;">
          <h6 class="modal-title" id="exampleModalLongTitle">แก้ไข Image Master Category</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="name">คีย์</label>
              <input type="hidden" id="id" name="id">
              <input type="text" class="form-control" id="key" name="key" readonly>
            </div>
            <div class="form-group">
              <label for="name">ชื่อ</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="desc">รายละเอียด</label>
              <textarea class="form-control" id="desc"  name="desc" rows="3"></textarea>
            </div>
            <div class="form-group">
              <p style="color: #ff8100; margin-bottom: unset;">หมายเหตุ : Image Master Category ไม่สามารถลบได้เนื่องจาก นำไปสร้าง key เพื่อใช้งานเกี่ยวกับรูปภาพ.</p>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"style="width: 15%;">ปิด</button>
          <button type="button" class="btn btn-primary edit-img-category-save" style="width: 15%;">บันทึก</button>
        </div>
      </div>
    </div>
  </div>


  <script>
    var base_url = $('input[name="base_url"]').val();
    Drawtable(base_url, '#table');

    $("#add-img-cate-btn" ).click(function() {
      var key = $('#AddModalCenter .modal-body #key').val('');
      var name = $('#AddModalCenter .modal-body #name').val('');
      var desc = $('#AddModalCenter .modal-body #desc').val('');
       $('#AddModalCenter').modal('show');
    });

    $(".add-img-category-save" ).click(function() {
      var key = $('#AddModalCenter .modal-body #key').val();
      var name = $('#AddModalCenter .modal-body #name').val();
      var desc = $('#AddModalCenter .modal-body #desc').val();
      insert(base_url, key, name, desc);
    });
    $(".edit-img-category-save" ).click(function() {
      var id = $('#EditModalCenter .modal-body #id').val();
      var name = $('#EditModalCenter .modal-body #name').val();
      var desc = $('#EditModalCenter .modal-body #desc').val();
      edit(base_url, id, name, desc);
    });

    //####  Function  ###//
    function insert(base_url, key, name, desc){
        $.ajax({
            url: base_url+"admin/image/mastercate_insert", //ทำงานกับไฟล์นี้
            data:  {
              'key':key,
              'name':name,
              'desc':desc
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
              if(data.status){
                //location.reload();
                Drawtable(base_url, '#table');
                $('#AddModalCenter').modal('hide');
              }else{
                alert('ไม่สามารถเพิ่มข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
              }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
    }
    function edit(base_url, id, name, desc){
        $.ajax({
            url: base_url+"admin/image/mastercate_update", //ทำงานกับไฟล์นี้
            data:  {
              'id':id,
              'name':name,
              'desc':desc
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
              if(data.status){
                Drawtable(base_url, '#table');
                $('#EditModalCenter').modal('hide');
              }else{
                alert('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
              }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
    }
    function Drawtable(base_url, replacePosition){
        var res = getData(base_url);
        if(res){ 
            var elements = '';
            $(replacePosition).DataTable().destroy();
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                var no = i+1;
                var agrmt = ["'"+base_url+"', '"+val.id+"', '"+val.key+"', '"+val.name+"', '"+val.desc+"'"];
                elements += '<tr>';
                elements += '   <td class="">'+no+'</td>';
                //elements += '   <td class="">'+val.id+'</td>';
                elements += '   <td class="">'+val.key+'</td>';
                elements += '   <td class="">'+val.name+'</td>';
                elements += '   <td class="">'+val.desc+'</td>';
                elements += '   <td class="" style="text-align: center;">';
                elements += '     <button id="edit-img-cate-btn" type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;" onclick="editEvent('+agrmt+')"><!--<i class="fa fa-wrench"></i>-->แก้ไข</button>';

                //elements += '       <a href="'+base_url+'admin/productCategory/delete/'+val['id']+' ">';
                //elements += '           <button type="button" class="btn btn-round btn-danger" onclick="delProductSet('+agrmt+')" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                //elements += '           <i class="fa fa-times"></i> ลบ</button>';
                //elements += '       </a>';
                elements += '   </td>';
                elements += '</tr>';
            });
            $(body).append(elements);
            
            // Datatable
            $(replacePosition).dataTable({
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    //{ "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function getData(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/image/mastercate_get", //ทำงานกับไฟล์นี้
            // data:  {
            //     'Search':search
            //     },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
              //console.log(data);
              res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
    function editEvent(base_url, id, key, name, desc){
      $('#EditModalCenter .modal-body #id').val(id);
      $('#EditModalCenter .modal-body #key').val(key);
      $('#EditModalCenter .modal-body #name').val(name);
      $('#EditModalCenter .modal-body #desc').val(desc);
      $('#EditModalCenter').modal('show');
    }
  </script>



<!---- End Content ------>
  