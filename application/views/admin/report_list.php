<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
    .gettopdf{

    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>รายงานการผ่อนชำระ</h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="reportCus">รหัสลูกค้า</label>
                    <select id="reportCus" name="reportCus" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสลูกค้า</option>
                        <?php  foreach($getCustomer as $item) :?>
                            <option value="<?php echo $item->customer_code;?>"> <?php echo $item->customer_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <!--<div class ="col-md-3">
                    <label for="reportName">ชื่อลูกค้า</label>
                    <input id="reportName" name="reportName"  class="form-control" value="">
                </div>-->
                <div class ="col-md-3">
                    <label for="reportTemp">รหัสสัญญา</label>
                    <select id="reportTemp" name="reportTemp" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสสัญญา</option>
                        <?php  foreach($getTemp as $item) :?>
                            <option value="<?php echo $item->temp_code;?>"> <?php echo $item->temp_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <div class ="col-md-2">
                    <label for="reportStatus">สถานะ</label>
                    <select id="reportStatus" name="reportStatus" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก สถานะ</option>
                        <?php  foreach($getStatus as $item) :?>
                            <option value="<?php echo $item->status_code;?>"> <?php echo $item->label;?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-5">
                    <label for="reservation">วันที่ครบกำหนดชำระ</label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stduedate" id="stduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="stduedate" id="stduedate" class="col-md-5 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="enduedate" id="enduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="enduedate" id="enduedate" class="col-md-6 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                    </div>
                </div>
                <div class ="col-md-5">
                    <label for="reservation">วันที่ชำระ</label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stinstallmentdate" id="stinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="stinstallmentdate" id="stinstallmentdate" class="col-md-4 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="endinstallmentdate" id="endinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="endinstallmentdate" id="endinstallmentdate" class="col-md-6 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                    </div>
                </div>
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="report-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="report-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>ตารางการผ่อนชำระ</h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <input name="installmentRes" id="installmentRes" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <form method="post" action="<?php echo base_url();?>admin/report/exportToPDF/" target="_blank">
                        <input type="hidden" id="expPdf" name="expPdf">
                        <button type='submit' class="btn btn-dark" style="border-radius: inherit;">
                            <i class="fa fa-file-pdf-o"></i> PDF
                        </button>
                    </form>
                </li>
                <li>
                    <form method="post" action="<?php echo base_url();?>admin/report/exportToExcel/" target="_blank">
                        <input type="hidden" id="expExcell" name="expExcell">
                        <button type='submit' class="btn btn-dark" style="border-radius: inherit;">
                            <i class="fa fa-file-excel-o"></i> EXCEL
                        </button>
                    </form>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table id="report_res" class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">รหัสลูกค้า </th>
                        <th class="column-title">ลูกค้า </th>
                        <th class="column-title">เลขที่สัญญา</th>
                        <th class="column-title">ราคาผ่อน</th>
                        <th class="column-title">วันที่ครบกำหนดชำระ</th>
                        <th class="column-title">จำนวนที่ชำระ </th>
                        <th class="column-title">วันที่ชำระ </th>
                        <th class="column-title" style='text-align: center;'>สถานะ </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
                
            <input id="reportAll"  name="reportAll" type="hidden" /> 
            <input id="page" value="1" type="hidden" /> 
            <div id="paginations"></div>
        </div>
    </div>
</div>
<script>

$(".datepicker").datepicker().on('show', function(e){
    $('.prev').text('<');
        $('.next').text(">");
});
var base_url = $('input[name="base_url"]').val(); 
var ItemPerPage = 30;  // item of page
var elementtable = "#report_res tbody"; // place of table

var url_getRes = base_url+"admin/report/getInstallment";
var url_getAll = base_url+"admin/report/getInstallmentAll";
var url_getTemp = base_url+"admin/report/getTempByCustomer";
var url_getToExport = base_url+"admin/report/getToExport";
var searchArray = {};

    /*$("#reportCus").change(function () {
        $.ajax({
            url: url_getTemp, //ทำงานกับไฟล์นี้
            data: {
                "customer" :$(this).val()
            }, //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
               
                var td = null;
                $('#reportTemp').html(null);
                td = "<option value=''>เลือก รหัสสัญญา</option>";
                $.each(data, function (i, val) {
                    td += "<option value='"+val.temp_code+"'>"+val.temp_code+"</option>";
                });
                $('#reportTemp').append(td);
                console.log($('#reportTemp').html());

            },
            error: function(xhr, status, exception) { 
                //console.log(exception);
            }
        });
    });*/
    $('#stduedate').change(function () {
        $('#enduedate').attr("min",$('#stduedate').val());
    });
    $('#enduedate').change(function () {
        $('#stduedate').attr("max",$('#enduedate').val());
    });

    $('#stinstallmentdate').change(function () {
        $('#endinstallmentdate').attr("min",$('#stinstallmentdate').val());
    });
    $('#endinstallmentdate').change(function () {
        $('#stinstallmentdate').attr("max",$('#endinstallmentdate').val());
    });

    getToTeble(url_getRes, null,ItemPerPage,1,ItemPerPage, elementtable);
    getToExport(url_getToExport, null, '#expPdf', '#expExcell');
    getreportAll(url_getAll,null,ItemPerPage,1,ItemPerPage);
    Reportpagination('#paginations',$('#reportAll').val(),ItemPerPage,'#page', searchArray);


    $("#report-reset").click(function () {
        $('#reportCus ').val(null);
        $('[data-id=reportCus] .filter-option-inner-inner').html('เลือก รหัสลูกค้า');

        $('#reportTemp ').val(null);
        $('[data-id=reportTemp] .filter-option-inner-inner').html('เลือก รหัสสัญญา');

        $('#reportStatus ').val(null);
        $('[data-id=reportStatus] .filter-option-inner-inner').html('เลือก สถานะ');
        
        $('#reportName ').val(null);
        $('#stduedate ').val(null);
        $('#enduedate ').val(null);

        $('#stduedate').removeAttr("min");
        $('#stduedate').removeAttr("max");

        $('#enduedate').removeAttr("min");
        $('#enduedate').removeAttr("max");

        $('#stinstallmentdate ').val(null);
        $('#endinstallmentdate ').val(null);

        $('#endinstallmentdate').removeAttr("min");
        $('#endinstallmentdate').removeAttr("max");

        $('#stinstallmentdate').removeAttr("min");
        $('#stinstallmentdate').removeAttr("max");
        
        getToTeble(url_getRes, null, ItemPerPage,1,ItemPerPage, elementtable);
        getToExport(url_getToExport, null,  '#expPdf', '#expExcell');
        getreportAll(url_getAll,null,ItemPerPage,1,ItemPerPage);
        Reportpagination('#paginations',$('#reportAll').val(),ItemPerPage,'#page', searchArray);
    });

    $("#report-search").click(function () {
        var  payment_duedate = null;
        var  installment_date = null;

        if( $('#stduedate').val() != '' && $('#enduedate').val() !='' ){
            payment_duedate = formatPDate($('#stduedate').val(),'-')+','+ formatPDate($('#enduedate').val(),'-');
        }
        if($('#stinstallmentdate').val() != '' && $('#endinstallmentdate').val()!= ''){
            installment_date = formatPDate($('#stinstallmentdate').val(),'-')+','+ formatPDate($('#endinstallmentdate').val(),'-');
        }
        
        searchArray["customer_code"] = $("#reportCus").val();
        //searchArray["name"] = $("#reportName").val();
        searchArray["temp_code"] = $("#reportTemp").val();
        searchArray["status_code"] = $("#reportStatus").val();
        searchArray["payment_duedate"] = payment_duedate;
        searchArray["installment_date"] = installment_date;
        
        getToTeble(url_getRes, searchArray, ItemPerPage,1,ItemPerPage, elementtable);
        getToExport(url_getToExport, searchArray,  '#expPdf', '#expExcell');
        getreportAll(url_getAll, searchArray, ItemPerPage,1,ItemPerPage);
        Reportpagination('#paginations',$('#reportAll').val(),ItemPerPage,'#page', searchArray);

    });
    /*$("#button-excel").click(function () {
        var  payment_duedate = null;
        var  installment_date = null;
        if( $('#stduedate').val() != '' && $('#enduedate').val() !='' ){
            payment_duedate = formatDate($('#stduedate').val(),'/')+'-'+ formatDate($('#enduedate').val(),'/');
        }
        if($('#stinstallmentdate').val() != '' && $('#endinstallmentdate').val()!= ''){
            installment_date = formatDate($('#stinstallmentdate').val(),'/')+'-'+ formatDate($('#endinstallmentdate').val(),'/');
        }
        
        searchArray["customer_code"] = $("#reportCus").val();
        searchArray["name"] = $("#reportName").val();
        searchArray["temp_code"] = $("#reportTemp").val();
        searchArray["status"] = $("#reportStatus").val();
        searchArray["payment_duedate"] = payment_duedate;
        searchArray["installment_date"] = installment_date;
        
        getToExport(url_getToExport, searchArray,'excel');
        //getreportAll(url_getAll, searchArray, ItemPerPage,1,ItemPerPage);
        //Reportpagination('#paginations',$('#reportAll').val(),ItemPerPage,'#page', searchArray);

    });*/

    function formatDate(date, string) {
        if(date != '' && date != null){
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join(string);
        }else{
            return '';
        }
    }

    function formatPDate(date, string) {
        if(date != '' && date != null){
            var temp = date.split('/'); 
            var res = temp[2]+string+temp[1]+string+temp[0];
            return res;
        }else{
            return '';
        }
    }
    
</script>
<!---- End Content ------>
