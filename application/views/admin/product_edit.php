<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขสินค้า</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-product-edit" action="<?php echo site_url('/admin/product/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" value="<?=$res[0]->cate_name;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" value="<?=$res[0]->brand_name;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-name" id="product-name" value="<?=$res[0]->product_name;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อรุ่นสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-version" id="product-version" value="<?=$res[0]->product_version;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้าอ้างอิง</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-ref" id="product-ref"  value="<?=$res[0]->product_ref;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดยี่สินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="product-detail" name="product-detail" class="form-control" rows="4" cols="50"><?=$res[0]->product_detail;?></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <!-- รูปแบบการผ่อนชำระ -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class="col-md-6 col-sm-6  ">
                            <table id="product-table" class="table table-striped jambo_table bulk_action" >
                                <thead>
                                    <tr class="">
                                        <th class="column-title" style="width: 3rem">ลำดับ</th>
                                        <th class="column-title" style="width: 8rem">รหัส</th>
                                        <th class="column-title">จำนวนงวด </th>
                                        <th class="column-title">จำนวนที่ต้องผ่อนต่องวด </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($installentType as $nume => $items):?>
                                    <tr class="">
                                        <td class="column-title" ><?=$nume+1;?></td>
                                        <td class="column-title" ><?=$items->installment_type_id?></td>
                                        <td class="column-title"><?=$items->amount_installment?></th>
                                        <td class="column-title"><?=$items->pay_per_month?></td>
                                    </tr>
                                <?php endforeach?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                    <div id="installment-format"></div>
                    <div class="ln_solid"> </div>
                    <!-- รูปแบบการผ่อนชำระ -->
                    
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?=$res[0]->product_id;?>">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/product');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไขสินค้า</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var submitRequireEle = ["#product-name","#product-version"];
    ProductEditSubmit(submitRequireEle, "#form-product-edit");
    ProductEleChange(submitRequireEle); 
</script>


