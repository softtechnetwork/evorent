<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADMIN EVORENT</title>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('./uploaded/evorentweb/favicon.ico');?>">
    <link href="<?php echo base_url('./assete/css/bootstrap-select.min.css');?>" rel="stylesheet">  
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">-->
    <link href="<?php echo base_url('./assete/css/datepicker.css');?>" rel="stylesheet" media="screen"> 
  
    <!-- admin theme(gentelella) https://colorlib.com/polygon/gentelella/  -->
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
 
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css');?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url('./assete/admin/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('./assete/admin/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
	  <link href="<?php echo base_url('./assete/admin/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url('./assete/admin/vendors/google-code-prettify/bin/prettify.min.css');?>" rel="stylesheet">

    <!-- Datatables -->
    <link href="<?php echo base_url('./assete/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css');?>" rel="stylesheet">

    <!-- dropzone css js!-->
    <link href="<?php echo base_url('./assete/admin/vendors/dropzone-master/dist/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/dropzone-master/dist/min/dropzone.min.css');?>" rel="stylesheet">
    
    
    <!--  ckeditor4 -->
    <script src="<?=base_url('./assete/ckeditor4/ckeditor.js');?>"></script>
    <script src="<?=base_url('./assete/ckeditor4/samples/js/sample.js');?>"></script>
    <!-- <link rel="stylesheet" href="<?=base_url('./assete/ckeditor4/samples/css/samples.css');?>"> -->
    <link rel="stylesheet" href="<?=base_url('./assete/ckeditor4/samples/toolbarconfigurator/lib/codemirror/neo.css');?>">
    
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap-select/css/bootstrap-select.min.css');?>"rel="stylesheet">
    <link href="<?php echo base_url('./assete/admin/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');?>" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url('./assete/admin/build/css/custom.css');?>" rel="stylesheet"> 
    <link href="<?php echo base_url('./assete/css/customs.css');?>" rel="stylesheet">  

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="<?php echo base_url('./assete/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/moment/min/moment.min.js');?>"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url('./assete/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js');?>"></script>

    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/bootstrap-datepicker-thai.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/locales/bootstrap-datepicker.th.js');?>"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url('./assete/admin/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>

    <!-- ECharts -->
    <script src="<?php echo base_url('./assete/admin/vendors/echarts/dist/echarts.min.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/echart_custom.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/validate.js');?>"></script>
    <script src="<?php echo base_url('./assete/js/admin_custom.js');?>"></script>

    <!-- bootstrap-multiselec -->
    <link rel="stylesheet" href="<?php echo base_url('./assete/admin/jquery-multiselect/css/bootstrap-multiselect.css');?>" type="text/css">
    <script type="text/javascript" src="<?php echo base_url('./assete/admin/jquery-multiselect/js/bootstrap-multiselect.js');?>"></script>

    <!-- Image-Uploader -->
    <!-- <link rel="stylesheet" href="<?//php echo base_url('./assete/image-uploader/dist/image-uploader.min.css');?>" rel="stylesheet">
    <script src="<?//php echo base_url('./assete/image-uploader/dist/image-uploader.min.js');?>" type="text/javascript"></script> -->

    
    <!-- ajax-file-uploader -->
    <link rel="stylesheet" href="<?php echo base_url('./assete/ajax-file-uploader/css/jquery.uploader.css');?>">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('./assete/ajax-file-uploader/dist/jquery.uploader.min.js');?>"></script>

    
    <script type="text/javascript" src="https://johnny.github.io/jquery-sortable/js/jquery-sortable.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/smoothness/jquery-ui.css" />

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.22/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.22/dist/sweetalert2.min.css" rel="stylesheet">

</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/dashboard');?>" class="site_title"><i class="fa fa-paw"></i> <span>EVORENT</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>ADMIN</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $userType = $this->session->userdata('userType');?>
           
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!--<h3>General</h3>-->

                <!-- menu for admin -->
                <?php if($userType == 'admin') {  ?>
                  <ul class="nav side-menu">
                    <li class="active">
                      <a><i class="fa fa-home"></i> หน้าหลัก <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: block;">
                        <li><a href="<?php echo base_url('admin/customer');?>">ข้อมูลลูกค้า</a></li>
                        <li><a href="<?php echo base_url('admin/inhabited');?>">ที่อยู่/สถานที่ติดตั้งสินค้า</a></li>
                        <li><a href="<?php echo base_url('admin/temp');?>">ข้อมูลสัญญาลูกค้า</a></li>
                        <li><a href="<?php echo base_url('admin/loan');?>">ข้อมูลการผ่อนสินค้า</a></li>
                        <li><a href="<?php echo base_url('admin/loan/overdue');?>">ข้อมูลการผ่อนชำระ</a></li>
                        <li><a href="<?php echo base_url('admin/service');?>">ประวัติการซ่อมบำรุง</a></li>
                        <!--<li><a href="<?php echo base_url('admin/mechanic');?>">ข้อมูลช่าง</a></li>-->
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-file-text" aria-hidden="true"></i> รายงาน <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('admin/report');?>">รายงาน การผ่อนชำระ</a></li>
                      </ul>
                    </li>
                  </ul>
                <?php  } ?>

                <!-- menu for superadmin -->
                <?php if($userType == 'superadmin') {  ?>
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'dashboard'){echo'active';}?> ">
                      <a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-bar-chart"></i> แดชบอร์ด</a>
                    </li>
                  </ul>
                  

                  <!-- <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'home'){echo'active';}?> ">
                      <a><i class="fa fa-home"></i> หน้าหลัก <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'home'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'temp'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/temp');?>">ข้อมูลสัญญาลูกค้า</a></li>
                        <li><a href="<?php echo base_url('admin/loan');?>">ข้อมูลการผ่อนสินค้า</a></li>
                        <li <?php if($submenu == 'loan'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/loan/overdue');?>">ข้อมูลการผ่อนชำระ</a></li>
                        <li <?php if($submenu == 'sms'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/sms');?>">ประวัติการส่งSMS</a></li>
                        <li <?php if($submenu == 'service'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/service');?>">ประวัติการซ่อมบำรุง</a></li>
                        <li <?php if($submenu == 'serialNumber'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/serialNumber');?>">หมายเลขเครื่อง</a></li>
                        <li <?php if($submenu == 'mechanic'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/mechanic');?>">ข้อมูลช่าง</a></li>
                        <li <?php if($submenu == 'agent'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/agent');?>">ผู้รับมอบอำนาจ</a></li>
                      </ul>
                    </li>
                  </ul> -->

                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'customer'){echo'active';}?> ">
                      <a><i class="fa fa-user"></i> จัดการลูกค้า <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'customer'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'customer'){echo'class="current-page"';}?> ><a href="<?php echo base_url('admin/customer');?>">ข้อมูลลูกค้า</a></li>
                        <li <?php if($submenu == 'corperation'){echo'class="current-page"';}?> ><a href="<?php echo base_url('admin/corperation');?>">ข้อมูลลูกค้า(นิติบุคคล)</a></li>
                        <li <?php if($submenu == 'inhabited'){echo'class="current-page"';}?> ><a href="<?php echo base_url('admin/inhabited');?>">ที่อยู่/สถานที่ติดตั้งสินค้า</a></li>
                        
                      </ul>
                    </li>
                  </ul>

                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'contract'){echo'active';}?> ">
                      <a><i class="fa fa-file"></i> จัดการสัญญา <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'contract'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'contractCus'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/contract');?>">สัญญาลูกค้า</a></li>
                        <!--<li <?php if($submenu == 'contractIns'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/contractInstallment');?>">การผ่อนชำระ</a></li>-->
                        <!--<li <?php if($submenu == 'contractSerial'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/contractSerial');?>">หมายเลขเครื่อง</a></li>-->
                        <li <?php if($submenu == 'contractService'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/contractService');?>">ประวัติการซ่อมบำรุง</a></li>
                        <li <?php if($submenu == 'contractRemark'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/ContractRemark');?>">หมายเหตุ สัญญา</a></li>
                        
                      </ul>
                    </li>
                  </ul>
                  
                  <!-- <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'sms'){echo'active';}?> ">
                      <a href="<?php echo base_url('admin/sms');?>"><i class="fa fa-envelope" aria-hidden="true"></i>ประวัติการส่งSMS</a>
                    </li>
                  </ul> -->



                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'sms'){echo'active';}?> ">
                      <a><i class="fa fa-envelope" aria-hidden="true"></i> SMS <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'sms'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'sms'){echo'class="current-page"';}?>><a href="<?=base_url('admin/sms');?>">ประวัติการส่ง SMS จากระบบ</a></li>
                        <li <?php if($submenu == 'send'){echo'class="current-page"';}?>><a href="<?=base_url('admin/sms/send_sms');?>">ส่ง SMS manual</a></li>
                        <li <?php if($submenu == 'template'){echo'class="current-page"';}?>><a href="<?=base_url('admin/sms/sms_template');?>">SMS Tamplate</a></li>
                      </ul>
                    </li>
                    <!-- <li><a href="<?php echo base_url('backend/promotion');?>">โปรโมชั่น</a></li> -->   
                  </ul>



                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'agent'){echo'active';}?> ">
                      <a href="<?php echo base_url('admin/agent');?>"><i class="fa fa-male" aria-hidden="true"></i>ผู้รับมอบอำนาจ</a>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'mechanic'){echo'active';}?> ">
                      <a href="<?php echo base_url('admin/mechanic');?>"><i class="fa fa-cogs" aria-hidden="true"></i>ข้อมูลช่าง</a>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'report'){echo'active';}?> ">
                      <a><i class="fa fa-file-text" aria-hidden="true"></i> รายงาน <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'report'){echo'block';}else{echo'none';}?>;">
                        <!--<li><a href="<?php echo base_url('admin/report');?>">รายงาน การผ่อนชำระ</a></li>   -->
                        <li <?php if($submenu == 'overdue'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/report/overdue');?>">รายงาน การผ่อนชำระ</a></li>
                        <li <?php if($submenu == 'contract'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/report/contract');?>">รายงาน สัญญาลูกค้า</a></li>
                      </ul>
                    </li>
                  </ul>
                  <!--<ul class="nav side-menu">
                    <li>
                      <a><i class="fa fa-edit"></i> Master <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('admin/status');?>">สถานะ</a></li>
                        <li><a href="<?php echo base_url('admin/logs');?>">Update Logs</a></li>
                        <li><a href="<?php echo base_url('admin/statusCategory');?>">Status Category</a></li>
                      </ul>
                    </li>
                  </ul>-->
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'product'){echo'active';}?> ">
                      <a><i class="fa fa-list-alt" aria-hidden="true"></i> จัดการสินค้า <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'product'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'productmaster'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/productMaster');?>">มาสเตอร์สินค้า</a></li>
                        <li <?php if($submenu == 'productset'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/productSet');?>">สินค้า(ชุด)</a></li>
                        <!--
                        <li <?php if($submenu == 'product'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/product');?>">สินค้า</a></li>
                        <li <?php if($submenu == 'productSub'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/productSub');?>">สินค้าย่อย</a></li>
                        <li <?php if($submenu == 'installmentType'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/installmentType');?>">ประเภทการผ่อนชำระ</a></li>
                        -->
                        <li <?php if($submenu == 'productCategory'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/productCategory');?>">หมวดหมู่สินค้า</a></li>
                        <li <?php if($submenu == 'brand'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/brand');?>">ยี่ห้อสินค้า</a></li>
                        
                        <!-- 
                        <li><a href="<?php echo base_url('backend/productpart');?>">อะไหล่สินค้า</a></li>
                        <li><a href="<?php echo base_url('backend/product');?>">สินค้า</a></li>
                        <li><a href="<?php echo base_url('backend/productinstall');?>">สินค้าพร้อมขาย</a></li>
                        <li><a href="<?php echo base_url('backend/productserialnumber');?>">หมายเลขเครื่อง</a></li>
                        <li><a href="<?php echo base_url('backend/productinstallments');?>">จัดการผ่อนชำระสินค้า</a></li>
                        -->
                      </ul>
                    </li>
                    <!-- <li><a href="<?php echo base_url('backend/promotion');?>">โปรโมชั่น</a></li> -->   
                  </ul>
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'image'){echo'active';}?> ">
                      <a><i class="fa fa-image" aria-hidden="true"></i> จัดการรูปภาพ <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'image'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'imageproduct'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/Image/imageproduct');?>">รูปภาพ สินค้า</a></li>
                        <li <?php if($submenu == 'imagecate'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/Image/category');?>">หมวดหมู่ รูปภาพ</a></li>
                        <li <?php if($submenu == 'imagemastercate'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/Image/mastercate');?>">Master หมวดหมู่</a></li>
                      </ul>
                    </li>
                    <!-- <li><a href="<?php echo base_url('backend/promotion');?>">โปรโมชั่น</a></li> -->   
                  </ul>
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'web'){echo'active';}?> ">
                      <a><i class="fa fa-laptop" aria-hidden="true"></i> หน้าเว็บ EVORENT <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'web'){echo'block';}else{echo'none';}?>;">
                        <li <?php if($submenu == 'webContact'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/webContact');?>">การติดต่อจากลูกค้า</a></li>
                        <li <?php if($submenu == 'webProduct'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/webProduct');?>">จัดการสินค้า</a></li>
                        <li <?php if($submenu == 'webBanner'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/WebBanner');?>">จัดการแบนเนอร์ หน้าหลัก</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'contractDoc'){echo'active';}?> ">
                      <a href="<?php echo base_url('admin/contractDoc');?>"><i class="fa fa-file-text" aria-hidden="true"></i>จัดการ เอกสารสัญญา</a>
                    </li>
                  </ul>

                  <ul class="nav side-menu">
                    <li class=" <?php if($mainmenu == 'setting'){echo'active';}?> ">
                      <a><i class="fa fa-cog" aria-hidden="true"></i> ตั้งค่า <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: <?php if($mainmenu == 'setting'){echo'block';}else{echo'none';}?>;">
                        <!--<li><a href="<?php echo base_url('backend/uom');?>">UOM</a></li>-->
                        <!--<li><a href="<?php echo base_url('backend/productcategory');?>">ประเภทสินค้า</a></li>-->
                        <!--<li><a href="<?php echo base_url('backend/warranty');?>">ประเภทการรับประกันสินค้า</a></li>-->
                        <li <?php if($submenu == 'status'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/status');?>">สถานะ</a></li>
                        <li <?php if($submenu == 'statusCategory'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/statusCategory');?>">หมวดหมู่ สถานะ</a></li>
                        <li <?php if($submenu == 'logs'){echo'class="current-page"';}?>><a href="<?php echo base_url('admin/logs');?>">Update Logs</a></li>
                      </ul>
                    </li>
                  </ul>
                <?php } ?>

              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!--
              <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('admin/user/logout');?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt=""><?php echo $this->session->userdata('userName'); ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <!--<a class="dropdown-item"  href="javascript:;"> Profile</a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">50%</span>
                          <span>Settings</span>
                        </a>
                      <a class="dropdown-item"  href="javascript:;">Help</a>-->
                      <a class="dropdown-item"  href="<?php echo base_url('admin/user/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
  
                  <!--
                    <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="<?php echo base_url('./assete/admin/production/images/img.jpg');?>" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  -->
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

        
