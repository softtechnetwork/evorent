<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างข้อมูลผู้รับมอบอำนาจ<small></small></h2>
                <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>-->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="agent-create-form" class="" action="<?php echo site_url('/admin/agent/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขบัตรประชาชน</label>
                        <div class="col-md-6 col-sm-6">
                          <!--<input name="idcard" id="idcard" type="text" class="form-control" data-inputmask="'mask' : '9-9999-99999-99-9'" required="required">-->
                          <input name="idcard" id="idcard" type="tel"  maxlength="13"  class="form-control" required="required">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="idcard-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="name" name="name" value="" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นามสกุล</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" id="sname" name="sname" required="required" />
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ปัจจุบัน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-current">จังหวัด</label>
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district">ตำบล / แขวง</label>
                            <select class="form-control" name="district"  id="district" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" readonly>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์มือถือ</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel" name="tel" type="tel" maxlength="10" class="form-control" >
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันเกิด</label>
                        <div class="col-md-3 col-sm-3">                        
                            <input class="form-control datepicker" type="text" id="bdate" name="bdate"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="ln_solid"> </div>





                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสถานที่ประกอบการ</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="text" class="form-control" name="name-office" id="name-office" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่ตั้งสถานที่ประกอบการ</label>
                        
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-office' name='address-office'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-office">จังหวัด</label>
                            <select name='province-office' id='province-office' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-office">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs-office"  id="amphurs-office" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-office">ตำบล / แขวง</label>
                            <select class="form-control" name="district-office"  id="district-office" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-office">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-office" id="zipcode-office" readonly>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel-office" name="tel-office" type="tel" maxlength="10" class="form-control" >
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                   
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อีเมล์</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control email" id="email" name="email" type="email"  />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ออกเอกสาร</label>
                        <div class="col-md-3 col-sm-3">                        
                            <input class="form-control datepicker" type="text" id="create-date" name="create-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/agent');?>" type='button' class="btn btn-success">กลับ</a>
                            <button type='submit' class="btn btn-primary">สร้างข้อมูลผู้รับมอบอำนาจ</button>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
       
$(".datepicker").datepicker().on('show', function(e){
    $('.prev').text('<');
        $('.next').text(">");
});
var base_url = $('input[name="base_url"]').val();

var input_id = ["#idcard","#name","#sname","#address","#province","#amphurs","#district","#tel", "#bdate"];
AgentCreatSubmit(input_id, '#agent-create-form'); // validate and submit

var AddressId = ["#province","#amphurs","#district","#zipcode","#address"]; // ห้าม เปลี่ยนตำแหน่ง
var OfficeAddId = ["#province-office","#amphurs-office","#district-office","#zipcode-office","#address-office"]; // ห้าม เปลี่ยนตำแหน่ง
var normal_id = ["#name","#sname","#tel","#email","#address","#bdate"];

rePlaceAddEle(AddressId, base_url);//replace ที่อยู่ปัจจุบัน
rePlaceAddEle(OfficeAddId, base_url);//replace ที่อยู่ บริษัท
normalValid(normal_id, base_url);// normal Validate

var IdcardCheckUrl = base_url+"admin/Agent/getCharngByIdcard";
IdcardChecked("#idcard", IdcardCheckUrl);

</script>