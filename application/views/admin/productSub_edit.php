<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
.btn-light {
    color: #212529bd;
    background-color: #f8f9fa00;
    border-color: #d5d9dc;
    border-radius: inherit !important;
}
.mh .dropdown-menu { max-height: 200px;}
.dropdown-item.active, .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #1abb9c;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขสินค้าย่อย</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSub-edit" action="<?php echo site_url('/admin/productSub/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product" id="product" value="<?=$res[0]->product_name;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-name" id="productSub-name"  value="<?=$res[0]->product_sub_name;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รุ่น</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-version" id="productSub-version" value="<?=$res[0]->product_sub_version;?>" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสอ้างอิงสินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-ref" id="productSub-ref" value="<?=$res[0]->product_sub_ref;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดยี่สินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="productSub-detail" name="productSub-detail" class="form-control" rows="4" cols="50"><?=$res[0]->product_sub_detail;?></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <input type="hidden"  name="id" value="<?=$res[0]->product_sub_id;?>"/>
                            <a href="<?php echo base_url('/admin/productSub');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไขสินค้าย่อย</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var submitRequireEle = ["#productSub-name","#productSub-version"];
    ProductSubEditSubmit(submitRequireEle, "#form-productSub-edit");
    ProductSubEleChange(submitRequireEle); 
</script>


