<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขหมวดหมู่สินค้า</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="" action="<?php echo site_url('/admin/productCategory/update');?>" method="post"  enctype="multipart/form-data" >
                    <input name="category-id" id="category-id" type="hidden" value="<?=$res[0]->id;?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-6 col-sm-6  ">
                            <label class="">หมวดหมู่สินค้า</label>
                            <input name="category-name" id="category-name" type="text" class="form-control" value="<?=$res[0]->cate_name;?>">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดหมวดหมู่สินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="category-detail" name="category-detail" class="form-control" rows="4" cols="50"><?=$res[0]->cate_detail;?></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('/admin/productCategory');?>" class="btn btn-primary">กลับ</a>
                                <button type='submit' class="btn btn-success">แก้ไขหมวดหมู่สินค้า</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>



