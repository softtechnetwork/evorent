<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<!--
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขหมายเลขเครื่อง</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-serial-edit" action="<?php echo site_url('/admin/contractSerial/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สัญญา</label>
                        <div class="col-md-6 col-sm-6 ">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสัญญา</label>
                                    <input class="form-control" type="text" name="contract-id"  id="contract-id" value="<?=$resTemp[0]->contract_code?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสินค้า</label>
                                    <input class="form-control" type="text" id="product_id" name="product_id" value="<?=$resTemp[0]->product_id?>" readonly/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <input class="form-control" type="text" id="product_name" name="product_name" value="<?=$resTemp[0]->product_name;?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-9 col-sm-9">
                            <?php 
                            $subCount = (int)count($resSubproduct);
                            $routCount = 0;
                            $routNo = 0;
                            $resSerialCount = count($resSerial);
                            for ($i = 0; $i < $resSerialCount; $i++ ) {
                                $stringsRow = '';
                                $strings = '<div class="col-md-2 col-sm-2">';
                                if ($resSerial[$i]->no != $GetSerialNoMax[0]->no) {
                                    $strings .= '<label class="col-form-label label-align">'.$resSerial[$i]->product_master_name.'</label>';
                                    $strings .= '<input disabled class="form-control" type="text" id="serial_'.$resSerial[$i]->product_sub_id.'" name="serial_'.$resSerial[$i]->product_sub_id.'" value="'.$resSerial[$i]->serial_number.'" />';
                                }else{
                                    $strings .= '<label class="col-form-label label-align">'.$resSerial[$i]->product_master_name.'</label>';
                                    $strings .= '<input class="form-control" type="text" id="serial_'.$resSerial[$i]->product_sub_id.'" name="serial_'.$resSerial[$i]->product_sub_id.'" value="'.$resSerial[$i]->serial_number.'" />';
                                }
                                $strings .= '<input class="form-control" type="hidden" id="no_'.$resSerial[$i]->product_sub_id.'" name="no_'.$resSerial[$i]->product_sub_id.'" value="'.$resSerial[$i]->no.'" />';
                                $strings .= '</div>';

                                if (((int)$resSerial[$i]->no - $routNo == 1)) {
                                    if($i != 0){$stringsRow .= '</div>';}
                                    if($i != 0){$stringsRow .= '<div class="ln_solid"> </div>';}
                                    $stringsRow .= '<div class="row">'.$strings;
                                }else{
                                    $stringsRow .= $strings;
                                    if($i == ($resSerialCount-1) ){
                                        $stringsRow .= '</div>';
                                        $stringsRow .= '<div class="ln_solid"> </div>';
                                    }
                                }
                                
                                $routNo = $resSerial[$i]->no;
                                echo $stringsRow;
                                
                            }?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/contractSerial');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success"> แก้ไขหมายเลขเครื่อง</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
-->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขหมายเลขเครื่อง</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="field item form-group">
                    <div class="col-md-3 col-sm-3">
                        <label class="col-form-label label-align"><strong>รหัสสัญญา : </strong><?=$resTemp[0]->contract_code?></label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label class="col-form-label label-align"><strong>รหัสสินค้า : </strong><?=$resTemp[0]->product_id?></label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label class="col-form-label label-align"><strong>สินค้า : </strong><?=$resTemp[0]->product_name?></label>
                    </div>
                </div>
                <hr>
                <div class="field item form-group"><div class="col-md-12"><h6>ตาราง ส่วนเสริมสินค้า</h6></div></div>
               
                <div class="field item form-group">
                    <div class="table-responsive">
                        <table id="contract-table" class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">ลำดับ</th>
                                    <th class="column-title">สินค้า </th>
                                    <th class="column-title">Serial Number</th>
                                    <th class="column-title">ประเถทสินค้า</th>
                                    <th class="column-title">หมายเหตุ</th>
                                    <th class="column-title">แก้ไข</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($resSerial as $i => $item){ ?> 
                                    <tr class="headings">
                                        <td style="width:5%;"><?=$i+1;?></td>
                                        <td style="width:15%;"><?=$item->product_master_name;?> </td>
                                        <td style="width:15%;"><?=$item->serial_number;?></td>
                                        <td style="width:10%;"><?=$item->label;?></td>
                                        <td style="width:40%;"><?=$item->remark;?></td>
                                        <td style="width:5%;">
                                            <button attr-data='<?= json_encode($item);?>' type="button" class="btn btn-round btn-warning serial-edit-btn" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">
                                                <i class="fa fa-wrench"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php }?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Adon Contract -->
<div class="clearfix"></div>
<div id="SerialEdit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="SerialEdit-form" action="<?=base_url('/serialEdit');?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">แก้ไขหมายเลขเครื่อง รหัสสัญญา : <?=$resTemp[0]->contract_code;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="contract-addon-place"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label label-align"><strong>สินค้า : </strong></label>
                            <label id='serial-products' name='serial-products' ></label>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label label-align"><strong>ประเถทสินค้า : </strong></label>
                            <label id='serial-products-type' name='serial-products-type'></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align"><strong>Serial Number : </strong></label>
                            <input id='serial-number' name='serial-number' class='form-control' >
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align"><strong>หมายเหตุ : </strong></label>
                            <textarea id='serial-remark' name='serial-remark' class='form-control'></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <input id='contract_code' name='contract_code'type="hidden" value="<?=$resTemp[0]->contract_code;?>">
                        <input id='serial_product_id' name='serial_product_id' type="hidden">
                        <input id='serial_id' name='serial_id' type="hidden">
                        <input id='product_sub_id' name='product_sub_id' type="hidden">
                        <input id='product_no' name='product_no' type="hidden">
                        <button type="submit" class="btn btn-primary" style="width:100%;">แก้ไข หมายเลขเครื่อง</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var elementID = ["#contract-id", "#product_id"];
    serialEditSubmit(elementID, "#form-serial-edit");
</script>
<script src="<?=base_url('./assete/js/admin/contractSerial_custom.js');?>"></script>



