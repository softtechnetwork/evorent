<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ค้นหาข้อมูลการผ่อนสินค้า</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="temp_code">เลขที่สัญญา</label>
                    <input id="temp_code" name="temp_code" type="text" class="form-control" placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="name">ชื่อลูกค้า</label>
                    <input id="name" name="name" type="text" class="form-control"  placeholder="..." />
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="button-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="x_panel">
    <div class="x_title">
        <h2>ข้อมูลการผ่อนสินค้า <small></small></h2>
        <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
            <table id="loan_res" class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th class="column-title">ลำดับ</th>
                    <th class="column-title">เลขที่สัญญา </th>
                    <th class="column-title">ลูกค้า </th>
                    <th class="column-title">สินค้า </th>
                    <th class="column-title" style="text-align: center;">จำนวนงวดที่ผ่อน </th>
                    <th class="column-title" style="text-align: center;">Action</th>
                </tr>
                </thead>

                <tbody></tbody>
            </table>
        </div>
        <div id="pagination" page="1" ></div>

    </div>
    </div>
</div>
<!---- End Content ------>
<script>

    var base_url = $('input[name="base_url"]').val();
    
    var getRes = base_url+"admin/loan/getRes";
    var getAll = base_url+"admin/loan/getResAll";

    var searchEle = ["#temp_code", "#name", "#status"];
    var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
    var resObj = {"getRes" :getRes, "elementTable":"#loan_res tbody"};
    var allObj = {"getAll":getAll, "functionResName":"getLoanRes"};

    getLoanRes(resObj, searchEle, pageObj);
    Pagination(allObj, searchEle, pageObj, resObj);


    $('#button-search').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        getLoanRes(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });
    
    $('#button-reset').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        $.each(searchEle, function (i, val) {
            $(val).val('');
        });

        getLoanRes(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });

   
</script>