<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>หมายเลขเครื่อง<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li style="margin-right: 3px;">
                    <input id="search_text" type="text"  class="form-control" placeholder="รหัสสัญญา..."/>
                </li>
                <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                    <i class="fa fa-search"></i> ค้นหา</button>
                </li>
                <li>
                    <button id="button-reset" type='button' class="btn btn-warning" style="border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li>
                <!--
                <li>
                    <a class="btn btn-success" href="<?php echo base_url('admin/contractSerial/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มหมายเลขเครื่อง
                    </a>
                </li>
                -->
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="serial-table" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 3rem">ลำดับ</th>
                            <th class="column-title">รหัสสัญญา </th>
                            <th class="column-title">ลูกค้า </th>
                            <th class="column-title">สินค้า </th>
                            <th class="column-title">หมวดหมู่สินค้า </th>
                            <th class="column-title">เบอร์โทร </th>
                            <th class="column-title no-link last" style="text-align: center;width:15rem;"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = $('input[name="base_url"]').val();
    var searchEle = ['#search_text'];
    DrawContractSerialList(base_url, searchEle, '#serial-table tbody');
    function DrawContractSerialList(base_url, searchEle, replacePosition){
        var searchJson = ContractSerialSearchJson(searchEle);
        var res = getResContractSerial(base_url, searchJson);
        if(res){ 
            var elements = '';
            $(replacePosition).html(null);
            $.each(res, function (i, val) {
                var no = i+1;
                var agrmt = ["'"+base_url+"', '"+val.product_id+"'"];
                elements += '<tr>';
                elements += '   <td class="">'+no+'</td>';
                elements += '   <td class="">'+val.contract_code+'</td>';
                elements += '   <td class="">'+val.firstname+' '+val.lastname+'</td>';
                elements += '   <td class="">'+val.product_name+'</td>';
                elements += '   <td class="">'+val.cate_name+'</td>';
                elements += '   <td class="">'+val.tel+'</td>';
                elements += '   <td class="" style="text-align: center;">';
                elements += '       <a href="'+base_url+'admin/contractSerial/edit/'+val.contract_code+' ">';
                elements += '           <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                elements += '           <i class="fa fa-wrench"></i> แก้ไข</button>';
                elements += '       </a>';
                elements += '       <a href="'+base_url+'admin/contractSerial/addition/'+val.contract_code+' ">';
                elements += '           <button type="button" class="btn btn-round btn-success" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                elements += '           <i class="fa fa-plus"></i> เพิ่ม</button>';
                elements += '       </a>';
                ////elements += '       <a href="'+base_url+'admin/productCategory/delete/'+val['id']+' ">';
                //elements += '           <button type="button" class="btn btn-round btn-danger" onclick="delProduct('+agrmt+')" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                //elements += '           <i class="fa fa-times"></i> ลบ</button>';
                ////elements += '       </a>';
                elements += '   </td>';
                elements += '</tr>';
            });
            $(replacePosition).append(elements);
        }
    }
    function ContractSerialSearchJson(searchEle){
        search = {};
        $.each(searchEle, function (i, val) {
            var resEle = $(val).val();
            var objKey = $(val).attr('id');
            search[objKey] = resEle;
        });
        return  JSON.stringify(search);
    }
    function getResContractSerial(base_url, search){
        var res = null;
        $.ajax({
            url: base_url+"admin/contractSerial/getResSerial", //ทำงานกับไฟล์นี้
            data:  {
                'Search':search
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }

    SearchContractSerialList(base_url, '#button-search', searchEle, '#serial-table tbody');
    function SearchContractSerialList(base_url, btn_event, searchEle, replacePosition){
        $(btn_event).click(function () {
            DrawContractSerialList(base_url, searchEle, replacePosition);
        });
    }
    
    ResetContractSerialList(base_url, '#button-reset', searchEle, '#serial-table tbody');
    function ResetContractSerialList(base_url, btn_event, searchEle, replacePosition){
        $(btn_event).click(function () {
            $.each(searchEle, function (i, val) {
                $(val).val(null);
            });
            DrawContractSerialList(base_url, searchEle, replacePosition);
        });
    };
</script>



<!---- End Content ------>
  