<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <!-- <div class="x_panel">
        <div class="x_title">
            <h2>ค้นหาข้อมูลลูกค้า(นิติบุคคล)</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="customer_code">รหัสลูกค้า</label>
                    <input id="customer_code" name="customer_code" type="text" class="form-control" placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="name">ชื่อลูกค้า</label>
                    <input id="name" name="name" type="text" class="form-control"  placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="name">สถานะ</label>
                    <select name='status' id='status' class="form-control">
                        <option value="" selected='false' disabled >เลือสถานะ</option>
                        <?php foreach ($resStatus as $item) : ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="button-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
                <div class ="col-md-2  navbar-right "  style=" padding-top: 1.65rem;">
                    <a class="btn btn-success" href="<?php echo base_url('admin/corperation/create');?>"  style="color: #ffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มข้อมูลลูกค้า(นิติบุคคล)
                    </a>
                </div>
            </div>
        </div>
    </div> -->
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลลูกค้า(นิติบุคคล)<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="btn" href="<?=base_url('admin/corperation/create');?>"  style="color: #466889;"><i class="fa fa-plus"></i> เพิ่มข้อมูลลูกค้า(นิติบุคคล)</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <!-- <table id="customer_res" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 3rem">ลำดับ</th>
                            <th class="column-title" style="width: 8rem">รหัสลูกค้า </th>
                            <th class="column-title">ชื่อลูกค้า </th>
                            <th class="column-title">ที่อยู่ </th>
                            <th class="column-title" style="width: 8rem">เลขบัตรประชาชน </th>
                            <th class="column-title" style="width: 8rem">เบอร์โทรศัพท์ </th>
                            <th class="column-title" style="text-align: center;width: 9rem">สถานะ </th>
                            <th class="column-title no-link last" style="text-align: center;width: 11rem;"><span class="nobr">กิจกรรม</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table> -->
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 1rem">ลำดับ</th>
                            <th class="column-title" style="width: 2rem">รหัสลูกค้า </th>
                            <th class="column-title">ชื่อลูกค้า </th>
                            <th class="column-title">ที่อยู่ </th>
                            <th class="column-title" style="width: 7rem">เลขบัตรประชาชน </th>
                            <th class="column-title" style="width: 6rem">เบอร์โทรศัพท์ </th>
                            <th class="column-title" style="text-align: center;width: 7rem">สถานะ </th>
                            <th class="column-title no-link last" style="text-align: center;width: 9rem;"><span class="nobr">กิจกรรม</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    var base_url = $('input[name="base_url"]').val();

    // var getRes = base_url+"admin/corperation/getRes";
    // var getAll = base_url+"admin/corperation/getResAll";

    // var searchEle = ["#customer_code", "#name", "#status"];
    // var pageObj = {"pagination" :"#pagination", "ItemPerPage":1, "itemStt":1, "itemEnd":1};
    // var resObj = {"getRes" :getRes, "elementTable":"#customer_res tbody"};
    // var allObj = {"getAll":getAll, "functionResName":"getCorperationRes"};

    // getCorperationRes(resObj, searchEle, pageObj);
    // Pagination(allObj, searchEle, pageObj, resObj);

    // $('#button-search').click(function(){
    //     var pageObj = {"pagination" :"#pagination", "ItemPerPage":1, "itemStt":1, "itemEnd":1};
    //     getCorperationRes(resObj, searchEle, pageObj);
    //     Pagination(allObj, searchEle, pageObj, resObj);
    // });
    
    // $('#button-reset').click(function(){
    //     var pageObj = {"pagination" :"#pagination", "ItemPerPage":1, "itemStt":1, "itemEnd":1};
    //     $.each(searchEle, function (i, val) {
    //         $(val).val('');
    //     });

    //     getCorperationRes(resObj, searchEle, pageObj);
    //     Pagination(allObj, searchEle, pageObj, resObj);
    // });


    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                tr += '<tr class=" ">';
                tr += '<td class=" ">'+val.RowNum+'</td>';
                tr += '<td class=" ">'+val.customer_code+'</td>';
                tr += '<td class=" ">'+val.firstname+'</td>';
                tr += '<td class=" ">'+val.address+'</td>';
                tr += '<td class=" ">'+val.idcard+'</td>';
                tr += '<td class=" ">'+val.tel+'</td>';
                tr += '<td class=" last"  style="text-align: center;">';
                tr +=     '<button type="button" class="btn btn-round" style="background-color: '+val.background_color+'; color: '+val.color+'; font-size: 13px; padding: 0 15px; margin-bottom: inherit;margin-right: inherit;"> '+val['label']+'</button>';
                tr += '</td>';
                tr += '<td class=" last"  style="text-align: center;">';
                tr +=   '<a href="'+base_url+'admin/corperation/edit/'+val.customer_code+' " data-toggle="tooltip" title="แก้ไข">';
                tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                tr +=   '</a>';
                tr +=   '<a href="'+base_url+'admin/corperation/detail/'+val.customer_code+' " data-toggle="tooltip" title="รายละเอียด">';
                tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i></button>';
                tr +=   '</a>';
                tr +=   '<a href="'+base_url+'admin/corperation/premise/'+val.customer_code+' " data-toggle="tooltip" title="หลักฐาน">';
                tr +=       '<button type="button" class="btn btn-round btn-success" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-image-o"></i></button>';
                tr +=   '</a>';
                tr += '</td>';
                tr += '</tr>';
            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [50, 100], [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/Corperation/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }
</script>


<!---- End Content ------>
  