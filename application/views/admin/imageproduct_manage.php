<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }

    .img-preview-action{position: absolute; text-align: center; top: 0; left: 0; height: 100%; width: 100%; z-index: 2; opacity: 0; background-color: rgba(0, 0, 0, 0.36);}
    .img-preview-action:hover { opacity: 100; }
    .img-preview-action > ul > li {
        cursor: pointer;
        padding: 5px;
        float: left;
    }
    .img-preview-action > ul {
        margin: 0;
        padding-left: 0;
        text-align: center;
        padding-top: 40px;
        list-style: none;
        display: inline-block;
    }
</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <h2>รูปภาพ <small>สินค้า รหัส : <span id='product-header'></span></small> </h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <input name="productSet_id" value="<?=$productSet_id;?>" type="hidden" >
            <!-- <ul class="nav navbar-right panel_toolbox"> -->
            <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
            <!-- </ul> -->
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12"></div>
            </div>
        </div>
    </div>
  </div>

  <div class="image-form-location"></div>


  
  <script>
    var base_url = $('input[name="base_url"]').val();
    var productSet_id = $('input[name="productSet_id"]').val();
    draw_product_data(base_url, productSet_id);
    draw_image_form(base_url, productSet_id);
    AddImg(base_url, '.img-add');
    DelImg(base_url, '.img-del');

    //#### Functions ####//
    function draw_product_data(base_url, productSet_id){
        var res = getProduct_once(base_url, productSet_id);
        if(res){
            $('#product-header').html(res[0]['product_id']);
        }
    }
    function draw_image_form(base_url, productSet_id){
        var category = getProduct_category(base_url);
        if(category){
            var str = '';
            $.each( category, function( key, categories ) {
                str += string_image_form(base_url, categories, productSet_id);
            });
            $('.image-form-location').append(str);

            //######   image upload ######/
            //let preloaded = [];
            // let preloaded = [
            //     {id: 1, src: 'https://picsum.photos/500/500?random=1'},
            //     {id: 2, src: 'https://picsum.photos/500/500?random=2'},
            //     {id: 3, src: 'https://picsum.photos/500/500?random=3'},
            //     {id: 4, src: 'https://picsum.photos/500/500?random=4'},
            //     {id: 5, src: 'https://picsum.photos/500/500?random=5'},
            //     {id: 6, src: 'https://picsum.photos/500/500?random=6'},
            // ];
            // $('.input-images-2').imageUploader({
            //     preloaded: preloaded,
            //     imagesInputName: 'photos',
            //     preloadedInputName: 'old',
            //     maxSize: 2 * 1024 * 1024,
            //     maxFiles: 10
            // });

            
            var str = '';
            $.each( category, function( key, categories ) {
                var prefix = categories.img_master_cate_key+'-'+categories.id;
                var id = "#"+prefix;
                var submit_btn = "#"+prefix+"_btn";
                
                let ajaxConfig = {
                    ajaxRequester: function (config, uploadFile, pCall, sCall, eCall) {
                        let progress = 0
                        let interval = setInterval(() => {
                            progress += 10;
                            pCall(progress)
                            if (progress >= 100) {
                                clearInterval(interval)
                                const windowURL = window.URL || window.webkitURL;
                                sCall({
                                    data: windowURL.createObjectURL(uploadFile.file)
                                })
                                // eCall("上传异常")
                            }
                        }, 300)
                    }
                }
                $(id+'_input').uploader({
                    multiple: true,
                    defaultValue: [
                        {
                            name: "jQuery",
                            url: "https://www.jqueryscript.net/dummy/5.jpg"
                        }, {
                            name: "script",
                            url: "https://www.jqueryscript.net/dummy/6.jpg"
                        }
                    ],
                    ajaxConfig: ajaxConfig
                });
                // $(id+'_input').uploader({
                //     multiple: true,
                //     defaultValue: [
                //         {
                //             name: "jQuery",
                //             url: "https://www.jqueryscript.net/dummy/5.jpg"
                //         }, {
                //             name: "script",
                //             url: "https://www.jqueryscript.net/dummy/6.jpg"
                //         }
                //     ],
                //     ajaxConfig: {
                //         //url: "",
                //         url: base_url+"admin/image/imageproduct_upload", //ทำงานกับไฟล์นี้
                //         method: "post",
                //         paramsBuilder: function (uploaderFile) {
                //             let form = new FormData();
                //             form.append("file", uploaderFile.file)
                //             return form
                //         },
                //         ajaxRequester: function (config, uploaderFile, progressCallback, successCallback, errorCallback) {
                //             $.ajax({
                //                 url: config.url,
                //                 contentType: false,
                //                 processData: false,
                //                 method: config.method,
                //                 data: config.paramsBuilder(uploaderFile),
                //                 // data:  {
                //                 //     'img': config.paramsBuilder(uploaderFile)
                //                 // },  //ส่งตัวแปร
                //                 success: function (response) {
                //                     successCallback(response)
                //                 },
                //                 error: function (response) {
                //                     console.error("Error", response)
                //                     errorCallback("Error")
                //                 },
                //                 xhr: function () {
                //                     let xhr = new XMLHttpRequest();
                //                     xhr.upload.addEventListener('progress', function (e) {
                //                         let progressRate = (e.loaded / e.total) * 100;
                //                         progressCallback(progressRate)
                //                     })
                //                     return xhr;
                //                 }
                //             })
                //         },
                //         responseConverter: function (uploaderFile, response) {
                //             return {
                //                 url: response.data,
                //                 name: null,
                //             }
                //         },
                //     },
                // });

                // $(submit_btn).click(function() {
                //     var ref_id = $(id+' input[name=ref_id]').val();
                //     var master_cate_key = $(id+' input[name=master_cate_key]').val();
                //     var namcate_ide = $(id+' input[name=cate_id]').val();
                    
                //     var file = $(id+' input[name='+prefix+'_input]').val();
                    
                //     var master_cate = $(id+' #master_cate').find(":selected").val();

                //     console.log(prefix);
                //     console.log( $(id).serialize() );
                //     $.ajax({
                //         url: base_url+"admin/image/imageproduct_upload", //ทำงานกับไฟล์นี้
                //         //data:  { 'method':'product', 'form' : $(id).serialize() },  //ส่งตัวแปร
                //         data:  { 
                //             'ref_id':ref_id, 
                //             'master_cate_key':master_cate_key, 
                //             'namcate_ide':namcate_ide, 
                //             'file':file, 
                //             'master_cate':master_cate
                //         },  //ส่งตัวแปร
                //         type: "POST",
                //         dataType: 'json',
                //         async:false,
                //         success: function(data, status) {
                //             console.log(data);
                //             //res = data;
                //         },
                //         error: function(xhr, status, exception) { 
                //             console.log(exception);
                //         }
                //     });
                //     //$('#AddModalCenter').modal('show');
                // });

              
            });
        }


        
            
    }
    function getProduct_once(base_url, productSet_id){
        var res = null;
        $.ajax({
            url: base_url+"admin/image/imageproduct_once", //ทำงานกับไฟล์นี้
            data:  { 'productSet_id':productSet_id },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
    function getProduct_category(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/image/imageproduct_category_get_key", //ทำงานกับไฟล์นี้
            data:  { 'method':'product' },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
    function string_image_form(base_url, categories, productSet_id){

        // get img //
        var getImg = null;
        $.ajax({
            url: base_url+'admin/image/getImg', //ทำงานกับไฟล์นี้
            data: {
                "ref_id" : productSet_id,
                "master_cate" : categories.img_master_cate_key,
                "img_cate_id" : categories.id
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                getImg = data;
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });


        var str = '<div class="col-md-6 col-sm-12 ">';
        str += '<div class="x_panel">';

        str += '<div class="x_title">';
        str += '   <h2>'+categories.name+' <small> '+categories.desc+'</small> </h2>';
        str += '   <div class="clearfix"></div>';
        str += '</div>';

        str += '<div class="x_content">';
        str += '   <div class="row">';
        str += '       <div class="col-sm-12">';



        var prefix = categories.img_master_cate_key+'-'+categories.id;
        var lastItem = getImg.length+1;
        
        str += '           <form method="POST" id="'+prefix+'-'+lastItem+'-form" action="'+base_url+'admin/image/do_upload" enctype="multipart/form-data">';
        str += '               <input type="hidden" name="ref_id" id="ref_id" value="'+productSet_id+'">';
        str += '               <input type="hidden" name="master_cate_key" id="master_cate_key" value="'+categories.img_master_cate_key+'">';
        str += '               <input type="hidden" name="cate_id" id="cate_id" value="'+categories.id+'">';
        
        $.each(getImg, function (i, val) {
            var json_val = JSON.stringify(val);
            str += '<div style="vertical-align: middle;width: 120px; height: 120px; border: 1px solid rgba(103, 103, 103, 0.39); padding: 5px; margin: 5px; display: inline-block;">';
            str += '<div style="position: relative; height: 100%; width: 100%;">';
            str += '<div class="img-preview-action">';
            str += '<ul>';
            str += "<li class='file-delete img-del' data-attr='"+json_val+"' >";
            str += '<i class="fa fa-trash-o" style="color: white;"></i></li></ul></div>';
            str += '<div style="position: absolute; background-color: rgba(0, 0, 0, 0.2); z-index: 3; width: 100%; height: 100%; display: none;">';
            str += '<div style="position: absolute;top: 0;z-index: -1;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.6);"></div>';
            str += '<div style="display: flex; justify-content: center; height: 100%; align-items: center;">';
            str += '<i class="fa fa-spinner fa-spin"></i></div></div>';
            str += '<img alt="preview" class="files_img" src="'+base_url+val.path+'?random='+Math.random()+'" style="object-fit: cover; height: 100%; width: 100%;">';
            str += '</div>';
            str += '</div>';
        });

        str += '                <div class="" style="vertical-align: middle; width: 120px; height: 120px; border: 1px dashed rgba(103, 103, 103, 0.39); padding: 5px; margin: 5px; display: inline-block;">';
        str += '                    <div class="" style="    position: relative; height: 100%; width: 100%; display: flex; align-items: center; justify-content: center; text-align: center;">';
        str += '                            <img id="'+prefix+'-'+lastItem+'" browsid="input-'+prefix+'-'+lastItem+'" class="thumnails-premise img-add" src="'+base_url+'/uploaded/DocumentTh.png" alt="image" style="border: unset; width: auto; height: 100%;border-radius: unset;display: inline-block;" />';
        str += '                    </div>';
        str += '                </div>';
        str += '                <input id="input-'+prefix+'-'+lastItem+'" name="input-'+prefix+'-'+lastItem+'" type="file" required="required"  style="display:none" >';
        str += '                <input name="imgno" type="hidden" value="'+lastItem+'" >';
        str += '           </form>';
        

        str += '       </div>';
        str += '   </div>';
        str += '</div>';

        str += '</div>';
        str += '</div>';
        
        return str;
    }


    //###### Img #####//
    function getImg(base_url, ref_id, master_cate, img_cate_id){
        var res = null;
        $.ajax({
            url: base_url+'admin/image/getImg', //ทำงานกับไฟล์นี้
            data: {
                "ref_id" : ref_id,
                "master_cate" : master_cate,
                "img_cate_id" : img_cate_id
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });
        return res;
    }
    function browsImg(base_url, files, place){
        $("#"+files).trigger('click');
        $("#"+files).change(function(){
            
           // console.log(files);
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+place).attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);

                
                var form_id = '#'+place+'-form';
                //console.log(form_id+' -> #product-1-1-from');
                $(form_id).submit();
                
            }
        });
    }
    function AddImg(base_url, premiseAdd){
        $(premiseAdd).click(function () {
            var attr = $(this)['context']['attributes'];
            var files = attr['browsid'].value;
            var form_id = '#'+this.id+'-form';
            var img_id = '#'+this.id;
            
            //browsImg(base_url, files, this.id);
            
            $("#"+files).trigger('click');
            $("#"+files).change(function(){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(img_id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                    $('form'+form_id).submit();
                }
            });
        });
    }
    function DelImg(base_url, Delclass,){
        $(Delclass).click(function () {
            var data = $(this).attr("data-attr");
            var confirmed = window.confirm("Do you want to delete ?");
            if(confirmed){
                $.ajax({
                    url: base_url+'admin/image/del_img', //ทำงานกับไฟล์นี้
                    data: JSON.parse(data),  //ส่งตัวแปร
                    type: "POST",
                    dataType: 'json',
                    async:false,
                    success: function(data, status) {
                        //console.log(data);
                        if(data['status']){
                            window.location=data['datas'];
                            //redirect($_SERVER['REQUEST_URI'], 'refresh'); 
                        }else{
                            window.alert("Delete fail Please try again.");
                        }
                    },
                    error: function(xhr, status, exception) { console.log(xhr); }
                });
            }
        });
    }
  </script>



<!---- End Content ------>
  