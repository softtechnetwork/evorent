<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ที่อยู่/สถานที่ติดตั้งสินค้า<small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="btn" href="<?=base_url('admin/inhabited/create');?>"  style="color: #466889;"><i class="fa fa-plus"></i> เพิ่มข้อมูลที่ติดตั้งสินค้า</a>
                </li>
            </ul>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">ลำดับ</th>
                            <th class="column-title">รหัสลูกค้า </th>
                            <th class="column-title">ชื่อลูกค้า </th>
                            <th class="column-title">ประเภทที่อยู่ </th>
                            <th class="column-title">ที่อยู่ </th>
                            <th class="column-title">ตำบล / แขวง </th>
                            <th class="column-title">อำเภอ / เขต </th>
                            <th class="column-title" style="text-align: center;">จังหวัด </th>
                            <th class="column-title no-link last" style="text-align: center;"><span class="nobr">รหัสไปรษณีย์</span></th>
                            <th class="column-title no-link last" style="text-align: center;"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                tr += '<tr class=" ">';
                tr += '<td class=" ">'+val['RowNum']+'</td>';
                tr += '<td class=" ">'+val['customer_code']+'</td>';
                tr += '<td class=" ">'+val['firstname']+' '+val['lastname']+'</td>';
                tr += '<td class=" ">'+val['category']+'</td>';
                tr += '<td class=" ">'+val['address']+'</td>';
                tr += '<td class=" ">'+val['district_name']+'</td>';
                tr += '<td class=" ">'+val['amphur_name']+'</td>';
                tr += '<td class=" last"  style="text-align: center;">'+val['province_name']+'</td>';
                tr += '<td class=" last"  style="text-align: center;">'+val['zip_code']+'</td>';

                tr += '<td class=" last"  style="text-align: center;">';
                tr +=   '<a href="'+base_url+'admin/Inhabited/edit/'+val['inhabited_code']+' ">';
                tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><!--<i class="fa fa-wrench"></i>--> แก้ไข</button>';
                tr +=   '</a>';
                tr += '</td>';
                tr += '</tr>';
            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [50, 100], [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/Inhabited/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }
</script>


<!---- End Content ------>
  