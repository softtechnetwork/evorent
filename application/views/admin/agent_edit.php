
<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขข้อมูลผู้รับมอบอำนาจ <small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="agent-edit-form" class="" action="<?php echo base_url('/admin/agent/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสผู้รับมอบอำนาจ</label>
                        <div class="col-md-3 col-sm-3">
                            <input  value="<?=$res[0]->agent_code;?>"  type="text"  class="form-control" readonly>
                        </div>
                    </div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขบัตรประชาชน</label>
                        <div class="col-md-6 col-sm-6">
                          <input class="form-control" value="<?=$res[0]->idcard;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="name" name="name" value="<?=$res[0]->name;?>" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นามสกุล</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="sname" name="sname" value="<?=$res[0]->sname;?>" required="required" />
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ปัจจุบัน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'><?=$res[0]->address;?></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-current">จังหวัด</label>
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->province_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
                                <option value="">อำเภอ / เขต</option>
                                <?php foreach ($amphoe as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->amphurs_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->amphur_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district">ตำบล / แขวง</label>
                            <select class="form-control" name="district"  id="district" required="required" >
                                <option value="">ตำบล / แขวง</option>
                                <?php foreach ($districs as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->district_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->district_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" value="<?=$res[0]->zip_code; ?>" readonly>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์มือถือ</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel" name="tel" type="tel" maxlength="10" class="form-control"  value="<?=$res[0]->tel; ?>" />
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันเกิด</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control date" type="date" id="bdate" name="bdate"  value="<?=$res[0]->bdate; ?>" readonly>
                            <!--<input  class="form-control date" type="date" id="bdate" name="bdate" />-->
                        </div>
                    </div>
                    <div class="ln_solid"> </div>





                                        
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสถานที่ประกอบการ</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="name-office" id="name-office" value="<?=$res[0]->office_name; ?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่ตั้งสถานที่ประกอบการ</label>
                        
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-office' name='address-office'><?=$res[0]->office_address; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-office">จังหวัด</label>
                            <select name='province-office' id='province-office' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->office_province == $item->id){echo 'selected="selected"';} ?>><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-office">อำเภอ / เขต</label>
                                <select class="form-control" name="amphurs-office"  id="amphurs-office" required="required" >
                                <option value="">อำเภอ / เขต</option>
                                <?php foreach ($amphoe as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->office_amphurs == $item->id){echo 'selected="selected"';} ?>><?php echo $item->amphur_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-office">ตำบล / แขวง</label>
                            <select class="form-control" name="district-office"  id="district-office" required="required" >
                                <option value="">ตำบล / แขวง</option>
                                <?php foreach ($districs as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($res[0]->office_district == $item->id){echo 'selected="selected"';} ?>><?php echo $item->district_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-office">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-office" id="zipcode-office" value="<?=$res[0]->office_zipcode; ?>" readonly>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel-office" name="tel-office" type="tel" maxlength="10" class="form-control"  value="<?=$res[0]->phone; ?>"  >
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                   
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อีเมล์</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control email" id="email" name="email" type="email"   value="<?=$res[0]->email; ?>" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ออกเอกสาร</label>
                        <div class="col-md-3 col-sm-3">                        
                            <input class="form-control datepicker" value="<?=date("d/m/Y", strtotime($res[0]->create_date));?>"  type="text" id="create-date" name="create-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <input name="agent_code" type="hidden"  value="<?=$res[0]->agent_code; ?>" />

                            <a href="<?php echo base_url('admin/agent');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">แก้ไขข้อมูลผู้รับมอบอำนาจ</button>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
            $('.next').text(">");
    });
    var base_url = "<?php echo base_url(); ?>";
    var AddressId = ["#province","#amphurs","#district","#zipcode","#address"]; // ห้าม เปลี่ยนตำแหน่ง
    var OfficeAddId = ["#province-office","#amphurs-office","#district-office","#zipcode-office","#address-office"]; // ห้าม เปลี่ยนตำแหน่ง
    
    rePlaceAddEle(AddressId, base_url);//replace ที่อยู่ปัจจุบัน
    rePlaceAddEle(OfficeAddId, base_url);//replace ที่อยู่ บริษัท
    var input_id = ["#name","#sname","#address","#province","#amphurs","#district","#tel", "#bdate"];
    AgentEditSubmit(input_id, '#agent-edit-form'); // validate and submit
    
</script>