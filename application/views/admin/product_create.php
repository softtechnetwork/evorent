<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างสินค้า</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-product-create" action="<?php echo site_url('/admin/product/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <select class="form-control" name="product-cate"  id="product-cate" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกประเภทสินค้า</option>
                                <?php foreach ($productCate as $item) : ?>
                                    <option value="<?= $item->cate_id;?>"><?= $item->cate_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <select class="form-control" name="product-brand"  id="product-brand" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกยี่ห้อสินค้า</option>
                                <?php foreach ($productBrand as $item) : ?>
                                    <option value="<?= $item->brand_id;?>"><?= $item->brand_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-name" id="product-name" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อรุ่นสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-version" id="product-version" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้าอ้างอิง</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-ref" id="product-ref" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดยี่สินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="product-detail" name="product-detail" class="form-control" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <!-- รูปแบบการผ่อนชำระ -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current">จำนวนงวด/เดือน ที่ผ่อนชำระ</label>
                            <input type="number" step="1" min="1" class="form-control" name="installment-amount1" id="installment-amount1" />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current"> จำนวนเงินที่ผ่อนชำระต่อ งวด/เดือน </label>
                            <input type="number" step="1" min="1" class="form-control" name="installment-pay1" id="installment-pay1" />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current"></label>
                            <div style="position: absolute;bottom: 1px;">
                                <button type="button" id="installment-plus" class="btn btn-info" style="margin-bottom: inherit;"><i class="fa fa-plus-square"></i></button>
                            </div>
                        </div>
                        <input type="hidden" name="installment-format-amount" id="installment-format-amount" value="1"/>

                    </div>
                    <div id="installment-format"></div>
                    <div class="ln_solid"> </div>
                    <!-- รูปแบบการผ่อนชำระ -->
                    
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/product');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">สร้างสินค้า</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();

    InstallmentPlus('#installment-plus', '#installment-format',"#installment-format-amount");
    
    var submitRequireEle = ["#product-cate","#product-brand","#product-name","#product-version","#product-ref", "#installment-format-amount"];
    ProductCreateSubmit(submitRequireEle, "#form-product-create");
    ProductEleChange(submitRequireEle); 
    InstallmentEleChange("#installment-format-amount"); 
    

</script>


