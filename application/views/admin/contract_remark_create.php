<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
    <div class="x_panel">
        <div class="x_title">
            <h2>เพิ่มหมายเหตุ<small></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <form id="action-form" class="" action="<?//=site_url('/admin/ContractRemark/action');?>" method="post"  enctype="multipart/form-data" novalidate>
                <input type="hidden" name="id" name="id"  value="<?=$id?>">
                <input type="hidden" name="action" value="insert">
                
                <div class="field item form-group">
                    <div class="col-md-6 col-sm-6">
                        <label class="label-align">สัญญา</label>
                        <select name='contract_code' id='contract_code' class="selectpicker form-control mh"  data-live-search="true">
                            <?php foreach ($contract as $item) : ?>
                                <option value="<?=$item->contract_code; ?>"><?=$item->contract_code; ?>  <?=$item->fullname; ?></option>
                                <?php endforeach ?>
                            </select>
                        </select>
                    </div>
                </div>
                <div class="field item form-group">
                    <div class="col-md-12 col-sm-12">
                    <label class="label-align">หมายเหตุ</label>
                        <textarea  class="form-control" required="required" id='remark' name='remark' cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <a href="<?=base_url('admin/ContractRemark');?>" type='button' class="btn btn-success">กลับ</a>
                        <button type='submit' class="btn btn-primary">บันทึก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

    var base_url = $('input[name="base_url"]').val();
    
    
    //### save form ###//
    $("form#action-form").on("submit",function(e){ // จะทำงานก็ต่อเมื่อกด submit ฟอร์ม
        e.preventDefault(); // ปิดการใช้งาน submit ปกติ เพื่อใช้งานผ่าน ajax
        var fd = new FormData(this); // เตรียมข้อมูล form สำหรับส่งด้วย  FormData Object
        $.ajax({
            url:base_url+'admin/ContractRemark/actions', //ให้ระบุชื่อไฟล์ PHP ที่เราจะส่งค่าไป
            type:'post',
            dataType: 'json',
            data:fd, //ข้อมูลจาก input ที่ส่งเข้าไปที่ PHP
            contentType: false,
            processData: false,
            success:function(response){ //หากทำงานสำเร็จ จะรับค่ามาจาก JSON หลังจากนั้นก็ให้ทำงานตามที่เรากำหนดได้
                // window.location.href = response.datas;
                // $("#loading-spinner").hide();
                if(response.status){
                    window.location.href = response.datas;
                    // Swal.fire({ icon: 'success',  text: response.massege });
                }else{
                    Swal.fire({ icon: 'warning',  text: response.massege });
                }
            }
        });
    });

</script>


<!---- End Content ------>
  