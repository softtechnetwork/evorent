<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }

    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px; color: #212529bd !important;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff !important;
        text-decoration: none;
        background-color: #1abb9c;
    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>สินค้าย่อย<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li style="margin-right: 3px; width:8rem;">
                    <select id="product_id" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">รหัสสินค้า</option>
                        <?php foreach ($product as $item) : ?>
                            <option value="<?= $item->product_id;?>"><?= $item->product_id;?></option>
                        <?php endforeach ?>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <input id="search_text" type="text"  class="form-control"/>
                </li>
                <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                    <i class="fa fa-search"></i> ค้นหา</button>
                </li>
                <li>
                    <button id="button-reset" type='button' class="btn btn-warning" style=" margin-right: 15px;border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li>
                <li>
                    <a class="btn btn-success" href="<?php echo base_url('admin/productSub/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มสินค้าย่อย
                    </a>
                </li>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="productSub-table" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 3rem">ลำดับ</th>
                            <th class="column-title" style="width: 8rem">รหัสสินค้าย่อย</th>
                            <th class="column-title">สินค้าสินค้าย่อย</th>
                            <th class="column-title" style="width: 8rem">รหัสสินค้า</th>
                            <th class="column-title">สินค้า</th>
                            <th class="column-title">ยี่ห้อสินค้า </th>
                            <th class="column-title no-link last" style="text-align: center;width:15rem;"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = $('input[name="base_url"]').val();
    var searchEle = ['#product_id', '#search_text'];
    DrawProductSubList(base_url, searchEle, '#productSub-table tbody');
    SearchProductSubList(base_url, '#button-search', searchEle, '#productSub-table tbody');
    ResetProductSubList(base_url, '#button-reset', searchEle, '#productSub-table tbody');
</script>



<!---- End Content ------>
  