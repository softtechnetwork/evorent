
<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>หลักฐานผู้รับมอบอำนาจ <small>รหัส: <?=$res[0]->agent_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สำเนาบัตรประชาชน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-card"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สำเนาทะเบียนบ้าน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-houseRegis"></div>
            </div>
        </div>
    </div>
</div>
<!-- Premise Model -->
<div id="modelPlace"></div>

<script>
    var base_url = "<?php echo base_url(); ?>";
    var agentCodep  = "<?php echo $res[0]->agent_code; ?>";
    var mode = "agent";

    var url_getRes = base_url+"admin/premise/getpremise"; // get premise
    var insertController = base_url+"admin/premise/insertpremise"; // insert premise
    getpremise(url_getRes, agentCodep, "#position-card", insertController, mode); // หลักฐาน สำเนาบัตรประชาชน
    getpremise(url_getRes, agentCodep, "#position-houseRegis", insertController, mode);// หลักฐาน สำเนาทะเบียนบ้าน
    
    AddPremise('.premise-add');
    viewPremise('.premise-view','#modelPlace');
    
</script>