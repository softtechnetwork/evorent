<style>
.fail-stat-sale{color: #d9530e;}
.succ-stat-sale{color: #26b99a!important;}

.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
#contract-ui li{ padding: 5px;}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขข้อมูลสัญญาลูกค้า <small><?=$res[0]->contract_code;?></small></h2>
                <ul id="contract-ui" class="nav navbar-right panel_toolbox" style="min-width: 0;">
                    <li>
                        <button id="AddonProduct-btn" type='button' class="btn btn-success" data-toggle="modal"   style=" border-radius: inherit;width: 100%;margin-bottom: inherit;padding: .2rem .75rem;"><i class="fa fa-plus"></i> สินค้า</button>
                     </li>
                    <li>
                        <a href="<?=base_url('admin/contractSerial/'.$res[0]->contract_code.'/'.$res[0]->product_id);?>" style="padding: 0;">
                            <button id="ContractSerial-btn" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;margin-bottom: inherit;padding: .2rem .75rem;">Serial Number</button>
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/Installments/'.$res[0]->contract_code);?>" style="padding: 0;">
                            <button type='button' class="btn btn-info" style=" border-radius: inherit;width: 100%;margin-bottom: inherit;padding: .2rem .75rem;">การผ่อนชำระ</button>
                        </a>
                     </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="contract-edit-form" action="<?=base_url('/admin/contract/update');?>" enctype="multipart/form-data"  method="post" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ลูกค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 ">
                                    <input type="text" name="customer" id="customer" class="form-control" value="<?php echo $res[0]->firstname." ".$res[0]->lastname;?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                                    <input class="form-control" type="text"  id="product" name="product" value="<?php echo $res[0]->cate_name;?>" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">ประเภทสินค้าสินค้า(สำหรับลูกค้า)</label>
                                    <input class="form-control" type="text"  id="customer_type" name="customer_type" value="<?php echo $res[0]->customerType_name;?>" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">ประเภทสัญญา</label>
                                    <input class="form-control" type="text"  id="contract_type" name="contract_type" value="<?php echo $res[0]->contractType_name;?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <input class="form-control" type="text"  id="product" name="product" value="<?php echo $res[0]->product_name;?>" readonly/>
                                </div>
                                
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">ยี่ห้อ</label>
                                    <input class="form-control" type="text" value="<?php echo $res[0]->brand_name;?>"  id="product-brand" name="product-brand"required="required" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ทำสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->contract_do_date));?>"  id="contract-do-date" name="contract-do-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่เริ่มสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->contract_date));?>"  id="contract-date" name="contract-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">วันที่เริ่มชำระงวดแรก</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->payment_start_date));?>"  id="payment-start-date" name="payment-start-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">กำหนดชำระวันที</label>
                        <div class="col-md-1 col-sm-1">
                            <!--<input class="form-control" type="number"  min="1"  max="31" value="<?php echo $res[0]->payment_due;?>" id="payment-due" name="payment-due" />-->
                            <select name='payment-due' id='payment-due' class="form-control" disabled>
                                <option value="15" <?php if($res[0]->payment_due == 15 ){ echo 'selected="selected"';} ?>>15</option>
                                <option value="30" <?php if($res[0]->payment_due == 30 ){ echo 'selected="selected"';} ?>>30</option>
                            </select>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1">ของทุกเดือน</label>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เจ้าของ ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control" type="text" id="inhabited-name" name="inhabited-name" value="<?=$res[0]->inhabited_name;?>" />
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <!--<th>เลือก</th>-->
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inhabited as $item) : ?>
                                        <tr class="">
                                            <!--
                                            <td style="  padding: .5rem;">
                                                <input class="inhabitedInput" type='radio'  name='inhabited' id='inhabited<?=$item->id?>' value='<?=$item->inhabited_code?>' <?php if($res[0]->addr == $item->inhabited_code){echo 'checked="checked"';} ?>/> 
                                            </td>
                                            -->
                                            <td><?=$item->address?></td>
                                            <td><?=$item->district_name?></td>
                                            <td><?=$item->amphur_name?></td>
                                            <td><?=$item->province_name?></td>
                                            <td><?=$item->zip_code?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ขนาดพื้นที่ติดตั้งอุปกรณ์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"  id="place-ins-size" name="place-ins-size" value="<?=$res[0]->place_ins_size;?>"/>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3">(ตารางเมตร)</label>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อัตราค่าเช่าพื้นที่ติดตั้งอุปกรณ์ รายปี</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"  id="annual-rental-rate" name="annual-rental-rate" value="<?=$res[0]->annual_rental_rate;?>"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าผ่อนรายเดือน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value="<?php echo $res[0]->monthly_rent;?>"  id="monthly-rent" name="monthly-rent"required="required" readonly/>
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">ภาษีมูลค่าเพิ่ม</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value="<?php echo $res[0]->monthly_plus_tax;?>"  id="monthly-plus-tax" name="monthly-plus-tax"readonly/>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">ระยะเวลาการเช่า</label>
                        <div class="col-md-1 col-sm-1">
                            <input class="form-control"  min="1"  type="number" value="<?php echo $res[0]->rental_period;?>" id="rental-period" name="rental-period"required="required" readonly/>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1">เดือน</label>
                    </div>
                    -->

                    <?php if($res[0]->installment != ''){ ?>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                            <div class=" col-md-4 col-sm-4">
                                <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                    <thead>
                                        <tr>
                                            <th>จำนวนงวด</th>
                                            <th>ผ่อนเดือนละ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?=$res[0]->rental_period;?></td>
                                            <td>
                                                <?=$res[0]->monthly_rent;?>
                                                <!-- <button type="button" class="btn btn-round edit-instalment" style=" font-size: 13px; padding: 0px;"><i class="fa fa-wrench"></i></button> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" id="installment" name="installment"/>
                        </div>
                    <?php } ?>
                    <div class="ln_solid"></div>



                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เงินดาวน์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" min="0" value="<?php echo $res[0]->down_payment;?>"  id="down-payment" name="down-payment" required="required" readonly/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระค่าเช่าล่วงหน้าเป็นเงินจำนวน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="<?php echo $res[0]->advance_payment;?>" min="0" id="advance-payment" name="advance-payment"required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จำนวนเงินที่ต้อง ชำระเริ่มต้น</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="<?php echo $res[0]->statart_payment_amount;?>" min="0" id="statart_payment_amount" name="statart_payment_amount"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าธรรมเนียมดำเนินการติดตั้ง</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"value="<?php echo $res[0]->installation_fee;?>" min="0" id="installation-fee" name="installation-fee"required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">มูลค่าสินค้า(ที่ซื้อจาก PSI)</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"value="<?php echo $res[0]->product_value;?>" min="0" id="product-value" name="product-value"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">มูลค่าทรัพย์สินเมื่อครบสัญญา</label>
                        <div class="col-md-1 col-sm-1">
                            <label class="col-form-label label-align">% ในการคิดคำนวน</label>
                            <input class="form-control" type="number"value="<?php echo $res[0]->percen_end_contract;?>" min="0" id="percen-end-contract" name="percen-end-contract" readonly/>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label class="col-form-label label-align">มูลค่าทรัพย์สิน</label>
                            <input class="form-control" type="number"value="<?php echo $res[0]->value_end_contract;?>" min="0" id="value-end-contract" name="value-end-contract" readonly/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระโดยการโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร กสิกรไทย</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร ไยพาณิชย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="ln_solid"></div>-->

                    <!--------  Supporter --------->
                    <input type="hidden" id='supportered' name='supportered' value="<?=$res[0]->supporter;?>"/>
                    <?php if($res[0]->supporter == 1){?>

                        <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-8 col-sm-8 ">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <!--<th>เลือก</th>-->
                                        <th>ผู้คำ้ประกัน</th>
                                        <th>หมายเลขบัตรประชาชน</th>
                                        <th>หมายเลขมือถือ</th>
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td><?=$supporter[0]->fname?> <?=$supporter[0]->lname?></td>
                                        <td><?=$supporter[0]->idcard?></td>
                                        <td><?=$supporter[0]->tel?></td>
                                        <td><?=$supporter[0]->addr?></td>
                                        <td><?=$supporter[0]->district_name?></td>
                                        <td><?=$supporter[0]->amphur_name?></td>
                                        <td><?=$supporter[0]->province_name?></td>
                                        <td><?=$supporter[0]->zip_code?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <?php }else{ ?>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-9 col-sm-9">
                            <ul id="stats" style=" padding-left: initial; margin-top: 6px;">
                                <li style=" display: inline;list-style-type: none;padding-right: 3px;float: left;">
                                    <input class="inhabitedInput" type="checkbox" id='supporter' name='supporter' value=""/>
                                    <input type="hidden" id='supporter-check' name='supporter-check' value="0"/>
                                </li>
                                <li style=" display: inline;list-style-type: none;padding-right: 10px;float: left;">
                                    <p>เลือกเมื่อมีผู้คำ้ประกัน</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="field item form-group collapse" id="collapse-supporter">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-9 col-sm-9">
                            <div class="card card-body " style="border-radius: inherit;background-color: #f8f9fa;">
                                <div class="field item form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">ชื่อ ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="name-supporter" name="name-supporter"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">นามสกุล ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="sname-supporter" name="sname-supporter"/>
                                    </div>
                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขบัตรประชาชน ผู้คำ้ประกัน</label>
                                        <input name="idcard-supporter" id="idcard-supporter" type="tel"  maxlength="13"  class="form-control" required="required">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">วันเกิด ผู้คำ้ประกัน</label>
                                        <input class="form-control datepicker" type="text" id="bdate-supporter" name="bdate-supporter"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขมือถือ ผู้คำ้ประกัน</label>
                                        <input class="form-control" type="tel" id="mobile-supporter" name="mobile-supporter" >
                                    </div>

                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label class="col-form-label label-align">ที่อยู่ผู้คำ้ประกัน</label>
                                        <textarea  class="form-control" required="required" id='address-supporter' name='address-supporter'></textarea>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <div class="col-md-3 col-sm-3">
                                        <label for="province-supporter">จังหวัด</label>
                                        <select name='province-supporter' id='province-supporter' class="form-control">
                                            <option value="" selected='false' disabled>จังหวัด</option>	
                                            <?php foreach ($province as $item) : ?>
                                                <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="amphurs-supporter">อำเภอ / เขต</label>
                                        <select class="form-control" name="amphurs-supporter"  id="amphurs-supporter">
                                            <option value="">อำเภอ / เขต</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="district-supporter">ตำบล / แขวง</label>
                                        <select class="form-control" name="district-supporter"  id="district-supporter">
                                            <option value="">ตำบล / แขวง</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="zipcode-supporter">รหัสไปรษณีย์</label>
                                        <input type="tel" maxlength="5" class="form-control" name="zipcode-supporter" id="zipcode-supporter" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <?php } ?>
                    <!---------------------------------------->

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นายหน้าให้เช่าทรัพท์สินและบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" value="<?php echo $res[0]->sale_code;?>" id="sale" name="sale" />-->
                            <input class="form-control" value="<?php echo $res[0]->sale_code;?>"   name="sale-id" readonly />
                        </div>
                        <!--<button id="sale-check"  type='button' class="btn btn-info">ตรวจสอบ</button>-->
                        <!--<label id="sale-status" class="col-form-label col-md-3 col-sm-3"></label>-->

                        <label class="col-form-label col-md-1 col-sm-1  label-align">ตัวแทนบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" value="<?php echo $res[0]->techn_code;?>" id="sale" name="sale" />-->
                            <input class="form-control" value="<?php echo $res[0]->techn_code;?>"  id="techn-id" name="techn-id" readonly />
                        </div>

                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเหตุ</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" name='remark'><?php echo $res[0]->remark;?></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Status</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='estatus' id='estatus' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option  value="<?php echo $item->id; ?>" key="<?php echo $item->status_code; ?>"
                                        <?php if($res[0]->status == $item->id ){ echo 'selected="selected"';} ?> >
                                        <?php echo $item->label; ?>
                                    </option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    
                    <input type="hidden" name="contract_code" value="<?php echo $res[0]->contract_code; ?>">
                    <input type="hidden"  name="product_id" value="<?php echo $res[0]->product_id;?>">
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/contract');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">Submit</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Addon Contract -->
<div class="clearfix"></div>
<div id="AddonContract-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <form id="AddonContract-form" action="<?=base_url('/contractaddon/addon');?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">ส่วนเสริมสัญญา รหัสสัญญา : <?=$res[0]->contract_code;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--
                    <div class="row">
                        <div class="col-md-12"><h6>สัญญาหลัก</h6></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><label class="col-form-label label-align"><strong>ลูกค้า : </strong><?=$res[0]->firstname;?> <?=$res[0]->lastname;?></div>
                        <div class="col-md-6"><label class="col-form-label label-align"><strong>สินค้า : </strong><?=$res[0]->product_name;?></div>
                        <div class="col-md-2"><label class="col-form-label label-align"><strong>ผ่อนชำระ : </strong><?=$res[0]->rental_period;?> งวด/เดือน</div>
                    </div>
                    <hr>
                    -->
                    <div id="contract-addon-place"></div>
                    <div class="row">
                        <div class="col-md-12"><h6>เพิ่ม ส่วนเสริมสัญญา</h6></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align"><strong>สินค้า เพิ่มเติม : </strong></label>
                            <select id='addon-contract-products' name='addon-contract-products' class='selectpicker form-control mh' data-live-search='true' data-product="<?=$res[0]->product_id;?>" ></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>จำนวนงวด/เดือน : </strong></label>
                            <input id='addonrental_period' name='addonrental_period' class='form-control' type="number" min="1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>ราคาผ่อนต่องวด/เดือน : </strong></label>
                            <input id='addonmonthly_rent' name='addonmonthly_rent' class='form-control' type="number">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>วันที่ชำระงวดแรก : </strong></label>
                            <input class="form-control datepicker" 
                                type="text" 
                                id="addon-payment-start-date" 
                                name="addon-payment-start-date"  
                                data-provide="datepicker" 
                                data-date-language="th-th" 
                                autocomplete="off" 
                                placeholder="วว/ดด/ปป"
                            >
                        </div>
                        <div class="col-md-3 col-sm-3" style="padding-top:1.8rem;">
                            
                            <input id='contract_code' name='contract_code' type="hidden" value="<?=$res[0]->contract_code;?>">
                            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>-->
                            <button type="submit" class="btn btn-primary" style="width:100%;">เพิ่ม ส่วนเสริมสัญญา</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Addon Product -->
<div class="clearfix"></div>
<div id="AddonProduct-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <form id="AddonProduct-form" action="<?=base_url('/contractaddon/addonproduct');?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">ส่วนเสริมสินค้า รหัสสัญญา : <?=$res[0]->contract_code;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="products-addon-place"></div>
                    <div class="row"><div class="col-md-12"><h6>เพิ่ม ส่วนเสริมสินค้า</h6></div></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <label class="col-form-label label-align"><strong>หมวดหมู่ สินค้า: </strong></label>
                            <select id='addon-pcate' name='addon-pcate' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <label class="col-form-label label-align"><strong>สินค้า: </strong></label>
                            <select id='addon-pmaster' name='addon-pmaster' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                    </div>

                    <div class="row justify-content-between">
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>จำนวน : </strong></label>
                            <input id='addon-pcount' name='addon-pcount' class='form-control'type="number" min="1" value="1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>ประเภท สินค้า: </strong></label>
                            <select id='addon-pstatus' name='addon-pstatus' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>Serial number : </strong></label>
                            <div class="checkbox" style="padding-top: 4%;">
                                <label class="">
                                    <div class="icheckbox_flat-green checked" style="position: relative;">
                                        <input name="active-serial" type="checkbox" class="flat" checked="checked" style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                    </div> มี Serial number
                                </label>
                            </div>
                        </div>


                        

                        <div class="col-md-3 col-sm-3" style="padding-top:1.8rem;">
                            <input id='contract_code' name='contract_code' type="hidden" value="<?=$res[0]->contract_code;?>">
                            <button type="submit" class="btn btn-primary" style="width:100%;">เพิ่ม ส่วนเสริมสัญญา</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
        $('.next').text(">");
    });
    var base_url = "<?php echo base_url(); ?>";
    //--------- supporter ------------  //
    $("#supporter").change(function () {
        var supporter = $('#'+this.id).prop('checked');
        if(supporter){
            $("#supporter-check").val(1);
            $("#collapse-supporter").collapse('show');
        }else{
            $("#supporter-check").val(0);
            $("#collapse-supporter").collapse('hide');
        }
    });

    var supporterProvince = ["#province-supporter","#amphurs-supporter","#district-supporter","#zipcode-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    rePlaceAddEle(supporterProvince, base_url);//replace ข้อมูลสถานที่ทำงาน

    var supporterEle = ["#supporter","#name-supporter","#sname-supporter","#idcard-supporter","#bdate-supporter","#mobile-supporter","#address-supporter",  "#province-supporter","#amphurs-supporter","#district-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    ChangeRemoveValidClass(supporterEle);
    //--------------------------------//

    //################  submit Edit form #############//
    var input_id = [];
    tempEditSubmit(input_id, '#contract-edit-form', supporterEle);
</script>
<script src="<?=base_url('./assete/js/admin/contract_custom.js');?>"></script>

