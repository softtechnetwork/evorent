<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>เพิ่มหมายเลขเครื่อง</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-serial-addition" action="<?php echo site_url('/admin/contractSerial/additional');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สัญญา</label>
                        <div class="col-md-6 col-sm-6 ">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสัญญา</label>
                                    <input class="form-control" type="text" name="contract-id"  id="contract-id" value="<?=$resTemp[0]->contract_code?>" readonly/>
                                </div>
                                <!--<div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text" name="product-number"  id="product-number" value="<?=$resTemp[0]->product_number?>" readonly/>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสินค้า</label>
                                    <input class="form-control" type="text" id="product_id" name="product_id" value="<?=$resTemp[0]->product_id?>" readonly/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <input class="form-control" type="text" id="product_name" name="product_name" value="<?=$resTemp[0]->product_name?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-6 col-sm-6">
                            <?php 
                            /*
                                $subCount = (int)count($resSubproduct);
                                $routCount = 1;
                                for ($i = 0; $i < count($resSerial); $i++ ) {
                                    $stringsRow = '';
                                    $strings = '<div class="col-md-6 col-sm-6">';
                                    $strings .= '<label class="col-form-label label-align">'.$resSerial[$i]->product_master_name.'</label>';
                                    $strings .= '<input readonly class="form-control" type="text" value="'.$resSerial[$i]->serial_number.'" />';
                                    $strings .= '</div>';
                                    $strings .= '<div class="col-md-6 col-sm-6">';
                                    $strings .= '<label class="col-form-label label-align">หมายเหตู</label>';
                                    $strings .= '<textarea  class="form-control" rows="1" readonly>'.$resSerial[$i]->remark.'</textarea>';
                                    $strings .= '</div>';

                                    if (($i % $subCount == 0)) {
                                        $stringsRow .= '<div class="row">'.$strings;
                                    }else{
                                        $stringsRow .= $strings;
                                    }

                                    if($routCount == $subCount ){
                                        $stringsRow .= '</div>';
                                        $stringsRow .= '<div class="ln_solid"> </div>';
                                        $routCount = 1;
                                    }else{
                                        $routCount++;
                                    }
                                    echo $stringsRow;
                                }
                                */
                            ?>
                            <div class="row">
                            <?php foreach($resSerial as $item):?>
                                <div class="col-md-1 col-sm-1" style="padding-top: 2rem;">
                                    <input class="form-control" type="text"  value="<?=$item->no;?>" readonly/>
                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <label class="col-form-label label-align"><?=$item->product_master_name;?></label>
                                    <input class="form-control" type="text" value="<?=$item->serial_number;?>" readonly/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">หมายเหตู</label>
                                    <textarea  class="form-control" rows="1" readonly><?=$item->remark;?></textarea>
                                </div>
                                <div class="ln_solid"> </div>
                            <?php endforeach;?>
                            </div>
                        </div>
                    </div>-->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                            <?php foreach($resSubproduct as $item):?>
                                <!--<div class="col-md-1 col-sm-1" style="padding-top: 2rem;">
                                    <input class="form-control" type="text" readonly/>
                                </div>-->
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align"><?=$item->product_master_name;?></label>
                                    <input class="form-control" type="text" id="serial_<?=$item->id;?>" name="serial_<?=$item->id;?>" />
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">หมายเหตู</label>
                                    <textarea  class="form-control" rows="1" id="remark_<?=$item->id;?>" name="remark_<?=$item->id;?>" ></textarea>
                                </div>
                            <?php endforeach;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/contractSerial');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success"> เพิ่มหมายเลขเครื่อง</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var elementID = ["#contract-id", "#product_id"];
    /*ContractSerial_AdditionSubmit(elementID, "#form-serial-addition");
    function ContractSerial_AdditionSubmit(element_id, submit_id){
        $(submit_id).submit(function() {
            var valids  = 0;
            var subProduct = ContractSerial_GetSubByProduct(base_url, $(element_id[1]).val());
            if(subProduct.length > 0 ){
                $.each(subProduct, function (key, val) {
                    var serialID = "#serial_"+val.id;
                    if($(serialID).val() == ''){
                        $(serialID).addClass("input-valid");
                        valids++;
                    }
                });
            }

            if(valids == 0){
                return true;
            }else{
                return false;
            }
            //return false;
        });
    };
    
    function ContractSerial_GetSubByProduct(base_url, product_id){
        var res = null;
        $.ajax({
            url: base_url+"admin/contractSerial/GetSubByProduct", //ทำงานกับไฟล์นี้
            data:  {
                'product_id':product_id
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    };*/
</script>


