<style> .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ประวัติการซ่อมบำรุง</h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">เลขที่สัญญา </th>
                        <th class="column-title">ลูกค้า </th>
                        <th class="column-title">สินค้า </th>
                        <!--<th class="column-title" style="text-align: center;">จำนวนครั้ง</th>-->
                        <th class="column-title" style="text-align: center;">Action</th>
                    </tr>
                    </thead>

                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!---- End Content ------>
<script>
    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                tr += '<tr class="even pointer">';
                tr += '<td class="a-center ">'+val['RowNum']+'</td>';
                tr += '<td class=" ">'+val['contract_code']+'</td>';
                tr += '<td class=" ">'+val['firstname']+' '+val['lastname']+'</td>';
                tr += '<td class=" ">'+val['product_name']+'</td>';
                tr += '<td class=" last"  style="text-align: center;">';
                tr +=   '<a href="'+base_url+'admin/ContractService/view/'+val['service_code']+' ">';
                tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i> View</button>';
                tr +=   '</a>';
                tr += '</td>';
                tr += '</tr>';
            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [30, 100], [30, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/ContractService/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }


</script>