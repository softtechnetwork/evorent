<!-- page content -->
<style> 
.btn.disabled, .btn:disabled {
        opacity: .25 !important;
        background-color: #565656 !important;
        border: 1px solid #565656 !important;
    }
</style>    
<div class="clearfix"></div>

<div class="row">
    <div class="col-sm-4">
        <div class="x_panel">
            <div class="x_title">
                <h2>ติดต่อจากหน้าเว็บ EVORENT<small></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="WebContract_pie" style="height:370px;"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <!-- <div class="x_panel">
            <div class="x_title">
                <h2>ข้อมูลการผ่อนชำระ<small></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="DashboardInstallment_chart" style="height:370px;"></div>
            </div>
        </div> -->
        <div class="x_panel">
            <div class="x_title">
                <h2>สัญญาทั้งหมด<small></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="AllContract_pie" style="height:370px;"></div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="x_panel">
            <div class="x_title">
                <h2>สถานะการผ่อนชำระ</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-4"style=" text-align: end; ">
                        <h2>เลขที่สัญญา : </h2>
                    </div>
                    <div class="col-6">
                        <div id="contract_selection"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <style>
                            #contract_selection > .bootstrap-select .dropdown-toggle:focus{ border: 1px solid #d5d9dc !important; outline: 0 !important;outline: 0 !important; box-shadow: unset;}
                        </style>
                        <div id="DrawContractEchartBarPanel" style="height:316px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="row" id="TableInstallment_panel">
    <div class="col-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ตารางการค้าง และครบกำหนดชำระ</h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-8">
                        <div class="table-responsive">
                            <table id="DashboardInstallment_table" class="table table-striped jambo_table bulk_action">
                                <thead class="headings" style="font-size: 11px;">
                                    <th class="column-title">ลำดับ</th>
                                    <th class="column-title">รหัสสัญญา</th>
                                    <th class="column-title">งวดที่</th>
                                    <th class="column-title">ลูกค้า </th>
                                    <th class="column-title">ราคาผ่อน</th>
                                    <th class="column-title">วันที่ครบชำระ</th>
                                    <th class="column-title">วันที่สิ้นสุดชำระ</th>
                                    <th class="column-title">การค้างชำระ</th>
                                    <th class="column-title" style="text-align:center;">กิจกรรม</th>
                                </thead>
                                <tbody style="font-size: 11px;"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-4">
                        <div id="InstallentTable_pie" style="height:350px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>ยอดชำระ 12 เดือนย้อนหลัง<small>(ประมาณการ)</small></h2>
        <!-- <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul> -->
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div id="DrawPaymentEchartBarPanel" style="height:350px;"></div>
        </div>
    </div>
</div>



<!---  Modal send message    ---->
<div id="message-form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="x_content" style="padding: 0 1rem;">
                <!--<form id="instalment-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('/admin/dashboard/sendSMS');?>">-->
                <form id="instalment-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('/admin/loan/sendSMS');?>">
                
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12" style=" margin-top: 1rem;">
                            <h5 class="modal-title">ข้อมูลก่อนส่ง SMS</h5>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 ">
                            <label class="col-form-label label-align">ลูกค้า</label>
                            <input type="text" id="customer-sms" name="customer-sms" class="form-control" readonly>
                            <input type="hidden" id="customer-code" name="customer-code">
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-4 col-sm-4 ">
                            <label class="col-form-label label-align">เลขที่สัญญา</label>
                            <input type="text" id="temp-code-sms" name="temp-code-sms" class="form-control" readonly>
                        </div>
                        <div class="col-md-3 col-sm-3 ">
                            <label class="col-form-label label-align">งวดที่</label>
                            <input type="number" id="inst-period-sms" name="inst-period-sms" class="form-control" readonly>
                        </div>
                        <div class="col-md-5 col-sm-5 ">
                            <label class="col-form-label label-align">จำนวนที่ต้องชำระ</label>
                            <input type="number" id="inst-payment-sms" name="inst-payment-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        
                        <div class="col-md-4 col-sm-4 ">
                            <label class="col-form-label label-align"> วันที่ครบกำหนดชำระ</label>
                            <input type="text" id="duedate-sms" name="duedate-sms"  class="form-control" readonly>
                        </div>
                        <div class="col-md-8 col-sm-8 ">
                            <label class="col-form-label label-align"> หมายเลขโทรศัพท์มือถือ</label>
                            <input type="text" id="tel-sms" name="tel-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 ">
                            <label class="col-form-label label-align">หัวข้อ SMS</label>
                            <input type="text" id="keywords-sms" name="keywords-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 label-align">
                            <button class="btn btn-primary" type="button" data-dismiss="modal" >ยกเลิก</button>
                            <button id="sms-btm-submit" type="submit" class="btn btn-success">ส่ง SMS</button>
                            <input type="hidden" id="cate" name="cate" value='<?=$this->uri->segment(1).'/'.$this->uri->segment(2)?>'>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<script>
    var base_url = "<?=base_url('admin/dashboard/');?>";

    function messageModal(json) {
        $('#message-form').modal('show');
        
        $('#customer-sms').val(json.customer);
        $('#customer-code').val(json.customer_code);
        $('#temp-code-sms').val(json.temp_code);
        $('#inst-period-sms').val(json.period);
        $('#inst-payment-sms').val(json.payment);
        $('#keywords-sms').val(json.keywords);
        $('#duedate-sms').val(json.duedate);
        $('#tel-sms').val(json.tel);
    }
    
    $('#message-form').submit(function() {
        var confirmPanel = confirm("คุณต้องการที่จะส่งข้อความไปยังหมายเลขโทรศัพท์มือถือ "+$('#tel-sms').val()+' จริงหรือไม่');
        if (confirmPanel == true) {
            return true;
        }else{
            return false;
        }
        //return false;
    });
    

    //#########  Dashboard WebContract #########//
    var DashboardWebContract_res = DashboardWebContractRes(base_url+"DashboardWebContractRes");
    DashboardWebContractPie('WebContract_pie', DashboardWebContract_res);

    //#########  Dashboard Installment #########//
    // var DashboardInstallment_res = DashboardInstallmentRes(base_url+"DashboardInstallmentRes");
    // DashboardInstallment_chart('DashboardInstallment_chart', DashboardInstallment_res);
    
    //#########  Dashboard Installment table #########//
    // var TableInstallment_res = GetInstallmentToTable(base_url+"TableInstallmentRes");
    // if(TableInstallment_res.length > 0){
    //     $('#TableInstallment_panel').show();
    //     DashboardInstallment_table('#DashboardInstallment_table tbody', TableInstallment_res);
    // }else{
    //     $('#TableInstallment_panel').hide();
    // }
    
    // InstallentTable_pie('InstallentTable_pie', null, null);
    // $('.contracted').click(function () {
    //     var contract_code = $(this).attr('attr-data');
    //     var chart_res = GetInstallentTable_pie(base_url+"TableInstallmentResPie", contract_code);
    //     InstallentTable_pie('InstallentTable_pie', chart_res, contract_code);
    // });


    //#########  Dashboard AllContract #########//
    DrawAllContractPie();
    function DrawAllContractPie(){

        var res = Get_AllContractRes(base_url);
        var elem = 'AllContract_pie';
        var datas = (res.datas != null) ? res.datas : [{value: 0, name: 'ไม่มีข้อมูล'}] ;
        
        var echartPie = echarts.init(document.getElementById(elem), theme);
        var colorPalette = [];
        $.each(res.datas, function (i, val) {
            colorPalette.push(val.background_color);
        });
        
        echartPie.setOption({
            title: { 
                text: 'สัญญาทั้งหมด '+ res.total+' สัญญา', 
                //subtext: 'ข้อความที่ติดต่อจากหน้าเว็บไซต์ EVORENT', 
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
                    console.log(params);
                    var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + color + '"></span>';
                    return `${colorSpan(params.color)} ${params.data.name} : ${params.data.value} สัญญา ( ${params.percent}% )`;
                }
                //formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                x: 'center',
                y: 'bottom',
                data: res.legend
            },
            toolbox: {
                show: true,
                feature: {
                    magicType: {
                        show: true,
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'left',
                                max: 1548
                            }
                        }
                    },
                    // restore: {
                    //     show: true,
                    //     title: "Restore"
                    // },
                    // saveAsImage: {
                    //     show: true,
                    //     title: "Save Image"
                    // }
                }
            },
            calculable: true,
            color: colorPalette,
            series: [{
                type: 'pie',
                radius: '70%',
                center: ['50%', '50%'],
                data: datas
            }]
        });

        var dataStyle = {
            normal: {
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            }
        };

        var placeHolderStyle = {
            normal: {
                color: 'rgba(0,0,0,0)',
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            },
            emphasis: {
                color: 'rgba(0,0,0,0)'
            }
        };
    }
    function Get_AllContractRes(base_url){
        var res = null;
        $.ajax({
            url: base_url+"DashboardAllContractRes", //ทำงานกับไฟล์นี้
            // data: {/*'contract_code':contract_code*/},  
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data; },
            error: function(xhr, status, exception) { }
        });
        return res;
    };


    //#########  Dashboard Contract EchartBar Panel #########//
    DrawContractEchartBarPanel();
    function DrawContractEchartBarPanel(){

        var res = Get_ContractRes(base_url);// Get sContractRes
        
        // Drawing contract select options
        var selections = '';
        selections += '<select id="contract_code" name="contract_code" class="selectpicker form-control mh " data-live-search="true">';
        $.each(res, function (i, val) {
            selections += '<option value="'+val.contract_code+'">'+val.contract_code+'</option>';
        });
        selections += '</select>';
        $('#contract_selection').html(selections);


        DrawContractEchartBar(res[0].contract_code, "DrawContractEchartBarPanel"); //set echartbar

        //option selected 
        $('select#contract_code').on('change', function () {
            DrawContractEchartBar(this.value, "DrawContractEchartBarPanel");
        });

    }

    function DrawContractEchartBar(contract_code, panel){
        var res = Get_InstallmentContractEchardBar(contract_code);
        var elem = panel;
        var datas = (res.datas != null) ? res.datas : [{value: 0, name: 'ไม่มีข้อมูล'}] ;
        
        var echartPie = echarts.init(document.getElementById(elem), theme);
        var colorPalette = [];
        $.each(res.datas, function (i, val) {
            colorPalette.push(val.background_color);
        });
        
        echartPie.setOption({
            title: { 
                text: 'จำนวนงวดทั้งหมด '+ res.total+' งวด', 
                //subtext: 'ข้อความที่ติดต่อจากหน้าเว็บไซต์ EVORENT', 
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
                    console.log(params);
                    var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + color + '"></span>';
                    return `${colorSpan(params.color)} ${params.data.name} : ${params.data.value} งวด ( ${params.percent}% )`;
                }
                //formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                x: 'center',
                y: 'bottom',
                data: res.legend
            },
            toolbox: {
                show: true,
                feature: {
                    magicType: {
                        show: true,
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'left',
                                max: 1548
                            }
                        }
                    },
                    // restore: {
                    //     show: true,
                    //     title: "Restore"
                    // },
                    // saveAsImage: {
                    //     show: true,
                    //     title: "Save Image"
                    // }
                }
            },
            calculable: true,
            color: colorPalette,
            series: [{
                type: 'pie',
                radius: '70%',
                center: ['50%', '50%'],
                data: datas
            }]
        });

        var dataStyle = {
            normal: {
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            }
        };

        var placeHolderStyle = {
            normal: {
                color: 'rgba(0,0,0,0)',
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            },
            emphasis: {
                color: 'rgba(0,0,0,0)'
            }
        };

    }

    function Get_ContractRes(base_url){
        var res = null;
        $.ajax({
            url: base_url+"getContractRes", //ทำงานกับไฟล์นี้
            // data: {/*'contract_code':contract_code*/},  
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data; },
            error: function(xhr, status, exception) { }
        });
        return res;
    };
    
    function Get_InstallmentContractEchardBar(contract_code){
        var res = null;
        $.ajax({
            url: base_url+"InstallmentContractEchardBar", //ทำงานกับไฟล์นี้
            data: {'contract_code':contract_code},  
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data; },
            error: function(xhr, status, exception) { }
        });
        return res;
    };



    //#########  Dashboard Payment amount monthy Panel #########//
    DrawPaymentEchartBarPanel();
    function DrawPaymentEchartBarPanel(){

        var res = get_PaymentAmountRetrospect(base_url);// Get datas

        var months = [];
        var payment = [];
        $.each(res, function (i, val) {
           months.push(toThaiMonthString(new Date(val.Year+'/'+val.Month)));
           payment.push(val.payment_amount);
        });

        // Drawing Payment Amount Retrospect
        var echartBar = echarts.init(document.getElementById('DrawPaymentEchartBarPanel'), theme);
        echartBar.setOption({
            // title: {
            //     text: 'Graph title',
            //     subtext: 'Graph Sub-text'
            // },
            tooltip: { trigger: 'axis' },
            // legend: {
            //     data: ['sales']
            // },
            toolbox: { show: false },
            calculable: false,
            xAxis: [{
                type: 'category',
                data: months
            }],
            yAxis: [{ type: 'value' }],
            series: [{
                name: 'ยอดชำระ',
                type: 'bar',
                data: payment,
                markPoint: {
                    data: [{
                        type: 'max',
                        name: 'สูงสุด'
                    }, {
                        type: 'min',
                        name: 'ต่ำสุด'
                    }]
                },
                markLine: {
                    data: [{
                        type: 'average',
                        name: 'เฉลี่ย'
                    }]
                },
            }]
        });

    }
    function get_PaymentAmountRetrospect(base_url){
        var res = null;
        $.ajax({
            url: base_url+"getPaymentAmountRetrospect", //ทำงานกับไฟล์นี้
            // data: {/*'contract_code':contract_code*/},  
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data; },
            error: function(xhr, status, exception) { }
        });
        return res;
    };
    function toThaiMonthString(date) {
        // let monthNames = [ "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม.", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ];
        let monthNames = [ "ม.ค", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย", "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค" ];

        //let year = date.getFullYear() + 543;
        let year = date.getFullYear().toString().substr(-2);
        let month = monthNames[date.getMonth()];
        let numOfDay = date.getDate();

        // let hour = date.getHours().toString().padStart(2, "0");
        // let minutes = date.getMinutes().toString().padStart(2, "0");
        // let second = date.getSeconds().toString().padStart(2, "0");

        //return `${numOfDay} ${month} ${year} ` + `${hour}:${minutes}:${second} น.`;
        return ` ${month} ${year} `;
    }

</script>
<!-- /page content -->