<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขสินค้า(ชุด)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSet-edit" action="<?php echo base_url('/admin/productSet/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-3 col-sm-3  ">
                            <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                            <input class="form-control" type="text" name="product-cate" id="product-cate" value='<?=$res[0]->cate_name;?>' disabled/>
                        </div>
                        <div class="col-md-3 col-sm-3  ">
                            <label class="col-form-label label-align">ยี่ห้อสินค้า</label>
                            <select class="form-control" name="product-brand"  id="product-brand" required="required" disabled>
                                <option value="" selected='false' disabled>กรุณาเลือกยี่ห้อสินค้า</option>
                                <?php foreach ($productBrand as $item) : ?>
                                    <option value="<?= $item->brand_id;?>" <?php if($res[0]->product_brand == $item->brand_id){echo 'selected="selected"';} ?>><?= $item->brand_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="product-name" id="product-name" value='<?=$res[0]->product_name;?>'/>
                        </div>
                        
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รุ่น</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="product-version" id="product-version" value='<?=$res[0]->product_version;?>'/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา</label>
                        <div class="col-md-3 col-sm-3">
                            <input  type="number" min="0" class="form-control" name="product-pricce" id="product-pricce"  value='<?=$res[0]->product_pricce;?>'/>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท ราคาค่าติดตั้ง</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="installation-type"  id="installation-type">
                                <?php foreach ($installationType as $item) : ?>
                                    <option data-code="<?=$item->status_code;?>" value="<?=$item->id;?>"<?php if($res[0]->product_installation_type == $item->id){echo 'selected="selected"';} ?>><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input type="text" class="form-control" 
                                name="installation-fee" id="installation-fee" 
                                <?php if($res_installationType[0]->status_code == 1){?>
                                    value='0'  disabled
                                <?php }else{ ?>
                                    value='<?=$res[0]->product_installation_fee;?>' 
                                <?php } ?>/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท ลูกค้า</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="customer-type"  id="customer-type">
                                <?php foreach ($productType as $item) : ?>
                                    <option value="<?=$item->id;?>" <?php if($res[0]->product_customer_type == $item->id){echo 'selected="selected"';} ?>><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท สัญญา(เอกสารสัญญา)</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="contract-type"  id="contract-type">
                                <?php foreach ($contractType as $item) : ?>
                                    <option value="<?=$item->id;?>" <?php if($res[0]->product_contract_type == $item->id){echo 'selected="selected"';} ?>><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3  ">
                            <select class="form-control" name="contract-doc"  id="contract-doc"  required="required">
                                <?php foreach ($contractDoc as $item) : ?>
                                    <option value="<?=$item->contract_id;?>" <?php if($res[0]->product_contract_doc == $item->contract_id){echo 'selected="selected"';} ?>><?=$item->contract_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="product-detail" name="product-detail" class="form-control" rows="4" cols="50"><?=$res[0]->product_detail;?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <input type="hidden" name="id" id="id" value="<?=$res[0]->product_id;?>"/>
                            <a href="<?php echo base_url('/admin/productSet');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไขสินค้า</button>
                        </div>
                    </div>
                </form>
                <div class="ln_solid"> </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-7">
        <div class="x_panel">
            <div class="x_title">
                <h2>สินค้าย่อย<small></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a id="add_subproduct" class=""><i class="fa fa-plus"  data-toggle="tooltip" title="เพิ่ม สินค้าย่อย"></i></a></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12" style="padding-right: inherit;padding-left: initial;">
                    <table id="product-table" class="table table-striped jambo_table bulk_action" >
                        <thead>
                            <tr class="">
                                <th class="column-title" style="width: 5%;">ลำดับ</th>
                                <th class="column-title" style="width: 13%;">รหัส</th>
                                <th class="column-title" style="width: 35%;">ชื่อสินค้า</th>
                                <th class="column-title" style="width: 9%;">จำนวน</th>
                                <th class="column-title" style="width: 12%;">ยี่ห้อสินค้า </th>
                                <th class="column-title" style="width: 18%;">ประเภทสินค้าย่อย</th>
                                <th class="column-title" style="width: 10%;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $productSubType =  "'".json_encode((object)$product_sub_type)."'";?>
                        <?php foreach($product_sub as $nume => $items):?>
                            <tr class="">
                                <td class="column-title"><?=$nume+1;?></td>
                                <td class="column-title"><?=$items->product_master_id?></td>
                                <td class="column-title"><?=$items->product_name?></th>
                                <td class="column-title"><?=$items->count?></th>
                                <td class="column-title"><?=$items->brand_name?></td>
                                <td class="column-title"><?=$items->product_type?></td>
                                <td class="column-title">
                                    <div class="field item form-group"style="margin-bottom: inherit;">
                                        <button type="button" 
                                            class="btn btn-round btn-warning" 
                                            onclick='editProductSet_sub(<?=json_encode($items);?>)'
                                            style=" font-size: 13px; padding: 0 5px; margin-bottom: inherit;"
                                            data-toggle="tooltip" title="แก้ไข"><i class="fa fa-wrench"></i></button>
                                        <button type="button" 
                                            class="btn btn-round btn-danger" 
                                            onclick="delProductSet_sub('<?=base_url();?>','<?=$items->id;?>','<?=$items->product_master_id;?>','<?=$res[0]->product_id;?>')" 
                                            style=" font-size: 13px; padding: 0 5px; margin-bottom: inherit;" 
                                            data-toggle="tooltip" title="ลบ"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปแบบการผ่อนชำระ<small></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                
                    <li><a id="add_installment_type" class=""><i class="fa fa-plus"  data-toggle="tooltip" title="เพิ่ม รูปแบบการผ่อนชำระ"></i></a></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <!--<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12" style="padding-right: inherit;padding-left: initial;">
                    <table id="product-table" class="table table-striped jambo_table bulk_action" >
                        <thead>
                            <tr class="">
                                <th class="column-title" style="width: 10%;">ลำดับ</th>
                                <th class="column-title" style="width: 40%">จำนวนงวด ที่ผ่อนชำระ</th>
                                <th class="column-title" style="width: 40%">จำนวนเงินผ่อน ต่องวด</th>
                                <th class="column-title" style="width: 10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($product_installment_type as $nume => $items):?>
                            <tr class="">
                                <td class="column-title" ><?=$nume+1;?></td>
                                <td class="column-title" ><?=$items->amount_installment?></td>
                                <td class="column-title"><?=$items->pay_per_month?></th>
                                <td class="column-title">
                                    <div class="field item form-group" style="margin-bottom: inherit;">
                                        <button type="button" 
                                            class="btn btn-round btn-warning" 
                                            style=" font-size: 13px; padding: 0 5px; margin-bottom: inherit;" 
                                            data-toggle="tooltip" title="แก้ไข"
                                            onclick="editProductSetInstallment('<?=base_url();?>','<?=$items->id;?>','<?=$items->amount_installment;?>','<?=$items->pay_per_month;?>')"
                                        ><i class="fa fa-wrench"></i></button>
                                        <button type="button" 
                                            class="btn btn-round btn-danger" 
                                            onclick="delProductSetInstallment('<?=base_url();?>','<?=$items->id;?>','<?=$items->amount_installment;?>','<?=$items->pay_per_month;?>','<?=$res[0]->product_id;?>')" 
                                            style=" font-size: 13px; padding: 0 5px; margin-bottom: inherit;" 
                                            data-toggle="tooltip" title="ลบ"
                                        ><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Modal Add Sub Product -->
    <div class="modal fade" id="AddSubProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form id="form-product-sub-add" action="<?php echo base_url('/admin/productSet/add_sub_product');?>" method="post"  enctype="multipart/form-data" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">เพิ่ม สินค้าย่อย</h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align">สินค้าย่อย</label>
                            <!--<select class="form-control" name="product_master_id"  id="product_master_id" tabindex="-1" aria-hidden="true" multiple="multiple">-->
                            <select name="product_master_id"  id="product_master_id" class="selectpicker form-control mh" data-live-search="true" >
                                <option selected='false'  value="" disabled>เลือกสินค้าย่อย</option>
                                <?php foreach ($product_master_all as $item) : ?>
                                    <option value="<?= $item->product_master_id;?>"><?= $item->product_master_name;?></option>
                                <?php endforeach ?>
                            </select>
                            <input type="hidden" name="product-master-json" id="product-master-json" data-attr='' value=''/>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align">ประเภทสินค้าย่อย</label>
                            <select class="form-control" name="product-sub-type"  id="product-sub-type" >
                                <?php foreach ($product_sub_type as $item) : ?>
                                    <option value="<?= $item->id;?>" <?=( $item->status_code != 0)? 'disabled': '';?>><?= $item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="col-form-label label-align">จำนวน</label>
                            <input class="form-control" type="number" value="1" min="1" id="productSub-count" name="productSub-count"/>
                        </div>
                        
                        <div class="col-md-6 col-sm-6">
                            <label class="col-form-label label-align">Serial number : </label>
                            <div class="checkbox" style="padding-top: 3%;">
                                <label class="">
                                    <div class="icheckbox_flat-green checked" style="position: relative;">
                                        <input id="add-active-serial" name="add-active-serial" type="checkbox" class="flat" checked="checked" style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                    </div> มี Serial number
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="product_set_id" id="product_set_id" value='<?=$res[0]->product_id;?>'/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-primary">เพิ่ม สินค้าย่อย</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal Edit Sub Product  -->
    <div class="modal fade" id="EditSubProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form id="form-productSet-sub-edit" action="<?php echo base_url('/admin/productSet/edit_sub_product');?>" method="post"  enctype="multipart/form-data" novalidate>
                <div class="modal-content">
                    <div class="modal-header label-align">
                        <h5 class="modal-title" id="exampleModalCenterTitle">แก้ไข สินค้าย่อย</h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 col-sm-12">
                                <label class="col-form-label label-align">สินค้าย่อย</label>
                                <input type="text" class="form-control" name="product_master_id_edit" id="product_master_id_edit" value="" disabled/>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <label class="col-form-label label-align">ประเภทสินค้าย่อย</label>
                                <select class="form-control" name="product-sub-type-edit"  id="product-sub-type-edit" readonly>
                                    <?php foreach ($product_sub_type as $item) : ?>
                                        <option value="<?=$item->id;?>"<?=( $item->status_code != 0)? 'disabled': '';?>><?= $item->label;?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="col-form-label label-align">จำนวน</label>
                                <input class="form-control" type="number" value="1" min="1" id="productSub-count-edit" name="productSub-count-edit" value=""/>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="col-form-label label-align">Serial number : </label>
                                <?//=print_r($activeSerial)?>
                                <select class="form-control" id="edit-active-serial" name="edit-active-serial">
                                    <?php foreach ($activeSerial as $items) : ?>
                                        <option value="<?=$items['val'];?>"><?= $items['name'];?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <input type="hidden" name="productSet-sub-edit-id" id="productSet-sub-edit-id" value="" />
                        <input type="hidden" name="product-id-edit" id="product-id-edit" value="<?=$res[0]->product_id;?>"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                        <button  type="submit" class="btn btn-success">แก้ไข สินค้าย่อย</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal Add Installment Type -->
    <div class="modal fade" id="AddInstallmentType" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form id="form-productSet-installment" action="<?php echo base_url('/admin/productSet/add_installment');?>" method="post"  enctype="multipart/form-data" novalidate>
                <div class="modal-content">
                    <div class="modal-header label-align">
                        <h5 class="modal-title" id="exampleModalCenterTitle">เพิ่ม รูปแบบการผ่อนชำระ</h5>
                    </div>
                    <div class="modal-body">
                        <!-- รูปแบบการผ่อนชำระ -->
                        <div class="field item form-group">
                            <div class="col-md-5 col-sm-5">
                                <label for="province-current">จำนวนงวด ที่ผ่อนชำระ</label>
                                <input type="number" step="1" min="1"  value="1" class="form-control" name="installment-amount1" id="installment-amount1" />
                            </div>
                            <div class="col-md-5 col-sm-5">
                                <label for="province-current"> จำนวนเงินผ่อน ต่องวด</label>
                                <input type="number" step="1" min="100"  value="100" class="form-control" name="installment-pay1" id="installment-pay1" />
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <label for="province-current"></label>
                                <div style="position: absolute;bottom: 1px;">
                                    <button type="button" id="installment-plus" class="btn btn-info" style="margin-bottom: inherit;"><i class="fa fa-plus-square"></i></button>
                                </div>
                            </div>
                            <input type="hidden" name="id" id="id" value="<?=$res[0]->product_id;?>"/>
                            <input type="hidden" name="installment-format-amount" id="installment-format-amount" value="1"/>
                        </div>
                        <div id="installment-format"></div>
                        <!-- รูปแบบการผ่อนชำระ -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                        <button  type="submit" class="btn btn-success">เพิ่ม รูปแบบการผ่อนชำระ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal Edit Installment Type -->
    <div class="modal fade" id="EditInstallmentType" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form id="form-productSet-installment-edit" action="<?php echo base_url('/admin/productSet/edit_installment');?>" method="post"  enctype="multipart/form-data" novalidate>
                <div class="modal-content">
                    <div class="modal-header label-align">
                        <h5 class="modal-title" id="exampleModalCenterTitle">แก้ไข รูปแบบการผ่อนชำระ</h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align">ชื่อสินค้า</label>
                            <input class="form-control" type="text" name="product-cate" id="product-cate" value='<?=$res[0]->product_name;?>' disabled/>
                        </div>
                        
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align">จำนวนงวด ที่ผ่อนชำระ</label>
                            <input class="form-control" type="number" name="amount_installment" id="amount_installment"/>
                        </div>
                        
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align">จำนวนเงินผ่อน ต่องวด</label>
                            <input class="form-control" type="number" name="pay_per_month" id="pay_per_month"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="productSet-installment-edit-id" id="productSet-installment-edit-id" value="" />
                        <input type="hidden" name="product-id-edit" id="product-id-edit" value="<?=$res[0]->product_id;?>"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                        <button  type="submit" class="btn btn-success">แก้ไข รูปแบบการผ่อนชำระ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    var base_url = $('input[name="base_url"]').val();

    /*------ product set ------*/
    var submitRequireEle = ["#product-name"];
    ProductSetCreateSubmit(submitRequireEle, "#form-productSet-edit");
    ProductSetEleChange(submitRequireEle);
    
    
    $('#installation-type').change(function(){
        var res = $(this).find(':selected').attr('data-code');
        if(res == 1){
            $("#installation-fee").attr('disabled','disabled');
            $("#installation-fee").val(0);
        }else{
            $("#installation-fee").removeAttr('disabled');
            $("#installation-fee").val(0);
        }
    });


    /*------ sub product set ------*/
    $('#add_subproduct').click(function(){
        $('#AddSubProductModal').modal();
    });

    //ProductSetSubCreateSubmit(submitRequireEle, "#form-productSet-sub-edit");
    $('select#product_master_id').change(function(){
        var myJsonString = JSON.stringify(Object.assign({}, $(this).val()));
        $('#product-master-json').val(myJsonString);
    });
    /*$('select#product_master_id').multiselect({
        nonSelectedText: '== เลือกสินค้า ==',
        filterPlaceholder: 'ค้นหา',
        enableFiltering: true,
        includeSelectAllOption: true,
        selectAllText: '== เลือกทั้งหมด ==',
        maxHeight: 260,
        buttonWidth: '100%',
        buttonTextAlignment: 'left',
        dropUp: true,
        buttonText: function(options, select) {
        if (options.length == 0) {
            return '== เลือกสินค้า ==';
        }
        else {
            if (options.length > this.numberDisplayed) {
            return options.length + ' ' + this.nSelectedText;
            }
            else {
            var selected = '';
            options.each(function() {
                var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
        
                selected += label + ', ';
            });
            return selected.substr(0, selected.length - 2);
            }
        }
        },
    });*/

    $('select#contract-type').change(function(){
        $("select#contract-doc").html('');
        $.ajax({
            url:  base_url+"admin/productSet/getResContractDoc", //ทำงานกับไฟล์นี้
            data:  {'type':$(this).val()},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                if(data.length > 0){
                    $("select#contract-doc").removeAttr('disabled');
                    var select;
                    $.each(data,function(index,json){
                        select += '<option value="'+json.contract_id+'">'+json.contract_name+'</option>';
                    });
                    $("select#contract-doc").html(select);
                }
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
    });



    /*------ installment type product set ------*/
    ProductSetInstallmentPlus('#installment-plus', '#installment-format',"#installment-format-amount");
    ProductSetInstallmentEleChange("#installment-format-amount");
    $('#add_installment_type').click(function(){
        $('#installment-amount1').val(1);
        $('#installment-pay1').val(100);
        $('#installment-format').html('');
        $('#AddInstallmentType').modal();
    });
    ProductSetInstallmentSubmit("#installment-format-amount", "#form-productSet-installment");

    var ProductSetInstallmentEle = ["#amount_installment","#pay_per_month"];
    editProductSetInstallmentSubmit(ProductSetInstallmentEle, "#form-productSet-installment-edit");
    
</script>


