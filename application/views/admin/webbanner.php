<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }

    /* Sortable */
    
    .dragged {
      cursor: pointer !important;
      position: absolute;
      opacity: 0.5;
      z-index: 2000;
    }

</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Banner<small>หน้าหลัก (สามารถลากวาง เพื่อเรียงตำแหน่งได้)</small></h2>
        <input name="base_url" value="<?=base_url();?>" type="hidden" >
        <ul class="nav navbar-right panel_toolbox">
          <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
            <li>
                <a href="<?=base_url('admin/WebBanner/banner_manage/');?>">
                    <button id="add-btn" type="button" class="btn" style="color: #466889;"><i class="fa fa-plus"></i> Banner</button>
                </a>
            </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <!-- <p class="text-muted font-13 m-b-30">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code></p> -->
                <table id="products-table" class="table table-striped jambo_table bulk_action sorted_table" style="width:100%; border-spacing: 1px !important;">
                  <thead>
                    <tr class="headings">
                      <th class="column-title" >ลำดับ</th>
                      <th class="column-title" ></th>
                      <th class="column-title" style="width: 4rem;">Banner</th>
                      <th class="column-title">link</th>
                      <th class="column-title">detail</th>
                      <th class="column-title" style="width: 7rem;">สถานะ</th> 
                      <th class="column-title no-link last" style="text-align: center; width: 5rem;"><span class="nobr"></span></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    var base_url = $('input[name="base_url"]').val();
    DrawTable(base_url, '#products-table');
    function DrawTable(base_url, replacePosition){
        var res = get_datas(base_url);
        if(res){ 
            var elements = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                var no = i+1;
                var img = base_url+'/uploaded/DocumentTh.png';
                if(val.img != ''){
                  img = base_url+val.img;
                }
                elements += '<tr  banner-id="'+val.id+'">';
                elements += '   <td class=""style="width: 1rem; !important">'+no+'</td>';
                elements += '   <td class=""style="width: 5rem; !important"><img class="thumnails-premise" src="'+img +'" alt="image" style="border: unset; max-width:100%; border-radius: unset;display: inline-block;" /></td>';
                elements += '   <td class="">'+val.name+'</td>';
                var title = (val.title != null)? val.title : '';
                elements += '   <td class="">'+val.link+'</td>';
                elements += '   <td class="">'+val.detail+'</td>';

                var status_elm = '<div style="font-size: 13px; margin-bottom: inherit; background-color: #ffc107; border-radius: 30px;">ไม่ แสดงบนหน้าเว็บ</div>';
                if(val.display_status == 1){ 
                  status_elm ='<div style="font-size: 13px; margin-bottom: inherit; background-color: #1abb9c; color: white; border-radius: 30px; ">แสดงบนหน้าเว็บ</div>';
                }
                elements += '   <td class="" style="text-align: center;">'+status_elm+'</td>';

                elements += '   <td class="" style="text-align: center;">';
                elements += '       <a href="'+base_url+'admin/WebBanner/banner_manage/'+val.id+'">';
                elements += '           <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                elements += '           <i class="fa fa-wrench"></i> แก้ไข</button>';
                elements += '       </a>';
                elements += '   </td>';
                elements += '</tr>';
            });
            $(body).append(elements);
            
            // Datatable
            $(replacePosition).dataTable({
              destroy: true,
              lengthMenu: [
                  [50, 100],
                  [50, 100],
              ],
              "aoColumnDefs": [
                  // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                  //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
              ]
            });
        }
    }
    function get_datas(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/webBanner/banner_get", //ทำงานกับไฟล์นี้
            //data:  {'Search':search},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }

    //#### sort table rows ###//
    $(".sorted_table").sortable({
      containerSelector: 'table',
      itemPath: '> tbody',
      itemSelector: 'tr',
      placeholder: '<tr class="placeholder"/>',
      cursor: "move",
      onDrop: function  ($item, container, _super) {
        $item.closest('table').find('tbody tr').each(function (i, row) {
          var banner_id = $(row).attr("banner-id");
          update_table_sort(base_url, banner_id, i);
          
        });
        var $clonedItem = $('<tr/>').css({height: 0});
        $item.before($clonedItem);
        $clonedItem.animate({'height': $item.height()});

        $item.animate($clonedItem.position(), function  () {
          $clonedItem.detach();
          _super($item, container);
        });

        $('#products-table').DataTable().destroy();
        DrawTable(base_url, '#products-table');
      }
    });
    
    function update_table_sort(base_url, id, sortable){
        var res = null;
        $.ajax({
            url: base_url+'admin/WebBanner/update_sorting', //ทำงานกับไฟล์นี้
            data: {
                'id': id,
                'sortable':sortable,
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {},
            error: function(xhr, status, exception) {}
        });
       // return res;
    }
  </script>



<!---- End Content ------>
  