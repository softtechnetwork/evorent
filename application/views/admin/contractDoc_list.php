<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>เอกสาร สัญญา<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <!--
                <li style="margin-right: 3px;">
                    <select class="form-control" id="product_brand" required="required" >
                        <option value="">ยี่ห้อสินค้า</option>
                        <?php foreach ($productBrand as $item) : ?>
                            <option value="<?= $item->brand_id;?>"><?= $item->brand_name;?></option>
                        <?php endforeach ?>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <select class="form-control" id="product_cate" required="required" >
                        <option value="">ประเภทสินค้า</option>
                        <?php foreach ($productCate as $item) : ?>
                            <option value="<?= $item->cate_id;?>"><?= $item->cate_name;?></option>
                        <?php endforeach ?>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <input id="search_text" type="text"  class="form-control"/>
                </li>
                <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                    <i class="fa fa-search"></i> ค้นหา</button>
                </li>
                <li>
                    <button id="button-reset" type='button' class="btn btn-warning" style=" margin-right: 15px;border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li>
                -->
                <!--<li>
                    <a class="btn btn-success" href="<?php echo base_url('admin/contractDoc/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เอกสาร สัญญา
                    </a>
                </li>
                -->
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="product-table" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 3rem">ลำดับ</th>
                            <th class="column-title" style="width: 10rem">ชื่อ เอกสารสัญญา</th>
                            <th class="column-title" style="width: 10rem">ประเภท เอกสารสัญญา</th>
                            <th class="column-title">รายละเอียด</th>
                            <th class="column-title" style="width: 10rem">วันที่สร้าง เอกสาร</th>
                            <th class="column-title" style="width: 8rem; text-align: center;">สถานะการใช้งาน</th>
                            <th class="column-title no-link last" style="text-align: center;width:7rem;"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; foreach ($resContractDoc as $item) : ?> 
                            <tr>
                                <td><?=$no;?></td>
                                <td><?=$item->contract_name;?></td>
                                <td><?=$item->label;?></td>
                                <td><?=$item->contract_detail;?></td>
                                <td><?=$item->cdate;?></td>
                                <td style="text-align: center; background-color: <?=$item->useStatusBackground;?>; color: <?=$item->useStatusColor;?>;"><?=$item->useStatustitle;?></td>
                                <td class="" style="text-align: center;">
                                    <a href="<?=base_url('admin/contractDoc/edit/'.$item->contract_id)?>">
                                        <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">
                                            <i class="fa fa-wrench"></i> แก้ไข</button>
                                    </a>
                                    <!--
                                    <a href="'+base_url+'admin/productCategory/delete/'+val['id']+' ">
                                        <button type="button" class="btn btn-round btn-danger" onclick="delProduct('+agrmt+')" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">
                                            <i class="fa fa-times"></i> ลบ</button>
                                    </a>
                                    -->
                                </td>
                            </tr>
                        <?php $no++; endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<!---- End Content ------>
  