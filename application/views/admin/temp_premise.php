
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>หลักฐานสัญญา <small>รหัส: <?=$res[0]->temp_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอกสาร/สัญญา<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-contract"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>บัตรประชาชน ผู้ค้ำประกัน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-cardSupporter"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>ทะเบียนบ้าน ผู้ค้ำประกัน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-houseRegisSupporter"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปถ่ายที่พัก<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-residence"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รูปถ่าย Statment<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-statment"></div>
            </div>
        </div>
    </div>
</div>

<!-- Premise Model -->
<div id="modelPlace"></div>

<script>
    var base_url = "<?php echo base_url(); ?>";
    var refCode  = "<?php echo $res[0]->temp_code; ?>";
    var mode = "temp";

    var url_getRes = base_url+"admin/premise/getpremise"; // get premise
    var insertController = base_url+"admin/premise/insertpremise"; // insert premise

    getpremise(url_getRes, refCode, "#position-contract", insertController, mode); // หลักฐาน เอกสาร/สัญญา
    getpremise(url_getRes, refCode, "#position-cardSupporter", insertController, mode);// หลักฐาน บัตรประชาชน ผู้ค้ำประกัน
    getpremise(url_getRes, refCode, "#position-houseRegisSupporter", insertController, mode);// หลักฐาน ทะเบียนบ้าน ผู้ค้ำประกัน
    
    getpremise(url_getRes, refCode, "#position-residence", insertController, mode);// หลักฐาน รูปถ่ายที่พัก
    getpremise(url_getRes, refCode, "#position-statment", insertController, mode);// หลักฐาน รูปถ่าย Statment
    
    AddPremise('.premise-add');
    viewPremise('.premise-view','#modelPlace');
    
    var delController = base_url+"admin/premise/delpremise"; // del premise
    delPremise('.premise-del', delController);
</script>