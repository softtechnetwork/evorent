<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างข้อมูลลูกค้า <small></small></h2>
                <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>-->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="<?php echo site_url('/admin/customer/insert');?>" method="post" enctype="multipart/form-data" novalidate>-->
                <form id="customer-create-form" class="" action="<?php echo site_url('/admin/customer/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขบัตรประชาชน</label>
                        <div class="col-md-6 col-sm-6">
                          <!--<input name="idcard" id="idcard" type="text" class="form-control" data-inputmask="'mask' : '9-9999-99999-99-9'" required="required">-->
                          <input name="idcard" id="idcard" type="tel"  maxlength="13"  class="form-control" required="required">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="idcard-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="name" name="name" value="" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นามสกุล</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" id="sname" name="sname" required="required" />
                        </div>
                    </div>
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'></textarea>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จังหวัด</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อำเภอ / เขต</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
								<option value="">อำเภอ / เขต</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ตำบล / แขวง</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="district"  id="district" required="required" >
								<option value="">ตำบล / แขวง</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสไปรษณีย์</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" readonly>
                            </div>
                    </div>
                    -->

                    <div class="ln_solid"> </div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ตามบัตรประาชน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province">จังหวัด</label>
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
								<option value="">อำเภอ / เขต</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district">ตำบล / แขวง</label>
                            <select class="form-control" name="district"  id="district" required="required" >
								<option value="">ตำบล / แขวง</option>
							</select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" readonly>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class=" col-md-6 col-sm-6 checkbox">
                            <label><input class="inhabitedInput" type="checkbox" id='addOfId-check' value=""> ใช้ที่อยู่ตามบัตรประาชน</label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ปัจจุบัน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-current' name='address-current'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-current">จังหวัด</label>
                            <select name='province-current' id='province-current' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-current">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs-current"  id="amphurs-current" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-current">ตำบล / แขวง</label>
                            <select class="form-control" name="district-current"  id="district-current" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-current">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-current" id="zipcode-current" readonly>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>





                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> ชื่อบริษัท</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="text" class="form-control" name="name-office" id="name-office" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ข้อมูลสถานที่ทำงาน</label>
                        
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-office' name='address-office'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-office">จังหวัด</label>
                            <select name='province-office' id='province-office' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-office">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs-office"  id="amphurs-office" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-office">ตำบล / แขวง</label>
                            <select class="form-control" name="district-office"  id="district-office" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-office">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-office" id="zipcode-office" readonly>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel-office" name="tel-office" type="tel" maxlength="10" class="form-control" >
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>






                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายได้ต่อเดือน</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="number" min="1" class="form-control" name="monthly-income" id="monthly-income" />

                            <!--<select class="form-control" name="monthly-income"  id="monthly-income" required="required" >
                                <option value="" selected='false' disabled>กรุณาระบุรายได้ของคุณ</option>
                                <?php foreach ($monthlyIncome as $id => $item) : ?>
                                    <option value="<?= $id;?>"><?= $item;?></option>
                                <?php endforeach ?>
                            </select>-->
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อาชีพ</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="career-select"  id="career-select" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกอาชีพของคุณ</option>
                                <?php foreach ($career as $id => $item) : ?>
                                    <option value="<?= $id;?>" datas="<?= $item;?>"><?= $item;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input  id="career" class="form-control" type="text" name="career" readonly />
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันเกิด</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control datepicker" type="text" id="bdate" name="bdate"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขมือถือ</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel" name="tel" value=""  type="tel" maxlength="10" class="form-control" required="required" >
                            <!--<input name="tel" value=""  type="text" class="form-control" data-inputmask="'mask' : '999-999-9999'" required>-->
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="tel-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อีเมล์<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" id="email" name="email" class='email' required="required" type="email"  required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เพศ *</label>
                        <div class="col-md-6 col-sm-6">
                            <p style="padding-top: 7px;">
                                <label style=" margin-right: 3px;">ชาย:</label>
                                <input type="radio" class="flat" name="gender" id="genderM" value="M" checked /> 
                                <label style=" margin-left: 10px; margin-right: 3px;">หญิง:</label>
                                <input type="radio" class="flat" name="gender" id="genderF" value="F" />
                            </p>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> ประเภทลูกค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='customerType' id='customerType' class="form-control">
                                <?php foreach ($customerType as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> สถานะ</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> หมายเหตุ</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='remark' name='remark'></textarea>
                        </div>
                    </div>

                    <div class="ln_solid"> </div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/customer');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    
$(".datepicker").datepicker().on('show', function(e){
    $('.prev').text('<');
        $('.next').text(">");
});
var base_url = $('input[name="base_url"]').val();

var currentAddId = ["#province-current","#amphurs-current","#district-current","#zipcode-current","#address-current"]; // ห้าม เปลี่ยนตำแหน่ง
var OfficeAddId = ["#province-office","#amphurs-office","#district-office","#zipcode-office","#address-office"]; // ห้าม เปลี่ยนตำแหน่ง
var AddById = ["#province","#amphurs","#district","#zipcode","#address"];// ห้าม เปลี่ยนตำแหน่ง

var input_id = ["#idcard","#name","#sname","#address","#province","#amphurs","#district","#address-current","#province-current","#amphurs-current","#district-current","#tel","#monthly-income","#career-select","#career", "#bdate"];
var office_id = ["#name-office","#address-office","#province-office","#amphurs-office","#district-office","#tel-office"];
var thumnails_id = '#thumnails-contract1, #thumnails-contract2, #thumnails-contract3, #thumnails-contract4, #thumnails-idcardPremise, #thumnails-houseregisPremise, #thumnails-idcardSupporter, #thumnails-houseregisSupporter' ;
var normal_id = ["#idcard","#name","#sname","#tel","#email","#address","#address-current","#career","#monthly-income" ,"#name-office","#address-office", "#bdate","#tel-office"];

rePlaceAdd('#addOfId-check', base_url, AddById, currentAddId); //replace ที่อยู่ปัจจุบัน by ทีอยู่ตามบัตรประาชน
rePlaceAddEle(AddById, base_url);//replace ที่อยู่ตามบัตรประชาชน
rePlaceAddEle(currentAddId, base_url);//replace ที่อยู่ปัจจุบัน
rePlaceAddEle(OfficeAddId, base_url);//replace ข้อมูลสถานที่ทำงาน


CustomerCreatSubmit(input_id, office_id, '#customer-create-form'); // validate and submit
normalValid(normal_id, base_url);// normal Validate

career('#career-select','#career');

</script>