<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>SMS Template<small>ปี <?=date('Y')+543; ?></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" > 
            <ul class="nav navbar-right panel_toolbox">
                <!-- <li><button class="btn" style="color: #466889;" data-toggle="modal" data-target="#actionModal"><i class="fa fa-plus"></i> เพิ่ม Template</button></li> -->
                <li><button class="btn add-btn" style="color: #466889;" ><i class="fa fa-plus"></i> เพิ่ม Template</button></li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">ลำดับ</th>
                            <th class="column-title">ชื่อ</th>
                            <th class="column-title">ข้อความ </th>
                            <th class="column-title">สถานะ</th>
                            <th class="column-title">วันที่ส่ง </th>
                            <th class="column-title"> </th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Action Modal -->
<div class="clearfix"></div>
<div class="modal fade" id="actionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <form method="post" action="" enctype="multipart/form-data" id="template-form">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Template </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="method" id="method" value="insert" >
                    <input type="hidden" name="id" id="id" value="" >
                    <div class="col-md-12 form-group">
                        <label for="" style="margin-bottom: .1rem;">ชื่อ template</label>
                        <input type="text" class="form-control" name="name" id="name" value="" require>
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="" style="margin-bottom: .1rem;">Message</label>
                        <textarea id="message" name="message"  class="form-control" rows="4" cols="50"></textarea>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="" style="margin-bottom: .1rem;">สถานะ</label>
                        <!-- <select id="status" name="status" class="selectpicker form-control mh" data-live-search="true"> -->
                        <select id="status" name="status" class="form-control mh">
                            <option value="1" >เปิดใช้งาน</option>
                            <option value="0" >ปิดใช้งาน</option>
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="submit" class="btn btn-primary" id="save-btn" >บันทึก</button>
            </div>
        </form>
    </div>
  </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        var datatotable = [];
        $.each( results.datas, function( key, val ) {
            const arrs = {
                "no": val.RowNum,
                "name" : val.name,
                "message": val.message,
                "status":  (val.status == 1)? 'เปิดใช้งาน': 'ปิดใช้งาน',
                "cdate": val.cdate,
                "action": '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"  onclick="custom(\''+val.id+'\', \''+val.name+'\', \''+val.message+'\', \''+val.status+'\')" ><i class="fa fa-wrench"></i></button>'
            }
            datatotable.push(arrs);
        });


        $('#table').dataTable({
            "destroy" : true
            ,paging: true
            // ,dom: "<'row'<'col-sm-5'B><'col-sm-3'l><'col-sm-4'f>> <'row'<'col-sm-12't>>  <'row'<'col-sm-6'i><'col-sm-6'p>>"
            // ,buttons: [
            //     {
            //         extend: 'excel',
            //         title: 'กำไรขั้นต้น',
            //         text: '<i class="fa fa-file-excel-o"> Excel',
            //         className: 'exportButton'
            //     },
            //     // {
            //     //     extend: 'pdf',
            //     //     title: 'กำไรขั้นต้น',
            //     //     text: 'PDF',
            //     //     className: 'exportButton'
            //     // }
            // ]
            ,lengthMenu: [
                [50, 100, 150], [50, 100, 150]
            ]
            ,"searching": true
            ,data: datatotable
            ,columns: [
                {   render: function (data, type, row, meta) { return row.no; } },
                {   render: function (data, type, row, meta) { return row.name;} },
                {   render: function (data, type, row, meta) { return row.message; } },
                {   render: function (data, type, row, meta) { return row.status; } },
                {   render: function (data, type, row, meta) { return row.cdate; } },
                {   render: function (data, type, row, meta) { return row.action; } },
            ]
            ,"language": {  "emptyTable": "ไม่มีข้อมูล" }
            ,columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    // "className": "text-center",
                    "width": "20%"
                },
                {
                    "targets": 2, // your case first column
                    // "className": "text-center",
                    "width": "55%"
                },
                {
                    "targets": 3, // your case first column
                    "className": "text-center",
                    "width": "5%"
                },
                {
                    "targets": 4, // your case first column
                    "className": "text-center",
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "className": "text-center",
                    "width": "5%"
                }
            ]

        });


    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/Sms/get_template_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }

    $('.add-btn').click(function () {
        $('#actionModal #method').val('insert');
        $('#actionModal #id').val('');
        $('#actionModal #name').val('');
        $('#actionModal #message').val('');
        $('#actionModal').modal('show');
    });

    function custom(id, name, message, status){
        event.preventDefault();
        $('#actionModal #method').val('update');
        $('#actionModal #id').val(id);
        $('#actionModal #name').val(name);
        $('#actionModal #message').val(message);
        $('#actionModal #status').val(status);

        $('#actionModal').modal('show');
    }
    $("form#template-form").on("submit",function(e){ // จะทำงานก็ต่อเมื่อกด submit ฟอร์ม
        e.preventDefault(); // ปิดการใช้งาน submit ปกติ เพื่อใช้งานผ่าน ajax
        var fd = new FormData(this); // เตรียมข้อมูล form สำหรับส่งด้วย  FormData Object
        // fd.append("tmcode",$(".action-area #tmcode").val());
        $.ajax({
            url:base_url+'admin/Sms/sms_template_action', //ให้ระบุชื่อไฟล์ PHP ที่เราจะส่งค่าไป
            type:'post',
            dataType: 'json',
            data:fd, //ข้อมูลจาก input ที่ส่งเข้าไปที่ PHP
            contentType: false,
            processData: false,
            success:function(response){ //หากทำงานสำเร็จ จะรับค่ามาจาก JSON หลังจากนั้นก็ให้ทำงานตามที่เรากำหนดได้
                if(response.status){
                    // window.open(response.printSlip, '_blank');
                    // window.location.href = response.url;
                    drawtable(base_url);
                    $('#actionModal').modal('hide');
                    Swal.fire({ icon: 'success',  text: response.massege });
                }else{
                    Swal.fire({ icon: 'warning',  text: response.massege });
                }
            }
        });
        
    });
</script>


<!---- End Content ------>
  