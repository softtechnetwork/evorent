<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลสัญญาลูกค้า <small></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="btn" href="<?=base_url('admin/Contract/created');?>"  style="color: #466889;"><i class="fa fa-plus"></i> เพิ่มข้อมูลลูกค้า</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                    <tr class="headings">
                        <th class="column-title" style="width: 1%;">ลำดับ</th>
                        <th class="column-title" style="width: 8%;">รหัสสัญญา</th>
                        <th class="column-title" style="width: 11%;">ชื่อลูกค้า</th>
                        <th class="column-title" style="width: 10%;">เบอร์โทรศัพท์</th>
                        <th class="column-title" style="width: 15%;">สินค้า</th>
                        
                        <th class="column-title" style="width: 7%;">ประเภท</th>
                        <!--<th class="column-title">หมายเลขทรัพย์สิน </th>-->
                        <th class="column-title" style="text-align: center;width: 13%;">สถานะ</th>
                        <th class="column-title no-link last"  style="text-align: center;width: 5%;"> <span class="nobr">กิจกรรม</span> </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Modal Add Sub Product -->
    <div class="modal fade" id="contractMode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">เลือกเอกสาร</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--<div class="modal-body"></div>-->
                <div class="modal-footer" style="justify-content: center !important;">
                    <a id="coppy-pdf" class="btn btn-info text-center" href="" target="_blank" style="">คู่ฉบับ</a>
                    <a id="origin-pdf" class="btn btn-primary text-center" href="" target="_blank" style="">ต้นฉบับ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!---- End Content ------>
<script>

    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                var no = key+1;
                tr += '<tr class="even pointer">';
                tr += '<td class="a-center " style="vertical-align: inherit;">'+no+'</td>';
                tr += '<td style="vertical-align: inherit;">'+val['contract_code']+'</td>';
                tr += '<td style="vertical-align: inherit;">'+val['firstname']+' '+val['lastname']+'</td>';
                tr += '<td style="vertical-align: inherit;">'+val['tel']+'</td>';
                tr += '<td style="vertical-align: inherit;">'+val['product_name']+'</td>';

                var contractType_name = '';
                if(val['contractType_name'] != null && val['contractType_name'] !=''){ contractType_name = val['contractType_name'];}
                tr += '<td style="center;vertical-align: inherit;">'+contractType_name+'</td>';

                //tr += '<td>'+val['product_number']+'</td>';
                tr += '<td class=""  style="text-align: center;vertical-align: inherit;">';
                tr +=     '<button type="button" class="btn btn-round" style="background-color: '+val['background_color']+'; color: '+val['color']+'; font-size: 12px; padding: 4px 10px; margin-bottom: inherit;margin-right: inherit;"> '+val['label']+'</button>';
                tr += '</td>';
                tr += '<td class="" style="">';

                tr += '<ul class="" style="list-style: none; display: inline-flex; margin-bottom: 0rem; min-width: auto; padding-inline: 0px;">';
                tr += '<li>';
                tr +=   '<a class="" href="'+base_url+'admin/contract/edit/'+val['contract_code']+' " style="min-width: max-content;padding: 2px;" data-toggle="tooltip" title="แก้ไข">';
                tr +=       '<button type="button" class="btn btn-round btn-warning" style="margin-right: 0px; font-size: 11px; padding: 4px 8px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                tr +=   '</a>';
                tr += '</li>';

                tr += '<li>';
                tr +=   '<a class="" href="'+base_url+'admin/Installments/'+val['contract_code']+' " style="min-width: max-content;padding: 2px;" data-toggle="tooltip" title="การผ่อนชำระ">';
                tr +=       '<button type="button" class="btn btn-round btn-danger" style="margin-right: 0px; font-size: 11px; padding: 4px 8px; margin-bottom: inherit;"><i class="fa fa-list"></i></button>';
                tr +=   '</a>';
                tr += '</li>';

                tr += '<li>';
                tr +=   '<a class="" href="'+base_url+'admin/contractaddon/'+val['contract_code']+' " style="min-width: max-content;padding: 2px;" data-toggle="tooltip" title="ส่วนเสริมสัญญา">';
                tr +=       '<button type="button" class="btn btn-round btn-info" style="margin-right: 0px; font-size: 11px; padding: 4px 8px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i></button>';
                tr +=   '</a>';
                tr += '</li>';

                tr += '<li>';
                tr +=   '<a class="" href="'+base_url+'admin/contract/premise/'+val['contract_code']+' " style="min-width: max-content;padding: 2px;" data-toggle="tooltip" title="หลักฐาน">';
                tr +=       '<button type="button" class="btn btn-round btn-success" style="margin-right: 0px; font-size: 11px; padding: 4px 8px; margin-bottom: inherit;"><i class="fa fa-file-image-o"></i></button>';
                tr +=   '</a>';
                tr += '</li>';

                // tr += '<li>';
                // tr +=   '<a class="" href="'+base_url+'admin/contract/edit/'+val['contract_code']+' " style="min-width: max-content;padding: 2px;" data-toggle="tooltip" title="แก้ไข">';
                // tr +=       '<button type="button" class="btn btn-round btn-warning" style="margin-right: 0px; font-size: 11px; padding: 4px 8px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                // tr +=   '</a>';
                // tr += '</li>';

                tr += '<li>';
                var jsonContractPDF = "{'url':'"+base_url+"admin/contract/contractPDF/', 'id':'"+val['contract_code']+"'}";
                tr +=   '<a class="" href="#" style="min-width: max-content;padding: 2px; margin: 0;" data-toggle="tooltip" title="หลักฐาน">';
                tr +=     '<button type="button" onclick="contractPDF_fn('+jsonContractPDF+')" class="btn btn-round btn-dark" style="font-size: 11px; padding: 4px 8px;"><i class="fa fa-print"></i></button>';
                tr +=   '</a>';
                tr += '</li>';

                tr += '</ul>';

                tr += '</td>';
                tr += '</tr>';
            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [50, 100], [50, 100],
                ],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [7] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/Contract/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }
    function contractPDF_fn(param){
        $('#contractMode .modal-footer #coppy-pdf').attr('href',param.url+'coppy/'+param.id);
        $('#contractMode .modal-footer #origin-pdf').attr('href',param.url+'origin/'+param.id);
        $('#contractMode').modal();
    }
</script>
    