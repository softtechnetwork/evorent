<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างหมายเลขเครื่อง</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-serial-create" action="<?php echo site_url('/admin/serialNumber/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสัญญา</label>
                        <div class="col-md-8 col-sm-8 ">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <select name="contract-id"  id="contract-id"  class="selectpicker form-control mh " data-live-search="true">
                                        <option value="" selected='false' disabled>กรุณาเลือกรหัสสัญญา</option>
                                        <?php foreach ($TempTocombo as $item) : ?>
                                            <option value="<?= $item->temp_code;?>" res='<?php echo json_encode($item);?>'><?= $item->temp_code;?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--################## Srial element ######################-->
                    <div class="field item form-group" id="productPlace">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row" id="productElePositions"></div>
                        </div>
                    </div>
                    <div class="field item form-group" id="serialPlace">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row" id="serialElePotions"></div>
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/serialNumber');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">สร้างหมายเลขเครื่อง</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    $("#productPlace").hide();
    $("#serialPlace").hide();
    TempChange(base_url, "#contract-id");
    
    var elementID = ["#contract-id"];
    serialCreateSubmit(elementID, "#form-serial-create");
    
</script>


