<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
.multiselect-native-select .dropdown-menu {
     width: 99%; 
    box-shadow: 0px 1px 6px rgb(152 147 147 / 57%) !important;
    margin: 7px 2px;
    background: #fff !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างสินค้า(ชุด)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSet-create" action="<?php echo site_url('/admin/productSet/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-3 col-sm-3  ">
                            <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                            <select class="form-control" name="product-cate"  id="product-cate" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกหมวดหมู่สินค้า</option>
                                <?php foreach ($productCate as $item) : ?>
                                    <!--<option 
                                        <?php if($item->coundOfProductMaster == null){ ?> 
                                            value="" disabled
                                        <?php }else{ ?>
                                            value="<?= $item->cate_id;?>" 
                                        <?php } ?> >
                                            <?= $item->cate_name;?>
                                    </option>-->
                                    <option productC="<?=$item->coundOfProductMaster;?>"  value="<?= $item->cate_id;?>"><?= $item->cate_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3  ">
                            <label class="col-form-label label-align">ยี่ห้อสินค้า</label>
                            <select class="form-control" name="product-brand"  id="product-brand" required="required" disabled>
                                <option value="" selected='false' disabled>กรุณาเลือกยี่ห้อสินค้า</option>
                            </select>
                        </div>
                    </div>
                    <!-- ซ่อนไว้ก่อน -->
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-6 col-sm-6  ">
                            <label class="col-form-label label-align">เลือกสินค้า</label>
                            <div id="product-master-place">
                                <select class="form-control" name="product-master"  id="product-master"  required="required" disabled>
                                    <option value="">== เลือกสินค้า ==</option>
                                </select>
                            </div>
                            <input type="hidden" name="product-master-json" id="product-master-json" data-attr='' value=''/>
                        </div>
                    </div>
                    -->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รุ่น</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="product-version" id="product-version"/>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>



                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="product-name" id="product-name" required="required"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคา</label>
                        <div class="col-md-3 col-sm-3">
                            <input  type="number" min="0" class="form-control" name="product-pricce" id="product-pricce" />
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท ราคาค่าติดตั้ง</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="installation-type"  id="installation-type">
                                <?php foreach ($installationType as $item) : ?>
                                    <option data-code="<?=$item->status_code;?>" value="<?=$item->id;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input type="text" class="form-control" name="installation-fee" id="installation-fee" value="0"/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท ลูกค้า</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="customer-type"  id="customer-type">
                                <?php foreach ($productType as $item) : ?>
                                    <option value="<?=$item->id;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท สัญญา(เอกสารสัญญา)</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="contract-type"  id="contract-type">
                                <option value="" selected='false' disabled>กรุณาเลือกประเภท สัญญา(เอกสารสัญญา)</option>
                                <?php foreach ($contractType as $item) : ?>
                                    <option value="<?=$item->id;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3  ">
                            <select class="form-control" name="contract-doc"  id="contract-doc"  required="required" disabled>
                                <option value="" selected='false' disabled>กรุณาเลือก สัญญา(เอกสารสัญญา)</option>
                            </select>
                        </div>
                    </div>


                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อรุ่นสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-version" id="product-version" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้าอ้างอิง</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="product-ref" id="product-ref" />
                        </div>
                    </div>
                    -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="product-detail" name="product-detail" class="form-control" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <!-- รูปแบบการผ่อนชำระ -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current">จำนวนงวด/เดือน ที่ผ่อนชำระ</label>
                            <input type="number" step="1" min="1"  value="1" class="form-control" name="installment-amount1" id="installment-amount1" />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current"> จำนวนเงินที่ผ่อนชำระต่อ งวด/เดือน </label>
                            <input type="number" step="1" min="100"  value="100" class="form-control" name="installment-pay1" id="installment-pay1" />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="province-current"></label>
                            <div style="position: absolute;bottom: 1px;">
                                <button type="button" id="installment-plus" class="btn btn-info" style="margin-bottom: inherit;"><i class="fa fa-plus-square"></i></button>
                            </div>
                        </div>
                        <input type="hidden" name="installment-format-amount" id="installment-format-amount" value="1"/>

                    </div>
                    <div id="installment-format"></div>
                    <div class="ln_solid"> </div>
                    <!-- รูปแบบการผ่อนชำระ -->
                    
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/productSet');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">สร้างสินค้า</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();

    ProductSetInstallmentPlusAdd('#installment-plus', '#installment-format',"#installment-format-amount");
    ProductSetInstallmentEleChangeAdd("#installment-format-amount"); 
    
    var submitRequireEle = ["#product-cate","#product-brand","#product-name","#contract-doc","#installment-format-amount"];
    ProductSetCreateSubmit(submitRequireEle, "#form-productSet-create");
    ProductSetEleChange(submitRequireEle); 
    

    

    $('select#product-cate').change(function(){

        var productC = $('option:selected', this).attr('productC');
        if(productC != '' && productC != null ){
            /*--- Replace Product Brand --*/
            $.ajax({
                url:  base_url+"admin/productSet/getResProductBrand", //ทำงานกับไฟล์นี้
                data:  {'cate':$(this).val()},  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                    $("select#product-brand").removeAttr('disabled');
                    $("select#product-brand").html('');
                    if(data.length > 0){
                        var select = '<option value="" selected="false" disabled>กรุณาเลือกยี่ห้อสินค้า</option>';
                        $.each(data,function(index,json){
                            var attr = '';
                            /*if(json.coundOfProductMaster == null){
                                attr = 'value="" disabled';
                            }else{
                                attr = ' value="'+json.brand_id+'"';
                            }*/
                            attr = 'productC="'+json.coundOfProductMaster+'"  value="'+json.brand_id+'" ';
                            select += '<option '+attr+'>'+json.brand_name+'</option>';
                        });
                        $("select#product-brand").html(select);
                    }
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
        }else{
            alert('ไม่มีสินค้าในหมวดหมู่ '+$('option:selected', this).html());
            $(this).val('');
            $("select#product-brand").val('');
            $("select#product-brand").attr('disabled', 'true');
            
        }
        // ซ่อนไว้ก่อน
        /*
        $("#product-master-place").html('');
        var select = '<select class="form-control" name="product-master"  id="product-master"  required="required" disabled>';
        select += '<option value="">== เลือกสินค้า ==</option>';
        select += '</select>';
        $("#product-master-place").append(select);
        */

    });

    $('select#product-brand').change(function(){
        var productC = $('option:selected', this).attr('productC');
        if(productC != '' && productC != 'null' ){
            /*--- Replace Product master --*/
            // ซ่อนไว้ก่อน
            /*
            $.ajax({
                url:  base_url+"admin/productSet/getResProductMaster", //ทำงานกับไฟล์นี้
                data:  {'brand':$(this).val(), 'cate':$('select#product-cate').val()},  //ส่งตัวแปร
                type: "POST",
                dataType: 'json',
                async:false,
                success: function(data, status) {
                
                    if(data.length > 0){
                        $("#product-master-place").html('');
                        var select = '<select class="form-control" name="product-master"  id="product-master"  required="required">';
                        $.each(data,function(index,json){
                            select += '<option value="'+json.product_master_id+'">'+json.product_master_name+'</option>';
                        });
                        select += '</select>';
                        $("#product-master-place").append(select);
                        
                        // multiple select
                        $("select#product-master").attr('tabindex','-1');
                        $("select#product-master").attr('aria-hidden', 'true');
                        $("select#product-master").attr('multiple', 'multiple');
                        $('select#product-master').multiselect({
                            nonSelectedText: '== เลือกสินค้า ==',
                            filterPlaceholder: 'ค้นหา',
                            enableFiltering: true,
                            //includeSelectAllOption: true,
                            //selectAllText: '== เลือกทั้งหมด ==',
                            maxHeight: 260,
                            buttonWidth: '100%',
                            buttonTextAlignment: 'left',
                            dropUp: true,
                            buttonText: function(options, select) {
                                if (options.length == 0) {
                                    return '== เลือกสินค้า ==';
                                } else {
                                    if (options.length > this.numberDisplayed) {
                                        return options.length + ' ' + this.nSelectedText;
                                    }else {
                                        var selected = '';
                                        options.each(function() {
                                            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
                                            selected += label + ', ';
                                        });
                                        return selected.substr(0, selected.length - 2);
                                    }
                                }
                            },
                        });


                        $('select#product-master').change(function(){
                            var myJsonString = JSON.stringify(Object.assign({}, $(this).val()));
                            $('#product-master-json').val(myJsonString);
                        });
                    }
                },
                error: function(xhr, status, exception) { 
                    //console.log(xhr);
                }
            });
            */
            
        }else{
            alert('ไม่มีสินค้าในยี่ห้อ '+$('option:selected', this).html());
            $(this).val('');
            // ซ่อนไว้ก่อน
            /*
            $("#product-master-place").html('');
            var selects = '<select class="form-control" name="product-master"  id="product-master"  required="required" disabled readonly>';
            selects += '<option value="">== เลือกสินค้า ==</option>';
            selects += '</select>';
            $("#product-master-place").append(selects);
            */
        }
    });

    $('#product-brand').change(function(){
        $("#product-master").removeAttr('disabled');
    });

    $('#installation-type').change(function(){
        var res = $(this).find(':selected').attr('data-code');
        if(res == 1){
            $("#installation-fee").attr('disabled','disabled');
            $("#installation-fee").val(0);
        }else{
            $("#installation-fee").removeAttr('disabled');
            $("#installation-fee").val(0);
        }
    });

    $('#contract-type').change(function(){
        $("select#contract-doc").html('');
        $.ajax({
            url:  base_url+"admin/productSet/getResContractDoc", //ทำงานกับไฟล์นี้
            data:  {'type':$(this).val()},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                if(data.length > 0){
                    $("select#contract-doc").removeAttr('disabled');
                    var select = '<option value="" selected="false" disabled>กรุณาเลือก สัญญา(เอกสารสัญญา)</option>';
                    $.each(data,function(index,json){
                        select += '<option value="'+json.contract_id+'">'+json.contract_name+'</option>';
                    });
                    $("select#contract-doc").html(select);
                }
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
    });

</script>


