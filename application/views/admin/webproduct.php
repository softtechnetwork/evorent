<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>สินค้า<small></small></h2>
        <input name="base_url" value="<?=base_url();?>" type="hidden" >
        <ul class="nav navbar-right panel_toolbox">
          <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <!-- <p class="text-muted font-13 m-b-30">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code></p> -->
                <table id="products-table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                  <thead>
                    <tr class="headings">
                      <th class="column-title" style="width: 2rem;">ลำดับ</th>
                      <th class="column-title" style="width: 4rem;">รหัส</th>
                      <th class="column-title">สินค้า(หน้าเว็บ)</th>
                      <th class="column-title">สินค้า</th>
                      <!-- <th class="column-title">ราคา</th>
                      <th class="column-title">ยี่ห้อสินค้า</th>
                      <th class="column-title">ประเภทสินค้า</th>
                      <th class="column-title">รายละเอียด </th>-->
                      <th class="column-title">ประเภท ลูกค้า</th>
                      <th class="column-title" style="width: 7rem;">สถานะ</th> 
                      <th class="column-title no-link last" style="text-align: center; width: 5rem;"><span class="nobr"></span></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    var base_url = $('input[name="base_url"]').val();
    DrawTable(base_url, '#products-table');
    function DrawTable(base_url, replacePosition){
        var res = getProducts(base_url);
        if(res){ 
            var elements = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                var no = i+1;
                var agrmt = ["'"+base_url+"', '"+val.product_id+"'"];
                elements += '<tr>';
                elements += '   <td class="">'+no+'</td>';
                elements += '   <td class="">'+val.product_id+'</td>';
                var title = (val.title != null)? val.title : '';
                elements += '   <td class="">'+title+'</td>';
                elements += '   <td class="">'+val.product_name+'</td>';
                // elements += '   <td class="">'+val.product_pricce+'</td>';
                // elements += '   <td class="">'+val.brand_name+'</td>';
                // elements += '   <td class="">'+val.cate_name+'</td>';
                //elements += '   <td class="">'+val.product_detail+'</td>';
                elements += '   <td class="">'+val.customer_type+'</td>';
                
                var status_elm = '<div style="font-size: 13px; margin-bottom: inherit; background-color: #ffc107; border-radius: 30px;">ไม่ แสดงบนหน้าเว็บ</div>';
                if(val.display_status == 1){ 
                  status_elm ='<div style="font-size: 13px; margin-bottom: inherit; background-color: #1abb9c; color: white; border-radius: 30px; ">แสดงบนหน้าเว็บ</div>';
                }
                elements += '   <td class="" style="text-align: center;">'+status_elm+'</td>';

                elements += '   <td class="" style="text-align: center;">';
                elements += '       <a href="'+base_url+'admin/webProduct/product_manage/'+val.product_id+'">';
                elements += '           <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                elements += '           <i class="fa fa-wrench"></i> แก้ไข</button>';
                elements += '       </a>';
                elements += '   </td>';
                elements += '</tr>';
            });
            $(body).append(elements);
            
            // Datatable
            $(replacePosition).dataTable({
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function getProducts(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/webProduct/product_get", //ทำงานกับไฟล์นี้
            //data:  {'Search':search},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
  </script>



<!---- End Content ------>
  