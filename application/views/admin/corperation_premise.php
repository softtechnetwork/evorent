
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>หลักฐานลูกค้า(นิติบุคคล) <small>รหัส: <?=$res[0]->customer_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สำเนาบัตรประชาชน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-card"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สำเนาทะเบียนบ้าน<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="position-houseRegis"></div>
            </div>
        </div>
    </div>
</div>
<!-- Premise Model -->
<div id="modelPlace"></div>

<script>
    var base_url = "<?php echo base_url(); ?>";
    var refcode  = "<?php echo $res[0]->customer_code; ?>";
    var mode = "customer";

    var url_getRes = base_url+"admin/premise/getpremise"; // get premise
    var insertController = "admin/premise/insertpremise"; // insert premise
    getpremise(url_getRes, refcode, "#position-card", insertController, mode); // หลักฐาน สำเนาบัตรประชาชน
    getpremise(url_getRes, refcode, "#position-houseRegis", insertController, mode);// หลักฐาน สำเนาทะเบียนบ้าน
    
    AddPremise('.premise-add');
    viewPremise('.premise-view','#modelPlace');
    
    var delController = base_url+"admin/premise/delpremise"; // del premise
    delPremise('.premise-del', delController);
</script>