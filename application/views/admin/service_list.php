<style> .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>ประวัติการซ่อมบำรุง</h2>
        <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
        <ul class="nav navbar-right panel_toolbox">
            <li style="margin-right: 3px;">
                <select name='Search-type' id='Search-type' class="form-control">
                    <option value="t.temp_code" >เลขที่สัญญา</option>	
                    <option value="c.firstname" >ชื่อลูกค้า</option>
                </select>
            </li>
            <li style="margin-right: 3px;">
                <input name="Search" id="Search"value=""  type="text"  class="form-control" >
               
            </li>
            <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;"> <i class="fa fa-search"></i> ค้นหา</button></li>
            <li>
                <button id="service-button-reset" type='button' class="btn btn-warning" style=" margin-right:inherit; border-radius: inherit;">
                <i class="fa fa-refresh"></i> Reset</button>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
            <table id="loan_res" class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th class="column-title">ลำดับ</th>
                    <th class="column-title">เลขที่สัญญา </th>
                    <th class="column-title">ลูกค้า </th>
                    <th class="column-title">สินค้า </th>
                    <!--<th class="column-title" style="text-align: center;">จำนวนครั้ง</th>-->
                    <th class="column-title" style="text-align: center;">Action</th>
                </tr>
                </thead>

                <tbody></tbody>
            </table>
        </div>
            
        <input id="AllItems" value="" type="hidden" /> 
        <input id="page" value="1" type="hidden" /> 
        <div id="paginations"></div>
    </div>
    </div>
</div>
<!---- End Content ------>
<script>

    var base_url = $('input[name="base_url"]').val();
    var CItemPerPage = 30;
    //################  Group search #############//
   
    var url_getRes = base_url+"admin/service/getRes";
    getServiceRes(base_url,url_getRes,null,null,CItemPerPage,1,CItemPerPage);
    var url_getAll = base_url+"admin/service/getResAll";
    getCustomerAll(base_url,url_getAll,null,null);
    pagination();


    $('#button-search').click(function(){
        var type = $('#Search-type').val();
        var Search = $('#Search').val();
        
        getServiceRes(base_url, url_getRes, type, Search,CItemPerPage,1,CItemPerPage);
        getCustomerAll(base_url,url_getAll,type, Search);
        pagination();
    });

    $('#service-button-reset').click(function(){
        $('#Search-type').val("l.temp_code");
        $('#Search').val(null);
        getServiceRes(base_url,url_getRes,null,null,CItemPerPage,1,CItemPerPage);
        getCustomerAll(base_url,url_getAll,null,null);
        pagination();
    });


    //################  table  ################//
    function getServiceRes(base_url, url_getRes, type, Search, itemPerPage,itemStt,itemEnd){
        var res = null;
        $.ajax({
            url: url_getRes, //ทำงานกับไฟล์นี้
            data: {
                "type" : type,
                "search" : Search,

                "itemPerPage" : itemPerPage, 
                "itemStt" : itemStt,
                "itemEnd" : itemEnd
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
               console.log(data);
                var tr = null;
                if(data.length > 0){
                    var num = 1;
                    $.each(data, function (i, val) {
                        tr += '<tr class="even pointer">';
                        tr += '<td class="a-center ">'+val['RowNum']+'</td>';
                        tr += '<td class=" ">'+val['temp_code']+'</td>';
                        tr += '<td class=" ">'+val['firstname']+' '+val['lastname']+'</td>';
                        tr += '<td class=" ">'+val['product_name']+'</td>';
                        tr += '<td class=" last"  style="text-align: center;">';
                        tr +=   '<a href="'+base_url+'admin/service/view/'+val['service_code']+' ">';
                        tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i> View</button>';
                        tr +=   '</a>';
                        tr += '</td>';
                        tr += '</tr>';
                        num++;
                    });
                }

                $("#loan_res tbody").html(tr);
                 
            },
            error: function(xhr, status, exception) {  }
        });
    }

    //################  get All Items  ################//
    function getCustomerAll(base_url, url_getAll, type, Search){
        $.ajax({
            url: url_getAll, //ทำงานกับไฟล์นี้
            data: { 
                "type" : type,
                "Search" : Search
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { console.log(data); $("#AllItems").val(data[0].allitems); },
            error: function(xhr, status, exception) {  }
        });
    }

    function pagination(){
        $('#paginations').html(null);
        var CAllItem = $("#AllItems").val();
        var Cpage = Math.ceil(CAllItem/CItemPerPage) //ปัดขึ้น;
        //var Cpage = 12 //ปัดขึ้น;
        //console.log('kkkkkkk');

        //-----------html pagination------------//
        // paginationHTML(Cpage, 1);
        $('#paginations').html(null);

        var btnPage = '<button id="suspend-left"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-left"></i></button>';
        for (let index = 1; index <= Cpage; index++) {
            btnPage += '<button id="'+index+'"  type="button" class="btn btn-warning btn-paginations ">'+index+'</button>';
        }
        btnPage += '<button id="suspend-next"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-right"></i></button>';
        $('#paginations').append(btnPage);

        $('#1').addClass('focus-paginations');
        if(Cpage > 1){ $("#suspend-next").removeAttr('disabled');}

        //-----------btn click------------//
        $('.btn-paginations').click(function(){
            var paged = parseInt($('#page').val());
            switch(this.id){
                case'suspend-left': paged = parseInt(paged)-1; break;
                case'suspend-next':paged = parseInt(paged)+1; break;
                default: paged = parseInt(this.id); break;
            }

            if(paged > 1){ $("#suspend-left").removeAttr('disabled');}
            if(paged == 1){ $("#suspend-left").attr('disabled','disabled');}
            if(paged == Cpage){ $("#suspend-next").attr('disabled','disabled');}
            if(paged < Cpage){ $("#suspend-next").removeAttr('disabled');}

            $('#page').val(paged);

            if(paged == parseInt($('#page').val())){ $('.btn-paginations').removeClass('focus-paginations'); $('#'+paged).addClass('focus-paginations');}
            
            var itemStt = ((CItemPerPage*paged)-CItemPerPage)+1;
            var itemEnd = CItemPerPage*paged;
            
            var type = $('#cusSearch-type').val();
            var cusSearch = $('#cusSearch').val();
            var cusStatus = $('#cusStatus').val();
            getServiceRes(base_url, url_getRes, type, cusSearch,CItemPerPage,itemStt,itemEnd);
        });
    }

    function paginationHTML( Cpage, page){
        $('#paginations').html(null);

        var btnPage = '<button id="suspend-left"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-left"></i></button>';
        for (let index = 1; index <= Cpage; index++) {
            btnPage += '<button id="'+index+'"  type="button" class="btn btn-warning btn-paginations ">'+index+'</button>';
        }
        btnPage += '<button id="suspend-next"  type="button" class="btn btn-success btn-paginations"><i class="fa fa-chevron-right"></i></button>';
        $('#paginations').append(btnPage);

        $('#1').addClass('focus-paginations');
    }


</script>