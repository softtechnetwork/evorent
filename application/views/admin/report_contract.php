<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
    .gettopdf{

    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>รายงานสัญญาลูกค้า</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="customer_code">รหัสลูกค้า</label>
                    <select id="customer_code" name="customer_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสลูกค้า</option>
                        <?php  foreach($getCustomer as $item) :?>
                            <option value="<?php echo $item->customer_code;?>"> <?php echo $item->customer_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <div class ="col-md-2">
                    <label for="contract_code">รหัสสัญญา</label>
                    <select id="contract_code" name="contract_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสสัญญา</option>
                        <?php  foreach($getTemp as $item) :?>
                            <option value="<?php echo $item->contract_code;?>"> <?php echo $item->contract_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <div class ="col-md-2">
                    <label for="reservation">วันที่เริ่มต้นสัญญา</label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stcontractdate" id="stcontractdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b; max-width: 100% !important;">
                    </div>
                </div>
                <div class ="col-md-2">
                    <div class ="row ">
                        <div class ="col-md-6  navbar-right " style=" padding-top: 1.65rem;">
                            <button id="report-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                                <i class="fa fa-search"></i> ค้นหา
                            </button>
                        </div>
                        <div class ="col-md-6  navbar-right "  style=" padding-top: 1.65rem;">
                            <button id="report-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                                <i class="fa fa-refresh"></i> Reset
                            </button>
                        </div>
                    </div> 
                </div>
                <div class ="col-md-4">
                    <div class ="row ">
                        <div class ="col-md-12  navbar-right " style=" padding-top: 1.65rem; text-align: right;">
                            <form id="pdfExport-form"  method="post" action="<?=base_url();?>admin/report/exportContractToPDF/" target="_blank">
                                <input type="hidden" id="customer_code" name="customer_code">
                                <input type="hidden" id="contract_code" name="contract_code">
                                <input type="hidden" id="stcontractdate" name="stcontractdate">
                                <button id="pdfExport-btn"  type='button' class="btn btn-dark" style="border-radius: inherit;">
                                    <i class="fa fa-file-excel-o"></i> PDF
                                </button>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>ตารางสัญญาลูกค้า</h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table id="tables" class="table table-striped jambo_table bulk_action"   style="width:100%; border-spacing: 1px !important;">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">เลขที่สัญญา</th>
                        <th class="column-title">รหัสลูกค้า </th>
                        <th class="column-title">ลูกค้า </th>
                        <th class="column-title">สินค้า </th>
                        <th class="column-title">ยี่ห้อ </th>
                        <th class="column-title">ราคาผ่อนต่อเดือน</th>
                        <th class="column-title">วันที่เริ่มต้นสัญญา</th>
                        <th class="column-title">หมายเลขโทรศัพท์</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    var base_url = $('input[name="base_url"]').val(); 
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
            $('.next').text(">");
    });

    $("#report-reset").click(function () {
        $('#customer_code').val(null);
        $('[data-id=customer_code] .filter-option-inner-inner').html('เลือก รหัสลูกค้า');

        $('#contract_code ').val(null);
        $('[data-id=contract_code] .filter-option-inner-inner').html('เลือก รหัสสัญญา');

        $('#stcontractdate ').val(null);
        $('#stcontractdate').removeAttr("min");
        $('#stcontractdate').removeAttr("max");

        DrawTable(base_url, '#tables');
    });

    $("#report-search").click(function () {
        DrawTable(base_url, '#tables');
    });

    $("#pdfExport-btn").click(function () {
        DrawTable(base_url, '#tables');
        $('#pdfExport-form #customer_code').val($("#customer_code").val());
        $('#pdfExport-form #contract_code').val($("#contract_code").val());
        $('#pdfExport-form #stcontractdate').val(formatPDate($('#stcontractdate').val(),'-'));
        $("#pdfExport-form").submit();
    });
    
    function formatPDate(date, string) {
        if(date != '' && date != null){
            var temp = date.split('/'); 
            var res = temp[2]+string+temp[1]+string+temp[0];
            return res;
        }else{
            return '';
        }
    }

    DrawTable(base_url, '#tables');
    function DrawTable(base_url, replacePosition){
        $('#tables').DataTable().destroy();
        var res = getdatas(base_url);
        //console.log(res);
        if(res){ 
            var td = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                td += "<tr>";
                td += " <td>"+val.RowNum+"</td>";
                td += " <td>"+val.contract_code+"</td>";
                td += " <td>"+val.customer_code+"</td>";
                td += " <td>"+val.firstname+" "+val.lastname+"</td>";
                td += " <td>"+val.product_name+"</td>";
                td += " <td>"+val.brand_name+"</td>";
                td += " <td>"+val.monthly_rent+"</td>";
                td += " <td>"+val.contract_date+"</td>";
                td += " <td>"+val.tel+"</td>";
                td += '</td>';
                td += "</tr>";
            });
            $(body).append(td);
            
            // Datatable
            $(replacePosition).dataTable({
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function getdatas(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/report/getContract", //ทำงานกับไฟล์นี้
            data:  {
                'customer_code': $('#customer_code ').val(), 
                'contract_code': $('#contract_code ').val(), 
                'stcontractdate': formatPDate($('#stcontractdate').val(),'-')
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
</script>
<!---- End Content ------>
