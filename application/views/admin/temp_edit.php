<style>
.fail-stat-sale{color: #d9530e;}
.succ-stat-sale{color: #26b99a!important;}

.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขข้อมูลสัญญาลูกค้า <small><?=$res[0]->temp_code;?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>-->
                </ul>
                <div class="clearfix"></div>
            </div>
            <?php //print_r($res[0]); ?>
            <div class="x_content">
                <form id="temp-edit-form" action="<?php echo base_url('/admin/temp/update');?>" enctype="multipart/form-data"  method="post" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ลูกค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 ">
                                    <input type="text" name="customer" id="customer" class="form-control" value="<?php echo $res[0]->firstname." ".$res[0]->lastname;?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                                    <input class="form-control" type="text"  id="product" name="product" value="<?php echo $res[0]->cate_name;?>" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <input class="form-control" type="text"  id="product" name="product" value="<?php echo $res[0]->product_name;?>" readonly/>
                                </div>
                            </div>
                        </div>
                        <!--
                        <label class="col-form-label col-md-1 col-sm-1  label-align">ราคา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" id="product-price" name="product-price" value="<?php echo $res[0]->product_price;?>" readonly/>
                        </div>
                        
                        <label class="col-form-label col-md-1 col-sm-1  label-align">จำนวน</label>
                        <div class="col-md-1 col-sm-1">
                            <input class="form-control" type="number"  id="product-count" name="product-count" min="1" value="<?php echo $res[0]->product_count;?>" readonly />
                        </div>
                        -->
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">ยี่ห้อ</label>
                                    <input class="form-control" type="text" value="<?php echo $res[0]->brand_name;?>"  id="product-brand" name="product-brand"required="required" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label  label-align">รุ่น</label>
                                    <input class="form-control" type="text"  value="<?php echo $res[0]->product_version;?>" id="product-version" name="product-version" required="required" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text"  value="<?php echo $res[0]->product_number;?>"id="property-number" name="property-number" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control" type="text" value="<?php echo $res[0]->product_serial;?>" id="product-serial" name="product-serial"required="required" readonly/>
                        </div>
                    </div>-->
                    <div class="ln_solid"></div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ทำสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->contract_do_date));?>"  id="contract-do-date" name="contract-do-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่เริ่มสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->contract_date));?>"  id="contract-date" name="contract-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">วันที่เริ่มชำระงวดแรก</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" value="<?=date("d/m/Y", strtotime($res[0]->payment_start_date));?>"  id="payment-start-date" name="payment-start-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">กำหนดชำระวันที</label>
                        <div class="col-md-1 col-sm-1">
                            <!--<input class="form-control" type="number"  min="1"  max="31" value="<?php echo $res[0]->payment_due;?>" id="payment-due" name="payment-due" />-->
                            <select name='payment-due' id='payment-due' class="form-control">
                                <option value="15" <?php if($res[0]->payment_due == 15 ){ echo 'selected="selected"';} ?>>15</option>
                                <option value="30" <?php if($res[0]->payment_due == 30 ){ echo 'selected="selected"';} ?>>30</option>
                            </select>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1">ของทุกเดือน</label>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <!--<th>เลือก</th>-->
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inhabited as $item) : ?>
                                        <tr class="">
                                            <!--
                                            <td style="  padding: .5rem;">
                                                <input class="inhabitedInput" type='radio'  name='inhabited' id='inhabited<?=$item->id?>' value='<?=$item->inhabited_code?>' <?php if($res[0]->addr == $item->inhabited_code){echo 'checked="checked"';} ?>/> 
                                            </td>
                                            -->
                                            <td><?=$item->address?></td>
                                            <td><?=$item->district_name?></td>
                                            <td><?=$item->amphur_name?></td>
                                            <td><?=$item->province_name?></td>
                                            <td><?=$item->zip_code?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าผ่อนรายเดือน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value="<?php echo $res[0]->monthly_rent;?>"  id="monthly-rent" name="monthly-rent"required="required" readonly/>
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">ภาษีมูลค่าเพิ่ม</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value="<?php echo $res[0]->monthly_plus_tax;?>"  id="monthly-plus-tax" name="monthly-plus-tax"readonly/>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">ระยะเวลาการเช่า</label>
                        <div class="col-md-1 col-sm-1">
                            <input class="form-control"  min="1"  type="number" value="<?php echo $res[0]->rental_period;?>" id="rental-period" name="rental-period"required="required" readonly/>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1">เดือน</label>
                    </div>
                    -->

                    <?php if($res[0]->installment != ''){ ?>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                            <div class=" col-md-4 col-sm-4">
                                <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                    <thead>
                                        <tr>
                                            <th>จำนวนงวด</th>
                                            <th>ผ่อนเดือนละ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?=$res[0]->rental_period;?></td>
                                            <td><?=$res[0]->monthly_rent;?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" id="installment" name="installment"/>
                        </div>
                        <div class="ln_solid"></div>
                    <?php } ?>



                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เงินดาวน์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" min="0" value="<?php echo $res[0]->down_payment;?>"  id="down-payment" name="down-payment" required="required" readonly/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระค่าเช่าล่วงหน้าเป็นเงินจำนวน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="<?php echo $res[0]->advance_payment;?>" min="0" id="advance-payment" name="advance-payment"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าธรรมเนียมดำเนินการติดตั้ง</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"value="<?php echo $res[0]->installation_fee;?>" min="0" id="installation-fee" name="installation-fee"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระโดยการโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร กสิกรไทย</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร ไยพาณิชย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="ln_solid"></div>-->

                    





                    <!--------  Supporter --------->
                    
                    <input type="hidden" id='supportered' name='supportered' value="<?=$res[0]->supporter;?>"/>
                    <?php if($res[0]->supporter == 1){?>

                        <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-8 col-sm-8 ">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <!--<th>เลือก</th>-->
                                        <th>ผู้คำ้ประกัน</th>
                                        <th>หมายเลขบัตรประชาชน</th>
                                        <th>หมายเลขมือถือ</th>
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td><?=$supporter[0]->fname?> <?=$supporter[0]->lname?></td>
                                        <td><?=$supporter[0]->idcard?></td>
                                        <td><?=$supporter[0]->tel?></td>
                                        <td><?=$supporter[0]->addr?></td>
                                        <td><?=$supporter[0]->district_name?></td>
                                        <td><?=$supporter[0]->amphur_name?></td>
                                        <td><?=$supporter[0]->province_name?></td>
                                        <td><?=$supporter[0]->zip_code?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <?php }else{ ?>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-9 col-sm-9">
                            <ul id="stats" style=" padding-left: initial; margin-top: 6px;">
                                <li style=" display: inline;list-style-type: none;padding-right: 3px;float: left;">
                                    <input class="inhabitedInput" type="checkbox" id='supporter' name='supporter' value=""/>
                                    <input type="hidden" id='supporter-check' name='supporter-check' value="0"/>
                                </li>
                                <li style=" display: inline;list-style-type: none;padding-right: 10px;float: left;">
                                    <p>เลือกเมื่อมีผู้คำ้ประกัน</p>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="field item form-group collapse" id="collapse-supporter">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-9 col-sm-9">
                            <div class="card card-body " style="border-radius: inherit;background-color: #f8f9fa;">
                                <div class="field item form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">ชื่อ ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="name-supporter" name="name-supporter"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">นามสกุล ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="sname-supporter" name="sname-supporter"/>
                                    </div>
                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขบัตรประชาชน ผู้คำ้ประกัน</label>
                                        <input name="idcard-supporter" id="idcard-supporter" type="tel"  maxlength="13"  class="form-control" required="required">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">วันเกิด ผู้คำ้ประกัน</label>
                                        <input class="form-control datepicker" type="text" id="bdate-supporter" name="bdate-supporter"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขมือถือ ผู้คำ้ประกัน</label>
                                        <input class="form-control" type="tel" id="mobile-supporter" name="mobile-supporter" >
                                    </div>

                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label class="col-form-label label-align">ที่อยู่ผู้คำ้ประกัน</label>
                                        <textarea  class="form-control" required="required" id='address-supporter' name='address-supporter'></textarea>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <div class="col-md-3 col-sm-3">
                                        <label for="province-supporter">จังหวัด</label>
                                        <select name='province-supporter' id='province-supporter' class="form-control">
                                            <option value="" selected='false' disabled>จังหวัด</option>	
                                            <?php foreach ($province as $item) : ?>
                                                <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="amphurs-supporter">อำเภอ / เขต</label>
                                        <select class="form-control" name="amphurs-supporter"  id="amphurs-supporter">
                                            <option value="">อำเภอ / เขต</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="district-supporter">ตำบล / แขวง</label>
                                        <select class="form-control" name="district-supporter"  id="district-supporter">
                                            <option value="">ตำบล / แขวง</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="zipcode-supporter">รหัสไปรษณีย์</label>
                                        <input type="tel" maxlength="5" class="form-control" name="zipcode-supporter" id="zipcode-supporter" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <?php } ?>
                    

                    
                    <!---------------------------------------->









                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นายหน้าให้เช่าทรัพทืสินและบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" value="<?php echo $res[0]->sale_code;?>" id="sale" name="sale" />-->
                            <input class="form-control" value="<?php echo $res[0]->sale_code;?>"   name="sale-id" readonly />
                        </div>
                        <!--<button id="sale-check"  type='button' class="btn btn-info">ตรวจสอบ</button>-->
                        <!--<label id="sale-status" class="col-form-label col-md-3 col-sm-3"></label>-->

                        <label class="col-form-label col-md-1 col-sm-1  label-align">ตัวแทนบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" value="<?php echo $res[0]->techn_code;?>" id="sale" name="sale" />-->
                            <input class="form-control" value="<?php echo $res[0]->techn_code;?>"  id="techn-id" name="techn-id" readonly />
                        </div>

                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเหตุ</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" name='remark'><?php echo $res[0]->remark;?></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Status</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='estatus' id='estatus' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option  value="<?php echo $item->id; ?>" key="<?php echo $item->status_code; ?>"
                                        <?php if($res[0]->status == $item->id ){ echo 'selected="selected"';} ?> >
                                        <?php echo $item->label; ?>
                                    </option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    
                    <input type="hidden" name="temp_code" value="<?php echo $res[0]->temp_code; ?>">
                    <input type="hidden"  name="product_id" value="<?php echo $res[0]->product_id;?>">
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/temp');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">Submit</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
        $('.next').text(">");
    });
    var base_url = "<?php echo base_url(); ?>";
    //--------- supporter ------------  //
    $("#supporter").change(function () {
        var supporter = $('#'+this.id).prop('checked');
        if(supporter){
            $("#supporter-check").val(1);
            $("#collapse-supporter").collapse('show');
        }else{
            $("#supporter-check").val(0);
            $("#collapse-supporter").collapse('hide');
        }
    });

    var supporterProvince = ["#province-supporter","#amphurs-supporter","#district-supporter","#zipcode-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    rePlaceAddEle(supporterProvince, base_url);//replace ข้อมูลสถานที่ทำงาน

    var supporterEle = ["#supporter","#name-supporter","#sname-supporter","#idcard-supporter","#bdate-supporter","#mobile-supporter","#address-supporter",  "#province-supporter","#amphurs-supporter","#district-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    ChangeRemoveValidClass(supporterEle);
    //--------------------------------//

    //################  submit Edit form #############//
    var input_id = [];
    tempEditSubmit(input_id, '#temp-edit-form', supporterEle);
</script>

