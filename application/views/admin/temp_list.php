<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ค้นหาข้อมูลสัญญาลูกค้า</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="temp_code">รหัสสัญญา</label>
                    <input id="temp_code" name="temp_code" type="text" class="form-control" placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="name">ชื่อลูกค้า</label>
                    <input id="name" name="name" type="text" class="form-control"  placeholder="..." />
                </div>
                <div class ="col-md-2">
                    <label for="name">สถานะ</label>
                    <select name='status' id='status' class="form-control">
                        <option value="" selected='false' disabled>เลือกสะถานะ</option>
                        <?php foreach ($resStatus as $item) : ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="button-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
                <div class ="col-md-2  navbar-right "  style=" padding-top: 1.65rem;">
                    <a class="btn btn-success" href="<?php echo base_url('admin/temp/created');?>"  style="color: #ffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มสัญญาลูกค้า
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลสัญญาลูกค้า <small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="temp-table" class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">รหัสสัญญา </th>
                        <th class="column-title">ชื่อลูกค้า </th>
                        <th class="column-title">เบอร์โทรศัพท์ </th>
                        <th class="column-title">สินค้า </th>
                        
                        <th class="column-title">อีเมล์ </th>
                        <th class="column-title">หมายเลขทรัพย์สิน </th>
                        <th class="column-title" style="text-align: center;">สถานะ </th>
                        <th class="column-title no-link last"  style="text-align: center;width: 15rem;">
                            <span class="nobr">กิจกรรม</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="pagination" page="1" ></div>
            
        </div>
    </div>
</div>
<!---- End Content ------>
<script>

    var base_url = $('input[name="base_url"]').val();
    var getRes = base_url+"admin/temp/TempGetRes";
    var getAll = base_url+"admin/temp/getResAll";

    var searchEle = ["#temp_code", "#name", "#status"];
    var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
    var resObj = {"getRes" :getRes, "elementTable":"#temp-table tbody"};
    var allObj = {"getAll":getAll, "functionResName":"TempDrawList"};

    TempDrawList(resObj, searchEle, pageObj);
    Pagination(allObj, searchEle, pageObj, resObj);


    $('#button-search').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        TempDrawList(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });
    
    $('#button-reset').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        $.each(searchEle, function (i, val) {
            $(val).val('');
        });

        TempDrawList(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });

</script>
    