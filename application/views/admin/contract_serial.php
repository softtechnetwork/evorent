<!----  Style ------>
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
<style>
#AddSerial-modal .table td{padding: .5rem;}
#AddSerial-modal .table tr .form-control{height: calc(1em + .75rem + 2px);}

#EditSerial-modal .table td{padding: .5rem;}
#EditSerial-modal .table tr .form-control{height: calc(1em + .75rem + 2px);}

.table td, .table th {
    font-size:small;
}
.table td button, .table th button{
    
    font-size: inherit;
    border-radius: 50%;
    /* padding: 3%;
    width: 25%; */
    margin-bottom: inherit;
}
</style>
<!----  Content ------>
<div class="clearfix"></div>
<input name="base_url" value="<?=base_url();?>" type="hidden" >

<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>สัญญาหลัก รหัส : <small><?=$contract_code;?></small></h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <label class="col-form-label col-md-12 col-sm-12"><strong>สินค้าหลัก</strong></label>
                    <div class="col-md-12 col-sm-12 ">
                        <table class="table table-striped jambo_table bulk_action" id="address-table">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">ลำดับ</th>
                                    <th>สินค้า</th>
                                    <th></th>
                                    <th>จำนวน</th>
                                    <th>ประเภท</th>
                                    <th style="text-align: center;padding: .75rem .3rem;">เพิ่ม</th>
                                    <th style="text-align: center;padding: .75rem .3rem;">แก้ไข</th>
                                    <th style="text-align: center;padding: .75rem .3rem;">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($mainProduct as $n => $item){?>
                                    <tr>
                                        <td style="width:5%"><?=$n+1;?></td>
                                        <td style="width:60%"><?=$item->product_master_name;?></td>
                                        <td style="width:20%"><?=($item->serial_count == 0)?'<div style="color: #ff6e00;">ไม่มีข้อมูล Serial number ในระบบ</div>':'';?></td>
                                        <td style="width:5%"><?=$item->count;?></td>
                                        <td style="width:10%"><?=$item->label;?></td>
                                        <td style="text-align: center;padding: .75rem .3rem;">
                                            <button type='button' data-attr='<?=json_encode($item)?>' data-contractCode='<?=$contract_code?>' class='btn btn-round btn-success serial-add-btn' style="margin-right: 0px;padding:5px 10px;"><i class='fa fa-plus'></i></button>
                                        </td>
                                        <td style="text-align: center;padding: .75rem .3rem;">
                                            <button type='button' data-attr='<?=json_encode($item)?>' data-contractCode='<?=$contract_code?>' class='btn btn-round btn-warning serial-edt-btn' style="margin-right: 0px;padding:5px 10px;"><i class='fa fa-wrench'></i></button>
                                        </td>
                                        <td style="text-align: center;padding: .75rem .3rem;">
                                            <button type='button' data-attr='<?=json_encode($item)?>' data-contractCode='<?=$contract_code?>' class='btn btn-round btn-danger serial-del-btn' style="margin-right: 0px;padding:5px 10px;"><i class='fa fa-wrench'></i></button>
                                        </td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(count($addonContract) > 0){ ?>
<div class="row mt-5">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>ส่วนเสริมสัญญา</h2>
                <ul class="nav navbar-right panel_toolbox" style="min-width: 0 !important;">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php foreach($addonContract as $item){?>
                    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a class="panel-heading collapsed collapse-btn" data-id="<?=$item->addon_code?>" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapse<?=$item->addon_code?>" aria-expanded="false" aria-controls="collapseOne">
                                <h4 class="panel-title"><strong>รหัสส่วนเสริมสัญญา</strong> : <?=$item->addon_code?> </h4>
                            </a>
                            <div id="collapse<?=$item->addon_code?>" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                <div class="panel-body">
                                    <table class="table table-striped jambo_table bulk_action" id="address-table">
                                        <thead>
                                            <tr>
                                                <th>ลำดับ</th>
                                                <th>สินค้า</th>
                                                <th></th>
                                                <th>จำนวน</th>
                                                <th>ประเภท</th>
                                                <th style="text-align: center;padding: .75rem .3rem;">เพิ่ม</th>
                                                <th style="text-align: center;padding: .75rem .3rem;">แก้ไข</th>
                                                <th style="text-align: center;padding: .75rem .3rem;">ลบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($item->productRes as $n => $pitem){?>
                                                <tr>
                                                    <td style="width:5%"><?=$n+1;?></td>
                                                    <td style="width:60%"><?=$pitem->product_master_name;?></td>
                                                    <td style="width:20%"><?=($pitem->serial_count == 0)?'<div style="color: #ff6e00;">ไม่มีข้อมูล Serial number ในระบบ</div>':'';?></td>
                                                    <td style="width:5%"><?=$pitem->count;?></td>
                                                    <td style="width:10%"><?=$pitem->label;?></td>
                                                    <td style="text-align: center;padding: .75rem .3rem;">
                                                        <button type='button' data-attr='<?=json_encode($pitem)?>' data-contractCode='<?=$item->addon_code?>' class='btn btn-round btn-success serial-add-btn' style="margin-right: 0px; 0px;padding:5px 10px;"><i class='fa fa-plus'></i></button>
                                                    </td>
                                                    <td style="text-align: center;padding: .75rem .3rem;">
                                                        <button type='button' data-attr='<?=json_encode($pitem)?>' data-contractCode='<?=$item->addon_code?>' class='btn btn-round btn-warning serial-edt-btn' style="margin-right: 0px; 0px;padding:5px 10px;"><i class='fa fa-wrench'></i></button>
                                                    </td>
                                                    <td style="text-align: center;padding: .75rem .3rem;">
                                                        <button type='button' data-attr='<?=json_encode($pitem)?>' data-contractCode='<?=$item->addon_code?>' class='btn btn-round btn-danger serial-del-btn' style="margin-right: 0px; 0px;padding:5px 10px;"><i class='fa fa-wrench'></i></button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>      
                    </div>
                    <hr>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<!-- Modal Edit Serial -->
<div class="clearfix"></div>
<div id="EditSerial-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title EditSerial-header">Serial number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="serial-place"></div>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-info edit-serial-submit' attr-data-id=""><i class='fa fa-save'></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
<!---- End Content ------>

<!-- Modal Add Serial -->
<div class="clearfix"></div>
<div id="AddSerial-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title AddSerial-header">Create serial number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="addserial-place" data-count=""></div>
            </div>
            <div class="modal-footer">
                <button type='button' data-contract="" data-res="" class='btn btn-warning without-serial-btn'><i class='fa fa-save'></i> ไม่มี serial number</button>
                <button type='button' data-contract="" data-res="" class='btn btn-success add-serial-list  mr-auto'><i class='fa fa-plus'></i> สร้าง serial number</button>
                <button type='button' class='btn btn-info add-serial-submit'><i class='fa fa-save'></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
<!---- End Content ------>

<!-- <script src="<?//=base_url('./assete/js/admin/contract_custom.js');?>"></script> -->
<script>

    var base_url = $('input[name="base_url"]').val();
    

    


    //เพิ่ม Serial number
    $('.serial-add-btn').click(function(){
        var contract_code = $(this).attr('data-contractCode');
        var res = $(this).attr('data-attr');
        $('.without-serial-btn').attr('data-contract', contract_code);
        $('.without-serial-btn').attr('data-res', res);
        $('.without-serial-btn').prop('disabled', false);// disable ปุ่มบันทึก

        $('.add-serial-list').attr('data-contract', contract_code);
        $('.add-serial-list').attr('data-res', res);
        $('.add-serial-list').prop('disabled', false);// disable ปุ่มบันทึก

        $('.add-serial-submit').prop('disabled', true);// disable ปุ่มบันทึก

        var resObj = JSON.parse(res);
        var serials = getSerialsByProductsub(contract_code, resObj.product_id, resObj.product_sub_id);
        if(serials.length == 0){
            var table = '<div style=" text-align: center;color: #fe7106; font-size: 15px; ">ไม่มีข้อมูล Serial number ในระบบ</div>';
            $('#addserial-place').attr('data-count', resObj.count);
            $('#addserial-place').html(null);
            $('#addserial-place').append(table);

            var headers = resObj.product_master_name+' ( จำนวน '+resObj.count+' )';
            $('.AddSerial-header').html(headers);
            $('#AddSerial-modal').modal();
        }else{
            Swal.fire({
                title: "มีข้อมูล Serial number ในระบบแล้ว",
                //text: "You won't be able to revert this!",
                icon: 'success',
                //showCancelButton: true,
                confirmButtonColor: '#3085d6',
                //cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง'
            });
        }
    });
    function getSerialsByProductsub(contract_code, product_id, product_sub_id){
        var res = null;
        $.ajax({
            url: base_url+"admin/ContractAddon/getSerialsByProductsub", //ทำงานกับไฟล์นี้
            data:  {
                'contract_code': contract_code,
                'product_id': product_id,
                'product_sub_id': product_sub_id
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
    function DrawTableAddSerial(contract_code, resObj){
        var table = '';
        table += '<div class="table-responsive">';
        table += '<table class="table table-striped jambo_table bulk_action">';
        table += '<thead>';
        table += '<tr class="headings">';
        table += '<th class="column-title">ลำดับ</th>';
        table += '<th class="column-title">Serial number</th>';
        table += '<th class="column-title">หมายเหตุ</th>';
        table += '</tr>';
        table += '</thead>';
        table += '<tbody>';
        for (let c = 1; c <= resObj.count; c++) {
            table += '<tr>';
            table += '<td style="width:5%;text-align: center;">'+c;
            table += '<input id="contract_code'+c+'" type="hidden" value="'+contract_code+'">';
            table += '<input id="product_id'+c+'" type="hidden" value="'+resObj.product_set_id+'">';
            table += '<input id="product_sub_id'+c+'" type="hidden" value="'+resObj.id+'">';
            table += '</td>';
            table += '<td style="width:20%;"><input id="serial'+c+'" type="text" class="form-control serials-res" placeholder="..." value=""></td>';
            table += '<td style="width:80%;"><input id="remark'+c+'" type="text" class="form-control remark-res" placeholder="..." value=""></td>';
            table += '</td>';
            table += '</tr>';
        }
        table += '</tbody>';
        table += '</table>';
        table += '</div>';
       
        return table;
    }
    function insertSerials(arr){
        $.ajax({
            url: base_url+"admin/ContractAddon/CreatSerialAjax",
            data:{ 'obj': arr },
            dataType: 'json',
            type: "POST",
            async:false,
            success: function(data)
            {
                $('.add-serial-submit').prop('disabled', true);// disable ปุ่มบันทึก
                if(data.status){
                    Swal.fire({
                        title: data.data,
                        //text: "You won't be able to revert this!",
                        icon: 'success',
                        //showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        //cancelButtonColor: '#d33',
                        confirmButtonText: 'ตกลง'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#AddSerial-modal').modal('hide');
                        }
                    });
                }else{
                    Swal.fire(data.data, '', 'info');
                }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
    }
    $('.without-serial-btn').click(function(){
        var contract_code = $(this).attr('data-contract');
        var res = $(this).attr('data-res');
        var resObj = JSON.parse(res);
        var arr = [];
        var res = {
            "contract_code" : contract_code,
            "product_id":resObj.product_id,
            "product_sub_id":resObj.product_sub_id,
            "serial_number":'',
            "remark":''
        };
        arr.push(res);
        insertSerials(arr);
    });
    $('.add-serial-list').click(function(){
        var contract_code = $(this).attr('data-contract');
        var res = $(this).attr('data-res');
        var resObj = JSON.parse(res);
        var table = DrawTableAddSerial(contract_code, resObj);
        $('#addserial-place').attr('data-count', resObj.count);
        $('#addserial-place').html(null);
        $('#addserial-place').append(table);

        $('.add-serial-submit').prop('disabled', false);// disable ปุ่มบันทึก
        $('.add-serial-list').prop('disabled', true);
        $('.without-serial-btn').prop('disabled', true);
        $('#AddSerial-modal').modal();
    });
    $(".add-serial-submit").click(function(e) {
        var datacount = $('#addserial-place').attr('data-count');
        var arr = [];
        for (let c = 1; c <= datacount; c++) {
            var res = {
                "contract_code" : $('#contract_code'+c).val(),
                "product_id":$('#product_id'+c).val(),
                "product_sub_id":$('#product_sub_id'+c).val(),
                "serial_number":$('#serial'+c).val(),
                "remark":$('#remark'+c).val()
            };
            arr.push(res);
        }
        insertSerials(arr);
    });

    //แก้ไข Serial number
    $('.serial-edt-btn').click(function(){
        var contract_code = $(this).attr('data-contractCode');
        var res = $(this).attr('data-attr');
        var getRes = getResSerials(contract_code, res);
        if(getRes.length > 0){
            var table = setTableEditSerial(getRes);
            var resObj = JSON.parse(res);
            var headers = resObj.product_master_name+' ( จำนวน '+resObj.count+' )';
            $('.EditSerial-header').html(headers);

            $('#serial-place').html(null);
            $('#serial-place').append(table);
            $('.edit-serial-submit').attr('attr-data-id', JSON.stringify(getRes));
            $('.edit-serial-submit').prop('disabled', false);// undisable ปุ่มบันทึก
            $('#EditSerial-modal').modal();
        }else{
            Swal.fire({
                title: "ไม่มีข้อมูล Serial number ในระบบ",
                //text: "You won't be able to revert this!",
                icon: 'warning',
                //showCancelButton: true,
                confirmButtonColor: '#3085d6',
                //cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง'
            });
        }
    });
    function getResSerials(contract_code, datas){
        var res = null;
        $.ajax({
            url: base_url+"contractaddon/getResSerials", //ทำงานกับไฟล์นี้
            data:  {
                'contract_code': contract_code,
                'data': datas
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    
    }
    function setTableEditSerial(res){
        var table = '';
            table += '<div class="table-responsive">';
            table += '<table class="table table-striped jambo_table bulk_action">';
            table += '<thead>';
            table += '<tr class="headings">';
            table += '<th class="column-title">ลำดับ</th>';
            table += '<th class="column-title">Serial number</th>';
            table += '<th class="column-title">หมายเหตุ</th>';
            table += '</tr>';
            table += '</thead>';
            table += '<tbody>';
            $.each(res, function( index, value ) {
                var jsonRes = JSON.stringify(value);
                var serils = (value.serial_number == null)?'':value.serial_number;
                var remarks = (value.remark == null)?'':value.remark;
                var rows = index+1;
                table += '<tr>';
                table += '<td style="width:5%;text-align: center;">'+rows+'</td>';
                table += '<td style="width:20%;"><input id="serial'+value.id+'" name="serial'+value.id+'" type="text" class="form-control serials-res" placeholder="..." value="'+serils+'"></td>';
                table += '<td style="width:40%;"><input id="remark'+value.id+'" name="remark'+value.id+'" type="text" class="form-control remark-res" placeholder="..." value="'+remarks+'"></td>';
                table += '</tr>';
            });
            table += '</tbody>';
            table += '</table>';
            table += '</div>';
        return table;
    }
    $(".edit-serial-submit").click(function(e) {
        var attr_serial = $(this).attr('attr-data-id');
        var serials = JSON.parse(attr_serial);
        var arr = [];
        $.each(serials, function( index, value ) {
            var res = {
                "id" : value.id,
                "serial_number":$('#serial'+value.id).val(),
                "remark":$('#remark'+value.id).val()
            };
            arr.push(res);
        }); 
        $.ajax({
            url: base_url+"admin/ContractAddon/EditSerialAjax",
            data:{ 'obj': arr } ,
            dataType: 'json',
            type: "POST",
            async:false,
            success: function(data)
            {
                $('.edit-serial-submit').prop('disabled', true);// disable ปุ่มบันทึก
                if(data.status){
                    Swal.fire({
                        title: data.data,
                        //text: "You won't be able to revert this!",
                        icon: 'success',
                        //showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        //cancelButtonColor: '#d33',
                        confirmButtonText: 'ตกลง'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#EditSerial-modal').modal('hide');
                        }
                    });
                }else{
                    Swal.fire(data.data, '', 'info');
                }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
    });

    //ลบ Serial number
    $('.serial-del-btn').click(function(){
        var contract_code = $(this).attr('data-contractCode');
        var res = $(this).attr('data-attr');
        var resObj = JSON.parse(res);
        var getRes = getResSerials(contract_code, res);
        if(getRes.length > 0){
            Swal.fire({
                title: 'คุณมั่นไหมที่จะลบ Serial number นี้?',
                text: "Serial number ของ"+resObj.product_master_name+" จะถูกลบทั้งหมด.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ไม่ใช่'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+"admin/ContractAddon/DelSerialAjax",
                        data:{ 
                            'contract_code': contract_code,
                            'product_id': resObj.product_id,
                            'product_sub_id': resObj.product_sub_id,
                        } ,
                        dataType: 'json',
                        type: "POST",
                        async:false,
                        success: function(data)
                        {
                            if(data.status){
                                Swal.fire({
                                    title: data.data,
                                    //text: "You won't be able to revert this!",
                                    icon: 'success',
                                    //showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    //cancelButtonColor: '#d33',
                                    confirmButtonText: 'ตกลง'
                                });
                            }else{
                                Swal.fire(data.data, '', 'info');
                            }
                        },
                        error: function(xhr, status, exception) { 
                            console.log(exception);
                        }
                    });
                }
            });
        }else{
            Swal.fire({
                title: "ไม่มีข้อมูล Serial number ในระบบ",
                //text: "You won't be able to revert this!",
                icon: 'warning',
                //showCancelButton: true,
                confirmButtonColor: '#3085d6',
                //cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง'
            });
        }
    });
</script>
    