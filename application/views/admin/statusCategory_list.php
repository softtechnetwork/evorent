
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>หมวดหมู่ ของสถานะ <small></small></h2>
        <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
        <ul class="nav navbar-right panel_toolbox">
            <!--
            <li style="margin-right: 3px;">
                <select class="form-control" id="product_brand" required="required" >
                    <option value="">ยี่ห้อสินค้า</option>
                    <?php foreach ($productBrand as $item) : ?>
                        <option value="<?= $item->brand_id;?>"><?= $item->brand_name;?></option>
                    <?php endforeach ?>
                </select>
            </li>
            <li style="margin-right: 3px;">
                <select class="form-control" id="product_cate" required="required" >
                    <option value="">ประเภทสินค้า</option>
                    <?php foreach ($productCate as $item) : ?>
                        <option value="<?= $item->cate_id;?>"><?= $item->cate_name;?></option>
                    <?php endforeach ?>
                </select>
            </li>
            <li style="margin-right: 3px;">
                <input id="search_text" type="text"  class="form-control"/>
            </li>
            <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                <i class="fa fa-search"></i> ค้นหา</button>
            </li>
            <li>
                <button id="button-reset" type='button' class="btn btn-warning" style=" margin-right: 15px;border-radius: inherit;">
                <i class="fa fa-refresh"></i> Reset</button>
            </li>
            -->
            <li>
                <a class="btn btn-success" href="<?php echo base_url('admin/statusCategory/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                    <i class="fa fa-plus"></i> หมวดหมู่ ของสถานะ 
                </a>
            </li>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
            <tr class="headings">
                <th class="column-title">ลำดับ</th>
                <th class="column-title">รหัส </th>
                <th class="column-title">หมวดหมู่ ของสถานะ </th>
                <th class="column-title">รายละเอียด </th>
                
                <th class="column-title no-link last" style="text-align: center;"><span class="nobr">Action</span></th>
                <!--<th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                </th>-->
            </tr>
            </thead>

            <tbody>
                <?php //print_r( count($res_customer)); ?>
                <?php  
                $x = 1;
                foreach($res as $items){?> 
                    
                    <tr class="even pointer">
                        <td class="a-center "><?php echo $x; ?></td>
                        <td class=" "><?php echo $items->status_category_code;  ?></td>
                        <td class=" "><?php echo $items->label;  ?></td>
                        <td class=" "><?php echo $items->detail;  ?></td>
                        <td class=" last"  style="text-align: center;">
                            <a href="<?php echo base_url('/admin/statusCategory/edit/').$items->status_category_code;?>">
                                <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>
                            </a>
                        </td>
                    </tr>
                    
                <?php $x++; } ?>
            
            
            </tbody>
        </table>
        </div>
                
            
    </div>
    </div>
</div>
<!---- End Content ------>
  