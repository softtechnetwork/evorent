<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รายละเอียดการซ่อมบำรุง <small><?=$serviceRES[0]->contract_code;?></small></h2>
                <ul class="nav navbar-right panel_toolbox" style=" min-width: inherit;">
                    <li>
                        <button id="add-service-btn"  class="btn btn-success"  style="color: #ffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                            <i class="fa fa-plus"></i> เพิ่มการให้บริการ
                        </button>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php //print_r( $serviceRES[0]); ?>
                <div class="row invoice-info">
                    <div class="col-6 invoice-col">
                        <div class="row">
                            <div class="col-6 invoice-col">
                                <address>
                                    <h5><strong>ข้อมูลลูกค้า</strong></h5>
                                    <strong>รหัส :</strong> <?=$serviceRES[0]->customer_code;?>
                                    <br><strong>หมายเลขบัตรประชาชน :</strong> <?=$serviceRES[0]->idcard;?>
                                    <br><strong>ชื่อ :</strong> <?=$serviceRES[0]->firstname;?> <?=$serviceRES[0]->lastname;?>
                                    <br><strong>Phone :</strong> <?=$serviceRES[0]->tel;?> 
                                    <br><strong>Email :</strong> <?=$serviceRES[0]->email;?>
                                </address>
                            </div>
                            <div class="col-6 invoice-col">
                                <address>
                                    <br><strong>ที่อยู่ปัจจุบัน :</strong> <?=$address[0]->address;?>
                                    <br><strong>ตำบล / แขวง :</strong> <?=$address[0]->district_name;?> 
                                    <br><strong>อำเภอ / เขต :</strong> <?=$address[0]->amphur_name;?> 
                                    <br><strong>จังหวัด :</strong> <?=$address[0]->province_name;?>
                                    <br><strong>รหัสไปรษณีย์ :</strong> <?=$address[0]->zip_code;?>
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 invoice-col">
                        <div class="row">
                            <div class="col-6 invoice-col">
                                <address>
                                    <h5><strong>ข้อมูลสินค้า</strong></h5>
                                    <strong>สินค้า :</strong> <?=$serviceRES[0]->product_name;?>
                                    <!--<br><strong>ราคา :</strong> <?=$serviceRES[0]->product_price;?><?php echo str_repeat("&nbsp;", 20);?><strong>จำนวน :</strong> <?=$serviceRES[0]->product_count;?>-->
                                    
                                    <br><strong>ยี่ห้อ :</strong> <?=$serviceRES[0]->brand_name;?>
                                    <br><strong>รุ่น :</strong> <?=$serviceRES[0]->product_version;?>
                                    <br><strong>สถานที่ติดตั้งสินค้า :</strong> <?=$installationLocation[0]->address;?> ตำบล / แขวง  <?=$installationLocation[0]->district_name;?> อำเภอ / เขต <?=$installationLocation[0]->amphur_name;?>  จังหวัด <?=$installationLocation[0]->province_name;?> รหัสไปรษณีย์ <?=$installationLocation[0]->zip_code;?>
                                </address>
                            </div>
                            <div class="col-6 invoice-col">
                                <address>
                                    <br><strong>วันที่เริ่มสัญญา :</strong> <?=$serviceRES[0]->contract_date;?>
                                    <br><strong>วันที่เริ่มชำระงวดแรก :</strong> <?=$serviceRES[0]->payment_start_date;?>
                                    <br><strong>ค่าผ่อนรายเดือน :</strong> <?=$serviceRES[0]->monthly_rent;?>
                                    <br><strong>ระยะเวลาการเช่า :</strong> <?=$serviceRES[0]->rental_period;?> งวด <?php echo str_repeat("&nbsp;", 8);?>เงินดาวน์ : <?=$serviceRES[0]->down_payment;?>
                                    
                                </address>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!---  Modal Edit form    ---->
    <div id="add-form" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <div class="x_title" style="border-bottom: initial;"></div>
                <div class="x_content">
                    <form id="instalment-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('/admin/contractService/insertServiceHistory');?>">
                        
                        <input type="hidden" id="service-id" name="service-id"  value="<?=$serviceRES[0]->service_code;?>">
                        <input type="hidden" id="contract_code" name="contract_code"  value="<?=$serviceRES[0]->contract_code;?>">
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">ชื่อ การให้บริการ</label>
                            <div class="col-md- col-sm-9 ">
                                <input type="text" id="service-history-name" name="service-history-name" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">วันที่ให้บริการ</label>
                            <div class="col-md-9 col-sm-9 ">
                                <input type="date" id="service-history-date" name="service-history-date" required="required" class="form-control">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">ID ช่าง</label>
                            <div class="col-md- col-sm-9 ">
                                <input type="text" id="service-history-tachn" name="service-history-tachn" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">หมายเหตุ</label>
                            <div class="col-md-9 col-sm-9 ">
                                <textarea  class="form-control" name='service-history-remark'></textarea>
                            </div>
                        </div>
                        
                        <div class="ln_solid"></div>
                        <div class="item form-group">
                            <div class="col-md-12 col-sm-12 label-align">
                                <button class="btn btn-primary" type="button" data-dismiss="modal" >Cancel</button>
                                <button id="installment-btm-submit" type="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
    
</div>
<?php //print_r($history); ?>
<?php if($history != null){?>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title"><h2>ประวัติการให้บริการ<small></small></h2> <div class="clearfix"></div></div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table id="loan_res" class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">ลำดับ</th>
                                    <th class="column-title">ชื่อ การให้บริการ </th>
                                    <th class="column-title">วันที่ให้บริการ </th>
                                    <th class="column-title">ID ช่าง </th>
                                    <th class="column-title">หมายเหตุ</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php  $nume = 1; foreach($history as $item){?> 
                                    <tr class="even pointer">
                                        <td class="a-center "><?=$nume;?></td>
                                        <td class="a-center "><?=$item->name;?></td>
                                        <td class=" "><?=$item->service_date;?></td>
                                        <td class=" "><?=$item->service_by;?></td>
                                        <td class=" "><?=$item->remark;?></td>
                                    </tr>
                                <?php $nume++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    $('#add-service-btn').click(function (e) {
        $('#add-form').modal('show');
        //$('#inst-period').val(index);
    });
</script>

