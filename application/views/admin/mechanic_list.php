<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
    .gettopdf{

    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลช่าง</h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="btn" href="<?=base_url('admin/Mechanic/create');?>"  style="color: #466889;"><i class="fa fa-plus"></i> เพิ่มข้อมูลลูกค้า</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">รหัสช่าง </th>
                        <th class="column-title">ชื่อ </th>
                        <th class="column-title">เลขบัตรประชาชน</th>
                        <th class="column-title">โทรศัพท์มือถือ</th>
                        <th class="column-title">อีเมล์ </th>
                        <th class="column-title" style="text-align:center; width: 13rem">กิจกรรม</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                var t = key+1;
                tr += '<tr>';
                tr += '<td>'+t+'</td>';
                tr += '<td>'+val['charng_code']+'</td>';
                tr += '<td>'+val['name']+' '+val['sname']+'</td>';
                tr += '<td>'+val['idcard']+'</td>';
                tr += '<td>'+val['tel']+'</td>';
                tr += '<td>'+val['email']+'</td>';
                tr += '<td class=" last"  style="text-align: center;">';
                tr +=   '<a href="'+base_url+'admin/Mechanic/edit/'+val['charng_code']+' "  data-toggle="tooltip" title="แก้ไข">';
                tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                tr +=   '</a>';
                tr +=   '<a href="'+base_url+'admin/Mechanic/detail/'+val['charng_code']+' " data-toggle="tooltip" title="รายละเอียด">';
                tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i></button>';
                tr +=   '</a>';
                tr +=   '<a href="'+base_url+'admin/Mechanic/PDF/'+val['charng_code']+' " target="_blank" style="padding-right: 3px;padding-left: 3px;" data-toggle="tooltip" title="PDF">';
                tr +=       '<button type="button" class="btn btn-round btn-dark" style="width: inherit; font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-print"></i></button>';
                tr +=   '</a>';
                tr += '</td>';
                tr += '</tr>';
            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [50, 100], [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/Mechanic/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }


</script>
<!---- End Content ------>
