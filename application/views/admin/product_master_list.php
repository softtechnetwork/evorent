<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>มาสเตอร์สินค้า <small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <!-- <li style="margin-right: 3px;">
                    <select class="form-control" id="product_master_brand" required="required" >
                        <option value="">ยี่ห้อสินค้า</option>
                        <?php foreach ($productBrand as $item) : ?>
                            <option value="<?= $item->brand_id;?>"><?= $item->brand_name;?></option>
                        <?php endforeach ?>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <select class="form-control" id="product_master_cate" required="required" >
                        <option value="">ประเภทสินค้า</option>
                        <?php foreach ($productCate as $item) : ?>
                            <option value="<?= $item->cate_id;?>"><?= $item->cate_name;?></option>
                        <?php endforeach ?>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <input id="search_text" type="text"  class="form-control"/>
                </li>
                <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                    <i class="fa fa-search"></i> ค้นหา</button>
                </li>
                <li>
                    <button id="button-reset" type='button' class="btn btn-warning" style=" margin-right: 15px;border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li> -->
                <li>
                    <a class="btn btn-success" href="<?php echo base_url('admin/productMaster/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มสินค้า
                    </a>
                </li>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <!-- <table id="product-table" class="table table-striped jambo_table bulk_action" > -->
                <table id="tables" class="table table-striped jambo_table bulk_action"  style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 2rem">ลำดับ</th>
                            <th class="column-title" style="width: 4rem">รหัส</th>
                            <th class="column-title">สินค้า </th>
                            <th class="column-title">ราคา </th>
                            <th class="column-title">ยี่ห้อสินค้า </th>
                            <th class="column-title">ประเภทสินค้า </th>
                            <!--<th class="column-title">รายละเอียด </th>-->
                            <th class="column-title no-link last" style="text-align: center;width:5rem;"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    // var base_url = $('input[name="base_url"]').val();
    // var searchEle = ['#product_master_brand', '#product_master_cate', '#search_text'];
    // DrawMasterProductList(base_url, searchEle, '#product-table tbody');
    // SearchMasterProductList(base_url, '#button-search', searchEle, '#product-table tbody');
    // ResetMasterProductList(base_url, '#button-reset', searchEle, '#product-table tbody');

    var base_url = $('input[name="base_url"]').val();
    DrawTable(base_url, '#tables');
    function DrawTable(base_url, replacePosition){
        var res = getProducts(base_url);
        if(res){ 
            var elements = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                var no = i+1;
                var agrmt = ["'"+base_url+"', '"+val.product_master_id+"'"];
                elements += '<tr>';
                elements += '   <td class="">'+no+'</td>';
                elements += '   <td class="">'+val.product_master_id+'</td>';
                elements += '   <td class="">'+val.product_master_name+'</td>';
                elements += '   <td class="">'+val.product_master_price+'</td>';
                elements += '   <td class="">'+val.brand_name+'</td>';
                elements += '   <td class="">'+val.cate_name+'</td>';
                //elements += '   <td class="">'+val.product_master_detail+'</td>';
                elements += '   <td class="" style="text-align: center;">';
                elements += '       <a href="'+base_url+'admin/productMaster/edit/'+val.product_master_id+' ">';
                elements += '           <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                elements += '           <i class="fa fa-wrench"></i> แก้ไข</button>';
                elements += '       </a>';
                //elements += '       <a href="'+base_url+'admin/productCategory/delete/'+val['id']+' ">';
                // elements += '           <button type="button" class="btn btn-round btn-danger" onclick="delMasterProduct('+agrmt+')" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                // elements += '           <i class="fa fa-times"></i> ลบ</button>';
                //elements += '       </a>';
                elements += '   </td>';
                elements += '</tr>';
            });
            $(body).append(elements);
            
            // Datatable
            $(replacePosition).dataTable({
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function getProducts(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/ProductMaster/ProductMaster_get", //ทำงานกับไฟล์นี้
            //data:  {'Search':search},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
</script>



<!---- End Content ------>
  