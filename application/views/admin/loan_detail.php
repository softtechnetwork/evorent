<style>
    .col-md-66{
        position: relative;
        min-height: 1px;
        float: left;
        padding-right: 10px;
        padding-left: 10px;
        width: 50%;
        margin-bottom: 10px;
    }
    .panel_toolbox>li> .btn-dark { color: #E9EDEF !important; }
    .panel_toolbox>li> .btn-dark:hover { background: #0a2946  !important; }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>รายละเอียดสัญญาลูกค้า <small><?=$res[0]->temp_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row invoice-info">
                    <div class="col-5 invoice-col">
                        <!--<address>
                            <strong>ข้อมูลลูกค้า</strong>
                            <br>รหัส : <?=$res[0]->customer_code;?>
                            <br>หมายเลขบัตรประชาชน : <?=$res[0]->idcard;?>
                            <br>ชื่อ : <?=$res[0]->firstname;?> <?=$res[0]->lastname;?>
                            <br>ที่อยู่ : <?=$res[0]->addr;?>
                            <br>ตำบล / แขวง : <?=$res[0]->district_name;?> 
                            <br>อำเภอ / เขต : <?=$res[0]->amphur_name;?> 
                            <br>จังหวัด : <?=$res[0]->province_name;?>
                            <br>รหัสไปรษณีย์ : <?=$res[0]->zip_code;?>
                            <br>Phone : <?=$res[0]->tel;?> 
                            <br>Email : <?=$res[0]->email;?>
                        </address>-->
                        <address>
                            <h5><strong>ข้อมูลลูกค้า</strong></h5>
                            <strong>รหัส :</strong> <?=$res[0]->customer_code;?>
                            <br><strong>หมายเลขบัตรประชาชน :</strong> <?=$res[0]->idcard;?>
                            <br><strong>ชื่อ :</strong> <?=$res[0]->firstname;?> <?=$res[0]->lastname;?>
                            <br><strong>ที่อยู่ปัจจุบัน :</strong> <?=$address[0]->address;?>
                            <br><strong>ตำบล / แขวง :</strong> <?=$address[0]->district_name;?> 
                            <br><strong>อำเภอ / เขต :</strong> <?=$address[0]->amphur_name;?> 
                            <br><strong>จังหวัด :</strong> <?=$address[0]->province_name;?>
                            <br><strong>รหัสไปรษณีย์ :</strong> <?=$address[0]->zip_code;?>
                            <br><strong>Phone :</strong> <?=$res[0]->tel;?> 
                            <br><strong>Email :</strong> <?=$res[0]->email;?> 
                        </address>
                    </div>
                    <div class="col-4 invoice-col">
                        <!--<address>
                            <strong>ข้อมูลสินค้า</strong>
                            <br>สินค้า : <?=$res[0]->piname;?>
                            <br>ราคา : <?=$res[0]->product_price;?>
                            <br>จำนวน : <?=$res[0]->product_count;?>
                            <br>ยี่ห้อ : <?=$res[0]->product_brand;?>
                            <br>รุ่น : <?=$res[0]->product_version;?>
                            <br>วันที่เริ่มสัญญา : <?=$res[0]->contract_date;?>
                            <br>วันที่เริ่มชำระงวดแรก : <?=$res[0]->payment_start_date;?>
                            <br>ค่าผ่อนรายเดือน : <?=$res[0]->monthly_rent;?>
                            <br>ระยะเวลาการเช่า : <?=$res[0]->rental_period;?> งวด
                            <br>เงินดาวน์ : <?=$res[0]->down_payment;?>
                            <br>สถานะ : <span style=" padding: 0px 25px; color: <?=$res[0]->color;?>; background-color: <?=$res[0]->background_color;?>;"><?=$res[0]->label;?></span>
                        </address>-->
                        <address>
                            <h5><strong>ข้อมูลสินค้า</strong></h5>
                            <strong>สินค้า :</strong> <?=$res[0]->product_name;?>
                            <!--<br><strong>ราคา :</strong> <?//=$res[0]->product_price;?><?php //echo str_repeat("&nbsp;", 20);?><strong>จำนวน :</strong> <?=$res[0]->product_count;?>-->
                            
                            <br><strong>ยี่ห้อ :</strong> <?=$res[0]->brand_name;?>
                            <br><strong>รุ่น :</strong> <?=$res[0]->product_version;?>
                            <br><strong>วันที่เริ่มสัญญา :</strong> <?=$res[0]->contract_date;?>
                            <br><strong>วันที่เริ่มชำระงวดแรก :</strong> <?=$res[0]->payment_start_date;?>
                            <br><strong>ค่าผ่อนรายเดือน :</strong> <?=$res[0]->monthly_rent;?>
                            <br><strong>ระยะเวลาการเช่า :</strong> <?=$res[0]->rental_period;?> งวด <?php echo str_repeat("&nbsp;", 8);?>เงินดาวน์ : <?=$res[0]->down_payment;?>
                            <br><strong>สถานะ :</strong> <span style=" padding: 0px 25px; color: <?=$res[0]->color;?>; background-color: <?=$res[0]->background_color;?>;"><?=$res[0]->label;?></span>
                            <br><strong>สถานที่ติดตั้งสินค้า :</strong> <?=$installationLocation[0]->address;?> ตำบล / แขวง  <?=$installationLocation[0]->district_name;?> อำเภอ / เขต <?=$installationLocation[0]->amphur_name;?>  จังหวัด <?=$installationLocation[0]->province_name;?> รหัสไปรษณีย์ <?=$installationLocation[0]->zip_code;?>
                        </address>
                    </div>
                    <div class="col-3 invoice-col">
                        <?php 
                        $supporter_fname = null; $supporter_lname = null;  $supporter_tel = null;
                        if($supporter != null){
                            $supporter_fname = $supporter[0]->fname;
                            $supporter_lname = $supporter[0]->lname;
                            $supporter_tel = $supporter[0]->tel;
                        }
                        ?>
                        <address>
                            <strong>ข้อมูลผู้คำ้ประกัน</strong>
                            <br>ชื่อ : <?=$supporter_fname;?> <?=$supporter_lname;?> 
                            <br>Phone : <?=$supporter_tel;?>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ตารางการผ่อนชำระ<!--<small>Sample user invoice design</small>--> </h2>
                
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <!---  Modal view premise    ---->
                <div id="view-premise" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="x_title" style="border-bottom: initial;margin-bottom: inherit;padding: 10px 3px 0px 10px;">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title"></h4>
                            </div>
                            <div class="x_content" id="premies-content"></div>
                        </div>
                    </div>
                </div>


                <!-- Table row -->
                <?php  //print_r($idcart); print_r('<br>');  print_r($res[0]); ?>
                
                <div class="row">
                    <div class="table">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">งวด</th>
                                    <th style="text-align: center;">ราคาผ่อนบวก ภาษีมูลค่าเพิ่ม</th>
                                    <th style="text-align: center;">วันที่ครบกำหนดชำระ</th>
                                    <th style="text-align: center;">สถานะ</th><!-- ยังไม่จ่าย,จ่ายแล้ว, จ่ายล่าช้า--->
                                    <th style="text-align: center;">จำนวนที่ชำระ</th><!--จำนวนที่จ่าย--->
                                    <th style="text-align: center;">วันที่ชำระ</th>
                                    <th>หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($resInst as $key => $value) {
                                $installment_date = null;
                                if($value->installment_date != null){ $installment_date = date_format(date_create($value->installment_date),"d-m-Y"); }
                                ?>
                                <tr>
                                    <td style="text-align: center;"><?=$value->period; ?></td>
                                    <td style="text-align: center;"><?=$value->installment_payment; ?></td>
                                    <td style="text-align: center;"><?=date_format(date_create($value->payment_duedate),"d-m-Y") ?></td>
                                    <td>
                                        <div style="text-align: center; color:<?=$value->color;?>;background-color:<?=$value->background_color;?>;">
                                            <?=$value->status_label;?>
                                        </div>
                                    </td>
                                    <td style="text-align: center;"><?=$value->payment_amount; ?></td>
                                    <td style="text-align: center;"><?=$installment_date; ?></td>
                                    <td><?=$value->remark; ?></td>
                                </tr>
                            <?php  } ?>
                            </tbody>
                        </table>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $(" img ").click(function(e){
    $('#premies-content').html(null);
    $('.modal-title').html(null);
    var id = this.id;
    var path = $('#'+id).attr('src');
    
    $('#view-premise').modal('show');
    
    var img = "<img id='view-"+id+"' class=''  src='"+path+"' alt='image' style='width: 100%; border: 2px solid #80808040; border-radius: 7px;'/>";
    $('#premies-content').prepend(img);
    var t = $ ('#'+id).parents();
    $('.modal-title').prepend(t[0].outerText);
  });
</script>