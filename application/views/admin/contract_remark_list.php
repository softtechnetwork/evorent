<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
    <div class="x_panel">
        <div class="x_title">
            <h2>หมายเหตุ<small></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="btn" href="<?=base_url('admin/ContractRemark/create');?>"  style="color: #466889;"><i class="fa fa-plus"></i> เพิ่ม หมายเหตุ</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="table" class="table table-striped jambo_table bulk_action" style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 1rem">ลำดับ</th>
                            <th class="column-title" style="width: 2rem">สัญญา</th>
                            <th class="column-title">หมายเหตุ </th>
                            <th class="column-title" style="width: 7rem">แอดมิน </th>
                            <th class="column-title" style="width: 7rem">วันที่เพิ่ม </th>
                            <th class="column-title" style="width: 6rem">วันที่แก้ไข </th>
                            <th class="column-title no-link last" style="text-align: center;width: 8rem;"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- action Modal -->
<div class="modal fade" id="actionModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <form id="action-form" class="" action="<?//=site_url('/admin/ContractRemark/action');?>" method="post"  enctype="multipart/form-data" novalidate>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">จัดการ หมายเหตุ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" name="id"  value="<?=base_url();?>">
                <input type="hidden" name="action" value="insert">
                
                <div class="field item form-group">
                    <div class="col-md-6 col-sm-6">
                        <label class="label-align">สัญญา</label>
                        <select name='contract_code' id='contract_code' class="selectpicker form-control mh" required="required" >
                            <?php foreach ($contract as $item) : ?>
                                <option value="<?=$item->contract_code; ?>"><?=$item->contract_code; ?></option>
                                <?php endforeach ?>
                            </select>
                        </select>
                    </div>
                </div>
                <div class="field item form-group">
                    <div class="col-md-12 col-sm-12">
                    <label class="label-align">หมายเหตุ</label>
                        <textarea  class="form-control" required="required" id='remark' name='remark' cols="30" rows="5"></textarea>
                    </div>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="submit" class="btn btn-primary">บันทุก</button>
            </div>
        </form>
    </div>
  </div>
</div>



<script>

    var base_url = $('input[name="base_url"]').val();
    drawtable(base_url);
    function drawtable(base_url){
        var results = get_results(base_url);
        if(results.datas.length > 0){   
            var tr = '';
            $.each( results.datas, function( key, val ) {
                
                tr += '<tr class=" ">';
                tr += '<td class=" ">'+(key+1)+'</td>';
                tr += '<td class=" ">'+val.contract_code+'</td>';
                tr += '<td class=" ">'+val.remark+'</td>';
                tr += '<td class=" ">'+((val.admin != '' && val.admin != null)?val.admin : '')+'</td>';
                tr += '<td class=" ">'+val.create_date+'</td>';
                tr += '<td class=" ">'+val.update_date+'</td>';
                tr += '   <td class="" style="text-align: center;">';
                tr += '       <a href="'+base_url+'admin/ContractRemark/edit/'+val.id+' ">';
                tr += '           <button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">';
                tr += '           <i class="fa fa-wrench"></i></button>';
                tr += '       </a>';
                tr += '   </td>';
                tr += '</tr>';
                // var str = JSON.stringify(val);
                // $('button#btn'+key).data("res",str);
                // $('button#btn'+key).attr('onclick', 'ssss');

            });
            var tableid = '#table';
            $(tableid+' tbody').html(null);
            $(tableid+' tbody').append(tr);
            $(tableid).dataTable({
                lengthMenu: [
                    [50, 100], [50, 100],
                ],
                // "aoColumnDefs": [
                //     { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                //     { "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                // ]
            });
        }
    }
    function get_results(base_url){
        var res = null;
        $.ajax({
            url: base_url+'/admin/ContractRemark/get_resulted', //ทำงานกับไฟล์นี้
            data: '',  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
        return res;
    }
</script>


<!---- End Content ------>
  