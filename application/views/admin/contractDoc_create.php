<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างเอกสาร สัญญา</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSet-create" action="<?php echo site_url('/admin/contractDoc/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท สัญญา(เอกสารสัญญา)</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="contract_type"  id="contract_type">
                                <?php foreach ($contractType as $item) : ?>
                                    <option value="<?=$item->id;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="province-current"> ชื่อ (เอกสารสัญญา)</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="contract_name" id="contract_name"/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="province-current">เอกสารสัญญา</label>
                        <div class="form-group drop-files col-md-6 col-sm-6">
                            <input type="file" name="contract_file[]" class="form-control" multiple="">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด (เอกสารสัญญา)</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="contract_detail" name="contract_detail" class="form-control" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สถานะการใช้งาน</label>
                        <div class="col-md-2 col-sm-2">
                            <select class="form-control" name="use_status"  id="use_status">
                                <?php foreach ($useStatus as $item) : ?>
                                    <option value="<?=$item->id;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/contractDoc');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">สร้าง (เอกสารสัญญา)</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
