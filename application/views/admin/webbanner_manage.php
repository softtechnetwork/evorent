<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }

    .img-preview-action{position: absolute; text-align: center; top: 0; left: 0; height: 100%; width: 100%; z-index: 2; opacity: 0; background-color: rgba(0, 0, 0, 0.36);}
    .img-preview-action:hover { opacity: 100; }
    .img-preview-action > ul > li {
        cursor: pointer;
        padding: 5px;
        float: left;
    }
    .img-preview-action > ul {
        margin: 0;
        padding-left: 0;
        text-align: center;
        padding-top: 40px;
        list-style: none;
        display: inline-block;
    }
    div.cazary {border: 1px solid #ced4da !important;}
        
</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Banner </h2><small><?//=$banner_id;?><span id='product-header'></span></small> 
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <input name="banner_id" value="<?=$banner_id;?>" type="hidden" >
            <!-- <ul class="nav navbar-right panel_toolbox"> -->
            <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
            <!-- </ul> -->
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal form-label-left" action="<?=base_url('admin/WebBanner/banner_actions');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id" value="<?=$banner_id;?>">
                            <input type="hidden" name="method" id="method" value="insert">

                            <div class="col-sm-3">
                                <div class="col-sm-12">
                                    <div class="" style="vertical-align: middle; width: 100%; height: auto; border: 1px dashed rgba(103, 103, 103, 0.39); padding: 5px; margin: 5px; display: inline-block;">
                                        <div class="" style="    position: relative; height: 100%; width: 100%; display: flex; align-items: center; justify-content: center; text-align: center;">
                                            <img id="img" class="thumnails-premise img-add" src="<?=base_url('/uploaded/DocumentTh.png');?>" alt="image" style="border: unset; width: 100%; border-radius: unset;display: inline-block;" />
                                        </div>
                                    </div>
                                    <div class="form-group row" style="margin-top: .5rem;">
                                        <div class="col-sm-12">
                                            <input id="input-img" name="input-img" type="file" style="display:none;" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="name">ชื่อ <span class="required">*</span></label>
                                        <input class="form-control" type="text" name="title" id="title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="title">ลิ้งค์ <span class="required">*</span></label>
                                        <input class="form-control" type="text" name="link" id="link">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="first-name">รายละเอียด <span class="required">*</span></label>
                                        <textarea class="form-control" rows="15" id="detail" name="detail"></textarea>
                                    </div>
                                </div>
                            
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="control-label ">สถานะ การแสดงบนหน้าเว็บ</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="0">ไม่ แสดงบนหน้าเว็บ</option>
                                            <option value="1">แสดงบนหน้าเว็บ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <a href="<?=base_url('admin/WebBanner/')?>"><button type="button" class="btn btn-warning">กลับ</button></a>
                                        <!-- <button type="button" class="btn btn-warning"  id="back-btn">กลับ</button> -->
                                        <button type="submit" class="btn btn-success"  id="save-btn">บันทึก</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
  
  <script>
    var base_url = $('input[name="base_url"]').val();
    var banner_id = $('input[name="banner_id"]').val();

    CKEDITOR.replace('detail');
    draw_form(base_url, banner_id);
    function draw_form(base_url, banner_id){
        var res = null;
        $.ajax({
            url: base_url+'admin/WebBanner/banner_get_once', //ทำงานกับไฟล์นี้
            data: {
                "banner_id" : banner_id
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });
        
        if(res.length > 0){
            $("#id" ).val(res[0].id);
            $("#method" ).val('update');
            $("#img").attr("src", base_url+res[0].img+'?random='+Math.random());
            //$('#img').attr("src", '/Content/img/disable.png');
            $("#title" ).val(res[0].name);
            $("#link" ).val(res[0].link);
            $("#detail" ).val(res[0].detail);
            $("#status").val(res[0].display_status).change();

        }else{
            //console.log('xxxxxxxxxxxx');
        }
        return res;
    }

    $('.img-add').click(function () {
        var attr = $(this)['context']['attributes'];
        var files = 'input-img';
        $("#input-img").trigger('click');
        $("#input-img").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.img-add').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
                //$('form'+form_id).submit();
            }
        });
    });

    // $( "#save-btn" ).click(function() {
    //     var base_url = $('input[name="base_url"]').val();
    //     var id = $("#id").val();
    //     var method = $("#method").val();
    //     var title = $("#title").val();
    //     var link = $("#link").val();
    //     var detail = CKEDITOR.instances['detail'].getData();
    //     var status = $("#status").val();
    //     var img = $("#input-img").val();

    //          actions(base_url, id, method, img, title, link. detail, status);
    // });
    // function actions(base_url, id, method, img, title, link, detail, status){
    //     console.log(base_url, id, method, img, title, link, detail, status);
    //     $.ajax({
    //         url: base_url+'admin/webBanner/banner_actions', //ทำงานกับไฟล์นี้
    //         data: {
    //             "id" : id,
    //             "method" : method,
    //             "img" : img,
    //             "title" : title,
    //             "link" : link,
    //             "detail" : detail,
    //             "status" : status
    //         },  //ส่งตัวแปร
    //         type: "POST",
    //         dataType: 'json',
    //         async:false,
    //         success: function(data, status) {
    //             //res = data;
    //             window.location=data['datas'];
    //         },
    //         error: function(xhr, status, exception) {  console.log(exception); }
    //     });
    // }

  </script>



<!---- End Content ------>
  