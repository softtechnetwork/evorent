<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้าง<small>หมวดหมู่ของสถานะ</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="" action="<?php echo site_url('/admin/statusCategory/insert');?>" method="post"  enctype="multipart/form-data" >
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมวดหมู่ ของสถานะ<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" name="stcate" placeholder="" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" name='stdetail'></textarea>
                        </div>
                    </div>

                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('/admin/statusCategory');?>" class="btn btn-primary">กลับ</a>
                                <button type='submit' class="btn btn-success">สร้างหมวดหมู่ของสถานะ</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>