<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
    .gettopdf{

    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>รายงานการค้างชำระ</h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <!-- <div class ="col-md-2">
                    <label for="customer_code">รหัสลูกค้า</label>
                    <select id="customer_code" name="customer_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสลูกค้า</option>
                        <?php  foreach($getCustomer as $item) :?>
                            <option value="<?php echo $item->customer_code;?>"> <?php echo $item->customer_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div> -->
                <!-- <div class ="col-md-3">
                    <label for="reportName">ชื่อลูกค้า</label>
                    <input id="reportName" name="reportName"  class="form-control" value="">
                </div> -->
                <div class ="col-md-3">
                    <label for="customer_code">ลูกค้า</label>
                    <select id="customer_code" name="customer_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก ลูกค้า</option>
                        <?php  foreach($getCustomer as $item) :?>
                            <option value="<?=$item->customer_code;?>"> <?=$item->firstname;?>  <?=$item->lastname;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <div class ="col-md-2" id="contract_code_position">
                    <label for="contract_code">รหัสสัญญา</label>
                    <select id="contract_code" name="contract_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก รหัสสัญญา</option>
                        <?php  foreach($getTemp as $item) :?>
                            <option value="<?php echo $item->contract_code;?>"> <?php echo $item->contract_code;?></option>
                        <?php endforeach  ?>
                    </select>
                </div>
                <div class ="col-md-2">
                    <label for="status_code">สถานะ</label>
                    <select id="status_code" name="status_code" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก สถานะ</option>
                        <?php  foreach($getStatus as $item) :?>
                            <option value="<?php echo $item->status_code;?>"> <?php echo $item->label;?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="report-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>

                <!-- <div class ="col-md-2">
                    <label for="overdue">การค้างชำระ</label>
                    <select id="overdue" name="overdue" class="selectpicker form-control mh"  data-live-search="true">
                        <option value="">เลือก การค้างชำระ</option>
                        <option value="x">ค้างชำระ</option>
                        <option value="xx">ครบกำหนดชำระแล้ว</option>
                        <option value="i">ก่อนครบกำหนดชำระ 1 วัน</option>
                        <option value="ii">ก่อนครบกำหนดชำระ 2 วัน</option>
                        <option value="iii">ก่อนครบกำหนดชำระ 3 วัน</option>
                        <option value="iv">ก่อนครบกำหนดชำระ 4 วัน</option>
                        <option value="v">ก่อนครบกำหนดชำระ 5 วัน</option>
                    </select>
                </div> -->

            </div>
            <!-- <div class ="row ">
                <div class ="col-md-5">
                    <label for="reservation">วันที่ครบกำหนดชำระ</label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stduedate" id="stduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="enduedate" id="enduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                    </div>
                </div> -->
                <!-- <div class ="col-md-5">
                    <label for="reservation">วันที่ชำระ</label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stinstallmentdate" id="stinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="endinstallmentdate" id="endinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                    </div>
                </div>
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="report-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="report-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
                <div class ="col-md-10">
                    <div class ="row ">
                        <div class ="col-md-12  navbar-right " style=" padding-top: 1.65rem; text-align: right;">
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <form id="pdfExport-form" method="post" action="<?=base_url();?>admin/report/exportOverDueToPDF/" target="_blank">
                                        <input type="hidden" id="customer_code" name="customer_code">
                                        <input type="hidden" id="contract_code" name="contract_code">
                                        <input type="hidden" id="status_code" name="status_code">
                                        <button id="pdfExport-btn" type='button' class="btn btn-dark" style="border-radius: inherit;">
                                            <i class="fa fa-file-pdf-o"></i> PDF
                                        </button>
                                    </form>
                                </li>
                                <li>
                                    <form id="excelExport-form" method="post" action="<?=base_url();?>admin/report/exportOverDueToExcel/" target="_blank">
                                        <input type="hidden" id="customer_code" name="customer_code">
                                        <input type="hidden" id="contract_code" name="contract_code">
                                        <input type="hidden" id="status_code" name="status_code">
                                        <button id="excelExport-btn" type='button' class="btn btn-dark" style="border-radius: inherit;">
                                            <i class="fa fa-file-excel-o"></i> EXCEL
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div> -->
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>ตารางการผ่อนชำระ</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <form id="pdfExport-form" method="post" action="<?=base_url('admin/Report/exportOverDueToPDF/');?>" target="_blank">
                        <input type="hidden" id="customer_code" name="customer_code">
                        <input type="hidden" id="contract_code" name="contract_code">
                        <input type="hidden" id="status_code" name="status_code">
                        <button id="pdfExport-btn" type='button' class="btn btn-dark" style="border-radius: inherit;">
                            <i class="fa fa-file-pdf-o"></i> PDF
                        </button>
                    </form>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table id="tables" class="table table-striped jambo_table bulk_action"   style="width:100%; border-spacing: 1px !important;">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">ลำดับ</th>
                        <th class="column-title">รหัสลูกค้า </th>
                        <th class="column-title">ลูกค้า </th>
                        <th class="column-title">เลขที่สัญญา</th>
                        <th class="column-title">ราคาผ่อน</th>
                        <th class="column-title">วันที่ครบชำระ</th>
                        <th class="column-title">วันที่สิ้นสุดชำระ</th>
                        <th class="column-title">การค้างชำระ</th>
                        <th class="column-title">จำนวนที่ชำระ </th>
                        <th class="column-title">วันที่ชำระ </th>
                        <th class="column-title" style='text-align: center;'>สถานะ </th>
                        <th class="column-title" style='text-align: center;'>สถานะ สัญญา </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
            $('.next').text(">");
    });
    var base_url = $('input[name="base_url"]').val(); 


    $("select#customer_code").change(function () {
        var val = $(this).val();
        var contract = getcontractBycustomer(base_url,val);
        var option = '';
        if(val == ''){option += '<option value="">เลือก รหัสสัญญา</option>';}
        $.each(contract, function (i, val) {
            option += '<option value="'+val.contract_code+'">'+val.contract_code+'</option>';
        });
        $("#contract_code").html(option);
        $('.selectpicker').selectpicker('refresh');
    });
    
    function getcontractBycustomer(base_url, customer_code){
        var res = null;
        $.ajax({
            url: base_url+"admin/report/getcontractBycustomer", //ทำงานกับไฟล์นี้
            data:  {
                'customer_code': customer_code,
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }

    $("#report-search").click(function () {
        DrawTable(base_url, '#tables');
    });
    $("#report-reset").click(function () {

        $('#customer_code ').val(null);
        $('[data-id=customer_code] .filter-option-inner-inner').html('เลือก รหัสลูกค้า');

        $('#contract_code ').val(null);
        $('[data-id=contract_code] .filter-option-inner-inner').html('เลือก รหัสสัญญา');

        $('#status_code ').val(null);
        $('[data-id=status_code] .filter-option-inner-inner').html('เลือก สถานะ');
        
        $('#overdue').val(null);
        $('[data-id=overdue] .filter-option-inner-inner').html('เลือก การค้างชำระ');
        
        $('#stduedate ').val(null);
        $('#enduedate ').val(null);

        $('#stduedate').removeAttr("min");
        $('#stduedate').removeAttr("max");

        $('#enduedate').removeAttr("min");
        $('#enduedate').removeAttr("max");

        $('#stinstallmentdate ').val(null);
        $('#endinstallmentdate ').val(null);

        $('#endinstallmentdate').removeAttr("min");
        $('#endinstallmentdate').removeAttr("max");

        $('#stinstallmentdate').removeAttr("min");
        $('#stinstallmentdate').removeAttr("max");

        DrawTable(base_url, '#tables');
    });
    
    $("#pdfExport-btn").click(function () {
        DrawTable(base_url, '#tables');
        $('#pdfExport-form #customer_code').val($("#customer_code").val());
        $('#pdfExport-form #contract_code').val($("#contract_code").val());
        $('#pdfExport-form #status_code').val($("#status_code").val());
        $("#pdfExport-form").submit();
    });
    
    $("#excelExport-btn").click(function () {
        DrawTable(base_url, '#tables');
        $('#excelExport-form #customer_code').val($("#customer_code").val());
        $('#excelExport-form #contract_code').val($("#contract_code").val());
        $('#excelExport-form #status_code').val($("#status_code").val());
        $("#excelExport-form").submit();
    });
    
    function formatPDate(date, string) {
        if(date != '' && date != null){
            var temp = date.split('/'); 
            var res = temp[2]+string+temp[1]+string+temp[0];
            return res;
        }else{
            return '';
        }
    }
    
    //DrawTable(base_url, '#tables');
    function DrawTable(base_url, replacePosition){
        $('#tables').DataTable().destroy();
        var res = getdatas(base_url);
        if(res){ 
            var td = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                var styles = '';
                switch(val.overdue_code){
                    case 10: styles =' style="color: #ec0505;" '; break;
                    case 5: styles =' style="color: #ec3e05;" '; break;
                    case 4: styles =' style="color: #ec6805;" '; break;
                    case 3: styles =' style="color: #ec7e05;" '; break;
                    case 2: styles =' style="color: #ec9305;" '; break;
                    case 1: styles =' style="color: #dcbb09;" '; break;
                    default:styles ='';break;
                }
                
                td += "<tr>";
                td += " <td>"+val.RowNum+"</td>";
                td += " <td>"+val.customer_code+"</td>";
                td += " <td>"+val.firstname+" "+val.lastname+"</td>";
                td += " <td>"+val.contract_code+"</td>";
                td += " <td>"+val.installment_payment+"</td>";
                td += " <td>"+ formatDate(val.payment_duedate,'-')+"</td>";
                td += " <td>"+ formatDate(val.extend_duedate,'-')+"</td>";
                td += " <td  "+styles+">"+val.overdue+"</td>";
                var payment_amount = (val.payment_amount == null) ? '' :val.payment_amount;
                td += " <td>"+ payment_amount+"</td>";
                td += " <td>"+ formatDate(val.installment_date,'-')+"</td>";
                td += " <td style='color: #fff; text-align: center;background-color:"+val.background_color+";'>"+val.label+"</td>";
                td += " <td style='color: #fff; text-align: center;background-color:"+val.contract_background_color+";'>"+val.contract_status+"</td>";
                //td += '<td class=" last"  style="text-align: center;">';
                //tr +=   '<a href="'+base_url+'admin/customer/edit/'+val['customer_code']+' ">';
                //tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i> Edit</button>';
                //tr +=   '</a>';
                //td +=   '<a href="'+base_url+'admin/customer/detail/'+val['customer_code']+' ">';
                //td +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i> View</button>';
                //td +=   '</a>';
                td += '</td>';

                td += "</tr>";
            });
            $(body).append(td);
            
            // Datatable
            $(replacePosition).dataTable({
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
        }
    }
    function getdatas(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/report/getOverDueInstallment", //ทำงานกับไฟล์นี้
            data:  {
                'customer_code': $('#customer_code ').val(), 
                'contract_code': $('#contract_code ').val(), 
                'status_code': $('#status_code ').val(), 
                'overdue': $('#overdue ').val(), 
                'stduedate': formatPDate($('#stduedate').val(),'-'),
                'enduedate': formatPDate($('#enduedate').val(),'-'),
                'stinstallmentdate': formatPDate($('#stinstallmentdate').val(),'-'),
                'endinstallmentdate': formatPDate($('#endinstallmentdate').val(),'-')
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
</script>
<!---- End Content ------>
