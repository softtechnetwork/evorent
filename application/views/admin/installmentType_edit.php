<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขประเภทการผ่อนชำระ</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-InstallmentType-edit" action="<?php echo site_url('/admin/installmentType/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" value="<?=$res[0]->product_name;?>" readonly/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จำนวนงวดที่ผ่อน</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="number" step="1" min="1"  class="form-control" name="amount_installment" id="amount_installment"  value="<?=$res[0]->amount_installment;?>" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคาผ่อนต่องวด</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="number" step="1" min="1"  class="form-control" name="pay_per_month" id="pay_per_month"  value="<?=$res[0]->pay_per_month;?>" />
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?=$res[0]->installment_type_id;?>">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/installmentType');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไขประเภทการผ่อนชำระ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var submitRequireEle = ["#amount_installment","#pay_per_month"];
    InstallmentTypeEditSubmit(submitRequireEle, "#form-InstallmentType-edit");
    InstallmentTypeEleChange(submitRequireEle); 
</script>


