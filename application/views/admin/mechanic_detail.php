<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 1px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.contracted:hover{cursor: pointer;}
</style>


<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
      <div class="x_panel">
          <div class="x_title">
              <h2>รายละเอียดข้อมูลช่าง<small><?=$res[0]->charng_code;?></small></h2>
              <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row invoice-info">
              <div class="col-sm-3 invoice-col">
                <address>
                    <br>ชื่อช่าง : <?=$res[0]->name;?> <?=$res[0]->sname;?>
                    <br>หมายเลขบัตรประชาชน : <?=$res[0]->idcard;?>
                    <br>สถานที่ประกอบการ : <?=$res[0]->office_name;?>
                    <br>เลขที่ประจำตัวผู้เสียภาษี : <?=$res[0]->office_taxpayer;?>
                </address>
              </div>
              <div class="col-sm-3 invoice-col">
                  <address>
                      <br>ที่อยู่ปัจจุบัน : <?=$res[0]->address;?>
                      <br>ตำบล / แขวง : <?=$res[0]->district_name;?> 
                      <br>อำเภอ / เขต : <?=$res[0]->amphur_name;?> 
                      <br>จังหวัด : <?=$res[0]->province_name;?> <?=$res[0]->zip_code;?>
                  </address>
              </div>
              <div class="col-sm-3 invoice-col">
                  <address>
                      <br>ที่ตั้งสถานที่ประกอบการ : <?=$res[0]->office_address;?>
                      <br>ตำบล / แขวง : <?=$res[0]->op_province_name;?> 
                      <br>อำเภอ / เขต : <?=$res[0]->op_amphur_name;?> 
                      <br>จังหวัด : <?=$res[0]->op_district_name;?> <?=$res[0]->office_zipcode;?>
                  </address>
              </div>
              <div class="col-sm-3 invoice-col">
                <address>
                  <br>Phone : <?=$res[0]->tel;?> 
                  <br>Email : <?=$res[0]->email;?>
                </address>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
<div class="row">
  
  <div class="col-md-7">
      <div class="x_panel">
        <div class="x_title">
            <h2>ตารางการทำสัญญา<small></small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="table-responsive">
            <table id="customer_res" class="table table-striped jambo_table bulk_action" >
              <thead>
                <tr class="headings">
                    <th class="column-title">ลำดับ</th>
                    <th class="column-title">สัญญา </th>
                    <th class="column-title">ลูกค้า </th>
                    <th class="column-title">สินค้า </th>
                    <th class="column-title" style="padding: .50rem;">สถานะ </th>
                </tr>
              </thead>
              <tbody>
                <?php $nume = 1; foreach($charng_contract as $item){?>  
                  <tr class="headings contracted" attr-data="<?=$item->temp_code?>"  data-toggle="tooltip" title="สัญญา <?=$item->temp_code?>">
                      <td class="column-title"><?=$nume?></td>
                      <td class="column-title"><?=$item->temp_code?> </td>
                      <td class="column-title"><?=$item->firstname?> <?=$item->lastname?> </td>
                      <td class="column-title"><?=$item->product_name?>  </td>
                      <td class="column-title" style="padding: .50rem;"><span style=" padding: 10px 20px; color: <?=$item->color;?>; background-color: <?=$item->background_color;?>;"><?=$item->label;?></span> </td>
                  </tr>
                <?php $nume ++;} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>

  <div class="col-md-5 col-sm-5  ">
    <div class="x_panel">
      <div class="x_title">
        <h2> สัญญา <small id="contract_pie"></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="echart_pies" style="height:350px;"></div>
      </div>
    </div>
  </div>
  
</div>

<script>
  CharngPieChart('echart_pies', null);
  $('.contracted').click(function () {
    var base_url = "<?php echo base_url(); ?>";
    var url = base_url+"admin/mechanic/getToChart";
    var contract_code = $(this).attr('attr-data');

    var chart_res = jsonContractToChart(url, contract_code);
      CharngPieChart('echart_pies', chart_res);
      $('#contract_pie').html(contract_code);
  });
</script>