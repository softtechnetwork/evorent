<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างข้อมูลสัญญาลูกค้า</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="" method="post" novalidate>-->
                <form id="temp-create-form"  class="" action="<?php echo site_url('/admin/temp/insert');?>" method="post" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสลูกค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <select id="customer" name="customer" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false' disabled>เลือกลูกค้า</option>
                                        <?php  foreach($Customer as $item){?>
                                            <option value="<?php echo $item->customer_code;?>"><?=$item->firstname;?> <?=$item->lastname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                                    <select id="product-cate" name="product-cate" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false' disabled>เลือกหมวดหมู่สินค้า</option>
                                        <?php  foreach($ProductCate as $item){?>
                                            <option value="<?php echo $item->cate_id;?>"> <?php echo $item->cate_name;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4" id="product-ele-position">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <select id="product" name="product" class="selectpicker form-control mh"  data-live-search="true">
                                        <option selected="false" disabled>เลือกสินค้า</option>'
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1" id="product-ele-position">
                                    <label class="col-form-label label-align">จำนวน</label>
                                    <input class="form-control" type="number" value="1" min="1" id="product-count" name="product-count" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">ยี่ห้อ</label>
                                    <input class="form-control" type="text" id="product-brand" name="product-brand"readonly/>
                                    <input class="form-control" type="hidden" id="product-brand-id" name="product-brand-id"/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">รุ่น</label>
                                    <input class="form-control" type="text"  id="product-version" name="product-version" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text"  id="property-number" name="property-number" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--################## Srial element ######################-->
                    <!--<div class="field item form-group" id="serialPlace">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row" id="serialElePotions"></div>
                        </div>
                    </div>-->
                    <div class="ln_solid"></div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ทำสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="contract-do-date" name="contract-do-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่เริ่มสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="contract-date" name="contract-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">วันที่เริ่มชำระงวดแรก</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="payment-start-date" name="payment-start-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">กำหนดชำระวันที</label>
                        <div class="col-md-1 col-sm-1">
                            <!--<input class="form-control" type="number"  min="1"  max="31" value="5" id="payment-due" name="payment-due" />-->
                            <select name='payment-due' id='payment-due' class="form-control">
                                <option value="15">15</option><option value="30">30</option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='inhabited' id='inhabited' class="form-control">
                            <option  value="">ประเภทที่อยู่</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าผ่อนรายเดือน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value=""  id="monthly-rent" name="monthly-rent"readonly/>
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">ภาษีมูลค่าเพิ่ม</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="text" value=""  id="monthly-plus-tax" name="monthly-plus-tax"readonly/>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">ระยะเวลาการเช่า</label>
                        <div class="col-md-1 col-sm-1">
                            <input class="form-control"  min="1"  type="number" value="" id="rental-period" name="rental-period"required="required" readonly/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    -->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class=" col-md-8 col-sm-8"> 
                            <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                <thead>
                                    <tr>
                                        <th>เลือก</th>
                                        <th>จำนวนงวด</th>
                                        <th>ผ่อนเดือนละ</th>
                                    </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>
                        </div>
                        <input type="hidden" id="installment" name="installment"/>
                    </div>
                    <div class="ln_solid"></div>
                    

                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เงินดาวน์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" min="0" value="0"  id="down-payment" name="down-payment" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระค่าเช่าล่วงหน้าเป็นเงินจำนวน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="advance-payment" name="advance-payment"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าธรรมเนียมดำเนินการติดตั้ง</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="installation-fee" name="installation-fee"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระโดยการโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร กสิกรไทย</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="ln_solid"></div>
                    -->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-9 col-sm-9">
                            <ul id="stats" style=" padding-left: initial; margin-top: 6px;">
                                <li style=" display: inline;list-style-type: none;padding-right: 3px;float: left;">
                                    <input class="inhabitedInput" type="checkbox" id='supporter' name='supporter' value=""/>
                                    <input type="hidden" id='supporter-check' name='supporter-check' value="0"/>
                                </li>
                                <li style=" display: inline;list-style-type: none;padding-right: 10px;float: left;">
                                    <p>เลือกเมื่อมีผู้คำ้ประกัน</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="field item form-group collapse" id="collapse-supporter">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-9 col-sm-9">
                            <div class="card card-body " style="border-radius: inherit;background-color: #f8f9fa;">
                                <div class="field item form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">ชื่อ ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="name-supporter" name="name-supporter"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">นามสกุล ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="sname-supporter" name="sname-supporter"/>
                                    </div>
                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขบัตรประชาชน ผู้คำ้ประกัน</label>
                                        <input name="idcard-supporter" id="idcard-supporter" type="tel"  maxlength="13"  class="form-control" required="required">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">วันเกิด ผู้คำ้ประกัน</label>
                                        <input class="form-control datepicker" type="text" id="bdate-supporter" name="bdate-supporter"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขมือถือ ผู้คำ้ประกัน</label>
                                        <input class="form-control" type="tel" id="mobile-supporter" name="mobile-supporter" >
                                    </div>

                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label class="col-form-label label-align">ที่อยู่ผู้คำ้ประกัน</label>
                                        <textarea  class="form-control" required="required" id='address-supporter' name='address-supporter'></textarea>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <div class="col-md-3 col-sm-3">
                                        <label for="province-supporter">จังหวัด</label>
                                        <select name='province-supporter' id='province-supporter' class="form-control">
                                            <option value="" selected='false' disabled>จังหวัด</option>	
                                            <?php foreach ($province as $item) : ?>
                                                <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="amphurs-supporter">อำเภอ / เขต</label>
                                        <select class="form-control" name="amphurs-supporter"  id="amphurs-supporter">
                                            <option value="">อำเภอ / เขต</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="district-supporter">ตำบล / แขวง</label>
                                        <select class="form-control" name="district-supporter"  id="district-supporter">
                                            <option value="">ตำบล / แขวง</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="zipcode-supporter">รหัสไปรษณีย์</label>
                                        <input type="tel" maxlength="5" class="form-control" name="zipcode-supporter" id="zipcode-supporter" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <?php //print_r($Broker); ?>
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นายหน้าให้เช่าทรัพทืสินและบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" id="sale-id" name="sale-id" />-->
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <select id="sale-id" name="sale-id" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false' disabled>เลือกรหัสนายหน้า</option>
                                        <?php  foreach($Broker as $item){?>
                                            <option value="<?php echo $item->charng_code;?>"> <?php echo $item->charng_code.' : '.$item->name.' '.$item->sname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--<button id="sale-check"  type='button' class="btn btn-info">ตรวจสอบ</button>
                        <label id="sale-status" class="col-form-label col-md-3 col-sm-3"></label>-->

                        <label class="col-form-label col-md-1 col-sm-1  label-align">ตัวแทนบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" id="techn-id" name="techn-id" />-->
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <select id="techn-id" name="techn-id" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false' disabled>เลือกรหัสตัวแทน</option>
                                        <?php  foreach($Agent as $item){?>
                                            <option value="<?php echo $item->charng_code;?>"> <?php echo $item->charng_code.' : '.$item->name.' '.$item->sname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--<button id="sale-check"  type='button' class="btn btn-info">ตรวจสอบ</button>
                        <label id="sale-status" class="col-form-label col-md-3 col-sm-3"></label>-->

                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเหตุ</label>
                        <div class="col-md-5 col-sm-5"><textarea  class="form-control" required="required" name='remark'></textarea></div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Status</label>
                        <div class="col-md-5 col-sm-5">
                        <?php //print_r($resStatus); ?>
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($item->status_code != 0) echo 'disabled'; ?>><?php echo $item->label; ?></option>
                                    <!--<option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>-->
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/temp');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
           $('.next').text(">");
    });
     
	var base_url = "<?php echo base_url(); ?>";

    var ProductCate_concernedID = ["#product", "#product-brand", "#product-brand-id","#product-version","#serialPlace","#serialElePotions", "#installment-table tbody","#property-number"];
    var Address_concernedID = ['#inhabited','#address-table tbody'];

    Temp_CstomerChange(base_url, "#customer", Address_concernedID);
    Temp_ProductCateChange(base_url, "#product-cate", ProductCate_concernedID, Address_concernedID);

    //$(ProductCate_concernedID[4]).hide();

    Temp_ProductChange(base_url, ProductCate_concernedID, '#product-count');
    ChangeInhabited(base_url, '#inhabited','#address-table tbody');  // replace address whene change

    
    $('#payment-start-date').change(function(){
        removeClassed($("#payment-start-date").selector, "input-valid");
    });
    
    /*$("#sale-check").on("click", function(e) {
        jsonTechn($("#sale-id").val(), "<?php echo base_url("admin/temp/ajaxTechn"); ?>");
    });*/

    var normalEle = ["#customer", "#product-cate","#product","#sale-id", "#payment-start-date", "#inhabited","#techn-id"];
    var installmentEle = ["installmentCheck","#installment-table"];
    Temp_SearchComboChange("#sale-id");
    Temp_SearchComboChange("#techn-id");


    //--------- supporter ------------  //
    $("#supporter").change(function () {
        var supporter = $('#'+this.id).prop('checked');
        if(supporter){
            $("#supporter-check").val(1);
            $("#collapse-supporter").collapse('show');
        }else{
            $("#supporter-check").val(0);
            $("#collapse-supporter").collapse('hide');
        }
    });

    var supporterProvince = ["#province-supporter","#amphurs-supporter","#district-supporter","#zipcode-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    rePlaceAddEle(supporterProvince, base_url);//replace ข้อมูลสถานที่ทำงาน

    var supporterEle = ["#supporter","#name-supporter","#sname-supporter","#idcard-supporter","#bdate-supporter","#mobile-supporter","#address-supporter",  "#province-supporter","#amphurs-supporter","#district-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    ChangeRemoveValidClass(supporterEle);
    //--------------------------------//

    tempCreateSubmit(normalEle, '#temp-create-form', installmentEle, supporterEle);
</script>

