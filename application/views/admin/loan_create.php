<style>
    .fail-stat-sale{color: #d9530e;}
    .succ-stat-sale{color: #26b99a!important;}
    .input-valid {
        border: 1px solid #fb48004d !important;
        box-shadow: 0px 0px 3px #ff470063 !important;
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Loan <small>Create</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="" method="post" novalidate>-->
                <form id="loan-create-form"  class="" action="<?php echo site_url('/admin/loan/insert');?>" method="post" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <div class="col-md-4 col-sm-4 ">
                            <input type="text" name="contract_id" id="contract_id" class="form-control" placeholder="กรุณากรอก เลขที่สัญญา" autocomplete="off" required="required">
                        </div>
                        <div class="col-md-1 col-sm-1 ">
                            <button id="contract_get" type='button' class="btn btn-primary"><i class="fa fa-search"></i></button>
                            <button id="contract_reset" type='button' class="btn btn-warning"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="field item form-group">
                        <div class="col-md-12 col-sm-12  ">
                            <div class="x_panel"  id="loan_c_panel">
                                <div class="x_title">
                                    <h2>ข้อมูล <small>สินค้าและการผ่อนชำระ</small></h2>
                                    <ul class="nav navbar-right panel_toolbox" style=" min-width: inherit !important;">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up" id="loan_c_collapse_icon"></i></a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" id="loan_c_contnt">
                                    
                                    <div class="field item form-group">
                                        <div class="col-form-label col-md-4 col-sm-4">
                                            ลูกค้า
                                            <input class="form-control"  type="text" id="cus_name" name="cus_name"  required="required" readonly/>
                                            <input type="hidden" id="cus_code" name="cus_code" />
                                        </div>

                                        <div class="col-form-label col-md-4 col-sm-4">
                                            เบอร์โทรศัพท์มือถือ
                                            <input class="form-control"  type="tel" id="tel_code" name="tel_code"  required="required" readonly/>
                                        </div>
                                        <div class="col-form-label col-md-4 col-sm-4">
                                            อีเมล์
                                            <input class="form-control"  type="email" id="email_code" name="email_code"  required="required" readonly/>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <div class="col-form-label col-md-4 col-sm-4">
                                            สินค้า
                                            <input class="form-control"  type="text" id="product_name" name="product_name"  required="required" readonly/>
                                            <input type="hidden" id="product_set" name="product_set" />
                                        </div>

                                        <div class="col-form-label col-md-4 col-sm-4">
                                            ราคา
                                            <input class="form-control"  type="tel" id="price_code" name="price_code"  required="required" readonly/>
                                        </div>
                                        <div class="col-form-label col-md-2 col-sm-2">
                                            จำนวน
                                            <input class="form-control"  type="number" id="count_code" name="count_code"  required="required" readonly/>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <div class="col-form-label col-md-4 col-sm-4">
                                            หมายเลขเครื่อง
                                            <input class="form-control"  type="text" id="serialcode" name="serialcode"  required="required" readonly/>
                                            <input type="hidden" id="serial_code" name="serial_code" />
                                        </div>
                                        <div class="col-form-label col-md-4 col-sm-4">
                                            ยี่ห้อ
                                            <input class="form-control"  type="text" id="brand_code" name="brand_code"  required="required" readonly/>
                                        </div>

                                        <div class="col-form-label col-md-4 col-sm-4">
                                            รุ่น
                                            <input class="form-control"  type="text" id="version_code" name="version_code"  required="required" readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class=" col-md-12 col-sm-12"> 
                                            <h2>ตารางการผ่อนชำระ</h2>
                                            <table class="table table-striped" id="installment-table">
                                                <thead>
                                                    <tr>
                                                        <th>งวด</th>
                                                        <th>ดอกเบี้ย</th>
                                                        <th>ราคาผ่อน</th>
                                                        <th>ภาษีมูลค่าเพิ่ม</th>
                                                        <th>ราคาผ่อนบวก ภาษีมูลค่าเพิ่ม</th>
                                                    </tr>
                                                </thead>
                                                <tbody> </tbody>
                                            </table>
                                            <input type="hidden" id="installment" name="installment"/>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-md-12 offset-md-12 label-align" >
                                            <a href="<?php echo base_url('/admin/loan');?>" type='button' class="btn btn-success">Back</a>
                                            <button type='submit' class="btn btn-primary">Create</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
	var base_url = "<?php echo base_url(); ?>";
    var arrayTemp = <?php echo json_encode($resTemp); ?>;
    var urlgetTempOne = base_url+"admin/loan/getTempOne";
    
    //################  Button event #############//
    $("#loan_c_contnt").css("display","none");
    $('#contract_get').click(function() {
        
        var contract_id = $('#contract_id').val();
        if(contract_id != null && contract_id != ''){
            var result = jsonTempOne(contract_id, urlgetTempOne);
            if(result != null){
                $("#loan_c_collapse_icon").removeClass("fa-chevron-up");
                $("#loan_c_collapse_icon").addClass("fa-chevron-down");
                $("#loan_c_contnt").css("display","block");

                $("#cus_name").val(result.firstname+" "+result.lastname);
                $("#cus_code").val(result.customer_code);
                $("#tel_code").val(result.tel);
                $("#email_code").val(result.email);

                $("#product_name").val(result.piname);
                $("#product_set").val(result.product_set);
                $("#price_code").val(result.product_price);
                $("#count_code").val(result.product_count);
                $("#serialcode").val(JSON.parse(result.product_serial).main.serial);
                $("#serial_code").val(result.product_serial);
                $("#brand_code").val(result.product_brand);
                $("#version_code").val(result.product_version);
                $("#installment").val(result.installment);

                //################### Installment Table ###########################//
                var res = result.installment;
                var td = null;
                $('#installment-table > tbody').html(null);
                $.each(JSON.parse(res), function (i, val) {
                    td += "<tr><td>"+val.period+"</td><td>"+val.interest+"</td><td>"+val.per_month_price+"</td><td>"+val.per_month_price_vat+"</td><td>"+val.per_month_price_include_vat+"</td></tr>";
                }); 
                $('#installment-table > tbody').append(td);
                //################### Installment Table ###########################//

                $("#contract_id").removeClass("input-valid");
            }else{
                $("#loan_c_collapse_icon").removeClass("fa-chevron-down");
                $("#loan_c_collapse_icon").addClass("fa-chevron-up");
                $("#loan_c_contnt").css("display","none");
                $("#contract_id").addClass("input-valid");
            }
        }else{
            $("#loan_c_collapse_icon").removeClass("fa-chevron-down");
            $("#loan_c_collapse_icon").addClass("fa-chevron-up");
            $("#loan_c_contnt").css("display","none");
            $("#contract_id").addClass("input-valid");
        }
    });
    $('#contract_reset').click(function() {
        $("#loan_c_collapse_icon").removeClass("fa-chevron-down");
        $("#loan_c_collapse_icon").addClass("fa-chevron-up");
        $("#loan_c_contnt").css("display","none");
        $("#contract_id").removeClass("input-valid");
        $("#contract_id").val(null);
    });


    //################  Get json Temp One  #############//
    function jsonTempOne(id, url){
        var res = null;
        $.ajax({
            url: url, //ทำงานกับไฟล์นี้
            data:  {'id':id},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                if(data.length >0){res = data[0];}
            },
            error: function(xhr, status, exception) { }
        });
        return res;
    };


    //################  submit Crate form #############//
    var input_id = ["#contract_id"];

    $('#loan-create-form').submit(function() {
        var valids  = 0;

        //################  Validate input #############//
        $.each(input_id, function (i, val) {
            if($(val).val() == '' || $(val).val() == null ){
                $(val).addClass("input-valid");
                valids++;
            }else{
                if($(val).selector == "#contract_id"){
                    var result = jsonTempOne($(val).val(), urlgetTempOne);
                    if(result == null){
                        $(val).addClass("input-valid");
                        valids++;
                    }
                }
            }
        });
        
        if(valids == 0){
            return true;
        }else{
            return false;
        }
        //return false;
    });

</script>
<!--<script src="<?php echo base_url('./assete/js/page/admin/temp_create.js');?>"></script>-->


