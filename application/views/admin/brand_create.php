<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างยี่ห้อสินค้า</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="" action="<?php echo site_url('/admin/brand/insert');?>" method="post"  enctype="multipart/form-data" >
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ยี่ห้อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input name="brand-name" id="brand-name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดยี่ห้อสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="brand-detail" name="brand-detail" class="form-control" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('/admin/brand');?>" class="btn btn-primary">กลับ</a>
                                <button type='submit' class="btn btn-success">สร้างยี่ห้อสินค้า</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>



