<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

#inhabited-create-form .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
.mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
.input-validat{color:#fb4800c4 !important;}

    /* The container */
    .field #select-add-field {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .field #select-add-field input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #e9ecef;
        border: 1px solid #ced4da;
    }

    /* On mouse-over, add a grey background color */
    .field #select-add-field:hover input ~ .checkmark { background-color: #ccc; }

    /* When the checkbox is checked, add a blue background */
    .field  #select-add-field input:checked ~ .checkmark { background-color: #1ABB9C;}

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .field #select-add-field input:checked ~ .checkmark:after { display: block; }

    /* Style the checkmark/indicator */
    .field #select-add-field .checkmark:after {
        left: 10px;
        top: 6px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างข้อมูลที่อยู่/สถานที่ติดตั้งสินค้า <small></small></h2>
                <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>-->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="<?php echo site_url('/admin/customer/insert');?>" method="post" enctype="multipart/form-data" novalidate>-->
                <form id="inhabited-create-form" class="" action="<?php echo site_url('/admin/inhabited/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ลูกค้า</label>
                        <div class="col-md-3 col-sm-3">
                            <!--<input class="form-control" type="text"  id="product" name="product" autocomplete="off" required="required" />-->

                            <select id="customer" name="customer" class="selectpicker form-control mh " name="part" data-live-search="true">
                                <option value="000000">เลือกลูกค้า</option>
                                <?php  foreach($resCustomer as $item){?>
                                    <option value="<?php echo $item->customer_code;?>"><?=$item->firstname;?> <?=$item->lastname;?></option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทที่อยู่</label>
                        <div class="col-md-3 col-sm-3">
                           <input  id="type" name="type" value=""  type="text"  class="form-control"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ประเภทอาคาร</label>
                        <div class="col-md-6 col-sm-6">
                           <input  id="building-type" name="building-type" value=""  type="text"  class="form-control"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อบริษัท/ห้างร้าน</label>
                        <div class="col-md-6 col-sm-6">
                           <input  id="company-name" name="company-name" value=""  type="text"  class="form-control"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่ออาคาร/หมู่บ้าน</label>
                        <div class="col-md-6 col-sm-6">
                           <input  id="village-name" name="village-name" value=""  type="text"  class="form-control"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จังหวัด</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อำเภอ / เขต</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
								<option value="">อำเภอ / เขต</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ตำบล / แขวง</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="district"  id="district" required="required" >
								<option value="">ตำบล / แขวง</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสไปรษณีย์</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" readonly>
                            <!--<input class="form-control" value="" type="number" class='number' name="zipcode" data-validate-minmax="0,5" required='required'>-->
                        </div>
                    </div>

                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('admin/inhabited');?>" type='button' class="btn btn-success">Back</a>
                                <button type='submit' class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = "<?php echo base_url(); ?>";
    var element_id = ["#customer","#address","#province","#amphurs","#district","#zipcode","#type"];
    InhabitedCreate(base_url,element_id);
    InhabitedCreateSubmit(element_id, '#inhabited-create-form');
</script>