<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างข้อมูลสัญญาลูกค้า</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="" method="post" novalidate>-->
                <form id="contract-create-form"  class="" action="<?php echo site_url('/admin/contract/insert');?>" method="post" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ลูกค้า</label>
                        <div class="col-md-8 col-sm-8 ">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <select id="customer" name="customer" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false' disabled>เลือกลูกค้า</option>
                                        <?php  foreach($Customer as $item){?>
                                            <option value="<?php echo $item->customer_code;?>"><?=$item->firstname;?> <?=$item->lastname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">หมวดหมู่สินค้า</label>
                                    <select id="product-cate" name="product-cate" class="selectpicker form-control mh" data-live-search="true">
                                        <option selected='false'  value="" disabled>เลือกหมวดหมู่สินค้า</option>
                                        <?php  foreach($ProductCate as $item){?>
                                            <option productC="<?=$item->coundOfProductMaster;?>" value="<?php echo $item->cate_id;?>"> <?php echo $item->cate_name;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4" id="product-ele-position">
                                    <label class="col-form-label label-align">ประเภทสินค้าสินค้า(สำหรับลูกค้า)</label>
                                    <select id="customer-type" name="customer-type" class="form-control form-control mh" data-live-search="true">
                                        <!--<option selected='false'  value="" disabled>เลือกประเภทสินค้าสินค้า</option>-->
                                        <?php  foreach($CustoerType as $item){?>
                                            <option value="<?php echo $item->id;?>"> <?php echo $item->label;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4" id="product-ele-position">
                                    <label class="col-form-label label-align">ประเภทสัญญา</label>
                                    <select id="contract-type" name="contract-type" class="form-control form-control mh" data-live-search="true">
                                        <!--<option selected='false'  value="" disabled>เลือกประเภทสัญญา</option>-->
                                        <?php  foreach($ContractType as $item){?>
                                            <option value="<?php echo $item->id;?>"> <?php echo $item->label;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4" id="product-ele-position">
                                    <label class="col-form-label label-align">ชื่อสินค้า</label>
                                    <select id="product" name="product" class="selectpicker form-control mh"  data-live-search="true">
                                        <option selected="false" disabled>เลือกสินค้า</option>'
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="col-form-label label-align">ยี่ห้อ</label>
                                    <input class="form-control" type="text" id="product-brand" name="product-brand" readonly/>
                                    <input class="form-control" type="hidden" id="product-brand-id" name="product-brand-id"/>
                                </div>
                                <!--<div class="col-md-4 col-sm-4">
                                    <label class="label-align">รุ่น</label>
                                    <input class="form-control" type="text"  id="product-version" name="product-version" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text"  id="property-number" name="property-number" readonly/>
                                </div>-->
                                <div class="col-md-1 col-sm-1" id="product-ele-position">
                                    <label class="col-form-label label-align">จำนวน</label>
                                    <input class="form-control" type="number" value="1" min="1" id="product-count" name="product-count" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">ยี่ห้อ</label>
                                    <input class="form-control" type="text" id="product-brand" name="product-brand"readonly/>
                                    <input class="form-control" type="hidden" id="product-brand-id" name="product-brand-id"/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">รุ่น</label>
                                    <input class="form-control" type="text"  id="product-version" name="product-version" readonly/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label class="label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text"  id="property-number" name="property-number" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่ทำสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="contract-do-date" name="contract-do-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันที่เริ่มสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="contract-date" name="contract-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">วันที่เริ่มชำระงวดแรก</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control datepicker" type="text" id="payment-start-date" name="payment-start-date"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">กำหนดชำระวันที</label>
                        <div class="col-md-1 col-sm-1">
                            <!--<input class="form-control" type="number"  min="1"  max="31" value="5" id="payment-due" name="payment-due" />-->
                            <select name='payment-due' id='payment-due' class="form-control">
                                <option value="15">15</option><option value="30">30</option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เจ้าของ ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control" type="text" id="inhabited-name" name="inhabited-name"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ที่ติดตั้งสินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='inhabited' id='inhabited' class="form-control">
                            <option  value="">ประเภทที่อยู่</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-8 col-sm-8">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ขนาดพื้นที่ติดตั้งอุปกรณ์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"  id="place-ins-size" name="place-ins-size" value=""/>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3">(ตารางเมตร)</label>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อัตราค่าเช่าพื้นที่ติดตั้งอุปกรณ์ รายปี</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number"  id="annual-rental-rate" name="annual-rental-rate" value=""/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รูปแบบการผ่อนชำระ</label>
                        <div class=" col-md-8 col-sm-8"> 
                            <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                <thead>
                                    <tr>
                                        <th>เลือก</th>
                                        <th>จำนวนงวด</th>
                                        <th>ผ่อนเดือนละ</th>
                                    </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>
                        </div>
                        <input type="hidden" id="installment" name="installment"/>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เงินดาวน์</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" min="0" value="0"  id="down-payment" name="down-payment" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระค่าเช่าล่วงหน้าเป็นเงินจำนวน</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="advance-payment" name="advance-payment"required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จำนวนเงินที่ต้อง ชำระเริ่มต้น</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="statart_payment_amount" name="statart_payment_amount"required="required" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ค่าธรรมเนียมดำเนินการติดตั้ง</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="installation-fee" name="installation-fee"required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">มูลค่าสินค้า(ที่ซื้อจาก PSI)</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" type="number" value="0" min="0" id="product-value" name="product-value"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">มูลค่าทรัพย์สินเมื่อครบสัญญา</label>
                        <div class="col-md-2 col-sm-2">
                            <label class="col-form-label label-align"> % ในการคิดคำนวน</label>
                            <input class="form-control" type="number" value="10" min="0" id="percen-end-contract" name="percen-end-contract"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชำระโดยการโอนเงินเข้าบัญชีเงินฝากประเภทออมทรัพย์</label>
                        <label class="col-form-label col-md-2 col-sm-2  ">ธนาคาร กสิกรไทย</label>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">หมายเลขบัญชี<span class="required"> 05958649048585</span></label>
                    </div>
                    <div class="ln_solid"></div>
                    -->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ผู้คำ้ประกัน</label>
                        <div class="col-md-9 col-sm-9">
                            <ul id="stats" style=" padding-left: initial; margin-top: 6px;">
                                <li style=" display: inline;list-style-type: none;padding-right: 3px;float: left;">
                                    <input class="inhabitedInput" type="checkbox" id='supporter' name='supporter' value=""/>
                                    <input type="hidden" id='supporter-check' name='supporter-check' value="0"/>
                                </li>
                                <li style=" display: inline;list-style-type: none;padding-right: 10px;float: left;">
                                    <p>เลือกเมื่อมีผู้คำ้ประกัน</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="field item form-group collapse" id="collapse-supporter">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-9 col-sm-9">
                            <div class="card card-body " style="border-radius: inherit;background-color: #f8f9fa;">
                                <div class="field item form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">ชื่อ ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="name-supporter" name="name-supporter"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label class="col-form-label label-align">นามสกุล ผู้คำ้ประกัน</label>
                                        <input class="form-control" id="sname-supporter" name="sname-supporter"/>
                                    </div>
                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขบัตรประชาชน ผู้คำ้ประกัน</label>
                                        <input name="idcard-supporter" id="idcard-supporter" type="tel"  maxlength="13"  class="form-control" required="required">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">วันเกิด ผู้คำ้ประกัน</label>
                                        <input class="form-control datepicker" type="text" id="bdate-supporter" name="bdate-supporter"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <label class="col-form-label label-align">หมายเลขมือถือ ผู้คำ้ประกัน</label>
                                        <input class="form-control" type="tel" id="mobile-supporter" name="mobile-supporter" >
                                    </div>

                                </div>
                                <div class="field item form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label class="col-form-label label-align">ที่อยู่ผู้คำ้ประกัน</label>
                                        <textarea  class="form-control" required="required" id='address-supporter' name='address-supporter'></textarea>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <div class="col-md-3 col-sm-3">
                                        <label for="province-supporter">จังหวัด</label>
                                        <select name='province-supporter' id='province-supporter' class="form-control">
                                            <option value="" selected='false' disabled>จังหวัด</option>	
                                            <?php foreach ($province as $item) : ?>
                                                <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="amphurs-supporter">อำเภอ / เขต</label>
                                        <select class="form-control" name="amphurs-supporter"  id="amphurs-supporter">
                                            <option value="">อำเภอ / เขต</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="district-supporter">ตำบล / แขวง</label>
                                        <select class="form-control" name="district-supporter"  id="district-supporter">
                                            <option value="">ตำบล / แขวง</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="zipcode-supporter">รหัสไปรษณีย์</label>
                                        <input type="tel" maxlength="5" class="form-control" name="zipcode-supporter" id="zipcode-supporter" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นายหน้าให้เช่าทรัพท์สินและบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <select id="sale-id" name="sale-id" class="selectpicker form-control mh" data-live-search="true">
                                        <option value="" selected='true' >เลือกรหัสนายหน้า</option>
                                        <?php  foreach($Broker as $item){?>
                                            <option value="<?php echo $item->charng_code;?>"> <?php echo $item->charng_code.' : '.$item->name.' '.$item->sname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <label class="col-form-label col-md-1 col-sm-1  label-align">ตัวแทนบริการ</label>
                        <div class="col-md-3 col-sm-3">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <select id="techn-id" name="techn-id" class="selectpicker form-control mh" data-live-search="true">
                                        <option value="" selected='true' >เลือกรหัสตัวแทน</option>
                                        <?php  foreach($Agent as $item){?>
                                            <option value="<?php echo $item->charng_code;?>"> <?php echo $item->charng_code.' : '.$item->name.' '.$item->sname;?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเหตุ</label>
                        <div class="col-md-5 col-sm-5"><textarea  class="form-control" required="required" name='remark'></textarea></div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Status</label>
                        <div class="col-md-5 col-sm-5">
                        <?php //print_r($resStatus); ?>
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"  <?php if($item->status_code != 0) echo 'disabled'; ?>><?php echo $item->label; ?></option>
                                    <!--<option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>-->
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('admin/contract');?>" type='button' class="btn btn-success">Back</a>
                            <button type='submit' class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
           $('.next').text(">");
    });
     
	var base_url = "<?php echo base_url(); ?>";
    
    $('#product-cate').change(function() {
        if(this.value != '' && this.value != null ){console.log(this.value);
            var product_cate = this.value;
            var customer_type = $("#customer-type").val();
            var contract_type = $("#contract-type").val();
            $("#product-brand").val(null); // preoduct brand

            Search_Products(base_url, product_cate, customer_type, contract_type);

            $(this).parent().children('button').removeClass("input-valid");
        }
    });

    $('#customer-type').change(function() {
        if(this.value != '' && this.value != null ){console.log(this.value);
            var product_cate = $("#product-cate").val();
            var customer_type = this.value;
            var contract_type = $("#contract-type").val();
            $("#product-brand").val(null); // preoduct brand

            Search_Products(base_url, product_cate, customer_type, contract_type);
        }
    });

    $('#contract-type').change(function() {
        if(this.value != '' && this.value != null ){console.log(this.value);
            var product_cate = $("#product-cate").val();
            var customer_type = $("#customer-type").val();
            var contract_type = this.value;
            $("#product-brand").val(null); // preoduct brand
            
            Search_Products(base_url, product_cate, customer_type, contract_type);
        }
    });

    function Search_Products(base_url, product_cate, customer_type, contract_type){
        var res = Contract_GetProducts(base_url, product_cate, customer_type, contract_type);
        if(res.length > 0 ){ 
            Contract_AppendProductCombs(res);
        }else{
            $('#product').html(null);
            var options = '<option selected="false" disabled>ไม่มีสินค้า</option>';
            $('#product').append(options);
            $('.selectpicker').selectpicker('refresh'); // refresh class

            $("#installment-table tbody").html(null);
        }
    };
    function Contract_GetProducts(base_url, product_cate, customer_type, contract_type){
        var res = null;
        $.ajax({
            url: base_url+"admin/contract/GetProducts", //ทำงานกับไฟล์นี้
            data:  {
                'product_cate':product_cate,
                'customer_type':customer_type,
                'contract_type':contract_type
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data; },
            error: function(xhr, status, exception) { console.log(exception); }
        });
        return res;
    };
    function Contract_AppendProductCombs(products){
        // append option to product combobox
        $('#product').html(null);
        var options = '<option selected="false" disabled>เลือกสินค้า</option>';
        products.forEach(item => {
            options += "<option value='"+item.product_id+"' res='"+JSON.stringify(item)+"'>"+item.product_name+"</option>";
        });
        $('#product').append(options);
        $('.selectpicker').selectpicker('refresh'); // refresh class
    };

    Contract_CstomerChange(base_url, "#customer");
    function Contract_CstomerChange(base_url, EventId){
        $(EventId).change(function() {
            $(this).parent().children('button').removeClass("input-valid");
            var res = Contract_getIhbByCus(base_url, this.value);

            var inhabited_name = $(this).find(":selected").text();
            $('#inhabited-name').val(inhabited_name);// replace inhabited_name

            Contract_drawIhbComb(res, '#inhabited'); // draw Inhabited
            $('#address-table tbody').html(null);  // replace Inhabited table whene change
        });
    };
    function Contract_getIhbByCus(base_url, customer_code){
        var res = null;
        $.ajax({
            url: base_url+"admin/contract/getIhbByCus", //ทำงานกับไฟล์นี้
            data: "customer_code=" + customer_code,  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data;},
            error: function(xhr, status, exception) {  console.log(exception); }
        });
        return res;
    };
    function Contract_drawIhbComb(data, replacePosition){
        removeClassed(replacePosition, "input-valid");
        var srting = '<option selected="false" disabled>ประเภทที่อยู่</option>';
        $(replacePosition).html(null);
        $.each(data, function (i, item) {
            srting += '<option value="'+data[i].inhabited_code+'">'+data[i].category+'</option>';
        });
        $(replacePosition).append(srting);
    };

    Contract_ProductChange(base_url, "#product");
    function Contract_ProductChange(base_url, EventId){
        $(EventId).change(function() {
            $(this).parent().children('button').removeClass("input-valid");
            var Product = JSON.parse($(this).find(':selected').attr('res'));
            $("#product-brand").val(Product.brand_name); // preoduct brand
            $("#product-brand-id").val(Product.brand_id); // preoduct brand id
            
            // Installment type table 
            var installmentType = Contract_GetInstallmentTypeByProduct(base_url, Product.product_id);
            Contract_DrowInstallmentTypeTable(installmentType, "#installment-table tbody");
        });
    };
    function Contract_GetInstallmentTypeByProduct(base_url, product_id){
        var res = null;
        $.ajax({
            url: base_url+"admin/contract/GetInstallmentTypeByProductID", //ทำงานกับไฟล์นี้
            data:  {
                'product_id':product_id
                },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    };
    function Contract_DrowInstallmentTypeTable(data, EventId){
        var td = null;
        $(EventId).html(null);
        $.each(data, function (i, val) {
            //var res =  JSON.stringify({'period': i, 'interest': obj[i]['interest'], 'per_month_price': obj[i]['per_month_price_a'], 'per_month_price_vat': obj[i]['per_month_price_a_vat'],'per_month_price_include_vat': obj[i]['per_month_price_a_vat']});
            var res =  JSON.stringify({'amount_installment': val.amount_installment, 'per_month_price': val.pay_per_month});
            td += "<tr>";
            td += "<td><input class='inhabitedInput' type='radio' name='installmentCheck' id='installmentCheck"+i+"' value='"+res +"' /> </td>";
            td += "<td>"+val.amount_installment+"</td>";
            td += "<td>"+val.pay_per_month+"</td>";
            td += "</tr>";
        });
        $(EventId).append(td);
    };

    Contract_ChangeInhabited(base_url, '#inhabited','#address-table tbody');  // replace address whene change
    function Contract_ChangeInhabited(base_url, ele, replacePosition) {
        $(ele).change(function(){
            removeClassed(ele, 'input-valid');
            var ihbRes = Contract_getInhabitedByCode(base_url, $(ele).val());
            Contract_drawIhbtable(ihbRes, replacePosition);
        });
    }
    function Contract_getInhabitedByCode(base_url, inhabited_code){
        var res = null;
        $.ajax({
            url: base_url+"admin/customer/getInhabitedByCode", //ทำงานกับไฟล์นี้
            data: "inhabited_code=" + inhabited_code,  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { res = data;},
            error: function(xhr, status, exception) {  console.log(exception); }
        });
        return res;
    }
    function Contract_drawIhbtable(data, replacePosition){
        $(replacePosition).html(null);
        var srting = '<tr>';
        srting += '<td>'+data[0]['address']+'</td>';
        srting += '<td>'+data[0]['district_name']+'</td>';
        srting += '<td>'+data[0]['amphur_name']+'</td>';
        srting += '<td>'+data[0]['province_name']+'</td>';
        srting += '<td>'+data[0]['zip_code']+'</td>';
        srting += '</tr>';
        $(replacePosition).append(srting);
    }


    $('#contract-do-date').change(function(){
        removeClassed($('#contract-do-date').selector, "input-valid");
    });
    $('#contract-date').change(function(){
        removeClassed($('#contract-date').selector, "input-valid");
    });
    $('#payment-start-date').change(function(){
        removeClassed($("#payment-start-date").selector, "input-valid");
    });
    
    /*$("#sale-check").on("click", function(e) {
        jsonTechn($("#sale-id").val(), "<?php echo base_url("admin/temp/ajaxTechn"); ?>");
    });*/

    Contract_SearchComboChange("#sale-id");
    Contract_SearchComboChange("#techn-id");
    function Contract_SearchComboChange(EventId){
        $(EventId).change(function() {
            $(this).parent().children('button').removeClass("input-valid");
        });
    };

    //--------- supporter ------------  //
    $("#supporter").change(function () {
        var supporter = $('#'+this.id).prop('checked');
        if(supporter){
            $("#supporter-check").val(1);
            $("#collapse-supporter").collapse('show');
        }else{
            $("#supporter-check").val(0);
            $("#collapse-supporter").collapse('hide');
        }
    });

    var supporterProvince = ["#province-supporter","#amphurs-supporter","#district-supporter","#zipcode-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    rePlaceAddEle(supporterProvince, base_url);//replace ข้อมูลสถานที่ทำงาน validate.js file


    var supporterEle = ["#supporter","#name-supporter","#sname-supporter","#idcard-supporter","#bdate-supporter","#mobile-supporter","#address-supporter",  "#province-supporter","#amphurs-supporter","#district-supporter"]; // ห้าม เปลี่ยนตำแหน่ง
    ChangeRemoveValidClass(supporterEle);//validate.js file
    //--------------------------------//


    //var normalEle = ["#customer", "#product-cate","#product","#sale-id", "#payment-start-date", "#inhabited","#techn-id"];
    var normalEle = ["#customer", "#product-cate","#product", "#contract-do-date",  "#contract-date", "#payment-start-date", "#inhabited"];
    Contract_CreateSubmit(normalEle, '#contract-create-form', supporterEle);
    function Contract_CreateSubmit(element_id, submit_id, supporterEle){
        $(submit_id).submit(function() {
            var valids  = 0;
            //################  Validate input #############//
            $.each(element_id, function (i, val) {
                if($(val).val() == '' || $(val).val() == null ){
                    switch(val){
                        case '#customer': 
                        case '#product-cate':
                        case '#product': 
                        case "#sale-id":
                        case "#techn-id":
                            $(val).parent().children('button').addClass("input-valid");
                        break;
                        default:             
                            $(val).addClass("input-valid");
                            break;
                    }
                    valids++;
                }
            });

            if($('input[name=installmentCheck]:checked').val() == undefined ){
                $("#installment-table").addClass("input-valid");
                valids++;
            }else{
                removeClassed($("#installment-table").selector, "input-valid");
            }

            //----   Supporter ----//
            if ($(supporterEle[0]).is(":checked"))
            {
                $.each(supporterEle, function (i, val) {
                    if(i != 0){
                        if($(val).val() == '' || $(val).val() == null ){
                            $(val).addClass("input-valid");
                            valids++;
                        }
                    }
                });
            }
            //-----------------------//

            if(valids == 0){
                return true;
            }else{
                return false;
            }
            //console.log(valids);  return false;
        });
    };

</script>

