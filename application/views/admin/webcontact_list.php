
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลการติดต่อจากหน้าเว็บ EVORENT<small></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="tables" class="table table-striped jambo_table bulk_action"  style="width:100%; border-spacing: 1px !important;">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">ลำดับ</th>
                            <th class="column-title">ลูกค้า</th>
                            <th class="column-title">โทรศัพท์มือถือ</th>
                            <th class="column-title">อีเมล์</th>
                            <th class="column-title">หัวข้อ</th>
                            <th class="column-title">รายละเอียด</th>
                            <th class="column-title" style="text-align:center;">สถานะ</th>
                            <th class="column-title" style="text-align:center;">วันที่ติดต่อ</th>
                            <th class="column-title no-link last" style="text-align: center;"><span class="nobr">กิจกรรม</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <!-- <div id="pagination" page="1" ></div> -->
        </div>
    </div>
</div>
<!---- End Content ------>

<script>

    var base_url = $('input[name="base_url"]').val();
    DrawTable(base_url, '#tables');
    function DrawTable(base_url, replacePosition){
        var res = getProducts(base_url);
        if(res){ 
            var elements = '';
            var body = replacePosition+' tbody';
            $(body).html(null);
            $.each(res, function (i, val) {
                elements += '<tr class=" ">';
                elements += '<td class=" ">'+val['RowNum']+'</td>';
                elements += '<td class=" ">'+val['name']+' '+val['sname']+'</td>';
                elements += '<td class=" ">'+val['tel']+'</td>';
                elements += '<td class=" ">'+val['email']+'</td>';
                elements += '<td class=" ">'+val['topic']+'</td>';
                elements += '<td class=" ">'+val['message']+'</td>';
                elements += '<td class=" last"  style="text-align: center;">';
                elements +=     '<button type="button" class="btn btn-round" style="background-color: '+val['background_color']+'; color: '+val['color']+'; font-size: 13px; padding: 0 15px; margin-bottom: inherit;margin-right: inherit;"> '+val['label']+'</button>';
                elements += '</td>';
                elements += '<td class=" " style="text-align:center;">'+val['cdate']+'</td>';
                elements += '<td class=" last"  style="text-align: center;">';
                elements +=   '<a href="'+base_url+'admin/webContact/edit/'+val['id']+' " data-toggle="tooltip" title="แก้ไข">';
                elements +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 2px 6px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                elements +=   '</a>';
                elements +=   '<button type="button" class="btn btn-round btn-info remove-btn" data-id="'+val['id']+'" data-name="'+val['name']+' '+val['sname']+'" style=" font-size: 13px; padding: 2px 6px; margin-bottom: inherit; background-color: #e01a46; border-color: #e01a46;"  data-toggle="tooltip" title="ลบ การติดต่อ"><i class="fa fa-trash-o"></i></button>';

                //elements +=   '<a href="'+base_url+'admin/customer/premise/'+val['customer_code']+' " data-toggle="tooltip" title="หลักฐาน">';
                //elements +=       '<button type="button" class="btn btn-round btn-success" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-image-o"></i></button>';
                //elements +=   '</a>';
                elements += '</td>';
                elements += '</tr>';
            });
            $(body).append(elements);
            
            // Datatable
            $(replacePosition).dataTable({
                destroy: true,
                lengthMenu: [
                    [50, 100],
                    [50, 100],
                ],
                "aoColumnDefs": [
                    // { "bSortable": false, "aTargets": [4,5,6,7,8 ] }, 
                    //{ "bSearchable": false, "aTargets": [ 0, 1, 2, 3 ] }
                ]
            });
            
            $( ".remove-btn" ).click(function() {
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                if(confirm("คุณต้องการที่จะลบ ข้อมูลการติดต่อของ คุณ"+name+" จริงหรือไม่")) {
                    removecontacr(base_url, id);
                }
            });
        }
    }
    function getProducts(base_url){
        var res = null;
        $.ajax({
            url: base_url+"admin/webContact/contract_get", //ทำงานกับไฟล์นี้
            //data:  {'Search':search},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        return res;
    }
    function removecontacr(base_url, id){
        var res = null;
        $.ajax({
            url: base_url+"admin/webContact/contract_delete", //ทำงานกับไฟล์นี้
            data:  {'id':id},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                $('#tables').DataTable().destroy();
                DrawTable(base_url, '#tables');
                //if(data.status){ }
            },
            error: function(xhr, status, exception) { 
                console.log(exception);
            }
        });
        //return res;
    }

</script>

  