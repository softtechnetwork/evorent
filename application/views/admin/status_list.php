
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ค้นหาข้อมูลสถานะ</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="label">สถานะ</label>
                    <input id="label" name="label" type="text" class="form-control" placeholder="..." />
                </div>
                <div class ="col-md-3">
                    <label for="statusCate">หมวดหมู่</label>
                    <select name='statusCate' id='statusCate' class="form-control">
                        <option value="" selected='false' disabled >เลือหมวดหมู่</option>
                        <?php foreach ($statusCate as $item) : ?>
                            <option value="<?php echo $item->status_category_code; ?>"><?php echo $item->label; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="button-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
                <div class ="col-md-2  navbar-right "  style=" padding-top: 1.65rem;">
                    <a class="btn btn-success" href="<?php echo base_url('admin/status/create');?>"  style="color: #ffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มสถานะ
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลสถานะ<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="status_res" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title">ลำดับ</th>
                            <th class="column-title">รหัส </th>
                            <th class="column-title">สถานะ </th>
                            <th class="column-title">หมวดหมู่ </th>
                            <th class="column-title">รายละเอียด </th>
                            <th class="column-title no-link last" style="text-align: center;"><span class="nobr">กิจกรรม</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="pagination" page="1" ></div>
        </div>
    </div>
</div>
<!---- End Content ------>

<script>

    var base_url = $('input[name="base_url"]').val();

    var getRes = base_url+"admin/status/getRes";
    var getAll = base_url+"admin/status/getResAll";

    var searchEle = ["#label", "#statusCate"];
    var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
    var resObj = {"getRes" :getRes, "elementTable":"#status_res tbody"};
    var allObj = {"getAll":getAll, "functionResName":"getCustomerRes"};

    getStatusRes(resObj, searchEle, pageObj);
    Pagination(allObj, searchEle, pageObj, resObj);


    $('#button-search').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        getStatusRes(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });
    
    $('#button-reset').click(function(){
        var pageObj = {"pagination" :"#pagination", "ItemPerPage":50, "itemStt":1, "itemEnd":50};
        $.each(searchEle, function (i, val) {
            $(val).val('');
        });

        getStatusRes(resObj, searchEle, pageObj);
        Pagination(allObj, searchEle, pageObj, resObj);
    });



    function getStatusRes(resObj, searchEle, pageObj){
        var searchJson = SearchJson(searchEle);
        $(resObj["elementTable"]).html(null);
        var res = null;
        $.ajax({
            url: resObj["getRes"], //ทำงานกับไฟล์นี้
            data: { 'Search':searchJson, 'itemStt':pageObj['itemStt'], 'itemEnd':pageObj['itemEnd']},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                var tr = null;
                if(data.length > 0){
                    var num = 1;
                    $.each(data, function (i, val) {
                        tr += '<tr class=" ">';
                        tr += '<td class=" ">'+val['RowNum']+'</td>';
                        tr += '<td class=" ">'+val['status_code']+'</td>';
                        tr += '<td class=" ">'+val['label']+'</td>';
                        tr += '<td class=" ">'+val['cateLabel']+'</td>';
                        tr += '<td class=" ">'+val['detail']+'</td>';
                        tr += '<td class=" last"  style="text-align: center;">';
                        tr +=   '<a href="'+base_url+'admin/status/edit/'+val['id']+' " data-toggle="tooltip" title="แก้ไข">';
                        tr +=       '<button type="button" class="btn btn-round btn-warning" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-wrench"></i></button>';
                        tr +=   '</a>';
                        //tr +=   '<a href="'+base_url+'admin/customer/detail/'+val['customer_code']+' " data-toggle="tooltip" title="รายละเอียด">';
                        //tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-text-o"></i></button>';
                        //tr +=   '</a>';
                        //tr +=   '<a href="'+base_url+'admin/customer/premise/'+val['customer_code']+' " data-toggle="tooltip" title="หลักฐาน">';
                        //tr +=       '<button type="button" class="btn btn-round btn-success" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;"><i class="fa fa-file-image-o"></i></button>';
                        //tr +=   '</a>';
                        tr += '</td>';
                        tr += '</tr>';
                        num++;
                    });
                }
                $(resObj["elementTable"]).append(tr);
                
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
    }

</script>

  