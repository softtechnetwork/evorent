<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างสถานะ</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="" action="<?php echo site_url('/admin/status/insert');?>" method="post"  enctype="multipart/form-data" >
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมวดหมู่</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='cate' id='cate' class="form-control">
                                <?php foreach ($category as $item) : ?>
                                    <option value="<?php echo $item->status_category_code; ?>"><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Catgory</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='cate' id='cate' class="form-control">
                                <option value="Customer">ลูกค้า</option>
                                <option value="Temp">สัญญา</option>
                                <option value="Loan">การผ่อน/ชำระ</option>
                                <option value="Installment">ค่างวดเงินผ่อน</option>
                            </select>
                        </div>
                    </div>-->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สถานะ<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" name="st" placeholder="" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" name='stdetail'></textarea>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สี ของสถานะ<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6  ">
                            <div class="input-group demo2 colorpicker-element">
                                <input name="color" type="text" value="#e01ab5" class="form-control">
                                <span class="input-group-addon"><i style="background-color: rgb(224, 26, 181);"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สีพื้นหลัง ของสถานะ<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6  ">
                            <div class="input-group demo2 colorpicker-element">
                                <input name="bgcolor" type="text" value="#e01ab5" class="form-control">
                                <span class="input-group-addon"><i style="background-color: rgb(224, 26, 181);"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('/admin/status');?>" class="btn btn-primary">กลับ</a>
                                <button type='submit' class="btn btn-success">สร้างสถานะ</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>



