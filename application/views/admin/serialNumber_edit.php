<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขหมายเลขเครื่อง</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-serial-edit" action="<?php echo site_url('/admin/serialNumber/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สัญญา</label>
                        <div class="col-md-6 col-sm-6 ">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสัญญา</label>
                                    <input class="form-control" type="text" name="contract-id"  id="contract-id" value="<?=$resTemp[0]->temp_code?>" readonly/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">หมายเลขทรัพย์สิน</label>
                                    <input class="form-control" type="text" name="product-number"  id="product-number" value="<?=$resTemp[0]->product_number?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">รหัสสินค้า</label>
                                    <input class="form-control" type="text" id="product_id" name="product_id" value="<?=$resTemp[0]->product_id?>" readonly/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label class="col-form-label label-align">สินค้า</label>
                                    <input class="form-control" type="text" id="product_name" name="product_name" value="<?=$resTemp[0]->product_name?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">หมายเลขเครื่อง</label>
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                            <?php foreach($resSerial as $item):?>
                                <div class="col-md-1 col-sm-1" style="padding-top: 2rem;">
                                    <input class="form-control" type="text"  value="<?=$item->no;?>" readonly/>
                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <label class="col-form-label label-align"><?=$item->product_sub_name;?></label>
                                    <input class="form-control" type="text" 
                                    id="serial_<?=$item->product_sub_id;?>" 
                                    name="serial_<?=$item->product_sub_id;?>"  
                                    value="<?=$item->serial_number;?>" />
                                </div>
                            <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/serialNumber');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success"> แก้ไขหมายเลขเครื่อง</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var elementID = ["#contract-id", "#product_id"];
    serialEditSubmit(elementID, "#form-serial-edit");
</script>


