<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ประเภทการผ่อนชำระ<small></small></h2>
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li style="margin-right: 3px;">
                    <input id="search_text" type="text"  class="form-control" placeholder="รหัสสินค้า..."/>
                </li>
                <li><button id="button-search" type='button' class="btn btn-info" style="border-radius: inherit;">
                    <i class="fa fa-search"></i> ค้นหา</button>
                </li>
                <li>
                    <button id="button-reset" type='button' class="btn btn-warning" style=" margin-right: 15px;border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li>
                <li>
                    <a class="btn btn-success" href="<?php echo base_url('admin/installmentType/create');?>"  style="color: #ffffff; margin-bottom: inherit;margin-right: inherit; height: 87%;padding: 8px;border-radius: inherit;">
                        <i class="fa fa-plus"></i> เพิ่มประเภทการผ่อนชำระ
                    </a>
                </li>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <table id="installment-type-table" class="table table-striped jambo_table bulk_action" >
                    <thead>
                        <tr class="headings">
                            <th class="column-title" style="width: 3rem">ลำดับ</th>
                            <th class="column-title" style="width: 11rem">รหัสประเภทการผ่อนชำระ</th>
                            <th class="column-title" style="width: 6rem">รหัสสินค้า</th>
                            <th class="column-title">สินค้า</th>
                            <th class="column-title">จำนวนงวดที่ผ่อน</th>
                            <th class="column-title">ราคาผ่อนต่องวด</th>
                            <th class="column-title no-link last" style="text-align: center;width:15rem;"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = $('input[name="base_url"]').val();
    var searchEle = ['#search_text'];
    DrawInstallmentTypeList(base_url, searchEle, '#installment-type-table tbody');
    SearchInstallmentTypeList(base_url, '#button-search', searchEle, '#installment-type-table tbody');
    ResetInstallmentTypeList(base_url, '#button-reset', searchEle, '#installment-type-table tbody');
</script>
<!---- End Content ------>
  