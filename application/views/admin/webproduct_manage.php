<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .panel_toolbox>li> .btn-success:hover { background: #1c866f !important; }

    .img-preview-action{position: absolute; text-align: center; top: 0; left: 0; height: 100%; width: 100%; z-index: 2; opacity: 0; background-color: rgba(0, 0, 0, 0.36);}
    .img-preview-action:hover { opacity: 100; }
    .img-preview-action > ul > li {
        cursor: pointer;
        padding: 5px;
        float: left;
    }
    .img-preview-action > ul {
        margin: 0;
        padding-left: 0;
        text-align: center;
        padding-top: 40px;
        list-style: none;
        display: inline-block;
    }
    div.cazary {border: 1px solid #ced4da !important;}
        
</style>             
<!----  Content ------>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <h2>สินค้า รหัส :  <small><?=$productSet_id;?><span id='product-header'></span></small> </h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <input name="productSet_id" value="<?=$productSet_id;?>" type="hidden" >
            <!-- <ul class="nav navbar-right panel_toolbox"> -->
            <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li> -->
            <!-- </ul> -->
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal form-label-left">
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id" value="">
                            <input type="hidden" name="method" id="method" value="insert">
                            <input type="hidden" name="product_id" id="product_id" value="<?=$productSet_id;?>">
                            <div class="col-sm-12">
                                <label class="control-label" for="title">สินค้า <span class="required">*</span></label>
                                <input class="form-control" type="text" name="title" id="title">
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label" for="first-name">รายละเอียด <span class="required">*</span></label>
                                <textarea class="form-control" rows="15" id="detail" name="detail"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="control-label ">สถานะ การแสดงบนหน้าเว็บ</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="0">ไม่ แสดงบนหน้าเว็บ</option>
                                    <option value="1">แสดงบนหน้าเว็บ</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <a href="<?=base_url('admin/webProduct/')?>"><button type="button" class="btn btn-warning">กลับ</button></a>
                                <!-- <button type="button" class="btn btn-warning"  id="back-btn">กลับ</button> -->
                                <button type="button" class="btn btn-success"  id="save-btn">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
  
  <script>
    var base_url = $('input[name="base_url"]').val();
    var productSet_id = $('input[name="productSet_id"]').val();

    CKEDITOR.replace('detail');
    draw_form(base_url, productSet_id);
    function draw_form(base_url, productSet_id){
        var res = null;
        $.ajax({
            url: base_url+'admin/webproduct/product_get_once', //ทำงานกับไฟล์นี้
            data: {
                "productSet_id" : productSet_id
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });

        if(res.length > 0){
            $("#id" ).val(res[0].id);
            $("#method" ).val('update');
            $("#title" ).val(res[0].title);
            $("#detail" ).val(res[0].detail);
            $("#status").val(res[0].display_status).change();

        }else{
            console.log('xxxxxxxxxxxx');
        }
        return res;
    }

    $( "#save-btn" ).click(function() {
        var id = $("#id").val();
        var method = $("#method").val();
        var productSet_id = $("#product_id").val();
        var title = $("#title").val();
        var detail = CKEDITOR.instances['detail'].getData();
        var status = $("#status").val();
        actions(base_url, id, method, productSet_id, title, detail, status);
    });
    function actions(base_url, id, method, productSet_id, title, detail, status){
        $.ajax({
            url: base_url+'admin/webproduct/product_actions', //ทำงานกับไฟล์นี้
            data: {
                "id" : id,
                "method" : method,
                "productSet_id" : productSet_id,
                "title" : title,
                "detail" : detail,
                "status" : status
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                //res = data;
                window.location=data['datas'];
            },
            error: function(xhr, status, exception) {  console.log(exception); }
        });
    }

  </script>



<!---- End Content ------>
  