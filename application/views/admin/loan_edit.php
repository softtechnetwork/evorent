<style>
    .fail-stat-sale{color: #d9530e;}
    .succ-stat-sale{color: #26b99a!important;}
    .input-valid {
        border: 1px solid #fb48004d !important;
        box-shadow: 0px 0px 3px #ff470063 !important;
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ข้อมูลสินค้า <small><?=$resTemp[0]->temp_code; ?></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="field item form-group">
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>ลูกค้า : </strong><span><?=$resTemp[0]->firstname; ?> <?=$resTemp[0]->lastname; ?></span>
                    </div>
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>เบอร์โทรศัพท์มือถือ : </strong><span><?=$resTemp[0]->tel; ?></span>
                    </div>
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>อีเมล์ : </strong><span><?=$resTemp[0]->email; ?></span>
                    </div>
                </div>
                
                <div class="field item form-group">
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>สินค้า : </strong><span><?=$resTemp[0]->product_name; ?></span>
                    </div>
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>ราคาผ่อนต่องวด : </strong><span><?=$resTemp[0]->monthly_rent; ?></span>
                    </div>
                    <!--<div class="col-form-label col-md-2 col-sm-2">
                        <strong>จำนวน : </strong><span><?=$resTemp[0]->product_count; ?></span>
                    </div>-->
                </div>

                <div class="field item form-group">
                    <!--<div class="col-form-label col-md-4 col-sm-4">
                        <strong>หมายเลขเครื่อง : </strong><span><?//=json_decode($resTemp[0]->product_serial)->main->serial; ?></span>
                    </div>-->
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>ยี่ห้อ : </strong><span><?=$resTemp[0]->brand_name; ?></span>
                    </div>
                    <div class="col-form-label col-md-4 col-sm-4">
                        <strong>รุ่น : </strong><span><?=$resTemp[0]->product_version; ?></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-12 offset-md-12 label-align" >
                        <a href="<?php echo base_url('/admin/loan');?>" type='button' class="btn btn-success">Back</a>
                        <!--<button type='submit' class="btn btn-primary">Create</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ตารางการผ่อนชำระ</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="loan-edit-form" action="<?php echo site_url('/admin/loan/updatInstallment');?>" method="post" enctype="multipart/form-data" novalidate>
                    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <?php  if($resInst != null){?> 
                        <div class="form-group">
                            <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>งวด</th>
                                        <th>ราคาผ่อนบวก ภาษีมูลค่าเพิ่ม</th>
                                        <th>วันที่ครบกำหนดชำระ</th>
                                        <th>สถานะ</th><!-- ยังไม่จ่าย,จ่ายแล้ว, จ่ายล่าช้า--->
                                        <th>จำนวนที่ชำระ</th><!--จำนวนที่จ่าย--->
                                        <th>วันที่ชำระ</th>
                                        <th>หมายเหตุ</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                <?php 
                                foreach ($resInst as $value) {
                                    $installment_date = null;
                                    if($value->installment_date != null){
                                        $installment_date = date_format(date_create($value->installment_date),"d-m-Y");
                                    }

                                    ?>

                                    <tr>
                                    <?php  ?>
                                        <input type="hidden" id="installment-<?=$value->period; ?>" name="installment-<?=$value->period; ?>" value='<?php echo json_encode($value);?>' >
                                        <td style="text-align: center;">
                                            <?php  if( $value->status_code == 0  && $value->installment_payment > 0){ ?>
                                                <button id="period<?=$value->period;?>" type="button" class="btn btn-round btn-warning edit-button" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;">
                                                    <i class="fa fa-wrench"> Edit</i>
                                                </button>
                                            <?php } ?>
                                        </td>
                                        <td><?=$value->period; ?></td>
                                        <td><?=$value->installment_payment; ?></td>
                                        <td><?=date_format(date_create($value->payment_duedate),"d-m-Y") ?></td>
                                        <td><?=$value->status_label; ?></td>
                                        <td><?=$value->payment_amount; ?></td>
                                        <td><?=$installment_date; ?></td>
                                        <td><?=$value->remark; ?></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                            <input type="hidden" id="installment" name="installment"/>
                        </div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
</div>
<!---  Modal Edit form    ---->
<div id="edit-form" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="x_title" style="border-bottom: initial;"></div>
            <div class="x_content">
                <form id="instalment-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('/admin/loan/updatInstallment');?>">
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">งวด</label>
                        <div class="col-md- col-sm-9 ">
                            <input type="number" id="inst-period" name="inst-period" class="form-control" readonly>
                            <!--<input type="hidden" id="loan-code" name="loan-code" value="<?=$resTemp[0]->loan_code;?>">-->
                            <input type="hidden" id="temp-code" name="temp-code" value="<?=$resTemp[0]->temp_code;?>">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">จำนวนที่ชำระ</label>
                        <div class="col-md- col-sm-9 ">
                            <input type="number" step="0.01" id="inst-payment" name="inst-payment" required="required" class="form-control ">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">วันที่ชำระ</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input class="form-control col-md-5 datepicker" 
                            type="text" 
                            name="inst-date" 
                            id="inst-date" 
                            data-provide="datepicker" 
                            data-date-language="th-th"
                             autocomplete="off" 
                             placeholder="วว/ดด/ปป" 
                             style="padding: 0 3px; color: #9a999b;"
                             required="required">
                            <!--<input type="date" id="inst-date" name="inst-date" required="required" class="form-control"  value="<?php echo date('Y-m-d');?>">-->
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">สถานะ</label>
                        <div class="col-md-9 col-sm-9 ">
                            <select name='inst-status' id='inst-status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?=$item->status_code;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">หมายเหตุ</label>
                        <div class="col-md-9 col-sm-9 ">
                            <textarea  class="form-control" name='inst-remark'></textarea>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 label-align">
                            <button class="btn btn-primary" type="button" data-dismiss="modal" >Cancel</button>
                            <button id="installment-btm-submit" type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
<script>
    
$(".datepicker").datepicker().on('show', function(e){
    $('.prev').text('<');
        $('.next').text(">");
});
	var base_url = "<?php echo base_url(); ?>";
    var resTemp = "<?php echo $resTemp[0]->rental_period; ?>";
    var urlgetTempOne = base_url+"admin/loan/getTempOne";
    
        //################  edit button  #############//

    for (let index = 1; index <= parseInt(resTemp); index++) {
       
        $('#period'+index).click(function (e) {
            var obj = JSON.parse($('#installment-'+index).val());
            $('#edit-form').modal('show');
            $('#inst-period').val(index);
            $('#inst-payment').val(obj['installment_payment']);
        });
    }

</script>


