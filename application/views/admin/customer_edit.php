
<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 1px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.ihabitedSelect{background-color: #1ABB9C !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขข้อมูลลูกค้า <small></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>-->
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php //print_r($res_customer[0]);  ?>
                <form id="customer-edit-form" action="<?php echo base_url('/admin/customer/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">รหัสลูกค้า</label>
                        <div class="col-md-6 col-sm-6">
                          <input name="customer_code" value="<?=$res_customer[0]->customer_code;?>"  type="text"  class="form-control" readonly>
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขบัตรประชาชน</label>
                        <div class="col-md-6 col-sm-6">
                          <input name="idcard" value="<?php echo $res_customer[0]->idcard;?>"  type="number"  class="form-control" readonly>
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="name" name="name" value="<?php echo $res_customer[0]->firstname;?>" required="required" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นามสกุล</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="sname" name="sname" value="<?php echo $res_customer[0]->lastname;?>" required="required" />
                        </div>
                    </div>
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <div class="col-md-8 col-sm-8 ">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <th>เลือก</th>
                                        <th>ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($inhabited as $item) : ?>
                                    <tr class="">
                                        <td style="  padding: .5rem;">
                                            <input class="inhabitedInput" type='radio'  name='inhabited' id='inhabited<?=$item->id?>' value='<?=$item->inhabited_code?>' <?php if($res_customer[0]->addr == $item->inhabited_code){echo 'checked="checked"';} ?>/> 
                                        </td>
                                        <td><?=$item->address?></td>
                                        <td><?=$item->district_name?></td>
                                        <td><?=$item->amphur_name?></td>
                                        <td><?=$item->province_name?></td>
                                        <td><?=$item->zip_code?></td>


                                    </tr>
                                   <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <!--<div class="col-md-6 col-sm-6">
                            <select name='inhabited' id='inhabited' class="form-control">
                                <?php foreach ($inhabited as $item) : ?>
                                    <option value="<?php echo $item->inhabited_code; ?>" <?php if($res_customer[0]->addr == $item->inhabited_code){echo 'selected="selected"';} ?>><?php echo $item->category; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                        -->
                        <div class="col-md-8 col-sm-8">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <th style=" width: 9rem;">ประเภทที่อยู่</th>
                                        <th style=" width: 18rem;">ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'><?php echo $res_customer[0]->addr;?></textarea>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จังหวัด</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='province' id='province' class="form-control">
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res_customer[0]->province_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->province_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อำเภอ / เขต</label>
                        <div class="col-md-6 col-sm-6">
                            <select  id='amphurs' name='amphurs' class="form-control">
                                <option value="" selected='false' disabled>อำเภอ / เขต</option>	
                                <?php foreach ($amphurs as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res_customer[0]->amphurs_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->amphur_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ตำบล / แขวง</label>
                        <div class="col-md-6 col-sm-6">
                            <select id='district' name='district' class="form-control">
                                <option value="" selected='false' disabled>ตำบล / แขวง</option>	
                                <?php foreach ($district as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res_customer[0]->tumbon_id == $item->id){echo 'selected="selected"';} ?>><?php echo $item->district_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสไปรษณีย์</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" value="<?php echo $res_customer[0]->zip_code;?>" readonly>
                            <input class="form-control" value="<?php echo $res_customer[0]->zip_code;?>" type="number" class='number' name="zipcode" data-validate-minmax="0,5" readonly>////////
                        </div>
                    </div>-->

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายได้ต่อเดือน</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="number" min="1" class="form-control" name="monthly-income" id="monthly-income"  value="<?=$res_customer[0]->monthly_income; ?>"/>
                            <!--<select class="form-control" name="monthly-income"  id="monthly-income" required="required" >
                                <option value="" selected='false' disabled>กรุณาระบุรายได้ของคุณ</option>
                                <?php foreach ($monthlyIncome as $id => $item) : ?>
                                    <option value="<?= $id;?>" <?php if($res_customer[0]->monthly_income == $id){echo 'selected="selected"';} ?>><?= $item;?></option>
                                <?php endforeach ?>
                            </select>-->
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อาชีพ</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="career-select"  id="career-select" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกอาชีพของคุณ</option>
                                <?php foreach ($career as $id => $item) : ?>
                                    <option value="<?= $id;?>" datas="<?= $item;?>" <?php if($res_customer[0]->career == $id){echo 'selected="selected"';} ?>><?= $item;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input  id="career" class="form-control" type="text" name="career" value="<?=$res_customer[0]->career_text; ?>"/>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันเกิด</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control datepicker"  value="<?=date("d/m/Y", strtotime($res_customer[0]->birthday));?>"   type="text" id="bdate" name="bdate"  data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขมือถือ</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel" name="tel" value="<?php echo $res_customer[0]->tel;?>" type="tel" maxlength="10" class="form-control" required="required" >
                            <!--<input name="tel" value="<?php echo $res_customer[0]->tel;?>"  type="text" class="form-control" data-inputmask="'mask' : '999-999-9999'">-->
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="tel-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อีเมล์<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="<?php echo $res_customer[0]->email;?>" id="email"  name="email" class='email' required="required" type="email" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เพศ</label>
                        <div class="col-md-6 col-sm-6">
                            <p style="padding-top: 7px;">
                                <label style=" margin-right: 3px;">ชาย:</label>
                                <input type="radio" class="flat" name="gender" id="genderM" value="M" required <?php if($res_customer[0]->sex == "M"){  echo "checked"; }?> /> 
                                <label style=" margin-left: 10px; margin-right: 3px;">หญิง:</label>
                                <input type="radio" class="flat" name="gender" id="genderF" value="F" <?php if($res_customer[0]->sex == "F"){  echo "checked"; }?>/>
                            </p>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> ประเภทลูกค้า</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='customerType' id='customerType' class="form-control">
                                <?php foreach ($customerType as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"<?php if($res_customer[0]->customer_type == $item->id){echo 'selected="selected"';} ?>><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สถานะ</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res_customer[0]->status == $item->id){echo 'selected="selected"';} ?>><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> หมายเหตุ</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='remark' name='remark'><?=$res_customer[0]->remark;?></textarea>
                        </div>
                    </div>

                    <input type="hidden" name="customer_code" value="<?php echo $res_customer[0]->customer_code; ?>">
                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                <a href="<?php echo base_url('admin/customer');?>" type='button' class="btn btn-success">Back</a>
                                <button type='submit' class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
     
    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
            $('.next').text(">");
    });
    // input id create form 
    var element_id = ["#name","#sname","#tel","#monthly-income","#career-select","#career"];
    //var input_id = ["#name","#sname","#address","#province","#amphurs","#district","#tel","#email"];

    // input premise id create form 
    var preise = ["contract1","contract2","contract3","contract4","idcardPremise","houseregisPremise","idcardSupporter","houseregisSupporter"];

    // base url
    var base_url = $('input[name="base_url"]').val();
    var thumnails_id = '#thumnails-contract1, #thumnails-contract2, #thumnails-contract3, #thumnails-contract4, #thumnails-idcardPremise, #thumnails-houseregisPremise, #thumnails-idcardSupporter, #thumnails-houseregisSupporter' ;

    CustomerEditValidate(element_id, base_url);
    CustomerEditSubmit(element_id, '#customer-edit-form');

    CurrentInhabiteds(base_url, "<?=$res_customer[0]->customer_code;?>",'#address-table tbody');  // replace Current address
    
    career('#career-select','#career');
</script>


