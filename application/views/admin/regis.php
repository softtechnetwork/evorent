<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hirepurchase</title>
    
    <link href="<?php echo base_url('./assete/admin/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('./assete/admin/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('./assete/admin/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url('./assete/admin/build/css/custom.min.css');?>" rel="stylesheet">  


   
  </head>

  <body class="login">
    <div>
      

      <div class="login_wrapper">
      
        <!--<div id="register" class="animate form registration_form">-->
        <div id="register" class="animate form">
        
          <section class="login_content">
            <form  method="post" action="<?php echo base_url('admin/user/validate_regis');?>">
            
              <h1>Create Account</h1>
              <div class="col-12">
                <input type="text" class="form-control" name="Username" placeholder="Username" required="" value="<?php echo set_value('Username');?>"/>
              </div>
              <div class="col-12">
                <input type="email" class="form-control" name="Email" placeholder="Email" required="" value="<?php echo set_value('Email');?>"/>
              </div>
              <div class="col-12">
                <input type="password" class="form-control" name="Pass" placeholder="Password" required="" value="<?php echo set_value('Pass');?>"/>
              </div>
              <div class="col-12">
                <?php  
                    if(!empty($success_msg)){ 
                        echo '<p class="status-msg success">'.$success_msg.'</p>'; 
                    }elseif(!empty($error_msg)){ 
                        echo '<p class="status-msg error">'.$error_msg.'</p>'; 
                    } 
                ?>
              </div>
              <div class="col-12">
                <button class="btn btn-default submit" type="submit" >Register</button>
               <!-- <a class="btn btn-default submit" href="index.html">Submit</a>-->
              </div>

              <div class="clearfix"></div>

              <!--<div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>-->
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
