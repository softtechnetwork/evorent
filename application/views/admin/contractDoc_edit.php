<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขรายละเอียด เอกสารสัญญา</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSet-create" action="<?php echo site_url('/admin/contractDoc/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เลือกประเภท สัญญา(เอกสารสัญญา)</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="contract_type"  id="contract_type">
                                <?php foreach ($contractType as $item) : ?>
                                    <option value="<?=$item->id;?>" <?php if($res[0]->contract_type == $item->id){echo 'selected="selected"';} ?>><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="province-current"> ชื่อ (เอกสารสัญญา)</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" name="contract_name" id="contract_name" value="<?=$res[0]->contract_name;?>"/>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด (เอกสารสัญญา)</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="contract_detail" name="contract_detail" class="form-control" rows="4" cols="50"><?=$res[0]->contract_detail;?></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สถานะการใช้งาน</label>
                        <div class="col-md-2 col-sm-2">
                            <select class="form-control" name="use_status"  id="use_status">
                                <?php foreach ($useStatus as $item) : ?>
                                    <option value="<?=$item->id;?>" <?php if($res[0]->use_status == $item->id){echo 'selected="selected"';} ?>><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <input type="hidden" name="id" id="id" value="<?=$res[0]->contract_id;?>"/>

                            <a href="<?php echo base_url('/admin/contractDoc');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">แก้ไข (รายละเอียด เอกสารสัญญา)</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!--
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>เอกสาร สัญญา</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php foreach ($contractImg as $item) : ?>
                    <div class="col-md-2 col-sm-2">
                        <img class="thumnails-premise" 
                            id="<?=$item->id;?>" 
                            src="<?=base_url($item->path);?>"
                            alt="image" 
                            style="border: 1px #08080759 solid; width:100%;"
                            onclick="contractDocImgModal('#ModalcontractDocImg', '<?=$item->name;?>', '<?=base_url($item->path);?>')">
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
-->

<!--  Modal -->
<div class="modal fade" id="ModalcontractDocImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalcontractDocImgTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="thumnails-premise" id="contractDocImg" src="" alt="image" style="border: 1px #08080759 solid; width:100%;">
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>

<script>
    function contractDocImgModal(modalID, title, imgPath) {
        $('#ModalcontractDocImgTitle').html(title);
        $('#contractDocImg').attr('src', imgPath);
        $(modalID).modal();
    }
</script>
