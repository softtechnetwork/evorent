<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไข ข้อมูลการติดต่อจากหน้าเว็บ EVORENT <small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="" action="<?php echo site_url('/admin/webContact/update');?>" method="post"  enctype="multipart/form-data" >
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label  label-align">ชื่อ</label>
                            <input class="form-control" value="<?php echo $res[0]->name;?>" readonly />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label  label-align">นามสกุล</label>
                            <input class="form-control" value="<?php echo $res[0]->sname;?>" readonly />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label  label-align">หมายเลขโทรศัพท์มือถือ</label>
                            <input class="form-control" value="<?php echo $res[0]->tel;?>" readonly />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label  label-align">อีเมล์</label>
                            <input class="form-control" value="<?php echo $res[0]->email;?>" readonly />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-6 col-sm-6">
                            <label class="col-form-label label-align">รายละเอียด</label>
                            <textarea  class="form-control" readonly><?php echo $res[0]->message;?></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-6 col-sm-6">
                            <label class="col-form-label label-align">สถานะ</label>
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res[0]->status == $item->id){echo 'selected="selected"';} ?>><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="<?php echo $res[0]->id; ?>">
                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                                
                                <a href="<?php echo base_url('/admin/webContact');?>" class="btn btn-primary">กลับ</a>
                                <button type='submit' class="btn btn-success">แก้ไข ข้อมูลการติดต่อจากหน้าเว็บ EVORENT</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

