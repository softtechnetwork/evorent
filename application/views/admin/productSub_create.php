<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
.btn-light {
    color: #212529bd;
    background-color: #f8f9fa00;
    border-color: #d5d9dc;
    border-radius: inherit !important;
}
.mh .dropdown-menu { max-height: 200px;}
.dropdown-item.active, .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #1abb9c;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>สร้างสินค้าย่อย</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-productSub-create" action="<?php echo site_url('/admin/productSub/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <select name="product"  id="product"  class="selectpicker form-control mh " data-live-search="true">
                                <option value="" selected='false' disabled>กรุณาเลือกสินค้า</option>
                                <?php foreach ($product as $item) : ?>
                                    <option value="<?= $item->product_id;?>"><?= $item->product_name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-name" id="productSub-name" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รุ่น</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-version" id="productSub-version" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสอ้างอิงสินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="text" class="form-control" name="productSub-ref" id="productSub-ref" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียดยี่สินค้าย่อย</label>
                        <div class="col-md-6 col-sm-6  ">
                            <textarea id="productSub-detail" name="productSub-detail" class="form-control" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/productSub');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">สร้างสินค้าย่อย</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var RequireEle = ["#product","#productSub-name","#productSub-version","#productSub-ref"];
    ProductSubCreateSubmit(RequireEle, "#form-productSub-create");
    ProductSubEleChange(RequireEle); 
</script>


