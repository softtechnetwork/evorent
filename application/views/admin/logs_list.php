<style>

.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
.btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}</style>             

<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
        <h2>Customer<small></small></h2>
        <!--<form id="search-form" action="<?php echo base_url('/admin/customer');?>" method="post"  enctype="multipart/form-data" novalidate>-->
            <input name="base_url" value="<?php echo base_url();?>" type="hidden" >
            <ul class="nav navbar-right panel_toolbox">
                <li style="margin-right: 3px;">
                    <select name='Logs-Search-Cate' id='Logs-Search-Cate' class="form-control">
                        <option value="" selected>All Category</option>	
                        <option value="Customer">Customer</option>	
                        <option value="Temp" >Temp</option>
                    </select>
                </li>
                <li style="margin-right: 3px;">
                    <select name='Logs-Search-type' id='Logs-Search-type' class="form-control">
                        <option value="logs_code" selected>Logs id </option>	
                        <option value="logs" >Logs</option>
                        <option value="create_logs" >Create Logs</option>
                    </select>
                </li>
                <li style="margin-right: 3px;" id="logText">
                    <input name="logsSearchText" id="logsSearchText"value=""  type="text"  class="form-control" >
                </li>
                <li style="margin-right: 3px;" id="logDateRang">
                    <form class="">
                        <fieldset>
                            <div class="control-group ">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <input type="text" name="reservation" id="reservation" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </li>
                <li><button id="logs-button-search" type='button' class="btn btn-info" style="border-radius: inherit;">Search</button></li>
                <li>
                    <button id="logs-button-reset" type='button' class="btn btn-warning" style="border-radius: inherit;">
                    <i class="fa fa-refresh"></i> Reset</button>
                </li>
                </li>
            </ul>
        <!--</form>-->
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="table-responsive">
            <table id="logs_res" class="table table-striped jambo_table bulk_action" >
                <thead>
                <tr class="headings">
                    <th class="column-title">ลำดับ</th>
                    <th class="column-title">Logs id </th>
                    <th class="column-title">Logs Category </th>
                    <th class="column-title">Logs Update </th>
                    <th class="column-title">Create Logs </th>
                    <th class="column-title" style="text-align: center;">Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        
        <input id="AllItems" value="" type="hidden" /> 
        <input id="page" value="1" type="hidden" /> 
        <div id="paginations"></div>
            
    </div>
    </div>
</div>
<script src="<?php echo base_url('assete/js/page/admin/customer_list.js');?>"></script>
<script>
var base_url = $('input[name="base_url"]').val();
var url_getRes = base_url+"admin/logs/getRes";
var CItemPerPage = 30;

//################  Display Logs Search  ################//
$('#logText').show(); 
$('#logDateRang').hide();
$('#Logs-Search-type').change(function(){
    switch(this.value){
        case 'create_logs': $('#logText').hide();$('#logDateRang').show(); break;
        default: $('#logText').show(); $('#logDateRang').hide(); break;
    }
});

//################  Get data  ################//
getLogsRes(base_url,url_getRes,null,null,null,CItemPerPage,1,CItemPerPage);

var url_getAll = base_url+"admin/customer/getResAll";
getCustomerAll(base_url,url_getAll,null,null,null);
pagination();

//################  Search  ################//
$('#logs-button-search').click(function(){
    var cate = $('#Logs-Search-Cate').val();
    var type = $('#Logs-Search-type').val();
    var searchText = $('#logsSearchText');
    var searchDateRang = $('#reservation');
    
    //---- valida input -------//
    if(type == "create_logs"){
        var res = validateInput(searchDateRang.selector,searchDateRang.val());
        if(res){
            getLogsRes(base_url, url_getRes, cate, type, searchDateRang.val(),CItemPerPage,1,CItemPerPage);
            getCustomerAll(base_url,url_getAll,cate, type, searchDateRang.val());
        }
    }else{
        var res = validateInput(searchText.selector,searchText.val());
        if(res){
            getLogsRes(base_url, url_getRes, cate, type, searchText.val(),CItemPerPage,1,CItemPerPage);
            getCustomerAll(base_url,url_getAll,cate, type, searchText.val());
        }
    }
    pagination();
});
$('#logs-button-reset').click(function(){
    $('#Logs-Search-Cate').val('');
    $('#Logs-Search-type').val("logs_code");

    $('#logText').show(); 
    $('#logDateRang').hide();
    $('#logsSearchText').removeClass("input-valid");;
    $('#logsSearchText').val(null);
    //$('#reservation').removeClass("input-valid");
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    $('#reservation').val(today+"-"+today);
    getLogsRes(base_url,url_getRes,null,null,null,CItemPerPage,1,CItemPerPage);
    getCustomerAll(base_url,url_getAll,null,null,null);
    pagination();
});

function getLogsRes(base_url, url_getRes, cate, type, val, itemPerPage,itemStt,itemEnd){
    var res = null;
    $.ajax({
        url: url_getRes, //ทำงานกับไฟล์นี้
        data: {
            "cate" : cate, 
            "type" : type, 
            "val" : val,

            "itemPerPage" : itemPerPage, 
            "itemStt" : itemStt,
            "itemEnd" : itemEnd
        },  //ส่งตัวแปร
        type: "POST",
        dataType: 'json',
        async:false,
        success: function(data, status) {
            console.log(data);
            var tr = null;
            if(data.length > 0){
                var num = 1;
                $.each(data, function (i, val) {
                    tr += '<tr class="even pointer">';
                    tr += '<td class="a-center ">'+val['RowNum']+'</td>';
                    tr += '<td class=" ">'+val['logs_code']+'</td>';
                    tr += '<td class=" ">'+val['category']+'</td>';
                    tr += '<td class=" ">'+val['logs']+'</td>';
                    tr += '<td class=" ">'+val['create_logs']+'</td>';
                    //tr += '<td class=" last"  style="text-align: center;">';
                    //tr +=   '<a href="'+base_url+'/admin/customer/detail/'+val['logs_code']+' ">';
                    //tr +=       '<button type="button" class="btn btn-round btn-info" style=" font-size: 13px; padding: 0 15px; margin-bottom: inherit;style="text-align: center;"><i class="fa fa-file-text-o"></i> View</button>';
                    //tr +=   '</a>';
                    //tr += '</td>';
                    tr += '</tr>';
                    num++;
                });
            }

            $("#logs_res tbody").html(tr);
        },
        error: function(xhr, status, exception) { 
            //console.log(xhr);
        }
    });
}

function validateInput(id,val){
    var status = true;
    if(val == ''){ 
        $(id).addClass("input-valid");
        status = false;
    }else{
        $(id).removeClass("input-valid");
    }
    return status;
}



//################  get All Items  ################//
    function getCustomerAll(base_url, url_getAll, cate, type, val){
        $.ajax({
            url: url_getAll, //ทำงานกับไฟล์นี้
            data: { 
                "cate" : cate, 
                "type" : type, 
                "val" : val
            },  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) { $("#AllItems").val(data[0].allitems); },
            error: function(xhr, status, exception) {  }
        });
    }

    function pagination(){
        $('#paginations').html(null);
        var CAllItem = $("#AllItems").val();
        var Cpage = Math.ceil(CAllItem/CItemPerPage) //ปัดขึ้น;
        //var Cpage = 12 //ปัดขึ้น;
        //console.log('kkkkkkk');

        //-----------html pagination------------//
        // paginationHTML(Cpage, 1);
        $('#paginations').html(null);

        var btnPage = '<button id="suspend-left"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-left"></i></button>';
        for (let index = 1; index <= Cpage; index++) {
            btnPage += '<button id="'+index+'"  type="button" class="btn btn-warning btn-paginations ">'+index+'</button>';
        }
        btnPage += '<button id="suspend-next"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-right"></i></button>';
        $('#paginations').append(btnPage);

        $('#1').addClass('focus-paginations');
        if(Cpage > 1){ $("#suspend-next").removeAttr('disabled');}

        //-----------btn click------------//
        $('.btn-paginations').click(function(){
            var paged = parseInt($('#page').val());
            switch(this.id){
                case'suspend-left': paged = parseInt(paged)-1; break;
                case'suspend-next':paged = parseInt(paged)+1; break;
                default: paged = parseInt(this.id); break;
            }

            if(paged > 1){ $("#suspend-left").removeAttr('disabled');}
            if(paged == 1){ $("#suspend-left").attr('disabled','disabled');}
            if(paged == Cpage){ $("#suspend-next").attr('disabled','disabled');}
            if(paged < Cpage){ $("#suspend-next").removeAttr('disabled');}

            $('#page').val(paged);

            if(paged == parseInt($('#page').val())){ $('.btn-paginations').removeClass('focus-paginations'); $('#'+paged).addClass('focus-paginations');}
            
            var itemStt = ((CItemPerPage*paged)-CItemPerPage)+1;
            var itemEnd = CItemPerPage*paged;
            
            var cate = $('#Logs-Search-Cate').val();
            var type = $('#Logs-Search-type').val();
            var searchText = $('#logsSearchText');
            var searchDateRang = $('#reservation');
            
            //---- valida input -------//
            if(type == "create_logs"){
                getLogsRes(base_url, url_getRes, cate, type, searchDateRang.val(),CItemPerPage,itemStt,itemEnd);
               
            }else{
                getLogsRes(base_url, url_getRes, cate, type, searchText.val(),CItemPerPage,itemStt,itemEnd);
            }

        });
    }

    function paginationHTML( Cpage, page){
        $('#paginations').html(null);

        var btnPage = '<button id="suspend-left"  type="button" class="btn btn-success btn-paginations" disabled><i class="fa fa-chevron-left"></i></button>';
        for (let index = 1; index <= Cpage; index++) {
            btnPage += '<button id="'+index+'"  type="button" class="btn btn-warning btn-paginations ">'+index+'</button>';
        }
        btnPage += '<button id="suspend-next"  type="button" class="btn btn-success btn-paginations"><i class="fa fa-chevron-right"></i></button>';
        $('#paginations').append(btnPage);

        $('#1').addClass('focus-paginations');
    }




</script>


<!---- End Content ------>
  