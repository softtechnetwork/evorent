<style> 
    .btn-paginations{padding: .1rem .4rem .1rem .3rem;border-radius: 0px; color:white;} 
    .focus-paginations{box-shadow: 1px 1px 0px 0px #2a3f54;color: #26b99a;}
    .btn-light {
        color: #212529bd;
        background-color: #f8f9fa00;
        border-color: #d5d9dc;
        border-radius: inherit !important;
    }
    .mh .dropdown-menu { max-height: 200px;}
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #1abb9c;
    }
    .btn.disabled, .btn:disabled {
        opacity: .25 !important;
        background-color: #565656 !important;
        border: 1px solid #565656 !important;
    }
    .table td, .table th {
        padding: .5rem .7rem;
    }
</style>             
<!----  Content ------>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>ข้อมูลการผ่อนชำระ สัญญา : <?=$contract_code?> <small>ส่วนเสริมสัญญา : <?=$addon_code?></small></h2>
            <input name="base_url" value="<?=base_url();?>" type="hidden" >
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class ="row">
                <div class ="col-md-2">
                    <label for="reportStatus"><strong>สถานะ</strong></label>
                    <select id="reportStatus" name="reportStatus" class="selectpicker form-control mh " data-live-search="true">
                        <option value="">เลือก สถานะ</option>
                        <?php  foreach($getStatus as $item) :?>
                            <option value="<?=$item->status_code;?>"> <?=$item->label;?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class ="col-md-2">
                    <label for="overdue"><srtrong>การค้างชำระ</srtrong></label>
                    <select id="overdue" name="overdue" class="selectpicker form-control mh"  data-live-search="true">
                        <option value="">เลือก การค้างชำระ</option>
                        <option value="10">ค้างชำระ</option>
                        <option value="0">ครบกำหนดชำระแล้ว</option>
                        <option value="1">ก่อนครบกำหนดชำระ 1 วัน</option>
                        <option value="2">ก่อนครบกำหนดชำระ 2 วัน</option>
                        <option value="3">ก่อนครบกำหนดชำระ 3 วัน</option>
                        <option value="4">ก่อนครบกำหนดชำระ 4 วัน</option>
                        <option value="5">ก่อนครบกำหนดชำระ 5 วัน</option>
                    </select>
                </div>
                <div class ="col-md-4">
                    <label for="reservation"> <strong>วันที่ครบกำหนดชำระ</strong></label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stduedate" id="stduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="stduedate" id="stduedate" class="col-md-5 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="enduedate" id="enduedate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="enduedate" id="enduedate" class="col-md-6 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                    </div>
                </div>
                <div class ="col-md-4">
                    <label for="reservation"><strong>วันที่ชำระ</strong></label>
                    <div class="">
                        <input class="form-control col-md-5 datepicker" type="text" name="stinstallmentdate" id="stinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="stinstallmentdate" id="stinstallmentdate" class="col-md-4 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                        <label class="col-md-1" style="margin-top: .5rem;text-align: center;">ถึง</label>
                        <input class="form-control col-md-6 datepicker" type="text" name="endinstallmentdate" id="endinstallmentdate" data-provide="datepicker" data-date-language="th-th" autocomplete="off" placeholder="วว/ดด/ปป" style="padding: 0 3px; color: #9a999b;">
                        <!--<input type="date" name="endinstallmentdate" id="endinstallmentdate" class="col-md-6 form-control" value="" style="padding: 0 3px; color: #9a999b;">-->
                    </div>
                </div>
            </div>
            <div class ="row ">
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <a href="<?=base_url('admin/contractaddon/'.$contract_code)?>" class="btn btn-success" style="border-radius: inherit;width: 100%;" type="button">
                        <i class="fa fa-reply"></i> กลับ
                    </a>
                </div>
                <div class ="col-md-1  navbar-right " style=" padding-top: 1.65rem;">
                    <input id="reportTemp" name="reportTemp" type="hidden" value="<?=$addon_code?>">
                    <button id="report-search" type='button' class="btn btn-info" style="border-radius: inherit;width: 100%;">
                        <i class="fa fa-search"></i> ค้นหา
                    </button>
                </div>
                <div class ="col-md-1  navbar-right "  style=" padding-top: 1.65rem;">
                    <button id="report-reset" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>ตารางการผ่อนชำระ</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div id="table-place">
                <div class="table-responsive">
                    <table id="report_res" class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th class="column-title"><small>งวด/เดือน</small></th>
                            <th class="column-title"><small>ราคาผ่อน</small></th>
                            <th class="column-title"><small>วันที่ครบชำระ</small></th>
                            <th class="column-title"><small>วันที่สิ้นสุดชำระ</small></th>
                            <th class="column-title"><small>การค้างชำระ</small></th>
                            <th class="column-title"><small>จำนวนที่ชำระ</small></th>
                            <th class="column-title"><small>ส่วนต่าง</small></th>
                            <th class="column-title"><small>วันที่ชำระ</small></th>
                            <th class="column-title"><small>หมายเหตุ</small></th>
                            <th class="column-title" style='text-align: center;'><small>สถานะ</small></th>
                            <th class="column-title" style='text-align: center;'><small>กิจกรรม</small></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!---  Modal Edit form    ---->
<div id="edit-form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="x_title" style="border-bottom: initial;"></div>
            <div class="x_content" style="padding: .1rem 1rem;">
                <form id="instalment-form" method="post" enctype="multipart/form-data" action="<?=base_url('admin/updateInstallments');?>">
                    <div class="item form-group">
                        <!--<label class="col-form-label col-md-3 col-sm-3 label-align">งวด</label>-->
                        <div class="col-md-6">
                            <label class="col-form-label label-align">รหัสสัญญา</label>
                            <input type="text" id="contract_code" name="contract_code" class="form-control" readonly>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label label-align">งวดที่</label>
                            <input type="number" id="inst-period" name="inst-period" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-6">
                            <label class="col-form-label label-align">จำนวนที่ชำระ</label>
                            <input type="number" step="0.01" id="inst-payment" name="inst-payment" required="required" class="form-control ">
                            <input type="hidden" id="payment" name="payment">
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label label-align">วันที่ชำระ</label>
                            <input class="form-control datepicker" 
                            type="text" 
                            name="inst-date" 
                            id="inst-date" 
                            data-provide="datepicker" 
                            data-date-language="th-th"
                             autocomplete="off" 
                             placeholder="วว/ดด/ปป" 
                             style="padding: 0 3px; color: #9a999b;"
                             required="required">
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <label class="col-form-label label-align">สถานะ</label>
                            <select name='inst-status' id='inst-status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?=$item->status_code;?>"><?=$item->label;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <label for="middle-name" class="col-form-label label-align">หมายเหตุ</label>
                            <textarea  class="form-control" name='inst-remark'></textarea>
                        </div>
                    </div>
                    
                    <!--<div class="ln_solid"></div>-->
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 label-align">
                            <input type="hidden" id="direct-url" name="direct-url" value="admin/AddonInstallments/<?=$contract_code?>/<?=$addon_code?>">
                            <button class="btn btn-primary" type="button" data-dismiss="modal" >ยกเลิก</button>
                            <button id="installment-btm-submit" type="submit" class="btn btn-success">แก้ไข</button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<!---  Modal send message    ---->
<div id="message-form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="x_content" style="padding: 0 1rem;">
                <form id="instalment-form" method="post" enctype="multipart/form-data" action="<?=base_url('admin/sendSms');?>">
                
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12" style=" margin-top: 1rem;">
                            <h5 class="modal-title">ข้อมูลก่อนส่ง SMS</h5>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 ">
                            <label class="col-form-label label-align">ลูกค้า</label>
                            <input type="text" id="customer-sms" name="customer-sms" class="form-control" readonly>
                            <input type="hidden" id="customer-code" name="customer-code">
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-4 col-sm-4 ">
                            <label class="col-form-label label-align">เลขที่สัญญา</label>
                            <input type="text" id="temp-code-sms" name="temp-code-sms" class="form-control" readonly>
                        </div>
                        <div class="col-md-3 col-sm-3 ">
                            <label class="col-form-label label-align">งวดที่</label>
                            <input type="number" id="inst-period-sms" name="inst-period-sms" class="form-control" readonly>
                        </div>
                        <div class="col-md-5 col-sm-5 ">
                            <label class="col-form-label label-align">จำนวนที่ต้องชำระ</label>
                            <input type="number" id="inst-payment-sms" name="inst-payment-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        
                        <div class="col-md-4 col-sm-4 ">
                            <label class="col-form-label label-align"> วันที่ครบกำหนดชำระ</label>
                            <input type="text" id="duedate-sms" name="duedate-sms"  class="form-control" readonly>
                        </div>
                        <div class="col-md-8 col-sm-8 ">
                            <label class="col-form-label label-align"> หมายเลขโทรศัพท์มือถือ</label>
                            <input type="text" id="tel-sms" name="tel-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 ">
                            <label class="col-form-label label-align">หัวข้อ SMS</label>
                            <input type="text" id="keywords-sms" name="keywords-sms"  class="form-control" readonly>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 label-align">
                            <button class="btn btn-primary" type="button" data-dismiss="modal" >ยกเลิก</button>
                            <button id="sms-btm-submit" type="submit" class="btn btn-success">ส่ง SMS</button>
                            <input type="hidden" id="direct-url" name="direct-url" value="admin/AddonInstallments/<?=$contract_code?>/<?=$addon_code?>">
                            <input type="hidden" id="cate" name="cate" value='<?=$this->uri->segment(1).'/'.$this->uri->segment(2)?>'>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<script>

    $(".datepicker").datepicker().on('show', function(e){
        $('.prev').text('<');
            $('.next').text(">");
    });
    var base_url = $('input[name="base_url"]').val(); 
    var searchArray = {};

    $('#stduedate').change(function () {
        $('#enduedate').attr("min",$('#stduedate').val());
    });
    $('#enduedate').change(function () {
        $('#stduedate').attr("max",$('#enduedate').val());
    });

    $('#stinstallmentdate').change(function () {
        $('#endinstallmentdate').attr("min",$('#stinstallmentdate').val());
    });
    $('#endinstallmentdate').change(function () {
        $('#stinstallmentdate').attr("max",$('#endinstallmentdate').val());
    });

    //--- installment table 
    var res = getContractInsRes($("#reportTemp").val(), null);
    console.log(res);
    var elementtable = "#report_res tbody";
    getContractInsTeble(res, elementtable);

    $("#report-reset").click(function () {
        searchArray = {};
        $('#reportCus ').val(null);
        $('[data-id=reportCus] .filter-option-inner-inner').html('เลือก รหัสลูกค้า');

        //$('#reportTemp ').val(null);
        //$('[data-id=reportTemp] .filter-option-inner-inner').html('เลือก รหัสสัญญา');

        $('#reportStatus ').val(null);
        $('[data-id=reportStatus] .filter-option-inner-inner').html('เลือก สถานะ');
        
        $('#reportName ').val(null);
        $('#stduedate ').val(null);
        $('#enduedate ').val(null);

        $('#stduedate').removeAttr("min");
        $('#stduedate').removeAttr("max");

        $('#enduedate').removeAttr("min");
        $('#enduedate').removeAttr("max");

        $('#stinstallmentdate ').val(null);
        $('#endinstallmentdate ').val(null);

        $('#endinstallmentdate').removeAttr("min");
        $('#endinstallmentdate').removeAttr("max");

        $('#stinstallmentdate').removeAttr("min");
        $('#stinstallmentdate').removeAttr("max");

        $('#overdue').val(null);
        $('[data-id=overdue] .filter-option-inner-inner').html('เลือก การค้างชำระ');

        //--- installment table 
        var res = getContractInsRes($("#reportTemp").val(), null);
        var elementtable = "#report_res tbody";
        getContractInsTeble(res, elementtable);
    });

    function getContractInsTeble(res, elementtable){
        var td = null;
        $(elementtable).html(null);
        $.each(res, function (i, val) {
            var styles = '';
            var SMSkeyword = '';
            switch(val.overdue_code){
                case 10: styles =' style="color: #ec0505;" '; SMSkeyword ='บริการแจ้งเตือน '+val.overdue;break;
                case 5: styles =' style="color: #ec3e05;" '; SMSkeyword ='บริการแจ้งยอด ก่อนครบกำหนดชำระ 5 วัน ';break;
                case 4: styles =' style="color: #ec6805;" '; SMSkeyword ='บริการแจ้งยอด ก่อนครบกำหนดชำระ 4 วัน ';break;
                case 3: styles =' style="color: #ec7e05;" '; SMSkeyword ='บริการแจ้งยอด ก่อนครบกำหนดชำระ 3 วัน ';break;
                case 2: styles =' style="color: #ec9305;" '; SMSkeyword ='บริการแจ้งยอด ก่อนครบกำหนดชำระ 2 วัน ';break;
                case 1: styles =' style="color: #dcbb09;" '; SMSkeyword ='บริการแจ้งยอด ก่อนครบกำหนดชำระ 1 วัน ';break;
                case 0: styles =' style="color: #dcbb09;" '; SMSkeyword ='บริการแจ้ง ครบกำหนดชำระ ';break;
                default:styles =''; SMSkeyword = '';break;
            }

            td += "<tr>";
            td += " <td><small>"+val.period+"</small></td>";
            td += " <td><small>"+val.installment_payment+"</small></td>";
            td += " <td><small>"+ formatDate(val.payment_duedate,'-')+"</small></td>";
            td += " <td><small>"+ formatDate(val.extend_duedate,'-')+"</small></td>";
            td += " <td  "+styles+"><small>"+val.overdue+"</small></td>";

            var payment_amount = (val.payment_amount == null) ? '' :val.payment_amount;
            td += " <td><small>"+ payment_amount+"</small></td>";

            var amount_deff = (val.amount_deff == null || val.amount_deff == 0 ) ? '' :val.amount_deff;
            td += " <td><small>"+ amount_deff+"</small></td>";
            td += " <td><small>"+ formatDate(val.installment_date,'-')+"</small></td>";

            var remark = (val.remark == null) ? '' :val.remark;
            td += " <td><small>"+remark+"</small></td>";
            td += " <td style='color: #fff; text-align: center;background-color:"+val.background_color+";padding-right: .2rem;padding-left: .2rem;'><small>"+val.label+"</small></td>";

            td += '<td class=" last"  style="text-align: center;padding-right: .2rem;padding-left: .2rem;"><small>';
            if(val.status_code == 0 && val.installment_payment > 0){
                var jsons = "{'contract_code':'"+val.contract_code+"', 'period':'"+val.period+"', 'payment':'"+val.installment_payment+"'}";
                td += '     <button id="period'+val.period+'" onclick="editModal('+jsons+')" type="button" class="btn btn-round btn-warning edit-button" style=" font-size: 13px; border-radius: 50%; padding: .1rem .35rem;margin-bottom: inherit;" data-toggle="tooltip" title="แก้ไข">';
                td += '         <i class="fa fa-wrench"></i>';
                td += '     </button>';
            }
        
            var sendsms = (val.sendsms == 1) ? 'disabled' :'';
            if(val.status_code == 0 && val.installment_payment > 0 && val.overdue != ''){
                var jsons = "{ 'customer_code':'"+val.customer_code+"', 'customer':'"+val.firstname+' '+val.lastname+"', 'contract_code':'"+val.contract_code+"', 'period':'"+val.period+"', 'payment':'"+val.installment_payment+"', 'keywords':'"+SMSkeyword+"', 'duedate':'"+formatDateDMY(val.payment_duedate,'/')+"', 'tel':'"+val.tel+"'}";
                td += '     <button '+sendsms+' id="period'+val.period+'" onclick="messageModal('+jsons+')" type="button" class="btn btn-round btn-info edit-button" style=" font-size: 13px; border-radius: 50%; padding: .1rem .35rem;margin-bottom: inherit;" data-toggle="tooltip" title="ส่ง sms">';
                td += '         <i class="fa fa-send"></i>';
                td += '     </button>';
            }
            td += '</small></td>';
            td += '</tr>';
        });
        $(elementtable).append(td);  
    }
    function getContractInsRes(contract_code, searchArray){
        var res = null;
        $.ajax({
            url: base_url+'getResAddonInstallments', //ทำงานกับไฟล์นี้
            data: {
                "search" : searchArray,
                "contract_code" : contract_code, 
            }, //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {
                res = data;
            },
            error: function(xhr, status, exception) { 
                //console.log(exception);
            }
        });
        return res;
    }

    $("#report-search").click(function () {
        var  payment_duedate = null;
        var  installment_date = null;
        var  contract_code = $("#reportTemp").val();

        if( $('#stduedate').val() != '' && $('#enduedate').val() !='' ){
            payment_duedate = formatPDate($('#stduedate').val(),'-')+','+ formatPDate($('#enduedate').val(),'-');
        }
        if($('#stinstallmentdate').val() != '' && $('#endinstallmentdate').val()!= ''){
            installment_date = formatPDate($('#stinstallmentdate').val(),'-')+','+ formatPDate($('#endinstallmentdate').val(),'-');
        }
        
        searchArray["customer_code"] = $("#reportCus").val();
        //searchArray["name"] = $("#reportName").val();
        searchArray["contract_code"] = contract_code;
        searchArray["status_code"] = $("#reportStatus").val();
        searchArray["payment_duedate"] = payment_duedate;
        searchArray["installment_date"] = installment_date;
        searchArray["overdue"] = $("#overdue").val();
        
        var res = getContractInsRes(contract_code, searchArray);
        getContractInsTeble(res, elementtable);
    });
    
    function editModal(json) {
        $('#edit-form').modal('show');
        $('#contract_code').val(json.contract_code);
        $('#inst-period').val(json.period);
        $('#inst-payment').val(json.payment);
        $("#inst-payment").attr("payment",json.payment);
        $('#payment').val(json.payment);


        /* set status */
        $("#inst-status").val(1);
        $("#inst-status option[value=0]").attr('disabled','disabled');
        $("#inst-status option[value=1]").attr('disabled',false);
        $("#inst-status option[value=2]").attr('disabled','disabled');
        $("#inst-status option[value=3]").attr('disabled','disabled');
    }
    function messageModal(json) {
        $('#message-form').modal('show');
        $('#customer-sms').val(json.customer);
        $('#customer-code').val(json.customer_code);
        $('#temp-code-sms').val(json.contract_code);
        $('#inst-period-sms').val(json.period);
        $('#inst-payment-sms').val(json.payment);
        $('#keywords-sms').val(json.keywords);
        $('#duedate-sms').val(json.duedate);
        $('#tel-sms').val(json.tel);
    }

    function formatPDate(date, string) {
        if(date != '' && date != null){
            var temp = date.split('/'); 
            var res = temp[2]+string+temp[1]+string+temp[0];
            return res;
        }else{
            return '';
        }
    }

    $("#inst-payment").keyup(function(){
        var dInput = this.value;
        var payment = parseInt($(this).attr("payment"));
        var values = parseInt(this.value);
        if(values == payment){
            $("#inst-status").val(1);
            $("#inst-status option[value=0]").attr('disabled','disabled');
            $("#inst-status option[value=1]").attr('disabled',false);
            $("#inst-status option[value=2]").attr('disabled','disabled');
            $("#inst-status option[value=3]").attr('disabled','disabled');
        }else if(values > payment){
            $("#inst-status").val(2);
            $("#inst-status option[value=0]").attr('disabled','disabled');
            $("#inst-status option[value=1]").attr('disabled','disabled');
            $("#inst-status option[value=2]").attr('disabled',false);
            $("#inst-status option[value=3]").attr('disabled','disabled');
        }else if(values < payment){
            $("#inst-status").val(3);
            $("#inst-status option[value=0]").attr('disabled','disabled');
            $("#inst-status option[value=1]").attr('disabled','disabled');
            $("#inst-status option[value=2]").attr('disabled','disabled');
            $("#inst-status option[value=3]").attr('disabled',false);
        }
    });

    $('#edit-form').submit(function() {
        var confirmPanel = confirm("คุณต้องการที่จะแก้ไขข้อมูล จริงหรือไม่");
        if (confirmPanel == true) {
            return true;
        }else{
            return false;
        }
        //return false;
    });

    $('#message-form').submit(function() {
        var confirmPanel = confirm("คุณต้องการที่จะส่งข้อความไปยังหมายเลขโทรศัพท์มือถือ "+$('#tel-sms').val()+' จริงหรือไม่');
        if (confirmPanel == true) {
            return true;
        }else{
            return false;
        }
        //return false;
    });
    
</script>
<!---- End Content ------>
