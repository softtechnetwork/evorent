<style>
.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
</style>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>แก้ไขข้อมูลลูกค้า(นิติบุคคล) <small></small></h2>
                <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>-->
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form class="" action="<?php echo site_url('/admin/corperation/insert');?>" method="post" enctype="multipart/form-data" novalidate>-->
                <form id="corperation-create-form" class="" action="<?php echo site_url('/admin/corperation/update');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขทะเบียนนิติบุคคล</label>
                        <div class="col-md-6 col-sm-6">
                          <!--<input name="idcard" id="idcard" type="text" class="form-control" data-inputmask="'mask' : '9-9999-99999-99-9'" required="required">-->
                          <input name="idcard" id="idcard" type="tel"  maxlength="13"  class="form-control" value="<?=$res_customer[0]->idcard; ?>" readonly>
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="idcard-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ชื่อสถานที่ประกอบการ</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" id="name" name="name"  value="<?=$res_customer[0]->firstname; ?>" required="required" />
                        </div>
                    </div>
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">นามสกุล</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" id="sname" name="sname" required="required" />
                        </div>
                    </div>
                    -->
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address' name='address'></textarea>
                        </div>
                    </div>
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จังหวัด</label>
                        <div class="col-md-6 col-sm-6">
                            <select name='province' id='province' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อำเภอ / เขต</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="amphurs"  id="amphurs" required="required" >
								<option value="">อำเภอ / เขต</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ตำบล / แขวง</label>
                        <div class="col-md-6 col-sm-6">
                            <select class="form-control" name="district"  id="district" required="required" >
								<option value="">ตำบล / แขวง</option>
							</select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสไปรษณีย์</label>
                        <div class="col-md-6 col-sm-6">
						    <input type="tel" maxlength="5" class="form-control" name="zipcode" id="zipcode" readonly>
                            </div>
                    </div>
                    -->

                    <div class="ln_solid"> </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ตามที่จดทะเบียน</label>
                        <!--<div class="col-md-6 col-sm-6">
                            <select name='inhabited' id='inhabited' class="form-control">
                               
                                </select>
                            </select>
                        </div>
                        -->
                        <div class="col-md-8 col-sm-8">
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <th style=" width: 18rem;">ที่อยู่</th>
                                        <th>ตำบล / แขวง</th>
                                        <th>อำเภอ / เขต</th>
                                        <th>จังหวัด</th>
                                        <th>รหัสไปรษณีย์</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inhabited as $item) : ?>
                                    <tr>
                                        <td style=" width: 18rem;"><?=$item->address;?></td>
                                        <td><?=$item->district_name;?></td>
                                        <td><?=$item->amphur_name;?></td>
                                        <td><?=$item->province_name;?></td>
                                        <td><?=$item->zip_code;?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class=" col-md-6 col-sm-6 checkbox">
                            <label><input class="inhabitedInput" type="checkbox" id='addOfId-check' value=""> ใช้ที่อยู่ตามบัตรประาชน</label>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ที่อยู่ปัจจุบัน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-current' name='address-current'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-current">จังหวัด</label>
                            <select name='province-current' id='province-current' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-current">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs-current"  id="amphurs-current" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-current">ตำบล / แขวง</label>
                            <select class="form-control" name="district-current"  id="district-current" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-current">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-current" id="zipcode-current" readonly>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>





                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ข้อมูลสถานที่ทำงาน</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='address-office' name='address-current'></textarea>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="province-current">จังหวัด</label>
                            <select name='province-current' id='province-current' class="form-control" required="required" >
                                <option value="" selected='false' disabled>จังหวัด</option>	
                                <?php foreach ($province as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->province_name; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="amphurs-current">อำเภอ / เขต</label>
                            <select class="form-control" name="amphurs-current"  id="amphurs-current" required="required" >
                                <option value="">อำเภอ / เขต</option>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"></label>
                        <div class="col-md-3 col-sm-3">
                            <label for="district-current">ตำบล / แขวง</label>
                            <select class="form-control" name="district-current"  id="district-current" required="required" >
                                <option value="">ตำบล / แขวง</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="zipcode-current">รหัสไปรษณีย์</label>
                            <input type="tel" maxlength="5" class="form-control" name="zipcode-current" id="zipcode-current" readonly>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>-->






                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รายได้ต่อเดือน</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="number" min="1" class="form-control" name="monthly-income" id="monthly-income" />
                        </div>
                    </div>-->
                    <!--<div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อาชีพ</label>
                        <div class="col-md-3 col-sm-3">
                            <select class="form-control" name="career-select"  id="career-select" required="required" >
                                <option value="" selected='false' disabled>กรุณาเลือกอาชีพของคุณ</option>
                                <?php foreach ($career as $id => $item) : ?>
                                    <option value="<?= $id;?>" datas="<?= $item;?>"><?= $item;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input  id="career" class="form-control" type="text" name="career" readonly />
                        </div>
                    </div>
                    <div class="ln_solid"> </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">วันเกิด</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" value="" class='date' type="date" name="bdate">
                        </div>
                    </div>-->
                    
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="phone" name="phone" type="tel" class="form-control" value="<?=$res_customer[0]->phone; ?>">
                            <!--<input name="tel" value=""  type="text" class="form-control" data-inputmask="'mask' : '999-999-9999'" required>-->
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="phone-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">หมายเลขโทรศัพท์มือถือ</label>
                        <div class="col-md-6 col-sm-6">
                            <input id="tel" name="tel" type="tel" maxlength="10" class="form-control" value="<?=$res_customer[0]->tel; ?>">
                            <!--<input name="tel" value=""  type="text" class="form-control" data-inputmask="'mask' : '999-999-9999'" required>-->
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <label class="col-form-label col-md-3 col-sm-3 input-validat" id="tel-validat"></label>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">อีเมล์<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control email" id="email" name="email" type="email" value="<?=$res_customer[0]->email; ?>" />
                        </div>
                    </div>
                    <!--
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">เพศ *</label>
                        <div class="col-md-6 col-sm-6">
                            <p style="padding-top: 7px;">
                                <label style=" margin-right: 3px;">ชาย:</label>
                                <input type="radio" class="flat" name="gender" id="genderM" value="M" checked /> 
                                <label style=" margin-left: 10px; margin-right: 3px;">หญิง:</label>
                                <input type="radio" class="flat" name="gender" id="genderF" value="F" />
                            </p>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    -->
                    <!--
                    <?php  $pathUrl = base_url('/uploaded/DocumentTh.png');?>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">เอกสาร/หลักฐาน</label>
                        <div class=" col-md-9 col-sm-9 file-loading">
                            <div class="row">

                                <?php 
                                        $PathidcardPremise =$pathUrl;
                                        $PathhouseregisPremise =$pathUrl;
                                        
                                        $idcardPremise_code = null;
                                        $houseregisPremise_code = null;
                                        
                                        if($customer_premise != null){
                                            $idcardPremise_code = $customer_premise[0]->premise_code;
                                            $houseregisPremise_code = $customer_premise[1]->premise_code;

                                            if($customer_premise[0]->img != '' && $customer_premise[0]->img != null){
                                                $PathidcardPremise = base_url('uploaded/customer/'.$customer_premise[0]->ref_code."/premise/".$customer_premise[0]->img);
                                            }

                                            if($customer_premise[1]->img != '' || $customer_premise[1]->img != null){
                                                $PathhouseregisPremise = base_url('uploaded/customer/'.$customer_premise[1]->ref_code."/premise/".$customer_premise[1]->img);
                                            }

                                        }
                                    ?>
                                    
                                    <div class="col-md-3 col-sm-3 premise-items">
                                        <img id="thumnails-idcardPremise" browsid="idcardPremise" class="thumnails-premise" src="<?=$PathidcardPremise;?>" alt="image" />
                                        <input id="idcardPremise" name="idcardPremise" type="file" style="display:none" >
                                        <input id="idcardPremise_code" name="idcardPremise_code" type="number" style="display:none" value="<?=$idcardPremise_code;?>" >
                                        <label class="" style=" padding-top: 5px;">บัตรประชาชน</label> <br>
                                    </div>
                                    <div class="col-md-3 col-sm-3 premise-items">
                                        <img id="thumnails-houseregisPremise" browsid="houseregisPremise" class="thumnails-premise" src="<?=$PathhouseregisPremise;?>" alt="image" />
                                        <input id="houseregisPremise" name="houseregisPremise" type="file" style="display:none" >
                                        <input id="houseregisPremise_code" name="houseregisPremise_code" type="number" style="display:none" value="<?=$houseregisPremise_code?>" >
                                        <label class="" style=" padding-top: 5px;">ทะเบียนบ้าน</label><br>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    -->
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">สถานะ</label>
                        <div class="col-md-6 col-sm-6">
                        <?php //print_r($resStatus); ?>
                            <select name='status' id='status' class="form-control">
                                <?php foreach ($resStatus as $item) : ?>
                                    <option value="<?php echo $item->id; ?>" <?php if($res_customer[0]->status == $item->id){echo 'selected="selected"';} ?>><?php echo $item->label; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </select>
                        </div>
                    </div>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align"> หมายเหตุ</label>
                        <div class="col-md-6 col-sm-6">
                            <textarea  class="form-control" required="required" id='remark' name='remark'><?=$res_customer[0]->remark;?></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                        <div class="form-group">
                            <div class="col-md-6 offset-md-3">
                            
                            <input type="hidden" name="customer_code" value="<?=$res_customer[0]->customer_code; ?>">
                            <input type="hidden" name="premise_code"  value="<?=$res_customer[0]->firstname; ?>" />

                                <a href="<?php echo base_url('admin/corperation');?>" type='button' class="btn btn-success">Back</a>
                                <button type='submit' class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
<script>
var base_url = $('input[name="base_url"]').val();

var AddById = ["#province","#amphurs","#district","#zipcode","#address"];// ห้าม เปลี่ยนตำแหน่ง
#var input_id = ["#name","#phone", "#tel", "#email"];

CorperationUpdateSubmit(input_id, '#corperation-create-form'); // validate and submit
// validate and submit

CorperationEditeEleChange(input_id, base_url);// normal Validate
// validate normal element


</script>