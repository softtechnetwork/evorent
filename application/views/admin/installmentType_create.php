<style>
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}
.btn-light {
    color: #212529bd;
    background-color: #f8f9fa00;
    border-color: #d5d9dc;
    border-radius: inherit !important;
}
.mh .dropdown-menu { max-height: 200px;}
.dropdown-item.active, .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #1abb9c;
}
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>เพิ่มประเภทการผ่อนชำระ</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-InstallmentType-create" action="<?php echo site_url('/admin/installmentType/insert');?>" method="post"  enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">รหัสสินค้า</label>
                        <div class="col-md-6 col-sm-6  ">
                            <select id="product-id" name="product-id" class="selectpicker form-control mh " data-live-search="true">
                                <option selected='false' disabled>เลือก รหัสสินค้า</option>
                                <?php  foreach($product as $item){?>
                                    <option value="<?php echo $item->product_id;?>"> <?php echo $item->product_id;?></option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">จำนวนงวดที่ผ่อน</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="number" step="1" min="1"  class="form-control" name="amount_installment" id="amount_installment" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">ราคาผ่อนต่องวด</label>
                        <div class="col-md-6 col-sm-6  ">
                            <input type="number" step="1" min="1"  class="form-control" name="pay_per_month" id="pay_per_month" />
                        </div>
                    </div>
                    <div class="ln_solid"> </div>
                    
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <a href="<?php echo base_url('/admin/installmentType');?>" class="btn btn-primary">กลับ</a>
                            <button type='submit' class="btn btn-success">เพิ่มประเภทการผ่อนชำระ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('input[name="base_url"]').val();
    var submitRequireEle = ["#product-id", "#amount_installment","#pay_per_month"];
    InstallmentTypeCrateEleChange(submitRequireEle); 
    InstallmentTypeCrateSubmit(submitRequireEle, "#form-InstallmentType-create");
</script>


