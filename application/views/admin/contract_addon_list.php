<style>
.fail-stat-sale{color: #d9530e;}
.succ-stat-sale{color: #26b99a!important;}

.premise-items{text-align: center; margin-bottom: 15px;}
.thumnails-premise{
    border: 2px #08080759 solid; border-radius: 7px;width: 100%; display: block;cursor:pointer
}
.thumnails-premise-valid{
    border: 1px solid #fd8a5c;
    box-shadow: 0px 0px 6px #ff4700a3;
}
.input-valid {
    border: 1px solid #fb48004d !important;
    box-shadow: 0px 0px 3px #ff470063 !important;
}

.input-validat{color:#fb4800c4 !important;}
.inhabitedInput, input:checked { height: 19px; width: 19px;}
#contract-ui li{ padding: 5px;}
</style>
<div class="clearfix"></div>

<!--  Contract panel --->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ข้อมูลสัญญา : <small><?=$res[0]->contract_code;?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row mb-5">
                    <div class="col-md-3 col-sm-3">
                        <label><strong>ลูกค้า :</strong></label>
                        <label><?=$res[0]->firstname." ".$res[0]->lastname;?></label>
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <label><strong>สินค้า :</strong></label>
                        <label><?=$res[0]->product_name;?></label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <label><strong>ประเภทสินค้าสินค้า(สำหรับลูกค้า) :</strong></label>
                        <label><?=$res[0]->customerType_name;?></label>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <label><strong>ประเภทสัญญา :</strong></label>
                        <label><?=$res[0]->contractType_name;?></label>
                    </div>
                </div>

                <div class="row">  
                    <?php if($res[0]->installment != ''){ ?>
                        <div class="col-md-4 col-sm-4">
                            <label><strong>รูปแบบการผ่อนชำระ :</strong></label>
                            <table class="table table-striped jambo_table bulk_action" id="installment-table">
                                <thead>
                                    <tr>
                                        <th><small>จำนวนงวด</small></th>
                                        <th><small>ผ่อนเดือนละ</small></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><small><?=$res[0]->rental_period;?></small></td>
                                        <td><small><?=$res[0]->monthly_rent;?></small></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <?php if(count($inhabited) > 0){ ?>
                        <div class="col-md-8 col-sm-8 ">
                            <label><strong>ที่อยู่ที่ติดตั้งสินค้า :</strong></label>
                            <table class="table table-striped jambo_table bulk_action" id="address-table">
                                <thead>
                                    <tr>
                                        <!--<th>เลือก</th>-->
                                        <th><small>ที่อยู่</small></th>
                                        <th><small>ตำบล / แขวง</small></th>
                                        <th><small>อำเภอ / เขต</small></th>
                                        <th><small>จังหวัด</small></th>
                                        <th><small>รหัสไปรษณีย์</small></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inhabited as $item) : ?>
                                        <tr class="">
                                            <td><small><?=$item->address?></small></td>
                                            <td><small><?=$item->district_name?></small></td>
                                            <td><small><?=$item->amphur_name?></small></td>
                                            <td><small><?=$item->province_name?></small></td>
                                            <td><small><?=$item->zip_code?></small></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Contract addon panel --->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>ข้อมูลสัญญาเพิ่มเติม</h2>
                <ul id="contract-ui" class="nav navbar-right panel_toolbox" style="min-width: 0;">
                    <li>
                        <button id="AddonContract-btn" type='button' class="btn btn-success" data-toggle="modal"   style=" border-radius: inherit;width: 100%;"><i class="fa fa-plus"></i> ส่วนเสริมสัญญา</button>
                     </li>
                    <li>
                        <a href="<?=base_url('admin/contractSerial/'.$res[0]->contract_code.'/'.$res[0]->product_id);?>" style="padding: 0;">
                            <button id="ContractSerial-btn" type='button' class="btn btn-warning" style=" border-radius: inherit;width: 100%;">Serial Number</button>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <input id="base_url" type="hidden" name="base_url" value="<?=base_url();?>">
                <input type="hidden" name="contract_code" value="<?=$res[0]->contract_code; ?>">
                <input type="hidden"  name="product_id" value="<?=$res[0]->product_id;?>">
                <div class="row mb-4">
                    <div class="col-md-12  col-sm-12"><div id="contract-addon-place"></div></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Addon Contract -->
<div class="clearfix"></div>
<div id="AddonContract-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <form id="AddonContract-form" action="<?=base_url('/contractaddon/addon');?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">ส่วนเสริมสัญญา รหัสสัญญา : <?=$res[0]->contract_code;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--
                    <div class="row">
                        <div class="col-md-12"><h6>สัญญาหลัก</h6></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><label class="col-form-label label-align"><strong>ลูกค้า : </strong><?=$res[0]->firstname;?> <?=$res[0]->lastname;?></div>
                        <div class="col-md-6"><label class="col-form-label label-align"><strong>สินค้า : </strong><?=$res[0]->product_name;?></div>
                        <div class="col-md-2"><label class="col-form-label label-align"><strong>ผ่อนชำระ : </strong><?=$res[0]->rental_period;?> งวด/เดือน</div>
                    </div>
                    <hr>
                    -->
                    <div id="contract-addon-place"></div>
                    <div class="row">
                        <div class="col-md-12"><h6>เพิ่ม ส่วนเสริมสัญญา</h6></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label class="col-form-label label-align"><strong>สินค้า เพิ่มเติม : </strong></label>
                            <select id='addon-contract-products' name='addon-contract-products' class='selectpicker form-control mh' data-live-search='true' data-product="<?=$res[0]->product_id;?>" ></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>จำนวนงวด/เดือน : </strong></label>
                            <input id='addonrental_period' name='addonrental_period' class='form-control' type="number" min="1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>ราคาผ่อนต่องวด/เดือน : </strong></label>
                            <input id='addonmonthly_rent' name='addonmonthly_rent' class='form-control' type="number">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>วันที่ชำระงวดแรก : </strong></label>
                            <input class="form-control datepicker" 
                                type="text" 
                                id="addon-payment-start-date" 
                                name="addon-payment-start-date"  
                                data-provide="datepicker" 
                                data-date-language="th-th" 
                                autocomplete="off" 
                                placeholder="วว/ดด/ปป"
                            >
                        </div>
                        <div class="col-md-3 col-sm-3" style="padding-top:1.8rem;">
                            
                            <input id='contract_code' name='contract_code' type="hidden" value="<?=$res[0]->contract_code;?>">
                            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>-->
                            <button type="submit" class="btn btn-primary" style="width:100%;">เพิ่ม ส่วนเสริมสัญญา</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Addon Product -->
<div class="clearfix"></div>
<div id="AddonProduct-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <form id="AddonProduct-form" action="<?=base_url('/contractaddon/addonproduct');?>" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">ส่วนเสริมสินค้า รหัสสัญญา : <?=$res[0]->contract_code;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="products-addon-place"></div>
                    <div class="row"><div class="col-md-12"><h6>เพิ่ม ส่วนเสริมสินค้า</h6></div></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <label class="col-form-label label-align"><strong>หมวดหมู่ สินค้า: </strong></label>
                            <select id='addon-pcate' name='addon-pcate' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <label class="col-form-label label-align"><strong>สินค้า: </strong></label>
                            <select id='addon-pmaster' name='addon-pmaster' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                    </div>

                    <div class="row justify-content-between">
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>จำนวน : </strong></label>
                            <input id='addon-pcount' name='addon-pcount' class='form-control'type="number" min="1" value="1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>ประเภท สินค้า: </strong></label>
                            <select id='addon-pstatus' name='addon-pstatus' class='selectpicker form-control mh' data-live-search='true'></select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="col-form-label label-align"><strong>Serial number : </strong></label>
                            <div class="checkbox" style="padding-top: 4%;">
                                <label class="">
                                    <div class="icheckbox_flat-green checked" style="position: relative;">
                                        <input name="active-serial" type="checkbox" class="flat" checked="checked" style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                    </div> มี Serial number
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3" style="padding-top:1.8rem;">
                            <input id='contract_code' name='contract_code' type="hidden" value="<?=$res[0]->contract_code;?>">
                            <input id='contract_addon' name='contract_addon' type="hidden" value="">
                            <button type="submit" class="btn btn-primary" style="width:100%;">เพิ่ม ส่วนเสริมสัญญา</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal View contract -->
<div class="row">
    <div class="modal fade" id="contractMode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">เลือกเอกสาร</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--<div class="modal-body"></div>-->
                <div class="modal-footer" style="justify-content: center !important;">
                    <a id="coppy-pdf" class="btn btn-info text-center" href="" target="_blank" style="">คู่ฉบับ</a>
                    <a id="origin-pdf" class="btn btn-primary text-center" href="" target="_blank" style="">ต้นฉบับ</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url('./assete/js/admin/contract_custom.js');?>"></script>
<script> 

//--- contract addon table ---//
var contractaddon = getResContractAddon(null, '<?=$res[0]->contract_code;?>');
var table = setTableAddon(contractaddon);
$('#contract-addon-place').html(null);
if(table[0] > 0){
    $('#contract-addon-place').append(table[1]);
}

AddAddonProduct();
DelAddonContract(); // del addon contract
</script>

