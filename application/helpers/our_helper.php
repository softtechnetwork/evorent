<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of b2c_helper
 *
 * @author DoOoO lnw
 */
if (!function_exists('script_tag')) {

    /**
     *
     * @param string $src
     * @param string $has_extheranl
     * @return string
     */
    function script_tag($src = '', $has_extheranl=false) {

        $CI = & get_instance();
        if (!$has_extheranl)
            return '<script type="text/javascript" language="JavaScript" src="' . $CI->config->slash_item('base_url') . $src . '"></script>';
        else
            return '<script type="text/javascript" language="JavaScript" src="' . $src . '"></script>';
    }

}
if (!function_exists('massage_success')) {

    /**
     *
     * @param <type> $obj ให้ใช้เป็น $this นะค่ะ
     * @param <type> $return ต้องการให้บรรทัดล่างแสดงหรือไม่
     */
    function massage_success($obj) {
        $text = '';
        $messages = $obj->msg->get();
        if (is_array($messages)):
            foreach ($messages as $type => $msgs):
                if (count($msgs > 0)):
                    foreach ($msgs as $message):
                        $text .= ( '<span class="' . $type . '">' . $message . '</span>');
                    endforeach;
                endif;
            endforeach;

            echo '        <div class="one_half">
            <div class="success-box"><strong>'.__('success','Front_home').'</strong> <br/> ' . $text . '</div>
        </div>';
            return true;
        endif;
        return false;
    }

}
if (!function_exists('message_warning')) {

    /**
     *
     * @param <type> $obj ให้ใช้เป็น $this นะค่ะ
     * @param <type> $return ต้องการให้บรรทัดล่างแสดงหรือไม่
     */
    function message_warning($obj) {
        $text = '';
        // print_r($obj);
        $messages = $obj->msg->get();
        //var_dump($messages);
       if (is_array($messages) && count($messages['success']) > 0){
            foreach($messages['success'] as $row){
                $text .= '<span class="success">' .$row. '</span>';
            }
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>'.__('Success','default').'</strong><br> '.$text."</div>";
       }else  if (is_array($messages) && count($messages['error']) > 0){
            foreach($messages['error'] as $row){
            
                $text .= '<span class="error">'.__(trim(strip_tags($row)),'default'). '</span>';
            }
            echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button><strong>เกิดข้อผิดพลาด!</strong><br> ".$text."</div>";
       }
        return FALSE;
        
        
    }

}

if (!function_exists('message_warning_front')) {

    /**
     *
     * @param <type> $obj ให้ใช้เป็น $this นะค่ะ
     * @param <type> $return ต้องการให้บรรทัดล่างแสดงหรือไม่
     */
    function message_warning_front($obj) {
        $text = '';
        $messages = $obj->msg->get();
        //var_dump($messages);
       if (is_array($messages) && count($messages['success']) > 0){
            foreach($messages['success'] as $row){
                $text .= '<p">' .$row. '</p>';
            }
            echo '<section class="success-box"><strong>'.__('Success','default').'</strong><br> '.$text."</section>";
       }else  if (is_array($messages) && count($messages['error']) > 0){
            foreach($messages['error'] as $row){
            
                $text .= '<p>' .__(trim(strip_tags($row)),'default'). '</p>';
            }
            echo "<section class='error-box'><strong>".__('Error','default')."</strong><br> ".$text."</section>";
       }
        return FALSE;
        
        
    }

}



if (!function_exists('massage_error')) {

    function massage_error($obj) {

        if (!empty($obj->error->all)) :

            echo ' <div class="one_half">
            <div class="error-box"><strong>Error</strong>
                ';

            foreach ($obj->error->all as $error) {
                echo $error;
            }


            echo ' </div>
            </div>';
        endif;
    }

}
if (!function_exists('log_admin')) {

    function log_admin($description) {
        $CI = & get_instance();
        $log_system = new M_log_system();
        $log_system->setLog_type('administrator');
        $log_system->setIp_address($CI->input->ip_address());
        $log_system->setUser_agent($CI->input->user_agent());
        $log_system->save();
        $log_admin = new M_log_administrator();
        $log_admin->setLog_system_id($log_system->getId());
        $log_admin->setAdministrator_id($CI->session->userdata('admin_id'));
        $log_admin->setDescription($description);
        $log_admin->save();
    }

}
if (!function_exists('log_owner_hotel')) {

    function log_owner_hotel($description) {
        $CI = & get_instance();
        $log_system = new M_log_system();
        $log_system->setLog_type('owner_hotel');
        $log_system->setIp_address($CI->input->ip_address());
        $log_system->setUser_agent($CI->input->user_agent());
        $log_system->save();

        $log_owner_hotel = new M_log_owner_hotel();
        $log_owner_hotel->setLog_system_id($log_system->getId());
        $log_owner_hotel->setOwner_hotel_id($CI->session->userdata('owner_hotel_id'));
        $log_owner_hotel->setDescription($description);
        $log_owner_hotel->save();
    }

}
if (!function_exists('log_customer')) {

    function log_customer($description) {
        $CI = & get_instance();
        $log_system = new M_log_system();
        $log_system->setLog_type('owner_hotel');
        $log_system->setIp_address($CI->input->ip_address());
        $log_system->setUser_agent($CI->input->user_agent());
        $log_system->save();

        $log_customer = new M_log_custormer();
        $log_customer->setLog_system_id($log_system->getId());
        $log_customer->setCustomer_id($CI->session->userdata('owner_hotel_id'));
        $log_customer->setDescription($description);
        $log_customer->save();
    }

}

if (!function_exists('use_lang')) {

    /**
     *
     * @return M_hotel_language 
     */
    function use_lang() {
        $CI = & get_instance();
        $shop_language = new M_gshop_shop_language();
        $shop_language->where('gshop_shop_id', $CI->shop_id)->where_related('language','active',1)->where('active', 1)->get();
        return $shop_language;
    }

}

if (!function_exists('all_lang')) {

    /**
     *
     * @return M_language
     */
    function all_lang() {
        $CI = & get_instance();
        $language = new M_language();
        $language->get();
        return $language;
    }

}
if (!function_exists('array_push_before')) {

    /**
     * @return array
     * @param array $src
     * @param array $in
     * @param int|string $pos
     */
    function array_push_before($src, $in, $pos) {
        if (is_int($pos))
            $R = array_merge(array_slice($src, 0, $pos), $in, array_slice($src, $pos));
        else {
            foreach ($src as $k => $v) {
                if ($k == $pos

                    )$R = array_merge($R, $in);
                $R[$k] = $v;
            }
        }return $R;
    }

}

if (!function_exists('array_push_after')) {

    /**
     * @return array
     * @param array $src
     * @param array $in
     * @param int|string $pos
     */
    function array_push_after($src, $in, $pos) {
        if (is_int($pos))
            $R = array_merge(array_slice($src, 0, $pos + 1), $in, array_slice($src, $pos + 1));
        else {
            foreach ($src as $k => $v) {
                $R[$k] = $v;
                if ($k == $pos

                    )$R = array_merge($R, $in);
            }
        }return $R;
    }

}


if (!function_exists('Datediff')) {

    /**
     *
     * @param <type> $datefrom
     * @param <type> $dateto
     * @return <type>
     */
    Function Datediff($datefrom, $dateto) {
        $startDate = strtotime($datefrom);
        $lastDate = strtotime($dateto);

        $differnce = $lastDate - $startDate;

        $differnce = ($differnce / (60 * 60 * 24)); //กรณืที่ต้องการให้ return ค่าเป็นวันนะครับ

        return $differnce;
    }

}
if (!function_exists('utf8_substr')) {

    function utf8_substr($str, $start_p, $len_p) {
        preg_match_all("/./u", $str, $ar);

        if (func_num_args() == 3) {
            $end = func_get_arg(2);
            return join("", array_slice($ar[0], $start_p, $len_p));
        } else {
            return join("", array_slice($ar[0], $start_p));
        }
    }
}

if(!function_exists('href_picture')){
    function href_picture($path_file){
        //echo $path_file;
        if(file_exists($path_file)){
            echo $path_file;
        }else{
            echo base_url().'themes/admin/assets/img/noimage.png';
        }
    }
}

if(!function_exists('datetime_show')){
    function datetime_show($datetime){
        $date_val = new DateTime($datetime);
        echo $date_val->format('d').' '.__($date_val->format('M'),'default').' '.$date_val->format('Y').' '.$date_val->format('H').':'.$date_val->format('i').':'.$date_val->format('s');
    }
}

if(!function_exists('clearDirectory')){
    function clearDirectory($dir_path){
        foreach(glob($dir_path.'*.*') as $v){
            unlink($v);
        }
    }
}


if(!function_exists('add_meta_og')){
    function add_meta_og($property,$content){
    $CI = & get_instance();
    $meta_tag[] = $CI->meta_og;
    
    $foundTag = false;
    foreach ($meta_tag as $meta_tag) {
        if (@$meta_tag['property'] == $property && @$meta_tag['content'] == $content){
            $foundTag = true;
            break;
        }else{
            continue;
        }            
    }
    if (!$foundTag) {
        $meta_tag[] = array('property' => $property, 'content' => $content);
        
        $CI->meta_og = $meta_tag;

        return TRUE;
    }
   
    
    //return FALSE;
    }
}
if(!function_exists('add_meta_tag')){
    function add_meta_tag($name,$content){
    $CI = & get_instance();

    $meta_tag[] = $CI->meta_tag;
    
    $foundTag = false;
    foreach ($meta_tag as $meta_tag) {
        if (@$meta_tag['name'] == $name && @$meta_tag['content'] == $content){
            $foundTag = true;
            break;
        }else{
            continue;
        }            
    }
   
    if (!$foundTag) {
        $meta_tag[] = array('name' => $name, 'content' => $content);
        
        $CI->meta_tag = $meta_tag;

        return TRUE;
    }
    
    return FALSE;
    }
}
if(!function_exists('update_meta_og')){
    function update_meta_og($property,$content){
        $CI = & get_instance();
        $meta_tag = $CI->meta_og;
        //var_dump($meta_tag);
        foreach($meta_tag as $key => $val){
                if($val['property'] == $property){
                    $meta_tag[$key]['content'] = $content;
            }
        }
        $CI->meta_og = $meta_tag;
       ///var_dump($CI->meta_og)."<br>";
        
    }
}
if(!function_exists('is_dir_empty')){
function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL; 
  return (count(scandir($dir)) == 2);
}
}

if(!function_exists('getHomepageSliderHref')){
    function getHomepageSliderHref($row){
        $CI = & get_instance();
        switch ($row->getArticle_type_id()) {
            case '5':
                echo base_url($row->article_type->get()->getType_name_th().'/'.$row->getRewrite_url());
            break;
            case '6':
                echo base_url($row->article_type->get()->getType_name_th().'/'.$row->getRewrite_url());
            break;

            case '7':
                echo base_url($row->article_type->get()->getType_name_th().'/'.$row->getRewrite_url());
            break;
            
            default:
                echo base_url(__('article','template').'/'.$row->article_type->get()->getType_name_th().'/'.$row->getRewrite_url());
            break;
        }

    }
}

if(!function_exists('showDateFormat')){
    function showDateFormat($datetime){
        echo date('d',strtotime($datetime)).' '.__(date('M',strtotime($datetime)),'default').' '.date('Y',strtotime($datetime));
    }
}


if(!function_exists('timeAgo')){
    function timeAgo($time_ago){
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
}


if(!function_exists('getCoverImage')){
    function getCoverImage($account_data){

        if(file_exists('./uploaded/member/'.$account_data['id'].'/'.$account_data['cover'])){
            return base_url('uploaded/member/'.$account_data['id'].'/'.$account_data['cover']);
        }else{
            return base_url('uploaded/member/default.png');
        }


    }


}

if(!function_exists('getYoutubeCategoryCoverImage')){
    function getYoutubeCategoryCoverImage($category_data){

        if(file_exists('./uploaded/youtube_category/'.$category_data->id.'/'.$category_data->cover_image) && $category_data->cover_image){
            return base_url('./uploaded/youtube_category/'.$category_data->id.'/'.$category_data->cover_image);
        }else{
            return base_url('uploaded/youtube_category/no_image.png');
        }


    }


}


if(!function_exists('checkAvailableMethod')){
    function checkAvailableMethod($arPermission,$arControllerMethod){
        // print_r($arControllerMethod);exit;
        $isAvailable = false;
        foreach ($arPermission as $key => $value) {
            # code...
            if(strtolower($value['controller_name']) == strtolower($arControllerMethod['controller_name']) && strtolower($value['method_name']) == strtolower($arControllerMethod['method_name'])){
                //print_r($value);
                $isAvailable = $value['event_ability'];
            }
        }
        return $isAvailable;

    }

}

if(!function_exists('getTVChannelLogo')){
    function getTVChannelLogo($channel_logo){
        switch (ENVIRONMENT) {
            case 'development':
               // return 'http://psi.development/uploaded/tv/logo/'.$channel_logo.'?'.time();
                return 'http://s3remoteservice.development/tv/uploaded/tv/logo/'.$channel_logo;
            break;
            case 'testing': 
                 return 'http://s3remoteservice.psi.co.th/tv/uploaded/tv/logo/'.$channel_logo;
            break;
            case 'production':
               // return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$channel_logo.'?'.time();
                return 'http://s3remoteservice.psi.co.th/tv/uploaded/tv/logo/'.$channel_logo;
            break;
            
            default:
                # code...
                break;
        }

    }
}


if(!function_exists('getproductpicture')){
    function getproductpicture($masterproduct){
        
        if(file_exists('./uploaded/masterproduct/'.$masterproduct->id.'/'.$masterproduct->pic) && $masterproduct->pic){
            return base_url('./uploaded/masterproduct/'.$masterproduct->id.'/'.$masterproduct->pic);
        }else{
            return base_url('uploaded/no_image.png');
        }



    }


}

if(!function_exists('getproductinstallpicture')){
    function getproductinstallpicture($product_install){
        
        if(file_exists('./uploaded/product_install/'.$product_install->id.'/'.$product_install->pic) && $product_install->pic){
            return base_url('./uploaded/product_install/'.$product_install->id.'/'.$product_install->pic);
        }else{
            return base_url('uploaded/no_image.png');
        }



    }


}

if(!function_exists('getproductpartpicture')){
    function getproductpartpicture($product_part){
       // echo '5';exit();
       //echo $product_part.' << ';exit();
      //print_r($product_part);
        if(file_exists('./uploaded/product_part/'.$product_part->part_id.'/'.$product_part->part_img)){
            return base_url('./uploaded/product_part/'.$product_part->part_id.'/'.$product_part->part_img);
        }else{
        
            return base_url('uploaded/no_image.png');
        }



    }


}



?>
