
$( document ).ready(function() {
	var image_arr = [];
	var base_url = $('input[name="base_url"]').val();
	
	Dropzone.autoDiscover = false;
	var dropzone_element = new Dropzone("#dropzone_uploadimg", {                   
		
		url: base_url + "/backend/product/ajaxuploadimgOnActionEdit/" + product_id,
		acceptedFiles: "image/*",
		addRemoveLinks: true,
		parallelUploads: 1,
	  
		removedfile: function(file) {
            x = confirm('ต้องการลบไฟล์นี้ใช่หรือไม่?');
            if(x){
				 // remove input hidden image
				 if($.inArray(file.name ,image_arr) != -1){
					
					image_arr = $.grep(image_arr, function(value) {
						return value != file.name;
					});
				}	
				console.log(image_arr);
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;     
            }	
		   
	   },
		init: function() {
			var submitButton = document.querySelector("#subinsert")
			dropzone_element = this;
			Dropzone.options.dropzone_element = false;
			dropzone_element.on("addedfile", function(file) {
		
				if (!file.type.match(/image.*/)) {
					if(file.type.match(/application.zip/)){
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img_edit/" + product_id);
					} else {
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img_edit/" + product_id);
					}
				}
			});
		this.on("sending", function(file, xhr, formData){
            
		});
		
			   
		this.on("success", function(file, responseText) {
				dropzone_element.on("complete", function(file) {
					const res = JSON.parse(responseText);
					if($.inArray(res.filename +'.'+ res.file_ext ,image_arr) == -1){
						image_arr.push(res.filename +'.'+ res.file_ext);
					}
				});
		});
		
	
		}
	});


	

	  $( "#edit_master_product" ).submit(function(){
			
			if(image_arr.length > 0){
				$.each(image_arr, function( index, value ) {
					
					$('#hidden_div').append('<input  name="file_dz['+index+']" value="'+value+'" />');
				});
				
			}
	   });

    $.ajax({
        type: "POST",
        url:  base_url + "/backend/product/ajaxretrievefilefromserver",
        data: {product_id: product_id , typeproduct_id: 2},
        success: function(data,status,xhr){
         
          var data = JSON.parse(data);
     
            // remove input hidden image
                 $.each(data, function(key,pic_value) {
                   
                    $.each(pic_value, function(pickey,value) {
                        //console.log(value);
                        var mockFile = { name: value };
                        dropzone_element.options.addedfile.call(dropzone_element, mockFile);
                        dropzone_element.options.thumbnail.call(dropzone_element, mockFile,  base_url + "/uploaded/masterproduct/" +product_id+"/"+value);

                        if($.inArray(value ,image_arr) == -1){
                            image_arr.push(value);
                        }
						//console.log(image_arr);
                        // dropzone_element.options.thumbnail.call(dropzone_element, mockFile, "uploaded/masterproduct/" +product_id+"/"+value.name);
                    });
                });		
        }
    });


});

