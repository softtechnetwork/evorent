
$( document ).ready(function() {
	var image_arr = [];
	var base_url = $('input[name="base_url"]').val();

	Dropzone.autoDiscover = false;
	var dropzone_element = new Dropzone("#dropzone_uploadimg", {                   
		
		url: base_url + "/backend/product/ajaxuploadtmpimg",
		acceptedFiles: "image/*",
		addRemoveLinks: true,
		parallelUploads: 1,
	  
		removedfile: function(file) {
			$.ajax({
				type: "POST",
				url:  base_url + "/backend/product/delete_img",
				data: {filename: file.name, action:"delete_img"},
				success: function(data,status,xhr){
					// remove input hidden image
					if($.inArray(file.name ,image_arr) != -1){
						image_arr = $.grep(image_arr, function(value) {
							return value != file.name;
						});
					}		
				}
			});		
		var _ref;
		return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
	   },
		init: function() {
			var submitButton = document.querySelector("#subinsert")
			dropzone_element = this;
			Dropzone.options.dropzone_element = false;
			dropzone_element.on("addedfile", function(file) {
		
				if (!file.type.match(/image.*/)) {
					if(file.type.match(/application.zip/)){
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img/");
					} else {
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img/");
					}
				}
			});
		this.on("sending", function(file, xhr, formData){
            
		});
		
			   
		this.on("success", function(file, responseText) {
				dropzone_element.on("complete", function(file) {
					const res = JSON.parse(responseText);
					if($.inArray(res.filename +'.'+ res.file_ext ,image_arr) == -1){
						image_arr.push(res.filename +'.'+ res.file_ext);
					}
				});
		});
		
	
		}
	});


	

	  $( "#create_master_product" ).submit(function(){
			
			if(image_arr.length > 0){
				$.each(image_arr, function( index, value ) {
					
					$('#hidden_div').append('<input  name="file_dz['+index+']" value="'+value+'" />');
				});
				
			}
	   });

});


// var dropzone_element = new Dropzone("#Update_myForm", {                   
// 	// autoProcessQueue: false,
//     // url: './ajax/on_form/upload_image_upd.php',
//     url: base_url+"backend/product/ajaxuploadtmpimg",
//     //acceptedFiles: "image/*",
// 	acceptedFiles: ".png,.jpg,.jpeg",
//     addRemoveLinks: true,
//     parallelUploads: 1,
// 	// removedfile: function(file) {
// 	// var product_id = $('#product_id').val();
// 	// var name = file.name;
// 	// var url = './ajax/on_form/delete_ajax.php';
// 	// 	$.ajax({
// 	// 		type: "POST",
// 	// 		url: url,
// 	// 		data: {product_id:product_id,name:name,action:"delete_img"},
// 	// 		success: function(data,status,xhr){
// 	// 				$('#response_1').css('display','none');
// 	// 				var product_id = $('#product_id').val();
// 	// 				var url = './ajax/on_form/draw_table.php';
// 	// 				$.ajax({
// 	// 					type: "POST",
// 	// 					url: url,
// 	// 					data: {product_id:product_id,action:"drawtbl_img"},
// 	// 					success: function(data,status,xhr){
// 	// 						$('#response_2').css('display','show');
// 	// 						$('#response_2').html(data);
// 	// 						imgtable();
// 	// 					}
// 	// 				});		
// 	// 		}
// 	// 	});		
// 	// var _ref;
// 	// return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
//  //    },
//     init: function() {
//         var submitButton = document.querySelector("#subinsert")
//         dropzone_element = this;
	
// 		Dropzone.options.dropzone_element = false;
		
//         dropzone_element.on("addedfile", function(file) {
//            var html = "<input type='hidden' id='getfilename' class='inputfile' value='"+ file.name +"'>";
//                 	$('.hiddenimg').append(html);
//                 	console.log(html);
//             if (!file.type.match(/image.*/)) {
//                 if(file.type.match(/application.zip/)){
                	
//                     dropzone_element.emit("thumbnail", file, "./images/personimage/tmppic/");
//                 } else {
                	
//                     dropzone_element.emit("thumbnail", file, "./images/personimage/tmppic/");
//                 }
//             }
//         });
//        this.on("sending", function(file, xhr, formData){
//             formData.append("product_id", $('#product_id').val());

// 		this.on("success", function(file, responseText) {
// 				dropzone_element.on("complete", function(file) {
// 					dropzone_element.processQueue();
// 					$('#response_1').css('display','none');
// 					var product_id = $('#product_id').val();
// 					var url = './ajax/on_form/draw_table.php';
// 					$.ajax({
// 						type: "POST",
// 						url: url,
// 						data: {product_id:product_id,action:"drawtbl_img"},
// 						success: function(data,status,xhr){
						
// 							$('#response_2').css('display','show');
// 							$('#response_2').html(data);
// 							imgtable();
// 						}
// 					});		
// 					//myDropzone.options.autoProcessQueue = true; 	
// 				});
// 		});
				
// 		this.on("error", function(file, response){			
// 					  if(response == 'Error405 IMAGE ALREADY EXIST !')
// 					 {
// 					 	$("#modal_img_exist").modal('show');
// 						 //dropzone_element.removeFile(file);				
						
// 					 }
				
// 			});      
//       	});
//     }
// });