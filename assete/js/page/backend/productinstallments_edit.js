

var remove_group = [];
$( document ).ready(function() {
    // jquery input on change  
    var formula_id = product_install_firstElement.formula_id;
    $('#formula_' + formula_id).attr('checked', true).trigger('click');
    $('#formula_' + formula_id).trigger("change");

    // draw filed set group 
    var groupCount = (product_install_rel_hirepurchaseinterest.length > 0 ) ? product_install_rel_hirepurchaseinterest.length : 0;
    for(var i = 1;i < groupCount; i ++){
        createInterestGroup();
    }
    $.each(product_install_rel_hirepurchaseinterest, function( index, value ) {
        // console.log(value);
        var group = index + 1;
        // input ( ผ่อนทั้งหมดกี่งวด )
        $('#end_payment_in_instalment_group'+group).val(value.term_of_payment_end);

        var per_month_price_a = JSON.parse(value['installment_amount_per_installment']);
        var per_month_price_a_ = 0;
        $.each( per_month_price_a, function( key, value ) {
            console.log(value.per_month_price_a);
            if(value.per_month_price_a > 0){ per_month_price_a_ = value.per_month_price_a;}
        });
        $('#instalment_'+group).val(per_month_price_a_);

        // input ( append product hirepurchase code)
        $('<input>').attr({
            type: 'hidden',
            id:'producthirepurchasecodegroup_' + group,
            name: 'producthirepurchasecodegroup_' + group,
            value: product_install_firstElement.product_hirepurchase_code
        }).appendTo('#create_productinstallments_product');

        $('<input>').attr({
            type: 'hidden',
            id:'productrelint_' + group,
            name: 'productrelint_' + group,
            value: value.id
        }).appendTo('#create_productinstallments_product');

        // remove group array
        remove_group[group] = value.id;
        
        // input ( พักชำระหนี้ )
       
        var holdonpayment_parseJson = (value.holdonpayment != null && value.holdonpayment != '') ? JSON.parse(value.holdonpayment) : null;
        if(holdonpayment_parseJson != null ){
           // console.log(holdonpayment_parseJson);
            if(holdonpayment_parseJson.length > 0){
               
                $('#ishelppaygroup_' + group).trigger('click');
                var countHoldonPayment = 0;
                $.each(holdonpayment_parseJson, function( idx, val ) {
                    // case : ถ้าพักชำระหนี้
                    ++ countHoldonPayment;
                    if(countHoldonPayment > 1){
                        addholdonpayment( group );
                    }
                    $('#holdonpayment_'+group+'_'+idx).val(val).trigger('keyup');
                });
           
              
            }
            
        }
        // input ( ดอกเบี้ย )
        $('#interest_group' + group).val(value.interest);
    });

     $( "#create_productinstallments_product" ).submit(function( event ) {   
            $('<input>').attr({
                type: 'hidden',
                id:'removeGroupArray',
                name: 'removeGroupArray',
                value: JSON.stringify( remove_group )
            }).appendTo('#create_productinstallments_product');

            $('#holdonpayment').val( JSON.stringify( holdonpayment ) );
     });

       
});
