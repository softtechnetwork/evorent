
$( document ).ready(function() {
	var image_arr = [];
	var base_url = $('input[name="base_url"]').val();

	Dropzone.autoDiscover = false;
	var dropzone_element = new Dropzone("#dropzone_uploadimg", {                   
		
		url: base_url + "/backend/product/ajaxuploadtmpimg",
		acceptedFiles: "image/*",
		addRemoveLinks: true,
		parallelUploads: 1,
	  
		removedfile: function(file) {
			$.ajax({
				type: "POST",
				url:  base_url + "/backend/product/delete_img",
				data: {filename: file.name, action:"delete_img"},
				success: function(data,status,xhr){
					// remove input hidden image
					if($.inArray(file.name ,image_arr) != -1){
						image_arr = $.grep(image_arr, function(value) {
							return value != file.name;
						});
					}		
				}
			});		
		var _ref;
		return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
	   },
		init: function() {
			var submitButton = document.querySelector("#subinsert")
			dropzone_element = this;
			Dropzone.options.dropzone_element = false;
			dropzone_element.on("addedfile", function(file) {
		
				if (!file.type.match(/image.*/)) {
					if(file.type.match(/application.zip/)){
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img/");
					} else {
						
						dropzone_element.emit("thumbnail", file,  base_url + "/uploaded/tmp_img/");
					}
				}
			});
		this.on("sending", function(file, xhr, formData){
            
		});
		
			   
		this.on("success", function(file, responseText) {
				dropzone_element.on("complete", function(file) {
					const res = JSON.parse(responseText);
					if($.inArray(res.filename +'.'+ res.file_ext ,image_arr) == -1){
						image_arr.push(res.filename +'.'+ res.file_ext);
					}
				});
		});
		
	
		}
	});


	

	  $( "#create_product_part" ).submit(function(){
			
			if(image_arr.length > 0){
				$.each(image_arr, function( index, value ) {
					
					$('#hidden_div').append('<input  name="file_dz['+index+']" value="'+value+'" />');
				});
				
			}
	   });

});

