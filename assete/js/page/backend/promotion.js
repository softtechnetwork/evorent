// $('#reservation-time').datetimepicker();
function cb_downpayment_onchange(element , group){
    var cb_element = (element.is(':checked'))?true:false;
    $('#is_downcondition_group1').val(cb_element);
    if(cb_element == true){
        $('#down_payment_' + group ).show();
    }else{
        $('#down_payment_' + group ).hide();
    }
 
}


function createInterestGroup(){
    var group_length  = $('.interest_group').length;
    var nextgroup_id = group_length + 1;
    var html = "";
    html += createbetween_instalment(nextgroup_id, null , null);
    html += createinterest_instalment(nextgroup_id, null);
    $('#fieldset_interest').append(html);
}

function editInterestGroup(nextgroup_id ,start_value , end_value , interest){
   
    var html = "";
    html += createbetween_instalment(nextgroup_id , start_value , end_value);
    html += createinterest_instalment(nextgroup_id , interest);
    $('#fieldset_interest').append(html);
}


function createbetween_instalment(nextgroup_id , start_value , end_value){
    var html ="";
    html += '<div class="field item form-group mt-2" id="d_payment_in_instalment_group'+nextgroup_id+'" style="">';
    html += '<label class="col-form-label col-md-3 col-sm-3  label-align">เริ่มผ่อนงวดที่เท่าไหร่ - ถึงงวดเท่าไหร่</label>';
    html += '   <div class="col-md-2 col-sm-2">';
    html += '       <input class="form-control" type="number" class="number" id="start_payment_in_instalment_group'+nextgroup_id+'"  value="'+start_value+'" name="startpaymentininstalmentgroup_'+nextgroup_id+'" data-validate-minmax="1,999999"  placeholder="1" required="" />';
    html += '   </div>';
    html += '   <div class="col-md-2 col-sm-2">';
    html += '       <input class="form-control" type="number" class="number" id="end_payment_in_instalment_group'+nextgroup_id+'"  value="'+end_value+'" name="endpaymentininstalmentgroup_'+nextgroup_id+'" data-validate-minmax="1,999999"  placeholder="12" required="" />';
    html += '   </div>';
    html += '   <div class="col-md-2 col-sm-2">';
    html += '       <span class="btn btn-success" style="cursor:pointer;" onclick="removeInterestGroup('+nextgroup_id+');"><i class="fa fa-minus" aria-hidden="true"></i></span>';
    html += '   </div>';
    html += '</div>';

    return html;
}

function createinterest_instalment(nextgroup_id , value){
    var html ="";
    html += '<div class="field item form-group" id="d_interest_group'+nextgroup_id+'" style="">';
    html += '   <label class="col-form-label col-md-3 col-sm-3  label-align">ดอกเบี้ย (%)</label>';
    html +='    <div class="col-md-2 col-sm-2">';
    html +='        <input class="form-control" type="number" class="number" id="interest_group'+nextgroup_id+'" value="'+value+'" name="interestgroup_'+nextgroup_id+'" step=".01" placeholder="0.25" required="" />';
    html +='    </div>';
    html +='</div>';

    return html;
}
function removeInterestGroup(nextgroup_id){
    // remove instalment group
    $('#d_payment_in_instalment_group' + nextgroup_id).remove();
    // remove interest group
    $('#d_interest_group' + nextgroup_id).remove();

}

