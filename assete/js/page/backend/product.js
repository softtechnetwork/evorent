

function ajaxGetContentData( base_url){
    var dataTable_ = dtDraw();
    // dt row count
    var rowCount = dataTable_.rows().count();
    // datatable add new row
    dtAddRow(dataTable_ , rowCount , null , "insert");
}



function ajaxGetContentData_onEdit(base_url , objprod_install_set){
    var dataTable_ = dtDraw();
    // dt row count
 
    // datatable add new row
    if(objprod_install_set.length > 0){
        $.each(objprod_install_set, function( index, value ) {
            var rowCount = dataTable_.rows().count();
            dtAddRow(dataTable_ , rowCount , value , "edit");
        });
        
    }
}
function addproductpart_newrow(){

  
    ajaxGetContentData(null);
}
function dtAddRow(dataTable_ , rowCount , objprod_install_set , type){
    
    var product_part_id = (objprod_install_set != null) ?  objprod_install_set.id : null;
    //console.log(objprod_install_set);
    var p_a = (objprod_install_set != null) ? objprod_install_set.price_a : 0 ;
    var p_b = (objprod_install_set != null) ? objprod_install_set.price_b : 0 ;
    var p_c = (objprod_install_set != null) ? objprod_install_set.price_c : 0 ;
    var p_d = (objprod_install_set != null) ? objprod_install_set.price_d : 0 ;
    p_a = (p_a > 0) ? p_a : 0;
    p_b = (p_b > 0) ? p_b : 0;
    p_c = (p_c > 0) ? p_c : 0;
    p_d = (p_d > 0) ? p_d : 0;

    var picodeHtml = picodebuilddropdown(rowCount , product_part_id);
    var priceA_html = buildPrice(rowCount , product_part_id , "A" , p_a);
    var priceB_html = buildPrice(rowCount , product_part_id , "B" , p_b);
    var priceC_html = buildPrice(rowCount , product_part_id , "C" , p_c);
    var priceD_html = buildPrice(rowCount , product_part_id , "D" , p_d);
    // var buildTextAreaHtml = buildTextArea(rowCount);

    var removerowHtml = buildRemoveRow(rowCount);
    dataTable_.row.add( [
        picodeHtml,
        priceA_html,
        priceB_html,
        priceC_html,
        priceD_html,
        removerowHtml
    ] ).node().id = 'td_' + rowCount;
    dataTable_.draw( false );

    //conf: selectpicker 
    confSelectPickerClass(rowCount );

    // ถ้าหน้าแก้ไขสินค้า ให้วาดยอดรวมเลย
    if( type == "edit"){
       confSelectDefaultValueEdit(rowCount ,  product_part_id);
    }
}

function confSelectDefaultValueEdit(group_id , product_part_id){
    $('#piddgroup_' + group_id)
    .val(product_part_id)
    .trigger('change');
}

function confSelectPickerClass(group_id){
    $('.selectpicker').selectpicker();
}
function confActiveAndExpireDate(group_id){

   

    $('#activedategroup_' + group_id).datetimepicker({
        format: 'YYYY-MM-DD'
    });
    
    $('#expiredategroup_' + group_id).datetimepicker({
        format: 'YYYY-MM-DD'
    });
}
function picodebuilddropdown(group_id , masterproduct_id){
    var html = "";
    
   console.log(masterproduct_id);
    html += '<select name="piddgroup_'+group_id+'" id="piddgroup_'+group_id+'" onchange="changePrice('+group_id+' ,$(this), this.value)" class="selectpicker" data-live-search="true" title="Please select">';
    $.each(  product , function( index, value ) {
        var selected =  "";
        if(value.id == masterproduct_id){
            selected = "selected";
        }
        html += "<option value='"+value.id+"' "+selected+" data-price_a='"+value.price_a+"' data-price_b='"+value.price_b+"' data-price_c='"+value.price_c+"' data-price_d='"+value.price_d+"'>"+value.mpcode+":"+value.mpname+"</option>";
    });
    html += '</select>';
   

    return html;
   
}
function changePrice(group_id , event ,  value){
  
    var price_a =  $('option:selected', event).attr('data-price_a');
    var price_b =  $('option:selected', event).attr('data-price_b');
    var price_c =  $('option:selected', event).attr('data-price_c');
    var price_d =  $('option:selected', event).attr('data-price_d');
    // recheck 
    price_a =(price_a > 0) ? price_a : 0;
    price_b =(price_b > 0) ? price_b : 0;
    price_c =(price_c > 0) ? price_c : 0;
    price_d =(price_d > 0) ? price_d : 0;
   // set value on span
    $('#price_' + "A_" +  group_id).text(price_a);
    $('#price_' + "A_" +  group_id).attr("data-price" , price_a);

    $('#price_' + "B_" +  group_id).text(price_b);
    $('#price_' + "B_" +  group_id).attr("data-price" , price_b);

    $('#price_' + "C_" +  group_id).text(price_c);
    $('#price_' + "C_" +  group_id).attr("data-price" , price_c);

    $('#price_' + "D_" +  group_id).text(price_d);
    $('#price_' + "D_" +  group_id).attr("data-price" , price_d);

    // วนวาดราคา
    drawSum( group_id );
   

}
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  function drawSum(group_id){
    //วน sum ราคา และวาดยอดรวม
    var sum_priceA = 0;
    var sum_priceB = 0;
    var sum_priceC = 0;
    var sum_priceD = 0;
    var group_id = $('.selectpicker ').length;
        for(var i =0; i <= group_id; ++i){
          
            var price_A = $('#price_A_' + i).length > 0 ? parseFloat($('#price_A_' + i).attr("data-price")) : 0;
            var price_B = $('#price_B_' + i).length > 0 ? parseFloat($('#price_B_' + i).attr("data-price")) : 0;
            var price_C = $('#price_C_' + i).length > 0 ? parseFloat($('#price_C_' + i).attr("data-price")) : 0;
            var price_D = $('#price_D_' + i).length > 0 ? parseFloat($('#price_D_' + i).attr("data-price")) : 0;
        
            sum_priceA += price_A;
            sum_priceB += price_B;
            sum_priceC += price_C;
            sum_priceD += price_D;
        }
        //วาดยอดรวม
        // $('#price_a').val(sum_priceA);
        // $('#price_b').val(sum_priceB);
        // $('#price_c').val(sum_priceC);
        // $('#price_d').val(sum_priceD);

        
        drawSumtext(sum_priceA , sum_priceB , sum_priceC ,sum_priceD);
  }
function drawSumtext(sum_priceA , sum_priceB , sum_priceC ,sum_priceD){
    //วาดยอดรวม
    $('#label_sum_price_a').html('');
    $('#label_sum_price_a').text('(total:'+formatNumber(sum_priceA)+')');

    $('#label_sum_price_b').html('');
    $('#label_sum_price_b').text('(total:'+formatNumber(sum_priceB)+')');

    $('#label_sum_price_c').html('');
    $('#label_sum_price_c').text('(total:'+formatNumber(sum_priceC)+')');

    $('#label_sum_price_d').html('');
    $('#label_sum_price_d').text('(total:'+formatNumber(sum_priceD)+')');
}

function buildPrice(group_id , masterproduct_id , pricetype , value){
    var html ="";
    html += '<span id=price_'+pricetype+'_'+group_id+' style="color:black !important;" data-price="0">' + value + "</span>";
    return html;

}
function buildTextArea(group_id){
    var html= "";
    html += "<textarea  class='form-control'  name='productinstalldescgroup_"+group_id+"'></textarea>";
    return html;
}

function amountbuildInput(group_id , amount){
    var html ="";
    html += "<input class='form-control' type='number' class='number' name='amountgroup_"+group_id+"' value='"+amount+"' step='.01' data-validate-minmax='0,999999' required='required'>";
    return html;
}



function buildRemoveRow(group_id){
    var html = "";
    html += '<span class="btn btn-danger"  style="cursor:pointer;font-size:small;" onclick="removeRow('+group_id+');"><i class="fa fa-trash"></i></span>';
    return html;
}

function removeRow(group_id){
    // remove row
    var table = $('#tablestructure').DataTable();
 
    var rows = table
        .rows( '#td_' + group_id )
        .remove()
        .draw();

    //ถ้าลบrow ให้วาดยอดรวมใหม่
    drawSum( group_id);

}
function dtDraw(){

    var columns = [  
                    { "width": "40%" },
                    { "width": "10%" , "class": "text-center "},
                    { "width": "10%" , "class": "text-center "},
                    { "width": "10%" , "class": "text-center "},
                    { "width": "10%" , "class": "text-center "},
                    { "width": "20%" , "class": "text-center "}
                  ]
   
    //
    var dataTable_ = $('#tablestructure').DataTable( {
        "processing": true,
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":true,
        "bInfo" : false,
        "searching": false,
        rowReorder: {
            selector: 'td:nth-child(0)'
        },
        responsive: true,
        initComplete: function(){
                     
         } ,
        "columns": columns
    });
    
    return dataTable_;
}