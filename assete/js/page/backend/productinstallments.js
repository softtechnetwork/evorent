// $('#reservation-time').datetimepicker();
var sumTotalInstallment_a = 0;
var sumTotalInstallment_b = 0;
var sumTotalInstallment_c = 0;
var sumTotalInstallment_d = 0;
var holdonpayment = [];

function cb_helppay_onchange(element , group){
    var cb_element = (element.is(':checked'))?true:false;
    var groupname = "group"+group;
    $('#is_productinstallmentn_group1').val(cb_element);
    
    if(cb_element == true){
        //$('#interest_' + groupname ).attr('readonly', true); 
        // build label
        $('#spanholdonpaymentgroup_' + group).show();

        //
        
        
        addholdonpayment( group );
    }else{
        //$('#interest_' + groupname ).attr('readonly', false); 
         // remove all child
         $('#spanholdonpaymentgroup_' + group).empty();
        // display : none
        $('#spanholdonpaymentgroup_' + group).hide();
        // holdonpayment : array
        holdonpayment[group] = [];
       
    }
 
}

function holdonpaymentinput(group , count){
    var html = "";
        html += '<div class="field item form-group" id="holdonypaymentgroup_'+count+'" >';
        /** label : งวดที่เท่าไหร่  */
            if(count == 0){
                html += '       <label class="col-form-label col-md-3 col-sm-3  label-align">งวดที่เท่าไหร่</label>';
            }
            else{
                html += '       <label class="col-form-label col-md-3 col-sm-3  label-align">&nbsp;</label>';
            }
        /** eof */

                html += '   <div class="col-md-2 col-sm-2 holdonypaymentgroup_'+group+'">';
                html += '       <input class="form-control" type="number" class="number" id="holdonpayment_'+group+'_'+count+'"  value="" onkeyup="onchangeholdonpayment('+group+','+count+')" name="holdonpayment_'+group+'_'+count+'" data-validate-minmax="1,999999"  placeholder="" required="" />';
                html += '   </div>';
            if(count > 0){
                html += '   <div class="col-md-2 col-sm-2">';
                html += '       <span class="btn btn-success" style="cursor:pointer;" onclick="removeholdonpayment('+group+','+count+' );"><i class="fa fa-minus" aria-hidden="true"></i></span>';
                html += '   </div>';
            }else{
                html += '   <div class="col-md-2 col-sm-2">';
                html += '       <span class="btn btn-success" style="cursor:pointer;" onclick="addholdonpayment('+group+' , '+count+');"><i class="fa fa-plus" aria-hidden="true"></i></span>';
                html += '   </div>';
            }
        html += "</div>";

    return html;
}

function onchangeholdonpayment(group , rowCount){
    var amountInstallment = $('#end_payment_in_instalment_group' + group).val();
    var value = $('#holdonpayment_'+group+'_'+rowCount).val();
    if(amountInstallment > 0){
        if(parseInt(value) > parseInt(amountInstallment)){
            // 
            alert('งวดที่พักชำระไม่ควรเกินจำนวนงวดทั้งหมด');
            // set new value    
            $('#holdonpayment_' + group + '_' + rowCount).val('');
        }else{
            // push new value
            if(holdonpayment[group] == undefined){
                holdonpayment[group] = [];
            }
            holdonpayment[group][rowCount] = parseInt(value);
        }
    } else{
        alert('กรุณากรอกจำนวนงวดทั้งหมด');
    }
}
function removeholdonpayment(group , count){
    // remove group 
    
    var value = parseInt($('#holdonpayment_'+group+'_'+count).val());

    // remove  array value 
    //holdonpayment[group] =  holdonpayment[group].filter(item => item != value);
    for(var i in holdonpayment[group]){
        if(holdonpayment[group][i] == value){
            holdonpayment[group].splice(i, 1);
        }
    }
    $('#holdonypaymentgroup_' + count).remove();
   
}

function addholdonpayment(group , count ){
    var newgroup = group +1;
    
    var count = $('.holdonypaymentgroup_' + group).length;
   
    // add new input
    var html = holdonpaymentinput( group  , count);
   

    $('#spanholdonpaymentgroup_' + group).append(html);
}
function build_holdonpayment(group){
    var html = '<div class="field item form-group" id="holdonypaymentgroup_'+group+'" style="display:none;">';
    html += '</div>';
    return html;
}
function display_group(formula_id){
    var base_url = $('input[name="base_url"]').val();
   
        $.ajax({
            type: "POST",
            url:  base_url + "/backend/productinstallments/ajaxdrawformulagroup",
            data: {formula_id: formula_id},
            success: function(data,status,xhr){
                var data = JSON.parse(data);
                if(data.status == true){
                    $("#formula_group").html('');
                    var html = "";
                    // create formula description
                    var formula = JSON.parse(data.formula.formula);
                    html += createFormulaDesc(formula);
                
                    // append html
                    $('#formula_group').append(html);
                }
                //console.log(data);
            }
        });
}
function createFormulaDesc(formula){
    //console.log(formula);
    var html ="";
    html += '<div class="field item form-group mt-2" id="d_payment_in_instalment_group'+formula.id+'" style="">';
    html += '<label class="col-form-label col-md-3 col-sm-3  label-align">รายละเอียด</label>';
    html += '<label class="col-form-label col-md-9 col-sm-9">'+ formula.formula_name +"="+ formula.formula_desc+'</label>';
    html += '</div>';

    return html;
}
function createInterestGroup(){
    var group_length  = $('.interest_group').length;
    var nextgroup_id = group_length + 1;
    var html = "";
    html += createbetween_instalment(nextgroup_id, null , null);
    html += create_instalment(nextgroup_id, null , null);
    html += createhelppay_installment(nextgroup_id);
    //html += createinterest_instalment(nextgroup_id, null);
    $('#fieldset_interest').append(html);
}

function editInterestGroup(nextgroup_id ,start_value , end_value , interest){
   
    var html = "";
    html += createbetween_instalment(nextgroup_id , start_value , end_value);
    html += create_instalment(nextgroup_id, null , null);
    html += createhelppay_installment(nextgroup_id);
    //html += createinterest_instalment(nextgroup_id , interest);
    $('#fieldset_interest').append(html);
}

function createhelppay_installment(nextgroup_id){
    var html ="";
    html += '<div class="field item form-group" id="div_helppay_group'+nextgroup_id+'">';
    html += '   <label for="ishelppaygroup_'+nextgroup_id+'" class="col-form-label col-9 col-md-3 col-sm-3  label-align">ต้องการพักชำระหนี้ ? <span class=""></span></label>';
    html += '   <label style="padding-left: 30px;padding-top:5px;">';
    html += "  <input type='checkbox' name='ishelppaygroup_"+nextgroup_id+"'  onclick='cb_helppay_onchange($(this),"+nextgroup_id+");' id='ishelppaygroup_"+nextgroup_id+"' value='' class='form-check-input col-3' /></label>"; 
    html += '</div>';
    html += '  <span id="spanholdonpaymentgroup_'+nextgroup_id+'">';                
    html += '</span>';

    return html;
}

function createbetween_instalment(nextgroup_id , start_value , end_value){
    var html ="";
    html += '<span id="group_'+nextgroup_id+'" class="interest_group">';
    html += '<div class="field item form-group mt-2 pt-2 " id="d_payment_in_instalment_group'+nextgroup_id+'" style="border-top: 1px solid gainsboro;">';
    html += '<label class="col-form-label col-md-3 col-sm-3  label-align">ผ่อนทั้งหมดกี่งวด</label>';
    html += '   <div class="col-9 col-md-2 col-sm-2" style="display:none;">';
    html += '       <input class="form-control" type="hidden" class="number" id="start_payment_in_instalment_group'+nextgroup_id+'"  value="1" name="startpaymentininstalmentgroup_'+nextgroup_id+'" data-validate-minmax="1,999999"  placeholder="1" required="" />';
    html += '   </div>';
    html += '   <div class="col-10 col-md-2 col-sm-2">';
    html += '       <input class="form-control" type="number" class="number" id="end_payment_in_instalment_group'+nextgroup_id+'"  value="'+end_value+'" name="endpaymentininstalmentgroup_'+nextgroup_id+'" min="1" step="1" placeholder="18" required="" />';
    html += '   </div>';
    html += '   <div class="col-2 col-md-2 col-sm-2">';
    html += '       <span class="btn btn-success" style="cursor:pointer;" onclick="removeInterestGroup('+nextgroup_id+');"><i class="fa fa-minus" aria-hidden="true"></i></span>';
    html += '   </div>';
    html += '</div>';

    html += '</span>';

    return html;
}

function create_instalment(nextgroup_id, start_value , end_value){
    var html ="";
    //html += '<span id="group_'+nextgroup_id+'" class="interest_group">';
    html += '<div class="field item form-group mt-2 pt-2 " id="instalment_group'+nextgroup_id+'">';
    html += '   <label class="col-form-label col-md-3 col-sm-3  label-align">งวดละ </label>';
    html += '   <div class="col-10 col-md-2 col-sm-2">';
    html += '       <input class="form-control" type="number" class="number" id="instalment_'+nextgroup_id+'" name="instalment_'+nextgroup_id+'"  value="500" min="1" required="" />';
    html += '   </div>';
    html += '   <label class="col-form-label col-md-3 col-sm-3">บาท</label>';
    html += '   </div>';
    html += '</div>';
    //html += '</span>';

    return html;
}

function createinterest_instalment(nextgroup_id , value){
    var html ="";
    html += '<div class="field item form-group" id="d_interest_group'+nextgroup_id+'" style="">';
    //html += '   <label class="col-form-label col-md-3 col-sm-3  label-align">ดอกเบี้ย (%)</label>';
    //html +='    <div class="col-md-2 col-sm-2">';
    //html +='        <input class="form-control" type="number" class="number" id="interest_group'+nextgroup_id+'" onchange="checkinterest(this.value,'+nextgroup_id+')" onkeyup="checkinterest(this.value,'+nextgroup_id+')" value="'+value+'" name="interestgroup_'+nextgroup_id+'" step=".01" placeholder="0.25" required="" />';
    //html +='    </div>';
    //html +='        <span id="buttoncalculategroup_'+nextgroup_id+'" onclick="drawtablecalculate('+nextgroup_id+')" class="btn btn-secondary text-white d-flex" style="cursor:pointer;">คำนวณยอดที่ต้องผ่อน</span>';
    //// html +='    <div class="col-md-2 col-sm-2">';
    //// html +='        <input class="form-control" type="number" class="number" id="interest_bath'+nextgroup_id+'" value="'+value+'"  step=".01" placeholder="" readonly="true" />';
    //// html +='    </div>';

    
    html += '   <label class="col-form-label col-md-3 col-sm-3  label-align"></label>';
    html += '   <div class="col-md-2 col-sm-2">';
    html +='        <span id="buttoncalculategroup_'+nextgroup_id+'" onclick="drawtablecalculate('+nextgroup_id+')" class="btn btn-secondary text-white d-flex" style="cursor:pointer;">คำนวณยอดที่ต้องผ่อน</span>';
    html += '   </div>';

    html +='</div>';

    return html;
}
function checkinterest(interest_value,group_id){
    var price  =$( "#product_parent_id option:selected" ).attr("data-price");
    var interest_bath = 0;
    if(interest_value > 0){
        interest_bath = price * ( interest_value / 100);
        interest_bath = interest_bath.toFixed(2);
    }
    $('#interest_bath'+group_id).val(interest_bath);
}
function removeInterestGroup(nextgroup_id){
    // remove instalment group
    $('#d_payment_in_instalment_group' + nextgroup_id).remove();
    // remove instalment group
    $('#instalment_group' + nextgroup_id).remove();
    // remove interest group
    $('#div_helppay_group' + nextgroup_id).remove();
    // remove interest group
    $('#d_interest_group' + nextgroup_id).remove();

    // remove holdon payment group
    $('#spanholdonpaymentgroup_'+nextgroup_id).remove();

    // on edit event
    if($('#producthirepurchasecodegroup_' + nextgroup_id).length){
        $('#producthirepurchasecodegroup_' + nextgroup_id).remove();
    }
    if($('#productrelint_' + nextgroup_id).length){
        $('#productrelint_' + nextgroup_id).remove();
    }

    // remove array 
    if(remove_group.length > 0){
        delete remove_group[nextgroup_id];
    }
}


function drawtablecalculate( group){
 sumTotalInstallment_a = 0;
 sumTotalInstallment_b = 0;
 sumTotalInstallment_c = 0;
 sumTotalInstallment_d = 0;

 sumTotalInstallmentVAT_a = 0;


    //clear table
    $('#tablestructure').DataTable()
    .clear()
    .draw();
    //สูตร
    var formula = $('input[name=formula]:checked', '#create_productinstallments_product').val()
    
    if(formula > 0){
        //เช็คดอกเบี้ย
        var interest = $('#interest_group' + group).val();

        if(parseFloat(interest) >= 0){
            // show table product installments 
            $('#fieldset_productinstallments').show();

            var dataTable_ = dtDraw();
            // get key of object
            var key = getKeyByValue(product_install , parseInt($('#product_install_id').val()));
            var formula_key = getKeyByValue(formula_obj , parseInt(formula));
            
            //send product key value 
            var countAmount = (holdonpayment[group] != null) ? holdonpayment[group].length : 0;
            var amountInstallment = parseInt($('#end_payment_in_instalment_group' + group).val());
            var countInstallment = amountInstallment - countAmount;
            //alert(countInstallment);
            // loading show
            for(var rowCount = 1; rowCount<= amountInstallment; rowCount++){
             
                dtAddRow(product_install[key] , parseFloat(interest) , rowCount , formula_obj[formula_key]  , countInstallment , group);
            }
            
        }else{
            alert('กรุณากรอกดอกเบี้ย');
        }
    }else{
        alert('กรุณาเลือกสูตรคำนวณดอกเบี้ย');
    }
    

}
function dtAddRow(objprod_install_set , interest , rowCount , formula_short , amountInstallment , group){
    var dataTable_ = dtDraw();
    
    var buildPriceHTML = tbl_buildPrice(objprod_install_set);
    var buildInterestHTML = tbl_buildinterest(objprod_install_set , interest)
    var buildInstallmentsHTML = tbl_buildInstallments(objprod_install_set , formula_short , interest , amountInstallment , rowCount , group);

    var buildInstallmentsIncludevat = tbl_buildInstallmentsVAT(objprod_install_set , formula_short , interest , amountInstallment , rowCount , group);
    dataTable_.row.add( [
        parseInt(rowCount),
        buildPriceHTML,
        buildInterestHTML,
        buildInstallmentsHTML,
        buildInstallmentsIncludevat
        
    ] ).node().id = 'td_' + rowCount;
    // ถ้า row สุดท้าย
    dataTable_.draw( false );
   
    // insert footer detail
    if(rowCount == amountInstallment){
        var tablesum = tbl_buildsumtotal();
        // clear data before add
        $('#footerData').html('');
        $('#footerData').append("<tr><td>ยอดรวม</td><td colspan='4' class='text-right'>"+tablesum+"</td></tr>");
        
    }
    
    

}

function tbl_buildsumtotal(){
    var html =""
    html += "รวมเป็น(ไม่รวมVAT):" + sumTotalInstallment_a.toFixed(2);
    html +="<br>";
    html += "รวมเป็น(รวมVAT)" + sumTotalInstallmentVAT_a.toFixed(2);
    html +="<br>";
    // html += "ราคาขาย C:" + sumTotalInstallment_c.toFixed(2);
    // html +="<br>";
    // html += "ราคาขาย D:" + sumTotalInstallment_d.toFixed(2);

    return html;
}

function tbl_buildinterest(objprod_install_set , interest){
    var html = "";
    $.each( objprod_install_set, function( key, value ) {
        if(key == "price_a"){
            var result = value * ( interest / 100);
            
            html += "ดอกเบี้ย ("+interest+"%):" + result.toFixed(2);
            html +="<br>";
        }
        // else if(key == "price_b"){
        //     var result = value * ( interest / 100);
        //     html += "ดอกเบี้ย B ("+interest+"%):" + result.toFixed(2);
        //     html +="<br>";
        // }else if(key == "price_c"){
        //     var result = value * ( interest / 100);
        //     html += "ดอกเบี้ย C ("+interest+"%):" + result.toFixed(2);
        //     html +="<br>";
        // }else if(key == "price_d"){
        //     var result = value * ( interest / 100);
        //     html += "ดอกเบี้ย D ("+interest+"%):" + result.toFixed(2);
        // }
        
    });

    return html;
}
function tbl_buildPrice(objprod_install_set){
    var html = "";
    $.each( objprod_install_set, function( key, value ) {
        if(key == "price_a"){
            html += "รวมเป็น(ไม่รวมVAT):" + value;
            html +="<br>";
        }

        // else if(key == "price_b"){
        //     html += "ราคาขาย B:" + value;
        //     html +="<br>";
        // }else if(key == "price_c"){
        //     html += "ราคาขาย C:" + value;
        //     html +="<br>";
        // }else if(key == "price_d"){
        //     html += "ราคาขาย D:" + value;
        // }
        
    });

    return html;
}
function tbl_buildInstallments(objprod_install_set, formula_short , interest , amountInstallment , rowCount , group){
    var formula_calculate = JSON.parse(formula_short.formula);
    console.log(amountInstallment);
    var html = "";
    $.each( objprod_install_set, function( key, value ) {
        if(key == "price_a"){
            var product_price = value;
            var nofmonth_product_installments  = amountInstallment;

            var result = tbl_calresultinstallment( product_price , nofmonth_product_installments 
                , value , amountInstallment , formula_calculate , interest);
            // sum total installment
             sumTotalInstallment_a += parseFloat(result);
             if(jQuery.inArray(rowCount, holdonpayment[group]) != -1) {
                html += "พักชำระหนี้";
             }else{
                html += "ยอดผ่อนชำระ(" + value +"):" + result;
                html +="<br>";
             }
        }
        // else if(key == "price_b"){
        //     var product_price = value;
        //     var nofmonth_product_installments  = amountInstallment;

        //     var result = tbl_calresultinstallment( product_price , nofmonth_product_installments 
        //         , value , amountInstallment , formula_calculate , interest);

        //       // sum total installment
        //       sumTotalInstallment_b += parseFloat(result);
        //        if(jQuery.inArray(rowCount, holdonpayment[group]) != -1) {
        //              html += "&nbsp;";
        //         }else{
        //             html += "ยอดผ่อนชำระ B(" + value +"):" + result;
        //             html +="<br>";
        //         }
        // }else if(key == "price_c"){
        //     var product_price = value;
        //     var nofmonth_product_installments  = amountInstallment;

        //     var result = tbl_calresultinstallment( product_price , nofmonth_product_installments 
        //         , value , amountInstallment , formula_calculate , interest);
            
        //     // sum total installment
        //     sumTotalInstallment_c += parseFloat(result);
        //     if(jQuery.inArray(rowCount, holdonpayment[group]) != -1) {
        //         html += "&nbsp;";
        //     }else{
        //         html += "ยอดผ่อนชำระ C(" + value +"):" + result;
        //         html +="<br>";
        //     }

        // }else if(key == "price_d"){
        //     var product_price = value;
        //      var nofmonth_product_installments  = amountInstallment;

        //     var result = tbl_calresultinstallment( product_price , nofmonth_product_installments 
        //         , value , amountInstallment , formula_calculate , interest);
              
        //     // sum total installment
        //     sumTotalInstallment_d += parseFloat(result);
        //     if(jQuery.inArray(rowCount, holdonpayment[group]) != -1) {
        //         html += "&nbsp;";
        //     }else{
        //         html += "ยอดผ่อนชำระ D(" + value +"):" + result;
        //     }
        // }
        
    });

    return html;
}

function tbl_buildInstallmentsVAT(objprod_install_set, formula_short , interest , amountInstallment , rowCount , group){
    var formula_calculate = JSON.parse(formula_short.formula);
    console.log(amountInstallment);
    var html = "";
    $.each( objprod_install_set, function( key, value ) {
        if(key == "price_a"){
            var product_price = value;
            var nofmonth_product_installments  = amountInstallment;

            var result = tbl_calresultinstallment( product_price , nofmonth_product_installments 
                , value , amountInstallment , formula_calculate , interest);
            // sum total installment
            var vat = parseFloat(result) * ( 7 / 100);
            var total = parseFloat(result) + vat;
            sumTotalInstallmentVAT_a += total;
             if(jQuery.inArray(rowCount, holdonpayment[group]) != -1) {
                html += "พักชำระหนี้";
             }else{
                html += "ยอดผ่อนชำระ(" + value +"):" + total;
                html +="<br>";
             }
        }
        
    });

    return html;
}

function tbl_calresultinstallment(product_price , nofmonth_product_installments , value , amountInstallment , formula_calculate , interest){
    
    var result = eval(formula_calculate.formula_short);
    result = result.toFixed(2);

    return result;
}

function getKeyByValue(object, value) {
  
    var key =  Object.keys(object).find(key => object[key].id === value);
    return key;
  }
  


function dtDraw(){

    var columns = [  
                    { "width": "10%" , "class": "text-left"},
                    { "width": "15%" , "class": "text-left"},
                    { "width": "25%"  , "class": "text-left"},
                    { "width": "25%"  , "class": "text-center"},
                    { "width": "25%"  , "class": "text-center"},
                   
                  ]
   
    //
    var dataTable_ = $('#tablestructure').DataTable( {
        "processing": true,
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":true,
        "bInfo" : false,
        "searching": false,
        "rowReorder": {
            selector: 'td:nth-child(0)'
        },
        "responsive": true,
        "aoColumns": [
            { "sType": "numeric" },
            null,
            null,
            null
        ],
        initComplete: function(){
                     
         } ,
        "columns": columns
    });
    
    return dataTable_;
}

