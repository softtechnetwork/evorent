
    // input id create form 
    var input_id = ["#name","#sname","#address","#province","#amphurs","#district","#tel","#email"];

    // input premise id create form 
    var preise = ["contract1","contract2","contract3","contract4","idcardPremise","houseregisPremise","idcardSupporter","houseregisSupporter"];

    // base url
    var base_url = $('input[name="base_url"]').val();

    //################  Validate input #############//
    $.each(input_id, function (i, val) {
        
        switch(val){
            case '#province':
                $(val).change(function(){
                    $(val).removeClass("input-valid");
                    
                    var province = $(val).val();
                    var url = base_url+"admin/customer/getamphoe";
                    var amphure = $("#amphurs").selector; // 
                    replaceAmphure(province, url, amphure);
                });
            break;
            case '#amphurs':
                $(val).change(function(){ 
                    $(val).removeClass("input-valid");

                    var amphurs = $(val).val();
                    var url = base_url+"admin/customer/getdistric";
                    var district = $("#district").selector; // 
                    replaceDistrict(amphurs, url, district);
                });
            break;
            case '#district':
                $(val).change(function(){ 
                    $(val).removeClass("input-valid");

                    var province = $("#province").val();
                    var amphue = $("#amphurs").val();
                    var district = $("#district").val();
                    var url = base_url+"admin/customer/getzipcode";
                    var zipcode = $("#zipcode").selector; // 
                    replaceZipcode(province,amphue,district, url, zipcode);
                });
            break;
            case '#idcard':
                $(val+"-validat").hide();
                $(val).change(function(){ 
                    $(val).removeClass("input-valid");

                    var idcard = $(val).val();
                    var num = idcard.split("-");
                    var id ="";
                    for (i = 0; i < num.length; i++) {
                        id += num[i];
                    }
                    
                    if(id != ''){
                        var url = base_url+"admin/customer/getCustomerByIdcard";
                        validateIdcards(id, url, val);
                    }else{
                        $(val).removeClass("input-valid");
                        $(val+"-validat").html(" ");
                        $(val+"-validat").hide();
                    }
                    //
                });
            break;
            case '#tel':
                $(val+"-validat").hide();
                $(val).change(function(){ 
                    var tels = $(val).val();
                    if(tels != '' ){
                        if(tels.length != 10){
                            $(val).addClass("input-valid");
                            $(val+"-validat").html("หมายเลขมือถือ ไม่ให้ถูกต้อง");
                            $(val+"-validat").show();

                        }else{
                            $(val+"-validat").html(" ");
                            $(val).removeClass("input-valid");
                            $(val+"-validat").hide();
                        }
                    }else{
                        $(val+"-validat").html(" ");
                        $(val).removeClass("input-valid");
                        $(val+"-validat").hide();
                    }
                });
            break;
            default:
                $(val).change(function(){ 
                    $(val).removeClass("input-valid");
                });
                break;
        }
    });

    //################  Trigger brows file #############//
    $('#thumnails-contract1, #thumnails-contract2, #thumnails-contract3, #thumnails-contract4, #thumnails-idcardPremise, #thumnails-houseregisPremise, #thumnails-idcardSupporter, #thumnails-houseregisSupporter').click(function () {
        var files = $(this)['context']['attributes']['browsid'].value;
        browsFile(files, this.id);
    });

    function browsFile(files, place){
        $("#"+files).trigger('click');
        $("#"+files).change(function(){
            readURL(this,place);
        });
    }

    function readURL(input, place) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $('#'+place).removeClass("thumnails-premise-valid");
            reader.onload = function (e) {
                $('#'+place).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    //################  submit edit form #############//
    $('#customer-edit-form').submit(function() {
        // DO STUFF...
        var valids  = 0;
        //################  Validate img premis #############//
        /*$.each(preise, function (i, val) {
            var preise = $('#'+val);
            if(preise.val() == '' ){
                $('#thumnails-'+val).addClass("thumnails-premise-valid");
                valids++;
            }
        });*/

        //################  Validate input #############//
        $.each(input_id, function (i, val) {
            if($(val).val() == '' || $(val).val() == null ){
                $(val).addClass("input-valid");
                valids++;
            }
        });

       
        if(valids == 0){
            return true;
        }else{
            return false;
        }
    });

    function validateIdcards(id, url, val){
        var resIdcard =  getCustomerByIdcard(id, url);
        
        if(resIdcard.length > 0){
        $(val).addClass("input-valid");
        $(val+"-validat").html("หมายเลขบัตรประชาชน มีการใช้งานเเล้ว");
        $(val+"-validat").show();
        }else{
            var sum = 0;
            var total = 0;
            var digi = 13;

            var arr = [];
            for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}
        
            for(i=0;i<12;i++){
                sum = sum + (arr[i] * digi);
                digi--;
            }

            total = ((11 - (sum % 11)) % 10);
            
            if(total != arr[12]){
                $(val).addClass("input-valid");
                $(val+"-validat").html("หมายเลขบัตรประชาชน ไม่ให้ถูกต้อง");
                $(val+"-validat").show();
            }else{
                $(val).removeClass("input-valid");
                $(val+"-validat").html(" ");
                $(val+"-validat").hide();
            }
        }
    }
    