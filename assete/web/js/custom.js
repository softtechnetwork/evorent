
    function EleChange(searchEle){
        $.each(searchEle, function (i, val) {
            $(val).change(function() {
                if(this.value != ''){
                    $(val).removeClass("input-valid");
                }
            });
        });
    }

    function getWebInstallment(resObj, searchEle){
        var searchJson = SearchJson(searchEle);
        $(resObj["elementTable"]).html(null);
        var res = null;
        $.ajax({
            url: resObj["getRes"], //ทำงานกับไฟล์นี้
            data: { 'Search':searchJson},  //ส่งตัวแปร
            type: "POST",
            dataType: 'json',
            async:false,
            success: function(data, status) {

                $("#installment-panel").html(null);
                if(data['res'].length > 0){

                    var product_panel = null;

                    // ที่อยููปัจจุบัน
                    var address2 = null;
                    if(data['address2'] != null){
                        var str = null;
                        str = data['address2'][0]['category']+' : '+data['address2'][0]['address'];
                        str += ' ตำบล / แขวง '+data['address2'][0]['district_name'];
                        str += ' อำเภอ / เขต '+data['address2'][0]['amphur_name'];
                        str += ' จังหวัด '+data['address2'][0]['province_name'];
                        str += ' รหัสไปรษณีย์ '+data['address2'][0]['zip_code'];
                        address2 = str;
                    }
                    // ข้อมูลนายหน้า
                    var supporter = null;
                    if(data['supporter'] != null){
                        var str = null;
                        str = '<li class="list-inline-item"  style="display: block;">ผู้คำประกัน : '+data['supporter'][0]['fname']+' '+data['supporter'][0]['lname']+'</li>';
                        str += '<li class="list-inline-item"  style="display: block;">โทรศัพท์มือถือผู้คำประกัน : '+data['supporter'][0]['tel']+'</li>';
                        //str += '<li class="list-inline-item"  style="display: block;">อีเมล์ผู้คำประกัน : '+data['supporter'][0]['email']+'</li>';
                        supporter = str;
                    }
                    // ข้อมูล ลูกค้า
                    var customer = null;
                    if(data['contract']!= null ){
                        customer = '<div class="col-md-6 mt-1">';
                        customer += '<h4>ข้อมูลลูกค้า:</h4>';
                        customer += '<ul class="list-inline">';
                        customer += '<li class="list-inline-item"  style="display: block;">หมายเลขบัตรประชาชน : '+data['contract'][0]['idcard']+'</li>';
                        customer += '<li class="list-inline-item"  style="display: block;">ชื่อ : '+data['contract'][0]['firstname']+' '+data['contract'][0]['lastname']+'</li>';
                        if(address2 != null){
                            customer += '<li class="list-inline-item"  style="display: block;">'+address2+'</li>';
                        }
                        customer += '<li class="list-inline-item"  style="display: block;">โทรศัพท์มือถือ : '+data['contract'][0]['tel']+'</li>';
                        customer += '<li class="list-inline-item"  style="display: block;">อีเมล์ : '+data['contract'][0]['email']+'</li>';
                        
                        if(supporter != null){
                            customer += supporter;
                        }
                        customer += '</ul>';
                        customer += '</div>';
                    }

                    // ข้อมูล สินค้า
                    var installationLocation = null;
                    if(data['installationLocation']!= null){
                        var str = null;
                        str = 'สถานที่ติดตั้งสินค้า : '+data['installationLocation'][0]['address'];
                        str += ' ตำบล / แขวง '+data['installationLocation'][0]['district_name'];
                        str += ' อำเภอ / เขต '+data['installationLocation'][0]['amphur_name'];
                        str += ' จังหวัด '+data['installationLocation'][0]['province_name'];
                        str += ' รหัสไปรษณีย์ '+data['installationLocation'][0]['zip_code'];
                        installationLocation = str;
                    }
                    // ข้อมูล สัญญา
                    var contract = null;
                    if(data['contract']!= null){
                        contract = '<div class="col-md-6 mt-1">';
                        contract += '<h4>ข้อมูลสินค้า:</h4>';

                        contract += '<ul class="list-inline">';
                        //contract += '<li class="list-inline-item"  style="display: block;">รหัสสัญญา : '+data['contract'][0]['temp_code']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">สินค้า  : '+data['contract'][0]['product_name']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">ยี่ห้อ  : '+data['contract'][0]['brand_name']+'   รุ่น  : '+data['contract'][0]['product_version']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">วันที่เริ่มสัญญา   : '+data['contract'][0]['payment_start_date']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">ค่าผ่อนรายเดือน  : '+data['contract'][0]['monthly_rent']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">ระยะเวลาการเช่า   : '+data['contract'][0]['rental_period']+'   เงินดาวน์  : '+data['contract'][0]['advance_payment']+'</li>';
                        contract += '<li class="list-inline-item"  style="display: block;">สถานะ    : '+data['contract'][0]['label']+'</li>';
                        if(installationLocation != null){
                            contract += '<li class="list-inline-item"  style="display: block;">'+installationLocation+'</li>';
                        }
                        contract += '</ul>';
                        contract += '</div>';
                        
                    }

                    product_panel = '<div class="col-md-12 mt-1">';
                    product_panel += '<div class="card">';
                    product_panel += '<div class="card-body">';
                    product_panel += '<div class="row">';
                    product_panel += customer;
                    product_panel += contract;
                    product_panel += '</div>';
                    product_panel += '</div>';
                    product_panel += '</div>';
                    product_panel += '</div>';
                    
                    
                    // ข้อมูล การผ่อน
                    if(data['res']!= null){
                        var tr = '';
                        $.each(data['res'], function (i, val) {
                            var payment_amount = '';
                            if(val['payment_amount'] != null){
                                payment_amount = addCommas(val['payment_amount']);
                            }

                            var installment_date = '';
                            if(val['installment_date'] != null){
                                installment_date = dateFormate(val['installment_date']);
                            }

                            tr += '<tr>';
                            tr += '<td class=" ">'+val['period']+'</td>';
                            tr += '<td class=" ">'+addCommas(val['installment_payment'])+'</td>';
                            tr += '<td class=" ">'+dateFormate(val['payment_duedate'])+'</td>';
                            tr += '<td class=" " style="color: '+val['background_color']+';">'+val['status_label']+'</td>';
                            tr += '<td class=" ">'+payment_amount+'</td>';
                            tr += '<td class=" ">'+installment_date+'</td>';
                            tr += '</tr>';
                        });

                        product_panel += '<div class="col-md-12 mt-1">';
                        product_panel += '<div class="card">';
                        product_panel += '<div class="card-body">';
                        product_panel += '<h4>ตารางการผ่อนรายดือน:</h4>';
                        product_panel += '<div class="table-responsive">';
                        product_panel += '<table class="table table-striped" id="installment-table">';
                        product_panel += '<thead><tr>';
                        product_panel += '<th><label>งวด</label></th>';
                        product_panel += '<th><label>ราคาผ่อน</label></th>';
                        product_panel += '<th><label>วันที่ครบกำหนดชำระ</label></th>';
                        product_panel += '<th><label>สถานะ</label></th>';
                        product_panel += '<th><label>จำนวนที่ชำระ</label></th>';
                        product_panel += '<th><label>วันที่ชำระ</label></th>';
                        product_panel += '</tr></thead>';
                        product_panel += '<tbody>';
                        product_panel += tr;
                        product_panel += '</tbody>';
                        product_panel += '</table>';
                        product_panel += '</div>';
                        product_panel += '</div>';
                        product_panel += '</div>';
                        product_panel += '</div>';
                    }
                    
                    $("#installment-panel").append(product_panel);
                    $('#installment-section').removeClass("content-padding-bottom");
                    $('#installment-panel-empty').addClass("installment-display");
                    $('#installment-panel').removeClass("installment-display");
                }else{
                    $('#installment-section').addClass("content-padding-bottom");
                    $('#installment-panel').addClass("installment-display");
                    $('#installment-panel-empty').removeClass("installment-display");
                }
                
            },
            error: function(xhr, status, exception) { 
                //console.log(xhr);
            }
        });
    }

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function dateFormate(nStr)
    {
        var d = new Date(nStr);

        var month = d.getMonth()+1;
        var day = d.getDate();
        //var hour = d.getHours();
        //var minute = d.getMinutes();
        //var second = d.getSeconds();

        var output =  ((''+day).length<2 ? '0' : '') + day + '-' +((''+month).length<2 ? '0' : '') + month + '-' + d.getFullYear();

        /*var output = d.getFullYear() + '-' +((''+month).length<2 ? '0' : '') + month + '-' +((''+day).length<2 ? '0' : '') + day + ' ' +
        ((''+hour).length<2 ? '0' :'') + hour + ':' +
        ((''+minute).length<2 ? '0' :'') + minute + ':' +
        ((''+second).length<2 ? '0' :'') + second;*/
        return output;
        
    }

    function SearchJson(searchEle){
        search = {};
        $.each(searchEle, function (i, val) {
            var resEle = $(val).val();
            var objKey = $(val).attr('id');
            search[objKey] = resEle;
        });
        return  JSON.stringify(search);
    }

    function autoTab(obj) {
        var pattern = new String("_-____-_____-_-__"); // กำหนดรูปแบบในนี้
        var pattern_ex = new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText = new String("");
        var obj_l = obj.value.length;
        var obj_l2 = obj_l - 1;
        for (i = 0; i < pattern.length; i++) {
            if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                returnText += obj.value + pattern_ex;
                obj.value = returnText;
            }
        }
        if (obj_l >= pattern.length) {
            obj.value = obj.value.substr(0, pattern.length);
        }
    }
